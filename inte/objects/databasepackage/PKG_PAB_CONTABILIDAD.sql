CREATE OR REPLACE PACKAGE         PKG_PAB_CONTABILIDAD
IS
   /**  Package el cual contrendra todas las funciones y procedimientos relacionados con la contabilidad
   -- @Santander */
   /**************************VARIABLE GLOBALES************************************/
   v_NOM_PCK     VARCHAR2 (30) := 'PKG_PAB_CONTABILIDAD';
   ERR_CODE      NUMBER := 0;
   ERR_MSG       VARCHAR2 (300);
   v_DES         VARCHAR2 (300);
   p_s_mensaje   VARCHAR2 (400);
   v_dia_ini     DATE := TRUNC (SYSDATE) + INTERVAL '1' MINUTE;
   v_dia_fin DATE
         := TRUNC (SYSDATE) + INTERVAL '23' HOUR + INTERVAL '59' MINUTE ;

   /**************************FIN VARIABLE GLOBALES********************************/

   --***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_CONTABILIDAD
   -- Objetivo: Genera la interfaz contable del sistema Altos Montos, Parte 1 solo pagos enviados
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_CNTBL_ALMOT,PABS_DT_DETLL_OPRCN,PABS_DT_TIPO_OPRCN_SALID_MNSJ,PABS_DT_CENTA_CNTBL,PABS_DT_SISTM_ENTRD_SALID
   -- Fecha: 13/06/2016
   -- Autor: Santander
   -- Input: p_FEC_PCS -> Fecha Proceso.
   -- Output: p_CURSOR -> PABS_DT_CNTBL_ALMOT, objeto del tipo tabla.
   --        p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: 04/08/2016, Se Modifica comentario.
   --***********************************************************************************************
   PROCEDURE Sp_PAB_CONTABILIDAD (p_FEC_PCS   IN     VARCHAR2,
                                  p_CURSOR       OUT SYS_REFCURSOR,
                                  p_ERROR        OUT NUMBER);

   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_AKON
   -- Objetivo: Genera la interfaz contable del sistema Altos Montos, Parte 1 solo pagos enviados
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN,PABS_DT_TIPO_OPRCN_SALID_MNSJ,PABS_DT_CENTA_CNTBL,PABS_DT_SISTM_ENTRD_SALID
   -- Fecha: 13/06/2016
   -- Autor: Santander
   -- Input: p_FEC_PCS -> Fecha Proceso.
   -- Output: p_CURSOR -> PABS_DT_CNTBL_ALMOT, objeto del tipo tabla.
   --        p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: 04/08/2016, Se Modifica comentario.
   --***********************************************************************************************
   PROCEDURE Sp_PAB_AKON (p_FEC_PCS   IN     VARCHAR2,
                          p_CURSOR       OUT SYS_REFCURSOR,
                          p_ERROR        OUT NUMBER);
END PKG_PAB_CONTABILIDAD;