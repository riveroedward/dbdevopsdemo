CREATE OR REPLACE PACKAGE         PKG_PAB_OPERACION_INT
IS
   v_NOM_PCK       VARCHAR2 (30) := 'PKG_PAB_OPERACION_INT';
   ERR_CODE        NUMBER := 0;
   ERR_MSG         VARCHAR2 (300);
   v_DES           VARCHAR2 (300);
   p_s_mensaje     VARCHAR2 (400);
   P_FEC_ICO_PCS   VARCHAR2 (30);
   V_DES_BIT       VARCHAR2 (100);
   V_DES           VARCHAR2 (300);
   P_GLS_DET_PCS   VARCHAR2 (300);
   P_COD_ERR_BIB   CHAR (6) := 0;


   V_DIA_INI       DATE := TRUNC (SYSDATE) + INTERVAL '1' MINUTE;
   V_DIA_FIN DATE
         := TRUNC (SYSDATE) + INTERVAL '23' HOUR + INTERVAL '59' MINUTE ;

   --***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_COMBO_MONEDA
   -- Objetivo: Procedimiento que retorna las monedas, según Flag, 0 Todas , 1 extranjeras
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_MONDA_ALMOT
   -- Fecha: 23-03-2019
   -- Autor: Santander CAH
   -- Input:
   --  p_FLG_MON
   -- Output:
   -- p_CURSOR -> RESULTADOS DE CONSULTA.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_COMBO_MONEDA (p_FLG_MON   IN     NUMBER,
                                  p_CURSOR       OUT SYS_REFCURSOR);

   /************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUS_OPE_LIB_ITR
   -- Objetivo: PROCEDIMIENTO ALMACENADO QUE ENTREGA LOS TIPOS
   -- DE OPERACION LIBERADAS, SEGUN FILTROS INGRESADOS
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN_INTER
                     PABS_DT_SISTM_ENTRD_SALID
   -- Fecha: 26-03-2019
   -- Autor: Santander - CAH
   -- Input:
   --    p_COD_TPO_OPE_AOS_LQD   Tipo de Operación
   --    p_COD_SIS_ENT_ITR       Código sistema Entrada
   --    p_COD_SIS_SAL_ITR       Código Sistema Salida
   --    p_NUM_FOL_OPE_ITR       Folio Operación
   --    p_NUM_FOL_NMN_ITR       Folio Nómina
   --    p_FLG_EGR_ING_ITR       Flag de ingreso o Egreso
   --    p_COD_DVI_ITR           MOneda
   --    p_IMP_DSD               Monto desde
   --    p_IMP_HST               Monto Hasta
   --    p_NUM_DOC_BFC_ITR       Rut Beneficiario
   --    p_NUM_CTA_BFC_ITR       Cuenta Beneficiario
   --    p_NOM_BFC_ITR           Nombre Beneficiario
   --    p_NOM_ODN_ITR           Nombre Ordenante
   --    p_NUM_DOC_ODN_ITR       Rut Ordenante
   --    p_NUM_CTA_ODN_ITR       Cuenta Ordenante
   --    p_COD_TPO_FPA           Forma Pago
   -- Output:
   --        p_CURSOR-> SYS_REFCURSOR, RESULTADOS DE CONSULTA.
   --        p_ERROR-> Devuelve error
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_BUS_OPE_LIB_ITR (
      p_COD_TPO_OPE_AOS_LQD   IN     CHAR,
      p_COD_SIS_ENT_ITR       IN     CHAR,
      p_COD_SIS_SAL_ITR       IN     CHAR,
      p_NUM_FOL_OPE_ITR       IN     NUMBER,
      p_NUM_FOL_NMN_ITR       IN     NUMBER,
      p_FLG_EGR_ING_ITR       IN     NUMBER,
      p_COD_DVI_ITR           IN     CHAR,
      p_IMP_DSD               IN     NUMBER,
      p_IMP_HST               IN     NUMBER,
      p_NUM_DOC_BFC_ITR       IN     CHAR,
      p_NUM_CTA_BFC_ITR       IN     VARCHAR2,
      p_NOM_BFC_ITR           IN     CHAR,
      p_NOM_ODN_ITR           IN     CHAR,
      p_NUM_DOC_ODN_ITR       IN     CHAR,
      p_NUM_CTA_ODN_ITR       IN     VARCHAR2,
      p_COD_TPO_FPA           IN     CHAR,
      p_EST_AOS               IN     VARCHAR2,
      p_FLG                   IN     CHAR,
      p_CURSOR                   OUT SYS_REFCURSOR,
      p_ERROR                    OUT NUMBER
   );

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_OBT_DSC_SIS_ENT_SAL
   -- Objetivo: Funcion que devuelve la descripcion del código de pago de una operación interna
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_TIPO_FRMPG
   -- Fecha: 06-04-2019
   -- Autor: Santander CAH
   -- Input:
   --  p_COD_SIS_ENT_SAL -> Codigo de sistema salida
   -- Output:
   -- N/A.
   -- Input/Output:
   -- N/A.
   -- Retorno:
   -- v_DSC_SIS_ENT_SAL -> Descripcion de codigo de sistema salida consultado
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   FUNCTION FN_PAB_OBT_DSC_CNL_PAGO (p_COD_SIS_ENT_SAL IN CHAR)
      RETURN VARCHAR2;

   /************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_CAMBIA_CNL_PAGO
   -- Objetivo: Procedimiento almacenado que cambia de de canal de pago a operaciones interna
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN_INTER

   -- Fecha: 27-03-2019
   -- Autor: Santander - CAH
   -- Input:
   --    p_NUM_FOL_OPE_ITR     Folio operación
   --    p_FEC_ISR_OPE_ITR     Fecha de inserción
   --    p_COD_EST_AOS         Nuevo estado operación
   --    p_COD_USR             Código usuario
   -- Output:
   --        p_ERROR-> Devuelve error
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_PAB_CAMBIA_CNL_PAGO (p_NUM_FOL_OPE_ITR   IN     NUMBER,
                                     p_FEC_ISR_OPE_ITR   IN     DATE,
                                     p_CANAL_PAGO        IN     CHAR,
                                     p_CANAL_SAL         IN     CHAR,
                                     p_NUM_CTA           IN     VARCHAR2,
                                     p_COD_USR           IN     VARCHAR2,
                                     p_ERROR                OUT NUMBER);

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_BUS_CUENTA_PAGO
   -- Objetivo: Procedimiento almacenado que busca numero relacionado a la cuenta del tipo abono
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_CENTA_CNTBL
   -- Fecha: 01-04-2019
   -- Autor: Santander - CAH
   -- Input:
   -- p_COD_SIS_SAL ->  codigo sistema entrada salida
   -- p_COD_DVI      >  Tipo de operaci?n Ingreso o Egreso
   -- Output:
   -- p_CURSOR     -> Cursor con registro encontrado
   -- p_ERROR      -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   --                 (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_BUS_CUENTA_PAGO (p_COD_SIS_SAL   IN     CHAR,
                                     p_COD_DVI       IN     CHAR,
                                     p_CURSOR           OUT SYS_REFCURSOR,
                                     p_ERROR            OUT NUMBER);

   /************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_OBT_EST_ACT_OPE_ITR
   -- Objetivo: Procedimiento que retorna el estado actual de la operación interna
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN_INTER
   -- Fecha: 26-03-2019
   -- Autor: Santander CAH
   -- Input:    p_NUM_FOL_OPE: Numero de Folio de una Operacion
   --           p_FEC_ISR_OPE: Fecha de una Operacion
   -- Output:
   --           p_COD_EST_AOS: Estado actual de la operacion
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_PAB_OBT_EST_ACT_OPE_ITR (p_NUM_FOL_OPE_ITR   IN     NUMBER,
                                         p_FEC_ISR_OPE_ITR   IN     DATE,
                                         p_COD_EST_AOS          OUT NUMBER);

   -- **************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_ELI_OPE_ITR
   -- Objetivo: Procedimiento que actualiza el estado a eliminada una operación interna
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN_INTER
   -- Fecha: 01-04-2019
   -- Autor: Santander - CAH
   -- Input:
   --        p_NUM_FOL_OPE_ITR -> Foliador del banco
   --        p_FEC_ISR_OPE_ITR -> Fecha de Operación
   --        p_COD_USR         -> Rut usuario
   --        p_GLS_MTV_EST     -> Motivo o glosa de eliminación
   -- Output:
   --        p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- **************************************************************************************************
   PROCEDURE SP_PAB_ELI_OPE_ITR (p_NUM_FOL_NMN       IN     NUMBER,
                                 p_FEC_ISR_OPE_ITR   IN     DATE,
                                 p_NUM_FOL_OPE_ITR   IN     NUMBER,
                                 p_COD_USR           IN     VARCHAR2,
                                 p_GLS_EST_ELI       IN     VARCHAR2,
                                 p_ERROR                OUT NUMBER);

   -- **************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_EST_OPE_ITR
   -- Objetivo: Procedimiento que actualiza el estado de una operación interna
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN_INTER
   -- Fecha: 09-07-2019
   -- Autor: Santander - CAH
   -- Input:
   --        p_NUM_FOL_OPE_ITR -> Foliador del banco
   --        p_FEC_ISR_OPE_ITR -> Fecha de Operación
   --        p_COD_USR         -> Rut usuario
   --        p_GLS_MTV_EST     -> Motivo o glosa de rechazo
   -- Output:
   --        p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- **************************************************************************************************
   PROCEDURE SP_PAB_EST_OPE_ITR (p_NUM_FOL_NMN       IN     NUMBER,
                                 p_FEC_ISR_OPE_ITR   IN     DATE,
                                 p_NUM_FOL_OPE_ITR   IN     NUMBER,
                                 p_COD_USR           IN     VARCHAR2,
                                 p_GLS_MTV_EST       IN     VARCHAR2,
                                 p_FLAG              IN     VARCHAR2,
                                 p_ERROR                OUT NUMBER);


   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_GES_ELI_OPE_ITR
   -- Objetivo: Procedimiento que recibe operaciones internas para eliminar
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: N/A
   -- Fecha:  03-04-2019
   -- Autor: Santander
   -- Input:
   --        p_REG_OPER -> Tipo registro que contiene el numeros y fecha operaciones internas
   --        p_COD_USR -> Rut usuario
   --        p_GLS_EST_ELI -> Motivo o glosa de eliminacion
   -- Output:
   --      p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_GES_ELI_OPE_ITR (p_REG_OPER      IN     REG_OPER,
                                     p_COD_USR       IN     VARCHAR2,
                                     p_GLS_EST_ELI   IN     VARCHAR2,
                                     p_ERROR            OUT NUMBER);

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_GES_EST_OPE_ITR
   -- Objetivo: Procedimiento que recibe operaciones internas para cambiar archivo
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: N/A
   -- Fecha:  09-07-2019
   -- Autor: Santander
   -- Input:
   --        p_REG_OPER -> Tipo registro que contiene el numeros y fecha operaciones internas
   --        p_COD_USR -> Rut usuario
   --        p_GLS_EST -> Motivo o glosa
   -- Output:
   --      p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_GES_EST_OPE_ITR (p_REG_OPER   IN     REG_OPER,
                                     p_COD_USR    IN     VARCHAR2,
                                     p_GLS_EST    IN     VARCHAR2,
                                     p_FLAG       IN     VARCHAR2,
                                     p_ERROR         OUT NUMBER);

   /************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUS_DET_OPE
   -- Objetivo: Procedimiento almacenado que muestra el detalle de una operación interna
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas:  PABS_DT_DETLL_OPRCN_INTER
   --
   -- Fecha: 27-03-2019
   -- Autor: Santander
   -- Input: p_NUM_FOL_OPE_ITR   Folio de la operación interna.
   --        p_FEC_ISR_OPE_ITR: Fecha de inserción de .
   -- Output:
   --        p_CURSOR : SYS_REFCURSOR, resultados de consulta.
   --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_PAB_BUS_DETLL_OPE_INTR (
      p_NUM_FOL_OPE_ITR   IN     NUMBER,
      p_FEC_ISR_OPE_ITR   IN     DATE,
      p_CURSOR               OUT SYS_REFCURSOR,
      p_ERROR                OUT NUMBER
   );

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_OBT_DSC_COD_TIP_PAG
   -- Objetivo: Funcion que devuelve la descripcion del toipo de forma de pago
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_SISTM_ENTRD_SALID
   -- Fecha: 40-04-2019
   -- Autor: Santander CAH
   -- Input:
   --  p_COD_TPO_FPA -> Codigo de tipo pago
   -- Output:
   -- N/A.
   -- Input/Output:
   -- N/A.
   -- Retorno:
   -- v_DSC_TPO_FPA -> Descripcion de Tipo pago
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   FUNCTION FN_PAB_OBT_DSC_COD_TIP_PAG (p_COD_TPO_FPA IN CHAR)
      RETURN VARCHAR2;

   --**********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_BTCOR_OPRCN
   -- Objetivo: Procedimiento que devuelve la bitacora de una operacion
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_BTCOR_OPRCN, PABS_DT_ESTDO_ALMOT
   -- Fecha: 07/12/2016
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_OPE
   -- p_FEC_ISR_OPE
   -- Output:
   -- p_CURSOR_BTCOR
   -- p_ERROR -> Indicador de errores (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_PAB_BTCOR_OPRCN (p_NUM_FOL_OPE    IN     NUMBER,
                                 p_FEC_ISR_OPE    IN     DATE,
                                 p_CURSOR_BTCOR      OUT SYS_REFCURSOR,
                                 p_ERROR             OUT NUMBER);

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_GES_PAG_OPE_ITR
   -- Objetivo: Procedimiento actualiza estado de la operación interna a pagada, segun canal de pago
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: N/A
   -- Fecha: 28-03-2019
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_OPE_ORI    -> Numero de Operación interna
   -- p_FEC_ISR_OPE_ORI    -> Fecha de insercion de operación
   -- p_GLS_ESTADO         -> Glosa de estado
   -- p_TIP_ABO_MAN        -> Codigo de tipo Abono
   -- p_NUM_MVNTO      -> Numero devuelto por WS de Vigencia Salida
   -- p_COD_USR            -> Codigo de Usuario que se registrara en bitacora
   -- Output:
   -- p_ERROR              -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_GES_PAG_OPE_ITR (p_NUM_FOL_OPE_ITR   IN     NUMBER,
                                     p_FEC_ISR_OPE_ITR   IN     DATE,
                                     p_COD_EST_AOS       IN     CHAR,
                                     p_GLS_ESTADO        IN     VARCHAR2,
                                     p_NUM_MVNTO         IN     VARCHAR2,
                                     p_COD_USR           IN     CHAR,
                                     p_ERROR                OUT NUMBER);

   /************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ACT_OPE_ITR
   -- Objetivo: PROCEDIMIENTO ALMACENADO QUE ENTREGA LOS TIPOS
   -- DE OPERACION LIBERADAS, SEGUN FILTROS INGRESADOS
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN_INTER
                     PABS_DT_SISTM_ENTRD_SALID
   -- Fecha: 26-03-2019
   -- Autor: Santander - CAH
   -- Input:
   --    p_NUM_FOL_OPE_ITR     Folio operación
   --    p_FEC_ISR_OPE_ITR     Fecha de inserción
   --    p_COD_EST_AOS         Nuevo estado operación
   --    p_COD_USR             Código usuario
   -- Output:
   --        p_ERROR-> Devuelve error
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_ACT_OPE_ITR (p_NUM_FOL_OPE_ITR   IN     NUMBER,
                                 p_FEC_ISR_OPE_ITR   IN     DATE,
                                 p_COD_EST_AOS       IN     CHAR,
                                 p_COD_USR           IN     VARCHAR2,
                                 p_ERROR                OUT NUMBER);

   --***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_COMBO_CANALES
   -- Objetivo: Procedimiento que retorna los canales internos y para Swift
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_SISTM_ENTRD_SALID
   -- Fecha: 27-03-2019
   -- Autor: Santander CAH
   -- Input:
   --  p_FLG_CNL   Flag para indicar tipo de canal, 0 internos, 1 Swift
   -- Output:
   -- p_CURSOR -> RESULTADOS DE CONSULTA.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_PAB_COMBO_CANALES (p_FLG_CNL   IN     NUMBER,
                                   p_CURSOR       OUT SYS_REFCURSOR);

   --***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_COMBO_TIPO_FRMPG
   -- Objetivo: Procedimiento que retorna los tipos de pagos
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_TIPO_FRMPG
   -- Fecha: 23-03-2019
   -- Autor: Santander CAH
   -- Input:
   --  p_FLG_MON
   -- Output:
   -- p_CURSOR -> RESULTADOS DE CONSULTA.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_PAB_COMBO_TIPO_FRMPG (p_FLG_EXT   IN     NUMBER,
                                      p_CURSOR       OUT SYS_REFCURSOR);

   -- ***********************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_COMBO_FLUJO
   -- Objetivo: Procedimiento que retorna flujo de operación, Egreso - Ingreso
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_SISTM_ENTRD_SALID
   -- Fecha: 04-04-2019
   -- Autor: Santander CAH
   -- Input:
   -- Output:
   -- p_CURSOR   -> Cursor con data de salida
   -- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   --          (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- *********************************************************************************************************
   PROCEDURE Sp_PAB_COMBO_FLUJO (p_CURSOR   OUT SYS_REFCURSOR,
                                 p_ERROR    OUT NUMBER);

   /************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_INS_OPE_ITR
   -- Objetivo: Procedimiento que inserta la informacion que se encuentra en la nomina
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN_INTER
   -- Fecha: 25/03/2019
   -- Autor: Santander CAH
   -- Input:
   --      p_FEC_ISR_OPE_ITR           Fecha de inserción
   --      p_NUM_FOL_OPE_ITR           Folio de la operación,
   --      p_COD_TPO_OPE_AOS_LQD       Código de liquidación,
   --      p_COD_SIS_ENT_ITR           Código de sistema de entrada,
   --      p_COD_SIS_SAL_ITR           Código de sistema de salida,
   --      p_COD_EST_AOS               Código de estado,
   --      p_COD_TPO_ING               Tipo de ingreso, 0 Egreso, 1 Ingreso
   --      p_NUM_FOL_NMN_ITR           Numero de folio nómina interna,
   --      p_FLG_EGR_ING_ITR           Tipo de ingreso, 0 Egreso, 1 Ingreso,
   --      p_NUM_OPE_SIS_ENT_ITR       Numero de operación de sistema origen,
   --      p_COD_DVI_ITR               Código de divisa,
   --      p_IMP_OPE_ITR               Monto de la operación,
   --      p_NUM_DOC_BFC_ITR           Numero de documento beneficiario,
   --      p_COD_TPD_BFC_ITR           Digito verificador beneficiario,
   --      p_NUM_CTA_BFC_ITR           Numero de cta beneficiario,
   --      p_COD_SUC_ITR               Código de sucursal,
   --      p_NUM_REF_CTB_ITR           Numero de referencia contable,
   --      p_NUM_FOL_OPE_ORG_ITR       Numero de operación interna origen,
   --      p_FEC_ISR_OPE_ORG_ITR       Fecha de inserción operación interna origen,
   --      p_NUM_MVT_CTB_ITR           Numero de movimiento contable,
   --      p_FLG_TPO_REG               Flag tipo de registro,
   --      p_GLS_OPC_ITR               Glosa opcional,
   --      p_NOM_BFC_ITR               Nombre beneficiario,
   --      p_GLS_ADC_EST               Gloza adicional de estado operación
   --      p_NOM_ODN_ITR               Nombre ordenate,
   --      p_NUM_DOC_ODN_ITR           Numero de documento ordenante,
   --      p_COD_TPD_ODN_ITR           Digito verificador ordenante,
   --      p_NUM_CTA_ODN_ITR           Numero de cta ordenante,
   --      p_COD_TPO_FPA               Código de forma de pago,
   --      p_FEC_ACT_OPE_ITR           Fecha de actualización de la operación.
   -- Output:
   --          p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_PAB_INS_OPE_ITR (p_FEC_ISR_OPE_ITR       IN     DATE,
                                 p_NUM_FOL_OPE_ITR       IN     NUMBER,
                                 p_COD_TPO_OPE_AOS_LQD   IN     CHAR,
                                 p_COD_SIS_ENT_ITR       IN     CHAR,
                                 p_COD_SIS_SAL_ITR       IN     CHAR,
                                 p_COD_EST_AOS           IN     CHAR,
                                 p_COD_TPO_ING           IN     NUMBER,
                                 p_NUM_FOL_NMN_ITR       IN     NUMBER,
                                 p_FLG_EGR_ING_ITR       IN     NUMBER,
                                 p_NUM_OPE_SIS_ENT_ITR   IN     NUMBER,
                                 p_COD_DVI_ITR           IN     CHAR,
                                 p_IMP_OPE_ITR           IN     NUMBER,
                                 p_NUM_DOC_BFC_ITR       IN     CHAR,
                                 p_COD_TPD_BFC_ITR       IN     CHAR,
                                 p_NUM_CTA_BFC_ITR       IN     VARCHAR2,
                                 p_COD_SUC_ITR           IN     CHAR,
                                 p_NUM_REF_CTB_ITR       IN     CHAR,
                                 p_NUM_FOL_OPE_ORG_ITR   IN     NUMBER,
                                 p_FEC_ISR_OPE_ORG_ITR   IN     DATE,
                                 p_NUM_MVT_CTB_ITR       IN     CHAR,
                                 p_FLG_TPO_REG           IN     NUMBER,
                                 p_GLS_OPC_ITR           IN     VARCHAR2,
                                 p_NOM_BFC_ITR           IN     CHAR,
                                 p_GLS_ADC_EST           IN     VARCHAR2,
                                 p_NOM_ODN_ITR           IN     CHAR,
                                 p_NUM_DOC_ODN_ITR       IN     CHAR,
                                 p_COD_TPD_ODN_ITR       IN     CHAR,
                                 p_NUM_CTA_ODN_ITR       IN     VARCHAR2,
                                 p_COD_TPO_FPA           IN     CHAR,
                                 p_FEC_ACT_OPE_ITR       IN     DATE,
                                 p_ERROR                    OUT NUMBER);

   --***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_COMBO_TIPO_OPE_LQD
   -- Objetivo: Procedimiento que retorna los tipos de operación de liquidación
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_TIPO_OPRCN_ALMOT_LIQDC
   -- Fecha: 23-03-2019
   -- Autor: Santander CAH
   -- Input:
   --
   -- Output:
   -- p_CURSOR -> RESULTADOS DE CONSULTA.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_COMBO_TIPO_OPE_LQD (p_CURSOR OUT SYS_REFCURSOR);
END PKG_PAB_OPERACION_INT;