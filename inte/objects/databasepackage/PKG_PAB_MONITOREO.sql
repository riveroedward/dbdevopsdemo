CREATE OR REPLACE PACKAGE         PKG_PAB_MONITOREO
AS
   -- Package el cual contrendra todas las funciones y procedimientos relacionados con las nominas
   -- //@gcorrea
   /******************************************************************************
      NAME:       PKG_PAB_MONITOREO
      PURPOSE:

      REVISIONS:
      Ver        Date        Author           Description
      ---------  ----------  ---------------  ------------------------------------
      1.0        11-07-2016             1. Created this package.
   ******************************************************************************/
   V_NOM_PCK             VARCHAR2 (30) := 'PKG_PAB_MONITOREO';
   ERR_CODE              NUMBER := 0;
   ERR_MSG               VARCHAR2 (300);
   V_DES                 VARCHAR2 (300);
   V_DES_BIT             VARCHAR2 (100);
   V_BANCO               VARCHAR2 (9) := '970150005';

   TYPE T_NUM_FOL_NMN
   IS
      TABLE OF NUMBER
         INDEX BY PLS_INTEGER;

   p_s_mensaje           VARCHAR2 (400); --BORRA CUANDO SE PASE A PCKS ORIGINAL
   v_dia_ini             DATE := TRUNC (SYSDATE) + INTERVAL '1' MINUTE;
   v_dia_fin DATE
         := TRUNC (SYSDATE) + INTERVAL '23' HOUR + INTERVAL '59' MINUTE ;
   v_dia_hoy             DATE := TRUNC (SYSDATE);

   --Variables para  orden de estados
   v_EST_VAL             NUMBER := 1;
   v_EST_DUP             NUMBER := 2;
   v_EST_INV             NUMBER := 3;
   v_EST_ELI             NUMBER := 4;
   v_EST_CUR             NUMBER := 5;
   v_EST_REC             NUMBER := 6;
   v_EST_AUT             NUMBER := 7;

   --Variables para  orden de estados Ingreso
   v_EST_EGR_OPE_PVI     NUMBER := 1;
   v_EST_EGR_OPEINGAR    NUMBER := 2;
   v_EST_EGR_OPEPDEB     NUMBER := 3;
   v_EST_EGR_OPEPAT      NUMBER := 4;
   v_EST_EGR_OPEAUT      NUMBER := 5;
   v_EST_EGR_OPELIB      NUMBER := 6;
   v_EST_EGR_OPEENV      NUMBER := 7;
   v_EST_EGR_OPEPAG      NUMBER := 8;
   v_EST_EGR_OPEPAGCFI   NUMBER := 9;
   v_EST_EGR_OPEREC      NUMBER := 10;
   v_EST_EGR_OPEPDV      NUMBER := 11;
   v_EST_EGR_OPEDEV      NUMBER := 12;
   v_EST_EGR_OPEELI      NUMBER := 13;

   --    --Variables para  orden de estados
   V_EST_REG             NUMBER := 1;
   v_EST_PAS             NUMBER := 2;
   v_EST_PAB             NUMBER := 3;
   v_EST_ABN             NUMBER := 4;
   v_EST_PDV             NUMBER := 5;
   v_EST_DEV             NUMBER := 6;

   V_IND_NOM             NUMBER := 0;
   V_IND_OPE             NUMBER := 1;

   --***********************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_BUS_ULT_REG_EST
   -- Objetivo: Consulta ultimo registro por estado de nomina
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas: PABS_DT_ESTDO_ALMOT, PABS_DT_CBCRA_NOMNA,PABS_DT_SISTM_ENTRD_SALID
   -- Fecha: 25/10/2016
   -- Autor: gcorrea
   -- Input:
   -- P_COD_EST_AOS -> CODIGO ESTADO
   -- P_COD_SIS_ENT -> CODIGO SISTEMA
   -- P_IND_NOM_OPE -> INDICA SI ES NOMINA 0 OPERACION 1
   -- N/A
   -- Output:
   -- N/A.
   -- Input/Output:
   -- N/A.
   -- Retorno:
   -- V_ULTIMO_REGISTRO -> valor de retorno que muestra la ultima fecha y horario por estado de nomina
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   --***********************************************************************************************
   FUNCTION FN_PAB_BUS_ULT_REG_EST (P_COD_EST_AOS   IN NUMBER,
                                    P_COD_SIS_ENT   IN CHAR,
                                    P_IND_NOM_OPE      NUMBER)
      RETURN VARCHAR2;

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_CANT_NMN_EST
   -- Objetivo: Procedimiento almacenado que cuenta por el tipo de estado cada una de las nominas
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA
   -- Fecha: 24/10/16
   -- Autor: gcorrea
   -- Input:
   -- P_COD_SIS_ENT -> codigo de sistema entrada
   -- P_COD_EST_AOS -> codigo estado
   -- Output:
   -- V_CANT_NMN -> CANTIDAD DE NOMINAS POR ESTADO
   -- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_CANT_NMN_EST (P_COD_EST_AOS   IN     NUMBER,
                                  P_COD_SIS_ENT   IN     CHAR,
                                  p_CURSOR           OUT SYS_REFCURSOR,
                                  P_ERROR            OUT NUMBER);

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_EST_NMN_DSH
   -- Objetivo: Procedimiento almacenado que obtine los estados de las nominas para el dashboard segun el rol
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA
   -- Fecha: 21/06/16
   -- Autor: gcorrea
   -- Input:
   -- p_COD_TROL -> tipo de rol
   -- p_COD_SIS_ENT -> canal entrada salida
   -- Output:
   -- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- p_CURSOR-> cursor de salida de resultado
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_EST_NMN_DSH (p_COD_TROL      IN     NUMBER,
                                 p_COD_SIS_ENT   IN     CHAR,
                                 p_CURSOR           OUT SYS_REFCURSOR,
                                 p_ERROR            OUT NUMBER);

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_EST_OPE_DSH
   -- Objetivo: Procedimiento almacenado que obtine los estados de las operaciones para el dashboard segun el rol
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA
   -- Fecha: 21/06/16
   -- Autor: gcorrea
   -- Input:
   -- p_COD_TROL -> Tipo de rol
   -- p_COD_SIS_ENT -> Codigo de sistema entrada
   -- Output:
   -- p_CURSOR-> Cursor de salida
   -- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de Ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_EST_OPE_DSH (p_COD_TROL      IN     NUMBER,
                                 p_COD_SIS_ENT   IN     CHAR,
                                 p_CURSOR           OUT SYS_REFCURSOR,
                                 p_ERROR            OUT NUMBER);

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_CANT_OPE_EST
   -- Objetivo: Procedimiento almacenado que cuenta por el tipo de estado cada una de las operaciones
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN, PABS_DT_ESTDO_ALMOT, PABS_DT_SISTM_ENTRD_SALID
   -- Fecha: 24/10/16
   -- Autor: gcorrea
   -- Input:
   -- P_COD_EST_AOS -> codigo de estado de operacion
   -- P_COD_SIS_ENT -> codigo de sistema entrada
   -- Output:
   -- p_CURSOR -> cursor de resultado
   -- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_CANT_OPE_EST (P_COD_EST_AOS   IN     NUMBER,
                                  P_COD_SIS_ENT   IN     CHAR,
                                  p_CURSOR           OUT SYS_REFCURSOR,
                                  P_ERROR            OUT NUMBER);

   --***********************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_BUS_HOR_INICIO
   -- Objetivo: Consulta el horario del corte horario - INICIO
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas: PABS_DT_SISTM_ENTRD_SALID
   -- Fecha: 09/11/2016
   -- Autor: gcorrea
   -- Input:
   -- P_COD_SIS_SAL -> CODIGO SISTEMA ENTRADA SALIDA (AREA)
   -- N/A
   -- Output:
   -- N/A.
   -- Input/Output:
   -- N/A.
   -- Retorno:
   -- V_HORA_INICIO -> valor de retorno que muestra el horario de inicio segun el area
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   --***********************************************************************************************
   FUNCTION FN_PAB_BUS_HOR_INICIO (P_COD_SIS_SAL IN CHAR)
      RETURN VARCHAR2;

   --***********************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_BUS_HOR_CIERRE
   -- Objetivo: Consulta el horario del corte horario - CIERRE
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas: PABS_DT_SISTM_ENTRD_SALID
   -- Fecha: 09/11/2016
   -- Autor: gcorrea
   -- Input:
   -- P_COD_SIS_SAL -> CODIGO SISTEMA ENTRADA SALIDA (AREA)
   -- N/A
   -- Output:
   -- N/A.
   -- Input/Output:
   -- N/A.
   -- Retorno:
   -- V_HORA_CIERRE -> valor de retorno que muestra el horario de CIERRE segun el area
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   --***********************************************************************************************
   FUNCTION FN_PAB_BUS_HOR_CIERRE (P_COD_SIS_SAL IN CHAR)
      RETURN VARCHAR2;

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUS_CRTE_HOR
   -- Objetivo: Procedimiento almacenado que obtiene el horario de inicio y cierre por area
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_SISTM_ENTRD_SALID
   -- Fecha: 09/11/16
   -- Autor: gcorrea
   -- Input:
   -- P_COD_SIS_ENT -> codigo de sistema entrada
   -- Output:
   -- p_CURSOR -> Cursor de salida
   -- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_BUS_CRTE_HOR (P_COD_SIS_ENT   IN     CHAR,
                                  p_CURSOR           OUT SYS_REFCURSOR,
                                  P_ERROR            OUT NUMBER);

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_RISG_COLR
   -- Objetivo: Funcion que retorna el color segun horario en el que se encuentra.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA
   -- Fecha: 21/06/16
   -- Autor: gcorrea
   -- Input:
   -- p_COD_SIS_ENT -> Codigo de sistema entrada
   -- P_COD_EST     -> Codigo estado nomina
   -- P_IND_NOM_OPE -> INDICA SI ES NOMINA 0 OPERACION 1
   -- Output:
   -- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: V_COLOR -> color segun el horario
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   -- ***********************************************************************************************
   FUNCTION FN_PAB_RISG_COLR (P_COD_SIS_ENT   IN CHAR,
                              P_COD_EST       IN NUMBER,
                              P_IND_NOM_OPE   IN NUMBER)
      RETURN VARCHAR2;

   --***********************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_BUS_ULT_REG_OPE_EST
   -- Objetivo: Consulta ultimo registro por estado de operaciones
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas: PABS_DT_ESTDO_ALMOT, PABS_DT_DETLL_OPRCN,PABS_DT_SISTM_ENTRD_SALID
   -- Fecha: 25/10/2016
   -- Autor: gcorrea
   -- Input:
   -- P_COD_EST_AOS -> CODIGO ESTADO NOMINA
   -- N/A
   -- Output:
   -- N/A.
   -- Input/Output:
   -- N/A.
   -- Retorno:
   -- V_ULTIMO_REGISTRO -> valor de retorno que muestra la ultima fecha y horario por estado de nomina
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   --***********************************************************************************************
   --FUNCTION FN_PAB_BUS_ULT_REG_OPE_EST ( P_COD_EST_AOS IN NUMBER ) RETURN VARCHAR2;

   --***********************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_BUS_ULT_REG_OPE_EST_EGR
   -- Objetivo: Consulta ultimo registro por estado de operaciones
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas: PABS_DT_ESTDO_ALMOT, PABS_DT_DETLL_OPRCN,PABS_DT_SISTM_ENTRD_SALID
   -- Fecha: 27-12-2017
   -- Autor: gcorrea
   -- Input:
   -- p_COD_SIS_ENT -> Codigo de sistema entrada
   -- P_COD_EST_AOS -> CODIGO ESTADO NOMINA
   -- P_FLG_EGR_ING -> Flag Ingreso o egreso de la operacion
   -- N/A
   -- Output:
   -- N/A.
   -- Input/Output:
   -- N/A.
   -- Retorno:
   -- V_ULTIMO_REGISTRO -> valor de retorno que muestra la ultima fecha y horario por estado de nomina
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   --***********************************************************************************************
   FUNCTION FN_PAB_BUS_ULT_REG_OPE_EST (P_COD_SIS_ENT   IN CHAR,
                                        P_COD_EST_AOS   IN NUMBER,
                                        P_FLG_EGR_ING   IN NUMBER)
      RETURN VARCHAR2;

   /************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUS_HIT_OFF
   -- Objetivo: Procedimiento almacenado que retorna lista de hitos para OOFF
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_SISTM_ENTRD_SALID
   --
   -- Fecha: 04/07/17
   -- Autor: gcorrea CAH
   -- Input: p_COD_SIS_ENT      Area de trabajo, OOFF, CANJE, Etc.
   -- Output:
   --        p_CURSOR : SYS_REFCURSOR, resultados de consulta.
   --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_BUS_HIT_TBJ (p_COD_SIS_ENT   IN     CHAR,
                                 p_CURSOR           OUT SYS_REFCURSOR,
                                 p_ERROR            OUT NUMBER);

   -- ***************************************************************************************************************
   -- FUNCION/PROCEDIMIENTO: SP_PAB_MNI_EGR_ENT
   -- OBJETIVO: SP MUESTRA ACTUALIZACIÓN EN PANTALLA MONITOREO PARA EGRESOS EN MONEDA NACIONAL Y EXTRANJERA (ENTRADA).
   -- SISTEMA: DBO_PAB.
   -- BASE DE DATOS: DBO_PAB.
   -- TABLAS USADAS: PABS_DT_SISTM_ENTRD_SALID
   -- FECHA: 09-08-2017
   -- AUTOR: GCORREA- GSD
   -- INPUT:
   -- P_FLAG_DVI -> INDICADOR DE DIVISA  (0 MN, 1 MX)
   -- OUTPUT:
   -- P_CURSOR -> CURSOR DE RESULTADO
   -- P_ERROR-> INDICADOR DE ERRORES: 0 SIN ERRORES 1 EXISTEN ERRORES.
   -- OBSERVACIONES: <FECHA Y DETALLE DE ULTIMOS CAMBIOS>
   -- *****************************************************************************************************************
   PROCEDURE Sp_PAB_MNI_EGR_ENT (p_FLAG_DVI   IN     NUMBER,
                                 p_CURSOR        OUT SYS_REFCURSOR,
                                 p_ERROR         OUT NUMBER);

   -- *****************************************************************************************************************
   -- FUNCION/PROCEDIMIENTO: SP_PAB_MNI_EGR_SAL
   -- OBJETIVO: SP MUESTRA ACTUALIZACIÓN EN PANTALLA MONITOREO PARA EGRESOS EN MONEDA NACIONAL Y EXTRANJERA (SALIDA).
   -- SISTEMA: DBO_PAB.
   -- BASE DE DATOS: DBO_PAB.
   -- TABLAS USADAS: PABS_DT_SISTM_ENTRD_SALID
   -- FECHA: 21-07-2017
   -- AUTOR: GCORREA- CAH
   -- INPUT:
   -- P_FLAG_DVI -> INDICADOR DE DIVISA
   -- OUTPUT:
   -- P_CURSOR -> CURSOR DE RESULTADO
   -- P_ERROR -> INDICADOR DE ERRORES: 0 SIN ERRORES 1 EXISTEN ERRORES.
   -- OBSERVACIONES: <FECHA Y DETALLE DE ULTIMOS CAMBIOS>
   -- *****************************************************************************************************************
   PROCEDURE Sp_PAB_MNI_EGR_SAL (p_FLAG_DVI   IN     NUMBER,
                                 p_CURSOR        OUT SYS_REFCURSOR,
                                 P_ERROR         OUT NUMBER);

   -- ****************************************************************************************************************
   -- FUNCION/PROCEDIMIENTO: SP_PAB_MNI_ING_SAL
   -- OBJETIVO: SP MUESTRA ACTUALIZACIÓN EN PANTALLA MONITOREO INGRESO DE MONEDA NACIONAL (ENTRADA Y SALIDA)
   -- SISTEMA: DBO_PAB
   -- BASE DE DATOS: DBO_PAB
   -- TABLAS USADAS: PABS_DT_SISTM_ENTRD_SALID
   -- FECHA: 21-07-2017
   -- AUTOR: GCORREA
   -- INPUT:
   -- P_FLAG_ENT_SAL -> INDICADOR DE FLUJO
   -- OUTPUT:
   -- P_CURSOR -> CURSOR DE RESULTADO
   -- P_ERROR-> INDICADOR DE ERRORES 0 SIN ERRORES 1 EXISTEN ERRORES.
   -- OBSERVACIONES: <FECHA Y DETALLE DE ULTIMOS CAMBIOS>
   -- ***************************************************************************************************************
   PROCEDURE Sp_PAB_MNI_ING_SAL (p_FLAG_ENT_SAL   IN     NUMBER,
                                 p_CURSOR            OUT SYS_REFCURSOR,
                                 P_ERROR             OUT NUMBER);

   --***************************************************************************************************************
   -- FUNCION/PROCEDIMIENTO: FN_PAB_BUS_CAN_OPE_LIB
   -- OBJETIVO: RETORNA CANTIDAD DE OPERACIONES POR SER LIBERADAS
   -- SISTEMA: DBO_PAB
   -- BASE DE DATOS: DBO_PAB
   -- TABLAS USADAS: PABS_DT_DETLL_OPRCN
   -- FECHA:21/07/2017
   -- AUTOR: GCORREA
   -- INPUT:
   -- P_COD_SIS_ENT_SAL -> CODIGO SISTEMA ENTRADA SALIDA (CANAL)
   -- P_COD_DVI -> INDICADOS DE MONEDA NACIONAL O EXTRANJERA (0 NACIONAL, 1 EXTRANJERA)
   -- OBSERVACIONES: <FECHA Y DETALLE>
   --***************************************************************************************************************
   FUNCTION FN_PAB_BUS_CAN_OPE_LIB (P_COD_SIS_ENT_SAL   IN CHAR,
                                    P_FLG_SIS_ENT_SAL      NUMBER,
                                    P_COD_DVI              CHAR)
      RETURN NUMBER;

   --***********************************************************************************************
   -- FUNCION/PROCEDIMIENTO: FN_PAB_BUS_CAN_OPE_PAG
   -- OBJETIVO: RETORNA CANTIDAD DE OPERACIONES PAGADAS
   -- SISTEMA: DBO_PAB
   -- BASE DE DATOS: DBO_PAB
   -- TABLAS USADAS: PABS_DT_DETLL_CARGO_ABONO_CAJA
   -- FECHA:21/07/2017
   -- AUTOR: GCORREA
   -- INPUT:
   -- P_COD_SIS_ENT_SAL -> CÓDIGO CANAL
   -- P_FLG_SIS_ENT_SAL -> FLAG CANAL DE SALIDA O ENTRADA
   -- P_COD_DVI -> DIVISA
   -- OBSERVACIONES: <FECHA Y DETALLE DE ?LTIMOS CAMBIOS>
   --***********************************************************************************************
   FUNCTION FN_PAB_BUS_CAN_OPE_PAG (P_COD_SIS_ENT_SAL   IN CHAR,
                                    P_FLG_SIS_ENT_SAL      NUMBER,
                                    P_COD_DVI              NUMBER)
      RETURN NUMBER;

   --*****************************************************************************************************
   -- FUNCION/PROCEDIMIENTO: FN_PAB_BUS_CAN_OPE_TOT
   -- OBJETIVO: FN RETORNA CANTIDAD TOTAL DE OPERACIONES POR LIBERAR + NO PAGADAS + PAGADAS + ENVIADAS
   -- SISTEMA: PAB
   -- BASE DE DATOS: DBO_PAB
   -- TABLAS USADAS: PABS_DT_DETLL_OPRCN - PABS_DT_DETLL_CARGO_ABONO_CAJA
   -- FECHA:21/07/2017
   -- AUTOR: GCORREA-CAH
   -- INPUT:
   -- P_COD_SIS_ENT_SAL -> CÓDIGO CANAL
   -- P_FLG_SIS_ENT_SAL -> FLAG CANAL
   -- P_COD_DVI -> DIVISA
   -- OBSERVACIONES: <FECHA Y DETALLE>
   --*****************************************************************************************************
   FUNCTION FN_PAB_BUS_CAN_OPE_TOT (P_COD_SIS_ENT_SAL   IN CHAR,
                                    P_FLG_SIS_ENT_SAL      NUMBER,
                                    P_COD_DVI              NUMBER)
      RETURN NUMBER;

   --*****************************************************************************************************
   -- FUNCION/PROCEDIMIENTO: FN_PAB_BUS_CAN_OPE_NPAG
   -- OBJETIVO: RETORNA CANTIDAD DE OPERACIONES NO PAGADAS
   -- SISTEMA: DBO_PAB
   -- BASE DE DATOS: DBO_PAB
   -- TABLAS USADAS: PABS_DT_DETLL_OPRCN
   -- FECHA:21/07/2017
   -- AUTOR: GCORREA
   -- INPUT:
   -- P_COD_SIS_ENT_SAL -> CÓDIGO CANAL
   -- P_FLG_SIS_ENT_SAL -> FLAG DE CANAL DE SALIDA O ENTRADA
   -- P_COD_DVI -> DIVISA
   -- OBSERVACIONES: <FECHA Y DETALLE >
   --****************************************************************************************************
   FUNCTION FN_PAB_BUS_CAN_OPE_NPAG (P_COD_SIS_ENT_SAL   IN CHAR,
                                     P_FLG_SIS_ENT_SAL      NUMBER,
                                     P_COD_DVI              NUMBER)
      RETURN NUMBER;

   --***********************************************************************************************
   -- FUNCION/PROCEDIMIENTO: FN_PAB_BUS_CANT_OPE_ABO
   -- OBJETIVO: RETORNA CANTIDA DE OPERACIONES ABONADAS
   -- SISTEMA: DBO_PAB
   -- BASE DE DATOS: DBO_PAB
   -- TABLAS USADAS: PABS_DT_DETLL_OPRCN - PABS_DT_DETLL_CARGO_ABONO_CAJA
   -- FECHA:21/07/2017
   -- AUTOR: GCORREA
   -- INPUT:
   -- P_COD_SIS_ENT_SAL -> CÓDIGO CANAL
   -- P_FLG_SIS_ENT_SAL -> FLAG DE CANAL DE SALIDA O ENTRADA
   -- OBSERVACIONES: <FECHA Y DETALLE>
   --***********************************************************************************************
   FUNCTION FN_PAB_BUS_CANT_OPE_ABO (P_COD_SIS_ENT_SAL   IN CHAR,
                                     p_FLAG_ENT_SAL      IN NUMBER)
      RETURN NUMBER;

   -- --***********************************************************************************************
   ---- FUNCION/PROCEDIMIENTO: FN_PAB_BUS_CANT_OPE_NAB
   ---- OBJETIVO: RETORNA CANTIDAD DE OPERACIONES POR SER LIBERADAS
   ---- SISTEMA: DBO_PAB
   ---- BASE DE DATOS: DBO_PAB
   ---- TABLAS USADAS: PABS_DT_DETLL_OPRCN
   ---- FECHA:21/07/2017
   ---- AUTOR: GCORREA
   ---- INPUT:
   ---- P_COD_SIS_ENT_SAL -> CANAL
   ---- P_FLG_SIS_ENT_SAL -> INIDCADO DE CANAL DE SALIDA O ENTRADA
   ---- OBSERVACIONES: <FECHA Y DETALLE>
   ----***********************************************************************************************
   FUNCTION FN_PAB_BUS_CANT_OPE_NAB (P_COD_SIS_ENT_SAL   IN CHAR,
                                     p_FLAG_ENT_SAL      IN NUMBER)
      RETURN NUMBER;

   --***********************************************************************************************
   -- FUNCION/PROCEDIMIENTO: FN_PAB_BUS_CAN_OPE_TOT_SAL
   -- OBJETIVO: RETORNA CANTIDAD TOTAL DE OPERACIONES LIBERAR + NO PAGADAS + PAGADAS + ENVIADAS
   -- SISTEMA: DBO_PAB
   -- BASE DE DATOS: DBO_PAB
   -- TABLAS USADAS: PABS_DT_DETLL_OPRCN, PABS_DT_DETLL_CARGO_ABONO_CAJA
   -- FECHA:21/07/2017
   -- AUTOR: GCORREA-CAH
   -- INPUT:
   -- P_COD_SIS_ENT_SAL -> CÓDIGO CANAL
   -- P_FLG_SIS_ENT_SAL -> FLAGIDCADO DE CANAL DE SALIDA O ENTRADA
   -- OBSERVACIONES: <FECHA Y DETALLE>
   --***********************************************************************************************
   FUNCTION FN_PAB_BUS_CAN_OPE_TOT_SAL (P_COD_SIS_ENT_SAL   IN CHAR,
                                        P_FLG_SIS_ENT_SAL      NUMBER,
                                        P_COD_DVI              NUMBER)
      RETURN NUMBER;

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_EST_OPE_DSH_EGR
   -- Objetivo: Procedimiento almacenado que obtine los estados de las operaciones para el dashboard segun el rol
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA
   -- Fecha: 21/06/16
   -- Autor: gcorrea
   -- Input:
   -- p_COD_TROL -> Tipo de rol
   -- p_COD_SIS_ENT -> Codigo de sistema entrada
   -- Output:
   -- p_CURSOR-> Cursor de salida
   -- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de Ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_EST_OPE_DSH_EGR (p_COD_TROL      IN     NUMBER,
                                     p_COD_SIS_ENT   IN     CHAR,
                                     p_CURSOR           OUT SYS_REFCURSOR,
                                     p_ERROR            OUT NUMBER);

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_EST_OPE_DSH_ING
   -- Objetivo: Procedimiento almacenado que obtine los estados de las operaciones para el dashboard segun el rol
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA
   -- Fecha: 21/06/16
   -- Autor: gcorrea
   -- Input:
   -- p_COD_TROL -> Tipo de rol
   -- p_COD_SIS_ENT -> Codigo de sistema entrada
   -- Output:
   -- p_CURSOR-> Cursor de salida
   -- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de Ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_EST_OPE_DSH_ING (p_COD_TROL      IN     NUMBER,
                                     p_COD_SIS_ENT   IN     CHAR,
                                     p_CURSOR           OUT SYS_REFCURSOR,
                                     p_ERROR            OUT NUMBER);

   -- ****************************************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_EST_NMN_DSH_EGR
   -- Objetivo: Procedimiento almacenado que obtiene los estados para operaciones en respecto a nóminas
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA
   -- Fecha: 06-11-2017
   -- Autor: gcorrea- CAH
   -- Input:
   -- p_COD_TROL -> Tipo de rol
   -- p_COD_SIS_ENT -> Codigo de sistema entrada
   -- Output:
   -- p_CURSOR-> Cursor de salida
   -- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de Ultimos cambios>
   -- ******************************************************************************************************************************
   PROCEDURE Sp_PAB_EST_NMN_DSH_EGR (p_COD_TROL      IN     NUMBER,
                                     p_COD_SIS_ENT   IN     CHAR,
                                     p_CURSOR           OUT SYS_REFCURSOR,
                                     p_ERROR            OUT NUMBER);

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_CANT_OPE_EST
   -- Objetivo: Procedimiento almacenado que cuenta por el tipo de estado cada una de las operaciones
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN, PABS_DT_ESTDO_ALMOT, PABS_DT_SISTM_ENTRD_SALID
   -- Fecha: 24/10/16
   -- Autor: gcorrea
   -- Input:
   -- P_COD_EST_AOS -> codigo de estado de operacion
   -- P_COD_SIS_ENT -> codigo de sistema entrada
   -- Output:
   -- p_CURSOR -> cursor de resultado
   -- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_CANT_OPE_EST_EGR (P_COD_EST_AOS   IN     NUMBER,
                                      P_COD_SIS_ENT   IN     CHAR,
                                      p_CURSOR           OUT SYS_REFCURSOR,
                                      P_ERROR            OUT NUMBER);

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_CANT_OPE_EST
   -- Objetivo: Procedimiento almacenado que cuenta por el tipo de estado cada una de las operaciones
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN, PABS_DT_ESTDO_ALMOT, PABS_DT_SISTM_ENTRD_SALID
   -- Fecha: 24/10/16
   -- Autor: gcorrea
   -- Input:
   -- P_COD_EST_AOS -> codigo de estado de operacion
   -- P_COD_SIS_ENT -> codigo de sistema entrada
   -- Output:
   -- p_CURSOR -> cursor de resultado
   -- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_CANT_OPE_EST_ING (P_COD_EST_AOS   IN     NUMBER,
                                      P_COD_SIS_ENT   IN     CHAR,
                                      p_CURSOR           OUT SYS_REFCURSOR,
                                      P_ERROR            OUT NUMBER);

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_CANT_OPE_EST
   -- Objetivo: Procedimiento almacenado que cuenta por el tipo de estado cada una de las operaciones
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN, PABS_DT_ESTDO_ALMOT, PABS_DT_SISTM_ENTRD_SALID
   -- Fecha: 06/11/2017
   -- Autor: gcorrea- CAH
   -- Input:
   -- P_COD_EST_AOS -> codigo de estado de operacion
   -- P_COD_SIS_ENT -> codigo de sistema entrada
   -- Output:
   -- p_CURSOR -> cursor de resultado
   -- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_CANT_NMN_EST_EGR (P_COD_EST_AOS   IN     NUMBER,
                                      P_COD_SIS_ENT   IN     CHAR,
                                      p_CURSOR           OUT SYS_REFCURSOR,
                                      P_ERROR            OUT NUMBER);

   -- --***********************************************************************************************
   ---- FUNCION/PROCEDIMIENTO: Sp_PAB_MNX_ING_SAL
   ---- OBJETIVO: SP MUESTRA ACTUALIZACIÓN EN PANTALLA MONITOREO PARA INGRESO EN MONEDA EXTRANJERA
   ---- SISTEMA: DBO_PAB
   ---- BASE DE DATOS: DBO_PAB
   ---- TABLAS USADAS: PABS_DT_SISTM_ENTRD_SALID
   ---- FECHA:
   ---- AUTOR: NEORIS
   ---- INPUT:
   ---- p_FLAG_ENT_SAL -> FLAG ENTRADA 0 SALIDA 1
   ---- OUTPUT:
   ---- p_CURSOR -> RETORNO DATOS
   ---- P_ERROR  -> MANEJO DE ERRORES.
   ---- OBSERVACIONES: <FECHA Y DETALLE DE ?LTIMOS CAMP_FLG_SIS_ENT_SALBIOS>
   ----***********************************************************************************************
   PROCEDURE Sp_PAB_MNX_ING_SAL (p_FLAG_ENT_SAL   IN     NUMBER,
                                 p_CURSOR            OUT SYS_REFCURSOR,
                                 P_ERROR             OUT NUMBER);

   -- --**********************************************************************************************
   ---- FUNCION/PROCEDIMIENTO: FN_PAB_BUS_CANT_OPE_ABONO
   ---- OBJETIVO: FN MUESTRA LOS MT910 PARA MONEDA EXTRANJERA DEL DÍA.
   ---- SISTEMA: DBO_PAB
   ---- BASE DE DATOS: DBO_PAB
   ---- TABLAS USADAS: PABS_DT_DETLL_CARGO_ABONO_CAJA
   ---- FECHA:
   ---- AUTOR: NEORIS
   ---- INPUT:
   ---- P_COD_SIS_ENT_SAL -> CÓDIGO CANAL
   ---- P_FLG_SIS_ENT_SAL -> FLAG ENTRADA O SALIDA
   ---- OUTPUT:
   ---- N/A
   ---- OBSERVACIONES: <FECHA Y DETALLE>
   ---- VAR 22.10.2018 SE MODIFICA FUNCIÓN.
   ----***********************************************************************************************
   FUNCTION FN_PAB_BUS_CANT_OPE_ABONO (P_COD_SIS_ENT_SAL   IN CHAR,
                                       P_FLG_SIS_ENT_SAL   IN CHAR)
      RETURN NUMBER;
END PKG_PAB_MONITOREO;