CREATE OR REPLACE PACKAGE         PKG_PAB_CAJA_MX
IS
   --
   v_NOM_PCK             VARCHAR2 (30) := 'PKG_PAB_CAJA_MX';
   ERR_CODE              NUMBER := 0;
   ERR_MSG               VARCHAR2 (300);
   v_DES                 VARCHAR2 (300);
   v_DES_BIT             VARCHAR2 (100);
   p_s_mensaje           VARCHAR2 (400);


   v_dia_ini             DATE := TRUNC (SYSDATE) + INTERVAL '1' MINUTE;
   v_dia_fin DATE
         := TRUNC (SYSDATE) + INTERVAL '23' HOUR + INTERVAL '59' MINUTE ;


   CURSOR C_CONSULTA_CTAS_CTL (
      P_DIV                 CHAR
   )
   IS
        SELECT   MAX (FEC_ING_CTL) AS FEC_ULT_CAR_REC,
                 COD_BCO_ORG AS BANCO,
                 NUM_CTA_CTE AS CUENTA
          FROM   PABS_DT_CBCRA_CRTLA
         WHERE   COD_DVI = P_DIV AND FEC_ING_CTL < TRUNC (SYSDATE)
      GROUP BY   COD_BCO_ORG, NUM_CTA_CTE;

   R_CONSULTA_CTAS_CTL   C_CONSULTA_CTAS_CTL%ROWTYPE;

   /************************************************************************************************
   -- Funcion/Procedimiento: SP_BUS_SALDO_CIERRE_MX
   -- Objetivo: Procedimiento almacenado que Muestra los datos de saldos del cierre MX
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX
   --
   -- Fecha: 31-10-2017
   -- Autor: Santander
   -- Input:
   --          p_MONEDA ->     Moneda a consultar.
   --
   -- Output:
   --          p_CURSOR ->     SYS_REFCURSOR, resultados de consulta.
   --          p_ERROR: ->     Indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_BUS_SALDO_CIERRE_MX (p_MONEDA   IN     CHAR,
                                     p_CURSOR      OUT SYS_REFCURSOR,
                                     p_ERROR       OUT NUMBER);


   /************************************************************************************************
   -- Funcion/Procedimiento: SP_BUS_SALDO_INTRADIA_MX
   -- Objetivo: Procedimiento almacenado que Muestra los datos de saldo intradía
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX
   --
   -- Fecha: 31-10-2017
   -- Autor: Santander
   -- Input:
   --          p_MONEDA  ->    Moneda a consultar.
   --
   -- Output:
   --          p_CURSOR ->     SYS_REFCURSOR, resultados de consulta.
   --          p_ERROR  ->     Indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_BUS_SALDO_INTRADIA_MX (p_MONEDA   IN     CHAR,
                                       p_CURSOR      OUT SYS_REFCURSOR,
                                       p_ERROR       OUT NUMBER);

   /************************************************************************************************
   -- Funcion/Procedimiento: SP_BUS_SALDO_24HORAS_MX
   -- Objetivo: Procedimiento almacenado que Muestra los datos de saldo a 24 horas
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX, TCDTBAI
   --
   -- Fecha: 31-10-2017
   -- Autor: Santander
   -- Input:
   --          p_MONEDA  ->    Moneda a consultar.
   --
   -- Output:
   --          p_CURSOR  ->    SYS_REFCURSOR, resultados de consulta.
   --          p_ERROR:  ->    Indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_BUS_SALDO_24HORAS_MX (p_MONEDA   IN     CHAR,
                                      p_CURSOR      OUT SYS_REFCURSOR,
                                      p_ERROR       OUT NUMBER);

   /************************************************************************************************
   -- Funcion/Procedimiento: SP_BUS_SALDO_48HORAS_MX
   -- Objetivo: Procedimiento almacenado que Muestra los datos de saldo a 48 horas
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX, TCDTBAI
   --
   -- Fecha: 31-10-2017
   -- Autor: Santander
   -- Input:
   --          p_MONEDA  ->    Moneda a consultar.
   --
   -- Output:
   --          p_CURSOR  ->    SYS_REFCURSOR, resultados de consulta.
   --          p_ERROR:  ->    Indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_BUS_SALDO_48HORAS_MX (p_MONEDA   IN     CHAR,
                                      p_CURSOR      OUT SYS_REFCURSOR,
                                      p_ERROR       OUT NUMBER);


   /************************************************************************************************
   -- Funcion/Procedimiento: SP_BUS_CRTLA_CTCTE_MX
   -- Objetivo: Procedimiento almacenado que Muestra la cabecera y detalle de la Cartola MX
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_CBCRA_CRTLA , PABS_DT_CENTA_BANCO_CRPSL , PABS_DT_DETLL_CRTLA, PABS_DT_DETLL_SALDO_CAJA_MN_MX, PABS_DT_DETLL_MVNTO_CAJA
   -- Fecha: 02-11-2017
   -- Autor: Santander
   -- Input:
   --          p_FOLIO   ->     Moneda a consultar.
   --          p_BANCO   ->     Bic Banco
   --          p_CTA_CTE ->     Cuenta Corriente
   --          p_COD_DET ->     Código Detalle
   --          p_COD_DVI ->     Divisa a buscar-
   --
   -- Output:
   --          p_CABECERA  ->    SYS_REFCURSOR, datos de cabecera.
   --          p_DETALLE   ->    SYS_REFCURSOR, datos del detalleresultados de consulta.
   --          p_ERROR:      ->    Indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_BUS_CRTLA_CTCTE_MX (p_FOLIO      IN     NUMBER,
                                    p_BANCO      IN     CHAR,
                                    p_CTA_CTE    IN     VARCHAR2,
                                    p_FEC_CTL    IN     DATE,
                                    p_COD_DET    IN     CHAR,
                                    p_COD_DVI    IN     CHAR,
                                    p_CABECERA      OUT SYS_REFCURSOR,
                                    p_DETALLE       OUT SYS_REFCURSOR,
                                    p_ERROR         OUT NUMBER);

   /************************************************************************************************
   -- Funcion/Procedimiento: SP_BUS_BANCO_MX
   -- Objetivo: Procedimiento almacenado que Busca los bancos corresponsales
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_CENTA_BANCO_CRPSL
   --
   -- Fecha: 02-11-2017
   -- Autor: Santander
   -- Input:  N/A
   -- Output:
   --        p_CURSOR : SYS_REFCURSOR, resultados de consulta.
   --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_BUS_BANCO_MX (p_CURSOR OUT SYS_REFCURSOR, p_ERROR OUT NUMBER);

   /************************************************************************************************
   -- Funcion/Procedimiento: SP_BUS_SALDO_BANCO_MX
   -- Objetivo: Procedimiento almacenado que Busca los movimientos por banco selecccionado.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX
   --
   -- Fecha: 02-11-2017
   -- Autor: Santander
   -- Input:
   --   p_BANCO  --> Bic Banco a consultar.
   --
   -- Output:
   --        p_CURSOR : SYS_REFCURSOR, resultados de consulta.
   --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_BUS_SALDO_BANCO_MX (p_BANCO    IN     CHAR,
                                    p_CURSOR      OUT SYS_REFCURSOR,
                                    p_ERROR       OUT NUMBER);


   /************************************************************************************************
   -- Funcion/Procedimiento: SP_BUS_MONDA_ALMOT
   -- Objetivo: Procedimiento almacenado que Muestra todas las monedas de altos montos
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_MONDA_ALMOT
   --
   -- Fecha: 07-11-2017
   -- Autor: Santander
   -- Input: N/A
   --
   -- Output:
   --        p_CURSOR : SYS_REFCURSOR, resultados de consulta.
   --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_BUS_MONDA_ALMOT (p_CURSOR   OUT SYS_REFCURSOR,
                                 p_ERROR    OUT NUMBER);


   /************************************************************************************************
   -- Funcion/Procedimiento: SP_BUS_ENC_SALDOS_MX
   -- Objetivo: Procedimiento almacenado que Muestra encabezado de saldos MX
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX
   --
   -- Fecha: 14-11-2017
   -- Autor: Santander
   -- Input:
   --          p_MONEDA  ->    Moneda a consultar.
   -- Output:
   --          p_CURSOR -> SYS_REFCURSOR, resultados de consulta.
   --          p_ERROR  -> Indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_BUS_ENC_SALDOS_MX (p_MONEDA   IN     CHAR,
                                   p_CURSOR      OUT SYS_REFCURSOR,
                                   p_ERROR       OUT NUMBER);



   /************************************************************************************************
   -- Funcion/Procedimiento: SP_BUS_ENC_SALDOS_INTRA_MX
   -- Objetivo: Procedimiento almacenado que Muestra encabezado de saldos intradía MX
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX
   --
   -- Fecha: 14-11-2017
   -- Autor: Santander
   -- Input:
   --          p_MONEDA  ->    Moneda a consultar.
   -- Output:
   --          p_CURSOR -> SYS_REFCURSOR, resultados de consulta.
   --          p_ERROR  -> Indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_BUS_ENC_SALDOS_INTRA_MX (p_MONEDA   IN     CHAR,
                                         p_CURSOR      OUT SYS_REFCURSOR,
                                         p_ERROR       OUT NUMBER);



   /************************************************************************************************
   -- Funcion/Procedimiento: SP_BUS_ENC_SALDOS_24HRS_MX
   -- Objetivo: Procedimiento almacenado que Muestra encabezado de saldos 24 horas MX
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX
   --
   -- Fecha: 14-11-2017
   -- Autor: Santander
   -- Input:
   --          p_MONEDA  ->    Moneda a consultar.
   -- Output:
   --          p_CURSOR -> SYS_REFCURSOR, resultados de consulta.
   --          p_ERROR  -> Indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_BUS_ENC_SALDOS_24HRS_MX (p_MONEDA   IN     CHAR,
                                         p_CURSOR      OUT SYS_REFCURSOR,
                                         p_ERROR       OUT NUMBER);



   /************************************************************************************************
   -- Funcion/Procedimiento: SP_BUS_ENC_SALDOS_48HRS_MX
   -- Objetivo: Procedimiento almacenado que Muestra encabezado de saldos 48 horas MX
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX
   --
   -- Fecha: 14-11-2017
   -- Autor:
   -- Input:
   --          p_MONEDA  ->    Moneda a consultar.
   -- Output:
   --          p_CURSOR -> SYS_REFCURSOR, resultados de consulta.
   --          p_ERROR  -> Indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_BUS_ENC_SALDOS_48HRS_MX (p_MONEDA   IN     CHAR,
                                         p_CURSOR      OUT SYS_REFCURSOR,
                                         p_ERROR       OUT NUMBER);

   /************************************************************************************************
   -- Funcion/Procedimiento: SP_GES_CAJA_EGRESO_MX
   -- Objetivo: Procedimiento que actualiza los saldos de operaciones MX recepcionadas tras una confirmación de Swift
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN,PABS_DT_DETLL_SALDO_CAJA_MN_MX
   -- Fecha: 14-11-2017
   -- Autor: Santander
   -- Input:
   -- p_NUM_REF_SWF  -> Referencia de operación a actualizar
   -- Output:
   -- p_ERROR        -> Indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_GES_CAJA_EGRESO_MX (p_NUM_REF_SWF   IN     VARCHAR2,
                                    p_ERROR            OUT NUMBER);



   /************************************************************************************************
   -- Funcion/Procedimiento: SP_OBT_MOV_PROY_MX
   -- Objetivo: Procedimiento que obtiene los movimientos cargados proyectados MX
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_MVNTO_CAJA
   -- Fecha: 14-11-2017
   -- Autor: Santander
   -- Input:
   -- N/A
   -- Output:
   -- p_CURSOR -> SYS_REFCURSOR, resultados de consulta.
   -- p_ERROR        -> Indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_OBT_MOV_PROY_MX (p_CURSOR   OUT SYS_REFCURSOR,
                                 p_ERROR    OUT NUMBER);



   /************************************************************************************************
   -- Funcion/Procedimiento: SP_ELI_MOV_PROY_MX
   -- Objetivo: Procedimiento que ELIMINA movimientos cargados proyectados MX
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX
   -- Fecha: 14-11-2017
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_OPE        -> Número de operación
   -- p_COD_BIC_ORG        -> Código Bic Banco
   -- p_COD_DIV            -> Código Divisa
   -- p_NUM_CTA_CTE        -> Número cuenta corriente
   -- Output:
   -- p_ERROR                -> Indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_ELI_MOV_PROY_MX (p_NUM_FOL_OPE   IN     NUMBER,
                                 p_COD_BIC_ORG   IN     VARCHAR2,
                                 p_COD_DIV       IN     CHAR,
                                 p_NUM_CTA_CTE   IN     VARCHAR2,
                                 p_ERROR            OUT NUMBER);


   /************************************************************************************************
   -- Funcion/Procedimiento: SP_ACT_SLD_EGR_MX
   -- Objetivo:
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: DUAL
   -- Fecha: 14-11-2017
   -- Autor: Santander
   -- Input:
   -- p_NUM_REF_SWF        -> Número referencia
   -- Output:
   -- p_ERROR                -> Indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/

   PROCEDURE SP_ACT_SLD_EGR_MX (p_NUM_REF_SWF   IN     VARCHAR2,
                                p_ERROR            OUT NUMBER);

   /************************************************************************************************
   -- Funcion/Procedimiento: SP_BUS_ENC_SALDOS_DIARIOS_MX
   -- Objetivo: Procedimiento almacenado que Muestra encabezado de saldos Diarios MX
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX
   --
   -- Fecha: 14-11-2017
   -- Autor: Santander
   -- Input:
   --          p_MONEDA  ->    Moneda a consultar.
   -- Output:
   --          p_CURSOR -> SYS_REFCURSOR, resultados de consulta.
   --          p_ERROR  -> Indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_BUS_ENC_SALDOS_DIARIOS_MX (p_MONEDA   IN     CHAR,
                                           p_CURSOR      OUT SYS_REFCURSOR,
                                           p_ERROR       OUT NUMBER);

   /************************************************************************************************
   -- Funcion/Procedimiento: SP_ACT_SLD_ING_MX
   -- Objetivo: ACTUALIZA INGRESOS PARA CAJA MX.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: DUAL
   -- Fecha: 14-11-2017
   -- Autor: Santander
   -- Input:
   -- p_NUM_REF_SWF        -> Número referencia
   -- p_NUM_REF_EXT        -> Numero de referencia externa cuando es un MT900 / 910
   -- p_COD_MT_SWF         -> Numero de mensaje asociado
   -- Output:
   -- p_ERROR                -> Indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/

   PROCEDURE SP_ACT_SLD_ING_MX (p_NUM_REF_SWF   IN     VARCHAR2,
                                p_NUM_REF_EXT   IN     VARCHAR2, -- SI ES UN MT0900 O MT 910 SE DEBE ENVIAR LA REFERENCIA RELACIONADA.
                                p_COD_MT_SWF    IN     CHAR,
                                p_ERROR            OUT NUMBER);

   /************************************************************************************************
   -- Funcion/Procedimiento: SP_BUS_SALDO_ONLINE_MX
   -- Objetivo: Procedimiento almacenado que Muestra los datos de saldo ONLINE
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX
   --
   -- Fecha: 31-10-2017
   -- Autor: Santander
   -- Input:
   --          p_MONEDA  ->    Moneda a consultar.
   --
   -- Output:
   --          p_CURSOR ->     SYS_REFCURSOR, resultados de consulta.
   --          p_ERROR  ->     Indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_BUS_SALDO_ONLINE_MX (p_MONEDA   IN     CHAR,
                                     p_CURSOR      OUT SYS_REFCURSOR,
                                     p_ERROR       OUT NUMBER);


   /************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_VAL_COD_ABA
   -- Objetivo: Procedimiento que busca codigo ABA de banco dado un BIC
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: DBO_PAB.PABS_DT_BANCO_ALMOT
   --
   -- Fecha: 27-09-2018
   -- Autor: Santander
   -- Input:
   --          p_COD_ABA  ->    CODIGO ABA A CONSULTAR.
   --
   -- Output:
   --          p_COD_BCO ->     CODIGO BIC CORRESPONDIENTE AL ABA INGRESADO.
   --          p_ERROR  ->     Indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_PAB_VAL_COD_ABA (p_COD_ABA   IN     CHAR,
                                 p_COD_BCO      OUT CHAR,
                                 p_ERROR        OUT NUMBER);
END PKG_PAB_CAJA_MX;