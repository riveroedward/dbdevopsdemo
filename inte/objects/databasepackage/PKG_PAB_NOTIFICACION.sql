CREATE OR REPLACE PACKAGE         PKG_PAB_NOTIFICACION
AS
   /******************************************************************************
      NAME:       PKG_PAB_NOTIFICACION
      PURPOSE:

      REVISIONS:
      Ver        Date        Author           Description
      ---------  ----------  ---------------  ------------------------------------
      1.0        10-07-2017      carcehu       1. Created this package.
   ******************************************************************************/
   v_NOM_PCK          VARCHAR2 (30) := 'PKG_PAB_NOTIFICACION';
   ERR_CODE           NUMBER := 0;
   ERR_MSG            VARCHAR2 (300);
   v_DES              VARCHAR2 (300);
   v_DES_BIT          VARCHAR2 (100);
   p_s_mensaje        VARCHAR2 (400);
   v_BANCO            VARCHAR2 (9) := '970150005';
   v_FLG_VGN_NTF_NO   NUMBER (1) := 0;              -- VALOR 0, notif No leida
   v_FLG_VGN_NTF_SI   NUMBER (1) := 1;              -- VALOR 0, notif No leida
   v_motivo_eli       VARCHAR2 (70);

   v_dia_ini          DATE := TRUNC (SYSDATE) + INTERVAL '1' MINUTE;
   v_dia_fin DATE
         := TRUNC (SYSDATE) + INTERVAL '23' HOUR + INTERVAL '59' MINUTE ;


   /******************************************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUS_NTFCC_ALMOT
   -- Objetivo: Procedimiento almacenado que retorna lista de notificaciones del dashboard y la cantidad de notificiones por leer
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_NTFCC_ALMOT
   --
   -- Fecha: 04/07/17
   -- Autor: Santander CAH
   -- Input:
   --   p_COD_SIS_ENT       Cod. sistema entrada eje:OOFF
   --   p_COD_TPO_ROL_AOS   Rol usuario ingresado
   -- Output:
   --       p_CURSOR        Notificaciones del día
   --       p_CANT_NTF     Cantidad de operaciones sin leer
   --       p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --******************************************************************************************************************************/
   PROCEDURE Sp_PAB_BUS_NTFCC_ALMOT (
      p_COD_SIS_ENT       IN     CHAR,
      p_COD_TPO_ROL_AOS   IN     NUMBER,
      p_CURSOR               OUT SYS_REFCURSOR,
      p_CANT_NTF             OUT NUMBER,
      p_ERROR                OUT NUMBER
   );


   /****************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUS_NTFCC_POR_REVIS
   -- Objetivo: Procedimiento almacenado que retorna lista de notificaciones del dashboard por revisar
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_NTFCC_ALMOT
   --
   -- Fecha: 04/07/17
   -- Autor: Santander CAH
   -- Input:
   --   p_FLG_NTF          Flag de notificaión revisada
   --   p_COD_SIS_ENT       Cod. sistema entrada eje:OOFF
   --   p_COD_TPO_ROL_AOS   Rol usuario ingresado
   -- Output:
   --       p_CURSOR        Notificaciones del día
   --       p_CANT_NTF     Cantidad de operaciones sin leer
   --       p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --************************************************************************************************/
   PROCEDURE Sp_PAB_BUS_NTFCC_POR_REVIS (
      p_FLG_NTF           IN     NUMBER,
      p_COD_SIS_ENT       IN     CHAR,
      p_COD_TPO_ROL_AOS   IN     NUMBER,
      p_CURSOR               OUT SYS_REFCURSOR,
      p_CANT_NTF             OUT NUMBER,
      p_ERROR                OUT NUMBER
   );

   /************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ACT_NTFCC_ALMOT
   -- Objetivo: Procedimiento almacenado que Actualiza las notificaciones leidas
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_SISTM_ENTRD_SALID
   --
   -- Fecha: 04/07/17
   -- Autor: Santander CAH
   -- Input:
   --   p_FEC_ING_NTF      Fecha de ingreso de la notificación
   --  p_COD_TIP_CRO      Tipo de notificación
   -- Output:
   --       p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_ACT_NTFCC_ALMOT (p_FEC_ING_NTF   IN     TIMESTAMP,
                                     p_COD_TIP_CRO   IN     CHAR,
                                     p_ERROR            OUT NUMBER);



   /************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_INS_NTFCC_ALMOT
   -- Objetivo: Procedimiento almacenado que inserta las notificaciones en la tabla
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_NTFCC_ALMOT
   --
   -- Fecha: 10/07/17
   -- Autor: Santander CAH
   -- Input:
   --   p_COD_TIP_CRO       Tipo de notificación
   --   p_COD_SIS_ENT       Cod. sistema entrada
   --   p_COD_TPO_ROL_AOS   Rol usuario
   --   p_IMP_OPE            Monto Operación asociada
   --   p_NUM_FOL_OPE      Numero de operación
   --   p_NUM_FOL_NOM      Numero de nómina
   --   p_GLS_MTV_NTF     Descripción de la notificación
   --
   -- Output:
   --       p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_INS_NTFCC_ALMOT (p_COD_TIP_CRO       IN     CHAR,
                                     p_COD_SIS_ENT       IN     CHAR,
                                     p_COD_TPO_ROL_AOS   IN     NUMBER,
                                     p_IMP_OPE           IN     NUMBER,
                                     p_NUM_FOL_OPE       IN     NUMBER,
                                     p_NUM_FOL_NOM       IN     NUMBER,
                                     p_GLS_MTV_NTF       IN     VARCHAR2,
                                     p_ERROR                OUT NUMBER);


   /************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_DET_CRRO_TIBCO
   -- Objetivo: Procedimiento almacenado que retorna el cuerpo del correo para la notificación
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_CRREO_ALMOT
   --
   -- Fecha: 10/07/17
   -- Autor: Santander CAH
   -- Input:
   --   p_REF_SWF             Referencia Swift de la operación
   --   p_NUM_FOL_OPE         Folio de operación
   --   p_FEC_ISR_OPE         fecha de inserción operación
   --   p_COD_TPO_OPE         Numero de operación
   --   p_ERROR_SWF           Numero de nómina
   --   p_COD_SIS_ENT_SAL     Descripción de la notificación
   --   p_AUX_1               Valor auxiliar utilizado para notificaciones
   --   p_AUX_2               Valor auxiliar utilizado para notificaciones
   --   p_AUX_3               Valor auxiliar utilizado para notificaciones
   --
   -- Output:
   --     p_ADJUNTO           Si tiene adjunto
   --     p_ID                Id de gestor
   --     MENSAJE_SWF         Contenido de mensaje
   --      p_ASUNTO           Asunto correo
   --      p_CORREO          Dirección de correo
   --       p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_PAB_DET_CRRO_TIBCO (p_REF_SWF           IN     VARCHAR2,
                                    p_NUM_FOL_OPE       IN     NUMBER,
                                    p_FEC_ISR_OPE       IN     DATE,
                                    p_COD_TPO_OPE       IN     CHAR,
                                    p_ERROR_SWF         IN     VARCHAR2,
                                    p_COD_SIS_ENT_SAL   IN     CHAR, --Para la notif de cambio horario canal.
                                    p_AUX_1             IN     VARCHAR2,
                                    p_AUX_2             IN     VARCHAR2,
                                    p_AUX_3             IN     VARCHAR2,
                                    p_ADJUNTO              OUT NUMBER,
                                    p_ID                   OUT VARCHAR2,
                                    MENSAJE_SWF            OUT VARCHAR2,
                                    p_ASUNTO               OUT VARCHAR2,
                                    p_CORREO               OUT VARCHAR2,
                                    p_ERROR                OUT NUMBER);

   /************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_BUS_OPE_SIN_MVT
   -- Objetivo: Procedimiento almacenado que retorna la operación sin cambio de estado.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_CRREO_ALMOT
   --
   -- Fecha: 16/08/17
   -- Autor: Santander CAH
   -- Input:
   --   p_NUM_FOL_OPE       Numero de operación
   --   p_FEC_ISR_OPE        fecha de operación
   --   p_COD_TPO_OPE     Tipo notificación
   -- Output:
   --      p_ID_NTF        Id de notificación
   --      p_CANT_PAB      Cantidad de operaciones
   --       p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_PAB_BUS_OPE_SIN_MVT (p_NUM_FOL_OPE   IN     NUMBER,
                                     p_FEC_ISR_OPE   IN     DATE,
                                     p_COD_TPO_OPE   IN     VARCHAR2,
                                     p_ID_NTF           OUT NUMBER,
                                     p_CANT_PAB         OUT NUMBER,
                                     p_ERROR            OUT NUMBER);

   /************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUS_OPE_SIN_CBO
   -- Objetivo: Procedimiento almacenado que retorna tiempo de operaciones sin cambio de estado,
   --          para operaciones en estado "Liberada" y "Por Abonar"
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN
   --
   -- Fecha: 30/06/17
   -- Autor: Santander CAH
   -- Input:
   --       p_COD_EST_AOS      Estado Operación(liberada-Por abonar)
   -- Output:
   --       p_FLG_NTF       Indica Si hay que notificar
   --       p_CAN_OPE_NOT   Cantidad de operaciones sin cambios
   --       p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_BUS_OPE_SIN_CBO (p_COD_EST_AOS   IN     VARCHAR2,
                                     p_FLG_NTF          OUT NUMBER,
                                     p_CAN_OPE_NOT      OUT NUMBER,
                                     p_ERROR            OUT NUMBER);

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_GES_NTF_OPE
   -- Objetivo: Procedimiento que gestiona el envio de notificación para cada operacion eliminada.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: N/A
   -- Fecha: 18/08/17
   -- Autor: Santander
   -- Input:
   --      p_REG_OPER       -> parametro tipo registro que contiene el numero y fecha operacion
   --      p_GLS_MTV_EST    -> motivo o glosa de eliminacion
   -- Output:

   --     p_ASUNTO           Asunto correo
   --     p_CORREO          Dirección de correo
   --     p_MENSAJE_SWF      Contenido de mensaje
   --     p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_GES_NTF_OPE (p_REG_OPER      IN     REG_OPER,
                                 p_GLS_MTV_EST   IN     VARCHAR2,
                                 p_ASUNTO           OUT VARCHAR2,
                                 p_CORREO           OUT VARCHAR2,
                                 p_MENSAJE_SWF      OUT VARCHAR2,
                                 p_ERROR            OUT NUMBER);

   /************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_GES_NTFCC_ALMOT
   -- Objetivo: Procedimiento almacenado que crea notificaciones solo por pantalla (Dashborad)
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_NTFCC_ALMOT
   --
   -- Fecha: 21/08/17
   -- Autor: Santander CAH
   -- Input:
   --   p_NUM_REF_SWF       Numero de referencia
   --   p_NUM_FOL_OPE       Numero de Operación
   --   p_FEC_ISR_OPE       Fecha de Operación
   --   p_NUM_FOL_NMN       Numero de Nómina
   --  p_COD_TPO_OPE       Tipo de notifiación
   -- Output:
   --   p_ERROR:            indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_GES_NTFCC_ALMOT (p_NUM_REF_SWF   IN     CHAR,
                                     p_NUM_FOL_OPE   IN     NUMBER,
                                     p_FEC_ISR_OPE   IN     DATE,
                                     p_NUM_FOL_NMN   IN     NUMBER,
                                     p_COD_TPO_OPE   IN     CHAR,
                                     p_COD_SIS_ENT   IN     CHAR,
                                     p_ERROR            OUT NUMBER);

   --***********************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_BUS_CAN_OPE_EST_AOS
   -- Objetivo: Retorna cantidad de operaciones por estado
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA
   -- Fecha:21/08/2017
   -- Autor: Santander-CAH
   -- Input:
   -- P_COD_SIS_ENT_SAL -> CODIGO SISTEMA ENTRADA SALIDA (CANAL)
   -- N/A
   -- Output:
   -- N/A.
   -- Input/Output:
   -- N/A.
   -- Retorno:
   -- V_CANT_LIB -> valor de retorno que muestra el horario de inicio segun el area
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   --***********************************************************************************************
   FUNCTION FN_PAB_BUS_CAN_OPE_EST_AOS (P_NUM_FOL_NMN      NUMBER,
                                        P_COD_EST_AOS   IN NUMBER)
      RETURN NUMBER;

   /******************************************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_GEN_NTFCC
   -- Objetivo: Procedimiento que gestiona Notificaciones Generales
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN, PABS_DT_ESTDO_ALMOT
   --
   -- Fecha: 04/07/17
   -- Autor: Santander CAH
   -- Input:
   -- NO APLICA
   -- Output:
   --       p_CURSOR  Operacion con notificaciones.
   --       p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --******************************************************************************************************************************/
   PROCEDURE Sp_PAB_GEN_NTFCC (p_CURSOR   OUT SYS_REFCURSOR,
                               p_ERROR    OUT NUMBER);
END PKG_PAB_NOTIFICACION;