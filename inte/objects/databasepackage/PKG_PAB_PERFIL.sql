CREATE OR REPLACE PACKAGE         PKG_PAB_PERFIL
AS
   /**  Package el cual contrendra todas las funciones y procedimientos relacionados con los perfiles
   -- @Santander */

   /**************************VARIABLE GLOBALES************************************/
   v_NOM_PCK     VARCHAR2 (30) := 'PKG_PAB_PERFIL';
   v_NOM_SP      VARCHAR2 (30);
   ERR_CODE      NUMBER := 0;
   ERR_MSG       VARCHAR2 (300);
   v_DES         VARCHAR2 (300);
   v_DES_BIT     VARCHAR2 (100);
   v_COD_USU     CHAR (11);
   p_s_mensaje   VARCHAR2 (400);

   /**************************FIN VARIABLE GLOBALES********************************/

   /************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUS_MENU_USU
   -- Objetivo: Procedimiento que obitne el menu del usaurio segun su perfil y area
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_USRIO
   --        , PABS_DT_TIPO_ROL_SISTM_ENTRD
   --        , PABS_DT_RELCN_ROL_MENU
   --        , PABS_DT_MENU_ALMOT
   -- Fecha: 02/06/16
   -- Autor: Santander
   -- Input: p_COD_USU->    CODIGO DEL USUARIO
   -- Output:
   --      p_CURSOR:   Cursor de salida con datos del menu por usuario.
   --          p_ERROR:  indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_BUS_MENU_USU (p_COD_USU   IN     CHAR,
                                  p_CURSOR       OUT SYS_REFCURSOR,
                                  p_ERROR        OUT NUMBER);

   /************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_CON_EXIS_USU
   -- Objetivo: Procedimiento que muestra si el usuario esta vigente y habilitado para funcionar en el aplicativo.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_USRIO
   --        , PABS_DT_TIPO_ROL_SISTM_ENTRD
   --        , PABS_DT_TIPO_ROL_ALMOT
   --        , PABS_DT_SISTM_ENTRD_SALID
   -- Fecha: 02/06/16
   -- Autor: Santander
   -- Input: p_COD_USU ->    CODIGO DEL USUARIO
   -- Output:
   --      p_CURSOR: Cursor de salida con datos usuario vigente.
   --          p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/

   PROCEDURE Sp_PAB_CON_EXIS_USU (p_COD_USU   IN     CHAR,
                                  p_CURSOR       OUT SYS_REFCURSOR,
                                  p_ERROR        OUT NUMBER);

   /******************************************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ACT_PERFIL
   -- Objetivo: Procedimiento almacenado que retorna lista de usuarios de altos montos.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_USRIO
   --
   -- Fecha: 03-01-2018
   -- Autor: Santander CAH
   -- Input:
   --  p_COD_USR           Rut usuario
   --   p_COD_SIS_ENT       Cod. sistema entrada eje:OOFF
   --   p_COD_TPO_ROL_AOS   Rol usuario ingresado
   -- Output:
   --       p_CURSOR        Notificaciones del día
   --       p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --******************************************************************************************************************************/
   PROCEDURE Sp_PAB_BUS_PERFIL (p_COD_USR       IN     CHAR,
                                p_COD_SIS_ENT   IN     CHAR,
                                p_CURSOR           OUT SYS_REFCURSOR,
                                p_ERROR            OUT NUMBER);

   /******************************************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ACT_PERFIL
   -- Objetivo: Procedimiento almacenado que actuliza el perfil de un usuario
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_USRIO
   --
   -- Fecha: 03-01-2018
   -- Autor: Santander CAH
   -- Input:
   --   p_COD_USR           Rut usuario
   --   p_COD_SIS_ENT       Cod. sistema entrada
   --   p_COD_TPO_ROL_AOS  Rol de usuario
   -- Output:
   --       p_CURSOR        Notificaciones del día
   --       p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --******************************************************************************************************************************/
   PROCEDURE Sp_PAB_ACT_PERFIL (p_COD_USR_PAD       IN     CHAR,
                                p_COD_USR           IN     CHAR,
                                p_COD_SIS_ENT       IN     CHAR,
                                p_COD_TPO_ROL_AOS   IN     NUMBER,
                                p_ERROR                OUT NUMBER);

   /******************************************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ELI_PERFIL
   -- Objetivo: Procedimiento almacenado que elimina un usuario de altos montos.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_USRIO
   --
   -- Fecha: 03-01-2018
   -- Autor: Santander CAH
   -- Input:
   --  p_COD_USR           Rut usuario
   --   p_COD_SIS_ENT       Cod. sistema entrada
   --   p_COD_TPO_ROL_AOS   Rol usuario ingresado
   -- Output:
   --       p_CURSOR        Notificaciones del día
   --       p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --******************************************************************************************************************************/
   PROCEDURE Sp_PAB_ELI_PERFIL (p_COD_USR           IN     CHAR,
                                p_COD_SIS_ENT       IN     CHAR,
                                p_COD_TPO_ROL_AOS   IN     NUMBER,
                                p_ERROR                OUT NUMBER);

   /******************************************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_INS_PERFIL
   -- Objetivo: Procedimiento almacenado que inserta un nuevo usuario en altos montos.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_USRIO
   --
   -- Fecha: 03-01-2018
   -- Autor: Santander CAH
   -- Input:
   --  p_COD_USR_PAD       Rut usuario creación
   --  p_COD_USR           Rut usuario
   --   p_COD_SIS_ENT       Cod. sistema entrada
   --   p_COD_TPO_ROL_AOS   Rol usuario ingresado
   -- Output:
   --       p_CURSOR        Notificaciones del día
   --       p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --******************************************************************************************************************************/
   PROCEDURE Sp_PAB_INS_PERFIL (p_COD_USR_PAD       IN     CHAR,
                                p_COD_USR           IN     CHAR,
                                p_COD_SIS_ENT       IN     CHAR,
                                p_COD_TPO_ROL_AOS   IN     NUMBER,
                                p_ERROR                OUT NUMBER);

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_COMBO_AREA
   -- Objetivo: Procedimiento que busca Area
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_SISTM_ENTRD_SALID
   -- Fecha: 03/07/2019
   -- Autor: NEORIS
   -- Input:
   -- N/A
   -- Output:
   -- p_CURSOR -> Cursor con los registros de los 2 estados
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************                                    );
   PROCEDURE SP_PAB_COMBO_AREA (p_CURSOR   OUT SYS_REFCURSOR,
                                p_ERROR    OUT NUMBER);

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_COMBO_CANAL_CAM_SAL
   -- Objetivo: Procedimiento que busca Area
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_SISTM_ENTRD_SALID
   -- Fecha: 03/07/2019
   -- Autor: NEORIS
   -- Input:
   -- N/A
   -- Output:
   -- p_CURSOR -> Cursor con los registros de los 2 estados
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************                                    );
   PROCEDURE Sp_PAB_COMBO_CANAL_CAM_SAL (p_CURSOR   OUT SYS_REFCURSOR,
                                         p_ERROR    OUT NUMBER);

   /********************************************************************************************************
   FUNCION/PROCEDIMIENTO: SP_PAB_COMBO_PERFIL.
   OBJETIVO             : MUESTRA FILTRO EN: ADM SISTEMAS -> MANTENEDORES -> GESTION DE PERFILES.
                              COMBO: ÁREA.
   SISTEMA              : DBO_PAB.
   BASE DE DATOS        : DBO_PAB.
   TABLAS USADAS        : PABS_DT_TIPO_ROL_ALMOT.
                          PABS_DT_TIPO_ROL_SISTM_ENTRD
   FECHA                : 03/07/2018
   AUTOR                : NEORIS.
   INPUT                : P_COD_SIS_ENT := CÓDIGO SISTEMA ENTRADA/SALIDA.
   OUTPUT               : P_CURSOR      := RESULTADOS DE CONSULTA.
                          P_ERR_MSG := GLOSA QUE INDICA EL ERROR.
   OBSERVACIONES
   FECHA       USUARIO  DETALLE
   03/07/2018  NEORIS   CREACIÓN SP.
   ********************************************************************************************************/
   PROCEDURE Sp_PAB_COMBO_PERFIL (p_COD_SIS_ENT   IN     CHAR,
                                  p_CURSOR           OUT SYS_REFCURSOR);
END PKG_PAB_PERFIL;