CREATE OR REPLACE PACKAGE         PKG_PAB_GARANTIA
AS
   V_NOM_PCK     VARCHAR2 (30) := 'PKG_PAB_GARANTIA';
   ERR_CODE      NUMBER := 0;
   ERR_MSG       VARCHAR2 (300);
   V_DES         VARCHAR2 (300);
   V_DES_BIT     VARCHAR2 (100);
   V_BANCO       VARCHAR2 (9) := '970150005';

   TYPE T_NUM_FOL_NMN
   IS
      TABLE OF NUMBER
         INDEX BY PLS_INTEGER;

   p_s_mensaje   VARCHAR2 (400);

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_INS_GRNTIA_CMDR_CMBNC
   -- Objetivo: Gestiona la insercion las operaciones que llegan por garantia tanto para combanc y comder.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA
   -- Fecha: 24/10/16
   -- Autor: Santander
   -- Input:
   --p_COD_TPO_GRNTIA -> Tipo de garantia
   --p_FEC_ISR_OPE -> Fecha de insercion de operacion origen
   --p_NUM_FOL_OPE  -> numero de folio operacion origen
   --p_FEC_VTA -> fecha de valuta
   --p_COD_DVI -> codigo divisa
   --p_IMp_OPE -> monto garantia
   --p_COD_BCO_DTN -> banco destino
   --p_COD_BCO_BFC -> banco beneficiario
   --p_NUM_FOL_MSJ -> numero de folio mensaje nuevo
   --p_FEC_ISR_MSJ -> fecha de insercion mensaje nuevo
   --p_NUM_REF_SWF -> referencia swift
   --p_NUM_CTA_CTE -> numero de cuenta corriente COMDER
   --p_GLS_TPO_LLM_MRG -> glosa de llamado a margen
   --P_FEC_LLM_MRG -> Fecha llamado a margen
   --P_FEC_RCP_SWF -> fecha de recepcion del swift
   --p_HOR_RCP_SWF -> hora de recepcion de swift
   --P_COD_BCO_ORD  -> banco ordenante
   -- Output:
   -- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_INS_GRNTIA_CMDR_CMBNC (
      p_COD_TPO_GRNTIA    IN     NUMBER,
      p_FEC_ISR_OPE       IN     VARCHAR2,
      p_NUM_FOL_OPE       IN     NUMBER,
      p_FEC_VTA           IN     VARCHAR2,
      p_COD_DVI           IN     CHAR,
      p_IMp_OPE           IN     NUMBER,
      p_COD_BCO_DTN       IN     CHAR,
      p_COD_BCO_BFC       IN     CHAR,
      --503--
      p_NUM_FOL_MSJ       IN     NUMBER,
      p_FEC_ISR_MSJ       IN     VARCHAR2,
      p_NUM_REF_SWF       IN     VARCHAR2,
      p_NUM_CTA_CTE       IN     VARCHAR2,
      p_GLS_TPO_LLM_MRG   IN     CHAR,
      P_FEC_LLM_MRG       IN     VARCHAR2,
      P_FEC_RCP_SWF       IN     VARCHAR2,
      p_HOR_RCP_SWF       IN     NUMBER,
      P_COD_BCO_ORD       IN     CHAR,
      p_ERROR                OUT NUMBER,
      ------------RETORNO DE NUMERO DE OPERACION
      p_o_NUM_FOL_OPE        OUT NUMBER,
      p_o_FEC_ISR_OPE        OUT DATE
   );


   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUS_COMBANC
   -- Objetivo: Busca las operaciones disponibles para pago por garantia combanc.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA
   -- Fecha: 24/10/16
   -- Autor: Santander
   -- Input:
   -- p_FEC_ISR_OPE -> Fecha de busqueda de operaciones (fecha de insercion al sistema)
   -- Output:
   -- p_Cursor -> Cursor con datos de operaciones
   -- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_BUS_COMBANC (p_FEC_ISR_OPE   IN     DATE,
                                 p_CURSOR           OUT SYS_REFCURSOR,
                                 P_ERROR            OUT NUMBER);

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUS_DETLL_COMBANC
   -- Objetivo: Busca el detalle de las operaciones disponibles para pago por garantia COMBANC.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA
   -- Fecha: 24/10/16
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_OPE -> Numero de folio de operacion a buscar
   -- p_FEC_ISR_OPE -> fecha insercion de operacion a buscar.
   -- Output:
   -- p_CURSOR-> Cursor de operaciones encontradas
   -- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_BUS_DETLL_COMBANC (p_NUM_FOL_OPE   IN     NUMBER,
                                       p_FEC_ISR_OPE   IN     DATE,
                                       p_CURSOR           OUT SYS_REFCURSOR,
                                       P_ERROR            OUT NUMBER);

   --
   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUS_COMDER
   -- Objetivo: Busca las operaciones disponibles para pago por garantia.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA
   -- Fecha: 24/10/16
   -- Autor: Santander
   -- p_FEC_ISR_OPE -> fecha insercion de operacion a buscar.
   -- Output:
   -- p_CURSOR-> Cursor de operaciones encontradas
   -- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_BUS_COMDER (p_CURSOR   OUT SYS_REFCURSOR,
                                P_ERROR    OUT NUMBER);

   --
   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_INS_MT503_COMDER
   -- Objetivo: Recibe informacion desde tibco para el ingreso de una garantia a pagar
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA
   -- Fecha: 24/10/16
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_OPE -> Numero de folio operacion origen
   -- p_FEC_ISR_OPE -> fecha insercion operacion origen
   -- p_NUM_REF_SWF -> numero de refencia swift
   -- p_COD_DVI    ->  codigo divisa
   -- p_IMP_OPE_MSJ_503  ->  monto informado mensaje 503
   -- p_FEC_VTA        -> fecha de valuta
   -- p_NUM_CTA_CTE    ->  cuenta corriente
   -- p_GLS_TPO_LLM_MRG ->  glosa tipo de llamado a margen
   -- p_FEC_LLM_MRG    ->  fecha llamado a mergen
   -- p_FEC_RCP_SWF    ->  fecha recepcion swift
   -- p_HOR_RCP_SWF    ->  hora recepcion swift
   -- p_COD_BCO_BFC    -> banco beneficiario
   -- p_COD_BCO_ORD    -> banco ordenante
   -- p_NUM_FOL_MT503  -> numero de folio mensaje 503
   -- p_FEC_ISR_MSJ_503 ->  fecha insercion mensaje 503
   -- Output:
   -- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_INS_MT503_COMDER (p_NUM_FOL_OPE       IN     NUMBER,
                                      p_FEC_ISR_OPE       IN     DATE,
                                      p_NUM_REF_SWF       IN     VARCHAR2,
                                      p_COD_DVI           IN     CHAR,
                                      p_IMP_OPE_MSJ_503   IN     NUMBER,
                                      p_FEC_VTA           IN     DATE,
                                      p_NUM_CTA_CTE       IN     VARCHAR2,
                                      p_GLS_TPO_LLM_MRG   IN     CHAR,
                                      p_FEC_LLM_MRG       IN     DATE,
                                      p_FEC_RCP_SWF       IN     DATE,
                                      p_HOR_RCP_SWF       IN     NUMBER,
                                      p_COD_BCO_BFC       IN     CHAR,
                                      p_COD_BCO_ORD       IN     CHAR,
                                      p_NUM_FOL_MT503     IN     NUMBER,
                                      p_FEC_ISR_MSJ_503   IN     DATE,
                                      P_ERROR                OUT NUMBER);

   --
   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUS_MT503
   -- Objetivo: Busca los MT503 relacionadas a operaciones pagadas por garantia.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA
   -- Fecha: 24/10/16
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_OPE -> Numero de folio de operacion a buscar
   -- p_FEC_ISR_OPE -> fecha insercion de operacion a buscar.
   -- Output:
   -- p_CURSOR-> Cursor de operaciones encontradas
   -- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_BUS_MT503 (p_NUM_FOL_OPE   IN     NUMBER,
                               p_FEC_ISR_OPE   IN     DATE,
                               p_CURSOR           OUT SYS_REFCURSOR,
                               P_ERROR            OUT NUMBER);

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUS_DETLL_COMDER
   -- Objetivo: Busca el detalle de las operaciones disponibles para pago por garantia.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA
   -- Fecha: 24/10/16
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_OPE -> Numero de folio de operacion a buscar
   -- p_FEC_ISR_OPE -> fecha insercion de operacion a buscar.
   -- Output:
   -- p_CURSOR-> Cursor de operaciones encontradas
   -- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_BUS_DETLL_COMDER (p_NUM_FOL_OPE   IN     NUMBER,
                                      p_FEC_ISR_OPE   IN     DATE,
                                      p_CURSOR           OUT SYS_REFCURSOR,
                                      P_ERROR            OUT NUMBER);

   --

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUS_DIA_HAB
   -- Objetivo: Procedimiento que realiza la busqueda de los dias habilies
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas:
   -- Fecha: 24/10/16
   -- Autor: Santander
   -- Input
   -- p_DIA_VAL -> Fecha a validar
   -- P_COD_PAIS -> Codigo pais del calendaro
   -- Output:
   -- P_IND_HABIL -> Indicaro de dia habil
   -- P_ERRORCODE -> codigo error de PL CHICON
   -- P_ERRORMSG  -> mensaje de error CHICON
   -- P_FEC_VAL   ->  fecha de valura
   -- P_VAL_FEC  ->  valor de la fecha
   -- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_BUS_DIA_HAB (p_DIA_VAL     IN     NUMBER,
                                 P_COD_PAIS    IN     CHAR,
                                 P_IND_HABIL      OUT NUMBER,
                                 P_ERRORCODE      OUT NUMBER,
                                 P_ERRORMSG       OUT VARCHAR2,
                                 P_FEC_VAL        OUT VARCHAR2,
                                 P_VAL_FEC        OUT NUMBER,
                                 P_ERROR          OUT NUMBER);

   --
   -- ***********************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_BUS_IMP_GAR
   -- Objetivo: Funcion que retorna valores de divisas sus montos maximos por moneda.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_MONDA_ALMOT
   -- Fecha: 24/10/16
   -- Autor: Santander
   -- Input:
   -- P_COD_DVI : Codigo de divisa
   -- Output:
   -- Input/Output: N/A
   -- Retorno: MONTO de garantoa
   -- Observaciones: N/A.
   -- ***********************************************************************************************
   FUNCTION FN_PAB_BUS_IMP_GAR (P_COD_DVI IN CHAR)
      RETURN NUMBER;
END PKG_PAB_GARANTIA;