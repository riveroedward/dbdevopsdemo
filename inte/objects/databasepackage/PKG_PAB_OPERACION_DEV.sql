CREATE OR REPLACE PACKAGE         PKG_PAB_OPERACION_DEV
AS
   /**  Package el cual contrendra todas las funciones y procedimientos relacionados con las operaciones de devolucion
   -- @Santander */

   /******************************************************************************
      NAME:       PKG_PAB_OPERACION_DEV
      PURPOSE: Package que contiene lo procedimientos que se encargan de la insercion
              de una operacion de devolucion, abono y envio devolucion.

      REVISIONS:
      Ver        Date        Author           Description
      ---------  ----------  ---------------  ------------------------------------
      1.0        13-04-2017             1. Created this package.
   ******************************************************************************/
   -- ************************** INICIO VARIABLE GLOBALES ********************************
   v_NOM_PCK      VARCHAR2 (30) := 'PKG_PAB_OPERACION_DEV';
   ERR_CODE       NUMBER := 0;
   ERR_MSG        VARCHAR2 (300);
   v_DES          VARCHAR2 (300);
   v_DES_BIT      VARCHAR2 (100);
   v_BANCO        VARCHAR2 (9) := '970150005';
   p_s_mensaje    VARCHAR2 (400);
   TOPE_MSJ_BLQ   NUMBER := 299; --VALOR MAXIMO DE DETALLE DE MENSAJE PROTOCOLO POR BLOQUES

   TYPE T_NUM_FOL_NMN
   IS
      TABLE OF NUMBER
         INDEX BY PLS_INTEGER;

   v_dia_ini      DATE := TRUNC (SYSDATE) + INTERVAL '1' MINUTE;
   v_dia_fin DATE
         := TRUNC (SYSDATE) + INTERVAL '23' HOUR + INTERVAL '59' MINUTE ;
   v_cero         NUMBER (1) := 0;

   --   TOPE_MSJ_BLQ NUMBER := 299;  --VALOR MAXIMO DE DETALLE DE MENSAJE PROTOCOLO POR BLOQUES
   --   TYPE T_NUM_FOL_NMN  is table of NUMBER index by pls_integer;
   -- ************************** FIN VARIABLE GLOBALES ********************************

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_GEN_MT_DEV
   -- Objetivo: Procedimiento almacenado que busca detalle devolucion manual
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN y PABS_DT_OPRCN_INFCN_OPCON
   -- Fecha: 07/11/16
   -- Autor: Santander
   -- Input:
   -- p_FEC_ISR_OPE -> Fecha de insercion de operacion en el sistema, PK tabla detalle operaciones.
   -- p_NUM_FOL_OPE -> foliador del banco, PK tabla detalle operaciones.
   -- Output:
   -- p_CURSOR -> Cursor con las operaciones encontradas
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_GEN_MT_DEV (p_FEC_ISR_OPE   IN     DATE,
                                p_NUM_FOL_OPE   IN     NUMBER,
                                p_CURSOR           OUT SYS_REFCURSOR,
                                p_ERROR            OUT NUMBER);



   /************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_GEN_OPE_DEV
   -- Objetivo: Procedimiento almacenado Orquestador que llama al PL que inserta Operacion Para Devolver y luego al PL que actualiza la operacion original,
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas:
   -- Fecha: 24/03/17
   -- Autor: Santander--CAH
   -- Input:
   -- p_NUM_FOL_OPE        -->Foliador del banco, PK tabla detalle operaciones.
   -- p_FEC_ISR_OPE_ORG    -->Folio operacion de origen, PK tabla detalle operaciones.
   -- p_NUM_FOL_OPE_ORG    -->Fecha de insercion de operacion origina, PK tabla detalle operaciones.
   -- p_COD_DVI            -->Codigo de moneda
   -- p_IMP_OPE            -->Monto operacion
   -- p_COD_BCO_DTN        -->Codigo banco destino BIC
   -- p_NUM_REF_EXT        -->Codigo Referencia SWIFT externa
   -- p_OBS_OPC_SWF        -->Observacion opcional
   -- p_COD_USR            -->Codigo usuario
   -- Output:
   -- p_ERROR              -->indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_GEN_OPE_DEV (p_FEC_ISR_OPE_ORG   IN     DATE,
                                 p_NUM_FOL_OPE_ORG   IN     NUMBER,
                                 p_COD_DVI           IN     CHAR,
                                 p_IMP_OPE           IN     NUMBER,
                                 p_COD_BCO_DTN       IN     CHAR,
                                 p_NUM_REF_EXT       IN     CHAR,
                                 ----------Opcionales-------
                                 p_OBS_OPC_SWF       IN     VARCHAR2,
                                 --------------------------------
                                 p_COD_USR           IN     CHAR,
                                 p_ERROR                OUT NUMBER,
                                 p_NUM_FOL_OPE          OUT NUMBER,
                                 p_FEC_ISR_OPE          OUT DATE);

   --
   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_BUS_OPE_ASO_SWF
   -- Objetivo: Procedimiento almacenado que retorna las operaciones origen posibles para asociar
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN_HTRCA, PABS_DT_OPRCN_OPCON_HTRCA
   -- Fecha: 07/11/16
   -- Autor: Santander
   -- Input:
   -- p_COD_BCO_DTN    -> codigo banco destino
   -- p_MTO_OPE        -> importe operacion
   -- p_FLG_EGR_ING    -> Tipo de operacion Ingreso = 1 o Egreso = 0
   -- p_FECHA_DES      -> Fecha desde
   -- p_FECHA_DES      -> Fecha hasta
   -- p_NUM_REF_SWF    -> Codigo Referencia SWIFT
   -- Output:
   -- p_CURSOR         -> Cursor con las operaciones posibles
   -- p_ERROR          -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_BUS_OPE_ASO_SWF (p_COD_BCO_DTN   IN     CHAR,
                                     p_MTO_OPE       IN     NUMBER,
                                     p_FLG_EGR_ING   IN     NUMBER,
                                     p_FECHA_DES     IN     DATE,
                                     p_FECHA_HAS     IN     DATE,
                                     p_NUM_REF_SWF   IN     CHAR,
                                     p_CURSOR           OUT SYS_REFCURSOR,
                                     p_ERROR            OUT NUMBER);

   --
   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_ASO_OPE_ORI_SWF
   -- Objetivo: Procedimiento almacenado que asocia operacion origen y actualiza estado de la operacion
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN
   -- Fecha: 07/11/16
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_OPE     -> foliador del banco, PK tabla detalle operaciones.
   -- p_FEC_ISR_OPE     -> Fecha de insercion de operacion en el sistema, PK tabla detalle operaciones.
   -- p_NUM_REF_SWF     -> Numero de Refencia SWIFT
   -- p_NUM_FOL_OPE_ORG -> Numero de folio de la operacion origen
   -- p_FEC_ISR_OPE_ORG -> Fecha de insercion de la operacion origen
   -- p_COD_USR         -> Codigo de Usuario que se registrara en bitacora
   -- Output:
   -- p_ERROR          -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_ASO_OPE_ORI_SWF (p_NUM_FOL_OPE       IN     NUMBER,
                                     p_FEC_ISR_OPE       IN     DATE,
                                     p_NUM_REF_EXT       IN     CHAR,
                                     p_NUM_FOL_OPE_ORG   IN     NUMBER,
                                     p_FEC_ISR_OPE_ORG   IN     DATE,
                                     p_COD_USR           IN     CHAR,
                                     p_ERROR                OUT NUMBER);

   --
   --
   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_GES_ABO_MAN
   -- Objetivo: Procedimiento actualiza estado de la operacion devuelta y la operacion origen
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: N/A
   -- Fecha: 18/11/16
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_OPE_ORI    -> Numero de Operacion Origen
   -- p_FEC_ISR_OPE_ORI    -> Fecha de insercion de operacion origen
   -- p_SIS_ENT_ORI        -> Nombre Canal de Entrada origen
   -- p_NUM_FOL_OPE_DEV    -> Numero de Operacion Devuelta
   -- p_FEC_ISR_OPE_DEV    -> Fecha de insercion de operacion devuelta
   -- p_NUM_CTA_BFC        -> Numero de cuenta
   -- p_TIP_ABO_MAN        -> Codigo de tipo Abono
   -- p_NUM_DEV_TRANS      -> Numero devuelto por WS de Vigencia Salida
   -- p_COD_USR            -> Codigo de Usuario que se registrara en bitacora
   -- Output:
   -- p_ERROR              -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_GES_ABO_MAN (p_NUM_FOL_OPE_ORI   IN     NUMBER,
                                 p_FEC_ISR_OPE_ORI   IN     DATE,
                                 p_SIS_ENT_ORI       IN     CHAR,
                                 p_NUM_FOL_OPE_DEV   IN     NUMBER,
                                 p_FEC_ISR_OPE_DEV   IN     DATE,
                                 p_NUM_CTA           IN     CHAR,
                                 p_TIP_ABO_MAN       IN     CHAR,
                                 p_NUM_DEV_TRANS     IN     VARCHAR2,
                                 p_COD_USR           IN     CHAR,
                                 p_ERROR                OUT NUMBER);

   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUS_SIS_SAL
   -- Objetivo: Procedimiento almacenado que Retorna Codigo de Sistema de Salida, segun Banco y Horario
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_BANCO_ALMOT,
   --          PABS_DT_SISTM_ENTRD_SALID,
   -- Fecha: 28/03/17
   -- Autor: Santander--CAH
   -- Input:
   -- p_COD_BCO_DTN    -->Codigo banco destino BIC
   -- p_COD_MT_SWF     -->Codigo MT SWF
   -- Output:
   -- p_COD_SIS_SAL    -->Codigo de sistema salida
   -- p_ERROR          -->indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/

   PROCEDURE Sp_PAB_BUS_SIS_SAL (p_COD_BCO_DTN   IN     CHAR,
                                 p_COD_MT_SWF    IN     NUMBER,
                                 p_COD_SIS_SAL      OUT CHAR,
                                 p_ERROR            OUT NUMBER);

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_CON_DEV_EGR
   -- Objetivo: Procedimiento que consulta y lista las operaciones Por Abonar o Por Asociar
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA, PABS_DT_DETLL_OPRCN
   -- Fecha: 27/10/16
   -- Autor: Santander
   -- Input:
   -- p_COD_EST_AOS -> Estado de la operacion Por Abonar y/o Por Asociar
   -- p_MNTO_DES -> Monto operacion desde
   -- p_MNTO_HAS -> Monto operacion hasta
   -- p_COD_DVI-> Codigo de moneda
   -- p_NUM_REF_SWF -> Numero Referencia SWIFT
   -- p_COD_BCO -> Codigo de banco
   -- Output:
   -- p_CURSOR -> Cursor con las operaciones encontradas
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_CON_DEV_EGR (p_COD_EST_AOS   IN     NUMBER,
                                 p_MNTO_DES      IN     NUMBER,
                                 p_MNTO_HAS      IN     NUMBER,
                                 p_COD_DVI       IN     CHAR,
                                 p_NUM_REF_SWF   IN     CHAR,
                                 p_COD_BCO       IN     CHAR,
                                 p_CURSOR           OUT SYS_REFCURSOR,
                                 p_ERROR            OUT NUMBER);

   --
   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_CMB_TIP_ABO
   -- Objetivo: Procedimiento que consulta y lista las tipos de abonos.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: N/A
   -- Fecha: 07/11/16
   -- Autor: Santander
   -- Input:
   -- p_COD_MT_SWF -> Codigo motivo SWIFT
   -- Output:
   -- p_CURSOR -> Cursor con loa tipos de abonos
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_CMB_TIP_ABO (p_COD_MT_SWF   IN     NUMBER,
                                 p_CURSOR          OUT SYS_REFCURSOR,
                                 p_ERROR           OUT NUMBER);

   --
   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_BUS_CTA_ABO
   -- Objetivo: Procedimiento almacenado que busca numero relacionado a la cuenta del tipo abono
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_CENTA_CNTBL
   -- Fecha: 07/11/16
   -- Autor: Santander
   -- Input:
   -- p_COD_SIS_ENT_SAL ->  codigo sistema entrada salida
   -- p_COD_DVI ->  Tipo de operaci?n Ingreso o Egreso
   -- Output:
   -- p_CURSOR -> Cursor con registro encontrado
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_BUS_CTA_ABO (p_COD_SIS_ENT_SAL   IN     CHAR,
                                 p_COD_DVI           IN     CHAR,
                                 p_CURSOR               OUT SYS_REFCURSOR,
                                 p_ERROR                OUT NUMBER);

   --

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_COMBO_EST_DEV_EGR
   -- Objetivo: Procedimiento que consulta estados Por Abonar o Por Asociar
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_ESTDO_ALMOT
   -- Fecha: 27/10/16

   -- Autor: Santander
   -- Input:
   -- N/A
   -- Output:
   -- p_CURSOR -> Cursor con los registros de los 2 estados
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_COMBO_EST_DEV_EGR (p_CURSOR   OUT SYS_REFCURSOR,
                                       p_ERROR    OUT NUMBER);

   --
   /************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ACT_EST_OPERACION_DEV
   -- Objetivo: Procedimiento que actualiza el estado de una operacion DEVUELTA Y DE ORIGEN
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN
   -- Fecha: 20/04/2017
   -- Autor: Santander- CAH
   -- Input:    p_NUM_FOL_OPE-->   Numero de Operacion
   --           p_FEC_ISR_OPE-->   Fecha de insercion de la operacion
   --           p_GLS_BTC_OPE-->   Glosa de bitacora
   --           p_NUM_REF_SWF-->   Ref Swift
   --           p_COD_EST_AOS->   Identificador de nuevo estado.
   --           p_COD_USR->       rut usuario
   -- Output:
   --        p_ERROR       -->     indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_ACT_EST_OPERACION_DEV (p_NUM_FOL_OPE   IN     NUMBER,
                                           p_FEC_ISR_OPE   IN     DATE,
                                           p_GLS_BTC_OPE   IN     VARCHAR2,
                                           p_NUM_REF_SWF   IN     CHAR,
                                           p_COD_EST_AOS   IN     CHAR,
                                           p_COD_USR       IN     VARCHAR2,
                                           p_ERROR            OUT NUMBER);



   /************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_GEN_OPE_DEV_298
   -- Objetivo: Procedimiento almacenado Orquestador que llama al PL que inserta Operacion Para Devolver y luego al PL que actualiza la operacion original,
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas:
   -- Fecha: 24/03/17
   -- Autor: Santander--CAH
   -- Input:
   -- p_NUM_FOL_OPE        -->Foliador del banco, PK tabla detalle operaciones.
   -- p_FEC_ISR_OPE_ORG    -->Folio operacion de origen, PK tabla detalle operaciones.
   -- p_NUM_FOL_OPE_ORG    -->Fecha de insercion de operacion origina, PK tabla detalle operaciones.
   -- p_COD_DVI            -->Codigo de moneda
   -- p_IMP_OPE            -->Monto operacion
   -- p_COD_BCO_DTN        -->Codigo banco destino BIC
   -- p_NUM_REF_EXT        -->Codigo Referencia SWIFT externa
   -- p_NUM_FOL_MT_298     -->Folio operacion MT298, PK tabla detalle operaciones.
   -- p_FEC_ISR_MT_298     -->Fecha de insercion de operacion MT298, PK tabla detalle operaciones.
   -- p_COD_USR            -->Codigo usuario
   -- Output:
   --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_GEN_OPE_DEV_298 (p_FEC_ISR_OPE_ORG   IN     DATE,
                                     p_NUM_FOL_OPE_ORG   IN     NUMBER,
                                     p_COD_DVI           IN     CHAR,
                                     p_IMP_OPE           IN     NUMBER,
                                     p_COD_BCO_DTN       IN     CHAR,
                                     p_NUM_REF_EXT       IN     CHAR,
                                     p_NUM_FOL_MT_298    IN     NUMBER,
                                     p_FEC_ISR_MT_298    IN     DATE,
                                     --------------------------------
                                     p_COD_USR           IN     CHAR,
                                     p_NUM_FOL_OPE          OUT NUMBER,
                                     p_ERROR                OUT NUMBER);


   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_GES_DEB_PROT
   -- Objetivo: Procedimiento actualiza estado de la operacion tras un cargo
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: N/A
   -- Fecha: 08/08/17
   -- Autor: Santander-GSD
   -- Input:
   --      p_NUM_FOL_OPE_ORI    -> Numero de Operacion Origen
   --      p_FEC_ISR_OPE_ORI    -> Fecha de insercion de operacion origen
   --      p_NUM_CTA            -> Numero de cuenta de cargo
   --      p_NUM_FOL_OPE        -> Numero de Operacion 202
   --      p_FEC_ISR_OPE        -> Fecha de insercion de operacion 202
   --      p_NUM_REF_SWF        -> Numero de referencia Swift202
   --      p_NUM_MOV            -> Número de moviimento Swift
   --      p_NUM_DEV_TRANS      -> Numero devuelto por WS de Vigencia Salida
   --      p_FEC_CGO            -> Fecha Cargo Operacion
   --    ¿ p_HRA_CGO            -> Hora Cargo Operacion
   --      p_COD_USR            -> Codigo de Usuario que se registrara en bitacora
   -- Output:
   --      p_ERROR              -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_GES_DEB_PROT (p_NUM_FOL_OPE_ORI   IN     NUMBER,
                                  p_FEC_ISR_OPE_ORI   IN     DATE,
                                  p_NUM_CTA           IN     CHAR,
                                  p_NUM_FOL_OPE       IN     NUMBER,
                                  p_FEC_ISR_OPE       IN     DATE,
                                  p_NUM_REF_SWF       IN     CHAR,
                                  p_NUM_MOV           IN     VARCHAR2,
                                  p_FEC_CGO           IN     DATE,
                                  p_HRA_CGO           IN     CHAR,
                                  p_COD_USR           IN     CHAR,
                                  p_ERROR                OUT NUMBER);

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_BUS_DEB_PAT
   -- Objetivo: Procedimiento que consulta y lista las operaciones Por Abonar o Por Asociar
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA, PABS_DT_DETLL_OPRCN
   -- Fecha: 27/10/16
   -- Autor: Santander
   -- Input:
   -- p_COD_EST_AOS -> Estado de la operacion Por Abonar y/o Por Asociar
   -- p_MNTO -> Monto operacion
   -- p_COD_DVI-> Codigo de moneda
   -- p_NUM_REF_SWF -> Numero Referencia SWIFT
   -- p_COD_BCO -> Codigo de banco
   -- p_FEC_ISR_OPE -> Fecha de insercion de la operacion
   -- Output:
   -- p_CURSOR -> Cursor con las operaciones encontradas
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_BUS_DEB_PAT (p_COD_EST_AOS   IN     CHAR,
                                 p_MNTO          IN     NUMBER,        --CHAR,
                                 p_COD_DVI       IN     CHAR,
                                 p_NUM_REF_SWF   IN     CHAR,
                                 p_COD_BCO       IN     CHAR,
                                 p_FEC_ISR_OPE   IN     DATE,
                                 p_CURSOR           OUT SYS_REFCURSOR,
                                 p_ERROR            OUT NUMBER);

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_TRAN_GLOSA_DEV
   -- Objetivo: Funcion que transforma la glosa que le corresponde a este estado
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PKG_PAB_CONSTANTES
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- p_GLS_DEV->     TEXTO A VALIDAR.
   -- Output: N/A.
   -- Input/Output: N/A.
   -- Retorno: p_DSC_GLS-> TEXTO VALIDADO
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   FUNCTION FN_PAB_TRAN_GLOSA_DEV (p_GLS_DEV IN VARCHAR2)
      RETURN VARCHAR;
END PKG_PAB_OPERACION_DEV;