CREATE OR REPLACE PACKAGE         PKG_PAB_PROTOCOLO
IS
   -- Package de Pl's desarrollados o modificados por GSD
   -- @Santander
   ---- **************************VARIABLE GLOBALES************************************
   v_NOM_PCK              VARCHAR2 (30) := 'PKG_PAB_PROTOCOLO';
   ERR_CODE               NUMBER := 0;
   ERR_MSG                VARCHAR2 (30000);
   v_DES                  VARCHAR2 (300);
   v_DES_BIT              VARCHAR2 (100);
   v_BANCO                VARCHAR2 (9) := '970150005';
   p_s_mensaje            VARCHAR2 (400);
   TOPE_MSJ_BLQ           NUMBER := 300;
   v_dia_ini              DATE := TRUNC (SYSDATE) + INTERVAL '1' MINUTE;
   v_dia_fin DATE
         := TRUNC (SYSDATE) + INTERVAL '23' HOUR + INTERVAL '59' MINUTE ;
   v_COD_TPO_OPE_BCOEXT   CHAR (8) := 'BCOEXT  ';

   TYPE T_NUM_FOL_NMN
   IS
      TABLE OF NUMBER
         INDEX BY PLS_INTEGER;

   -- **************************FIN VARIABLE GLOBALES************************************************


   /************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ACT_EST_MNSJE_PRTCZ
   -- Objetivo: Procedimiento que actualiza el estado de un mensaje protocolo
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_MNSJE_PRTCZ
   -- Fecha: 31/03/17
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_MSJ_PRZ       --> Folio mensaje protocolo
   -- p_FEC_ISR_MSJ_PRZ       --> Fecha de inserción
   -- p_COD_EST_AOS           --> Estado de mensaje
   -- p_GLS_EST_RCH           --> Glosa de estado rechazo
   -- p_COD_USR               --> Código usuario
   -- Output:
   -- p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_ACT_EST_MNSJE_PRTCZ (p_NUM_FOL_MSJ_PRZ   IN     NUMBER,
                                         p_FEC_ISR_MSJ_PRZ   IN     DATE,
                                         p_COD_EST_AOS       IN     VARCHAR2,
                                         p_GLS_EST_RCH       IN     VARCHAR2,
                                         p_COD_USR           IN     VARCHAR2,
                                         p_ERROR                OUT NUMBER);



   --**********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_BUS_MNSJE_PRTCZ
   -- Objetivo: Funcion para realizar busqueda de mensajerias de protocolo
   -- 799 / 199 / 299/ 298 / 198
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_MNSJE_PRTCZ, PABS_DT_DETLL_MNSJE_PRTCZ, PABS_DT_ESTDO_ALMOT
   -- Fecha: 31/03/17
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_MSJ_PRZ     -->  Folio mensaje protocolo
   -- p_COD_EST_AOS         -->  Código estado mensaje
   -- p_NUM_REF_SWF         -->  Referencia de mensaje
   -- p_COD_MT_SWF          -->  Código mensaje
   -- p_COD_BCO_DTN         -->  Código banco destino
   -- p_FLG_EGR_ING         -->  Flag ingreso / egreso
   -- p_FEC_DESDE           -->  Fecha incio búsqueda
   -- p_FEC_HASTA           -->  Fecha fin búsqueda
   -- p_REF_ORI             -->  Referencia operación original
   -- Output:
   -- p_CURSOR ->  Lista los mensajes de protocolo
   -- p_ERROR -> Indicada si el procedimiento
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_PAB_BUS_MNSJE_PRTCZ (
      p_NUM_FOL_MSJ_PRZ   IN     NUMBER,
      p_COD_EST_AOS       IN     VARCHAR2,
      p_NUM_REF_SWF       IN     CHAR,
      p_COD_MT_SWF        IN     NUMBER,
      p_COD_BCO_DTN       IN     CHAR,
      p_FLG_EGR_ING       IN     NUMBER,
      p_FEC_DESDE         IN     DATE,
      p_FEC_HASTA         IN     DATE,
      p_REF_ORI           IN     CHAR,
      p_CURSOR               OUT SYS_REFCURSOR,
      p_ERROR                OUT NUMBER
   );

   --******************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ACT_MSN_PRT
   -- Objetivo: Procedimiento que actualiza mensaje de protocolo
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_MNSJE_PRTCZ, PABS_DT_DETLL_MNSJE_PRTCZ
   -- Fecha: 31/03/17
   -- Autor: Santander
   -- Input:
   --p_NUM_FOL_MSJ_PRZ    --> Numero foliador mensajeria protocolo
   --p_COD_BCO_BFC        --> Codigo banco beneficiario
   --p_NUM_REF_SWF        --> Numero de referencia
   --p_COD_EST_AOS        --> Código estado
   --p_FEC_ISR_MSJ_PRZ    --> Fecha insercion mensajeria protocolo
   --p_NUM_FOL_OPE_ORI    --> Numero folio operacion original
   --p_COD_TPO_OPE        --> Estado tipo operación
   -- Output:
   -- p_ERROR             --> Indicador de errores (0 Sin errores :: 1 existen errores). Se ven en la tabla Log estos errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_ACT_MSN_PRT (p_NUM_FOL_MSJ_PRZ   IN     NUMBER,
                                 p_COD_BCO_BFC       IN     CHAR,
                                 p_NUM_REF_SWF       IN     CHAR,
                                 p_COD_EST_AOS       IN     VARCHAR2,
                                 p_FEC_ISR_MSJ_PRZ   IN     DATE,
                                 p_NUM_FOL_OPE_ORI   IN     NUMBER,
                                 p_COD_TPO_OPE       IN     CHAR,
                                 p_ERROR                OUT NUMBER);


   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_GES_MSJ_EDIT_PROT
   -- Objetivo: Gestiona la edición de mensaje de protocolo.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas:
   -- Fecha: 31/03/17
   -- Autor: Santander
   -- p_NUM_REF_SWF            --> Numero de referencia
   -- p_NUM_FOL_MSJ_PRZ        --> Numero de folio de mensajeria
   -- p_FEC_ISR_MSJ_PRZ        --> Fecha de inserción
   -- p_COD_MT_SWF             --> Codigo mensaje
   -- p_COD_EST_AOS            --> Codigo estado
   -- p_COD_BCO_BFC            --> Bic banco beneficiario
   -- p_NUM_FOL_OPE_ORI        --> Numero folio operacion origen
   -- p_COD_TPO_OPE            --> Codigo tipo operacion
   -- p_GLS_TXT_LBE_MSJ         --> Glosa del texto de 298
   -- p_COD_USR                --> Codigo usuario
   -- Output:
   -- p_ERROR          --> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Retorno: N/A.
   --***********************************************************************************************
   PROCEDURE Sp_PAB_GES_MSJ_EDIT_PROT (p_NUM_REF_SWF       IN     CHAR,
                                       p_NUM_FOL_MSJ_PRZ   IN     NUMBER,
                                       p_FEC_ISR_MSJ_PRZ   IN     DATE,
                                       p_COD_MT_SWF        IN     CHAR,
                                       p_COD_EST_AOS       IN     VARCHAR2,
                                       p_COD_BCO_BFC       IN     CHAR,
                                       p_NUM_FOL_OPE_ORI   IN     NUMBER,
                                       p_COD_TPO_OPE       IN     CHAR,
                                       p_GLS_TXT_LBE_MSJ   IN     VARCHAR2,
                                       p_COD_USR           IN     VARCHAR2,
                                       p_ERROR                OUT NUMBER);

   --***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_MENSAJE_MOTIVO_SWIFT
   -- Objetivo: PROCEDIMIENTO ALMACENADO QUE DEVUELVE LA DESCRICIÓN DE MEJSAJES 298 SEGUN TIPO DE OPERACION.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN, PABS_DT_TIPO_OPRCN
   -- Fecha: 31/03/17
   -- Autor: Santander
   -- Input:
   -- p_NUM_OPE         --> Número de folio
   -- p_FECHA_INS       --> Fecha de inserción
   -- p_COD_TPO_OPE     --> Tipo de operación
   -- Output:
   -- MENSAJE_SWF       --> Mensaje de 298
   -- p_ERROR           --> Indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_MENSAJE_MOTIVO_SWIFT (p_NUM_OPE       IN     NUMBER,
                                          p_FECHA_INS     IN     DATE,
                                          p_COD_TPO_OPE   IN     CHAR,
                                          MENSAJE_SWF        OUT VARCHAR2,
                                          p_ERROR            OUT NUMBER);

   --**********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_MNSJE_PRTCZ_HIST
   -- Objetivo: Funcion para realizar busqueda de mensajerias de protocolo
   -- 799 / 199 / 298 / 198
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_TIPO_OPRCN, PABS_DT_DETLL_MNSJE_PRTCZ_HTRC, PABS_DT_MNSJE_PRTCZ_HTRCA, PABS_DT_DETLL_OPRCN_HTRCA
   -- Fecha: 31/03/17
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_MSJ_PRZ     -> Folio mensaje
   -- p_FEC_ISR_MSJ_PRZ     -> Fecha inserción
   -- p_FEC_CARGA           -> Fecha carga
   -- p_ORIGEN              -> Origen de búsqueda de mensaje
   -- Output:
   -- p_CURSOR              -> Lista los mensajes de protocolo
   -- p_ERROR               -> Indicador de errores (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************--
   PROCEDURE SP_PAB_MNSJE_PRTCZ_HIST (
      p_NUM_FOL_MSJ_PRZ   IN     NUMBER,
      p_FEC_ISR_MSJ_PRZ   IN     DATE,
      p_FEC_CARGA         IN     DATE,
      p_ORIGEN            IN     CHAR,
      p_CURSOR               OUT SYS_REFCURSOR,
      p_ERROR                OUT NUMBER
   );

   --**********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_MNSJE_PRTCZ
   -- Objetivo: Funcion para realizar busqueda de mensajerias de protocolo
   -- 799 / 199 / 299 / 298 / 198
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_TIPO_OPRCN, PABS_DT_DETLL_MNSJE_PRTCZ, PABS_DT_MNSJE_PRTCZ, PABS_DT_DETLL_OPRCN
   -- Fecha: 31/03/17
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_MSJ_PRZ  -> Numero folio mensaje
   -- p_FEC_ISR_MSJ_PRZ  -> Fecha de inserción
   -- Output:
   -- p_CURSOR          ->  Lista de mensaje de protocolo
   -- p_ERROR           -> Indicador de errores (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_PAB_MNSJE_PRTCZ (p_NUM_FOL_MSJ_PRZ   IN     NUMBER,
                                 p_FEC_ISR_MSJ_PRZ   IN     DATE,
                                 p_CURSOR               OUT SYS_REFCURSOR,
                                 p_ERROR                OUT NUMBER);


   --***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_COMBO_MOTIVO_SWIFT
   -- Objetivo: PROCEDIMIENTO ALMACENADO QUE DEVUELVE MOTIVOS ASOCIADOS A UN MENSAJE.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_TIPO_OPRCN
   -- Fecha: 29/12/16
   -- Autor: Santander
   -- Input:
   -- N/A
   -- Output:
   -- p_CURSOR          -> Lista de motivos para mensajes
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_COMBO_MOTIVO_SWIFT (p_CURSOR OUT SYS_REFCURSOR);


   --**********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_BUS_OPE_SWF
   -- Objetivo: Procedimiento que realiza búsqueda de operaciones Swift para asociar a mensajería de protocolo
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN
   -- Fecha: 03/01/2017
   -- Autor: Santander
   -- Input:
   -- p_STR_FLG_MRD       -> Flag
   -- p_COD_BCO_DTN       -> Bic banco destino
   -- p_NUM_REF_SWF       -> Referencia mensaje
   -- p_FEC_VTA           -> Fecha valuta
   -- p_IMp_OPE           -> Monto
   -- p_FLG_EGR_ING       -> Flag egreso ingreso
   -- p_COD_EST_AOS       -> Estado
   -- Output:
   -- p_CURSOR_DETLL_OPE  -> Devuelve operaciones para relicionar a un mensaje 298
   -- p_ERROR -> Indicador de errores (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_PAB_BUS_OPE_SWF (p_STR_FLG_MRD        IN     NUMBER,
                                 p_COD_BCO_DTN        IN     CHAR,
                                 p_COD_BCO_BNF        IN     CHAR,
                                 p_NUM_REF_SWF        IN     CHAR,
                                 p_FEC_VTA            IN     DATE,
                                 p_IMp_OPE            IN     NUMBER,
                                 p_FLG_EGR_ING        IN     NUMBER,
                                 p_COD_EST_AOS        IN     NUMBER,
                                 p_CURSOR_DETLL_OPE      OUT SYS_REFCURSOR,
                                 p_ERROR                 OUT NUMBER);


   --******************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_INS_MSN_PRTCZ
   -- Objetivo: Procedimiento que inserta menjase de protocolo
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_MNSJE_PRTCZ
   -- Fecha: 04/01/17
   -- Autor: Santander
   -- Input:
   --p_NUM_SUB_MSJ_RLD   -> Numero de submenaje
   --p_NUM_REF_SWF_RLD   -> referencia relacionada
   --p_NUM_REF_SWF       -> referencia mensaje
   --p_NUM_FOL_MSJ_PRZ   -> Folio mensaje
   --p_HOR_ENV_RCP_SWF   -> Hora swift
   --p_GLS_EST_RCH       -> Glosa rechazo
   --p_FLG_MRD_UTZ_SWF   -> Flag de divisa
   --p_FLG_EGR_ING       -> Falg egreso ingreso
   --p_FEC_ISR_MSJ_PRZ   -> Fecha inserción mensaje
   --p_FEC_ENV_RCP_SWF   -> Fecha swift
   --p_COD_MT_SWF        -> Tipo de memsaje
   --p_COD_EST_AOS       -> Estado de mensaje
   --p_COD_BCO_BFC       -> Bic beneficiario
   --p_COD_ADC_EST       -> Codigo estado adicional
   --p_NUM_FOL_OPE_ORI   -> Folio operación original
   --p_COD_TPO_OPE       -> Tipo operación
   -- Output:
   -- p_ERROR            -> Indicador de errores (0 Sin errores :: 1 existen errores). Se ven en la tabla Log estos errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_INS_MSN_PRTCZ (p_NUM_SUB_MSJ_RLD   IN     CHAR,
                                   p_NUM_REF_SWF_RLD   IN     CHAR,
                                   p_NUM_REF_SWF       IN     CHAR,
                                   p_NUM_FOL_MSJ_PRZ   IN     NUMBER,
                                   p_HOR_ENV_RCP_SWF   IN     NUMBER,
                                   p_GLS_EST_RCH       IN     VARCHAR2,
                                   p_FLG_MRD_UTZ_SWF   IN     NUMBER,
                                   p_FLG_EGR_ING       IN     NUMBER,
                                   p_FEC_ISR_MSJ_PRZ   IN     DATE,
                                   p_FEC_ENV_RCP_SWF   IN     DATE,
                                   p_COD_MT_SWF        IN     CHAR,
                                   p_COD_EST_AOS       IN     NUMBER,
                                   p_COD_BCO_BFC       IN     CHAR,
                                   p_COD_ADC_EST       IN     CHAR,
                                   p_NUM_FOL_OPE_ORI   IN     NUMBER,
                                   p_FEC_ISR_OPE_ORI   IN     DATE,
                                   p_COD_TPO_OPE       IN     CHAR,
                                   p_ERROR                OUT NUMBER);

   --******************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_INS_DETLL_MSN_PRTCZ
   -- Objetivo: Procedimiento que inserta detalle del menjase de protocolo
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_MNSJE_PRTCZ
   -- Fecha: 04/01/17
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_MSJ_PRZ     -> Folio mensaje
   -- p_FEC_ISR_MSJ_PRZ     -> Fecha inserción
   -- p_GLS_TXT_LBE_MSJ     -> Detalle mensaje
   -- Output:
   -- p_ERROR               -> Indicador de errores (0 Sin errores :: 1 existen errores). Se ven en la tabla Log estos errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_INS_DETLL_MSN_PRTCZ (p_NUM_FOL_MSJ_PRZ   IN     NUMBER,
                                         p_FEC_ISR_MSJ_PRZ   IN     DATE,
                                         p_GLS_TXT_LBE_MSJ   IN     VARCHAR2,
                                         p_ERROR                OUT NUMBER);


   /********************************************************************************************************
    FUNCION/PROCEDIMIENTO: Sp_PAB_COMBO_MT_SWIFT_PRO
    OBJETIVO             : MUESTRA FILTRO EN
                            MENSAJES -> ENVIADO - PROTOCOLO -> CREAR.
                            MENSAJES -> ENVIADO - PROTOCOLO -> CURSAR.
                            MENSAJES -> ENVIADO - PROTOCOLO -> AUTORIZAR.
    SISTEMA              : DBO_PAB.
    BASE DE DATOS        : DBO_PAB.
    TABLAS USADAS        : PABS_DT_TIPO_MT_SWIFT.
    FECHA                : 27/12/16
    AUTOR                :
    INPUT                : P_CANAL  := CANAL.
    OUTPUT               : P_CURSOR := DATOS CURSOR.
    OBSERVACIONES
    FECHA       USUARIO    DETALLE
    20-09-2018  VARAYA     SE MODIFICA FILTRO.
    ********************************************************************************************************/
   PROCEDURE Sp_PAB_COMBO_MT_SWIFT_PRO (p_CURSOR OUT SYS_REFCURSOR);

   /********************************************************************************************************
   FUNCION/PROCEDIMIENTO: PKG_PAB_PROTOCOLO.SP_PAB_COMBO_AVISOS_PROT.
   OBJETIVO             : MUESTRA FILTRO EN
                           MENSAJES -> ENVIADO - PROTOCOLO -> CREAR.
                           MENSAJES -> ENVIADO - PROTOCOLO -> CURSAR.
                           MENSAJES -> ENVIADO - PROTOCOLO -> AUTORIZAR.
                           CONTINGENCIA -> ENVIADO -> COMBANC UPLOAD - DESCARGAR ARCHIVO.
   SISTEMA              : DBO_PAB.
   BASE DE DATOS        : DBO_PAB.
   TABLAS USADAS        : PABS_DT_TIPO_MT_SWIFT.
   FECHA                : 20/09/2018.
   AUTOR                : VARAYA.
   INPUT                : P_CANAL  := CANAL.
   OUTPUT               : P_CURSOR := DATOS CURSOR.
   OBSERVACIONES
   FECHA       USUARIO    DETALLE
   20-09-2018  VARAYA     CREACIÓN.
   ********************************************************************************************************/
   PROCEDURE SP_PAB_COMBO_AVISOS_PROT (P_CURSOR OUT SYS_REFCURSOR);

   --**********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_BUS_MNSJE_PRTCZ_HIST
   -- Objetivo: Función para realizar búsqueda de mensajerias histórica de protocolo
   -- 799 / 199 / 299 / 298 / 198
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_MNSJE_PRTCZ_HTRCA, PABS_DT_ESTDO_ALMOT, PABS_DT_MNSJE_PRTCZ
   -- Fecha: 30/11/16
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_MSJ_PRZ          Folio mensaje
   -- p_COD_EST_AOS              Estado mensaje
   -- p_NUM_REF_SWF              Ref mensaje
   -- p_COD_DVI                  Mercado
   -- p_COD_MT_SWF               Tipo mensaje
   -- p_COD_BCO_DTN              Bic banco
   -- p_FLG_EGR_ING              Flujo
   -- p_FEC_DESDE                Fecha rango desde
   -- p_FEC_HASTA                Fecha rango hasta
   -- Output:
   -- p_CURSOR                    Lista de Mensajes de protocolo
   -- p_ERROR                    Indicador de errores (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_PAB_BUS_MNSJE_PRTCZ_HIST (
      p_NUM_FOL_MSJ_PRZ   IN     NUMBER,
      p_COD_EST_AOS       IN     NUMBER,
      p_NUM_REF_SWF       IN     CHAR,
      p_COD_DVI           IN     CHAR,
      p_COD_MT_SWF        IN     NUMBER,
      p_COD_BCO_DTN       IN     CHAR,
      p_COD_BCO_ORI       IN     CHAR,
      p_FLG_EGR_ING       IN     NUMBER,
      p_FEC_DESDE         IN     DATE,
      p_FEC_HASTA         IN     DATE,
      p_CURSOR               OUT SYS_REFCURSOR,
      p_ERROR                OUT NUMBER
   );

   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_GES_MSJ_PROT
   -- Objetivo: Gestiona la inserción de mensaje de protocolo.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN
   -- Fecha: 16/03/2017
   -- Autor: Santander
   -- p_NUM_SUB_MSJ_RLD  -> Número submensaje
   -- p_NUM_REF_SWF_RLD  -> Referencia relacionada
   -- p_NUM_REF_SWF      -> Referencia de mensaje
   -- p_HOR_ENV_RCP_SWF  -> Hora swift
   -- p_GLS_EST_RCH      -> Glosa rechazo
   -- p_FLG_MRD_UTZ_SWF  -> Flag de mercado
   -- p_FLG_EGR_ING      -> Flag flujo
   -- p_FEC_ENV_RCP_SWF  -> Fecha wsift
   -- p_COD_MT_SWF       -> Tipo de mensaje
   -- p_COD_EST_AOS      -> Estado mensaje
   -- p_COD_BCO_BFC      -> Bic Beneficiario
   -- p_COD_ADC_EST      -> Estado adicional
   -- p_NUM_FOL_OPE_ORI  -> Número de operación
   -- p_FEC_OPE_ORI      -> Número de operación
   -- p_COD_TPO_OPE      -> Tipo de operación
   -- p_NUM_CAM_REG      -> Número campo por registro
   -- p_GLS_TXT_LBE_MSJ  -> Glosa texto libre
   -- p_COD_USR          -> Código usuario
   -- Output:
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- p_NUM_FOL_MSJ_PRZ  -> Folio mensaje
   -- p_FEC_ISR_MSJ_PRZ  -> fecha mensaje
   -- Retorno: N/A.
   --***********************************************************************************************
   --
   PROCEDURE Sp_PAB_GES_MSJ_PROT (p_NUM_SUB_MSJ_RLD   IN     CHAR,
                                  p_NUM_REF_SWF_RLD   IN     CHAR,
                                  p_NUM_REF_SWF       IN     CHAR,
                                  p_HOR_ENV_RCP_SWF   IN     NUMBER,
                                  p_GLS_EST_RCH       IN     VARCHAR2,
                                  p_FLG_MRD_UTZ_SWF   IN     NUMBER,
                                  p_FLG_EGR_ING       IN     NUMBER,
                                  p_FEC_ENV_RCP_SWF   IN     DATE,
                                  p_COD_MT_SWF        IN     CHAR,
                                  p_COD_EST_AOS       IN     VARCHAR2,
                                  p_COD_BCO_BFC       IN     CHAR,
                                  p_COD_ADC_EST       IN     CHAR,
                                  p_NUM_FOL_OPE_ORI   IN     NUMBER,
                                  p_FEC_OPE_ORI       IN     DATE,
                                  p_COD_TPO_OPE       IN     CHAR,
                                  p_NUM_CAM_REG       IN     NUMBER,
                                  p_GLS_TXT_LBE_MSJ   IN     VARCHAR2,
                                  p_COD_USR           IN     VARCHAR2,
                                  p_NUM_FOL_MSJ_PRZ      OUT NUMBER,
                                  p_FEC_ISR_MSJ_PRZ      OUT VARCHAR2,
                                  p_ERROR                OUT NUMBER);

   /************************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_GEN_REF_SWF_PRT
   -- Objetivo: Fusion que retorne el codifo de referencia Swift para envío  mensajeria de protocolo
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: N/A
   -- Fecha: 02/06/16
   -- Autor: Santander
   -- Input:      p_COD_TPO_OPE:      Tipo de mensaje de protocolo
   -- Output:
   --          Retorna el codigo de referencia Swift Protocolo
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   FUNCTION FN_PAB_GEN_REF_SWF_PRT (p_COD_TPO_OPE IN CHAR)
      RETURN VARCHAR2;


   --******************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ACT_MSN_PRT_SWF
   -- Objetivo: Procedimiento que actualiza mensaje de protocolo tras envio swift
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_MNSJE_PRTCZ,
   -- Fecha: 31/03/17
   -- Autor: Santander
   -- Input:
   --p_NUM_FOL_MSJ_PRZ    --> Numero foliador mensajeria protocolo
   --p_FEC_ISR_MSJ_PRZ    --> Fecha insercion mensajeria protocolo
   --p_NUM_REF_SWF        --> Numero de referencia
   --p_COD_EST_AOS        -->  Código estado
   --p_FEC_ENV_RCP_SWF    -->  Fecha recepción swift
   --p_HOR_ENV_RCP_SWF    -->  Hora recepción swift
   --p_FLG_ENV_RCP        -->  Flag envío (0) o recpeción (1)
   --p_MSJ_EST_RCH        -->  Mensaje rechazo u observación
   --p_COD_USR            -->  Rut usuario
   -- Output:
   -- p_ERROR             --> Indicador de errores (0 Sin errores :: 1 existen errores). Se ven en la tabla Log estos errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   --***********************************************************************************************

   PROCEDURE Sp_PAB_ACT_MSN_PRT_SWF (p_NUM_FOL_MSJ_PRZ   IN     NUMBER,
                                     p_FEC_ISR_MSJ_PRZ   IN     DATE,
                                     p_NUM_REF_SWF       IN     CHAR,
                                     p_COD_EST_AOS       IN     VARCHAR2,
                                     p_FEC_ENV_RCP_SWF   IN     DATE,
                                     p_HOR_ENV_RCP_SWF   IN     NUMBER,
                                     p_FLG_ENV_RCP       IN     NUMBER,
                                     p_MSJ_EST_RCH       IN     VARCHAR2,
                                     p_COD_USR           IN     VARCHAR2,
                                     p_ERROR                OUT NUMBER);

   --**********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_BUS_MNSJE_ING
   -- Objetivo: Procedimiento que retorna los mt298 de ingreso
   --
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_MNSJE_PRTCZ, PABS_DT_DETLL_MNSJE_PRTCZ, PABS_DT_ESTDO_ALMOT
   -- Fecha: 12-01-2018
   -- Autor: Santander- CAH
   -- Input:
   -- p_NUM_REF_SWF         -->  Referencia de mensaje.
   -- p_COD_BCO_DTN         -->  Código banco origen.
   -- p_FEC_DESDE           -->  Fecha incio búsqueda.
   -- p_FEC_HASTA           -->  Fecha fin búsqueda.
   -- Output:
   -- p_CURSOR ->  Lista los mensajes de protocolo
   -- p_ERROR -> Indicada si el procedimiento
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_PAB_BUS_MNSJE_ING (p_NUM_REF_SWF   IN     CHAR,
                                   p_COD_BCO_ORI   IN     CHAR,
                                   p_FEC_DESDE     IN     DATE,
                                   p_FEC_HASTA     IN     DATE,
                                   p_CURSOR           OUT SYS_REFCURSOR,
                                   p_ERROR            OUT NUMBER);

   --**********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_BUS_MNSJE_PRTCZ_HIST2
   -- Objetivo: Función para realizar búsqueda de mensajerias histórica de protocolo
   -- 799 / 199 / 299 / 298 / 198
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_MNSJE_PRTCZ_HTRCA, PABS_DT_ESTDO_ALMOT, PABS_DT_MNSJE_PRTCZ
   -- Fecha: 30/11/16
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_MSJ_PRZ          Folio mensaje
   -- p_COD_EST_AOS              Estado mensaje
   -- p_NUM_REF_SWF              Ref mensaje
   -- p_COD_DVI                  Mercado
   -- p_COD_MT_SWF               Tipo mensaje
   -- p_COD_BCO_DTN              Bic banco
   -- p_FLG_EGR_ING              Flujo
   -- p_FEC_DESDE                Fecha rango desde
   -- p_FEC_HASTA                Fecha rango hasta
   -- Output:
   -- p_CURSOR                    Lista de Mensajes de protocolo
   -- p_ERROR                    Indicador de errores (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_PAB_BUS_MNSJE_910_900_HIST (p_FOL_OPE   IN     NUMBER,
                                            p_FEC_OPE   IN     DATE,
                                            p_CURSOR       OUT SYS_REFCURSOR,
                                            p_ERROR        OUT NUMBER);

   /***********************************************************************************************
     FUNCION/PROCEDIMIENTO: SP_PAB_ACT_COD_ENT_SAL.
     OBJETIVO             : RECORRE LOS MT 900/910 SELECCIONADOS PARA CAMBIAR EL CANAL DE ENTRADA
                            O SALIDA Y LOS ACTUALIZA.
     SISTEMA              : DBO_PAB.
     BASE DE DATOS        : DBO_PAB.
     TABLAS USADAS        : PABS_DT_DETLL_CARGO_ABONO_CAJA.
     FECHA                : 28/08/2018.
     AUTOR                : VARAYA.
     INPUT                : P_REG_OPER    := 1° REGISTRO := FECHA.
                                             2° REGISTRO := NÚMERO FOLIO.
                                             3° REGISTRO := CÓDIGO MT.
                            P_COD_ENT_SAL := CANAL SELECCIONADO.
                            P_COD_USR     := USUARIO GESTOR.
     OUTPUT               : P_ERROR       := INDICADOR DE ERRORES:
                                             0 := SIN ERRORES.
                                             1 := CON ERRORES.
     OBSERVACIONES        : 28-08-2018, CREACIÓN SP.
   ***********************************************************************************************/
   PROCEDURE SP_PAB_ACT_COD_ENT_SAL (P_REG_OPER      IN     REG_OPER,
                                     P_COD_ENT_SAL   IN     CHAR,
                                     P_COD_USR       IN     CHAR,
                                     P_ERROR            OUT NUMBER);

   /***********************************************************************************************
     FUNCION/PROCEDIMIENTO: SP_PAB_BUS_CAMBIAR_CANAL.
     OBJETIVO             : BUSCA LOS MENSAJES DE PROTOCOLO QUE NO
     SISTEMA              : DBO_PAB.
     BASE DE DATOS        : DBO_PAB.
     TABLAS USADAS        : PABS_DT_DETLL_CARGO_ABONO_CAJA
     FECHA                : 28/08/2018.
     AUTOR                : SANTANDER
     INPUT                : p_COD_MT_SWF -> Codigo MT
                          : p_NUM_REF_SWF -> Numero de referencia Swift
                          : p_COD_TPO_OPE_AOS -> Tipo operación
                          : P_COD_SIS_SAL -> Codigo sistema salida
                          : P_COD_SIS_ENT -> Codigo sistema entrada
                          : p_COD_BCO_ORG -> Bic Banco Origen
     OUTPUT               : p_CURSOR     -> Cursor con operaciones de protocolo que cumplen los filtros
                          : P_ERROR       := INDICADOR DE ERRORES:
                                             0 := SIN ERRORES.
                                             1 := CON ERRORES.
     OBSERVACIONES        : 28-08-2018, CREACIÓN SP.
   ***********************************************************************************************/
   PROCEDURE SP_PAB_BUS_CAMBIAR_CANAL (
      p_COD_MT_SWF        IN     NUMBER,
      p_NUM_REF_SWF       IN     CHAR,
      p_COD_TPO_OPE_AOS   IN     CHAR,
      P_COD_SIS_SAL       IN     CHAR,
      P_COD_SIS_ENT       IN     CHAR,
      p_COD_BCO_ORG       IN     CHAR,
      p_CURSOR               OUT SYS_REFCURSOR,
      p_ERROR                OUT NUMBER
   );

   /********************************************************************************************************
   FUNCION/PROCEDIMIENTO: SP_PAB_CMB_CNLENT_CMBCNLSAL
   OBJETIVO             : Combo Canal entrada de protocolos de la pantalla cambio canal de salida
   SISTEMA              : DBO_PAB.
   BASE DE DATOS        : DBO_PAB.
   TABLAS USADAS        : PABS_DT_SISTM_ENTRD_SALID
   FECHA                : 02/06/17
   AUTOR                : SANTANDER.
   INPUT                : N/A
   OUTPUT               : P_CURSOR := DATOS CURSOR.
   OBSERVACIONES
   FECHA       USUARIO    DETALLE
   ********************************************************************************************************/
   PROCEDURE SP_PAB_CMB_CNLENT_CMBCNLSAL (P_CURSOR OUT SYS_REFCURSOR);

   /********************************************************************************************************
   FUNCION/PROCEDIMIENTO: SP_PAB_CMB_CNLSAL_CMBCNLSAL
   OBJETIVO             : Combo Canal salida de protocolos de la pantalla cambio canal de salida
   SISTEMA              : DBO_PAB.
   BASE DE DATOS        : DBO_PAB.
   TABLAS USADAS        : PABS_DT_SISTM_ENTRD_SALID
   FECHA                : 02/06/17
   AUTOR                : SANTANDER.
   INPUT                : N/A
   OUTPUT               : P_CURSOR := DATOS CURSOR.
   OBSERVACIONES
   FECHA       USUARIO    DETALLE
   ********************************************************************************************************/
   PROCEDURE SP_PAB_CMB_CNLSAL_CMBCNLSAL (P_CURSOR OUT SYS_REFCURSOR);

   /********************************************************************************************************
   FUNCION/PROCEDIMIENTO: SP_PAB_CMB_CAMBIO_CANAL
   OBJETIVO             : Combo Canal salida de protocolos de la pantalla cambio canal de salida
   SISTEMA              : DBO_PAB.
   BASE DE DATOS        : DBO_PAB.
   TABLAS USADAS        : PABS_DT_SISTM_ENTRD_SALID
   FECHA                : 02/06/17
   AUTOR                : SANTANDER.
   INPUT                : N/A
   OUTPUT               : P_CURSOR := DATOS CURSOR.
   OBSERVACIONES
   FECHA       USUARIO    DETALLE
   ********************************************************************************************************/
   PROCEDURE SP_PAB_CMB_CAMBIO_CANAL (P_CURSOR OUT SYS_REFCURSOR);

   /********************************************************************************************************
   FUNCION/PROCEDIMIENTO: PKG_PAB_PROTOCOLO.SP_PAB_COMBO_TODOS_PROT.
   OBJETIVO             : MUESTRA FILTRO EN
                           MENSAJES -> ENVIADO - PROTOCOLO -> CREAR.
                           MENSAJES -> ENVIADO - PROTOCOLO -> CURSAR.
                           MENSAJES -> ENVIADO - PROTOCOLO -> AUTORIZAR.
                           CONTINGENCIA -> ENVIADO -> COMBANC UPLOAD - DESCARGAR ARCHIVO.
   SISTEMA              : DBO_PAB.
   BASE DE DATOS        : DBO_PAB.
   TABLAS USADAS        : PABS_DT_TIPO_MT_SWIFT.
   FECHA                : 20/09/2018.
   AUTOR                : VARAYA.
   INPUT                : P_CANAL  := CANAL.
   OUTPUT               : P_CURSOR := DATOS CURSOR.
   OBSERVACIONES
   FECHA       USUARIO    DETALLE
   20-09-2018  VARAYA     CREACIÓN.
   ********************************************************************************************************/
   PROCEDURE SP_PAB_COMBO_TODOS_PROT (P_CURSOR OUT SYS_REFCURSOR);
END PKG_PAB_PROTOCOLO;