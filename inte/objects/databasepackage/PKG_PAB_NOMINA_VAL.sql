CREATE OR REPLACE PACKAGE         PKG_PAB_NOMINA_VAL
AS
   /**************************VARIABLE GLOBALES************************************/
   v_NOM_PCK     VARCHAR2 (30) := 'PKG_PAB_NOMINA_VAL';
   v_NOM_SP      VARCHAR2 (30);
   ERR_CODE      NUMBER := 0;
   ERR_MSG       VARCHAR2 (300);
   v_DES         VARCHAR2 (300);
   v_DES_BIT     VARCHAR2 (100);
   v_COD_USU     CHAR (11);
   p_s_mensaje   VARCHAR2 (400);

   /**************************FIN VARIABLE GLOBALES********************************/

   -- Funcion/Procedimiento: SP_PAB_ELI_MOT_NO_HABLT
   -- Objetivo: Procedimiento que elimina registro de errores encontrados por las validaciones de integracion
   -- en las n?minas
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_MOTVO_NO_HABLT
   -- Fecha: 19/08/16
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_NMN -> Numero de nomina
   -- Output:
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_PAB_ELI_MOT_NO_HABLT (p_NUM_FOL_NMN   IN     NUMBER,
                                      p_ERROR            OUT NUMBER);

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ACT_EST_OPE_NOMINA
   -- Objetivo: Procedimiento que actualiza el estado de una operacion de una nomina.
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_OPE -> foliador del banco, PK tabla operaciones nomina.
   -- p_FEC_ISR_OPE -> Fecha de insercion de operacion en el sistema, PK tabla operaciones n?mina.
   -- p_COD_EST_AOS -> Identificador de nuevo estado.
   -- p_GLS_ADC_EST -> motivo o glosa de la actualizacion
   -- Output:
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_ACT_EST_OPE_NOMINA (p_NUM_FOL_OPE   IN     NUMBER,
                                        p_FEC_ISR_OPE   IN     DATE,
                                        p_COD_EST_AOS   IN     NUMBER,
                                        p_GLS_ADC_EST   IN     VARCHAR2,
                                        p_ERROR            OUT NUMBER);

   --**********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_INS_ERR_VAL
   -- Objetivo: Procedimiento que registra los errores encontrados por las validaciones de integraci?n
   -- en las n?minas
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_MOTVO_NO_HABLT
   -- Fecha: 19/08/16
   -- Autor: Santander
   -- Input:
   -- p_FEC_ISR_OPE -> Fecha de una Operacion
   -- p_NUM_FOL_OPE -> Foliador del banco
   -- p_COD_ARC_ENT -> Codigo Archivo de Entrada
   -- p_COD_CAM_ARC -> Numero de campo del archivo
   -- p_COD_ERR_BIB -> Codifgo del error
   -- p_NUM_FOL_NMN -> Numero de folio de la nomina
   -- Output:
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_PAB_INS_ERR_VAL (p_FEC_ISR_OPE   IN     DATE,
                                 p_NUM_FOL_OPE   IN     NUMBER,
                                 p_COD_ARC_ENT   IN     CHAR,
                                 p_COD_CAM_ARC   IN     NUMBER,
                                 p_COD_ERR_BIB   IN     CHAR,
                                 p_NUM_FOL_NMN   IN     NUMBER,
                                 p_ERROR            OUT NUMBER);

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_GES_EST_NOM_REC_INV
   -- Objetivo: Gestiona el cambio de estado de una nomina despues de su validacion
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_CBCRA_NOMNA
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_NOM -> foliador de la nomina
   -- Output:
   -- P_STR_EST_AOS -> string de estado de la operacion
   -- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_GES_EST_NOM_REC_INV (p_NUM_FOL_NOM   IN     NUMBER,
                                         P_STR_EST_AOS      OUT CHAR,
                                         p_ERROR            OUT NUMBER);

   --**********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_VAL_RUT
   -- Objetivo: Procedimiento que valida RUT de acuerdo a su digito verificador
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: N/A
   -- Fecha: 19/08/16
   -- Autor: Santander
   -- Input:
   -- p_NUM_RUT -> Numero de RUT
   -- p_DIG_VERIF -> Digito Verificador
   -- Output:
   -- p_FLAG_VAL -> Flag cumple con las condiciones de hora y monto importe maximo -> 0 cumple y 1 no cumple
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_VAL_RUT (p_NUM_RUT     IN     NUMBER,
                             p_DIG_VERIF   IN     CHAR,
                             p_FLAG_VAL       OUT NUMBER,
                             p_ERROR          OUT NUMBER);

   --**********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUS_VAL_ARC
   -- Objetivo: Procedimiento que consultara la tabla donde se almacenaran las distintas relaciones
   -- que existen entre los tipos de mensajes, tipo de operaciones y  sistema de salida.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: N/A
   -- Fecha: 14/09/2016
   -- Autor: Santander
   -- Input:
   -- p_COD_ARC_ENT -> Codigo de archivo de Entrada
   -- Output:
   -- p_CURSOR  -> Cursor con todas las operaciones
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_BUS_VAL_ARC (p_COD_ARC_ENT   IN     CHAR,
                                 p_CURSOR           OUT SYS_REFCURSOR,
                                 p_ERROR            OUT NUMBER);

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_OBT_VAL_NOMINA
   -- Objetivo: Procedimiento que devuelve las validaciones de una nomina
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_BIBLI_ERROR_ALMOT
   -- Fecha: 24/02/17
   -- Autor: Santander
   -- Input:
   -- Output:
   -- p_validaciones-> Cursor con las validaciones
   -- p_ERROR              -> Indicador codigo error
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_OBT_VAL_NOMINA (p_validaciones   OUT SYS_REFCURSOR,
                                    p_ERROR          OUT NUMBER);

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_VAL_FEC
   -- Objetivo: Procedimiento que realiza validaciones segun el parametro de entrada
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_BIBLI_ERROR_ALMOT
   -- Fecha: 24/02/17
   -- Autor: Santander
   -- Input:
   -- p_tipoVal --> Tipo de validacion (1 - Mayor que fecha sistema)
   -- p_fecha   --> Fecha a Validar
   -- Output:
   -- p_ERROR              -> Indicador codigo error
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_VAL_FEC (p_tipoVal   IN     NUMBER,
                             p_fecha     IN     DATE,
                             p_ERROR        OUT NUMBER);


   -- **********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUS_CAM_OBT_SWF
   -- Objetivo: Procedimiento que ingresa operaciones del DCV automaticos
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_BIBLI_CAMPO_OBLTO_MT
   --
   -- Fecha: 16/06/17
   -- Autor: SII GROUP
   -- Input:
   -- p_COD_MT_SWF         -> Codigo MT a validar
   -- p_FLG_MRD_UTZ_SWF    -> flag de mercado del mensaje a validar
   -- p_COD_TPO_OPE_AOS    -> codigo operacion a validar
   -- Output:
   -- p_CAM_OBT            -> Retorna campos a validar.
   -- p_ERROR              -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_BUS_CAM_OBT_SWF (
      p_COD_MT_SWF        IN     NUMBER,
      p_FLG_MRD_UTZ_SWF   IN     NUMBER,
      p_COD_TPO_OPE_AOS   IN     VARCHAR2,
      p_CAM_OBT              OUT SYS_REFCURSOR,
      p_ERROR                OUT NUMBER
   );

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUS_MSJ_OBT_SWF
   -- Objetivo: Procedimiento que busca los mensajes a validar
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_BIBLI_CAMPO_OBLTO_MT
   --
   -- Fecha: 16/06/17
   -- Autor: SII GROUP
   -- Input: N/A
   -- Output:
   -- p_MSJ_OBT            -> Cursor de retorno de mensajes a validar.
   -- p_ERROR              -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_BUS_MSJ_OBT_SWF (p_MSJ_OBT   OUT SYS_REFCURSOR,
                                     p_ERROR     OUT NUMBER);
END PKG_PAB_NOMINA_VAL;