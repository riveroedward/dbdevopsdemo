CREATE OR REPLACE PACKAGE BODY         PKG_PAB_CONTABILIDAD
IS
   /**  Package el cual contrendra todas las funciones y procedimientos relacionados con la contabilidad
   -- @Santander */
   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_CONTABILIDAD
   -- Objetivo: Genera la interfaz contable del sistema Altos Montos, Parte 1 solo pagos enviados
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_CNTBL_ALMOT,PABS_DT_DETLL_OPRCN,PABS_DT_TIPO_OPRCN_SALID_MNSJ,PABS_DT_CENTA_CNTBL,PABS_DT_SISTM_ENTRD_SALID
   -- Fecha: 13/06/2016
   -- Autor: Santander
   -- Input: p_FEC_PCS -> Fecha Proceso .
   -- Output: p_CURSOR -> PABS_DT_CNTBL_ALMOT, objeto del tipo tabla.
   --        p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: 04/08/2016, Se Modifica comentario.
   --***********************************************************************************************
   PROCEDURE Sp_PAB_CONTABILIDAD (p_FEC_PCS   IN     VARCHAR2,
                                  p_CURSOR       OUT SYS_REFCURSOR,
                                  p_ERROR        OUT NUMBER)
   IS
      v_NOM_SP               VARCHAR2 (30);
      v_FEC_DIA              DATE;
      v_FEC_PCS              DATE;
      v_FEC_ALTA             NUMBER;
      v_numCero CONSTANT     NUMBER := 0;
      v_numTres CONSTANT     NUMBER := 3;
      v_unoEsp CONSTANT      CHAR (1) := ' ';
      v_unoCuatro CONSTANT   CHAR (4) := '    ';
      v_cuaCer CONSTANT      CHAR (4) := '0000';
      p_FEC_ICO_PCS          VARCHAR2 (30);
      p_NUM_TCK_PCS          NUMBER;
      p_GLS_DET_PCS          VARCHAR2 (300);
      p_COD_ERR_BIB          CHAR (6);
      p_COD_TPO_PSC          CHAR (3);
   --v_numRef    CONSTANT CHAR(12) := '            ';

   BEGIN
      -- INICIALIZACION DE VARIABLES Y PARAMETROS
      p_ERROR := 0;
      v_NOM_SP := 'Sp_PAB_CONTABILIDAD';
      v_DES := 'Se genera interfaz contable, fecha proceso: ' || p_FEC_PCS;
      v_FEC_DIA := TO_DATE (SYSDATE, 'DD-MM-YYYY');
      v_FEC_PCS := TO_DATE (p_FEC_PCS, 'DD-MM-YYYY');
      v_FEC_ALTA := TO_NUMBER (TO_CHAR (SYSDATE, 'YYYYMMDD'));
      p_COD_ERR_BIB := '0';
      p_COD_TPO_PSC := 'PCT';

      --Contabilidad Traza 1
      p_GLS_DET_PCS := 'Iniciando proceso contabilidad';
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_CAB_PROCESO (p_COD_TPO_PSC,
                                                   p_FEC_ICO_PCS,
                                                   p_NUM_TCK_PCS,
                                                   p_ERROR);
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);

      --Eliminar datos para fecha de proceso solicitada
      DELETE FROM   PABS_DT_CNTBL_ALMOT
            WHERE   FEC_PCS_REG_CTB = v_FEC_PCS;

      --Inicia Logica Genera Contabilidad

      /**************************************************************************/
      ----------------------------Egresos----------------------------------------
      /**************************************************************************/

      --Inserta datos al DEBE Moneda nacional en tabla contabilidad
      INSERT INTO PABS_DT_CNTBL_ALMOT (FEC_ISR_OPE,
                                       NUM_FOL_OPE,
                                       FLG_DEB_HAB,
                                       COD_END,
                                       COD_CLV_ITZ,
                                       FEC_CTB,
                                       FEC_OPE,
                                       COD_PTO,
                                       COD_SPO,
                                       NUM_GRT,
                                       DSC_TPO_PZO,
                                       CAN_PZO,
                                       COD_SUB_SEC,
                                       COD_CNP_SUB_SEC,
                                       NUM_DTN_INV,
                                       CAN_END_TDA,
                                       COD_ABT,
                                       DSC_TPO_TTM_MDA,
                                       DSC_TPO_INV,
                                       COD_OPE,
                                       NUM_CNP_CTA_CTB,
                                       COD_DVI,
                                       DSC_TPO_DVI,
                                       DSC_FLL_UNO,
                                       DSC_VRS_FJO_VAR,
                                       DSC_CLv_ATZ,
                                       COD_SUC_OPE,
                                       COD_SUC_ORG,
                                       COD_SUC_DTN,
                                       NUM_MVT_DEB,
                                       NUM_MVT_HAB,
                                       IMP_MON_BSE_DEB,
                                       IMP_MON_BSE_HAB,
                                       IMP_DVI_DEB,
                                       IMP_DVI_HAB,
                                       FLG_IDC_CCC,
                                       NUM_DOC,
                                       DSC_CLV_CNP,
                                       DSC_CNP,
                                       DSC_TPO_CDA,
                                       GLS_IFM_MVT_CTB,
                                       NUM_OPE_ORG,
                                       COD_SNO_CTB_ORG,
                                       COD_SNO_CTB_DTN,
                                       GLS_OBS_UNO,
                                       DSC_RSV_USO_ITR,
                                       COD_CLV_ITZ_GNR_MVT,
                                       FLG_CPS_CJA,
                                       FLG_CTA_ORD,
                                       DSC_TSA_INT,
                                       COD_CLV_OPE,
                                       COD_CTO_GTN,
                                       COD_APO,
                                       DSC_FLL_TRI,
                                       DSC_IDC_UTL,
                                       DSC_IDC_ROT,
                                       FEC_ALA_PTD,
                                       GLS_OBS_DOS,
                                       NUM_OPE_DTN,
                                       DSC_FLL_DOS,
                                       FEC_PCS_REG_CTB)
         SELECT   OPR.FEC_ISR_OPE AS FEC_ISR_OPE,
                  OPR.NUM_FOL_OPE AS NUM_FOL_OPE,
                  PKG_PAB_CONSTANTES.v_FLG_OPE_DEB AS FLG_DEB_HAB,      --Debe
                  PKG_PAB_CONSTANTES.v_COD_ENT_BAN_SANT AS COD_END,
                  PKG_PAB_CONSTANTES.v_COD_SIS_PAB AS COD_CLV_ITZ,
                  TO_NUMBER (TO_CHAR (OPR.FEC_VTA, 'YYYYMMDD')) AS FEC_CTB,
                  TO_NUMBER (TO_CHAR (OPR.FEC_ISR_OPE, 'YYYYMMDD'))
                     AS FEC_OPE,
                  '00' AS COD_PTO,
                  v_unoCuatro AS COD_SPO,
                  v_numCero AS NUM_GRT,
                  v_unoEsp AS TPO_PZO,
                  v_numCero AS CAN_PZO,
                  v_numCero AS COD_SUB_SEC,
                  v_numCero AS COD_CNP_SUB_SEC,
                  v_numCero AS NUM_DTN_INV,
                  v_unoCuatro AS CAN_END_TDA,
                  v_numCero AS COD_ABT,
                  v_unoEsp AS TPO_TTM_MDA,
                  v_unoEsp AS TPO_INV,
                  '000' AS COD_OPE,
                  DECODE (OPR.COD_TPO_OPE_AOS,
                          PKG_PAB_CONSTANTES.V_STR_TPO_OPE_CDLE,
                          PKG_PAB_CONSTANTES.V_COD_PUE_GAR_OOFF,
                          CTB.NUM_CNP_CTA_CTB)
                     AS NUM_CNP_CTA_CTB,
                  OPR.COD_DVI AS COD_DVI,
                  v_unoEsp AS TPO_DVI,
                  '00000     ' AS FLL_UNO,
                  '                              ' AS VRS_FJO_VAR,
                  '      ' AS CLV_ATZ,
                  PKG_PAB_CONSTANTES.v_COD_SUC_OPERANTE AS COD_SUC_OPE,
                  PKG_PAB_CONSTANTES.V_COD_SUC_ORG AS COD_SUC_ORG, --CTB.COD_SUC_DTN_TPN AS COD_SUC_ORG,
                  CTB.COD_SUC_DTN_TPN AS COD_SUC_DTN, --SDT.COD_SUC_DTN_TPN AS COD_SUC_DTN,
                  1 AS NUM_MVT_DEB,
                  v_numCero AS NUM_MVT_HAB,
                  OPR.IMP_OPE AS IMP_MON_BSE_DEB,
                  v_numCero AS IMP_MON_BSE_HAB,
                  v_numCero AS IMP_DVI_DEB,
                  v_numCero AS IMP_DVI_HAB,
                  v_unoEsp AS FLG_IDC_CCC,
                  '            ' AS NUM_DOC,
                  '   ' AS CLV_CNP,
                  '              ' AS DSC_CNP,
                  v_unoEsp AS TPO_CDA,
                  --CTB.GLS_IFM_MVT_CTB AS GLS_IFM_MVT_CTB,
                  DECODE (OPR.COD_TPO_OPE_AOS,
                          PKG_PAB_CONSTANTES.V_STR_TPO_OPE_CDLE,
                          PKG_PAB_CONSTANTES.V_STR_PUE_GAR_OOFF,
                          CTB.GLS_IFM_MVT_CTB)
                     AS GLS_IFM_MVT_CTB,
                  PKG_PAB_CONSTANTES.v_COD_ENT_BAN_SANT || v_cuaCer
                  ||           --NVL(OPR.NUM_REF_CTB,v_numRef) AS NUM_OPE_ORG,
                    LPAD (
                        DECODE (OPR.COD_SIS_ENT,
                                PKG_PAB_CONSTANTES.V_COD_TIP_CAJA,
                                TRIM (OPR.NUM_OPE_SIS_ENT),
                                PKG_PAB_CONSTANTES.V_COD_TIP_OB,
                                TRIM (OPR.NUM_OPE_SIS_ENT),
                                PKG_PAB_CONSTANTES.V_COD_TIP_ABO_CTA,
                                TRIM (OPR.NUM_CTA_BFC),
                                OPR.NUM_FOL_OPE),
                        12,
                        '0'
                     )
                     AS NUM_OPE_ORG,
                  DECODE (OPR.COD_TPO_OPE_AOS,
                          PKG_PAB_CONSTANTES.V_STR_TPO_OPE_CDLE,
                          PKG_PAB_CONSTANTES.V_STR_COR_PUE_GAR_OOFF,
                          CAO.COD_SNO_CTB)
                     AS COD_SNO_CTB_ORG,
                  --CAO.COD_SNO_CTB AS COD_SNO_CTB_ORG,
                  --CAD.COD_SNO_CTB AS COD_SNO_CTB_DTN,
                  DECODE (OPR.COD_TPO_OPE_AOS,
                          PKG_PAB_CONSTANTES.V_STR_TPO_OPE_DVP,
                          PKG_PAB_CONSTANTES.V_STR_COR_CMN,
                          CAD.COD_SNO_CTB)
                     AS COD_SNO_CTB_DTN,
                  v_unoCuatro AS GLS_OBS_UNO,
                  v_unoCuatro AS RSV_USO_ITR,
                  PKG_PAB_CONSTANTES.v_COD_SIS_PAB || v_unoEsp
                     AS COD_CLV_ITZ_GNR_MVT,
                  v_unoEsp AS FLG_CPS_CJA,
                  v_unoEsp AS FLG_CTA_ORD,
                  v_numCero AS TSA_INT,
                  '   ' AS COD_CLV_OPE,
                  v_unoCuatro AS COD_CTO_GTN,
                  '  ' AS COD_APO,
                  '  ' AS DSC_FLL_TRI,
                  v_unoEsp AS IDC_UTL,
                  '  ' AS IDC_ROT,
                  v_FEC_ALTA AS FEC_ALA_PTD,
                  ' 0000000000000000             ' AS GLS_OBS_DOS,
                  --PKG_PAB_CONSTANTES.v_COD_ENT_BAN_SANT || v_cuaCer || NVL(OPR.NUM_REF_CTB,v_numRef) AS NUM_OPE_DTN,
                  PKG_PAB_CONSTANTES.v_COD_ENT_BAN_SANT || v_cuaCer
                  || LPAD (
                        DECODE (OPR.COD_SIS_ENT,
                                PKG_PAB_CONSTANTES.V_COD_TIP_CAJA,
                                TRIM (OPR.NUM_OPE_SIS_ENT),
                                PKG_PAB_CONSTANTES.V_COD_TIP_OB,
                                TRIM (OPR.NUM_OPE_SIS_ENT),
                                PKG_PAB_CONSTANTES.V_COD_TIP_ABO_CTA,
                                TRIM (OPR.NUM_CTA_BFC),
                                OPR.NUM_FOL_OPE),
                        12,
                        '0'
                     )
                     AS NUM_OPE_DTN,
                  '     ' AS FLL_DOS,
                  v_FEC_PCS AS FEC_PCS_REG_CTB
           FROM                  PABS_DT_DETLL_OPRCN OPR
                              -- se elimina JOIN CAFF
                              JOIN
                                 PABS_DT_TIPO_OPRCN_SALID_MNSJ TSM
                              ON TSM.COD_SIS_ENT_SAL = OPR.COD_SIS_SAL
                                 AND TSM.COD_MT_SWF = OPR.COD_MT_SWF
                                 AND TSM.COD_TPO_OPE_AOS =
                                       OPR.COD_TPO_OPE_AOS
                                 AND TSM.FLG_MRD_UTZ_SWF =
                                       OPR.FLG_MRD_UTZ_SWF
                                 AND TSM.FLG_EGR_ING = OPR.FLG_EGR_ING
                           --Join obtiene cuenta contable y sucursal origen
                           JOIN
                              PABS_DT_CENTA_CNTBL CTB
                           ON CTB.COD_SIS_ENT_SAL = OPR.COD_SIS_ENT
                              AND CTB.FLAG_MRD_UTZ_SWF = OPR.FLG_MRD_UTZ_SWF
                        --Join obtiene sucursal destino
                        JOIN
                           PABS_DT_CENTA_CNTBL SDT
                        ON SDT.COD_SIS_ENT_SAL = OPR.COD_SIS_SAL
                           AND SDT.FLAG_MRD_UTZ_SWF = OPR.FLG_MRD_UTZ_SWF
                     --Join obtiene codigo de aplicacion sistema entrada salida
                     JOIN
                        PABS_DT_SISTM_ENTRD_SALID CAO
                     ON CAO.COD_SIS_ENT_SAL = OPR.COD_SIS_ENT
                  JOIN
                     PABS_DT_SISTM_ENTRD_SALID CAD
                  ON CAD.COD_SIS_ENT_SAL = OPR.COD_SIS_SAL
          WHERE   OPR.COD_EST_AOS IN
                        (PKG_PAB_CONSTANTES.v_COD_EST_AOS_OPEPAG,
                         PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAGCON,
                         PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDEV)
                  AND OPR.FEC_VTA = v_FEC_PCS --AND TRUNC(OPR.FEC_VTA) = v_FEC_PCS
                  AND OPR.FLG_EGR_ING = PKG_PAB_CONSTANTES.v_FLG_EGR
                  AND OPR.FLG_MRD_UTZ_SWF = PKG_PAB_CONSTANTES.v_FLG_MRD_MN
                  AND TSM.FLG_GNR_OPE_CTB =
                        PKG_PAB_CONSTANTES.v_FLG_GNR_OPE_CTB
                  AND CTB.FLG_VGN = PKG_PAB_CONSTANTES.v_REG_VIG;

      --Inserta datos al HABER Moneda nacional en tabla contabilidad
      INSERT INTO PABS_DT_CNTBL_ALMOT (FEC_ISR_OPE,
                                       NUM_FOL_OPE,
                                       FLG_DEB_HAB,
                                       COD_END,
                                       COD_CLv_ITZ,
                                       FEC_CTB,
                                       FEC_OPE,
                                       COD_PTO,
                                       COD_SPO,
                                       NUM_GRT,
                                       DSC_TPO_PZO,
                                       CAN_PZO,
                                       COD_SUB_SEC,
                                       COD_CNp_SUB_SEC,
                                       NUM_DTN_INV,
                                       CAN_END_TDA,
                                       COD_ABT,
                                       DSC_TPO_TTM_MDA,
                                       DSC_TPO_INV,
                                       COD_OPE,
                                       NUM_CNp_CTA_CTB,
                                       COD_DVI,
                                       DSC_TPO_DVI,
                                       DSC_FLL_UNO,
                                       DSC_VRS_FJO_VAR,
                                       DSC_CLv_ATZ,
                                       COD_SUC_OPE,
                                       COD_SUC_ORG,
                                       COD_SUC_DTN,
                                       NUM_MVT_DEB,
                                       NUM_MVT_HAB,
                                       IMp_MON_BSE_DEB,
                                       IMp_MON_BSE_HAB,
                                       IMp_DVI_DEB,
                                       IMp_DVI_HAB,
                                       FLG_IDC_CCC,
                                       NUM_DOC,
                                       DSC_CLv_CNP,
                                       DSC_CNP,
                                       DSC_TPO_CDA,
                                       GLS_IFM_MVT_CTB,
                                       NUM_OPE_ORG,
                                       COD_SNO_CTB_ORG,
                                       COD_SNO_CTB_DTN,
                                       GLS_OBS_UNO,
                                       DSC_RSv_USO_ITR,
                                       COD_CLv_ITZ_GNR_MVT,
                                       FLG_CPS_CJA,
                                       FLG_CTA_ORD,
                                       DSC_TSA_INT,
                                       COD_CLv_OPE,
                                       COD_CTO_GTN,
                                       COD_APO,
                                       DSC_FLL_TRI,
                                       DSC_IDC_UTL,
                                       DSC_IDC_ROT,
                                       FEC_ALA_PTD,
                                       GLS_OBS_DOS,
                                       NUM_OPE_DTN,
                                       DSC_FLL_DOS,
                                       FEC_PCS_REG_CTB)
         SELECT   OPR.FEC_ISR_OPE AS FEC_ISR_OPE,
                  OPR.NUM_FOL_OPE AS NUM_FOL_OPE,
                  PKG_PAB_CONSTANTES.v_FLG_OPE_HAB AS FLG_DEB_HAB,     --Haber
                  PKG_PAB_CONSTANTES.v_COD_ENT_BAN_SANT AS COD_END,
                  PKG_PAB_CONSTANTES.v_COD_SIS_PAB AS COD_CLv_ITZ,
                  TO_NUMBER (TO_CHAR (OPR.FEC_VTA, 'YYYYMMDD')) AS FEC_CTB,
                  TO_NUMBER (TO_CHAR (OPR.FEC_ISR_OPE, 'YYYYMMDD'))
                     AS FEC_OPE,
                  '00' AS COD_PTO,
                  '    ' AS COD_SPO,
                  v_numCero AS NUM_GRT,
                  v_unoEsp AS TPO_PZO,
                  v_numCero AS CAN_PZO,
                  v_numCero AS COD_SUB_SEC,
                  v_numCero AS COD_CNp_SUB_SEC,
                  v_numCero AS NUM_DTN_INV,
                  v_unoCuatro AS CAN_END_TDA,
                  v_numCero AS COD_ABT,
                  v_unoEsp AS TPO_TTM_MDA,
                  v_unoEsp AS TPO_INV,
                  '000' AS COD_OPE,
                  DECODE (
                     OPR.COD_TPO_OPE_AOS,                                --GCP
                     PKG_PAB_CONSTANTES.V_STR_TPO_OPE_DVP,
                     DECODE (COD_EST_AOS,
                             PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDEV,
                             CTB.NUM_CNP_CTA_CTB,
                             PKG_PAB_CONSTANTES.V_COD_PUE_CMN),
                     CTB.NUM_CNP_CTA_CTB
                  )
                     AS NUM_CNP_CTA_CTB,         --CONTABLE QUE LE CORRESPONDE
                  OPR.COD_DVI AS COD_DVI,
                  v_unoEsp AS TPO_DVI,
                  '00000     ' AS FLL_UNO,
                  '                              ' AS VRS_FJO_VAR,
                  '      ' AS CLv_ATZ,
                  PKG_PAB_CONSTANTES.v_COD_SUC_OPERANTE AS COD_SUC_OPE,
                  PKG_PAB_CONSTANTES.V_COD_SUC_ORG AS COD_SUC_ORG, --SOG.COD_SUC_DTN_TPN AS COD_SUC_ORG,
                  CTB.COD_SUC_DTN_TPN AS COD_SUC_DTN,
                  v_numCero AS NUM_MVT_DEB,
                  1 AS NUM_MVT_HAB,
                  v_numCero AS IMp_MON_BSE_DEB,
                  OPR.IMP_OPE AS IMp_MON_BSE_HAB,
                  v_numCero AS IMp_DVI_DEB,
                  v_numCero AS IMp_DVI_HAB,
                  v_unoEsp AS FLG_IDC_CCC,
                  '            ' AS NUM_DOC,
                  '   ' AS CLv_CNP,
                  '              ' AS DSC_CNP,
                  v_unoEsp AS TPO_CDA,
                  DECODE (
                     OPR.COD_TPO_OPE_AOS,                                --GCP
                     PKG_PAB_CONSTANTES.V_STR_TPO_OPE_DVP,
                     DECODE (COD_EST_AOS,
                             PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDEV,
                             CTB.GLS_IFM_MVT_CTB,
                             PKG_PAB_CONSTANTES.V_STR_PUE_CMN),
                     CTB.GLS_IFM_MVT_CTB
                  )
                     AS GLS_IFM_MVT_CTB,
                  PKG_PAB_CONSTANTES.v_COD_ENT_BAN_SANT || v_cuaCer
                  ||           --NVL(OPR.NUM_REF_CTB,v_numRef) AS NUM_OPE_ORG,
                    LPAD (
                        DECODE (
                           OPR.COD_SIS_SAL,
                           PKG_PAB_CONSTANTES.V_COD_TIP_CAJA,
                           TRIM (OPR.NUM_OPE_SIS_ENT),
                           PKG_PAB_CONSTANTES.V_COD_TIP_OB,
                           TRIM (OPR.NUM_OPE_SIS_ENT),
                           PKG_PAB_CONSTANTES.V_COD_TIP_ABO_CTA,
                           DECODE (COD_EST_AOS,
                                   PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDEV,
                                   TRIM (OPC.NUM_CTA_ODN),
                                   TRIM (OPR.NUM_CTA_BFC)),
                           OPR.NUM_FOL_OPE
                        ),
                        12,
                        '0'
                     )
                     AS NUM_OPE_ORG,
                  --CAO.COD_SNO_CTB AS COD_SNO_CTB_ORG,
                  DECODE (OPR.COD_TPO_OPE_AOS,
                          PKG_PAB_CONSTANTES.V_STR_TPO_OPE_CDLE,
                          PKG_PAB_CONSTANTES.V_STR_COR_PUE_GAR_OOFF,
                          CAO.COD_SNO_CTB)
                     AS COD_SNO_CTB_ORG,
                  DECODE (
                     OPR.COD_TPO_OPE_AOS,                                --GCP
                     PKG_PAB_CONSTANTES.V_STR_TPO_OPE_DVP,
                     DECODE (COD_EST_AOS,
                             PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDEV,
                             CAD.COD_SNO_CTB,
                             PKG_PAB_CONSTANTES.V_STR_COR_CMN),
                     CAD.COD_SNO_CTB
                  )
                     AS COD_SNO_CTB_DTN,
                  v_unoCuatro AS GLS_OBS_UNO,
                  v_unoCuatro AS RSv_USO_ITR,
                  PKG_PAB_CONSTANTES.v_COD_SIS_PAB || v_unoEsp
                     AS COD_CLv_ITZ_GNR_MVT,
                  v_unoEsp AS FLG_CPS_CJA,
                  v_unoEsp AS FLG_CTA_ORD,
                  v_numCero AS TSA_INT,
                  '   ' AS COD_CLv_OPE,
                  v_unoCuatro AS COD_CTO_GTN,
                  '  ' AS COD_APO,
                  '  ' AS E040_SACCDTGT,
                  v_unoEsp AS IDC_UTL,
                  '  ' AS IDC_ROT,
                  v_FEC_ALTA AS FEC_ALA_PTD,
                  ' 0000000000000000             ' AS GLS_OBS_DOS,
                  --PKG_PAB_CONSTANTES.v_COD_ENT_BAN_SANT || v_cuaCer || OPR.NUM_REF_CTB AS NUM_OPE_DTN,
                  PKG_PAB_CONSTANTES.v_COD_ENT_BAN_SANT || v_cuaCer
                  ||                --LPAD(NUM_CTA_BFC,12,'0') AS NUM_OPE_DTN,
                    LPAD (
                        DECODE (
                           OPR.COD_SIS_SAL,
                           PKG_PAB_CONSTANTES.V_COD_TIP_CAJA,
                           TRIM (OPR.NUM_OPE_SIS_ENT),
                           PKG_PAB_CONSTANTES.V_COD_TIP_OB,
                           TRIM (OPR.NUM_OPE_SIS_ENT),
                           PKG_PAB_CONSTANTES.V_COD_TIP_ABO_CTA,
                           DECODE (COD_EST_AOS,
                                   PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDEV,
                                   TRIM (OPC.NUM_CTA_ODN),
                                   TRIM (OPR.NUM_CTA_BFC)),
                           OPR.NUM_FOL_OPE
                        ),
                        12,
                        '0'
                     )
                     AS NUM_OPE_DTN,
                  '     ' AS FLL_DOS,
                  v_FEC_PCS AS FEC_PCS_REG_CTB
           FROM                     PABS_DT_DETLL_OPRCN OPR
                                 --Detalle operacion
                                 JOIN
                                    PABS_DT_OPRCN_INFCN_OPCON OPC
                                 ON OPR.NUM_FOL_OPE = OPC.NUM_FOL_OPE
                                    AND OPR.FEC_ISR_OPE = OPC.FEC_ISR_OPE
                              --se elimina JOIN CAFF
                              JOIN
                                 PABS_DT_TIPO_OPRCN_SALID_MNSJ TSM
                              ON TSM.COD_SIS_ENT_SAL = OPR.COD_SIS_SAL
                                 AND TSM.COD_MT_SWF = OPR.COD_MT_SWF
                                 AND TSM.COD_TPO_OPE_AOS =
                                       OPR.COD_TPO_OPE_AOS
                                 AND TSM.FLG_MRD_UTZ_SWF =
                                       OPR.FLG_MRD_UTZ_SWF
                                 AND TSM.FLG_EGR_ING = OPR.FLG_EGR_ING
                           --Join obtiene cuenta contable y sucursal destino
                           JOIN
                              PABS_DT_CENTA_CNTBL CTB
                           ON CTB.COD_SIS_ENT_SAL = OPR.COD_SIS_SAL
                              AND CTB.FLAG_MRD_UTZ_SWF = OPR.FLG_MRD_UTZ_SWF
                        --Join obtiene sucursal Origen
                        JOIN
                           PABS_DT_CENTA_CNTBL SOG
                        ON SOG.COD_SIS_ENT_SAL = OPR.COD_SIS_ENT
                           AND SOG.FLAG_MRD_UTZ_SWF = OPR.FLG_MRD_UTZ_SWF
                     --Join obtiene codigo de aplicacion sistema entrada salida
                     JOIN
                        PABS_DT_SISTM_ENTRD_SALID CAO
                     ON CAO.COD_SIS_ENT_SAL = OPR.COD_SIS_ENT
                  JOIN
                     PABS_DT_SISTM_ENTRD_SALID CAD
                  ON CAD.COD_SIS_ENT_SAL = OPR.COD_SIS_SAL
          WHERE   OPR.COD_EST_AOS IN
                        (PKG_PAB_CONSTANTES.v_COD_EST_AOS_OPEPAG,
                         PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAGCON,
                         PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDEV)
                  AND OPR.FEC_VTA = v_FEC_PCS --AND TRUNC(OPR.FEC_VTA) = v_FEC_PCS
                  AND OPR.FLG_EGR_ING = PKG_PAB_CONSTANTES.v_FLG_EGR
                  AND OPR.FLG_MRD_UTZ_SWF = PKG_PAB_CONSTANTES.v_FLG_MRD_MN
                  AND TSM.FLG_GNR_OPE_CTB =
                        PKG_PAB_CONSTANTES.v_FLG_GNR_OPE_CTB
                  AND CTB.FLG_VGN = PKG_PAB_CONSTANTES.v_REG_VIG;

      --Inserta datos al DEBE Moneda Extranjera en tabla contabilidad
      INSERT INTO PABS_DT_CNTBL_ALMOT (FEC_ISR_OPE,
                                       NUM_FOL_OPE,
                                       FLG_DEB_HAB,
                                       COD_END,
                                       COD_CLv_ITZ,
                                       FEC_CTB,
                                       FEC_OPE,
                                       COD_PTO,
                                       COD_SPO,
                                       NUM_GRT,
                                       DSC_TPO_PZO,
                                       CAN_PZO,
                                       COD_SUB_SEC,
                                       COD_CNp_SUB_SEC,
                                       NUM_DTN_INV,
                                       CAN_END_TDA,
                                       COD_ABT,
                                       DSC_TPO_TTM_MDA,
                                       DSC_TPO_INV,
                                       COD_OPE,
                                       NUM_CNp_CTA_CTB,
                                       COD_DVI,
                                       DSC_TPO_DVI,
                                       DSC_FLL_UNO,
                                       DSC_VRS_FJO_VAR,
                                       DSC_CLv_ATZ,
                                       COD_SUC_OPE,
                                       COD_SUC_ORG,
                                       COD_SUC_DTN,
                                       NUM_MVT_DEB,
                                       NUM_MVT_HAB,
                                       IMp_MON_BSE_DEB,
                                       IMp_MON_BSE_HAB,
                                       IMp_DVI_DEB,
                                       IMp_DVI_HAB,
                                       FLG_IDC_CCC,
                                       NUM_DOC,
                                       DSC_CLv_CNP,
                                       DSC_CNP,
                                       DSC_TPO_CDA,
                                       GLS_IFM_MVT_CTB,
                                       NUM_OPE_ORG,
                                       COD_SNO_CTB_ORG,
                                       COD_SNO_CTB_DTN,
                                       GLS_OBS_UNO,
                                       DSC_RSv_USO_ITR,
                                       COD_CLv_ITZ_GNR_MVT,
                                       FLG_CPS_CJA,
                                       FLG_CTA_ORD,
                                       DSC_TSA_INT,
                                       COD_CLv_OPE,
                                       COD_CTO_GTN,
                                       COD_APO,
                                       DSC_FLL_TRI,
                                       DSC_IDC_UTL,
                                       DSC_IDC_ROT,
                                       FEC_ALA_PTD,
                                       GLS_OBS_DOS,
                                       NUM_OPE_DTN,
                                       DSC_FLL_DOS,
                                       FEC_PCS_REG_CTB)
         SELECT   OPR.FEC_ISR_OPE AS FEC_ISR_OPE,
                  OPR.NUM_FOL_OPE AS NUM_FOL_OPE,
                  PKG_PAB_CONSTANTES.v_FLG_OPE_DEB AS FLG_DEB_HAB,      --Debe
                  PKG_PAB_CONSTANTES.v_COD_ENT_BAN_SANT AS COD_END,
                  PKG_PAB_CONSTANTES.v_COD_SIS_PAB AS COD_CLv_ITZ,
                  TO_NUMBER (TO_CHAR (OPR.FEC_VTA, 'YYYYMMDD')) AS FEC_CTB,
                  TO_NUMBER (TO_CHAR (OPR.FEC_ISR_OPE, 'YYYYMMDD'))
                     AS FEC_OPE,
                  '00' AS COD_PTO,
                  v_unoCuatro AS COD_SPO,
                  v_numCero AS NUM_GRT,
                  v_unoEsp AS TPO_PZO,
                  v_numCero AS CAN_PZO,
                  v_numCero AS COD_SUB_SEC,
                  v_numCero AS COD_CNp_SUB_SEC,
                  v_numCero AS NUM_DTN_INV,
                  v_unoCuatro AS CAN_END_TDA,
                  v_numCero AS COD_ABT,
                  v_unoEsp AS TPO_TTM_MDA,
                  v_unoEsp AS TPO_INV,
                  '000' AS COD_OPE,
                  CTB.NUM_CNp_CTA_CTB AS NUM_CNp_CTA_CTB,
                  OPR.COD_DVI AS COD_DVI,
                  v_unoEsp AS TPO_DVI,
                  '00000     ' AS FLL_UNO,
                  '                              ' AS VRS_FJO_VAR,
                  '      ' AS CLv_ATZ,
                  PKG_PAB_CONSTANTES.v_COD_SUC_OPERANTE AS COD_SUC_OPE,
                  PKG_PAB_CONSTANTES.V_COD_SUC_ORG AS COD_SUC_ORG, --CTB.COD_SUC_DTN_TPN AS COD_SUC_ORG,
                  CTB.COD_SUC_DTN_TPN AS COD_SUC_DTN,
                  1 AS NUM_MVT_DEB,
                  v_numCero AS NUM_MVT_HAB,
                  v_numCero AS IMp_MON_BSE_DEB,
                  v_numCero AS IMp_MON_BSE_HAB,
                  OPR.IMP_OPE AS IMp_DVI_DEB,
                  v_numCero AS IMp_DVI_HAB,
                  v_unoEsp AS FLG_IDC_CCC,
                  '            ' AS NUM_DOC,
                  '   ' AS CLv_CNP,
                  '              ' AS DSC_CNP,
                  v_unoEsp AS TPO_CDA,
                  CTB.GLS_IFM_MVT_CTB AS GLS_IFM_MVT_CTB,
                  PKG_PAB_CONSTANTES.v_COD_ENT_BAN_SANT || v_cuaCer
                  ||           --NVL(OPR.NUM_REF_CTB,v_numRef) AS NUM_OPE_ORG,
                    LPAD (
                        DECODE (OPR.COD_SIS_ENT,
                                PKG_PAB_CONSTANTES.V_COD_TIP_CAJA,
                                TRIM (OPR.NUM_OPE_SIS_ENT),
                                PKG_PAB_CONSTANTES.V_COD_TIP_OB,
                                TRIM (OPR.NUM_OPE_SIS_ENT),
                                PKG_PAB_CONSTANTES.V_COD_TIP_ABO_CTA,
                                TRIM (OPR.NUM_CTA_BFC),
                                OPR.NUM_FOL_OPE),
                        12,
                        '0'
                     )
                     AS NUM_OPE_ORG,
                  CAO.COD_SNO_CTB AS COD_SNO_CTB_ORG,
                  CAD.COD_SNO_CTB AS COD_SNO_CTB_DTN,
                  v_unoCuatro AS GLS_OBS_UNO,
                  v_unoCuatro AS RSv_USO_ITR,
                  PKG_PAB_CONSTANTES.v_COD_SIS_PAB || v_unoEsp
                     AS COD_CLv_ITZ_GNR_MVT,
                  v_unoEsp AS FLG_CPS_CJA,
                  v_unoEsp AS FLG_CTA_ORD,
                  v_numCero AS TSA_INT,
                  '   ' AS COD_CLv_OPE,
                  '    ' AS COD_CTO_GTN,
                  '  ' AS COD_APO,
                  '  ' AS E040_SACCDTGT,
                  v_unoEsp AS IDC_UTL,
                  '  ' AS IDC_ROT,
                  v_FEC_ALTA AS FEC_ALA_PTD,
                  ' 0000000000000000             ' AS GLS_OBS_DOS,
                  PKG_PAB_CONSTANTES.v_COD_ENT_BAN_SANT || v_cuaCer
                  ||           --NVL(OPR.NUM_REF_CTB,v_numRef) AS NUM_OPE_DTN,
                    LPAD (
                        DECODE (OPR.COD_SIS_ENT,
                                PKG_PAB_CONSTANTES.V_COD_TIP_CAJA,
                                TRIM (OPR.NUM_OPE_SIS_ENT),
                                PKG_PAB_CONSTANTES.V_COD_TIP_OB,
                                TRIM (OPR.NUM_OPE_SIS_ENT),
                                PKG_PAB_CONSTANTES.V_COD_TIP_ABO_CTA,
                                TRIM (OPR.NUM_CTA_BFC),
                                OPR.NUM_FOL_OPE),
                        12,
                        '0'
                     )
                     AS NUM_OPE_DTN,
                  '     ' AS FLL_DOS,
                  v_FEC_PCS AS FEC_PCS_REG_CTB
           FROM                  PABS_DT_DETLL_OPRCN OPR
                              -- Join operaciones que se deben contabilizar
                              JOIN
                                 PABS_DT_TIPO_OPRCN_SALID_MNSJ TSM
                              ON TSM.COD_SIS_ENT_SAL = OPR.COD_SIS_SAL
                                 AND TSM.COD_MT_SWF = OPR.COD_MT_SWF
                                 AND TSM.COD_TPO_OPE_AOS =
                                       OPR.COD_TPO_OPE_AOS
                                 AND TSM.FLG_MRD_UTZ_SWF =
                                       OPR.FLG_MRD_UTZ_SWF
                                 AND TSM.FLG_EGR_ING = OPR.FLG_EGR_ING
                           --Join obtiene cuenta contable y sucursal origen
                           JOIN
                              PABS_DT_CENTA_CNTBL CTB
                           ON CTB.COD_SIS_ENT_SAL = OPR.COD_SIS_ENT
                              AND CTB.FLAG_MRD_UTZ_SWF = OPR.FLG_MRD_UTZ_SWF
                        --Join obtiene sucursal destino
                        JOIN
                           PABS_DT_CENTA_CNTBL SDT
                        ON SDT.COD_SIS_ENT_SAL = OPR.COD_SIS_SAL
                           AND SDT.FLAG_MRD_UTZ_SWF = OPR.FLG_MRD_UTZ_SWF
                     --Join obtiene codigo de aplicacion sistema entrada salida
                     JOIN
                        PABS_DT_SISTM_ENTRD_SALID CAO
                     ON CAO.COD_SIS_ENT_SAL = OPR.COD_SIS_ENT
                  JOIN
                     PABS_DT_SISTM_ENTRD_SALID CAD
                  ON CAD.COD_SIS_ENT_SAL = OPR.COD_SIS_SAL
          WHERE   OPR.COD_EST_AOS = PKG_PAB_CONSTANTES.v_COD_EST_AOS_OPEPAG
                  AND OPR.FEC_VTA = v_FEC_PCS --AND TRUNC(OPR.FEC_VTA) = v_FEC_PCS
                  AND OPR.FLG_EGR_ING = PKG_PAB_CONSTANTES.v_FLG_EGR
                  AND OPR.FLG_MRD_UTZ_SWF = PKG_PAB_CONSTANTES.v_FLG_MRD_MX
                  AND TSM.FLG_GNR_OPE_CTB =
                        PKG_PAB_CONSTANTES.v_FLG_GNR_OPE_CTB
                  AND CTB.FLG_VGN = PKG_PAB_CONSTANTES.v_REG_VIG
                  AND NVL (OPR.GLS_ADC_EST, '#') <> 'Operación de origen AGP'; -- ELIMINAR OPERACIONES PROVENIENTES DE AGP

      --Inserta datos al HABER Moneda Extranjera en tabla contabilidad
      INSERT INTO PABS_DT_CNTBL_ALMOT (FEC_ISR_OPE,
                                       NUM_FOL_OPE,
                                       FLG_DEB_HAB,
                                       COD_END,
                                       COD_CLv_ITZ,
                                       FEC_CTB,
                                       FEC_OPE,
                                       COD_PTO,
                                       COD_SPO,
                                       NUM_GRT,
                                       DSC_TPO_PZO,
                                       CAN_PZO,
                                       COD_SUB_SEC,
                                       COD_CNp_SUB_SEC,
                                       NUM_DTN_INV,
                                       CAN_END_TDA,
                                       COD_ABT,
                                       DSC_TPO_TTM_MDA,
                                       DSC_TPO_INV,
                                       COD_OPE,
                                       NUM_CNp_CTA_CTB,
                                       COD_DVI,
                                       DSC_TPO_DVI,
                                       DSC_FLL_UNO,
                                       DSC_VRS_FJO_VAR,
                                       DSC_CLv_ATZ,
                                       COD_SUC_OPE,
                                       COD_SUC_ORG,
                                       COD_SUC_DTN,
                                       NUM_MVT_DEB,
                                       NUM_MVT_HAB,
                                       IMp_MON_BSE_DEB,
                                       IMp_MON_BSE_HAB,
                                       IMp_DVI_DEB,
                                       IMp_DVI_HAB,
                                       FLG_IDC_CCC,
                                       NUM_DOC,
                                       DSC_CLv_CNP,
                                       DSC_CNP,
                                       DSC_TPO_CDA,
                                       GLS_IFM_MVT_CTB,
                                       NUM_OPE_ORG,
                                       COD_SNO_CTB_ORG,
                                       COD_SNO_CTB_DTN,
                                       GLS_OBS_UNO,
                                       DSC_RSv_USO_ITR,
                                       COD_CLv_ITZ_GNR_MVT,
                                       FLG_CPS_CJA,
                                       FLG_CTA_ORD,
                                       DSC_TSA_INT,
                                       COD_CLv_OPE,
                                       COD_CTO_GTN,
                                       COD_APO,
                                       DSC_FLL_TRI,
                                       DSC_IDC_UTL,
                                       DSC_IDC_ROT,
                                       FEC_ALA_PTD,
                                       GLS_OBS_DOS,
                                       NUM_OPE_DTN,
                                       DSC_FLL_DOS,
                                       FEC_PCS_REG_CTB)
         SELECT   OPR.FEC_ISR_OPE AS FEC_ISR_OPE,
                  OPR.NUM_FOL_OPE AS NUM_FOL_OPE,
                  PKG_PAB_CONSTANTES.v_FLG_OPE_HAB AS FLG_DEB_HAB,     --Haber
                  PKG_PAB_CONSTANTES.v_COD_ENT_BAN_SANT AS COD_END,
                  PKG_PAB_CONSTANTES.v_COD_SIS_PAB AS COD_CLv_ITZ,
                  TO_NUMBER (TO_CHAR (OPR.FEC_VTA, 'YYYYMMDD')) AS FEC_CTB,
                  TO_NUMBER (TO_CHAR (OPR.FEC_ISR_OPE, 'YYYYMMDD'))
                     AS FEC_OPE,
                  '00' AS COD_PTO,
                  '    ' AS COD_SPO,
                  v_numCero AS NUM_GRT,
                  v_unoEsp AS TPO_PZO,
                  v_numCero AS CAN_PZO,
                  v_numCero AS COD_SUB_SEC,
                  v_numCero AS COD_CNp_SUB_SEC,
                  v_numCero AS NUM_DTN_INV,
                  '    ' AS CAN_END_TDA,
                  v_numCero AS COD_ABT,
                  v_unoEsp AS TPO_TTM_MDA,
                  v_unoEsp AS TPO_INV,
                  '000' AS COD_OPE,
                  CTB.NUM_CNp_CTA_CTB AS NUM_CNp_CTA_CTB,
                  OPR.COD_DVI AS COD_DVI,
                  v_unoEsp AS TPO_DVI,
                  '00000     ' AS FLL_UNO,
                  '                              ' AS VRS_FJO_VAR,
                  '      ' AS CLv_ATZ,
                  PKG_PAB_CONSTANTES.v_COD_SUC_OPERANTE AS COD_SUC_OPE,
                  PKG_PAB_CONSTANTES.V_COD_SUC_ORG AS COD_SUC_ORG, --SOG.COD_SUC_DTN_TPN AS COD_SUC_ORG,
                  CTB.COD_SUC_DTN_TPN AS COD_SUC_DTN,
                  v_numCero AS NUM_MVT_DEB,
                  1 AS NUM_MVT_HAB,
                  v_numCero AS IMp_MON_BSE_DEB,
                  v_numCero AS IMp_MON_BSE_HAB,
                  v_numCero AS IMp_DVI_DEB,
                  OPR.IMP_OPE AS IMp_DVI_HAB,
                  v_unoEsp AS FLG_IDC_CCC,
                  '            ' AS NUM_DOC,
                  '   ' AS CLv_CNP,
                  '              ' AS DSC_CNP,
                  v_unoEsp AS TPO_CDA,
                  CTB.GLS_IFM_MVT_CTB AS GLS_IFM_MVT_CTB,
                  PKG_PAB_CONSTANTES.v_COD_ENT_BAN_SANT || v_cuaCer
                  ||           --NVL(OPR.NUM_REF_CTB,v_numRef) AS NUM_OPE_ORG,
                    LPAD (
                        DECODE (OPR.COD_SIS_SAL,
                                PKG_PAB_CONSTANTES.V_COD_TIP_CAJA,
                                TRIM (OPR.NUM_OPE_SIS_ENT),
                                PKG_PAB_CONSTANTES.V_COD_TIP_OB,
                                TRIM (OPR.NUM_OPE_SIS_ENT),
                                PKG_PAB_CONSTANTES.V_COD_TIP_ABO_CTA,
                                TRIM (OPR.NUM_CTA_BFC),
                                OPR.NUM_FOL_OPE),
                        12,
                        '0'
                     )
                     AS NUM_OPE_ORG,
                  CAO.COD_SNO_CTB AS COD_SNO_CTB_ORG,
                  CAD.COD_SNO_CTB AS COD_SNO_CTB_DTN,
                  '    ' AS GLS_OBS_UNO,
                  '    ' AS RSv_USO_ITR,
                  PKG_PAB_CONSTANTES.v_COD_SIS_PAB || v_unoEsp
                     AS COD_CLv_ITZ_GNR_MVT,
                  v_unoEsp AS FLG_CPS_CJA,
                  v_unoEsp AS FLG_CTA_ORD,
                  v_numCero AS TSA_INT,
                  '   ' AS COD_CLv_OPE,
                  v_unoCuatro AS COD_CTO_GTN,
                  '  ' AS COD_APO,
                  '  ' AS E040_SACCDTGT,
                  v_unoEsp AS IDC_UTL,
                  '  ' AS IDC_ROT,
                  v_FEC_ALTA AS FEC_ALA_PTD,
                  ' 0000000000000000             ' AS GLS_OBS_DOS,
                  PKG_PAB_CONSTANTES.v_COD_ENT_BAN_SANT || v_cuaCer
                  ||           --NVL(OPR.NUM_REF_CTB,v_numRef) AS NUM_OPE_DTN,
                    LPAD (
                        DECODE (OPR.COD_SIS_SAL,
                                PKG_PAB_CONSTANTES.V_COD_TIP_CAJA,
                                TRIM (OPR.NUM_OPE_SIS_ENT),
                                PKG_PAB_CONSTANTES.V_COD_TIP_OB,
                                TRIM (OPR.NUM_OPE_SIS_ENT),
                                PKG_PAB_CONSTANTES.V_COD_TIP_ABO_CTA,
                                TRIM (OPR.NUM_CTA_BFC),
                                OPR.NUM_FOL_OPE),
                        12,
                        '0'
                     )
                     AS NUM_OPE_DTN,
                  '     ' AS FLL_DOS,
                  v_FEC_PCS AS FEC_PCS_REG_CTB
           FROM                  PABS_DT_DETLL_OPRCN OPR
                              -- Join operaciones que se deben contabilizar
                              JOIN
                                 PABS_DT_TIPO_OPRCN_SALID_MNSJ TSM
                              ON TSM.COD_SIS_ENT_SAL = OPR.COD_SIS_SAL
                                 AND TSM.COD_MT_SWF = OPR.COD_MT_SWF
                                 AND TSM.COD_TPO_OPE_AOS =
                                       OPR.COD_TPO_OPE_AOS
                                 AND TSM.FLG_MRD_UTZ_SWF =
                                       OPR.FLG_MRD_UTZ_SWF
                                 AND TSM.FLG_EGR_ING = OPR.FLG_EGR_ING
                           --Join obtiene cuenta contable y sucursal destino
                           JOIN
                              PABS_DT_CENTA_CNTBL CTB
                           ON CTB.COD_SIS_ENT_SAL = OPR.COD_SIS_SAL
                              AND CTB.FLAG_MRD_UTZ_SWF = OPR.FLG_MRD_UTZ_SWF
                        --Join obtiene sucursal Origen
                        JOIN
                           PABS_DT_CENTA_CNTBL SOG
                        ON SOG.COD_SIS_ENT_SAL = OPR.COD_SIS_ENT
                           AND SOG.FLAG_MRD_UTZ_SWF = OPR.FLG_MRD_UTZ_SWF
                     --Join obtiene codigo de aplicacion sistema entrada salida
                     JOIN
                        PABS_DT_SISTM_ENTRD_SALID CAO
                     ON CAO.COD_SIS_ENT_SAL = OPR.COD_SIS_ENT
                  JOIN
                     PABS_DT_SISTM_ENTRD_SALID CAD
                  ON CAD.COD_SIS_ENT_SAL = OPR.COD_SIS_SAL
          WHERE   OPR.COD_EST_AOS = PKG_PAB_CONSTANTES.v_COD_EST_AOS_OPEPAG
                  AND OPR.FEC_VTA = v_FEC_PCS --AND TRUNC(OPR.FEC_VTA) = v_FEC_PCS
                  AND OPR.FLG_EGR_ING = PKG_PAB_CONSTANTES.v_FLG_EGR
                  AND OPR.FLG_MRD_UTZ_SWF = PKG_PAB_CONSTANTES.v_FLG_MRD_MX
                  AND TSM.FLG_GNR_OPE_CTB =
                        PKG_PAB_CONSTANTES.v_FLG_GNR_OPE_CTB
                  AND CTB.FLG_VGN = PKG_PAB_CONSTANTES.v_REG_VIG
                  AND NVL (OPR.GLS_ADC_EST, '#') <> 'Operación de origen AGP'; -- ELIMINAR OPERACIONES PROVENIENTES DE AGP

      /**************************************************************************/
      ----------------------------Ingresos----------------------------------------
      /**************************************************************************/

      --Inserta datos al DEBE Moneda nacional en tabla contabilidad
      INSERT INTO PABS_DT_CNTBL_ALMOT (FEC_ISR_OPE,
                                       NUM_FOL_OPE,
                                       FLG_DEB_HAB,
                                       COD_END,
                                       COD_CLv_ITZ,
                                       FEC_CTB,
                                       FEC_OPE,
                                       COD_PTO,
                                       COD_SPO,
                                       NUM_GRT,
                                       DSC_TPO_PZO,
                                       CAN_PZO,
                                       COD_SUB_SEC,
                                       COD_CNp_SUB_SEC,
                                       NUM_DTN_INV,
                                       CAN_END_TDA,
                                       COD_ABT,
                                       DSC_TPO_TTM_MDA,
                                       DSC_TPO_INV,
                                       COD_OPE,
                                       NUM_CNp_CTA_CTB,
                                       COD_DVI,
                                       DSC_TPO_DVI,
                                       DSC_FLL_UNO,
                                       DSC_VRS_FJO_VAR,
                                       DSC_CLv_ATZ,
                                       COD_SUC_OPE,
                                       COD_SUC_ORG,
                                       COD_SUC_DTN,
                                       NUM_MVT_DEB,
                                       NUM_MVT_HAB,
                                       IMp_MON_BSE_DEB,
                                       IMp_MON_BSE_HAB,
                                       IMp_DVI_DEB,
                                       IMp_DVI_HAB,
                                       FLG_IDC_CCC,
                                       NUM_DOC,
                                       DSC_CLv_CNP,
                                       DSC_CNP,
                                       DSC_TPO_CDA,
                                       GLS_IFM_MVT_CTB,
                                       NUM_OPE_ORG,
                                       COD_SNO_CTB_ORG,
                                       COD_SNO_CTB_DTN,
                                       GLS_OBS_UNO,
                                       DSC_RSv_USO_ITR,
                                       COD_CLv_ITZ_GNR_MVT,
                                       FLG_CPS_CJA,
                                       FLG_CTA_ORD,
                                       DSC_TSA_INT,
                                       COD_CLv_OPE,
                                       COD_CTO_GTN,
                                       COD_APO,
                                       DSC_FLL_TRI,
                                       DSC_IDC_UTL,
                                       DSC_IDC_ROT,
                                       FEC_ALA_PTD,
                                       GLS_OBS_DOS,
                                       NUM_OPE_DTN,
                                       DSC_FLL_DOS,
                                       FEC_PCS_REG_CTB)
         SELECT   OPR.FEC_ISR_OPE AS FEC_ISR_OPE,
                  OPR.NUM_FOL_OPE AS NUM_FOL_OPE,
                  PKG_PAB_CONSTANTES.v_FLG_OPE_DEB AS FLG_DEB_HAB,      --Debe
                  PKG_PAB_CONSTANTES.v_COD_ENT_BAN_SANT AS COD_END,
                  PKG_PAB_CONSTANTES.v_COD_SIS_PAB AS COD_CLv_ITZ,
                  TO_NUMBER (TO_CHAR (OPR.FEC_VTA, 'YYYYMMDD')) AS FEC_CTB,
                  TO_NUMBER (TO_CHAR (OPR.FEC_ISR_OPE, 'YYYYMMDD'))
                     AS FEC_OPE,
                  '00' AS COD_PTO,
                  v_unoCuatro AS COD_SPO,
                  v_numCero AS NUM_GRT,
                  v_unoEsp AS TPO_PZO,
                  v_numCero AS CAN_PZO,
                  v_numCero AS COD_SUB_SEC,
                  v_numCero AS COD_CNp_SUB_SEC,
                  v_numCero AS NUM_DTN_INV,
                  v_unoCuatro AS CAN_END_TDA,
                  v_numCero AS COD_ABT,
                  v_unoEsp AS TPO_TTM_MDA,
                  v_unoEsp AS TPO_INV,
                  '000' AS COD_OPE,
                  CTB.NUM_CNP_CTA_CTB AS NUM_CNP_CTA_CTB,
                  OPR.COD_DVI AS COD_DVI,
                  v_unoEsp AS TPO_DVI,
                  '00000     ' AS FLL_UNO,
                  '                              ' AS VRS_FJO_VAR,
                  '      ' AS CLv_ATZ,
                  PKG_PAB_CONSTANTES.v_COD_SUC_OPERANTE AS COD_SUC_OPE,
                  PKG_PAB_CONSTANTES.V_COD_SUC_ORG AS COD_SUC_ORG, --CTB.COD_SUC_DTN_TPN AS COD_SUC_ORG,
                  CTB.COD_SUC_DTN_TPN AS COD_SUC_DTN,
                  1 AS NUM_MVT_DEB,
                  v_numCero AS NUM_MVT_HAB,
                  OPR.IMP_OPE AS IMp_MON_BSE_DEB,
                  v_numCero AS IMp_MON_BSE_HAB,
                  v_numCero AS IMp_DVI_DEB,
                  v_numCero AS IMp_DVI_HAB,
                  v_unoEsp AS FLG_IDC_CCC,
                  '            ' AS NUM_DOC,
                  '   ' AS CLv_CNP,
                  '              ' AS DSC_CNP,
                  v_unoEsp AS TPO_CDA,
                  CTB.GLS_IFM_MVT_CTB AS GLS_IFM_MVT_CTB,
                  PKG_PAB_CONSTANTES.v_COD_ENT_BAN_SANT || v_cuaCer
                  ||           --NVL(OPR.NUM_REF_CTB,v_numRef) AS NUM_OPE_ORG,
                    LPAD (
                        DECODE (OPR.COD_SIS_ENT,
                                PKG_PAB_CONSTANTES.V_COD_TIP_CAJA,
                                TRIM (OPR.NUM_OPE_SIS_ENT),
                                PKG_PAB_CONSTANTES.V_COD_TIP_OB,
                                TRIM (OPR.NUM_OPE_SIS_ENT),
                                PKG_PAB_CONSTANTES.V_COD_TIP_ABO_CTA,
                                TRIM (OPR.NUM_CTA_BFC),
                                OPR.NUM_FOL_OPE),
                        12,
                        '0'
                     )
                     AS NUM_OPE_ORG,
                  CAO.COD_SNO_CTB AS COD_SNO_CTB_ORG,
                  CAD.COD_SNO_CTB AS COD_SNO_CTB_DTN,
                  v_unoCuatro AS GLS_OBS_UNO,
                  v_unoCuatro AS RSv_USO_ITR,
                  PKG_PAB_CONSTANTES.v_COD_SIS_PAB || v_unoEsp
                     AS COD_CLv_ITZ_GNR_MVT,
                  v_unoEsp AS FLG_CPS_CJA,
                  v_unoEsp AS FLG_CTA_ORD,
                  v_numCero AS TSA_INT,
                  '   ' AS COD_CLv_OPE,
                  v_unoCuatro AS COD_CTO_GTN,
                  '  ' AS COD_APO,
                  '  ' AS DSC_FLL_TRI,
                  v_unoEsp AS IDC_UTL,
                  '  ' AS IDC_ROT,
                  v_FEC_ALTA AS FEC_ALA_PTD,
                  ' 0000000000000000             ' AS GLS_OBS_DOS,
                  PKG_PAB_CONSTANTES.v_COD_ENT_BAN_SANT || v_cuaCer
                  || LPAD (
                        DECODE (OPR.COD_SIS_ENT,
                                PKG_PAB_CONSTANTES.V_COD_TIP_CAJA,
                                TRIM (OPR.NUM_OPE_SIS_ENT),
                                PKG_PAB_CONSTANTES.V_COD_TIP_OB,
                                TRIM (OPR.NUM_OPE_SIS_ENT),
                                PKG_PAB_CONSTANTES.V_COD_TIP_ABO_CTA,
                                TRIM (OPR.NUM_CTA_BFC),
                                OPR.NUM_FOL_OPE),
                        12,
                        '0'
                     )
                     AS NUM_OPE_DTN,
                  --PKG_PAB_CONSTANTES.v_COD_ENT_BAN_SANT || v_cuaCer || NVL(OPR.NUM_REF_CTB,v_numRef) AS NUM_OPE_DTN,
                  '     ' AS FLL_DOS,
                  v_FEC_PCS AS FEC_PCS_REG_CTB
           FROM               PABS_DT_DETLL_OPRCN OPR
                           -- se elimina JOIN CAFF
                           /*JOIN
                               PABS_DT_TIPO_OPRCN_SALID_MNSJ TSM
                               ON
                               TSM.COD_SIS_ENT_SAL = OPR.COD_SIS_ENT
                               AND TSM.COD_MT_SWF = OPR.COD_MT_SWF
                               --AND TSM.COD_TPO_OPE_AOS = OPR.COD_TPO_OPE_AOS  --No aplica para los ingresos
                               AND TSM.FLG_MRD_UTZ_SWF = OPR.FLG_MRD_UTZ_SWF
                               AND TSM.FLG_EGR_ING  = OPR.FLG_EGR_ING*/
                           --Join obtiene cuenta contable y sucursal origen
                           JOIN
                              PABS_DT_CENTA_CNTBL CTB
                           ON CTB.COD_SIS_ENT_SAL = OPR.COD_SIS_ENT
                              AND CTB.FLAG_MRD_UTZ_SWF = OPR.FLG_MRD_UTZ_SWF
                        --Join obtiene sucursal destino
                        JOIN
                           PABS_DT_CENTA_CNTBL SDT
                        ON SDT.COD_SIS_ENT_SAL = OPR.COD_SIS_SAL
                           AND SDT.FLAG_MRD_UTZ_SWF = OPR.FLG_MRD_UTZ_SWF
                     --Join obtiene codigo de aplicacion sistema entrada salida
                     JOIN
                        PABS_DT_SISTM_ENTRD_SALID CAO
                     ON CAO.COD_SIS_ENT_SAL = OPR.COD_SIS_ENT
                  JOIN
                     PABS_DT_SISTM_ENTRD_SALID CAD
                  ON CAD.COD_SIS_ENT_SAL = OPR.COD_SIS_SAL
          WHERE   OPR.COD_EST_AOS IN
                        (PKG_PAB_CONSTANTES.v_COD_EST_AOS_OPEABN,
                         PKG_PAB_CONSTANTES.v_COD_EST_AOS_OPEDEV)
                  AND OPR.FEC_VTA = v_FEC_PCS --AND TRUNC(OPR.FEC_VTA) = v_FEC_PCS
                  AND OPR.FLG_EGR_ING = PKG_PAB_CONSTANTES.v_FLG_ING
                  AND OPR.FLG_MRD_UTZ_SWF = PKG_PAB_CONSTANTES.v_FLG_MRD_MN
                  --AND TSM.FLG_GNR_OPE_CTB = PKG_PAB_CONSTANTES.v_FLG_GNR_OPE_CTB
                  AND CTB.FLG_VGN = PKG_PAB_CONSTANTES.v_REG_VIG;

      --Inserta datos al HABER Moneda nacional en tabla contabilidad
      INSERT INTO PABS_DT_CNTBL_ALMOT (FEC_ISR_OPE,
                                       NUM_FOL_OPE,
                                       FLG_DEB_HAB,
                                       COD_END,
                                       COD_CLv_ITZ,
                                       FEC_CTB,
                                       FEC_OPE,
                                       COD_PTO,
                                       COD_SPO,
                                       NUM_GRT,
                                       DSC_TPO_PZO,
                                       CAN_PZO,
                                       COD_SUB_SEC,
                                       COD_CNp_SUB_SEC,
                                       NUM_DTN_INV,
                                       CAN_END_TDA,
                                       COD_ABT,
                                       DSC_TPO_TTM_MDA,
                                       DSC_TPO_INV,
                                       COD_OPE,
                                       NUM_CNp_CTA_CTB,
                                       COD_DVI,
                                       DSC_TPO_DVI,
                                       DSC_FLL_UNO,
                                       DSC_VRS_FJO_VAR,
                                       DSC_CLv_ATZ,
                                       COD_SUC_OPE,
                                       COD_SUC_ORG,
                                       COD_SUC_DTN,
                                       NUM_MVT_DEB,
                                       NUM_MVT_HAB,
                                       IMp_MON_BSE_DEB,
                                       IMp_MON_BSE_HAB,
                                       IMp_DVI_DEB,
                                       IMp_DVI_HAB,
                                       FLG_IDC_CCC,
                                       NUM_DOC,
                                       DSC_CLv_CNP,
                                       DSC_CNP,
                                       DSC_TPO_CDA,
                                       GLS_IFM_MVT_CTB,
                                       NUM_OPE_ORG,
                                       COD_SNO_CTB_ORG,
                                       COD_SNO_CTB_DTN,
                                       GLS_OBS_UNO,
                                       DSC_RSv_USO_ITR,
                                       COD_CLv_ITZ_GNR_MVT,
                                       FLG_CPS_CJA,
                                       FLG_CTA_ORD,
                                       DSC_TSA_INT,
                                       COD_CLv_OPE,
                                       COD_CTO_GTN,
                                       COD_APO,
                                       DSC_FLL_TRI,
                                       DSC_IDC_UTL,
                                       DSC_IDC_ROT,
                                       FEC_ALA_PTD,
                                       GLS_OBS_DOS,
                                       NUM_OPE_DTN,
                                       DSC_FLL_DOS,
                                       FEC_PCS_REG_CTB)
         SELECT   OPR.FEC_ISR_OPE AS FEC_ISR_OPE,
                  OPR.NUM_FOL_OPE AS NUM_FOL_OPE,
                  PKG_PAB_CONSTANTES.v_FLG_OPE_HAB AS FLG_DEB_HAB,     --Haber
                  PKG_PAB_CONSTANTES.v_COD_ENT_BAN_SANT AS COD_END,
                  PKG_PAB_CONSTANTES.v_COD_SIS_PAB AS COD_CLv_ITZ,
                  TO_NUMBER (TO_CHAR (OPR.FEC_VTA, 'YYYYMMDD')) AS FEC_CTB,
                  TO_NUMBER (TO_CHAR (OPR.FEC_ISR_OPE, 'YYYYMMDD'))
                     AS FEC_OPE,
                  '00' AS COD_PTO,
                  '    ' AS COD_SPO,
                  v_numCero AS NUM_GRT,
                  v_unoEsp AS TPO_PZO,
                  v_numCero AS CAN_PZO,
                  v_numCero AS COD_SUB_SEC,
                  v_numCero AS COD_CNp_SUB_SEC,
                  v_numCero AS NUM_DTN_INV,
                  v_unoCuatro AS CAN_END_TDA,
                  v_numCero AS COD_ABT,
                  v_unoEsp AS TPO_TTM_MDA,
                  v_unoEsp AS TPO_INV,
                  '000' AS COD_OPE,
                  CTB.NUM_CNp_CTA_CTB AS NUM_CNp_CTA_CTB,
                  OPR.COD_DVI AS COD_DVI,
                  v_unoEsp AS TPO_DVI,
                  '00000     ' AS FLL_UNO,
                  '                              ' AS VRS_FJO_VAR,
                  '      ' AS CLv_ATZ,
                  PKG_PAB_CONSTANTES.v_COD_SUC_OPERANTE AS COD_SUC_OPE,
                  PKG_PAB_CONSTANTES.V_COD_SUC_ORG AS COD_SUC_ORG, --SOG.COD_SUC_DTN_TPN AS COD_SUC_ORG,
                  CTB.COD_SUC_DTN_TPN AS COD_SUC_DTN,
                  v_numCero AS NUM_MVT_DEB,
                  1 AS NUM_MVT_HAB,
                  v_numCero AS IMp_MON_BSE_DEB,
                  OPR.IMP_OPE AS IMp_MON_BSE_HAB,
                  v_numCero AS IMp_DVI_DEB,
                  v_numCero AS IMp_DVI_HAB,
                  v_unoEsp AS FLG_IDC_CCC,
                  '            ' AS NUM_DOC,
                  '   ' AS CLv_CNP,
                  '              ' AS DSC_CNP,
                  v_unoEsp AS TPO_CDA,
                  CTB.GLS_IFM_MVT_CTB AS GLS_IFM_MVT_CTB,
                  PKG_PAB_CONSTANTES.v_COD_ENT_BAN_SANT || v_cuaCer
                  ||           --NVL(OPR.NUM_REF_CTB,v_numRef) AS NUM_OPE_ORG,
                    LPAD (
                        DECODE (OPR.COD_SIS_SAL,
                                PKG_PAB_CONSTANTES.V_COD_TIP_CAJA,
                                TRIM (OPR.NUM_OPE_SIS_ENT),
                                PKG_PAB_CONSTANTES.V_COD_TIP_OB,
                                TRIM (OPR.NUM_OPE_SIS_ENT),
                                PKG_PAB_CONSTANTES.V_COD_TIP_ABO_CTA,
                                TRIM (OPR.NUM_CTA_BFC),
                                OPR.NUM_FOL_OPE),
                        12,
                        '0'
                     )
                     AS NUM_OPE_ORG,
                  CAO.COD_SNO_CTB AS COD_SNO_CTB_ORG,
                  CAD.COD_SNO_CTB AS COD_SNO_CTB_DTN,
                  v_unoCuatro AS GLS_OBS_UNO,
                  v_unoCuatro AS RSv_USO_ITR,
                  PKG_PAB_CONSTANTES.v_COD_SIS_PAB || v_unoEsp
                     AS COD_CLv_ITZ_GNR_MVT,
                  v_unoEsp AS FLG_CPS_CJA,
                  v_unoEsp AS FLG_CTA_ORD,
                  v_numCero AS TSA_INT,
                  '   ' AS COD_CLv_OPE,
                  v_unoCuatro AS COD_CTO_GTN,
                  '  ' AS COD_APO,
                  '  ' AS E040_SACCDTGT,
                  v_unoEsp AS IDC_UTL,
                  '  ' AS IDC_ROT,
                  v_FEC_ALTA AS FEC_ALA_PTD,
                  ' 0000000000000000             ' AS GLS_OBS_DOS,
                  --PKG_PAB_CONSTANTES.v_COD_ENT_BAN_SANT || v_cuaCer || OPR.NUM_REF_CTB AS NUM_OPE_DTN, --GCP
                  PKG_PAB_CONSTANTES.v_COD_ENT_BAN_SANT || v_cuaCer
                  ||                --LPAD(NUM_CTA_BFC,12,'0') AS NUM_OPE_DTN,
                    LPAD (
                        DECODE (OPR.COD_SIS_SAL,
                                PKG_PAB_CONSTANTES.V_COD_TIP_CAJA,
                                TRIM (OPR.NUM_OPE_SIS_ENT),
                                PKG_PAB_CONSTANTES.V_COD_TIP_OB,
                                TRIM (OPR.NUM_OPE_SIS_ENT),
                                PKG_PAB_CONSTANTES.V_COD_TIP_ABO_CTA,
                                TRIM (OPR.NUM_CTA_BFC),
                                OPR.NUM_FOL_OPE),
                        12,
                        '0'
                     )
                     AS NUM_OPE_DTN,
                  '     ' AS FLL_DOS,
                  v_FEC_PCS AS FEC_PCS_REG_CTB
           FROM               PABS_DT_DETLL_OPRCN OPR
                           -- se elimina JOIN CAFF
                           /*JOIN
                               PABS_DT_TIPO_OPRCN_SALID_MNSJ TSM
                               ON
                               TSM.COD_SIS_ENT_SAL = OPR.COD_SIS_ENT
                               AND TSM.COD_MT_SWF = OPR.COD_MT_SWF
                               --AND TSM.COD_TPO_OPE_AOS = OPR.COD_TPO_OPE_AOS  --No aplica para los ingresos
                               AND TSM.FLG_MRD_UTZ_SWF = OPR.FLG_MRD_UTZ_SWF
                               AND TSM.FLG_EGR_ING  = OPR.FLG_EGR_ING*/
                           --Join obtiene cuenta contable y sucursal destino
                           JOIN
                              PABS_DT_CENTA_CNTBL CTB
                           ON CTB.COD_SIS_ENT_SAL = OPR.COD_SIS_SAL
                              AND CTB.FLAG_MRD_UTZ_SWF = OPR.FLG_MRD_UTZ_SWF
                        --Join obtiene sucursal Origen
                        JOIN
                           PABS_DT_CENTA_CNTBL SOG
                        ON SOG.COD_SIS_ENT_SAL = OPR.COD_SIS_ENT
                           AND SOG.FLAG_MRD_UTZ_SWF = OPR.FLG_MRD_UTZ_SWF
                     --Join obtiene codigo de aplicacion sistema entrada salida
                     JOIN
                        PABS_DT_SISTM_ENTRD_SALID CAO
                     ON CAO.COD_SIS_ENT_SAL = OPR.COD_SIS_ENT
                  JOIN
                     PABS_DT_SISTM_ENTRD_SALID CAD
                  ON CAD.COD_SIS_ENT_SAL = OPR.COD_SIS_SAL
          WHERE   OPR.COD_EST_AOS IN
                        (PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEABN,
                         PKG_PAB_CONSTANTES.v_COD_EST_AOS_OPEDEV)
                  AND OPR.FEC_VTA = v_FEC_PCS --AND TRUNC(OPR.FEC_VTA) = v_FEC_PCS
                  AND OPR.FLG_EGR_ING = PKG_PAB_CONSTANTES.v_FLG_ING
                  AND OPR.FLG_MRD_UTZ_SWF = PKG_PAB_CONSTANTES.v_FLG_MRD_MN
                  --AND TSM.FLG_GNR_OPE_CTB = PKG_PAB_CONSTANTES.v_FLG_GNR_OPE_CTB
                  AND CTB.FLG_VGN = PKG_PAB_CONSTANTES.v_REG_VIG;

      --Inserta datos al DEBE Moneda Extranjera en tabla contabilidad
      INSERT INTO PABS_DT_CNTBL_ALMOT (FEC_ISR_OPE,
                                       NUM_FOL_OPE,
                                       FLG_DEB_HAB,
                                       COD_END,
                                       COD_CLv_ITZ,
                                       FEC_CTB,
                                       FEC_OPE,
                                       COD_PTO,
                                       COD_SPO,
                                       NUM_GRT,
                                       DSC_TPO_PZO,
                                       CAN_PZO,
                                       COD_SUB_SEC,
                                       COD_CNp_SUB_SEC,
                                       NUM_DTN_INV,
                                       CAN_END_TDA,
                                       COD_ABT,
                                       DSC_TPO_TTM_MDA,
                                       DSC_TPO_INV,
                                       COD_OPE,
                                       NUM_CNp_CTA_CTB,
                                       COD_DVI,
                                       DSC_TPO_DVI,
                                       DSC_FLL_UNO,
                                       DSC_VRS_FJO_VAR,
                                       DSC_CLv_ATZ,
                                       COD_SUC_OPE,
                                       COD_SUC_ORG,
                                       COD_SUC_DTN,
                                       NUM_MVT_DEB,
                                       NUM_MVT_HAB,
                                       IMp_MON_BSE_DEB,
                                       IMp_MON_BSE_HAB,
                                       IMp_DVI_DEB,
                                       IMp_DVI_HAB,
                                       FLG_IDC_CCC,
                                       NUM_DOC,
                                       DSC_CLv_CNP,
                                       DSC_CNP,
                                       DSC_TPO_CDA,
                                       GLS_IFM_MVT_CTB,
                                       NUM_OPE_ORG,
                                       COD_SNO_CTB_ORG,
                                       COD_SNO_CTB_DTN,
                                       GLS_OBS_UNO,
                                       DSC_RSv_USO_ITR,
                                       COD_CLv_ITZ_GNR_MVT,
                                       FLG_CPS_CJA,
                                       FLG_CTA_ORD,
                                       DSC_TSA_INT,
                                       COD_CLv_OPE,
                                       COD_CTO_GTN,
                                       COD_APO,
                                       DSC_FLL_TRI,
                                       DSC_IDC_UTL,
                                       DSC_IDC_ROT,
                                       FEC_ALA_PTD,
                                       GLS_OBS_DOS,
                                       NUM_OPE_DTN,
                                       DSC_FLL_DOS,
                                       FEC_PCS_REG_CTB)
         SELECT   OPR.FEC_ISR_OPE AS FEC_ISR_OPE,
                  OPR.NUM_FOL_OPE AS NUM_FOL_OPE,
                  PKG_PAB_CONSTANTES.v_FLG_OPE_DEB AS FLG_DEB_HAB,      --Debe
                  PKG_PAB_CONSTANTES.v_COD_ENT_BAN_SANT AS COD_END,
                  PKG_PAB_CONSTANTES.v_COD_SIS_PAB AS COD_CLv_ITZ,
                  TO_NUMBER (TO_CHAR (OPR.FEC_VTA, 'YYYYMMDD')) AS FEC_CTB,
                  TO_NUMBER (TO_CHAR (OPR.FEC_ISR_OPE, 'YYYYMMDD'))
                     AS FEC_OPE,
                  '00' AS COD_PTO,
                  v_unoCuatro AS COD_SPO,
                  v_numCero AS NUM_GRT,
                  v_unoEsp AS TPO_PZO,
                  v_numCero AS CAN_PZO,
                  v_numCero AS COD_SUB_SEC,
                  v_numCero AS COD_CNp_SUB_SEC,
                  v_numCero AS NUM_DTN_INV,
                  v_unoCuatro AS CAN_END_TDA,
                  v_numCero AS COD_ABT,
                  v_unoEsp AS TPO_TTM_MDA,
                  v_unoEsp AS TPO_INV,
                  '000' AS COD_OPE,
                  CTB.NUM_CNp_CTA_CTB AS NUM_CNp_CTA_CTB,
                  OPR.COD_DVI AS COD_DVI,
                  v_unoEsp AS TPO_DVI,
                  '00000     ' AS FLL_UNO,
                  '                              ' AS VRS_FJO_VAR,
                  '      ' AS CLv_ATZ,
                  PKG_PAB_CONSTANTES.v_COD_SUC_OPERANTE AS COD_SUC_OPE,
                  PKG_PAB_CONSTANTES.V_COD_SUC_ORG AS COD_SUC_ORG, --CTB.COD_SUC_DTN_TPN AS COD_SUC_ORG,
                  CTB.COD_SUC_DTN_TPN AS COD_SUC_DTN,
                  1 AS NUM_MVT_DEB,
                  v_numCero AS NUM_MVT_HAB,
                  v_numCero AS IMp_MON_BSE_DEB,
                  v_numCero AS IMp_MON_BSE_HAB,
                  OPR.IMP_OPE AS IMp_DVI_DEB,
                  v_numCero AS IMp_DVI_HAB,
                  v_unoEsp AS FLG_IDC_CCC,
                  '            ' AS NUM_DOC,
                  '   ' AS CLv_CNP,
                  '              ' AS DSC_CNP,
                  v_unoEsp AS TPO_CDA,
                  CTB.GLS_IFM_MVT_CTB AS GLS_IFM_MVT_CTB,
                  PKG_PAB_CONSTANTES.v_COD_ENT_BAN_SANT || v_cuaCer
                  ||           --NVL(OPR.NUM_REF_CTB,v_numRef) AS NUM_OPE_ORG,
                    LPAD (
                        DECODE (OPR.COD_SIS_ENT,
                                PKG_PAB_CONSTANTES.V_COD_TIP_CAJA,
                                TRIM (OPR.NUM_OPE_SIS_ENT),
                                PKG_PAB_CONSTANTES.V_COD_TIP_OB,
                                TRIM (OPR.NUM_OPE_SIS_ENT),
                                PKG_PAB_CONSTANTES.V_COD_TIP_ABO_CTA,
                                TRIM (OPR.NUM_CTA_BFC),
                                OPR.NUM_FOL_OPE),
                        12,
                        '0'
                     )
                     AS NUM_OPE_ORG,
                  CAO.COD_SNO_CTB AS COD_SNO_CTB_ORG,
                  CAD.COD_SNO_CTB AS COD_SNO_CTB_DTN,
                  v_unoCuatro AS GLS_OBS_UNO,
                  v_unoCuatro AS RSv_USO_ITR,
                  PKG_PAB_CONSTANTES.v_COD_SIS_PAB || v_unoEsp
                     AS COD_CLv_ITZ_GNR_MVT,
                  v_unoEsp AS FLG_CPS_CJA,
                  v_unoEsp AS FLG_CTA_ORD,
                  v_numCero AS TSA_INT,
                  '   ' AS COD_CLv_OPE,
                  '    ' AS COD_CTO_GTN,
                  '  ' AS COD_APO,
                  '  ' AS E040_SACCDTGT,
                  v_unoEsp AS IDC_UTL,
                  '  ' AS IDC_ROT,
                  v_FEC_ALTA AS FEC_ALA_PTD,
                  ' 0000000000000000             ' AS GLS_OBS_DOS,
                  PKG_PAB_CONSTANTES.v_COD_ENT_BAN_SANT || v_cuaCer
                  ||           --NVL(OPR.NUM_REF_CTB,v_numRef) AS NUM_OPE_DTN,
                    LPAD (
                        DECODE (OPR.COD_SIS_ENT,
                                PKG_PAB_CONSTANTES.V_COD_TIP_CAJA,
                                TRIM (OPR.NUM_OPE_SIS_ENT),
                                PKG_PAB_CONSTANTES.V_COD_TIP_OB,
                                TRIM (OPR.NUM_OPE_SIS_ENT),
                                PKG_PAB_CONSTANTES.V_COD_TIP_ABO_CTA,
                                TRIM (OPR.NUM_CTA_BFC),
                                OPR.NUM_FOL_OPE),
                        12,
                        '0'
                     )
                     AS NUM_OPE_DTN,
                  '     ' AS FLL_DOS,
                  v_FEC_PCS AS FEC_PCS_REG_CTB
           FROM                  PABS_DT_DETLL_OPRCN OPR
                              -- Join operaciones que se deben contabilizar
                              JOIN
                                 PABS_DT_TIPO_OPRCN_SALID_MNSJ TSM
                              ON TSM.COD_SIS_ENT_SAL = OPR.COD_SIS_ENT
                                 AND TSM.COD_MT_SWF = OPR.COD_MT_SWF
                                 --AND TSM.COD_TPO_OPE_AOS = OPR.COD_TPO_OPE_AOS  --No aplica para los ingresos
                                 AND TSM.FLG_MRD_UTZ_SWF =
                                       OPR.FLG_MRD_UTZ_SWF
                                 AND TSM.FLG_EGR_ING = OPR.FLG_EGR_ING
                           --Join obtiene cuenta contable y sucursal origen
                           JOIN
                              PABS_DT_CENTA_CNTBL CTB
                           ON CTB.COD_SIS_ENT_SAL = OPR.COD_SIS_ENT
                              AND CTB.FLAG_MRD_UTZ_SWF = OPR.FLG_MRD_UTZ_SWF
                        --Join obtiene sucursal destino
                        JOIN
                           PABS_DT_CENTA_CNTBL SDT
                        ON SDT.COD_SIS_ENT_SAL = OPR.COD_SIS_SAL
                           AND SDT.FLAG_MRD_UTZ_SWF = OPR.FLG_MRD_UTZ_SWF
                     --Join obtiene codigo de aplicacion sistema entrada salida
                     JOIN
                        PABS_DT_SISTM_ENTRD_SALID CAO
                     ON CAO.COD_SIS_ENT_SAL = OPR.COD_SIS_ENT
                  JOIN
                     PABS_DT_SISTM_ENTRD_SALID CAD
                  ON CAD.COD_SIS_ENT_SAL = OPR.COD_SIS_SAL
          WHERE   OPR.COD_EST_AOS IN
                        (PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEABN,
                         PKG_PAB_CONSTANTES.v_COD_EST_AOS_OPEDEV)
                  AND OPR.FEC_VTA = v_FEC_PCS --AND TRUNC(OPR.FEC_VTA) = v_FEC_PCS
                  AND OPR.FLG_EGR_ING = PKG_PAB_CONSTANTES.v_FLG_ING
                  AND OPR.FLG_MRD_UTZ_SWF = PKG_PAB_CONSTANTES.v_FLG_MRD_MX
                  AND TSM.FLG_GNR_OPE_CTB =
                        PKG_PAB_CONSTANTES.v_FLG_GNR_OPE_CTB
                  AND CTB.FLG_VGN = PKG_PAB_CONSTANTES.v_REG_VIG
                  AND NVL (OPR.GLS_ADC_EST, '#') <> 'Operación de origen AGP'; -- ELIMINAR OPERACIONES PROVENIENTES DE AGP

      --Inserta datos al HABER Moneda Extranjera en tabla contabilidad
      INSERT INTO PABS_DT_CNTBL_ALMOT (FEC_ISR_OPE,
                                       NUM_FOL_OPE,
                                       FLG_DEB_HAB,
                                       COD_END,
                                       COD_CLv_ITZ,
                                       FEC_CTB,
                                       FEC_OPE,
                                       COD_PTO,
                                       COD_SPO,
                                       NUM_GRT,
                                       DSC_TPO_PZO,
                                       CAN_PZO,
                                       COD_SUB_SEC,
                                       COD_CNp_SUB_SEC,
                                       NUM_DTN_INV,
                                       CAN_END_TDA,
                                       COD_ABT,
                                       DSC_TPO_TTM_MDA,
                                       DSC_TPO_INV,
                                       COD_OPE,
                                       NUM_CNp_CTA_CTB,
                                       COD_DVI,
                                       DSC_TPO_DVI,
                                       DSC_FLL_UNO,
                                       DSC_VRS_FJO_VAR,
                                       DSC_CLv_ATZ,
                                       COD_SUC_OPE,
                                       COD_SUC_ORG,
                                       COD_SUC_DTN,
                                       NUM_MVT_DEB,
                                       NUM_MVT_HAB,
                                       IMp_MON_BSE_DEB,
                                       IMp_MON_BSE_HAB,
                                       IMp_DVI_DEB,
                                       IMp_DVI_HAB,
                                       FLG_IDC_CCC,
                                       NUM_DOC,
                                       DSC_CLv_CNP,
                                       DSC_CNP,
                                       DSC_TPO_CDA,
                                       GLS_IFM_MVT_CTB,
                                       NUM_OPE_ORG,
                                       COD_SNO_CTB_ORG,
                                       COD_SNO_CTB_DTN,
                                       GLS_OBS_UNO,
                                       DSC_RSv_USO_ITR,
                                       COD_CLv_ITZ_GNR_MVT,
                                       FLG_CPS_CJA,
                                       FLG_CTA_ORD,
                                       DSC_TSA_INT,
                                       COD_CLv_OPE,
                                       COD_CTO_GTN,
                                       COD_APO,
                                       DSC_FLL_TRI,
                                       DSC_IDC_UTL,
                                       DSC_IDC_ROT,
                                       FEC_ALA_PTD,
                                       GLS_OBS_DOS,
                                       NUM_OPE_DTN,
                                       DSC_FLL_DOS,
                                       FEC_PCS_REG_CTB)
         SELECT   OPR.FEC_ISR_OPE AS FEC_ISR_OPE,
                  OPR.NUM_FOL_OPE AS NUM_FOL_OPE,
                  PKG_PAB_CONSTANTES.v_FLG_OPE_HAB AS FLG_DEB_HAB,     --Haber
                  PKG_PAB_CONSTANTES.v_COD_ENT_BAN_SANT AS COD_END,
                  PKG_PAB_CONSTANTES.v_COD_SIS_PAB AS COD_CLv_ITZ,
                  TO_NUMBER (TO_CHAR (OPR.FEC_VTA, 'YYYYMMDD')) AS FEC_CTB,
                  TO_NUMBER (TO_CHAR (OPR.FEC_ISR_OPE, 'YYYYMMDD'))
                     AS FEC_OPE,
                  '00' AS COD_PTO,
                  '    ' AS COD_SPO,
                  v_numCero AS NUM_GRT,
                  v_unoEsp AS TPO_PZO,
                  v_numCero AS CAN_PZO,
                  v_numCero AS COD_SUB_SEC,
                  v_numCero AS COD_CNp_SUB_SEC,
                  v_numCero AS NUM_DTN_INV,
                  '    ' AS CAN_END_TDA,
                  v_numCero AS COD_ABT,
                  v_unoEsp AS TPO_TTM_MDA,
                  v_unoEsp AS TPO_INV,
                  '000' AS COD_OPE,
                  CTB.NUM_CNp_CTA_CTB AS NUM_CNp_CTA_CTB,
                  OPR.COD_DVI AS COD_DVI,
                  v_unoEsp AS TPO_DVI,
                  '00000     ' AS FLL_UNO,
                  '                              ' AS VRS_FJO_VAR,
                  '      ' AS CLv_ATZ,
                  PKG_PAB_CONSTANTES.v_COD_SUC_OPERANTE AS COD_SUC_OPE,
                  PKG_PAB_CONSTANTES.V_COD_SUC_ORG AS COD_SUC_ORG, --SOG.COD_SUC_DTN_TPN AS COD_SUC_ORG,
                  CTB.COD_SUC_DTN_TPN AS COD_SUC_DTN,
                  v_numCero AS NUM_MVT_DEB,
                  1 AS NUM_MVT_HAB,
                  v_numCero AS IMp_MON_BSE_DEB,
                  v_numCero AS IMp_MON_BSE_HAB,
                  v_numCero AS IMp_DVI_DEB,
                  OPR.IMP_OPE AS IMp_DVI_HAB,
                  v_unoEsp AS FLG_IDC_CCC,
                  '            ' AS NUM_DOC,
                  '   ' AS CLv_CNP,
                  '              ' AS DSC_CNP,
                  v_unoEsp AS TPO_CDA,
                  CTB.GLS_IFM_MVT_CTB AS GLS_IFM_MVT_CTB,
                  PKG_PAB_CONSTANTES.v_COD_ENT_BAN_SANT || v_cuaCer
                  ||           --NVL(OPR.NUM_REF_CTB,v_numRef) AS NUM_OPE_ORG,
                    LPAD (
                        DECODE (OPR.COD_SIS_SAL,
                                PKG_PAB_CONSTANTES.V_COD_TIP_CAJA,
                                TRIM (OPR.NUM_OPE_SIS_ENT),
                                PKG_PAB_CONSTANTES.V_COD_TIP_OB,
                                TRIM (OPR.NUM_OPE_SIS_ENT),
                                PKG_PAB_CONSTANTES.V_COD_TIP_ABO_CTA,
                                TRIM (OPR.NUM_CTA_BFC),
                                OPR.NUM_FOL_OPE),
                        12,
                        '0'
                     )
                     AS NUM_OPE_ORG,
                  CAO.COD_SNO_CTB AS COD_SNO_CTB_ORG,
                  CAD.COD_SNO_CTB AS COD_SNO_CTB_DTN,
                  '    ' AS GLS_OBS_UNO,
                  '    ' AS RSv_USO_ITR,
                  PKG_PAB_CONSTANTES.v_COD_SIS_PAB || v_unoEsp
                     AS COD_CLv_ITZ_GNR_MVT,
                  v_unoEsp AS FLG_CPS_CJA,
                  v_unoEsp AS FLG_CTA_ORD,
                  v_numCero AS TSA_INT,
                  '   ' AS COD_CLv_OPE,
                  v_unoCuatro AS COD_CTO_GTN,
                  '  ' AS COD_APO,
                  '  ' AS E040_SACCDTGT,
                  v_unoEsp AS IDC_UTL,
                  '  ' AS IDC_ROT,
                  v_FEC_ALTA AS FEC_ALA_PTD,
                  ' 0000000000000000             ' AS GLS_OBS_DOS,
                  PKG_PAB_CONSTANTES.v_COD_ENT_BAN_SANT || v_cuaCer
                  ||           --NVL(OPR.NUM_REF_CTB,v_numRef) AS NUM_OPE_DTN,
                    LPAD (
                        DECODE (OPR.COD_SIS_SAL,
                                PKG_PAB_CONSTANTES.V_COD_TIP_CAJA,
                                TRIM (OPR.NUM_OPE_SIS_ENT),
                                PKG_PAB_CONSTANTES.V_COD_TIP_OB,
                                TRIM (OPR.NUM_OPE_SIS_ENT),
                                PKG_PAB_CONSTANTES.V_COD_TIP_ABO_CTA,
                                TRIM (OPR.NUM_CTA_BFC),
                                OPR.NUM_FOL_OPE),
                        12,
                        '0'
                     )
                     AS NUM_OPE_DTN,
                  '     ' AS FLL_DOS,
                  v_FEC_PCS AS FEC_PCS_REG_CTB
           FROM                  PABS_DT_DETLL_OPRCN OPR
                              -- Join operaciones que se deben contabilizar
                              JOIN
                                 PABS_DT_TIPO_OPRCN_SALID_MNSJ TSM
                              ON TSM.COD_SIS_ENT_SAL = OPR.COD_SIS_ENT
                                 AND TSM.COD_MT_SWF = OPR.COD_MT_SWF
                                 --AND TSM.COD_TPO_OPE_AOS = OPR.COD_TPO_OPE_AOS  --No aplica para los ingresos
                                 AND TSM.FLG_MRD_UTZ_SWF =
                                       OPR.FLG_MRD_UTZ_SWF
                                 AND TSM.FLG_EGR_ING = OPR.FLG_EGR_ING
                           --Join obtiene cuenta contable y sucursal destino
                           JOIN
                              PABS_DT_CENTA_CNTBL CTB
                           ON CTB.COD_SIS_ENT_SAL = OPR.COD_SIS_SAL
                              AND CTB.FLAG_MRD_UTZ_SWF = OPR.FLG_MRD_UTZ_SWF
                        --Join obtiene sucursal Origen
                        JOIN
                           PABS_DT_CENTA_CNTBL SOG
                        ON SOG.COD_SIS_ENT_SAL = OPR.COD_SIS_ENT
                           AND SOG.FLAG_MRD_UTZ_SWF = OPR.FLG_MRD_UTZ_SWF
                     --Join obtiene codigo de aplicacion sistema entrada salida
                     JOIN
                        PABS_DT_SISTM_ENTRD_SALID CAO
                     ON CAO.COD_SIS_ENT_SAL = OPR.COD_SIS_ENT
                  JOIN
                     PABS_DT_SISTM_ENTRD_SALID CAD
                  ON CAD.COD_SIS_ENT_SAL = OPR.COD_SIS_SAL
          WHERE   OPR.COD_EST_AOS IN
                        (PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEABN,
                         PKG_PAB_CONSTANTES.v_COD_EST_AOS_OPEDEV)
                  AND OPR.FEC_VTA = v_FEC_PCS --AND TRUNC(OPR.FEC_VTA) = v_FEC_PCS
                  AND OPR.FLG_EGR_ING = PKG_PAB_CONSTANTES.v_FLG_ING
                  AND OPR.FLG_MRD_UTZ_SWF = PKG_PAB_CONSTANTES.v_FLG_MRD_MX
                  AND TSM.FLG_GNR_OPE_CTB =
                        PKG_PAB_CONSTANTES.v_FLG_GNR_OPE_CTB
                  AND CTB.FLG_VGN = PKG_PAB_CONSTANTES.v_REG_VIG
                  AND NVL (OPR.GLS_ADC_EST, '#') <> 'Operación de origen AGP'; -- ELIMINAR OPERACIONES PROVENIENTES DE AGP

      /**************************************************************************/



      /**************910 DEBE ABONO MN KH*******************/

      INSERT INTO PABS_DT_CNTBL_ALMOT (FEC_ISR_OPE,
                                       NUM_FOL_OPE,
                                       FLG_DEB_HAB,
                                       COD_END,
                                       COD_CLv_ITZ,
                                       FEC_CTB,
                                       FEC_OPE,
                                       COD_PTO,
                                       COD_SPO,
                                       NUM_GRT,
                                       DSC_TPO_PZO,
                                       CAN_PZO,
                                       COD_SUB_SEC,
                                       COD_CNp_SUB_SEC,
                                       NUM_DTN_INV,
                                       CAN_END_TDA,
                                       COD_ABT,
                                       DSC_TPO_TTM_MDA,
                                       DSC_TPO_INV,
                                       COD_OPE,
                                       NUM_CNp_CTA_CTB,
                                       COD_DVI,
                                       DSC_TPO_DVI,
                                       DSC_FLL_UNO,
                                       DSC_VRS_FJO_VAR,
                                       DSC_CLv_ATZ,
                                       COD_SUC_OPE,
                                       COD_SUC_ORG,
                                       COD_SUC_DTN,
                                       NUM_MVT_DEB,
                                       NUM_MVT_HAB,
                                       IMp_MON_BSE_DEB,
                                       IMp_MON_BSE_HAB,
                                       IMp_DVI_DEB,
                                       IMp_DVI_HAB,
                                       FLG_IDC_CCC,
                                       NUM_DOC,
                                       DSC_CLv_CNP,
                                       DSC_CNP,
                                       DSC_TPO_CDA,
                                       GLS_IFM_MVT_CTB,
                                       NUM_OPE_ORG,
                                       COD_SNO_CTB_ORG,
                                       COD_SNO_CTB_DTN,
                                       GLS_OBS_UNO,
                                       DSC_RSv_USO_ITR,
                                       COD_CLv_ITZ_GNR_MVT,
                                       FLG_CPS_CJA,
                                       FLG_CTA_ORD,
                                       DSC_TSA_INT,
                                       COD_CLv_OPE,
                                       COD_CTO_GTN,
                                       COD_APO,
                                       DSC_FLL_TRI,
                                       DSC_IDC_UTL,
                                       DSC_IDC_ROT,
                                       FEC_ALA_PTD,
                                       GLS_OBS_DOS,
                                       NUM_OPE_DTN,
                                       DSC_FLL_DOS,
                                       FEC_PCS_REG_CTB)
         SELECT   OPR.FEC_ING_OPE AS FEC_ISR_OPE,
                  OPR.NUM_FOL_OPE AS NUM_FOL_OPE,
                  PKG_PAB_CONSTANTES.v_FLG_OPE_DEB AS FLG_DEB_HAB,      --Debe
                  PKG_PAB_CONSTANTES.v_COD_ENT_BAN_SANT AS COD_END,
                  PKG_PAB_CONSTANTES.v_COD_SIS_PAB AS COD_CLv_ITZ,
                  TO_NUMBER (TO_CHAR (OPR.FEC_VTA, 'YYYYMMDD')) AS FEC_CTB,
                  TO_NUMBER (TO_CHAR (OPR.FEC_ING_OPE, 'YYYYMMDD'))
                     AS FEC_OPE,
                  '00' AS COD_PTO,
                  v_unoCuatro AS COD_SPO,
                  v_numCero AS NUM_GRT,
                  v_unoEsp AS TPO_PZO,
                  v_numCero AS CAN_PZO,
                  v_numCero AS COD_SUB_SEC,
                  v_numCero AS COD_CNp_SUB_SEC,
                  v_numCero AS NUM_DTN_INV,
                  v_unoCuatro AS CAN_END_TDA,
                  v_numCero AS COD_ABT,
                  v_unoEsp AS TPO_TTM_MDA,
                  v_unoEsp AS TPO_INV,
                  '000' AS COD_OPE,
                  CTB.NUM_CNp_CTA_CTB AS NUM_CNp_CTA_CTB,
                  OPR.COD_DVI AS COD_DVI,
                  v_unoEsp AS TPO_DVI,
                  '00000     ' AS FLL_UNO,
                  '                              ' AS VRS_FJO_VAR,
                  '      ' AS CLv_ATZ,
                  PKG_PAB_CONSTANTES.v_COD_SUC_OPERANTE AS COD_SUC_OPE,
                  PKG_PAB_CONSTANTES.V_COD_SUC_ORG AS COD_SUC_ORG, --SDT.COD_SUC_DTN_TPN AS COD_SUC_ORG,
                  CTB.COD_SUC_DTN_TPN AS COD_SUC_DTN,
                  1 AS NUM_MVT_DEB,
                  v_numCero AS NUM_MVT_HAB,
                  OPR.IMP_OPE AS IMp_MON_BSE_DEB,
                  v_numCero AS IMp_MON_BSE_HAB,
                  v_numCero AS IMp_DVI_DEB,
                  v_numCero AS IMp_DVI_HAB,
                  v_unoEsp AS FLG_IDC_CCC,
                  '            ' AS NUM_DOC,
                  '   ' AS CLv_CNP,
                  '              ' AS DSC_CNP,
                  v_unoEsp AS TPO_CDA,
                  CTB.GLS_IFM_MVT_CTB AS GLS_IFM_MVT_CTB,
                     PKG_PAB_CONSTANTES.v_COD_ENT_BAN_SANT
                  || v_cuaCer
                  || LPAD (OPR.NUM_FOL_OPE, 12, '0')
                     AS NUM_OPE_ORG,
                  CAD.COD_SNO_CTB AS COD_SNO_CTB_ORG,
                  CAO.COD_SNO_CTB AS COD_SNO_CTB_DTN,
                  v_unoCuatro AS GLS_OBS_UNO,
                  v_unoCuatro AS RSv_USO_ITR,
                  PKG_PAB_CONSTANTES.v_COD_SIS_PAB || v_unoEsp
                     AS COD_CLv_ITZ_GNR_MVT,
                  v_unoEsp AS FLG_CPS_CJA,
                  v_unoEsp AS FLG_CTA_ORD,
                  v_numCero AS TSA_INT,
                  '   ' AS COD_CLv_OPE,
                  '    ' AS COD_CTO_GTN,
                  '  ' AS COD_APO,
                  '  ' AS E040_SACCDTGT,
                  v_unoEsp AS IDC_UTL,
                  '  ' AS IDC_ROT,
                  v_FEC_ALTA AS FEC_ALA_PTD,
                  ' 0000000000000000             ' AS GLS_OBS_DOS,
                     PKG_PAB_CONSTANTES.v_COD_ENT_BAN_SANT
                  || v_cuaCer
                  || LPAD (OPR.NUM_FOL_OPE, 12, '0')
                     AS NUM_OPE_DTN,
                  '     ' AS FLL_DOS,
                  v_FEC_PCS AS FEC_PCS_REG_CTB
           FROM               PABS_DT_DETLL_CARGO_ABONO_CAJA OPR
                           JOIN
                              PABS_DT_CENTA_CNTBL CTB
                           ON CTB.COD_SIS_ENT_SAL = OPR.COD_SIS_ENT
                              AND CTB.FLAG_MRD_UTZ_SWF = OPR.FLG_MRD_UTZ_SWF
                        --Join obtiene sucursal destino
                        JOIN
                           PABS_DT_CENTA_CNTBL SDT
                        ON SDT.COD_SIS_ENT_SAL = OPR.COD_SIS_SAL
                           AND SDT.FLAG_MRD_UTZ_SWF = OPR.FLG_MRD_UTZ_SWF
                     JOIN
                        PABS_DT_SISTM_ENTRD_SALID CAO
                     ON CAO.COD_SIS_ENT_SAL = OPR.COD_SIS_SAL
                  JOIN
                     PABS_DT_SISTM_ENTRD_SALID CAD
                  ON CAD.COD_SIS_ENT_SAL = OPR.COD_SIS_ENT
          WHERE       OPR.FEC_VTA = v_FEC_PCS
                  AND OPR.COD_MT_SWF = PKG_PAB_CONSTANTES.V_COD_MT910    --910
                  AND OPR.FLG_MRD_UTZ_SWF = PKG_PAB_CONSTANTES.v_FLG_MRD_MN
                  AND OPR.FLG_CGO_ABN = PKG_PAB_CONSTANTES.V_FLG_MOV_ABN;

      /**************910 DEBE ABONO MX*******************/

      INSERT INTO PABS_DT_CNTBL_ALMOT (FEC_ISR_OPE,
                                       NUM_FOL_OPE,
                                       FLG_DEB_HAB,
                                       COD_END,
                                       COD_CLv_ITZ,
                                       FEC_CTB,
                                       FEC_OPE,
                                       COD_PTO,
                                       COD_SPO,
                                       NUM_GRT,
                                       DSC_TPO_PZO,
                                       CAN_PZO,
                                       COD_SUB_SEC,
                                       COD_CNp_SUB_SEC,
                                       NUM_DTN_INV,
                                       CAN_END_TDA,
                                       COD_ABT,
                                       DSC_TPO_TTM_MDA,
                                       DSC_TPO_INV,
                                       COD_OPE,
                                       NUM_CNp_CTA_CTB,
                                       COD_DVI,
                                       DSC_TPO_DVI,
                                       DSC_FLL_UNO,
                                       DSC_VRS_FJO_VAR,
                                       DSC_CLv_ATZ,
                                       COD_SUC_OPE,
                                       COD_SUC_ORG,
                                       COD_SUC_DTN,
                                       NUM_MVT_DEB,
                                       NUM_MVT_HAB,
                                       IMp_MON_BSE_DEB,
                                       IMp_MON_BSE_HAB,
                                       IMp_DVI_DEB,
                                       IMp_DVI_HAB,
                                       FLG_IDC_CCC,
                                       NUM_DOC,
                                       DSC_CLv_CNP,
                                       DSC_CNP,
                                       DSC_TPO_CDA,
                                       GLS_IFM_MVT_CTB,
                                       NUM_OPE_ORG,
                                       COD_SNO_CTB_ORG,
                                       COD_SNO_CTB_DTN,
                                       GLS_OBS_UNO,
                                       DSC_RSv_USO_ITR,
                                       COD_CLv_ITZ_GNR_MVT,
                                       FLG_CPS_CJA,
                                       FLG_CTA_ORD,
                                       DSC_TSA_INT,
                                       COD_CLv_OPE,
                                       COD_CTO_GTN,
                                       COD_APO,
                                       DSC_FLL_TRI,
                                       DSC_IDC_UTL,
                                       DSC_IDC_ROT,
                                       FEC_ALA_PTD,
                                       GLS_OBS_DOS,
                                       NUM_OPE_DTN,
                                       DSC_FLL_DOS,
                                       FEC_PCS_REG_CTB)
         SELECT   OPR.FEC_ING_OPE AS FEC_ISR_OPE,
                  OPR.NUM_FOL_OPE AS NUM_FOL_OPE,
                  PKG_PAB_CONSTANTES.v_FLG_OPE_DEB AS FLG_DEB_HAB,      --Debe
                  PKG_PAB_CONSTANTES.v_COD_ENT_BAN_SANT AS COD_END,
                  PKG_PAB_CONSTANTES.v_COD_SIS_PAB AS COD_CLv_ITZ,
                  TO_NUMBER (TO_CHAR (OPR.FEC_VTA, 'YYYYMMDD')) AS FEC_CTB,
                  TO_NUMBER (TO_CHAR (OPR.FEC_ING_OPE, 'YYYYMMDD'))
                     AS FEC_OPE,
                  '00' AS COD_PTO,
                  v_unoCuatro AS COD_SPO,
                  v_numCero AS NUM_GRT,
                  v_unoEsp AS TPO_PZO,
                  v_numCero AS CAN_PZO,
                  v_numCero AS COD_SUB_SEC,
                  v_numCero AS COD_CNp_SUB_SEC,
                  v_numCero AS NUM_DTN_INV,
                  v_unoCuatro AS CAN_END_TDA,
                  v_numCero AS COD_ABT,
                  v_unoEsp AS TPO_TTM_MDA,
                  v_unoEsp AS TPO_INV,
                  '000' AS COD_OPE,
                  CTB.NUM_CNp_CTA_CTB AS NUM_CNp_CTA_CTB,
                  OPR.COD_DVI AS COD_DVI,
                  v_unoEsp AS TPO_DVI,
                  '00000     ' AS FLL_UNO,
                  '                              ' AS VRS_FJO_VAR,
                  '      ' AS CLv_ATZ,
                  PKG_PAB_CONSTANTES.v_COD_SUC_OPERANTE AS COD_SUC_OPE,
                  PKG_PAB_CONSTANTES.V_COD_SUC_ORG AS COD_SUC_ORG, --SDT.COD_SUC_DTN_TPN AS COD_SUC_ORG,
                  CTB.COD_SUC_DTN_TPN AS COD_SUC_DTN,
                  1 AS NUM_MVT_DEB,
                  v_numCero AS NUM_MVT_HAB,
                  v_numCero AS IMp_MON_BSE_DEB,
                  v_numCero AS IMp_MON_BSE_HAB,
                  OPR.IMP_OPE AS IMp_DVI_DEB,
                  v_numCero AS IMp_DVI_HAB,
                  v_unoEsp AS FLG_IDC_CCC,
                  '            ' AS NUM_DOC,
                  '   ' AS CLv_CNP,
                  '              ' AS DSC_CNP,
                  v_unoEsp AS TPO_CDA,
                  CTB.GLS_IFM_MVT_CTB AS GLS_IFM_MVT_CTB,
                     PKG_PAB_CONSTANTES.v_COD_ENT_BAN_SANT
                  || v_cuaCer
                  || LPAD (OPR.NUM_FOL_OPE, 12, '0')
                     AS NUM_OPE_ORG,
                  CAO.COD_SNO_CTB AS COD_SNO_CTB_ORG,
                  CAD.COD_SNO_CTB AS COD_SNO_CTB_DTN,
                  v_unoCuatro AS GLS_OBS_UNO,
                  v_unoCuatro AS RSv_USO_ITR,
                  PKG_PAB_CONSTANTES.v_COD_SIS_PAB || v_unoEsp
                     AS COD_CLv_ITZ_GNR_MVT,
                  v_unoEsp AS FLG_CPS_CJA,
                  v_unoEsp AS FLG_CTA_ORD,
                  v_numCero AS TSA_INT,
                  '   ' AS COD_CLv_OPE,
                  '    ' AS COD_CTO_GTN,
                  '  ' AS COD_APO,
                  '  ' AS E040_SACCDTGT,
                  v_unoEsp AS IDC_UTL,
                  '  ' AS IDC_ROT,
                  v_FEC_ALTA AS FEC_ALA_PTD,
                  ' 0000000000000000             ' AS GLS_OBS_DOS,
                     PKG_PAB_CONSTANTES.v_COD_ENT_BAN_SANT
                  || v_cuaCer
                  || LPAD (OPR.NUM_FOL_OPE, 12, '0')
                     AS NUM_OPE_DTN,
                  '     ' AS FLL_DOS,
                  v_FEC_PCS AS FEC_PCS_REG_CTB
           FROM               PABS_DT_DETLL_CARGO_ABONO_CAJA OPR
                           JOIN
                              PABS_DT_CENTA_CNTBL CTB
                           ON CTB.COD_SIS_ENT_SAL = OPR.COD_SIS_ENT
                              AND CTB.FLAG_MRD_UTZ_SWF = OPR.FLG_MRD_UTZ_SWF
                        --Join obtiene sucursal destino
                        JOIN
                           PABS_DT_CENTA_CNTBL SDT
                        ON SDT.COD_SIS_ENT_SAL = OPR.COD_SIS_SAL
                           AND SDT.FLAG_MRD_UTZ_SWF = OPR.FLG_MRD_UTZ_SWF
                     JOIN
                        PABS_DT_SISTM_ENTRD_SALID CAO
                     ON CAO.COD_SIS_ENT_SAL = OPR.COD_SIS_SAL
                  JOIN
                     PABS_DT_SISTM_ENTRD_SALID CAD
                  ON CAD.COD_SIS_ENT_SAL = OPR.COD_SIS_ENT
          WHERE       OPR.FEC_VTA = v_FEC_PCS
                  AND OPR.COD_MT_SWF = PKG_PAB_CONSTANTES.V_COD_MT910    --910
                  AND OPR.FLG_MRD_UTZ_SWF = PKG_PAB_CONSTANTES.v_FLG_MRD_MX
                  AND OPR.FLG_CGO_ABN = PKG_PAB_CONSTANTES.V_FLG_MOV_ABN
                  AND OPR.COD_TPO_OPE_AOS IN ('ABN', 'OPB'); -- SOLAMENTE ABN/OPB

      /******* 910 HABER  ABONO MN*****/
      INSERT INTO PABS_DT_CNTBL_ALMOT (FEC_ISR_OPE,
                                       NUM_FOL_OPE,
                                       FLG_DEB_HAB,
                                       COD_END,
                                       COD_CLv_ITZ,
                                       FEC_CTB,
                                       FEC_OPE,
                                       COD_PTO,
                                       COD_SPO,
                                       NUM_GRT,
                                       DSC_TPO_PZO,
                                       CAN_PZO,
                                       COD_SUB_SEC,
                                       COD_CNp_SUB_SEC,
                                       NUM_DTN_INV,
                                       CAN_END_TDA,
                                       COD_ABT,
                                       DSC_TPO_TTM_MDA,
                                       DSC_TPO_INV,
                                       COD_OPE,
                                       NUM_CNp_CTA_CTB,
                                       COD_DVI,
                                       DSC_TPO_DVI,
                                       DSC_FLL_UNO,
                                       DSC_VRS_FJO_VAR,
                                       DSC_CLv_ATZ,
                                       COD_SUC_OPE,
                                       COD_SUC_ORG,
                                       COD_SUC_DTN,
                                       NUM_MVT_DEB,
                                       NUM_MVT_HAB,
                                       IMp_MON_BSE_DEB,
                                       IMp_MON_BSE_HAB,
                                       IMp_DVI_DEB,
                                       IMp_DVI_HAB,
                                       FLG_IDC_CCC,
                                       NUM_DOC,
                                       DSC_CLv_CNP,
                                       DSC_CNP,
                                       DSC_TPO_CDA,
                                       GLS_IFM_MVT_CTB,
                                       NUM_OPE_ORG,
                                       COD_SNO_CTB_ORG,
                                       COD_SNO_CTB_DTN,
                                       GLS_OBS_UNO,
                                       DSC_RSv_USO_ITR,
                                       COD_CLv_ITZ_GNR_MVT,
                                       FLG_CPS_CJA,
                                       FLG_CTA_ORD,
                                       DSC_TSA_INT,
                                       COD_CLv_OPE,
                                       COD_CTO_GTN,
                                       COD_APO,
                                       DSC_FLL_TRI,
                                       DSC_IDC_UTL,
                                       DSC_IDC_ROT,
                                       FEC_ALA_PTD,
                                       GLS_OBS_DOS,
                                       NUM_OPE_DTN,
                                       DSC_FLL_DOS,
                                       FEC_PCS_REG_CTB)
         SELECT   OPR.FEC_ING_OPE AS FEC_ISR_OPE,
                  OPR.NUM_FOL_OPE AS NUM_FOL_OPE,
                  PKG_PAB_CONSTANTES.v_FLG_OPE_HAB AS FLG_DEB_HAB,     --Haber
                  PKG_PAB_CONSTANTES.v_COD_ENT_BAN_SANT AS COD_END,
                  PKG_PAB_CONSTANTES.v_COD_SIS_PAB AS COD_CLv_ITZ,
                  TO_NUMBER (TO_CHAR (OPR.FEC_VTA, 'YYYYMMDD')) AS FEC_CTB,
                  TO_NUMBER (TO_CHAR (OPR.FEC_ING_OPE, 'YYYYMMDD'))
                     AS FEC_OPE,
                  '00' AS COD_PTO,
                  '    ' AS COD_SPO,
                  v_numCero AS NUM_GRT,
                  v_unoEsp AS TPO_PZO,
                  v_numCero AS CAN_PZO,
                  v_numCero AS COD_SUB_SEC,
                  v_numCero AS COD_CNp_SUB_SEC,
                  v_numCero AS NUM_DTN_INV,
                  '    ' AS CAN_END_TDA,
                  v_numCero AS COD_ABT,
                  v_unoEsp AS TPO_TTM_MDA,
                  v_unoEsp AS TPO_INV,
                  '000' AS COD_OPE,
                  CTB.NUM_CNp_CTA_CTB AS NUM_CNp_CTA_CTB,
                  OPR.COD_DVI AS COD_DVI,
                  v_unoEsp AS TPO_DVI,
                  '00000     ' AS FLL_UNO,
                  '                              ' AS VRS_FJO_VAR,
                  '      ' AS CLv_ATZ,
                  PKG_PAB_CONSTANTES.v_COD_SUC_OPERANTE AS COD_SUC_OPE,
                  PKG_PAB_CONSTANTES.V_COD_SUC_ORG AS COD_SUC_ORG, --SDT.COD_SUC_DTN_TPN AS COD_SUC_ORG,
                  CTB.COD_SUC_DTN_TPN AS COD_SUC_DTN,
                  v_numCero AS NUM_MVT_DEB,
                  1 AS NUM_MVT_HAB,
                  v_numCero AS IMp_MON_BSE_DEB,
                  OPR.IMP_OPE AS IMp_MON_BSE_HAB,
                  v_numCero AS IMp_DVI_DEB,
                  v_numCero AS IMp_DVI_HAB,
                  v_unoEsp AS FLG_IDC_CCC,
                  '            ' AS NUM_DOC,
                  '   ' AS CLv_CNP,
                  '              ' AS DSC_CNP,
                  v_unoEsp AS TPO_CDA,
                  CTB.GLS_IFM_MVT_CTB AS GLS_IFM_MVT_CTB,
                  PKG_PAB_CONSTANTES.v_COD_ENT_BAN_SANT || v_cuaCer
                  || LPAD (
                        DECODE (OPR.COD_SIS_SAL,
                                PKG_PAB_CONSTANTES.V_COD_TIP_FPA_CTA,
                                OPR.num_cta_cte,
                                OPR.NUM_FOL_OPE),
                        12,
                        '0'
                     )
                     AS NUM_OPE_ORG,
                  --CAO.COD_SNO_CTB AS COD_SNO_CTB_ORG,
                  --CAD.COD_SNO_CTB AS COD_SNO_CTB_DTN,
                  CAD.COD_SNO_CTB AS COD_SNO_CTB_ORG,
                  CAO.COD_SNO_CTB AS COD_SNO_CTB_DTN,
                  '    ' AS GLS_OBS_UNO,
                  '    ' AS RSv_USO_ITR,
                  PKG_PAB_CONSTANTES.v_COD_SIS_PAB || v_unoEsp
                     AS COD_CLv_ITZ_GNR_MVT,
                  v_unoEsp AS FLG_CPS_CJA,
                  v_unoEsp AS FLG_CTA_ORD,
                  v_numCero AS TSA_INT,
                  '   ' AS COD_CLv_OPE,
                  '    ' AS COD_CTO_GTN,
                  '  ' AS COD_APO,
                  '  ' AS E040_SACCDTGT,
                  v_unoEsp AS IDC_UTL,
                  '  ' AS IDC_ROT,
                  v_FEC_ALTA AS FEC_ALA_PTD,
                  ' 0000000000000000             ' AS GLS_OBS_DOS,
                  PKG_PAB_CONSTANTES.v_COD_ENT_BAN_SANT || v_cuaCer
                  ||            --LPAD(OPR.NUM_FOL_OPE,12,'0') AS NUM_OPE_DTN,
                    LPAD (
                        DECODE (OPR.COD_SIS_SAL,
                                PKG_PAB_CONSTANTES.V_COD_TIP_FPA_CTA,
                                OPR.num_cta_cte,
                                OPR.NUM_FOL_OPE),
                        12,
                        '0'
                     )
                     AS NUM_OPE_DTN,
                  '     ' AS FLL_DOS,
                  v_FEC_PCS AS FEC_PCS_REG_CTB
           FROM               PABS_DT_DETLL_CARGO_ABONO_CAJA OPR
                           -- Join operaciones que se deben enviar a CONTABILIZAR
                           JOIN
                              PABS_DT_CENTA_CNTBL CTB
                           ON CTB.COD_SIS_ENT_SAL = OPR.COD_SIS_SAL
                              AND CTB.FLAG_MRD_UTZ_SWF = OPR.FLG_MRD_UTZ_SWF
                        --Join obtiene sucursal destino
                        JOIN
                           PABS_DT_CENTA_CNTBL SDT
                        ON SDT.COD_SIS_ENT_SAL = OPR.COD_SIS_ENT
                           AND SDT.FLAG_MRD_UTZ_SWF = OPR.FLG_MRD_UTZ_SWF
                     JOIN
                        PABS_DT_SISTM_ENTRD_SALID CAO
                     ON CAO.COD_SIS_ENT_SAL = OPR.COD_SIS_SAL
                  JOIN
                     PABS_DT_SISTM_ENTRD_SALID CAD
                  ON CAD.COD_SIS_ENT_SAL = OPR.COD_SIS_ENT
          WHERE       OPR.FEC_VTA = v_FEC_PCS
                  AND OPR.COD_MT_SWF = PKG_PAB_CONSTANTES.V_COD_MT910
                  AND OPR.FLG_MRD_UTZ_SWF = PKG_PAB_CONSTANTES.v_FLG_MRD_MN
                  AND OPR.FLG_CGO_ABN = PKG_PAB_CONSTANTES.V_FLG_MOV_ABN;

      /******* 910 HABER  ABONO MX*****/
      INSERT INTO PABS_DT_CNTBL_ALMOT (FEC_ISR_OPE,
                                       NUM_FOL_OPE,
                                       FLG_DEB_HAB,
                                       COD_END,
                                       COD_CLv_ITZ,
                                       FEC_CTB,
                                       FEC_OPE,
                                       COD_PTO,
                                       COD_SPO,
                                       NUM_GRT,
                                       DSC_TPO_PZO,
                                       CAN_PZO,
                                       COD_SUB_SEC,
                                       COD_CNp_SUB_SEC,
                                       NUM_DTN_INV,
                                       CAN_END_TDA,
                                       COD_ABT,
                                       DSC_TPO_TTM_MDA,
                                       DSC_TPO_INV,
                                       COD_OPE,
                                       NUM_CNp_CTA_CTB,
                                       COD_DVI,
                                       DSC_TPO_DVI,
                                       DSC_FLL_UNO,
                                       DSC_VRS_FJO_VAR,
                                       DSC_CLv_ATZ,
                                       COD_SUC_OPE,
                                       COD_SUC_ORG,
                                       COD_SUC_DTN,
                                       NUM_MVT_DEB,
                                       NUM_MVT_HAB,
                                       IMp_MON_BSE_DEB,
                                       IMp_MON_BSE_HAB,
                                       IMp_DVI_DEB,
                                       IMp_DVI_HAB,
                                       FLG_IDC_CCC,
                                       NUM_DOC,
                                       DSC_CLv_CNP,
                                       DSC_CNP,
                                       DSC_TPO_CDA,
                                       GLS_IFM_MVT_CTB,
                                       NUM_OPE_ORG,
                                       COD_SNO_CTB_ORG,
                                       COD_SNO_CTB_DTN,
                                       GLS_OBS_UNO,
                                       DSC_RSv_USO_ITR,
                                       COD_CLv_ITZ_GNR_MVT,
                                       FLG_CPS_CJA,
                                       FLG_CTA_ORD,
                                       DSC_TSA_INT,
                                       COD_CLv_OPE,
                                       COD_CTO_GTN,
                                       COD_APO,
                                       DSC_FLL_TRI,
                                       DSC_IDC_UTL,
                                       DSC_IDC_ROT,
                                       FEC_ALA_PTD,
                                       GLS_OBS_DOS,
                                       NUM_OPE_DTN,
                                       DSC_FLL_DOS,
                                       FEC_PCS_REG_CTB)
         SELECT   OPR.FEC_ING_OPE AS FEC_ISR_OPE,
                  OPR.NUM_FOL_OPE AS NUM_FOL_OPE,
                  PKG_PAB_CONSTANTES.v_FLG_OPE_HAB AS FLG_DEB_HAB,     --Haber
                  PKG_PAB_CONSTANTES.v_COD_ENT_BAN_SANT AS COD_END,
                  PKG_PAB_CONSTANTES.v_COD_SIS_PAB AS COD_CLv_ITZ,
                  TO_NUMBER (TO_CHAR (OPR.FEC_VTA, 'YYYYMMDD')) AS FEC_CTB,
                  TO_NUMBER (TO_CHAR (OPR.FEC_ING_OPE, 'YYYYMMDD'))
                     AS FEC_OPE,
                  '00' AS COD_PTO,
                  '    ' AS COD_SPO,
                  v_numCero AS NUM_GRT,
                  v_unoEsp AS TPO_PZO,
                  v_numCero AS CAN_PZO,
                  v_numCero AS COD_SUB_SEC,
                  v_numCero AS COD_CNp_SUB_SEC,
                  v_numCero AS NUM_DTN_INV,
                  '    ' AS CAN_END_TDA,
                  v_numCero AS COD_ABT,
                  v_unoEsp AS TPO_TTM_MDA,
                  v_unoEsp AS TPO_INV,
                  '000' AS COD_OPE,
                  CTB.NUM_CNp_CTA_CTB AS NUM_CNp_CTA_CTB,
                  OPR.COD_DVI AS COD_DVI,
                  v_unoEsp AS TPO_DVI,
                  '00000     ' AS FLL_UNO,
                  '                              ' AS VRS_FJO_VAR,
                  '      ' AS CLv_ATZ,
                  PKG_PAB_CONSTANTES.v_COD_SUC_OPERANTE AS COD_SUC_OPE,
                  PKG_PAB_CONSTANTES.V_COD_SUC_ORG AS COD_SUC_ORG, --SDT.COD_SUC_DTN_TPN AS COD_SUC_ORG,
                  CTB.COD_SUC_DTN_TPN AS COD_SUC_DTN,
                  v_numCero AS NUM_MVT_DEB,
                  1 AS NUM_MVT_HAB,
                  v_numCero AS IMp_MON_BSE_DEB,
                  v_numCero AS IMp_MON_BSE_HAB,
                  v_numCero AS IMp_DVI_DEB,
                  OPR.IMP_OPE AS IMp_DVI_HAB,
                  v_unoEsp AS FLG_IDC_CCC,
                  '            ' AS NUM_DOC,
                  '   ' AS CLv_CNP,
                  '              ' AS DSC_CNP,
                  v_unoEsp AS TPO_CDA,
                  CTB.GLS_IFM_MVT_CTB AS GLS_IFM_MVT_CTB,
                     PKG_PAB_CONSTANTES.v_COD_ENT_BAN_SANT
                  || v_cuaCer
                  || LPAD (OPR.NUM_FOL_OPE, 12, '0')
                     AS NUM_OPE_ORG,
                  CAO.COD_SNO_CTB AS COD_SNO_CTB_ORG,
                  CAD.COD_SNO_CTB AS COD_SNO_CTB_DTN,
                  '    ' AS GLS_OBS_UNO,
                  '    ' AS RSv_USO_ITR,
                  PKG_PAB_CONSTANTES.v_COD_SIS_PAB || v_unoEsp
                     AS COD_CLv_ITZ_GNR_MVT,
                  v_unoEsp AS FLG_CPS_CJA,
                  v_unoEsp AS FLG_CTA_ORD,
                  v_numCero AS TSA_INT,
                  '   ' AS COD_CLv_OPE,
                  '    ' AS COD_CTO_GTN,
                  '  ' AS COD_APO,
                  '  ' AS E040_SACCDTGT,
                  v_unoEsp AS IDC_UTL,
                  '  ' AS IDC_ROT,
                  v_FEC_ALTA AS FEC_ALA_PTD,
                  ' 0000000000000000             ' AS GLS_OBS_DOS,
                     PKG_PAB_CONSTANTES.v_COD_ENT_BAN_SANT
                  || v_cuaCer
                  || LPAD (OPR.NUM_FOL_OPE, 12, '0')
                     AS NUM_OPE_DTN,
                  '     ' AS FLL_DOS,
                  v_FEC_PCS AS FEC_PCS_REG_CTB
           FROM               PABS_DT_DETLL_CARGO_ABONO_CAJA OPR
                           -- Join operaciones que se deben enviar a CONTABILIZAR
                           JOIN
                              PABS_DT_CENTA_CNTBL CTB
                           ON CTB.COD_SIS_ENT_SAL = OPR.COD_SIS_SAL
                              AND CTB.FLAG_MRD_UTZ_SWF = OPR.FLG_MRD_UTZ_SWF
                        --Join obtiene sucursal destino
                        JOIN
                           PABS_DT_CENTA_CNTBL SDT
                        ON SDT.COD_SIS_ENT_SAL = OPR.COD_SIS_ENT
                           AND SDT.FLAG_MRD_UTZ_SWF = OPR.FLG_MRD_UTZ_SWF
                     JOIN
                        PABS_DT_SISTM_ENTRD_SALID CAO
                     ON CAO.COD_SIS_ENT_SAL = OPR.COD_SIS_SAL
                  JOIN
                     PABS_DT_SISTM_ENTRD_SALID CAD
                  ON CAD.COD_SIS_ENT_SAL = OPR.COD_SIS_ENT
          WHERE       OPR.FEC_VTA = v_FEC_PCS
                  AND OPR.COD_MT_SWF = PKG_PAB_CONSTANTES.V_COD_MT910
                  AND OPR.FLG_MRD_UTZ_SWF = PKG_PAB_CONSTANTES.v_FLG_MRD_MX
                  AND OPR.FLG_CGO_ABN = PKG_PAB_CONSTANTES.V_FLG_MOV_ABN
                  AND OPR.COD_TPO_OPE_AOS IN ('ABN', 'OPB'); -- SOLAMENTE ABN/OPB

      /**************900 DEBE CARGO  MN*******************/

      /*   INSERT INTO PABS_DT_CNTBL_ALMOT
         (
             FEC_ISR_OPE,
             NUM_FOL_OPE,
             FLG_DEB_HAB,
             COD_END,
             COD_CLv_ITZ,
             FEC_CTB,
             FEC_OPE,
             COD_PTO,
             COD_SPO,
             NUM_GRT,
             DSC_TPO_PZO,
             CAN_PZO,
             COD_SUB_SEC,
             COD_CNp_SUB_SEC,
             NUM_DTN_INV,
             CAN_END_TDA,
             COD_ABT,
             DSC_TPO_TTM_MDA,
             DSC_TPO_INV,
             COD_OPE,
             NUM_CNp_CTA_CTB,
             COD_DVI,
             DSC_TPO_DVI,
             DSC_FLL_UNO,
             DSC_VRS_FJO_VAR,
             DSC_CLv_ATZ,
             COD_SUC_OPE,
             COD_SUC_ORG,
             COD_SUC_DTN,
             NUM_MVT_DEB,
             NUM_MVT_HAB,
             IMp_MON_BSE_DEB,
             IMp_MON_BSE_HAB,
             IMp_DVI_DEB,
             IMp_DVI_HAB,
             FLG_IDC_CCC,
             NUM_DOC,
             DSC_CLv_CNP,
             DSC_CNP,
             DSC_TPO_CDA,
             GLS_IFM_MVT_CTB,
             NUM_OPE_ORG,
             COD_SNO_CTB_ORG,
             COD_SNO_CTB_DTN,
             GLS_OBS_UNO,
             DSC_RSv_USO_ITR,
             COD_CLv_ITZ_GNR_MVT,
             FLG_CPS_CJA,
             FLG_CTA_ORD,
             DSC_TSA_INT,
             COD_CLv_OPE,
             COD_CTO_GTN,
             COD_APO,
             DSC_FLL_TRI,
             DSC_IDC_UTL,
             DSC_IDC_ROT,
             FEC_ALA_PTD,
             GLS_OBS_DOS,
             NUM_OPE_DTN,
             DSC_FLL_DOS,
             FEC_PCS_REG_CTB
         )SELECT
             OPR.FEC_ING_OPE AS FEC_ISR_OPE,
             OPR.NUM_FOL_OPE AS NUM_FOL_OPE,
             PKG_PAB_CONSTANTES.v_FLG_OPE_DEB AS FLG_DEB_HAB, --Debe
             PKG_PAB_CONSTANTES.v_COD_ENT_BAN_SANT AS COD_END,
             PKG_PAB_CONSTANTES.v_COD_SIS_PAB AS COD_CLv_ITZ,
             TO_NUMBER(TO_CHAR(OPR.FEC_VTA,'YYYYMMDD')) AS FEC_CTB,
             TO_NUMBER(TO_CHAR(OPR.FEC_ING_OPE,'YYYYMMDD')) AS FEC_OPE,
             '00' AS COD_PTO,
             v_unoCuatro AS COD_SPO,
             v_numCero AS NUM_GRT,
             v_unoEsp AS TPO_PZO,
             v_numCero AS CAN_PZO,
             v_numCero AS COD_SUB_SEC,
             v_numCero AS COD_CNp_SUB_SEC,
             v_numCero AS NUM_DTN_INV,
             v_unoCuatro AS CAN_END_TDA,
             v_numCero AS COD_ABT,
             v_unoEsp AS TPO_TTM_MDA,
             v_unoEsp AS TPO_INV,
             '000' AS COD_OPE,
             CTB.NUM_CNp_CTA_CTB AS NUM_CNp_CTA_CTB,
             OPR.COD_DVI AS COD_DVI,
             v_unoEsp AS TPO_DVI,
             '00000     ' AS FLL_UNO,
             '                              ' AS VRS_FJO_VAR,
             '      ' AS CLv_ATZ,
             PKG_PAB_CONSTANTES.v_COD_SUC_OPERANTE AS COD_SUC_OPE,
             PKG_PAB_CONSTANTES.V_COD_SUC_ORG      AS COD_SUC_ORG, --SDT.COD_SUC_DTN_TPN AS COD_SUC_ORG,
             CTB.COD_SUC_DTN_TPN AS COD_SUC_DTN,
             1 AS NUM_MVT_DEB,
             v_numCero AS NUM_MVT_HAB,
             OPR.IMP_OPE AS IMp_MON_BSE_DEB,
             v_numCero AS IMp_MON_BSE_HAB,
             v_numCero AS IMp_DVI_DEB,
             v_numCero AS IMp_DVI_HAB,
             v_unoEsp AS FLG_IDC_CCC,
             '            ' AS NUM_DOC,
             '   ' AS CLv_CNP,
             '              ' AS DSC_CNP,
             v_unoEsp AS TPO_CDA,
             CTB.GLS_IFM_MVT_CTB AS GLS_IFM_MVT_CTB,
             PKG_PAB_CONSTANTES.v_COD_ENT_BAN_SANT || v_cuaCer || LPAD(OPR.NUM_FOL_OPE,12,'0')AS NUM_OPE_ORG,
             CAD.COD_SNO_CTB AS COD_SNO_CTB_ORG,
             CAO.COD_SNO_CTB AS COD_SNO_CTB_DTN,
             v_unoCuatro AS GLS_OBS_UNO,
             v_unoCuatro AS RSv_USO_ITR,
             PKG_PAB_CONSTANTES.v_COD_SIS_PAB || v_unoEsp AS COD_CLv_ITZ_GNR_MVT,
             v_unoEsp AS FLG_CPS_CJA,
             v_unoEsp AS FLG_CTA_ORD,
             v_numCero AS TSA_INT,
             '   ' AS COD_CLv_OPE,
             '    ' AS COD_CTO_GTN,
             '  ' AS COD_APO,
             '  ' AS E040_SACCDTGT,
             v_unoEsp AS IDC_UTL,
             '  ' AS IDC_ROT,
             v_FEC_ALTA AS FEC_ALA_PTD,
             ' 0000000000000000             ' AS GLS_OBS_DOS,
             PKG_PAB_CONSTANTES.v_COD_ENT_BAN_SANT || v_cuaCer || LPAD(OPR.NUM_FOL_OPE,12,'0') AS NUM_OPE_DTN,
             '     ' AS FLL_DOS,
             v_FEC_PCS AS FEC_PCS_REG_CTB
         FROM PABS_DT_DETLL_CARGO_ABONO_CAJA OPR
             -- Join operaciones que se deben CONTABILIZAR
             JOIN PABS_DT_CENTA_CNTBL CTB ON CTB.COD_SIS_ENT_SAL = OPR.COD_SIS_ENT
                 AND CTB.FLAG_MRD_UTZ_SWF = OPR.FLG_MRD_UTZ_SWF
        --Join obtiene sucursal destino
             JOIN PABS_DT_CENTA_CNTBL SDT ON SDT.COD_SIS_ENT_SAL = OPR.COD_SIS_SAL
                 AND SDT.FLAG_MRD_UTZ_SWF = OPR.FLG_MRD_UTZ_SWF
             JOIN PABS_DT_SISTM_ENTRD_SALID CAO ON CAO.COD_SIS_ENT_SAL = OPR.COD_SIS_SAL
             JOIN PABS_DT_SISTM_ENTRD_SALID CAD ON CAD.COD_SIS_ENT_SAL = OPR.COD_SIS_ENT
         WHERE OPR.FEC_VTA = v_FEC_PCS
             AND OPR.COD_MT_SWF = PKG_PAB_CONSTANTES.V_COD_MT900--900
             AND OPR.FLG_MRD_UTZ_SWF = PKG_PAB_CONSTANTES.v_FLG_MRD_MN
             AND OPR.FLG_CGO_ABN = PKG_PAB_CONSTANTES.V_FLG_MOV_CGO
             AND OPR.COD_TPO_OPE_AOS NOT IN ('MLNS'); */

      /******* 900 HABER CARGO MN *****/

      /*    INSERT INTO PABS_DT_CNTBL_ALMOT
         (
             FEC_ISR_OPE,
             NUM_FOL_OPE,
             FLG_DEB_HAB,
             COD_END,
             COD_CLv_ITZ,
             FEC_CTB,
             FEC_OPE,
             COD_PTO,
             COD_SPO,
             NUM_GRT,
             DSC_TPO_PZO,
             CAN_PZO,
             COD_SUB_SEC,
             COD_CNp_SUB_SEC,
             NUM_DTN_INV,
             CAN_END_TDA,
             COD_ABT,
             DSC_TPO_TTM_MDA,
             DSC_TPO_INV,
             COD_OPE,
             NUM_CNp_CTA_CTB,
             COD_DVI,
             DSC_TPO_DVI,
             DSC_FLL_UNO,
             DSC_VRS_FJO_VAR,
             DSC_CLv_ATZ,
             COD_SUC_OPE,
             COD_SUC_ORG,
             COD_SUC_DTN,
             NUM_MVT_DEB,
             NUM_MVT_HAB,
             IMp_MON_BSE_DEB,
             IMp_MON_BSE_HAB,
             IMp_DVI_DEB,
             IMp_DVI_HAB,
             FLG_IDC_CCC,
             NUM_DOC,
             DSC_CLv_CNP,
             DSC_CNP,
             DSC_TPO_CDA,
             GLS_IFM_MVT_CTB,
             NUM_OPE_ORG,
             COD_SNO_CTB_ORG,
             COD_SNO_CTB_DTN,
             GLS_OBS_UNO,
             DSC_RSv_USO_ITR,
             COD_CLv_ITZ_GNR_MVT,
             FLG_CPS_CJA,
             FLG_CTA_ORD,
             DSC_TSA_INT,
             COD_CLv_OPE,
             COD_CTO_GTN,
             COD_APO,
             DSC_FLL_TRI,
             DSC_IDC_UTL,
             DSC_IDC_ROT,
             FEC_ALA_PTD,
             GLS_OBS_DOS,
             NUM_OPE_DTN,
             DSC_FLL_DOS,
             FEC_PCS_REG_CTB
         )SELECT
            OPR.FEC_ING_OPE AS FEC_ISR_OPE,
             OPR.NUM_FOL_OPE AS NUM_FOL_OPE,
             PKG_PAB_CONSTANTES.v_FLG_OPE_HAB AS FLG_DEB_HAB, --Haber
             PKG_PAB_CONSTANTES.v_COD_ENT_BAN_SANT AS COD_END,
             PKG_PAB_CONSTANTES.v_COD_SIS_PAB AS COD_CLv_ITZ,
             TO_NUMBER(TO_CHAR(OPR.FEC_VTA,'YYYYMMDD')) AS FEC_CTB,
             TO_NUMBER(TO_CHAR(OPR.FEC_ING_OPE,'YYYYMMDD')) AS FEC_OPE,
            '00' AS COD_PTO,
             '    ' AS COD_SPO,
             v_numCero AS NUM_GRT,
             v_unoEsp AS TPO_PZO,
             v_numCero AS CAN_PZO,
             v_numCero AS COD_SUB_SEC,
             v_numCero AS COD_CNp_SUB_SEC,
             v_numCero AS NUM_DTN_INV,
             '    ' AS CAN_END_TDA,
             v_numCero AS COD_ABT,
             v_unoEsp AS TPO_TTM_MDA,
             v_unoEsp AS TPO_INV,
             '000' AS COD_OPE,
             CTB.NUM_CNp_CTA_CTB AS NUM_CNp_CTA_CTB,
             OPR.COD_DVI AS COD_DVI,
             v_unoEsp AS TPO_DVI,
             '00000     ' AS FLL_UNO,
             '                              ' AS VRS_FJO_VAR,
             '      ' AS CLv_ATZ,
             PKG_PAB_CONSTANTES.v_COD_SUC_OPERANTE AS COD_SUC_OPE,
             PKG_PAB_CONSTANTES.V_COD_SUC_ORG      AS COD_SUC_ORG, --SDT.COD_SUC_DTN_TPN AS COD_SUC_ORG,
             CTB.COD_SUC_DTN_TPN AS COD_SUC_DTN,
             v_numCero AS NUM_MVT_DEB,
             1 AS NUM_MVT_HAB,
             v_numCero AS IMp_MON_BSE_DEB,
             OPR.IMP_OPE AS IMp_MON_BSE_HAB,
             v_numCero AS IMp_DVI_DEB,
             v_numCero AS IMp_DVI_HAB,
             v_unoEsp AS FLG_IDC_CCC,
             '            ' AS NUM_DOC,
             '   ' AS CLv_CNP,
             '              ' AS DSC_CNP,
             v_unoEsp AS TPO_CDA,
             CTB.GLS_IFM_MVT_CTB AS GLS_IFM_MVT_CTB,
             PKG_PAB_CONSTANTES.v_COD_ENT_BAN_SANT || v_cuaCer || LPAD(OPR.NUM_FOL_OPE,12,'0') AS NUM_OPE_ORG,
             CAD.COD_SNO_CTB AS COD_SNO_CTB_DTN,
             CAO.COD_SNO_CTB AS COD_SNO_CTB_ORG,
             '    ' AS GLS_OBS_UNO,
             '    ' AS RSv_USO_ITR,
             PKG_PAB_CONSTANTES.v_COD_SIS_PAB || v_unoEsp AS COD_CLv_ITZ_GNR_MVT,
             v_unoEsp AS FLG_CPS_CJA,
             v_unoEsp AS FLG_CTA_ORD,
             v_numCero AS TSA_INT,
             '   ' AS COD_CLv_OPE,
             '    ' AS COD_CTO_GTN,
             '  ' AS COD_APO,
             '  ' AS E040_SACCDTGT,
             v_unoEsp AS IDC_UTL,
             '  ' AS IDC_ROT,
             v_FEC_ALTA AS FEC_ALA_PTD,
             ' 0000000000000000             ' AS GLS_OBS_DOS,
             PKG_PAB_CONSTANTES.v_COD_ENT_BAN_SANT || v_cuaCer || LPAD(OPR.NUM_FOL_OPE,12,'0') AS NUM_OPE_DTN,
             '     ' AS FLL_DOS,
             v_FEC_PCS AS FEC_PCS_REG_CTB
         FROM PABS_DT_DETLL_CARGO_ABONO_CAJA OPR -- Join operaciones que se deben CONTABILIZAR
              JOIN PABS_DT_CENTA_CNTBL CTB ON CTB.COD_SIS_ENT_SAL = OPR.COD_SIS_SAL AND CTB.FLAG_MRD_UTZ_SWF = OPR.FLG_MRD_UTZ_SWF --'LBTR
         --Join obtiene sucursal destino
         JOIN PABS_DT_CENTA_CNTBL SDT ON SDT.COD_SIS_ENT_SAL = OPR.COD_SIS_ENT  AND SDT.FLAG_MRD_UTZ_SWF = OPR.FLG_MRD_UTZ_SWF --'LBTR'
         JOIN PABS_DT_SISTM_ENTRD_SALID CAO ON CAO.COD_SIS_ENT_SAL = OPR.COD_SIS_SAL --'SWF'
         JOIN PABS_DT_SISTM_ENTRD_SALID CAD ON CAD.COD_SIS_ENT_SAL = OPR.COD_SIS_ENT --'SWF'
         WHERE OPR.FEC_VTA = v_FEC_PCS
             AND OPR.COD_MT_SWF = PKG_PAB_CONSTANTES.V_COD_MT900--900
             AND OPR.FLG_MRD_UTZ_SWF = PKG_PAB_CONSTANTES.v_FLG_MRD_MN
             AND OPR.FLG_CGO_ABN = PKG_PAB_CONSTANTES.V_FLG_MOV_CGO
             AND OPR.COD_TPO_OPE_AOS NOT IN ('MLNS');  */

      /**************900 DEBE CARGO  MX*******************/

      INSERT INTO PABS_DT_CNTBL_ALMOT (FEC_ISR_OPE,
                                       NUM_FOL_OPE,
                                       FLG_DEB_HAB,
                                       COD_END,
                                       COD_CLv_ITZ,
                                       FEC_CTB,
                                       FEC_OPE,
                                       COD_PTO,
                                       COD_SPO,
                                       NUM_GRT,
                                       DSC_TPO_PZO,
                                       CAN_PZO,
                                       COD_SUB_SEC,
                                       COD_CNp_SUB_SEC,
                                       NUM_DTN_INV,
                                       CAN_END_TDA,
                                       COD_ABT,
                                       DSC_TPO_TTM_MDA,
                                       DSC_TPO_INV,
                                       COD_OPE,
                                       NUM_CNp_CTA_CTB,
                                       COD_DVI,
                                       DSC_TPO_DVI,
                                       DSC_FLL_UNO,
                                       DSC_VRS_FJO_VAR,
                                       DSC_CLv_ATZ,
                                       COD_SUC_OPE,
                                       COD_SUC_ORG,
                                       COD_SUC_DTN,
                                       NUM_MVT_DEB,
                                       NUM_MVT_HAB,
                                       IMp_MON_BSE_DEB,
                                       IMp_MON_BSE_HAB,
                                       IMp_DVI_DEB,
                                       IMp_DVI_HAB,
                                       FLG_IDC_CCC,
                                       NUM_DOC,
                                       DSC_CLv_CNP,
                                       DSC_CNP,
                                       DSC_TPO_CDA,
                                       GLS_IFM_MVT_CTB,
                                       NUM_OPE_ORG,
                                       COD_SNO_CTB_ORG,
                                       COD_SNO_CTB_DTN,
                                       GLS_OBS_UNO,
                                       DSC_RSv_USO_ITR,
                                       COD_CLv_ITZ_GNR_MVT,
                                       FLG_CPS_CJA,
                                       FLG_CTA_ORD,
                                       DSC_TSA_INT,
                                       COD_CLv_OPE,
                                       COD_CTO_GTN,
                                       COD_APO,
                                       DSC_FLL_TRI,
                                       DSC_IDC_UTL,
                                       DSC_IDC_ROT,
                                       FEC_ALA_PTD,
                                       GLS_OBS_DOS,
                                       NUM_OPE_DTN,
                                       DSC_FLL_DOS,
                                       FEC_PCS_REG_CTB)
         SELECT   OPR.FEC_ING_OPE AS FEC_ISR_OPE,
                  OPR.NUM_FOL_OPE AS NUM_FOL_OPE,
                  PKG_PAB_CONSTANTES.v_FLG_OPE_DEB AS FLG_DEB_HAB,      --Debe
                  PKG_PAB_CONSTANTES.v_COD_ENT_BAN_SANT AS COD_END,
                  PKG_PAB_CONSTANTES.v_COD_SIS_PAB AS COD_CLv_ITZ,
                  TO_NUMBER (TO_CHAR (OPR.FEC_VTA, 'YYYYMMDD')) AS FEC_CTB,
                  TO_NUMBER (TO_CHAR (OPR.FEC_ING_OPE, 'YYYYMMDD'))
                     AS FEC_OPE,
                  '00' AS COD_PTO,
                  v_unoCuatro AS COD_SPO,
                  v_numCero AS NUM_GRT,
                  v_unoEsp AS TPO_PZO,
                  v_numCero AS CAN_PZO,
                  v_numCero AS COD_SUB_SEC,
                  v_numCero AS COD_CNp_SUB_SEC,
                  v_numCero AS NUM_DTN_INV,
                  v_unoCuatro AS CAN_END_TDA,
                  v_numCero AS COD_ABT,
                  v_unoEsp AS TPO_TTM_MDA,
                  v_unoEsp AS TPO_INV,
                  '000' AS COD_OPE,
                  CTB.NUM_CNp_CTA_CTB AS NUM_CNp_CTA_CTB,
                  OPR.COD_DVI AS COD_DVI,
                  v_unoEsp AS TPO_DVI,
                  '00000     ' AS FLL_UNO,
                  '                              ' AS VRS_FJO_VAR,
                  '      ' AS CLv_ATZ,
                  PKG_PAB_CONSTANTES.v_COD_SUC_OPERANTE AS COD_SUC_OPE,
                  PKG_PAB_CONSTANTES.V_COD_SUC_ORG AS COD_SUC_ORG, --SDT.COD_SUC_DTN_TPN AS COD_SUC_ORG,
                  CTB.COD_SUC_DTN_TPN AS COD_SUC_DTN,
                  1 AS NUM_MVT_DEB,
                  v_numCero AS NUM_MVT_HAB,
                  v_numCero AS IMp_MON_BSE_DEB,
                  v_numCero AS IMp_MON_BSE_HAB,
                  OPR.IMP_OPE AS IMp_DVI_DEB,
                  v_numCero AS IMp_DVI_HAB,
                  v_unoEsp AS FLG_IDC_CCC,
                  '            ' AS NUM_DOC,
                  '   ' AS CLv_CNP,
                  '              ' AS DSC_CNP,
                  v_unoEsp AS TPO_CDA,
                  CTB.GLS_IFM_MVT_CTB AS GLS_IFM_MVT_CTB,
                     PKG_PAB_CONSTANTES.v_COD_ENT_BAN_SANT
                  || v_cuaCer
                  || LPAD (OPR.NUM_FOL_OPE, 12, '0')
                     AS NUM_OPE_ORG,
                  CAO.COD_SNO_CTB AS COD_SNO_CTB_ORG,
                  CAD.COD_SNO_CTB AS COD_SNO_CTB_DTN,
                  v_unoCuatro AS GLS_OBS_UNO,
                  v_unoCuatro AS RSv_USO_ITR,
                  PKG_PAB_CONSTANTES.v_COD_SIS_PAB || v_unoEsp
                     AS COD_CLv_ITZ_GNR_MVT,
                  v_unoEsp AS FLG_CPS_CJA,
                  v_unoEsp AS FLG_CTA_ORD,
                  v_numCero AS TSA_INT,
                  '   ' AS COD_CLv_OPE,
                  '    ' AS COD_CTO_GTN,
                  '  ' AS COD_APO,
                  '  ' AS E040_SACCDTGT,
                  v_unoEsp AS IDC_UTL,
                  '  ' AS IDC_ROT,
                  v_FEC_ALTA AS FEC_ALA_PTD,
                  ' 0000000000000000             ' AS GLS_OBS_DOS,
                     PKG_PAB_CONSTANTES.v_COD_ENT_BAN_SANT
                  || v_cuaCer
                  || LPAD (OPR.NUM_FOL_OPE, 12, '0')
                     AS NUM_OPE_DTN,
                  '     ' AS FLL_DOS,
                  v_FEC_PCS AS FEC_PCS_REG_CTB
           FROM               PABS_DT_DETLL_CARGO_ABONO_CAJA OPR
                           -- Join operaciones que se deben CONTABILIZAR
                           JOIN
                              PABS_DT_CENTA_CNTBL CTB
                           ON CTB.COD_SIS_ENT_SAL = OPR.COD_SIS_ENT
                              AND CTB.FLAG_MRD_UTZ_SWF = OPR.FLG_MRD_UTZ_SWF
                        --Join obtiene sucursal destino
                        JOIN
                           PABS_DT_CENTA_CNTBL SDT
                        ON SDT.COD_SIS_ENT_SAL = OPR.COD_SIS_SAL
                           AND SDT.FLAG_MRD_UTZ_SWF = OPR.FLG_MRD_UTZ_SWF
                     JOIN
                        PABS_DT_SISTM_ENTRD_SALID CAO
                     ON CAO.COD_SIS_ENT_SAL = OPR.COD_SIS_SAL          --'SWF'
                  JOIN
                     PABS_DT_SISTM_ENTRD_SALID CAD
                  ON CAD.COD_SIS_ENT_SAL = OPR.COD_SIS_ENT             --'SWF'
          WHERE       OPR.FEC_VTA = v_FEC_PCS
                  AND OPR.COD_MT_SWF = PKG_PAB_CONSTANTES.V_COD_MT900    --900
                  AND OPR.FLG_MRD_UTZ_SWF = PKG_PAB_CONSTANTES.v_FLG_MRD_MX
                  AND OPR.FLG_CGO_ABN = PKG_PAB_CONSTANTES.V_FLG_MOV_CGO
                  AND OPR.COD_TPO_OPE_AOS IN ('ABN', 'OPB'); -- SOLAMENTE ABN/OPB  ;

      /******* 900 HABER CARGO MX *****/

      INSERT INTO PABS_DT_CNTBL_ALMOT (FEC_ISR_OPE,
                                       NUM_FOL_OPE,
                                       FLG_DEB_HAB,
                                       COD_END,
                                       COD_CLv_ITZ,
                                       FEC_CTB,
                                       FEC_OPE,
                                       COD_PTO,
                                       COD_SPO,
                                       NUM_GRT,
                                       DSC_TPO_PZO,
                                       CAN_PZO,
                                       COD_SUB_SEC,
                                       COD_CNp_SUB_SEC,
                                       NUM_DTN_INV,
                                       CAN_END_TDA,
                                       COD_ABT,
                                       DSC_TPO_TTM_MDA,
                                       DSC_TPO_INV,
                                       COD_OPE,
                                       NUM_CNp_CTA_CTB,
                                       COD_DVI,
                                       DSC_TPO_DVI,
                                       DSC_FLL_UNO,
                                       DSC_VRS_FJO_VAR,
                                       DSC_CLv_ATZ,
                                       COD_SUC_OPE,
                                       COD_SUC_ORG,
                                       COD_SUC_DTN,
                                       NUM_MVT_DEB,
                                       NUM_MVT_HAB,
                                       IMp_MON_BSE_DEB,
                                       IMp_MON_BSE_HAB,
                                       IMp_DVI_DEB,
                                       IMp_DVI_HAB,
                                       FLG_IDC_CCC,
                                       NUM_DOC,
                                       DSC_CLv_CNP,
                                       DSC_CNP,
                                       DSC_TPO_CDA,
                                       GLS_IFM_MVT_CTB,
                                       NUM_OPE_ORG,
                                       COD_SNO_CTB_ORG,
                                       COD_SNO_CTB_DTN,
                                       GLS_OBS_UNO,
                                       DSC_RSv_USO_ITR,
                                       COD_CLv_ITZ_GNR_MVT,
                                       FLG_CPS_CJA,
                                       FLG_CTA_ORD,
                                       DSC_TSA_INT,
                                       COD_CLv_OPE,
                                       COD_CTO_GTN,
                                       COD_APO,
                                       DSC_FLL_TRI,
                                       DSC_IDC_UTL,
                                       DSC_IDC_ROT,
                                       FEC_ALA_PTD,
                                       GLS_OBS_DOS,
                                       NUM_OPE_DTN,
                                       DSC_FLL_DOS,
                                       FEC_PCS_REG_CTB)
         SELECT   OPR.FEC_ING_OPE AS FEC_ISR_OPE,
                  OPR.NUM_FOL_OPE AS NUM_FOL_OPE,
                  PKG_PAB_CONSTANTES.v_FLG_OPE_HAB AS FLG_DEB_HAB,     --Haber
                  PKG_PAB_CONSTANTES.v_COD_ENT_BAN_SANT AS COD_END,
                  PKG_PAB_CONSTANTES.v_COD_SIS_PAB AS COD_CLv_ITZ,
                  TO_NUMBER (TO_CHAR (OPR.FEC_VTA, 'YYYYMMDD')) AS FEC_CTB,
                  TO_NUMBER (TO_CHAR (OPR.FEC_ING_OPE, 'YYYYMMDD'))
                     AS FEC_OPE,
                  '00' AS COD_PTO,
                  '    ' AS COD_SPO,
                  v_numCero AS NUM_GRT,
                  v_unoEsp AS TPO_PZO,
                  v_numCero AS CAN_PZO,
                  v_numCero AS COD_SUB_SEC,
                  v_numCero AS COD_CNp_SUB_SEC,
                  v_numCero AS NUM_DTN_INV,
                  '    ' AS CAN_END_TDA,
                  v_numCero AS COD_ABT,
                  v_unoEsp AS TPO_TTM_MDA,
                  v_unoEsp AS TPO_INV,
                  '000' AS COD_OPE,
                  CTB.NUM_CNp_CTA_CTB AS NUM_CNp_CTA_CTB,
                  OPR.COD_DVI AS COD_DVI,
                  v_unoEsp AS TPO_DVI,
                  '00000     ' AS FLL_UNO,
                  '                              ' AS VRS_FJO_VAR,
                  '      ' AS CLv_ATZ,
                  PKG_PAB_CONSTANTES.v_COD_SUC_OPERANTE AS COD_SUC_OPE,
                  PKG_PAB_CONSTANTES.V_COD_SUC_ORG AS COD_SUC_ORG, --SDT.COD_SUC_DTN_TPN AS COD_SUC_ORG,
                  CTB.COD_SUC_DTN_TPN AS COD_SUC_DTN,
                  v_numCero AS NUM_MVT_DEB,
                  1 AS NUM_MVT_HAB,
                  v_numCero AS IMp_MON_BSE_DEB,
                  v_numCero AS IMp_MON_BSE_HAB,
                  v_numCero AS IMp_DVI_DEB,
                  OPR.IMP_OPE AS IMp_DVI_HAB,
                  v_unoEsp AS FLG_IDC_CCC,
                  '            ' AS NUM_DOC,
                  '   ' AS CLv_CNP,
                  '              ' AS DSC_CNP,
                  v_unoEsp AS TPO_CDA,
                  CTB.GLS_IFM_MVT_CTB AS GLS_IFM_MVT_CTB,
                     PKG_PAB_CONSTANTES.v_COD_ENT_BAN_SANT
                  || v_cuaCer
                  || LPAD (OPR.NUM_FOL_OPE, 12, '0')
                     AS NUM_OPE_ORG,
                  CAO.COD_SNO_CTB AS COD_SNO_CTB_ORG,
                  CAD.COD_SNO_CTB AS COD_SNO_CTB_DTN,
                  '    ' AS GLS_OBS_UNO,
                  '    ' AS RSv_USO_ITR,
                  PKG_PAB_CONSTANTES.v_COD_SIS_PAB || v_unoEsp
                     AS COD_CLv_ITZ_GNR_MVT,
                  v_unoEsp AS FLG_CPS_CJA,
                  v_unoEsp AS FLG_CTA_ORD,
                  v_numCero AS TSA_INT,
                  '   ' AS COD_CLv_OPE,
                  '    ' AS COD_CTO_GTN,
                  '  ' AS COD_APO,
                  '  ' AS E040_SACCDTGT,
                  v_unoEsp AS IDC_UTL,
                  '  ' AS IDC_ROT,
                  v_FEC_ALTA AS FEC_ALA_PTD,
                  ' 0000000000000000             ' AS GLS_OBS_DOS,
                     PKG_PAB_CONSTANTES.v_COD_ENT_BAN_SANT
                  || v_cuaCer
                  || LPAD (OPR.NUM_FOL_OPE, 12, '0')
                     AS NUM_OPE_DTN,
                  '     ' AS FLL_DOS,
                  v_FEC_PCS AS FEC_PCS_REG_CTB
           FROM               PABS_DT_DETLL_CARGO_ABONO_CAJA OPR -- Join operaciones que se deben CONTABILIZAR
                           JOIN
                              PABS_DT_CENTA_CNTBL CTB
                           ON CTB.COD_SIS_ENT_SAL = OPR.COD_SIS_SAL
                              AND CTB.FLAG_MRD_UTZ_SWF = OPR.FLG_MRD_UTZ_SWF --'LBTR
                        --Join obtiene sucursal destino
                        JOIN
                           PABS_DT_CENTA_CNTBL SDT
                        ON SDT.COD_SIS_ENT_SAL = OPR.COD_SIS_ENT
                           AND SDT.FLAG_MRD_UTZ_SWF = OPR.FLG_MRD_UTZ_SWF --'LBTR'
                     JOIN
                        PABS_DT_SISTM_ENTRD_SALID CAO
                     ON CAO.COD_SIS_ENT_SAL = OPR.COD_SIS_SAL          --'SWF'
                  JOIN
                     PABS_DT_SISTM_ENTRD_SALID CAD
                  ON CAD.COD_SIS_ENT_SAL = OPR.COD_SIS_ENT             --'SWF'
          WHERE       OPR.FEC_VTA = v_FEC_PCS
                  AND OPR.COD_MT_SWF = PKG_PAB_CONSTANTES.V_COD_MT900    --900
                  AND OPR.FLG_MRD_UTZ_SWF = PKG_PAB_CONSTANTES.v_FLG_MRD_MX
                  AND OPR.FLG_CGO_ABN = PKG_PAB_CONSTANTES.V_FLG_MOV_CGO
                  AND OPR.COD_TPO_OPE_AOS IN ('ABN', 'OPB'); -- SOLAMENTE ABN/OPB  ;


      /*****MT 200 ***/

      /*********MT 200 MOVIMIENTO 2DO BANCO *************/

      --Inserta datos al DEBE Moneda Extranjera en tabla contabilidad
      INSERT INTO PABS_DT_CNTBL_ALMOT (FEC_ISR_OPE,
                                       NUM_FOL_OPE,
                                       FLG_DEB_HAB,
                                       COD_END,
                                       COD_CLv_ITZ,
                                       FEC_CTB,
                                       FEC_OPE,
                                       COD_PTO,
                                       COD_SPO,
                                       NUM_GRT,
                                       DSC_TPO_PZO,
                                       CAN_PZO,
                                       COD_SUB_SEC,
                                       COD_CNp_SUB_SEC,
                                       NUM_DTN_INV,
                                       CAN_END_TDA,
                                       COD_ABT,
                                       DSC_TPO_TTM_MDA,
                                       DSC_TPO_INV,
                                       COD_OPE,
                                       NUM_CNp_CTA_CTB,
                                       COD_DVI,
                                       DSC_TPO_DVI,
                                       DSC_FLL_UNO,
                                       DSC_VRS_FJO_VAR,
                                       DSC_CLv_ATZ,
                                       COD_SUC_OPE,
                                       COD_SUC_ORG,
                                       COD_SUC_DTN,
                                       NUM_MVT_DEB,
                                       NUM_MVT_HAB,
                                       IMp_MON_BSE_DEB,
                                       IMp_MON_BSE_HAB,
                                       IMp_DVI_DEB,
                                       IMp_DVI_HAB,
                                       FLG_IDC_CCC,
                                       NUM_DOC,
                                       DSC_CLv_CNP,
                                       DSC_CNP,
                                       DSC_TPO_CDA,
                                       GLS_IFM_MVT_CTB,
                                       NUM_OPE_ORG,
                                       COD_SNO_CTB_ORG,
                                       COD_SNO_CTB_DTN,
                                       GLS_OBS_UNO,
                                       DSC_RSv_USO_ITR,
                                       COD_CLv_ITZ_GNR_MVT,
                                       FLG_CPS_CJA,
                                       FLG_CTA_ORD,
                                       DSC_TSA_INT,
                                       COD_CLv_OPE,
                                       COD_CTO_GTN,
                                       COD_APO,
                                       DSC_FLL_TRI,
                                       DSC_IDC_UTL,
                                       DSC_IDC_ROT,
                                       FEC_ALA_PTD,
                                       GLS_OBS_DOS,
                                       NUM_OPE_DTN,
                                       DSC_FLL_DOS,
                                       FEC_PCS_REG_CTB)
         SELECT   SYSDATE AS FEC_ISR_OPE,
                  OPR.NUM_FOL_OPE AS NUM_FOL_OPE,
                  PKG_PAB_CONSTANTES.v_FLG_OPE_DEB AS FLG_DEB_HAB,      --Debe
                  PKG_PAB_CONSTANTES.v_COD_ENT_BAN_SANT AS COD_END,
                  PKG_PAB_CONSTANTES.v_COD_SIS_PAB AS COD_CLv_ITZ,
                  TO_NUMBER (TO_CHAR (OPR.FEC_VTA, 'YYYYMMDD')) AS FEC_CTB,
                  TO_NUMBER (TO_CHAR (OPR.FEC_ISR_OPE, 'YYYYMMDD'))
                     AS FEC_OPE,
                  '00' AS COD_PTO,
                  v_unoCuatro AS COD_SPO,
                  v_numCero AS NUM_GRT,
                  v_unoEsp AS TPO_PZO,
                  v_numCero AS CAN_PZO,
                  v_numCero AS COD_SUB_SEC,
                  v_numCero AS COD_CNp_SUB_SEC,
                  v_numCero AS NUM_DTN_INV,
                  v_unoCuatro AS CAN_END_TDA,
                  v_numCero AS COD_ABT,
                  v_unoEsp AS TPO_TTM_MDA,
                  v_unoEsp AS TPO_INV,
                  '000' AS COD_OPE,
                  CTB.NUM_CNp_CTA_CTB AS NUM_CNp_CTA_CTB,
                  OPR.COD_DVI AS COD_DVI,
                  v_unoEsp AS TPO_DVI,
                  '00000     ' AS FLL_UNO,
                  '                              ' AS VRS_FJO_VAR,
                  '      ' AS CLv_ATZ,
                  PKG_PAB_CONSTANTES.v_COD_SUC_OPERANTE AS COD_SUC_OPE,
                  PKG_PAB_CONSTANTES.V_COD_SUC_ORG AS COD_SUC_ORG, --CTB.COD_SUC_DTN_TPN AS COD_SUC_ORG,
                  CTB.COD_SUC_DTN_TPN AS COD_SUC_DTN,
                  1 AS NUM_MVT_DEB,
                  v_numCero AS NUM_MVT_HAB,
                  v_numCero AS IMp_MON_BSE_DEB,
                  v_numCero AS IMp_MON_BSE_HAB,
                  OPR.IMP_OPE AS IMp_DVI_DEB,
                  v_numCero AS IMp_DVI_HAB,
                  v_unoEsp AS FLG_IDC_CCC,
                  '            ' AS NUM_DOC,
                  '   ' AS CLv_CNP,
                  '              ' AS DSC_CNP,
                  v_unoEsp AS TPO_CDA,
                  CTB.GLS_IFM_MVT_CTB AS GLS_IFM_MVT_CTB,
                     PKG_PAB_CONSTANTES.v_COD_ENT_BAN_SANT
                  || v_cuaCer
                  || LPAD (OPR.NUM_FOL_OPE, 12, '0')
                     AS NUM_OPE_ORG,
                  CAO.COD_SNO_CTB AS COD_SNO_CTB_ORG,
                  CAD.COD_SNO_CTB AS COD_SNO_CTB_DTN,
                  v_unoCuatro AS GLS_OBS_UNO,
                  v_unoCuatro AS RSv_USO_ITR,
                  PKG_PAB_CONSTANTES.v_COD_SIS_PAB || v_unoEsp
                     AS COD_CLv_ITZ_GNR_MVT,
                  v_unoEsp AS FLG_CPS_CJA,
                  v_unoEsp AS FLG_CTA_ORD,
                  v_numCero AS TSA_INT,
                  '   ' AS COD_CLv_OPE,
                  '    ' AS COD_CTO_GTN,
                  '  ' AS COD_APO,
                  '  ' AS E040_SACCDTGT,
                  v_unoEsp AS IDC_UTL,
                  '  ' AS IDC_ROT,
                  v_FEC_ALTA AS FEC_ALA_PTD,
                  ' 0000000000000000             ' AS GLS_OBS_DOS,
                     PKG_PAB_CONSTANTES.v_COD_ENT_BAN_SANT
                  || v_cuaCer
                  || LPAD (OPR.NUM_FOL_OPE, 12, '0')
                     AS NUM_OPE_DTN,
                  '     ' AS FLL_DOS,
                  v_FEC_PCS AS FEC_PCS_REG_CTB
           FROM                  PABS_DT_DETLL_OPRCN OPR
                              -- Join operaciones que se deben contabilizar
                              JOIN
                                 PABS_DT_TIPO_OPRCN_SALID_MNSJ TSM
                              ON TSM.COD_SIS_ENT_SAL = OPR.COD_SIS_SAL
                                 AND TSM.COD_MT_SWF = OPR.COD_MT_SWF
                                 AND TSM.COD_TPO_OPE_AOS =
                                       OPR.COD_TPO_OPE_AOS
                                 AND TSM.FLG_MRD_UTZ_SWF =
                                       OPR.FLG_MRD_UTZ_SWF
                                 AND TSM.FLG_EGR_ING = OPR.FLG_EGR_ING
                           --Join obtiene cuenta contable y sucursal origen
                           JOIN
                              PABS_DT_CENTA_CNTBL CTB
                           ON CTB.COD_SIS_ENT_SAL = OPR.COD_SIS_SAL
                              AND CTB.FLAG_MRD_UTZ_SWF = OPR.FLG_MRD_UTZ_SWF
                        --Join obtiene sucursal destino
                        JOIN
                           PABS_DT_CENTA_CNTBL SDT
                        ON SDT.COD_SIS_ENT_SAL = OPR.COD_SIS_ENT
                           AND SDT.FLAG_MRD_UTZ_SWF = OPR.FLG_MRD_UTZ_SWF
                     --Join obtiene codigo de aplicacion sistema entrada salida
                     JOIN
                        PABS_DT_SISTM_ENTRD_SALID CAO
                     ON CAO.COD_SIS_ENT_SAL = OPR.COD_SIS_ENT
                  JOIN
                     PABS_DT_SISTM_ENTRD_SALID CAD
                  ON CAD.COD_SIS_ENT_SAL = OPR.COD_SIS_SAL
          WHERE   OPR.COD_EST_AOS = PKG_PAB_CONSTANTES.v_COD_EST_AOS_OPEPAG
                  AND OPR.FEC_VTA = v_FEC_PCS --AND TRUNC(OPR.FEC_VTA) = v_FEC_PCS
                  AND OPR.FLG_EGR_ING = PKG_PAB_CONSTANTES.v_FLG_EGR
                  AND OPR.FLG_MRD_UTZ_SWF = PKG_PAB_CONSTANTES.v_FLG_MRD_MX
                  AND TSM.FLG_GNR_OPE_CTB =
                        PKG_PAB_CONSTANTES.v_FLG_GNR_OPE_CTB
                  AND CTB.FLG_VGN = PKG_PAB_CONSTANTES.v_REG_VIG
                  AND OPR.COD_MT_SWF = PKG_PAB_CONSTANTES.V_COD_MT200;

      --Inserta datos al HABER Moneda Extranjera en tabla contabilidad
      INSERT INTO PABS_DT_CNTBL_ALMOT (FEC_ISR_OPE,
                                       NUM_FOL_OPE,
                                       FLG_DEB_HAB,
                                       COD_END,
                                       COD_CLv_ITZ,
                                       FEC_CTB,
                                       FEC_OPE,
                                       COD_PTO,
                                       COD_SPO,
                                       NUM_GRT,
                                       DSC_TPO_PZO,
                                       CAN_PZO,
                                       COD_SUB_SEC,
                                       COD_CNp_SUB_SEC,
                                       NUM_DTN_INV,
                                       CAN_END_TDA,
                                       COD_ABT,
                                       DSC_TPO_TTM_MDA,
                                       DSC_TPO_INV,
                                       COD_OPE,
                                       NUM_CNp_CTA_CTB,
                                       COD_DVI,
                                       DSC_TPO_DVI,
                                       DSC_FLL_UNO,
                                       DSC_VRS_FJO_VAR,
                                       DSC_CLv_ATZ,
                                       COD_SUC_OPE,
                                       COD_SUC_ORG,
                                       COD_SUC_DTN,
                                       NUM_MVT_DEB,
                                       NUM_MVT_HAB,
                                       IMp_MON_BSE_DEB,
                                       IMp_MON_BSE_HAB,
                                       IMp_DVI_DEB,
                                       IMp_DVI_HAB,
                                       FLG_IDC_CCC,
                                       NUM_DOC,
                                       DSC_CLv_CNP,
                                       DSC_CNP,
                                       DSC_TPO_CDA,
                                       GLS_IFM_MVT_CTB,
                                       NUM_OPE_ORG,
                                       COD_SNO_CTB_ORG,
                                       COD_SNO_CTB_DTN,
                                       GLS_OBS_UNO,
                                       DSC_RSv_USO_ITR,
                                       COD_CLv_ITZ_GNR_MVT,
                                       FLG_CPS_CJA,
                                       FLG_CTA_ORD,
                                       DSC_TSA_INT,
                                       COD_CLv_OPE,
                                       COD_CTO_GTN,
                                       COD_APO,
                                       DSC_FLL_TRI,
                                       DSC_IDC_UTL,
                                       DSC_IDC_ROT,
                                       FEC_ALA_PTD,
                                       GLS_OBS_DOS,
                                       NUM_OPE_DTN,
                                       DSC_FLL_DOS,
                                       FEC_PCS_REG_CTB)
         SELECT   SYSDATE AS FEC_ISR_OPE,
                  OPR.NUM_FOL_OPE AS NUM_FOL_OPE,
                  PKG_PAB_CONSTANTES.v_FLG_OPE_HAB AS FLG_DEB_HAB,     --Haber
                  PKG_PAB_CONSTANTES.v_COD_ENT_BAN_SANT AS COD_END,
                  PKG_PAB_CONSTANTES.v_COD_SIS_PAB AS COD_CLv_ITZ,
                  TO_NUMBER (TO_CHAR (OPR.FEC_VTA, 'YYYYMMDD')) AS FEC_CTB,
                  TO_NUMBER (TO_CHAR (OPR.FEC_ISR_OPE, 'YYYYMMDD'))
                     AS FEC_OPE,
                  '00' AS COD_PTO,
                  '    ' AS COD_SPO,
                  v_numCero AS NUM_GRT,
                  v_unoEsp AS TPO_PZO,
                  v_numCero AS CAN_PZO,
                  v_numCero AS COD_SUB_SEC,
                  v_numCero AS COD_CNp_SUB_SEC,
                  v_numCero AS NUM_DTN_INV,
                  '    ' AS CAN_END_TDA,
                  v_numCero AS COD_ABT,
                  v_unoEsp AS TPO_TTM_MDA,
                  v_unoEsp AS TPO_INV,
                  '000' AS COD_OPE,
                  CTB.NUM_CNp_CTA_CTB AS NUM_CNp_CTA_CTB,
                  OPR.COD_DVI AS COD_DVI,
                  v_unoEsp AS TPO_DVI,
                  '00000     ' AS FLL_UNO,
                  '                              ' AS VRS_FJO_VAR,
                  '      ' AS CLv_ATZ,
                  PKG_PAB_CONSTANTES.v_COD_SUC_OPERANTE AS COD_SUC_OPE,
                  PKG_PAB_CONSTANTES.V_COD_SUC_ORG AS COD_SUC_ORG, --SOG.COD_SUC_DTN_TPN AS COD_SUC_ORG,
                  CTB.COD_SUC_DTN_TPN AS COD_SUC_DTN,
                  v_numCero AS NUM_MVT_DEB,
                  1 AS NUM_MVT_HAB,
                  v_numCero AS IMp_MON_BSE_DEB,
                  v_numCero AS IMp_MON_BSE_HAB,
                  v_numCero AS IMp_DVI_DEB,
                  OPR.IMP_OPE AS IMp_DVI_HAB,
                  v_unoEsp AS FLG_IDC_CCC,
                  '            ' AS NUM_DOC,
                  '   ' AS CLv_CNP,
                  '              ' AS DSC_CNP,
                  v_unoEsp AS TPO_CDA,
                  CTB.GLS_IFM_MVT_CTB AS GLS_IFM_MVT_CTB,
                     PKG_PAB_CONSTANTES.v_COD_ENT_BAN_SANT
                  || v_cuaCer
                  || LPAD (OPR.NUM_FOL_OPE, 12, '0')
                     AS NUM_OPE_ORG,
                  CAO.COD_SNO_CTB AS COD_SNO_CTB_ORG,
                  CAD.COD_SNO_CTB AS COD_SNO_CTB_DTN,
                  '    ' AS GLS_OBS_UNO,
                  '    ' AS RSv_USO_ITR,
                  PKG_PAB_CONSTANTES.v_COD_SIS_PAB || v_unoEsp
                     AS COD_CLv_ITZ_GNR_MVT,
                  v_unoEsp AS FLG_CPS_CJA,
                  v_unoEsp AS FLG_CTA_ORD,
                  v_numCero AS TSA_INT,
                  '   ' AS COD_CLv_OPE,
                  v_unoCuatro AS COD_CTO_GTN,
                  '  ' AS COD_APO,
                  '  ' AS E040_SACCDTGT,
                  v_unoEsp AS IDC_UTL,
                  '  ' AS IDC_ROT,
                  v_FEC_ALTA AS FEC_ALA_PTD,
                  ' 0000000000000000             ' AS GLS_OBS_DOS,
                     PKG_PAB_CONSTANTES.v_COD_ENT_BAN_SANT
                  || v_cuaCer
                  || LPAD (OPR.NUM_FOL_OPE, 12, '0')
                     AS NUM_OPE_DTN,
                  '     ' AS FLL_DOS,
                  v_FEC_PCS AS FEC_PCS_REG_CTB
           FROM                  PABS_DT_DETLL_OPRCN OPR
                              -- Join operaciones que se deben contabilizar
                              JOIN
                                 PABS_DT_TIPO_OPRCN_SALID_MNSJ TSM
                              ON TSM.COD_SIS_ENT_SAL = OPR.COD_SIS_SAL
                                 AND TSM.COD_MT_SWF = OPR.COD_MT_SWF
                                 AND TSM.COD_TPO_OPE_AOS =
                                       OPR.COD_TPO_OPE_AOS
                                 AND TSM.FLG_MRD_UTZ_SWF =
                                       OPR.FLG_MRD_UTZ_SWF
                                 AND TSM.FLG_EGR_ING = OPR.FLG_EGR_ING
                           --Join obtiene cuenta contable y sucursal destino
                           JOIN
                              PABS_DT_CENTA_CNTBL CTB
                           ON CTB.COD_SIS_ENT_SAL = OPR.COD_SIS_ENT
                              AND CTB.FLAG_MRD_UTZ_SWF = OPR.FLG_MRD_UTZ_SWF
                        --Join obtiene sucursal Origen
                        JOIN
                           PABS_DT_CENTA_CNTBL SOG
                        ON SOG.COD_SIS_ENT_SAL = OPR.COD_SIS_SAL
                           AND SOG.FLAG_MRD_UTZ_SWF = OPR.FLG_MRD_UTZ_SWF
                     --Join obtiene codigo de aplicacion sistema entrada salida
                     JOIN
                        PABS_DT_SISTM_ENTRD_SALID CAO
                     ON CAO.COD_SIS_ENT_SAL = OPR.COD_SIS_ENT
                  JOIN
                     PABS_DT_SISTM_ENTRD_SALID CAD
                  ON CAD.COD_SIS_ENT_SAL = OPR.COD_SIS_SAL
          WHERE   OPR.COD_EST_AOS = PKG_PAB_CONSTANTES.v_COD_EST_AOS_OPEPAG
                  AND OPR.FEC_VTA = v_FEC_PCS --AND TRUNC(OPR.FEC_VTA) = v_FEC_PCS
                  AND OPR.FLG_EGR_ING = PKG_PAB_CONSTANTES.v_FLG_EGR
                  AND OPR.FLG_MRD_UTZ_SWF = PKG_PAB_CONSTANTES.v_FLG_MRD_MX
                  AND TSM.FLG_GNR_OPE_CTB =
                        PKG_PAB_CONSTANTES.v_FLG_GNR_OPE_CTB
                  AND CTB.FLG_VGN = PKG_PAB_CONSTANTES.v_REG_VIG
                  AND OPR.COD_MT_SWF = PKG_PAB_CONSTANTES.V_COD_MT200;   --200

      /*** FIN MT 200 3DP BANCO ***/


      /***CONTABILIDAD OPERACIONES INTERNAS ***/

      -- MOVIMIENTOS AL DEBE
      INSERT INTO PABS_DT_CNTBL_ALMOT (FEC_ISR_OPE,
                                       NUM_FOL_OPE,
                                       FLG_DEB_HAB,
                                       COD_END,
                                       COD_CLV_ITZ,
                                       FEC_CTB,
                                       FEC_OPE,
                                       COD_PTO,
                                       COD_SPO,
                                       NUM_GRT,
                                       DSC_TPO_PZO,
                                       CAN_PZO,
                                       COD_SUB_SEC,
                                       COD_CNP_SUB_SEC,
                                       NUM_DTN_INV,
                                       CAN_END_TDA,
                                       COD_ABT,
                                       DSC_TPO_TTM_MDA,
                                       DSC_TPO_INV,
                                       COD_OPE,
                                       NUM_CNP_CTA_CTB,
                                       COD_DVI,
                                       DSC_TPO_DVI,
                                       DSC_FLL_UNO,
                                       DSC_VRS_FJO_VAR,
                                       DSC_CLv_ATZ,
                                       COD_SUC_OPE,
                                       COD_SUC_ORG,
                                       COD_SUC_DTN,
                                       NUM_MVT_DEB,
                                       NUM_MVT_HAB,
                                       IMP_MON_BSE_DEB,
                                       IMP_MON_BSE_HAB,
                                       IMP_DVI_DEB,
                                       IMP_DVI_HAB,
                                       FLG_IDC_CCC,
                                       NUM_DOC,
                                       DSC_CLV_CNP,
                                       DSC_CNP,
                                       DSC_TPO_CDA,
                                       GLS_IFM_MVT_CTB,
                                       NUM_OPE_ORG,
                                       COD_SNO_CTB_ORG,
                                       COD_SNO_CTB_DTN,
                                       GLS_OBS_UNO,
                                       DSC_RSV_USO_ITR,
                                       COD_CLV_ITZ_GNR_MVT,
                                       FLG_CPS_CJA,
                                       FLG_CTA_ORD,
                                       DSC_TSA_INT,
                                       COD_CLV_OPE,
                                       COD_CTO_GTN,
                                       COD_APO,
                                       DSC_FLL_TRI,
                                       DSC_IDC_UTL,
                                       DSC_IDC_ROT,
                                       FEC_ALA_PTD,
                                       GLS_OBS_DOS,
                                       NUM_OPE_DTN,
                                       DSC_FLL_DOS,
                                       FEC_PCS_REG_CTB)
         SELECT   OPR.FEC_ISR_OPE_ITR AS FEC_ISR_OPE,
                  OPR.NUM_FOL_OPE_ITR AS NUM_FOL_OPE,
                  PKG_PAB_CONSTANTES.v_FLG_OPE_DEB AS FLG_DEB_HAB,      --Debe
                  PKG_PAB_CONSTANTES.v_COD_ENT_BAN_SANT AS COD_END,
                  PKG_PAB_CONSTANTES.v_COD_SIS_PAB AS COD_CLV_ITZ,
                  TO_NUMBER (TO_CHAR (OPR.FEC_ISR_OPE_ITR, 'YYYYMMDD'))
                     AS FEC_CTB,
                  TO_NUMBER (TO_CHAR (OPR.FEC_ISR_OPE_ITR, 'YYYYMMDD'))
                     AS FEC_OPE,
                  '00' AS COD_PTO,
                  v_unoCuatro AS COD_SPO,
                  v_numCero AS NUM_GRT,
                  v_unoEsp AS TPO_PZO,
                  v_numCero AS CAN_PZO,
                  v_numCero AS COD_SUB_SEC,
                  v_numCero AS COD_CNP_SUB_SEC,
                  v_numCero AS NUM_DTN_INV,
                  v_unoCuatro AS CAN_END_TDA,
                  v_numCero AS COD_ABT,
                  v_unoEsp AS TPO_TTM_MDA,
                  v_unoEsp AS TPO_INV,
                  '000' AS COD_OPE,
                  DECODE (OPR.COD_TPO_OPE_AOS_LQD,
                          PKG_PAB_CONSTANTES.V_STR_TPO_OPE_CDLE,
                          PKG_PAB_CONSTANTES.V_COD_PUE_GAR_OOFF,
                          CTB.NUM_CNP_CTA_CTB)
                     AS NUM_CNP_CTA_CTB,
                  OPR.COD_DVI_ITR AS COD_DVI,
                  v_unoEsp AS TPO_DVI,
                  '00000     ' AS FLL_UNO,
                  '                              ' AS VRS_FJO_VAR,
                  '      ' AS CLV_ATZ,
                  PKG_PAB_CONSTANTES.v_COD_SUC_OPERANTE AS COD_SUC_OPE,
                  PKG_PAB_CONSTANTES.V_COD_SUC_ORG AS COD_SUC_ORG, --CTB.COD_SUC_DTN_TPN AS COD_SUC_ORG,
                  CTB.COD_SUC_DTN_TPN AS COD_SUC_DTN, --SDT.COD_SUC_DTN_TPN AS COD_SUC_DTN,
                  1 AS NUM_MVT_DEB,
                  v_numCero AS NUM_MVT_HAB,
                  OPR.IMP_OPE_ITR AS IMP_MON_BSE_DEB,
                  v_numCero AS IMP_MON_BSE_HAB,
                  v_numCero AS IMP_DVI_DEB,
                  v_numCero AS IMP_DVI_HAB,
                  v_unoEsp AS FLG_IDC_CCC,
                  '            ' AS NUM_DOC,
                  '   ' AS CLV_CNP,
                  '              ' AS DSC_CNP,
                  v_unoEsp AS TPO_CDA,
                  --CTB.GLS_IFM_MVT_CTB AS GLS_IFM_MVT_CTB,
                  DECODE (OPR.COD_TPO_OPE_AOS_LQD,
                          PKG_PAB_CONSTANTES.V_STR_TPO_OPE_CDLE,
                          PKG_PAB_CONSTANTES.V_STR_PUE_GAR_OOFF,
                          CTB.GLS_IFM_MVT_CTB)
                     AS GLS_IFM_MVT_CTB,
                  PKG_PAB_CONSTANTES.v_COD_ENT_BAN_SANT || v_cuaCer
                  ||           --NVL(OPR.NUM_REF_CTB,v_numRef) AS NUM_OPE_ORG,
                    LPAD (
                        DECODE (OPR.COD_SIS_ENT_ITR,
                                PKG_PAB_CONSTANTES.V_COD_TIP_CAJA,
                                TRIM (OPR.NUM_OPE_SIS_ENT_ITR),
                                PKG_PAB_CONSTANTES.V_COD_TIP_OB,
                                TRIM (OPR.NUM_OPE_SIS_ENT_ITR),
                                PKG_PAB_CONSTANTES.V_COD_TIP_ABO_CTA,
                                TRIM (OPR.NUM_CTA_BFC_ITR),
                                OPR.NUM_FOL_OPE_ITR),
                        12,
                        '0'
                     )
                     AS NUM_OPE_ORG,
                  DECODE (OPR.COD_TPO_OPE_AOS_LQD,
                          PKG_PAB_CONSTANTES.V_STR_TPO_OPE_CDLE,
                          PKG_PAB_CONSTANTES.V_STR_COR_PUE_GAR_OOFF,
                          CAO.COD_SNO_CTB)
                     AS COD_SNO_CTB_ORG,
                  --CAO.COD_SNO_CTB AS COD_SNO_CTB_ORG,
                  --CAD.COD_SNO_CTB AS COD_SNO_CTB_DTN,
                  DECODE (OPR.COD_TPO_OPE_AOS_LQD,
                          PKG_PAB_CONSTANTES.V_STR_TPO_OPE_DVP,
                          PKG_PAB_CONSTANTES.V_STR_COR_CMN,
                          CAD.COD_SNO_CTB)
                     AS COD_SNO_CTB_DTN,
                  v_unoCuatro AS GLS_OBS_UNO,
                  v_unoCuatro AS RSV_USO_ITR,
                  PKG_PAB_CONSTANTES.v_COD_SIS_PAB || v_unoEsp
                     AS COD_CLV_ITZ_GNR_MVT,
                  v_unoEsp AS FLG_CPS_CJA,
                  v_unoEsp AS FLG_CTA_ORD,
                  v_numCero AS TSA_INT,
                  '   ' AS COD_CLV_OPE,
                  v_unoCuatro AS COD_CTO_GTN,
                  '  ' AS COD_APO,
                  '  ' AS DSC_FLL_TRI,
                  v_unoEsp AS IDC_UTL,
                  '  ' AS IDC_ROT,
                  v_FEC_ALTA AS FEC_ALA_PTD,
                  ' 0000000000000000             ' AS GLS_OBS_DOS,
                  --PKG_PAB_CONSTANTES.v_COD_ENT_BAN_SANT || v_cuaCer || NVL(OPR.NUM_REF_CTB,v_numRef) AS NUM_OPE_DTN,
                  PKG_PAB_CONSTANTES.v_COD_ENT_BAN_SANT || v_cuaCer
                  || LPAD (
                        DECODE (OPR.COD_SIS_ENT_ITR,
                                PKG_PAB_CONSTANTES.V_COD_TIP_CAJA,
                                TRIM (OPR.NUM_OPE_SIS_ENT_ITR),
                                PKG_PAB_CONSTANTES.V_COD_TIP_OB,
                                TRIM (OPR.NUM_OPE_SIS_ENT_ITR),
                                PKG_PAB_CONSTANTES.V_COD_TIP_ABO_CTA,
                                TRIM (OPR.NUM_CTA_BFC_ITR),
                                OPR.NUM_FOL_OPE_ITR),
                        12,
                        '0'
                     )
                     AS NUM_OPE_DTN,
                  '     ' AS FLL_DOS,
                  v_FEC_PCS AS FEC_PCS_REG_CTB
           FROM               PABS_DT_DETLL_OPRCN_INTER OPR
                           --Join obtiene cuenta contable y sucursal origen
                           JOIN
                              PABS_DT_CENTA_CNTBL CTB
                           ON CTB.COD_SIS_ENT_SAL = OPR.COD_SIS_ENT_ITR
                              AND CTB.FLAG_MRD_UTZ_SWF =
                                    PKG_PAB_CONSTANTES.V_FLG_MRD_MN
                        --Join obtiene sucursal destino
                        JOIN
                           PABS_DT_CENTA_CNTBL SDT
                        ON SDT.COD_SIS_ENT_SAL = OPR.COD_SIS_SAL_ITR
                           AND SDT.FLAG_MRD_UTZ_SWF =
                                 PKG_PAB_CONSTANTES.V_FLG_MRD_MN
                     --Join obtiene codigo de aplicacion sistema entrada salida
                     JOIN
                        PABS_DT_SISTM_ENTRD_SALID CAO
                     ON CAO.COD_SIS_ENT_SAL = OPR.COD_SIS_ENT_ITR
                  JOIN
                     PABS_DT_SISTM_ENTRD_SALID CAD
                  ON CAD.COD_SIS_ENT_SAL = OPR.COD_SIS_SAL_ITR
          WHERE   OPR.COD_EST_AOS IN
                        (PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEABN,
                         PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDEV)
                  AND TRUNC (OPR.FEC_ISR_OPE_ITR) = v_FEC_PCS --AND TRUNC(OPR.FEC_VTA) = v_FEC_PCS
                  AND OPR.FLG_EGR_ING_ITR = PKG_PAB_CONSTANTES.v_FLG_EGR
                  --AND OPR.FLG_MRD_UTZ_SWF = PKG_PAB_CONSTANTES.v_FLG_MRD_MN
                  --AND TSM.FLG_GNR_OPE_CTB = PKG_PAB_CONSTANTES.v_FLG_GNR_OPE_CTB
                  AND CTB.FLG_VGN = PKG_PAB_CONSTANTES.v_REG_VIG;

      --Inserta datos al HABER en tabla contabilidad
      INSERT INTO PABS_DT_CNTBL_ALMOT (FEC_ISR_OPE,
                                       NUM_FOL_OPE,
                                       FLG_DEB_HAB,
                                       COD_END,
                                       COD_CLv_ITZ,
                                       FEC_CTB,
                                       FEC_OPE,
                                       COD_PTO,
                                       COD_SPO,
                                       NUM_GRT,
                                       DSC_TPO_PZO,
                                       CAN_PZO,
                                       COD_SUB_SEC,
                                       COD_CNp_SUB_SEC,
                                       NUM_DTN_INV,
                                       CAN_END_TDA,
                                       COD_ABT,
                                       DSC_TPO_TTM_MDA,
                                       DSC_TPO_INV,
                                       COD_OPE,
                                       NUM_CNp_CTA_CTB,
                                       COD_DVI,
                                       DSC_TPO_DVI,
                                       DSC_FLL_UNO,
                                       DSC_VRS_FJO_VAR,
                                       DSC_CLv_ATZ,
                                       COD_SUC_OPE,
                                       COD_SUC_ORG,
                                       COD_SUC_DTN,
                                       NUM_MVT_DEB,
                                       NUM_MVT_HAB,
                                       IMp_MON_BSE_DEB,
                                       IMp_MON_BSE_HAB,
                                       IMp_DVI_DEB,
                                       IMp_DVI_HAB,
                                       FLG_IDC_CCC,
                                       NUM_DOC,
                                       DSC_CLv_CNP,
                                       DSC_CNP,
                                       DSC_TPO_CDA,
                                       GLS_IFM_MVT_CTB,
                                       NUM_OPE_ORG,
                                       COD_SNO_CTB_ORG,
                                       COD_SNO_CTB_DTN,
                                       GLS_OBS_UNO,
                                       DSC_RSv_USO_ITR,
                                       COD_CLv_ITZ_GNR_MVT,
                                       FLG_CPS_CJA,
                                       FLG_CTA_ORD,
                                       DSC_TSA_INT,
                                       COD_CLv_OPE,
                                       COD_CTO_GTN,
                                       COD_APO,
                                       DSC_FLL_TRI,
                                       DSC_IDC_UTL,
                                       DSC_IDC_ROT,
                                       FEC_ALA_PTD,
                                       GLS_OBS_DOS,
                                       NUM_OPE_DTN,
                                       DSC_FLL_DOS,
                                       FEC_PCS_REG_CTB)
         SELECT   OPR.FEC_ISR_OPE_ITR AS FEC_ISR_OPE,
                  OPR.NUM_FOL_OPE_ITR AS NUM_FOL_OPE,
                  PKG_PAB_CONSTANTES.v_FLG_OPE_HAB AS FLG_DEB_HAB,     --Haber
                  PKG_PAB_CONSTANTES.v_COD_ENT_BAN_SANT AS COD_END,
                  PKG_PAB_CONSTANTES.v_COD_SIS_PAB AS COD_CLv_ITZ,
                  TO_NUMBER (TO_CHAR (OPR.FEC_ISR_OPE_ITR, 'YYYYMMDD'))
                     AS FEC_CTB,
                  TO_NUMBER (TO_CHAR (OPR.FEC_ISR_OPE_ITR, 'YYYYMMDD'))
                     AS FEC_OPE,
                  '00' AS COD_PTO,
                  '    ' AS COD_SPO,
                  v_numCero AS NUM_GRT,
                  v_unoEsp AS TPO_PZO,
                  v_numCero AS CAN_PZO,
                  v_numCero AS COD_SUB_SEC,
                  v_numCero AS COD_CNp_SUB_SEC,
                  v_numCero AS NUM_DTN_INV,
                  v_unoCuatro AS CAN_END_TDA,
                  v_numCero AS COD_ABT,
                  v_unoEsp AS TPO_TTM_MDA,
                  v_unoEsp AS TPO_INV,
                  '000' AS COD_OPE,
                  DECODE (
                     OPR.COD_TPO_OPE_AOS_LQD,                            --GCP
                     PKG_PAB_CONSTANTES.V_STR_TPO_OPE_DVP,
                     DECODE (COD_EST_AOS,
                             PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDEV,
                             CTB.NUM_CNP_CTA_CTB,
                             PKG_PAB_CONSTANTES.V_COD_PUE_CMN),
                     CTB.NUM_CNP_CTA_CTB
                  )
                     AS NUM_CNP_CTA_CTB,         --CONTABLE QUE LE CORRESPONDE
                  OPR.COD_DVI_ITR AS COD_DVI,
                  v_unoEsp AS TPO_DVI,
                  '00000     ' AS FLL_UNO,
                  '                              ' AS VRS_FJO_VAR,
                  '      ' AS CLv_ATZ,
                  PKG_PAB_CONSTANTES.v_COD_SUC_OPERANTE AS COD_SUC_OPE,
                  PKG_PAB_CONSTANTES.V_COD_SUC_ORG AS COD_SUC_ORG, --SOG.COD_SUC_DTN_TPN AS COD_SUC_ORG,
                  CTB.COD_SUC_DTN_TPN AS COD_SUC_DTN,
                  v_numCero AS NUM_MVT_DEB,
                  1 AS NUM_MVT_HAB,
                  v_numCero AS IMp_MON_BSE_DEB,
                  OPR.IMP_OPE_ITR AS IMp_MON_BSE_HAB,
                  v_numCero AS IMp_DVI_DEB,
                  v_numCero AS IMp_DVI_HAB,
                  v_unoEsp AS FLG_IDC_CCC,
                  '            ' AS NUM_DOC,
                  '   ' AS CLv_CNP,
                  '              ' AS DSC_CNP,
                  v_unoEsp AS TPO_CDA,
                  DECODE (
                     OPR.COD_TPO_OPE_AOS_LQD,                            --GCP
                     PKG_PAB_CONSTANTES.V_STR_TPO_OPE_DVP,
                     DECODE (COD_EST_AOS,
                             PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDEV,
                             CTB.GLS_IFM_MVT_CTB,
                             PKG_PAB_CONSTANTES.V_STR_PUE_CMN),
                     CTB.GLS_IFM_MVT_CTB
                  )
                     AS GLS_IFM_MVT_CTB,
                  PKG_PAB_CONSTANTES.v_COD_ENT_BAN_SANT || v_cuaCer
                  ||           --NVL(OPR.NUM_REF_CTB,v_numRef) AS NUM_OPE_ORG,
                    LPAD (
                        DECODE (
                           OPR.COD_SIS_SAL_ITR,
                           PKG_PAB_CONSTANTES.V_COD_TIP_CAJA,
                           TRIM (OPR.NUM_OPE_SIS_ENT_ITR),
                           PKG_PAB_CONSTANTES.V_COD_TIP_OB,
                           TRIM (OPR.NUM_OPE_SIS_ENT_ITR),
                           PKG_PAB_CONSTANTES.V_COD_TIP_ABO_CTA,
                           DECODE (COD_EST_AOS,
                                   PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDEV,
                                   TRIM (OPR.NUM_CTA_ODN_ITR),
                                   TRIM (OPR.NUM_CTA_BFC_ITR)),
                           OPR.NUM_FOL_OPE_ITR
                        ),
                        12,
                        '0'
                     )
                     AS NUM_OPE_ORG,
                  --CAO.COD_SNO_CTB AS COD_SNO_CTB_ORG,
                  DECODE (OPR.COD_TPO_OPE_AOS_LQD,
                          PKG_PAB_CONSTANTES.V_STR_TPO_OPE_CDLE,
                          PKG_PAB_CONSTANTES.V_STR_COR_PUE_GAR_OOFF,
                          CAO.COD_SNO_CTB)
                     AS COD_SNO_CTB_ORG,
                  DECODE (
                     OPR.COD_TPO_OPE_AOS_LQD,                            --GCP
                     PKG_PAB_CONSTANTES.V_STR_TPO_OPE_DVP,
                     DECODE (COD_EST_AOS,
                             PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDEV,
                             CAD.COD_SNO_CTB,
                             PKG_PAB_CONSTANTES.V_STR_COR_CMN),
                     CAD.COD_SNO_CTB
                  )
                     AS COD_SNO_CTB_DTN,
                  v_unoCuatro AS GLS_OBS_UNO,
                  v_unoCuatro AS RSv_USO_ITR,
                  PKG_PAB_CONSTANTES.v_COD_SIS_PAB || v_unoEsp
                     AS COD_CLv_ITZ_GNR_MVT,
                  v_unoEsp AS FLG_CPS_CJA,
                  v_unoEsp AS FLG_CTA_ORD,
                  v_numCero AS TSA_INT,
                  '   ' AS COD_CLv_OPE,
                  v_unoCuatro AS COD_CTO_GTN,
                  '  ' AS COD_APO,
                  '  ' AS E040_SACCDTGT,
                  v_unoEsp AS IDC_UTL,
                  '  ' AS IDC_ROT,
                  v_FEC_ALTA AS FEC_ALA_PTD,
                  ' 0000000000000000             ' AS GLS_OBS_DOS,
                  --PKG_PAB_CONSTANTES.v_COD_ENT_BAN_SANT || v_cuaCer || OPR.NUM_REF_CTB AS NUM_OPE_DTN,
                  PKG_PAB_CONSTANTES.v_COD_ENT_BAN_SANT || v_cuaCer
                  ||                --LPAD(NUM_CTA_BFC,12,'0') AS NUM_OPE_DTN,
                    LPAD (
                        DECODE (
                           OPR.COD_SIS_SAL_ITR,
                           PKG_PAB_CONSTANTES.V_COD_TIP_CAJA,
                           TRIM (OPR.NUM_OPE_SIS_ENT_ITR),
                           PKG_PAB_CONSTANTES.V_COD_TIP_OB,
                           TRIM (OPR.NUM_OPE_SIS_ENT_ITR),
                           PKG_PAB_CONSTANTES.V_COD_TIP_ABO_CTA,
                           DECODE (COD_EST_AOS,
                                   PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDEV,
                                   TRIM (OPR.NUM_CTA_ODN_ITR),
                                   TRIM (OPR.NUM_CTA_BFC_ITR)),
                           OPR.NUM_FOL_OPE_ITR
                        ),
                        12,
                        '0'
                     )
                     AS NUM_OPE_DTN,
                  '     ' AS FLL_DOS,
                  v_FEC_PCS AS FEC_PCS_REG_CTB
           FROM               PABS_DT_DETLL_OPRCN_INTER OPR
                           --Join obtiene cuenta contable y sucursal destino
                           JOIN
                              PABS_DT_CENTA_CNTBL CTB
                           ON CTB.COD_SIS_ENT_SAL = OPR.COD_SIS_SAL_ITR
                              AND CTB.FLAG_MRD_UTZ_SWF =
                                    PKG_PAB_CONSTANTES.V_FLG_MRD_MN
                        --Join obtiene sucursal Origen
                        JOIN
                           PABS_DT_CENTA_CNTBL SOG
                        ON SOG.COD_SIS_ENT_SAL = OPR.COD_SIS_ENT_ITR
                           AND SOG.FLAG_MRD_UTZ_SWF =
                                 PKG_PAB_CONSTANTES.V_FLG_MRD_MN
                     --Join obtiene codigo de aplicacion sistema entrada salida
                     JOIN
                        PABS_DT_SISTM_ENTRD_SALID CAO
                     ON CAO.COD_SIS_ENT_SAL = OPR.COD_SIS_ENT_ITR
                  JOIN
                     PABS_DT_SISTM_ENTRD_SALID CAD
                  ON CAD.COD_SIS_ENT_SAL = OPR.COD_SIS_SAL_ITR
          WHERE   OPR.COD_EST_AOS IN
                        (PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEABN,
                         PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDEV)
                  AND TRUNC (OPR.FEC_ISR_OPE_ITR) = v_FEC_PCS --AND TRUNC(OPR.FEC_VTA) = v_FEC_PCS
                  AND OPR.FLG_EGR_ING_ITR = PKG_PAB_CONSTANTES.v_FLG_EGR
                  --AND OPR.FLG_MRD_UTZ_SWF = PKG_PAB_CONSTANTES.v_FLG_MRD_MN
                  -- AND TSM.FLG_GNR_OPE_CTB = PKG_PAB_CONSTANTES.v_FLG_GNR_OPE_CTB
                  AND CTB.FLG_VGN = PKG_PAB_CONSTANTES.v_REG_VIG;

      /**************************************************************************/



      --Cursor, Select salida
      OPEN p_CURSOR FOR
           SELECT   COD_END,
                    COD_CLV_ITZ,
                    FEC_CTB,
                    FEC_OPE,
                    LPAD (COD_PTO, 2, '0') AS COD_PTO,
                    LPAD (COD_SPO, 4, ' ') AS COD_SPO,
                    LPAD (NUM_GRT, v_numTres, v_numCero) AS NUM_GRT,
                    LPAD (DSC_TPO_PZO, 1, ' ') AS DSC_TPO_PZO,
                    LPAD (CAN_PZO, v_numTres, v_numCero) AS CAN_PZO,
                    COD_SUB_SEC,
                    LPAD (COD_CNp_SUB_SEC, 2, v_numCero) AS COD_CNP_SUB_SEC,
                    LPAD (NUM_DTN_INV, 5, v_numCero) AS NUM_DTN_INV,
                    LPAD (CAN_END_TDA, 4, ' ') AS CAN_END_TDA,
                    LPAD (COD_ABT, 2, v_numCero) AS COD_ABT,
                    LPAD (DSC_TPO_TTM_MDA, 1, ' ') AS DSC_TPO_TTM_MDA,
                    LPAD (DSC_TPO_INV, 1, ' ') AS DSC_TPO_INV,
                    LPAD (COD_OPE, v_numTres, '0') AS COD_OPE,
                    NUM_CNp_CTA_CTB AS NUM_CNP_CTA_CTB,
                    LPAD (COD_DVI, v_numTres, ' ') AS COD_DVI,
                    LPAD (DSC_TPO_DVI, 1, ' ') AS DSC_TPO_DVI,
                    DSC_FLL_UNO,
                    LPAD (DSC_VRS_FJO_VAR, 30, ' ') AS DSC_VRS_FJO_VAR,
                    LPAD (DSC_CLv_ATZ, 6, ' ') AS DSC_CLV_ATZ,
                    COD_SUC_OPE AS COD_SUC_OPE,
                    COD_SUC_ORG AS COD_SUC_ORG,
                    COD_SUC_DTN AS COD_SUC_DTN,
                    LPAD (NUM_MVT_DEB, 7, v_numCero) AS NUM_MVT_DEB,
                    LPAD (NUM_MVT_HAB, 7, v_numCero) AS NUM_MVT_HAB,
                    CONCAT (LPAD (TRUNC (IMP_MON_BSE_DEB), 13, v_numCero),
                            '00')
                       AS IMP_MON_BSE_DEB,
                    CONCAT (LPAD (TRUNC (IMP_MON_BSE_HAB), 13, v_numCero),
                            '00')
                       AS IMP_MON_BSE_HAB,
                    CONCAT (
                       LPAD (TRUNC (IMP_DVI_DEB), 13, v_numCero),
                       SUBSTR (TO_CHAR (IMP_DVI_DEB, '9G999G999G999G990D99'),
                               20,
                               2)
                    )
                       AS IMP_DVI_DEB,
                    CONCAT (
                       LPAD (TRUNC (IMP_DVI_HAB), 13, v_numCero),
                       SUBSTR (TO_CHAR (IMP_DVI_HAB, '9G999G999G999G990D99'),
                               20,
                               2)
                    )
                       AS IMP_DVI_HAB,
                    LPAD (FLG_IDC_CCC, 1, ' ') AS FLG_IDC_CCC,
                    LPAD (NUM_FOL_OPE, 12, '0') AS NUM_DOC,
                    LPAD (DSC_CLV_CNP, v_numTres, ' ') AS DSC_CLV_CNP,
                    LPAD (DSC_CNP, 14, ' ') AS DSC_CNP,
                    LPAD (DSC_TPO_CDA, 1, ' ') AS DSC_TPO_CDA,
                    LPAD (GLS_IFM_MVT_CTB, 30, ' ') AS GLS_IFM_MVT_CTB,
                    NUM_OPE_ORG,
                    LPAD (COD_SNO_CTB_ORG, v_numTres, ' ') AS COD_SNO_CTB_ORG,
                    LPAD (COD_SNO_CTB_DTN, v_numTres, ' ') AS COD_SNO_CTB_DTN,
                    LPAD (GLS_OBS_UNO, 4, ' ') AS GLS_OBS_UNO,
                    LPAD (DSC_RSv_USO_ITR, 4, ' ') AS DSC_RSv_USO_ITR,
                    LPAD (COD_CLv_ITZ_GNR_MVT, 4, ' ') AS COD_CLv_ITZ_GNR_MVT,
                    LPAD (FLG_CPS_CJA, 1, ' ') AS FLG_CPS_CJA,
                    LPAD (FLG_CTA_ORD, 1, ' ') AS FLG_CTA_ORD,
                    LPAD (DSC_TSA_INT, 5, v_numCero) AS DSC_TSA_INT,
                    LPAD (COD_CLV_OPE, v_numTres, ' ') AS COD_CLV_OPE,
                    LPAD (COD_CTO_GTN, 4, ' ') AS COD_CTO_GTN,
                    LPAD (COD_APO, 2, ' ') AS COD_APO,
                    LPAD (DSC_FLL_TRI, 2, ' ') AS DSC_FLL_TRI,
                    LPAD (DSC_IDC_UTL, 1, ' ') AS DSC_IDC_UTL,
                    LPAD (DSC_IDC_ROT, 2, ' ') AS DSC_IDC_ROT,
                    FEC_ALA_PTD,
                    ' 0000000000000000             ' AS GLS_OBS_DOS,
                    NUM_OPE_DTN,
                    LPAD (DSC_FLL_DOS, 6, ' ') AS DSC_FLL_DOS
             FROM   PABS_DT_CNTBL_ALMOT
            WHERE   FEC_PCS_REG_CTB = v_FEC_PCS
         ORDER BY   NUM_FOL_OPE ASC, FEC_ISR_OPE ASC, FLG_DEB_HAB ASC;

      p_ERROR := PKG_PAB_CONSTANTES.v_OK;

      --Fin Traza
      p_GLS_DET_PCS := 'Fin proceso contabilidad';
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);
      PKG_PAB_TRACKING.Sp_PAB_UPD_BIT_CAB_PROCESO (p_FEC_ICO_PCS,
                                                   p_COD_TPO_PSC,
                                                   p_NUM_TCK_PCS,
                                                   p_ERROR,
                                                   p_ERROR);
   EXCEPTION
      WHEN DUP_VAL_ON_INDEX
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;

         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_CONTABILIDAD;

   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_AKON
   -- Objetivo: Genera la interfaz contable del sistema Altos Montos, Parte 1 solo pagos enviados
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN,PABS_DT_TIPO_OPRCN_SALID_MNSJ,PABS_DT_CENTA_CNTBL,PABS_DT_SISTM_ENTRD_SALID
   -- Fecha: 13/06/2016
   -- Autor: Santander
   -- Input: p_FEC_PCS -> Fecha Proceso.
   -- Output: p_CURSOR -> PABS_DT_CNTBL_ALMOT, objeto del tipo tabla.
   --        p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: 04/08/2016, Se Modifica comentario.
   --***********************************************************************************************
   PROCEDURE Sp_PAB_AKON (p_FEC_PCS   IN     VARCHAR2,
                          p_CURSOR       OUT SYS_REFCURSOR,
                          p_ERROR        OUT NUMBER)
   IS
      v_NOM_SP        VARCHAR2 (30);
      v_FEC_PCS       DATE;
      v_FEC_OFICI     VARCHAR2 (8);
      p_FEC_ICO_PCS   VARCHAR2 (30);
      p_NUM_TCK_PCS   NUMBER;
      p_GLS_DET_PCS   VARCHAR2 (300);
      p_COD_ERR_BIB   CHAR (6);
      p_COD_TPO_PSC   CHAR (3);
   BEGIN
      -- INICIALIZACION DE VARIABLES Y PARAMETROS
      p_ERROR := 0;
      v_NOM_SP := 'Sp_PAB_AKON';
      v_DES := 'Se genera interfaz akon, fecha proceso: ' || p_FEC_PCS;
      v_FEC_PCS := TO_DATE (p_FEC_PCS, 'DD-MM-YYYY');
      v_FEC_OFICI := TO_CHAR (SYSDATE, 'DDMMYYYY');
      p_COD_ERR_BIB := '0';
      p_COD_TPO_PSC := 'PCT';

      --Contabilidad Traza 1
      p_GLS_DET_PCS := 'Iniciando proceso akon';
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_CAB_PROCESO (p_COD_TPO_PSC,
                                                   p_FEC_ICO_PCS,
                                                   p_NUM_TCK_PCS,
                                                   p_ERROR);
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);

      --Inicia Logica Genera Akon

      OPEN p_CURSOR FOR
         --Select datos al Akon
         SELECT   RPAD (RTRIM (DET.COD_BCO_BFC), 11, 'X') AS NUM_CTACO, --VARCHAR2 (11)    ABBYGB2LXXX
                  '        ' AS GLS_ALADI,        --    VARCHAR2 (8)    Blanco
                  RPAD ('C', 1, ' ') AS COD_DECRE,       --VARCHAR2 (1)    D/C
                  LPAD (TRUNC (OPR.IMp_OPE), 12, 0) AS MTO_DECRE, --    VARCHAR2 (12)    000000050000
                  '.' AS PTO_DECRE,                         --    VARCHAR2 (1)
                  SUBSTR (TO_CHAR (OPR.IMp_OPE, '9G999G999G999G990D99'),
                          20,
                          2)
                     AS DEC_DECRE,
                  LPAD (OPR.NUM_FOL_OPE, 12, '0') AS OPR_LLAVE, --    VARCHAR2 (12)    8855051111
                  '000' AS COD_PAIS,                 --    VARCHAR2 (3)    013
                  SUBSTR (OPR.NUM_REF_SWF, 0, 15) AS NUM_REFEX, --    VARCHAR2 (15)    8855051111
                  '000000000000000' AS NUM_CHEQU, --    VARCHAR2 (15)    000000000000000
                  '000' AS COD_CONCE,                --    VARCHAR2 (3)    000
                  '0' AS NUM_INDRE,                    --    VARCHAR2 (1)    0
                  SUBSTR (RPAD (TOP.DSC_TPO_OPE_AOS, 30, ' '), 1, 30)
                     AS GLS_MOVTO, --    VARCHAR2 (30)    INVERSION EN DEP. A PLAZO
                  LPAD (OPR.COD_DVI, 3, ' ') AS COD_MONED, --    VARCHAR2 (3)    USD
                  TO_CHAR (OPR.FEC_VTA, 'DDMMYYYY') AS FEC_PROCE, --    VARCHAR2 (8)    06112013
                  RPAD (SUBSTR (PKG_PAB_CONSTANTES.v_COD_SUC_ORG, 2), 3, ' ')
                     AS COD_OFIOR,                   --    VARCHAR2 (3)    174
                  '000' AS COD_DEPOR,                --    VARCHAR2 (3)    000
                  '00' AS COD_REOFI,                  --    VARCHAR2 (2)    00
                  v_FEC_OFICI AS FEC_OFICI,     --    VARCHAR2 (8)    06112013
                  v_FEC_OFICI AS FEC_ORIGE, --TO_CHAR(OPR.FEC_ISR_OPE,'DDMMYYYY') AS FEC_ORIGE, --    VARCHAR2 (8)    06112013
                  '00' AS COD_BANCA,                  --    VARCHAR2 (2)    00
                  PKG_PAB_CONSTANTES.v_COD_PROD AS COD_PROD_ALT, --    VARCHAR2 (2)    30
                  PKG_PAB_CONSTANTES.v_COD_SUB_PROD AS COD_SUB_PROD_ALT, --    VARCHAR2 (4)    3031
                  PKG_PAB_CONSTANTES.v_COD_CTA_ALT AS COD_CTA_CONT_ALT, --    VARCHAR2 (11)    21051210240
                  LPAD (OPR.COD_DVI, 3, ' ') AS COD_DIv_ALT, --    VARCHAR2 (3)    USD
                  '00000000000' AS RUT_CLI --    VARCHAR2 (11))    04589158283
           FROM               PABS_DT_DETLL_OPRCN OPR
                           -- Join operaciones que se deben enviar a AKON
                           JOIN
                              PABS_DT_OPRCN_INFCN_OPCON DET
                           ON DET.NUM_FOL_OPE = OPR.NUM_FOL_OPE
                              AND DET.FEC_ISR_OPE = OPR.FEC_ISR_OPE
                        JOIN
                           PABS_DT_TIPO_OPRCN_SALID_MNSJ TSM
                        ON     TSM.COD_SIS_ENT_SAL = OPR.COD_SIS_SAL
                           AND TSM.COD_MT_SWF = OPR.COD_MT_SWF
                           AND TSM.COD_TPO_OPE_AOS = OPR.COD_TPO_OPE_AOS
                           AND TSM.FLG_MRD_UTZ_SWF = OPR.FLG_MRD_UTZ_SWF
                     --Join obtiene cuenta contable
                     JOIN
                        PABS_DT_CENTA_CNTBL CTB
                     ON CTB.COD_SIS_ENT_SAL = OPR.COD_SIS_ENT
                        AND CTB.FLAG_MRD_UTZ_SWF = OPR.FLG_MRD_UTZ_SWF
                  --Join obtiene glosa tipo operacion
                  JOIN
                     PABS_DT_TIPO_OPRCN_ALMOT TOP
                  ON TOP.COD_TPO_OPE_AOS = OPR.COD_TPO_OPE_AOS
          WHERE   OPR.COD_EST_AOS = PKG_PAB_CONSTANTES.v_COD_EST_AOS_OPEPAG
                  AND OPR.FEC_VTA = v_FEC_PCS
                  AND OPR.FLG_EGR_ING = PKG_PAB_CONSTANTES.v_FLG_EGR
                  AND OPR.FLG_MRD_UTZ_SWF = PKG_PAB_CONSTANTES.v_FLG_MRD_MX
                  AND TSM.FLG_GNR_OPE_CTB =
                        PKG_PAB_CONSTANTES.v_FLG_GNR_OPE_CTB
                  AND CTB.FLG_VGN = PKG_PAB_CONSTANTES.v_REG_VIG
         --AND NVL(OPR.GLS_ADC_EST,'#') <> 'Operación de origen AGP' -- ELIMINAR OPERACIONES PROVENIENTES DE AGP

         UNION
         --PARA LOS MT200 SE DEBE REALIZAR EL MOVIMIENTO AL 2do BANCO
         SELECT   RPAD (RTRIM (OPR.COD_BCO_DTN), 11, 'X') AS NUM_CTACO, --VARCHAR2 (11)    ABBYGB2LXXX
                  '        ' AS GLS_ALADI,        --    VARCHAR2 (8)    Blanco
                  RPAD (PKG_PAB_CONSTANTES.v_COD_OPE_DEB, 1, ' ')
                     AS COD_DECRE,                       --VARCHAR2 (1)    D/C
                  LPAD (TRUNC (OPR.IMp_OPE), 12, 0) AS MTO_DECRE, --    VARCHAR2 (12)    000000050000
                  '.' AS PTO_DECRE,                         --    VARCHAR2 (1)
                  SUBSTR (TO_CHAR (OPR.IMp_OPE, '9G999G999G999G990D99'),
                          20,
                          2)
                     AS DEC_DECRE,
                  LPAD (OPR.NUM_FOL_OPE, 12, '0') AS OPR_LLAVE, --    VARCHAR2 (12)    8855051111
                  '000' AS COD_PAIS,                 --    VARCHAR2 (3)    013
                  SUBSTR (OPR.NUM_REF_SWF, 0, 15) AS NUM_REFEX, --    VARCHAR2 (15)    8855051111
                  '000000000000000' AS NUM_CHEQU, --    VARCHAR2 (15)    000000000000000
                  '000' AS COD_CONCE,                --    VARCHAR2 (3)    000
                  '0' AS NUM_INDRE,                    --    VARCHAR2 (1)    0
                  SUBSTR (RPAD (TOP.DSC_TPO_OPE_AOS, 30, ' '), 1, 30)
                     AS GLS_MOVTO, --    VARCHAR2 (30)    INVERSION EN DEP. A PLAZO
                  LPAD (OPR.COD_DVI, 3, ' ') AS COD_MONED, --    VARCHAR2 (3)    USD
                  TO_CHAR (OPR.FEC_VTA, 'DDMMYYYY') FEC_PROCE, --    VARCHAR2 (8)    06112013
                  RPAD (SUBSTR (PKG_PAB_CONSTANTES.v_COD_SUC_ORG, 2), 3, ' ')
                     AS COD_OFIOR,                   --    VARCHAR2 (3)    174
                  '000' AS COD_DEPOR,                --    VARCHAR2 (3)    000
                  '00' AS COD_REOFI,                  --    VARCHAR2 (2)    00
                  v_FEC_OFICI AS FEC_OFICI,     --    VARCHAR2 (8)    06112013
                  v_FEC_OFICI AS FEC_ORIGE, --TO_CHAR(OPR.FEC_ISR_OPE,'DDMMYYYY') AS FEC_ORIGE, --    VARCHAR2 (8)    06112013
                  '00' AS COD_BANCA,                  --    VARCHAR2 (2)    00
                  PKG_PAB_CONSTANTES.v_COD_PROD AS COD_PROD_ALT, --    VARCHAR2 (2)    30
                  PKG_PAB_CONSTANTES.v_COD_SUB_PROD AS COD_SUB_PROD_ALT, --    VARCHAR2 (4)    3031
                  PKG_PAB_CONSTANTES.v_COD_CTA_ALT AS COD_CTA_CONT_ALT, --    VARCHAR2 (11)    21051210240
                  LPAD (OPR.COD_DVI, 3, ' ') AS COD_DIv_ALT, --    VARCHAR2 (3)    USD
                  '00000000000' AS RUT_CLI --    VARCHAR2 (11))    04589158283
           FROM               PABS_DT_DETLL_OPRCN OPR
                           -- Join operaciones que se deben enviar a AKON
                           JOIN
                              PABS_DT_OPRCN_INFCN_OPCON DET
                           ON DET.NUM_FOL_OPE = OPR.NUM_FOL_OPE
                              AND DET.FEC_ISR_OPE = OPR.FEC_ISR_OPE
                        JOIN
                           PABS_DT_TIPO_OPRCN_SALID_MNSJ TSM
                        ON     TSM.COD_SIS_ENT_SAL = OPR.COD_SIS_SAL
                           AND TSM.COD_MT_SWF = OPR.COD_MT_SWF
                           AND TSM.COD_TPO_OPE_AOS = OPR.COD_TPO_OPE_AOS
                           AND TSM.FLG_MRD_UTZ_SWF = OPR.FLG_MRD_UTZ_SWF
                     --Join obtiene cuenta contable
                     JOIN
                        PABS_DT_CENTA_CNTBL CTB
                     ON CTB.COD_SIS_ENT_SAL = OPR.COD_SIS_ENT
                        AND CTB.FLAG_MRD_UTZ_SWF = OPR.FLG_MRD_UTZ_SWF
                  --Join obtiene glosa tipo operacion
                  JOIN
                     PABS_DT_TIPO_OPRCN_ALMOT TOP
                  ON TOP.COD_TPO_OPE_AOS = OPR.COD_TPO_OPE_AOS
          WHERE   OPR.COD_EST_AOS = PKG_PAB_CONSTANTES.v_COD_EST_AOS_OPEPAG
                  AND OPR.FEC_VTA = v_FEC_PCS
                  AND OPR.FLG_EGR_ING = PKG_PAB_CONSTANTES.v_FLG_EGR
                  AND OPR.FLG_MRD_UTZ_SWF = PKG_PAB_CONSTANTES.v_FLG_MRD_MX
                  AND TSM.FLG_GNR_OPE_CTB =
                        PKG_PAB_CONSTANTES.v_FLG_GNR_OPE_CTB
                  AND CTB.FLG_VGN = PKG_PAB_CONSTANTES.v_REG_VIG
                  AND OPR.COD_MT_SWF = '200'
         --AND NVL(OPR.GLS_ADC_EST,'#') <> 'Operación de origen AGP' -- ELIMINAR OPERACIONES PROVENIENTES DE AGP

         --SE AGREGA LOGICA PARA 900-910 ABN, OPB
         -- BUSCAMOS MENSAJE 910 CORRESPONDIENTE A OPB
         UNION
         SELECT   RPAD (RTRIM (OPR.COD_BCO_ORG), 11, 'X') AS NUM_CTACO, --VARCHAR2 (11)    ABBYGB2LXXX
                  '        ' AS GLS_ALADI,        --    VARCHAR2 (8)    Blanco
                  RPAD ('D', 1, ' ') AS COD_DECRE,       --VARCHAR2 (1)    D/C
                  LPAD (TRUNC (OPR.IMP_OPE), 12, 0) AS MTO_DECRE, --    VARCHAR2 (12)    000000050000
                  '.' AS PTO_DECRE,                         --    VARCHAR2 (1)
                  SUBSTR (TO_CHAR (OPR.IMP_OPE, '9G999G999G999G990D99'),
                          20,
                          2)
                     AS DEC_DECRE,
                  LPAD (OPR.NUM_FOL_OPE, 12, '0') AS OPR_LLAVE, --    VARCHAR2 (12)    8855051111
                  '000' AS COD_PAIS,                 --    VARCHAR2 (3)    013
                  SUBSTR (OPR.NUM_REF_SWF, 0, 15) AS NUM_REFEX, --    VARCHAR2 (15)    8855051111
                  '000000000000000' AS NUM_CHEQU, --    VARCHAR2 (15)    000000000000000
                  '000' AS COD_CONCE,                --    VARCHAR2 (3)    000
                  '0' AS NUM_INDRE,                    --    VARCHAR2 (1)    0
                  SUBSTR (RPAD ('OPB', 30, ' '), 1, 30) AS GLS_MOVTO, --    VARCHAR2 (30)    INVERSION EN DEP. A PLAZO
                  LPAD (OPR.COD_DVI, 3, ' ') AS COD_MONED, --    VARCHAR2 (3)    USD
                  TO_CHAR (OPR.FEC_VTA, 'DDMMYYYY') FEC_PROCE, --    VARCHAR2 (8)    06112013
                  RPAD (SUBSTR (PKG_PAB_CONSTANTES.v_COD_SUC_ORG, 2), 3, ' ')
                     AS COD_OFIOR,                   --    VARCHAR2 (3)    174
                  '000' AS COD_DEPOR,                --    VARCHAR2 (3)    000
                  '00' AS COD_REOFI,                  --    VARCHAR2 (2)    00
                  v_FEC_OFICI AS FEC_OFICI,     --    VARCHAR2 (8)    06112013
                  v_FEC_OFICI AS FEC_ORIGE, --TO_CHAR(OPR.FEC_ISR_OPE,'DDMMYYYY') AS FEC_ORIGE, --    VARCHAR2 (8)    06112013
                  '00' AS COD_BANCA,                  --    VARCHAR2 (2)    00
                  PKG_PAB_CONSTANTES.v_COD_PROD AS COD_PROD_ALT, --    VARCHAR2 (2)    30
                  PKG_PAB_CONSTANTES.v_COD_SUB_PROD AS COD_SUB_PROD_ALT, --    VARCHAR2 (4)    3031
                  PKG_PAB_CONSTANTES.v_COD_CTA_ALT AS COD_CTA_CONT_ALT, --    VARCHAR2 (11)    21051210240
                  LPAD (OPR.COD_DVI, 3, ' ') AS COD_DIv_ALT, --    VARCHAR2 (3)    USD
                  '00000000000' AS RUT_CLI --    VARCHAR2 (11))    04589158283
           FROM   PABS_DT_DETLL_CARGO_ABONO_CAJA OPR
          -- Join operaciones que se deben enviar a AKON
          WHERE       OPR.FEC_VTA = v_FEC_PCS
                  AND OPR.COD_MT_SWF = 910
                  AND OPR.COD_BCO_ITD = 'BCECCLRM'
                  OR    OPR.COD_BCO_ITD = 'BCECCLR0' -- SE ASEGURA QUE SEA ABN
                    AND OPR.COD_BCO_ORG <> 'BCECCLRR' -- VIENE DEL CORRESPONSAL, COMO INTERMEDIARIO EL BANCO CENTRAL.
                    AND OPR.COD_DVI = PKG_PAB_CONSTANTES.V_STR_DVI_USD --DEBE SER DOLAR
         -- SE AGREGA LA LOGICA PARA MOVIMIENTOS 900 - 910 QUE NO SEAN DEL CENTRAL.
         UNION
         SELECT   RPAD (RTRIM (OPR.COD_BCO_ORG), 11, 'X') AS NUM_CTACO, --VARCHAR2 (11)    ABBYGB2LXXX
                  '        ' AS GLS_ALADI,        --    VARCHAR2 (8)    Blanco
                  RPAD ('C', 1, ' ') AS COD_DECRE,       --VARCHAR2 (1)    D/C
                  LPAD (TRUNC (OPR.IMP_OPE), 12, 0) AS MTO_DECRE, --    VARCHAR2 (12)    000000050000
                  '.' AS PTO_DECRE,                         --    VARCHAR2 (1)
                  SUBSTR (TO_CHAR (OPR.IMP_OPE, '9G999G999G999G990D99'),
                          20,
                          2)
                     AS DEC_DECRE,
                  LPAD (OPR.NUM_FOL_OPE, 12, '0') AS OPR_LLAVE, --    VARCHAR2 (12)    8855051111
                  '000' AS COD_PAIS,                 --    VARCHAR2 (3)    013
                  SUBSTR (OPR.NUM_REF_SWF, 0, 15) AS NUM_REFEX, --    VARCHAR2 (15)    8855051111
                  '000000000000000' AS NUM_CHEQU, --    VARCHAR2 (15)    000000000000000
                  '000' AS COD_CONCE,                --    VARCHAR2 (3)    000
                  '0' AS NUM_INDRE,                    --    VARCHAR2 (1)    0
                  SUBSTR (RPAD (OPR.GLS_TRN, 30, ' '), 1, 30) AS GLS_MOVTO, --    VARCHAR2 (30)    INVERSION EN DEP. A PLAZO
                  LPAD (OPR.COD_DVI, 3, ' ') AS COD_MONED, --    VARCHAR2 (3)    USD
                  TO_CHAR (OPR.FEC_VTA, 'DDMMYYYY') FEC_PROCE, --    VARCHAR2 (8)    06112013
                  RPAD (SUBSTR (PKG_PAB_CONSTANTES.v_COD_SUC_ORG, 2), 3, ' ')
                     AS COD_OFIOR,                   --    VARCHAR2 (3)    174
                  '000' AS COD_DEPOR,                --    VARCHAR2 (3)    000
                  '00' AS COD_REOFI,                  --    VARCHAR2 (2)    00
                  v_FEC_OFICI AS FEC_OFICI,     --    VARCHAR2 (8)    06112013
                  v_FEC_OFICI AS FEC_ORIGE, --TO_CHAR(OPR.FEC_ISR_OPE,'DDMMYYYY') AS FEC_ORIGE, --    VARCHAR2 (8)    06112013
                  '00' AS COD_BANCA,                  --    VARCHAR2 (2)    00
                  PKG_PAB_CONSTANTES.v_COD_PROD AS COD_PROD_ALT, --    VARCHAR2 (2)    30
                  PKG_PAB_CONSTANTES.v_COD_SUB_PROD AS COD_SUB_PROD_ALT, --    VARCHAR2 (4)    3031
                  PKG_PAB_CONSTANTES.v_COD_CTA_ALT AS COD_CTA_CONT_ALT, --    VARCHAR2 (11)    21051210240
                  LPAD (OPR.COD_DVI, 3, ' ') AS COD_DIv_ALT, --    VARCHAR2 (3)    USD
                  '00000000000' AS RUT_CLI --    VARCHAR2 (11))    04589158283
           FROM   PABS_DT_DETLL_CARGO_ABONO_CAJA OPR
          -- Join operaciones que se deben enviar a AKON
          WHERE   OPR.FEC_VTA = v_FEC_PCS
                  AND OPR.COD_MT_SWF IN
                           (PKG_PAB_CONSTANTES.V_COD_MT900,
                            PKG_PAB_CONSTANTES.V_COD_MT910) -- TODOS LOS 900 - 910
                  AND OPR.COD_BCO_ORG = 'BCECCLRR' -- VIENE DEL CORRESPONSAL, COMO INTERMEDIARIO EL BANCO CENTRAL.
                  --AND (OPR.COD_BCO_ITD <> 'BCECCLRM' AND OPR.COD_BCO_ITD <> 'BCECCLR0')
                  AND OPR.COD_DVI <> PKG_PAB_CONSTANTES.V_STR_DVI_CLP; --DEBE SER DOLAR /TEMPORAL NO CONSIDERAR 910/900


      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
      --Fin Logica Genera Akon

      --Fin Traza
      p_GLS_DET_PCS := 'Fin proceso akon';
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);
      PKG_PAB_TRACKING.Sp_PAB_UPD_BIT_CAB_PROCESO (p_FEC_ICO_PCS,
                                                   p_COD_TPO_PSC,
                                                   p_NUM_TCK_PCS,
                                                   p_ERROR,
                                                   p_ERROR);
   EXCEPTION
      WHEN DUP_VAL_ON_INDEX
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_AKON;
END PKG_PAB_CONTABILIDAD;