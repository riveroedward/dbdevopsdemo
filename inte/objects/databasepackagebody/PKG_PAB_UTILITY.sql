CREATE OR REPLACE PACKAGE BODY         "PKG_PAB_UTILITY"
IS
   -- Package el cual contrendra todas las funciones y procedimientos comunes que pueden ser utilizados en mas de una funcionalidad
   -- @Santander
   -- **************************VARIABLE GLOBALES************************************
   --
   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_OBT_CAN_ENT_USU
   -- Objetivo: Procedimiento que obtiene el canal de entrada segun el usuario
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_USRIO
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- p_COD_USR -> rut usuario. (Obligatorio).
   -- Output:
   -- p_COD_SIS_ENT -> codigo del canal de entrada.
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_OBT_CAN_ENT_USU (p_COD_USR       IN     CHAR,
                                     p_COD_SIS_ENT      OUT CHAR,
                                     p_ERROR            OUT NUMBER)
   IS
      v_NOM_SP   VARCHAR2 (30) := 'Sp_PAB_OBT_CAN_ENT_USU';
   BEGIN
      --Obtenemos el canal del usuario segun el identificador
      SELECT   COD_SIS_ENT
        INTO   p_COD_SIS_ENT
        FROM   PABS_DT_USRIO
       WHERE   COD_USR = p_COD_USR AND FLG_VGN = PKG_PAB_CONSTANTES.v_REG_VIG;

      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := NULL;
         err_msg :=
               'No se encontro el usuario :'
            || p_COD_USR
            || ' o se encuentra en estado invalido';
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);

         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_OBT_CAN_ENT_USU;

   --
   --***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_CAL_MER
   -- Objetivo: Procedimiento que obtiene el mercado segun la moneda
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: N/A
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- p_COD_DVI -> Codigo de la moneda.
   -- Output:
   -- p_FLG_MRD_UTZ_SWF -> Identificador del mercado 0)MN 1)MX
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_CAL_MER (p_COD_DVI IN CHAR, p_FLG_MRD_UTZ_SWF OUT NUMBER)
   IS
      v_NOM_SP   VARCHAR2 (30) := 'Sp_PAB_CAL_MER';
   BEGIN
      --
      IF (p_COD_DVI = PKG_PAB_CONSTANTES.v_STR_DVI_CLP)
      THEN
         p_FLG_MRD_UTZ_SWF := PKG_PAB_CONSTANTES.v_FLG_MRD_MN;
      ELSE
         p_FLG_MRD_UTZ_SWF := PKG_PAB_CONSTANTES.v_FLG_MRD_MX;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_CAL_MER;

   --
   --***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUSCA_ESTRUC_ARCH
   -- Objetivo: Procedimiento que recibe como parametro ID_ARCHIVO, con el valor asociado a la nomina o planilla excel para la carga masiva,
   --           el procedimiento devuelve un cursor con los nombres de los campos que deberia componer la nomina, los tipos de datos de estos y su ID.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_ARCHv_ENTRD_SALID, PABS_DT_ESTRT_ARCHv_ENTRD, PABS_DT_CAMPO_ARCHv_ENTRD
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- p_IDF_ARC -> Identificador unico de la tabla ARCHIVO.
   -- p_IDF_SIS -> Nombre del sistema que llama al procedimiento.
   -- Output:
   -- p_CURSOR -> SYS_REFCURSOR, resultados de consulta.
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_BUSCA_ESTRUC_ARCH (p_IDF_ARC   IN     CHAR,
                                       p_IDF_SIS   IN     VARCHAR2,
                                       p_CURSOR       OUT SYS_REFCURSOR,
                                       p_ERROR        OUT NUMBER)
   IS
      --Seteo de Variables
      v_NOM_SP   VARCHAR2 (30) := 'Sp_PAB_BUSCA_ESTRUC_ARCH';
   BEGIN
      OPEN p_CURSOR FOR
           SELECT   CABE.COD_ARC_ENT_SAL AS CODIGO_ARCHIVO,
                    CABE.NOM_ARC AS NOMBRE_ARCHIVO,
                    ESTRU.FLG_VLD_ESP AS IND_VAL_ESP,
                    ESTRU.NUM_ORD_VSN AS POSICION_CAMPO,
                    ESTRU.NOM_TAG_CAM_ARC AS TAG_XML,
                    CAMP.NOM_CAM_ARC AS NOMBRE_CAMPO,
                    FORMATO.DESTIPFT AS TIPO_CAMPO
             FROM   PABS_DT_ARCHv_ENTRD_SALID CABE,
                    PABS_DT_ESTRT_ARCHv_ENTRD ESTRU,
                    PABS_DT_CAMPO_ARCHv_ENTRD CAMP,
                    TGEN_1048 FORMATO
            WHERE       CABE.COD_ARC_ENT_SAL = ESTRU.COD_ARC_ENT
                    AND CAMP.COD_CAM_ARC = ESTRU.COD_CAM_ARC
                    AND FORMATO.CODTIPFT = CAMP.COD_TPO_FTO
                    AND CABE.COD_ARC_ENT_SAL = p_IDF_ARC
                    AND CABE.FLG_VGN = PKG_PAB_CONSTANTES.v_REG_VIG
                    AND ESTRU.FLG_VGN = PKG_PAB_CONSTANTES.v_REG_VIG
                    AND CAMP.FLG_VGN = PKG_PAB_CONSTANTES.v_REG_VIG
         ORDER BY   ESTRU.NUM_ORD_VSN;

      p_ERROR := PKG_PAB_CONSTANTES.v_OK; --Procedimiento ejecutado de forma correcta
   EXCEPTION
      WHEN DUP_VAL_ON_INDEX
      THEN
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_BUSCA_ESTRUC_ARCH;

   --
   --***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_DESCOMp_RUT
   -- Objetivo: Procedimiento que descompone el Rut, separandolo del digito verificador.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: N/A
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- p_RUT_PSN -> Rut de personaa.
   -- Output:
   -- p_NUM_DOC_PSN -> Numero documento persona
   -- p_COD_TPD_PSN -> Codigo tipo documento persona
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_DESCOMp_RUT (p_RUT_PSN       IN     VARCHAR2,
                                 p_NUM_DOC_PSN      OUT VARCHAR2,
                                 p_COD_TPD_PSN      OUT CHAR)
   IS
      --Seteo de Variables
      v_NOM_SP    VARCHAR2 (30) := 'Sp_PAB_DESCOMp_RUT';
      v_RUT_PSN   VARCHAR2 (20);
   BEGIN
      --Limpiamos la variable por si viene con espaciones en blanco
      v_RUT_PSN := TRIM (REPLACE (REPLACE (p_RUT_PSN, '.', ''), '-', ''));

      p_NUM_DOC_PSN := TO_CHAR (SUBSTR (v_RUT_PSN, 1, LENGTH (v_RUT_PSN) - 1));
      p_COD_TPD_PSN := TO_CHAR (SUBSTR (v_RUT_PSN, LENGTH (v_RUT_PSN), 1));
   EXCEPTION
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_DESCOMp_RUT;

   --
   --***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_MAN_BFC_ODN
   -- Objetivo:
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: N/A
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- p_NUM_DOC_PSN -> Numero documento del beneficiario/ordenante
   -- p_COD_TPD_PSN -> Digito Verificador del beneficiario/ordenante
   -- Output:
   -- Retorno: N/A.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_MAN_BFC_ODN (p_NUM_DOC_PSN   IN VARCHAR2,
                                 p_COD_TPD_PSN   IN CHAR)
   IS
      --Seteo de Variables
      v_NOM_SP         VARCHAR2 (30) := 'Sp_PAB_MAN_BFC_ODN';
      v_EXIS_BFC_ODN   NUMBER;
      v_NUM_DOC_PSN    CHAR (11);
   BEGIN
      --
      --SETEAMOS VARIABLE
      v_NUM_DOC_PSN := p_NUM_DOC_PSN;

      --Consultamos por el ordenante y beneficiario
      SELECT   COUNT (1)
        INTO   v_EXIS_BFC_ODN
        FROM   PABS_DT_BNFCR_ORDNT
       WHERE   NUM_DOC_PSN = v_NUM_DOC_PSN AND COD_TPD_PSN = p_COD_TPD_PSN;

      IF v_EXIS_BFC_ODN = 0
      THEN
         --Insertamos beneficiario /Ordenante nuevo
         PKG_PAB_UTILITY.Sp_PAB_INS_BNF_ODN (p_NUM_DOC_PSN, p_COD_TPD_PSN);
      END IF;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_MAN_BFC_ODN;

   --
   --***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_INS_BNF_ODN
   -- Objetivo:
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_BNFCR_ORDNT
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- p_NUM_DOC_PSN -> Numero documento del beneficiario/ordenante
   -- p_COD_TPD_PSN -> Digito Verificador del beneficiario/ordenante
   -- Output:
   -- Retorno: N/A.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_INS_BNF_ODN (p_NUM_DOC_PSN   IN VARCHAR2,
                                 p_COD_TPD_PSN   IN CHAR)
   IS
      --Seteo de Variables
      v_NOM_SP   VARCHAR2 (30) := 'Sp_PAB_INS_BNF_ODN';
      v_RUT      VARCHAR2 (20);
   BEGIN
      --Insertamos la informacion de entrada
      INSERT INTO PABS_DT_BNFCR_ORDNT (
                                          NUM_DOC_PSN,
                                          COD_TPD_PSN,
                                          COD_TPO_SEG_AOS
                 )
        VALUES   (
                     p_NUM_DOC_PSN,
                     p_COD_TPD_PSN,
                     PKG_PAB_CONSTANTES.v_COD_TPO_SEG_AOS_CLI
                 );

      v_RUT := TRIM (p_NUM_DOC_PSN) || '-' || p_COD_TPD_PSN;

      v_DES := 'Se inserto la persona:' || v_RUT;
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (NULL,
                                          v_NOM_PCK,
                                          v_DES,
                                          v_NOM_SP);
   EXCEPTION
      WHEN DUP_VAL_ON_INDEX
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_INS_BNF_ODN;

   --
   --***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_MAN_CTA_BFC_ODN
   -- Objetivo: procediemitno que verifica si existe la cuenta del ordenasnte/beneficiario en caso de no existir la inserta
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_BNFCR_ORDNT
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- p_RUT_PSN ->Rut de persona
   -- p_COD_TPD_PSN ->Codigo tipo documento persona:
   -- p_NUM_CTA_TRF ->Cuenta de trasnferencia
   -- p_FLG_DCV ->Indica si la cuenat es DCV : 1)DCV 0)Cuenta Normal
   -- p_COD_BIC_BCO -> Codigo bitacora banco
   -- Output:
   -- Retorno: N/A.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_MAN_CTA_BFC_ODN (p_NUM_DOC_PSN   IN VARCHAR2,
                                     p_COD_TPD_PSN   IN CHAR,
                                     p_NUM_CTA_TRF   IN VARCHAR2,
                                     p_FLG_DCV       IN NUMBER,
                                     p_COD_BIC_BCO   IN VARCHAR2)
   IS
      --Seteo de Variables
      v_NOM_SP             VARCHAR2 (30) := 'Sp_PAB_MAN_CTA_BFC_ODN';
      v_EXIS_CTA_BFC_ODN   NUMBER;
      v_NUM_DOC_PSN        CHAR (11);
      v_COD_BIC_BCO        CHAR (11);
   BEGIN
      --SETEAMOS VARIABLE
      v_NUM_DOC_PSN := p_NUM_DOC_PSN;
      v_COD_BIC_BCO := p_COD_BIC_BCO;

      --Consultamos por la cuenta del beneficiario/ordenante
      SELECT   COUNT (1)
        INTO   v_EXIS_CTA_BFC_ODN
        FROM   PABS_DT_CENTA_TRNSC
       WHERE       NUM_DOC_PSN = v_NUM_DOC_PSN
               AND COD_TPD_PSN = p_COD_TPD_PSN
               AND NUM_CTA_TRF = p_NUM_CTA_TRF
               AND COD_BIC_BCO = v_COD_BIC_BCO;

      IF v_EXIS_CTA_BFC_ODN = 0
      THEN
         PKG_PAB_UTILITY.Sp_PAB_INS_CTA_TRANS (v_NUM_DOC_PSN,
                                               p_COD_TPD_PSN,
                                               v_COD_BIC_BCO,
                                               p_NUM_CTA_TRF,
                                               p_FLG_DCV);
      END IF;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_MAN_CTA_BFC_ODN;

   --
   --***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_INS_CTA_TRANS
   -- Objetivo: Procedimiento que inserta la cuenta asociada al rut del beneficiario
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_CENTA_TRNSC
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- p_NUM_DOC_PSN -> Rut dueno de la cuenta.
   -- p_COD_TPD_PSN -> Digito Verificado dueuo de la cuenta
   -- p_COD_BIC_BCO -> BIC del Banco dueuo de la cuenta.
   -- p_NUM_CTA_TRF -> Numero de cuenta.
   -- p_FLG_DCV -> Indica si la cuenat es DCV : 1)DCV 0)Cuenta Normal
   -- Output:
   -- Retorno: N/A.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_INS_CTA_TRANS (p_NUM_DOC_PSN   IN CHAR,
                                   p_COD_TPD_PSN   IN CHAR,
                                   p_COD_BIC_BCO   IN VARCHAR2,
                                   p_NUM_CTA_TRF   IN VARCHAR2,
                                   p_FLG_DCV       IN NUMBER)
   IS
      --Seteo de Variables
      v_NOM_SP   VARCHAR2 (30) := 'Sp_PAB_INS_CTA_TRANS';
      v_RUT      VARCHAR2 (20);
   BEGIN
      INSERT INTO PABS_DT_CENTA_TRNSC (NUM_DOC_PSN,
                                       COD_TPD_PSN,
                                       NUM_CTA_TRF,
                                       COD_BIC_BCO,
                                       FLG_DCV)
        VALUES   (p_NUM_DOC_PSN,
                  p_COD_TPD_PSN,
                  p_NUM_CTA_TRF,
                  p_COD_BIC_BCO,
                  p_FLG_DCV);
   EXCEPTION
      WHEN DUP_VAL_ON_INDEX
      THEN
         err_code := SQLCODE;
         err_msg :=
               'Registro ya existe: p_NUM_DOC_PSN:->'
            || p_NUM_DOC_PSN
            || '<- p_COD_TPD_PSN:->'
            || p_COD_TPD_PSN
            || '<- p_NUM_CTA_TRF:->'
            || p_NUM_CTA_TRF
            || '<- p_COD_BIC_BCO:->'
            || p_COD_BIC_BCO
            || '<- p_FLG_DCV:-> '
            || p_FLG_DCV
            || '<-';

         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_INS_CTA_TRANS;

   --
   --***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_DEv_COD_TPO_FND
   -- Objetivo: Devuelve el codigo del tipo de Fondo
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_TIPO_FONDO_ALMOT
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- p_COD_TPO_FND -> Codigo del tipo de fondo
   -- Output:
   -- Input/Output:
   -- p_GLS_TPO_FND -> Glosa del tipo de fondo a buscar.
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_DEv_COD_TPO_FND (p_GLS_TPO_FND   IN     VARCHAR2,
                                     p_COD_TPO_FND   IN OUT CHAR)
   IS
      --Seteo de Variables
      v_NOM_SP   VARCHAR2 (30) := 'Sp_PAB_DEv_COD_TPO_FND';
   BEGIN
      --OBTENEMOS EL CODIGO TIPO DE FONDO
      SELECT   COD_TPO_FND
        INTO   p_COD_TPO_FND
        FROM   PABS_DT_TIPO_FONDO_ALMOT
       WHERE   GLS_TPO_FND = p_GLS_TPO_FND
               AND FLG_VGN = PKG_PAB_CONSTANTES.v_REG_VIG;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         BEGIN
            --Realizamos la busqueda por codigo

            --OBTENEMOS EL CODIGO TIPO DE FONDO
            SELECT   COD_TPO_FND
              INTO   p_COD_TPO_FND
              FROM   PABS_DT_TIPO_FONDO_ALMOT
             WHERE   COD_TPO_FND = p_GLS_TPO_FND
                     AND FLG_VGN = PKG_PAB_CONSTANTES.v_REG_VIG;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               --No se encontro por busqueda de glosa/codigo
               p_COD_TPO_FND := 'TFA';
               err_code := SQLCODE;
               err_msg :=
                  'No se encontro informacion para el codigo:'
                  || p_GLS_TPO_FND;
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
            WHEN OTHERS
            THEN
               err_code := SQLCODE;
               err_msg := SUBSTR (SQLERRM, 1, 300);
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
         END;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_DEv_COD_TPO_FND;

   --
   --***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_COMBO_MONEDA
   -- Objetivo: Devuelve el codigo del tipo de Fondo
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_MONEDA_ALMOT
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- N/A
   -- Output:
   -- p_CURSOR -> SYS_REFCURSOR, RESULTADOS DE CONSULTA.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_COMBO_MONEDA (p_CURSOR OUT SYS_REFCURSOR)
   IS
      --
      v_NOM_SP   VARCHAR2 (30) := 'Sp_PAB_COMBO_MONEDA';
   --
   BEGIN
      --
      OPEN p_CURSOR FOR
           --
           SELECT   MON.COD_DVI, MON.COD_DVI AS NBDIVIC          --MOH.NBDIVIC
             FROM   PABS_DT_MONDA_ALMOT MON, TCDT080 MOH
            WHERE   MOH.CDDIVISS = MON.COD_DVI
                    AND FLG_VGN = PKG_PAB_CONSTANTES.V_REG_VIG
         ORDER BY   COD_DVI;
   --
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   --
   END Sp_PAB_COMBO_MONEDA;

   /********************************************************************************************************
   FUNCION/PROCEDIMIENTO: SP_PAB_COMBO_CANAL
   OBJETIVO             : MUESTRA FILTRO EN COMBO:
                           EGRESOS -> ENVIADOS - OPERACIONES -> LIBERAR
                                   COMBO: CANAL PAGO, CAMBIO CANAL, CANAL DE ORIGEN.
                           CONTINGENCIA -> ENVIADO -> BCCH UPLOAD - DESCARGAR ARCHIVO
                                   COMBO: CAMBIO CANAL, CANAL DE ORIGEN.
   SISTEMA              : DBO_PAB.
   BASE DE DATOS        : DBO_PAB.
   TABLAS USADAS        : PABS_DT_SISTM_ENTRD_SALID.
   FECHA                : 21/06/16
   AUTOR                : SANTANDER
   INPUT                : P_CANAL  := CANAL.
   OUTPUT               : P_CURSOR := DATOS CURSOR.
   OBSERVACIONES
   FECHA       USUARIO    DETALLE
   21/06/16    SANTANDER  CREACIÓN
   ********************************************************************************************************/
   PROCEDURE SP_PAB_COMBO_CANAL (P_CANAL    IN     NUMBER,
                                 P_CURSOR      OUT SYS_REFCURSOR)
   IS
      ---------------------------------------DECLARACIÓN DE VARIABLES Y CONSTANTES----------------------------
      V_NOM_SP            VARCHAR2 (30) := 'SP_PAB_COMBO_CANAL';
      V_FLG_SIS_ENT_SAL   NUMBER := NULL;
   --------------------------------------------------------------------------------------------------------

   -- V_CANAL NUMBER := 1;
   BEGIN
      --ENTRADA
      IF (P_CANAL = PKG_PAB_CONSTANTES.V_FLG_SIS_ENT)
      THEN
         OPEN P_CURSOR FOR
              SELECT   COD_SIS_ENT_SAL, DSC_SIS_ENT_SAL
                FROM   PABS_DT_SISTM_ENTRD_SALID
               WHERE   FLG_SIS_ENT_SAL = P_CANAL
                       AND COD_SIS_ENT_SAL NOT IN
                                (PKG_PAB_CONSTANTES.V_COD_TIP_CTACTELBTR) --'TESCEN'
                       AND FLG_VGN = PKG_PAB_CONSTANTES.V_REG_VIG
            ORDER BY   DSC_SIS_ENT_SAL;
      END IF;

      --SALIDA
      IF (P_CANAL = PKG_PAB_CONSTANTES.V_FLG_SIS_SAL)
      THEN
         OPEN P_CURSOR FOR
              SELECT   COD_SIS_ENT_SAL, DSC_SIS_ENT_SAL
                FROM   PABS_DT_SISTM_ENTRD_SALID
               WHERE   FLG_SIS_ENT_SAL = P_CANAL
                       AND COD_SIS_ENT_SAL NOT IN
                                (PKG_PAB_CONSTANTES.V_COD_TIP_TESCEN) --'TESCEN'
                       AND FLG_VGN = PKG_PAB_CONSTANTES.V_REG_VIG
            ORDER BY   DSC_SIS_ENT_SAL;
      END IF;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         ERR_CODE := SQLCODE;
         ERR_MSG := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (ERR_CODE,
                                             V_NOM_PCK,
                                             ERR_MSG,
                                             V_NOM_SP);
      WHEN OTHERS
      THEN
         ERR_CODE := SQLCODE;
         ERR_MSG := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (ERR_CODE,
                                             V_NOM_PCK,
                                             ERR_MSG,
                                             V_NOM_SP);
         P_S_MENSAJE := ERR_CODE || '-' || ERR_MSG;
         RAISE_APPLICATION_ERROR (-20000, P_S_MENSAJE);
   END SP_PAB_COMBO_CANAL;

   /********************************************************************************************************
     FUNCION/PROCEDIMIENTO: SP_PAB_COMBO_MT_SWIFT.
     OBJETIVO             : MUESTRA FILTRO EN
                             NOMINAS -> SWIFT -> CREAR UNITARIA        COMBO: TIPO MENSAJE
                             ENVIADOS - OPERACIONES -> LIBERAR         COMBO: MT SWIFT
                             MENSAJES -> ENVIADO - PROTOCOLO -> CREAR  COMBO: SUB - MENSAJE
                             CONSULTAS -> SWIFT -> OPERACIONES         COMBO: MT SWIFT
     SISTEMA              : DBO_PAB.
     BASE DE DATOS        : DBO_PAB.
     TABLAS USADAS        : PABS_DT_TIPO_MT_SWIFT.
     FECHA                : 21/06/2016.
     AUTOR                : SANTANDER.
     INPUT                : NA.
     OUTPUT               : P_CURSOR := DATOS CURSOR.
     OBSERVACIONES
     FECHA       USUARIO    DETALLE
     21/06/2016  SANTANDER  CREACIÓN.
     ********************************************************************************************************/
   PROCEDURE SP_PAB_COMBO_MT_SWIFT (p_CURSOR OUT SYS_REFCURSOR)
   IS
      ---------------------------------------DECLARACIÓN DE VARIABLES Y CONSTANTES----------------------------
      V_NOM_SP   VARCHAR2 (30) := 'SP_PAB_COMBO_MT_SWIFT';
   --------------------------------------------------------------------------------------------------------
   BEGIN
      OPEN P_CURSOR FOR
           SELECT   DISTINCT (COD_MT_SWF)
             FROM   PABS_DT_TIPO_MT_SWIFT
            WHERE   FLG_VGN = PKG_PAB_CONSTANTES.V_REG_VIG
                    AND FLG_TPO_MT = PKG_PAB_CONSTANTES.V_NUM_MT_PAGO --TIPO DE PAGO
         ORDER BY   COD_MT_SWF;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         ERR_CODE := SQLCODE;
         ERR_MSG := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (ERR_CODE,
                                             V_NOM_PCK,
                                             ERR_MSG,
                                             V_NOM_SP);
      WHEN OTHERS
      THEN
         ERR_CODE := SQLCODE;
         ERR_MSG := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (ERR_CODE,
                                             V_NOM_PCK,
                                             ERR_MSG,
                                             V_NOM_SP);
         P_S_MENSAJE := ERR_CODE || '-' || ERR_MSG;
         RAISE_APPLICATION_ERROR (-20000, P_S_MENSAJE);
   END SP_PAB_COMBO_MT_SWIFT;

   --***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_COMBO_TIPO_OPERACION
   -- Objetivo: PROCEDIMIENTO ALMACENADO QUE CONSULTA Y ENTREGA TODOS LOS TIPOS DE OPERACION QUE SON
   -- UTILIZADOS EN LA APLICACION DE ALTOS MONTOS. LOS QUE SON LLAMADOS EN LOS COMBO BOX.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_TIPO_OPRCN_ALMOT
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- N/A
   -- Output:
   -- p_CURSOR -> SYS_REFCURSOR, RESULTADOS DE CONSULTA.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_COMBO_TIPO_OPERACION (p_CURSOR OUT SYS_REFCURSOR)
   IS
      v_NOM_SP   VARCHAR2 (30) := 'Sp_PAB_COMBO_TIPO_OPERACION';
   BEGIN
      OPEN p_CURSOR FOR
           --
           SELECT   COD_TPO_OPE_AOS, COD_TPO_OPE_AOS DSC_TPO_OPE_AOS --DSC_TPO_OPE_AOS
             FROM   PABS_DT_TIPO_OPRCN_ALMOT
            WHERE   FLG_VGN = PKG_PAB_CONSTANTES.v_REG_VIG
         ORDER BY   DSC_TPO_OPE_AOS;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   --
   END Sp_PAB_COMBO_TIPO_OPERACION;

   --
   --***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUSCA_INFO_ARCH
   -- Objetivo: Procedimiento que recibe como parametro ID_ARCHIVO, y devuelve la estructura de este
   -- en la tabla PABS_DT_ARCHv_ENTRD_SALID.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_ARCHv_ENTRD_SALID
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- p_IDF_ARC -> identificador unico de la tabla ARCHIVO.
   -- Output:
   -- p_CURSOR -> SYS_REFCURSOR, resultados de consulta.
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_BUSCA_INFO_ARCH (p_IDF_ARC   IN     CHAR,
                                     p_CURSOR       OUT SYS_REFCURSOR,
                                     p_ERROR        OUT NUMBER)
   IS
      --Seteo de Variables
      v_NOM_SP   VARCHAR2 (30) := 'Sp_PAB_BUSCA_INFO_ARCH';
   BEGIN
      OPEN p_CURSOR FOR
         SELECT   COD_ARC_ENT_SAL,
                  NOM_ARC,
                  GLS_PTH_RUA_ARC,
                  CAN_MAX_TNO_ARC,
                  CASE FLG_ECN_ARC
                     WHEN PKG_PAB_CONSTANTES.v_FLG_EXT_XLS
                     THEN
                        PKG_PAB_CONSTANTES.v_STR_EXT_XLS
                     WHEN PKG_PAB_CONSTANTES.v_FLG_EXT_TXT
                     THEN
                        PKG_PAB_CONSTANTES.v_STR_EXT_TXT
                     ELSE
                        ''
                  END
                     AS FLG_ECN_ARC,
                  NOM_COM_ARC,
                  NUM_MAX_REG
           FROM   PABS_DT_ARCHv_ENTRD_SALID
          WHERE   COD_ARC_ENT_SAL = p_IDF_ARC
                  AND FLG_VGN = PKG_PAB_CONSTANTES.v_REG_VIG;

      p_ERROR := PKG_PAB_CONSTANTES.v_OK; --Procedimiento ejecutado de forma correcta
   EXCEPTION
      WHEN DUP_VAL_ON_INDEX
      THEN
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_BUSCA_INFO_ARCH;

   --
   --***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_COMBO_BAN_ORD
   -- Objetivo: PROCEDIMIENTO ALMACENADO QUE CONSULTA Y ENTREGA TODOS LOS BANCOS DEL SISTEMA
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_ARCHv_ENTRD_SALID
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- N/A
   -- Output:
   -- p_CURSOR -> SYS_REFCURSOR, resultados de consulta.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_COMBO_BAN_ORD (p_CURSOR OUT SYS_REFCURSOR)
   IS
      v_NOM_SP   VARCHAR2 (30) := 'Sp_PAB_COMBO_BAN_ORD';
   --

   BEGIN
      OPEN p_CURSOR FOR
           SELECT   BAN.COD_BIC_BCO, BAN.COD_BIC_BCO AS GLS_BAN_DEST
             FROM   PABS_DT_BANCO_ALMOT BAN
            WHERE   BAN.FLG_VGN = PKG_PAB_CONSTANTES.v_REG_VIG
         ORDER BY   BAN.COD_BIC_BCO;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         ERR_CODE := SQLCODE;
         ERR_MSG := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (ERR_CODE,
                                             v_NOM_PCK,
                                             ERR_MSG,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         ERR_CODE := SQLCODE;
         ERR_MSG := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (ERR_CODE,
                                             v_NOM_PCK,
                                             ERR_MSG,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_COMBO_BAN_ORD;

   --
   --***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_DEv_FECHA_HORA_SERV
   -- Objetivo: PROCEDIMIENTO QUE DEVUELVE LA FECHA Y HORA DEL SERVIDOR
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: DUAL
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- N/A
   -- Output:
   -- p_FECHA_SERV -> Fecha del servidor
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_DEv_FECHA_HORA_SERV (p_FECHA_SERV OUT DATE)
   IS
      v_NOM_SP   VARCHAR2 (30) := 'Sp_PAB_DEv_FECHA_HORA_SERV';
   --

   BEGIN
      SELECT   SYSDATE INTO p_FECHA_SERV FROM DUAL;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         ERR_CODE := SQLCODE;
         ERR_MSG := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (ERR_CODE,
                                             v_NOM_PCK,
                                             ERR_MSG,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         ERR_CODE := SQLCODE;
         ERR_MSG := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (ERR_CODE,
                                             v_NOM_PCK,
                                             ERR_MSG,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_DEv_FECHA_HORA_SERV;

   --**********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_VAL_HOR_SIS
   -- Objetivo: Procedimiento que informa si el sistema esta fuera de horario.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_SISTM_ENTRD_SALID
   -- Fecha: 19/08/16
   -- Autor: Santander
   -- Input:
   -- p_COD_SIS_ENT -> Codigo de sistema
   -- Output:
   -- p_FLG_SIS_HAB -> Indicador para informar si esta en horario valido -> 0 cumple y 1 no cumple
   -- p_ERROR -> indica si termino correctamente 0 si 1 no
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_VAL_HOR_SIS (p_COD_SIS_ENT   IN     CHAR,
                                 p_FLG_SIS_HAB      OUT NUMBER,
                                 p_ERROR            OUT NUMBER)
   IS
      v_NOM_SP         VARCHAR2 (30) := 'Sp_PAB_VAL_HOR_SIS';
      v_HORA_SYSDATE   NUMBER (4) := TO_NUMBER (TO_CHAR (SYSDATE, 'HH24MI'));
      v_DSC_SIS        VARCHAR2 (50);
      v_HOR_ICO        NUMBER (4);
      v_HOR_CRR        NUMBER (4);
      v_FLG_SIS_ENT    NUMBER (1);
      v_FLG_BSR_IFM    NUMBER (1);
      v_COD_SNO_CTB    CHAR (3);
      v_ERROR          NUMBER;
   BEGIN
      p_ERROR := PKG_PAB_CONSTANTES.v_OK;

      Sp_PAB_BUSCA_INFO_SIS (p_COD_SIS_ENT,
                             v_DSC_SIS,
                             v_HOR_ICO,
                             v_HOR_CRR,
                             v_FLG_SIS_ENT,
                             v_FLG_BSR_IFM,
                             v_COD_SNO_CTB,
                             v_ERROR);

      IF (v_ERROR = PKG_PAB_CONSTANTES.V_OK)
      THEN
         IF (v_HOR_ICO < v_HORA_SYSDATE AND v_HORA_SYSDATE < v_HOR_CRR)
         THEN
            p_FLG_SIS_HAB := PKG_PAB_CONSTANTES.V_OK;
         ELSE
            p_FLG_SIS_HAB := PKG_PAB_CONSTANTES.V_error;
         END IF;
      ELSE
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_VAL_HOR_SIS;

   --**********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUSCA_INFO_SIS
   -- Objetivo: Procedimiento que despliega datos del sistema o producto consultado.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_SISTM_ENTRD_SALID
   -- Fecha: 19/08/16
   -- Autor: Santander
   -- Input:
   -- p_COD_SIS_ENT_SAL -> Codigo de sistema
   -- Output:
   -- p_DSC_SIS -> Descripcion del sistema de entrada / salida
   -- p_HOR_ICO -> Hora de inicio ventana de tiempo sistema
   -- p_HOR_CRR -> Hora de Cierre ventana de tiempo sistema
   -- p_FLG_ENT_SAL -> Flag de sistema entrada salida
   -- p_FLG_BSR_IFM -> Flag de busqueda informacion
   -- p_COD_SNO_CTB -> Codigo sinonimp contabilidad
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_BUSCA_INFO_SIS (p_COD_SIS_ENT_SAL   IN     CHAR,
                                    p_DSC_SIS              OUT VARCHAR2,
                                    p_HOR_ICO              OUT NUMBER,
                                    p_HOR_CRR              OUT NUMBER,
                                    p_FLG_ENT_SAL          OUT NUMBER,
                                    p_FLG_BSR_IFM          OUT NUMBER,
                                    p_COD_SNO_CTB          OUT CHAR,
                                    P_ERROR                OUT NUMBER)
   IS
      v_NOM_SP            VARCHAR2 (30) := 'Sp_PAB_BUSCA_INFO_SIS';
      v_COD_SIS_ENT_SAL   CHAR (10) := p_COD_SIS_ENT_SAL;
   --Buscamos  nominas segun filtro por busqueda.
   BEGIN
      SELECT   DSC_SIS_ENT_SAL,
               HOR_ICO_SIS,
               HOR_CRR_SIS,
               FLG_SIS_ENT_SAL,
               FLG_BSR_IFM,
               COD_SNO_CTB
        INTO   p_DSC_SIS,
               p_HOR_ICO,
               p_HOR_CRR,
               p_FLG_ENT_SAL,
               p_FLG_BSR_IFM,
               p_COD_SNO_CTB
        FROM   PABS_DT_SISTM_ENTRD_SALID
       WHERE   COD_SIS_ENT_SAL = p_COD_SIS_ENT_SAL
               AND FLG_VGN = PKG_PAB_CONSTANTES.V_REG_VIG;

      p_ERROR := PKG_PAB_CONSTANTES.v_OK; --Procedimiento ejecutado de forma correcta
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_BUSCA_INFO_SIS;

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_TRAN_COD_EST_GLOSA
   -- Objetivo: Funcion que transforma segun el codigo del estado la glosa que le corresponde a este estado
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PKG_PAB_CONSTANTES
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- p_COD_EST->     Identificador de nuevo estado.
   -- p_TIP_ORI ->    Identifica si la consulta es por estado de nomina o operacion
   -- Output: N/A.
   -- Input/Output: N/A.
   -- Retorno: p_DSC_ESTADO -> sigla del estado
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   FUNCTION FN_PAB_TRAN_COD_EST_GLOSA (p_COD_EST   IN NUMBER,
                                       p_TIP_ORI   IN NUMBER)
      RETURN VARCHAR2
   IS
      p_DSC_ESTADO   VARCHAR2 (3);
      v_NOM_SP       VARCHAR2 (30) := 'FN_PAB_TRAN_COD_EST_GLOSA';
      ERR_COD_INEX EXCEPTION;
   BEGIN
      --Nomina
      IF (p_TIP_ORI = PKG_PAB_CONSTANTES.V_IND_NMN)
      THEN
         CASE
            WHEN p_COD_EST = PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNVAL
            THEN
               --Valida
               p_DSC_ESTADO := PKG_PAB_CONSTANTES.V_STR_EST_AOS_NMNVAL;
            WHEN p_COD_EST = PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNCUR
            THEN
               --Cursada
               p_DSC_ESTADO := PKG_PAB_CONSTANTES.V_STR_EST_AOS_NMNCUR;
            WHEN p_COD_EST = PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNINV
            THEN
               --Invalidas
               p_DSC_ESTADO := PKG_PAB_CONSTANTES.V_STR_EST_AOS_NMNINV;
            WHEN p_COD_EST = PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNAUT
            THEN
               --Autorizada
               p_DSC_ESTADO := PKG_PAB_CONSTANTES.V_STR_EST_AOS_NMNAUT;
            WHEN p_COD_EST = PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNREC
            THEN
               --Rechazadas
               p_DSC_ESTADO := PKG_PAB_CONSTANTES.V_STR_EST_AOS_NMNREC;
            WHEN p_COD_EST = PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNELI
            THEN
               --Eliminadas
               p_DSC_ESTADO := PKG_PAB_CONSTANTES.V_STR_EST_AOS_NMNELI;
            WHEN p_COD_EST = PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNDUP
            THEN
               --Duplicado
               p_DSC_ESTADO := PKG_PAB_CONSTANTES.V_STR_EST_AOS_NMNDUP;
            ELSE
               RAISE ERR_COD_INEX;
         END CASE;
      ELSE
         CASE
            --Operaciones
            WHEN p_COD_EST = PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEVAL
            THEN
               --Valida
               p_DSC_ESTADO := PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEVAL;
            WHEN p_COD_EST = PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEINV
            THEN
               --Invalida
               p_DSC_ESTADO := PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEINV;
            WHEN p_COD_EST = PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPECUR
            THEN
               --Cursada
               p_DSC_ESTADO := PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPECUR;
            WHEN p_COD_EST = PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDUP
            THEN
               --Duplicada
               p_DSC_ESTADO := PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEDUP;
            WHEN p_COD_EST = PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEREC
            THEN
               --Rechazada
               p_DSC_ESTADO := PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEREC;
            WHEN p_COD_EST = PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT
            THEN
               --Autorizada
               p_DSC_ESTADO := PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEAUT;
            WHEN p_COD_EST = PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPELIB
            THEN
               --Liberada
               p_DSC_ESTADO := PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPELIB;
            WHEN p_COD_EST = PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEENV
            THEN
               --Enviada
               p_DSC_ESTADO := PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEENV;
            WHEN p_COD_EST = PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAG
            THEN
               --Pagada
               p_DSC_ESTADO := PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEPAG;
            WHEN p_COD_EST = PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEELI
            THEN
               --Eliminada
               p_DSC_ESTADO := PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEELI;
            WHEN p_COD_EST = PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPVI
            THEN
               --Por Visar
               p_DSC_ESTADO := PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEPVI;
            WHEN p_COD_EST = PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAB
            THEN
               --Por Abonar
               p_DSC_ESTADO := PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEPAB;
            WHEN p_COD_EST = PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAS
            THEN
               --Por Asociar
               p_DSC_ESTADO := PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEPAS;
            WHEN p_COD_EST = PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPDV
            THEN
               --Por devolver
               p_DSC_ESTADO := PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEPDV;
            WHEN p_COD_EST = PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPDEB
            THEN
               --Pendiente por debitar
               p_DSC_ESTADO := PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEPDEB;
            WHEN p_COD_EST = PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPENDE
            THEN
               --Mensajes 298 que no son para devolucion
               p_DSC_ESTADO := PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPENDE;
            WHEN p_COD_EST = PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAT
            THEN
               --Por Autorizar
               p_DSC_ESTADO := PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEPAT;
            WHEN p_COD_EST = PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDEV
            THEN
               --Devuelta
               p_DSC_ESTADO := PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEDEV;
            WHEN p_COD_EST = PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEASO
            THEN
               --Asociado
               p_DSC_ESTADO := PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEASO;
            WHEN p_COD_EST = PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAGCON
            THEN
               --Pagado por contingencia
               p_DSC_ESTADO := PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEPAGCON;
            WHEN p_COD_EST = PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPROBLM
            THEN
               -- Operacion con problemas
               p_DSC_ESTADO := PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEPROBLM;
            ELSE
               RAISE ERR_COD_INEX;
         END CASE;
      END IF;

      RETURN p_DSC_ESTADO;
   EXCEPTION
      WHEN ERR_COD_INEX
      THEN
         err_code := SQLCODE;
         err_msg := 'No existe informacion para: ' || p_COD_EST;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END FN_PAB_TRAN_COD_EST_GLOSA;

   --***********************************************************************************************
   -- Funcion/Procedimiento: Fn_PAB_COMBO_PAIS
   -- Objetivo: Devuelve la glosa del PAIS
   -- Sistema: PAB
   -- Base de Datos: TABLAS CORPORATIVAS
   -- Tablas Usadas: TGEN_0112
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- p_COD_PAIS -> CODIGO DEL PAIS
   -- Output:
   -- N/A
   -- Input/Output: N/A
   -- Retorno:
   -- v_GLOSA_PAIS -> Devuelve el nombre del pais
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   FUNCTION Fn_PAB_COMBO_PAIS (p_COD_PAIS IN VARCHAR2)
      RETURN VARCHAR2
   IS
      --
      v_NOM_SP       VARCHAR2 (30) := 'Fn_PAB_COMBO_PAIS';
      v_GLOSA_PAIS   VARCHAR2 (50);
   --
   BEGIN
      --
      IF (NVL (p_COD_PAIS, '#') <> '#')
      THEN
         SELECT   TCMPAIS
           INTO   v_GLOSA_PAIS
           FROM   TGEN_0112
          WHERE   TCCPAIS4 = p_COD_PAIS;
      ELSE
         v_GLOSA_PAIS := NULL;
      END IF;

      RETURN v_GLOSA_PAIS;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := 'No existe informacion para: ' || p_COD_PAIS;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);

         RETURN NULL;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   --
   END Fn_PAB_COMBO_PAIS;

   --
   --***********************************************************************************************
   -- Funcion/Procedimiento: Fn_PAB_COMBO_CAMARA
   -- Objetivo: Consulta los tipos de camara para altos montos
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas: PABS_DT_TIPO_CAMAR_ALMOT
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input: p_COD_TPO_CMA -> codigo del tipo camara
   -- N/A
   -- Output:
   -- N/A.
   -- Input/Output:
   -- N/A.
   -- Retorno:
   -- v_GLS_TPO_CMA-> GLOSA TIPO CAMARA
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   FUNCTION Fn_PAB_COMBO_CAMARA (p_COD_TPO_CMA IN CHAR)
      RETURN VARCHAR2
   IS
      v_NOM_SP        VARCHAR2 (30) := 'Fn_PAB_COMBO_CAMARA';
      v_GLS_TPO_CMA   VARCHAR2 (50);
   BEGIN
      IF (NVL (p_COD_TPO_CMA, '#') <> '#')
      THEN
         SELECT   GLS_TPO_CMA
           INTO   v_GLS_TPO_CMA
           FROM   PABS_DT_TIPO_CAMAR_ALMOT
          WHERE   COD_TPO_CMA = p_COD_TPO_CMA
                  AND FLG_VGN = PKG_PAB_CONSTANTES.V_REG_VIG;
      ELSE
         v_GLS_TPO_CMA := NULL;
      END IF;

      RETURN v_GLS_TPO_CMA;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := 'No existe informacion para: ' || p_COD_TPO_CMA;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Fn_PAB_COMBO_CAMARA;

   --
   --***********************************************************************************************
   -- Funcion/Procedimiento: Fn_PAB_COMBO_SUCURSAL
   -- Objetivo: Consulta los tipos de sucursal para altos montos
   -- Sistema: PAB
   -- Base de Datos: TCDT050
   -- Tablas Usadas:
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input: p_tccofici -> codigo de la sucursal
   -- Output:
   -- N/A.
   -- Input/Output:
   -- Retorno:
   -- v_tcmofcur -> glosa de tipo de sucursal.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   FUNCTION Fn_PAB_COMBO_SUCURSAL (p_tccofici IN VARCHAR2)
      RETURN VARCHAR2
   IS
      v_NOM_SP     VARCHAR2 (30) := 'Fn_PAB_COMBO_SUCURSAL';
      v_tcmofcur   VARCHAR2 (50);
      V_tccofici   CHAR (13);
   BEGIN
      V_tccofici := p_tccofici;

      IF (NVL (V_tccofici, '#') <> '#')
      THEN
         SELECT   tcmofcur
           INTO   v_tcmofcur
           FROM   TCDT050
          WHERE   TCNTELEX = V_tccofici;
      ELSE
         v_tcmofcur := NULL;
      END IF;

      RETURN v_tcmofcur;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := 'No existe informacion para: ' || p_tccofici;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Fn_PAB_COMBO_SUCURSAL;

   --***********************************************************************************************
   --
   --***********************************************************************************************
   -- Funcion/Procedimiento: Fn_PAB_COMBO_TSALDO
   -- Objetivo: Consulta los tipos de SALDO para altos montos
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas: PABS_DT_TIPO_SALDO_ALMOT
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input: p_COD_TPO_SDO -> codigo de tipo saldo
   -- Output:
   -- N/A.
   -- Input/Output:
   -- N/A.
   -- Retorno: v_GLS_TPO_SDO->GLOSA TIPO SALDO
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   FUNCTION Fn_PAB_COMBO_TSALDO (p_COD_TPO_SDO IN CHAR)
      RETURN VARCHAR2
   IS
      v_NOM_SP        VARCHAR2 (30) := 'Fn_PAB_COMBO_TSALDO';
      v_GLS_TPO_SDO   VARCHAR2 (50);
   BEGIN
      IF (NVL (p_COD_TPO_SDO, '#') <> '#')
      THEN
         SELECT   GLS_TPO_SDO
           INTO   v_GLS_TPO_SDO
           FROM   PABS_DT_TIPO_SALDO_ALMOT
          WHERE   COD_TPO_SDO = p_COD_TPO_SDO
                  AND FLG_VGN = PKG_PAB_CONSTANTES.V_REG_VIG;
      ELSE
         v_GLS_TPO_SDO := NULL;
      END IF;

      RETURN v_GLS_TPO_SDO;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := 'No existe informacion para: ' || p_COD_TPO_SDO;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Fn_PAB_COMBO_TSALDO;

   --
   --***********************************************************************************************
   -- Funcion/Procedimiento: Fn_PAB_COMBO_TIPO_OPERACION
   -- Objetivo: PROCEDIMIENTO ALMACENADO QUE CONSULTA Y ENTREGA TODOS LOS TIPOS DE OPERACION
   -- UTILIZADOS EN LA APLICACION DE ALTOS MONTOS. LOS QUE SON LLAMADOS EN LOS COMBO BOX.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_TIPO_OPRCN_ALMOT
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- P_COD_TPO_OPE_AOS -> TIPO DE OPERACIONES
   -- Output:
   -- N/A.
   -- Input/Output:
   -- N/A.
   -- Retorno:
   -- v_DSC_TPO_OPE_AOS -> TIPO OPERACION
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   FUNCTION Fn_PAB_COMBO_TIPO_OPERACION (P_COD_TPO_OPE_AOS IN CHAR)
      RETURN VARCHAR2
   IS
      v_NOM_SP            VARCHAR2 (30) := 'Fn_PAB_COMBO_TIPO_OPERACION';
      v_DSC_TPO_OPE_AOS   VARCHAR2 (50);
   BEGIN
      IF (NVL (P_COD_TPO_OPE_AOS, '#') <> '#')
      THEN
         SELECT   COD_TPO_OPE_AOS AS DSC_TPO_OPE_AOS
           INTO   v_DSC_TPO_OPE_AOS
           FROM   PABS_DT_TIPO_OPRCN_ALMOT
          WHERE   FLG_VGN = PKG_PAB_CONSTANTES.V_REG_VIG
                  AND COD_TPO_OPE_AOS = P_COD_TPO_OPE_AOS;
      ELSE
         v_DSC_TPO_OPE_AOS := NULL;
      END IF;

      RETURN v_DSC_TPO_OPE_AOS;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := 'No existe informacion para: ' || v_DSC_TPO_OPE_AOS;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Fn_PAB_COMBO_TIPO_OPERACION;

   --***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_COMBO_PAIS
   -- Objetivo: Devuelve los paises que soporta el sistema
   -- Sistema: PAB
   -- Base de Datos: TABLAS CORPORATIVAS
   -- Tablas Usadas: TGEN_0112
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- N/A
   -- Output:
   -- p_CURSOR -> SYS_REFCURSOR, RESULTADOS DE CONSULTA.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_COMBO_PAIS (p_CURSOR OUT SYS_REFCURSOR)
   IS
      v_NOM_SP   VARCHAR2 (30) := 'Sp_PAB_COMBO_PAIS';
   BEGIN
      OPEN p_CURSOR FOR
           SELECT   TCCPAIS4, TCMPAIS
             FROM   TGEN_0112
            WHERE   TRIM (TCCPAIS4) IS NOT NULL
                    AND TCCPAIS4 NOT IN
                             (  SELECT   TCCPAIS4
                                  FROM   TGEN_0112
                                 WHERE   TRIM (TCCPAIS4) IS NOT NULL
                                         AND TRIM (TCCPAIS4) IS NOT NULL
                              GROUP BY   TCCPAIS4
                                HAVING   COUNT (TCCPAIS4) > 1)
         ORDER BY   TCCPAIS4;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_COMBO_PAIS;

   --***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_COMBO_TSALDO
   -- Objetivo: Consulta los tipos de saldos para altos montos
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas: PABS_DT_TIPO_SALDO_ALMOT
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- N/A
   -- Output:
   -- p_CURSOR -> SYS_REFCURSOR, RESULTADOS DE CONSULTA.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_COMBO_TSALDO (p_CURSOR OUT SYS_REFCURSOR)
   IS
      v_NOM_SP   VARCHAR2 (30) := 'Sp_PAB_COMBO_TSALDO';
   BEGIN
      OPEN p_CURSOR FOR
         SELECT   COD_TPO_SDO, GLS_TPO_SDO AS GLS_TPO_SDO
           FROM   PABS_DT_TIPO_SALDO_ALMOT
          WHERE   FLG_VGN = PKG_PAB_CONSTANTES.V_REG_VIG;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_COMBO_TSALDO;

   --**********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_VAL_RUT
   -- Objetivo: Procedimiento que valida RUT de acuerdo a su digito verificador
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: N/A
   -- Fecha: 19/08/16
   -- Autor: Santander
   -- Input:
   -- p_NUM_RUT -> Numero de RUT
   -- p_DIG_VERIF -> Digito Verificador
   -- Output:
   -- p_FLAG_VAL -> Flag cumple con las condiciones de hora y monto importe maximo -> 0 cumple y 1 no cumple
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_VAL_RUT (p_NUM_RUT     IN     NUMBER,
                             p_DIG_VERIF   IN     CHAR,
                             p_FLAG_VAL       OUT NUMBER,
                             p_ERROR          OUT NUMBER)
   IS
      v_NOM_SP   VARCHAR2 (30) := 'Sp_PAB_VAL_RUT';
      v_RUT      VARCHAR2 (10) := SUBSTR (LPAD (p_NUM_RUT, 9, '0'), 1, 10);
      v_RES      NUMBER (2);
      v_DIG      VARCHAR2 (1);
   BEGIN
      p_ERROR := PKG_PAB_CONSTANTES.v_OK;

      v_RES :=
         11
         - MOD (
                TO_NUMBER (SUBSTR (v_RUT, 1, 1)) * 4
              + TO_NUMBER (SUBSTR (v_RUT, 2, 1)) * 3
              + TO_NUMBER (SUBSTR (v_RUT, 3, 1)) * 2
              + TO_NUMBER (SUBSTR (v_RUT, 4, 1)) * 7
              + TO_NUMBER (SUBSTR (v_RUT, 5, 1)) * 6
              + TO_NUMBER (SUBSTR (v_RUT, 6, 1)) * 5
              + TO_NUMBER (SUBSTR (v_RUT, 7, 1)) * 4
              + TO_NUMBER (SUBSTR (v_RUT, 8, 1)) * 3
              + TO_NUMBER (SUBSTR (v_RUT, 9, 1)) * 2,
              11
           );

      IF v_RES = 10
      THEN
         v_DIG := 'K';
      ELSIF v_RES = 11
      THEN
         v_DIG := '0';
      ELSE
         v_DIG := LTRIM (v_RES);
      END IF;

      IF v_dig = p_DIG_VERIF
      THEN
         p_FLAG_VAL := PKG_PAB_CONSTANTES.V_OK;
      ELSE
         p_FLAG_VAL := PKG_PAB_CONSTANTES.V_error;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_VAL_RUT;

   --***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_COMBO_CAMARA
   -- Objetivo: Consulta los tipos de camara para altos montos
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas:
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- N/A
   -- Output:
   -- p_CURSOR -> SYS_REFCURSOR, RESULTADOS DE CONSULTA.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_COMBO_CAMARA (p_CURSOR OUT SYS_REFCURSOR)
   IS
      v_NOM_SP   VARCHAR2 (30) := 'Sp_PAB_COMBO_CAMARA';
   BEGIN
      OPEN p_CURSOR FOR
         SELECT   COD_TPO_CMA, GLS_TPO_CMA AS GLS_TPO_CMA
           FROM   PABS_DT_TIPO_CAMAR_ALMOT
          WHERE   FLG_VGN = PKG_PAB_CONSTANTES.V_REG_VIG;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_COMBO_CAMARA;

   --***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_COMBO_SUCURSAL
   -- Objetivo: Consulta los tipos de sucursal para altos montos
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas:
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- N/A
   -- Output:
   -- p_CURSOR -> SYS_REFCURSOR, RESULTADOS DE CONSULTA.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_COMBO_SUCURSAL (p_CURSOR OUT SYS_REFCURSOR)
   IS
      v_NOM_SP   VARCHAR2 (30) := 'Sp_PAB_COMBO_SUCURSAL';
   BEGIN
      OPEN p_CURSOR FOR
         SELECT   TCNTELEX AS tccofici, TCNTELEX AS tcmofcur FROM TCDT050;
   --ORDER BY tcmofcur;

   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_COMBO_SUCURSAL;

   --***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_COMBO_SEGMENTO
   -- Objetivo: Consulta los tipos de segmento de los clientes
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas:
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- N/A
   -- Output:
   -- p_CURSOR -> SYS_REFCURSOR, RESULTADOS DE CONSULTA.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_COMBO_SEGMENTO (p_CURSOR OUT SYS_REFCURSOR)
   IS
      v_NOM_SP   VARCHAR2 (30) := 'Sp_PAB_COMBO_SEGMENTO';
   BEGIN
      OPEN p_CURSOR FOR
         SELECT   COD_TPO_SEG_AOS CODIGO, GLS_TPO_SEG_AOS GLOSA
           FROM   PABS_DT_TIPO_SGMTO_ALMOT
          WHERE   FLG_VGN = PKG_PAB_CONSTANTES.V_REG_VIG;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_COMBO_SEGMENTO;

   --***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_COMBO_ESTADO_OPE
   -- Objetivo: Consulta los tipos de estados de operacion
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas:
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- N/A
   -- Output:
   -- p_CURSOR -> SYS_REFCURSOR, RESULTADOS DE CONSULTA.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones:
   --***********************************************************************************************
   PROCEDURE SP_PAB_COMBO_ESTADO_OPE (p_CURSOR OUT SYS_REFCURSOR)
   IS
      v_NOM_SP   VARCHAR2 (30) := 'SP_PAB_COMBO_ESTADO_OPE';
   BEGIN
      OPEN p_CURSOR FOR
           SELECT   COD_EST_AOS CODIGO, DSC_EST_AOS GLOSA
             FROM   PABS_DT_ESTDO_ALMOT
            WHERE   COD_EST_AOS < 40 AND FLG_VGN = PKG_PAB_CONSTANTES.V_REG_VIG
         ORDER BY   DSC_EST_AOS;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_COMBO_ESTADO_OPE;

   --***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_COMBO_ESTADO_NOM
   -- Objetivo: Consulta los tipos de estados de una nomina
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas:
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- N/A
   -- Output:
   -- p_CURSOR -> SYS_REFCURSOR, RESULTADOS DE CONSULTA.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_PAB_COMBO_ESTADO_NOM (p_CURSOR OUT SYS_REFCURSOR)
   IS
      v_NOM_SP   VARCHAR2 (30) := 'SP_PAB_COMBO_ESTADO_NOM';
   BEGIN
      OPEN p_CURSOR FOR
         SELECT   COD_EST_AOS CODIGO, DSC_EST_AOS GLOSA
           FROM   PABS_DT_ESTDO_ALMOT
          WHERE       COD_EST_AOS > 60
                  AND COD_EST_AOS < 80
                  AND FLG_VGN = PKG_PAB_CONSTANTES.V_REG_VIG;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_COMBO_ESTADO_NOM;

   --***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_COMBO_TFONDO
   -- Objetivo: Consulta los tipos de fondos
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas: PABS_DT_TIPO_FONDO_ALMOT
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- N/A
   -- Output:
   -- p_CURSOR -> SYS_REFCURSOR, RESULTADOS DE CONSULTA.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_COMBO_TFONDO (p_CURSOR OUT SYS_REFCURSOR)
   IS
      v_NOM_SP   VARCHAR2 (30) := 'Sp_PAB_COMBO_TFONDO';
   BEGIN
      OPEN p_CURSOR FOR
         SELECT   COD_TPO_FND CODIGO, GLS_TPO_FND GLOSA
           FROM   PABS_DT_TIPO_FONDO_ALMOT
          WHERE   FLG_VGN = PKG_PAB_CONSTANTES.V_REG_VIG;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_COMBO_TFONDO;

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_TRAN_COD_FONDO
   -- Objetivo: Funcion que transforma segun el codigo de fondo
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_TIPO_FONDO_ALMOT
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- p_COD_FND -> Codigo identificador del Fondo.
   -- Output: N/A.
   -- Input/Output: N/A.
   -- Retorno:
   -- v_GLOSA_FONDO -> Retorno Descripcion del Fondo
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   FUNCTION FN_PAB_TRAN_COD_FONDO (p_COD_FND IN CHAR)
      RETURN VARCHAR2
   IS
      --
      v_NOM_SP        VARCHAR2 (30) := 'FN_PAB_TRAN_COD_FONDO';
      v_GLOSA_FONDO   VARCHAR2 (20);
   --
   BEGIN
      --
      IF (NVL (p_COD_FND, '#') <> '#')
      THEN
         SELECT   GLS_TPO_FND GLOSA
           INTO   v_GLOSA_FONDO
           FROM   PABS_DT_TIPO_FONDO_ALMOT
          WHERE   FLG_VGN = PKG_PAB_CONSTANTES.V_REG_VIG
                  AND COD_TPO_FND = p_COD_FND;
      ELSE
         v_GLOSA_FONDO := NULL;
      END IF;

      RETURN v_GLOSA_FONDO;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := 'No existe informacion para: ' || p_COD_FND;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   --
   END FN_PAB_TRAN_COD_FONDO;

   --**********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUS_VAL_ARC
   -- Objetivo: Procedimiento que consultara la tabla donde se almacenaran las distintas relaciones
   -- que existen entre los tipos de mensajes, tipo de operaciones y  sistema de salida.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: N/A
   -- Fecha: 14/09/2016
   -- Autor: Santander
   -- Input:
   -- p_COD_ARC_ENT -> Codigo de archivo de Entrada
   -- Output:
   -- p_CURSOR  -> Cursor con todas las operaciones
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_BUS_VAL_ARC (p_COD_ARC_ENT   IN     CHAR,
                                 p_CURSOR           OUT SYS_REFCURSOR,
                                 p_ERROR            OUT NUMBER)
   IS
      v_NOM_SP   VARCHAR2 (30) := 'Sp_PAB_BUS_VAL_ARC';
   BEGIN
      OPEN p_CURSOR FOR
         SELECT   ARCH.COD_VAL_ARC                                    -- Llave
                                  ,
                  ARCH.COD_TPO_OPE_AOS,
                  ARCH.FLG_MRD_UTZ_SWF,
                  ARCH.COD_MT_SWF,
                  ARCH.COD_CAM_ARC,
                  ARCH.VAL_LST_DTS,
                  ARCH.VAL_ERS_RGL,
                  ARCH.VAL_LRG_ERS_RGL,
                  ARCH.FLG_TPO_ALR,
                  ARCH.FLG_VGN_VAL,
                  ARCH.COD_TBL_DPN,
                  ARCH.COD_CAM_DPN,
                  ARCHENT.NOM_TAG_CAM_ARC
           FROM   PABS_DT_VLDCN_CAMPO_ARCHV ARCH,
                  PABS_DT_ESTRT_ARCHv_ENTRD ARCHENT
          WHERE       ARCH.COD_ARC_ENT = p_COD_ARC_ENT
                  AND ARCH.COD_CAM_ARC = ARCHENT.COD_CAM_ARC
                  AND ARCHENT.COD_ARC_ENT = p_COD_ARC_ENT
                  AND ARCH.FLG_VGN_VAL = PKG_PAB_CONSTANTES.V_REG_VIG
                  AND ARCHENT.FLG_VGN = PKG_PAB_CONSTANTES.V_REG_VIG;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_BUS_VAL_ARC;

   --***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_COMBO_TIPO_OPE_FILMT
   -- Objetivo: PROCEDIMIENTO ALMACENADO ENTREGA LOS TIPOS DE OPERACIONES RELACIONADA AL TIPO DE MENSAJE
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_TIPO_OPRCN_ALMOT
   -- Fecha: 19/10/16
   -- Autor: Santander GSD
   -- Input:
   -- p_COD_MT_SWF -> Codigo tipo de mensaje
   -- P_COD_DVI -> Codigo de moneda
   -- p_COD_SIS_ENT_SAL -> Codigo de sistema
   -- Output:
   -- p_CURSOR -> SYS_REFCURSOR, Resultado de consulta
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_COMBO_TIPO_OPE_FILMT (
      p_COD_MT_SWF        IN     CHAR,
      P_COD_DVI           IN     CHAR,
      p_COD_SIS_ENT_SAL   IN     CHAR,
      p_CURSOR               OUT SYS_REFCURSOR
   )
   IS
      v_NOM_SP            VARCHAR2 (30) := 'Sp_PAB_COMBO_TIPO_OPE_FILMT';
      p_FLG_MRD_UTZ_SWF   CHAR (3);
   BEGIN
      IF (p_COD_DVI = PKG_PAB_CONSTANTES.v_STR_DVI_CLP)
      THEN
         p_FLG_MRD_UTZ_SWF := PKG_PAB_CONSTANTES.v_FLG_MRD_MN;
      ELSE
         p_FLG_MRD_UTZ_SWF := PKG_PAB_CONSTANTES.v_FLG_MRD_MX;
      END IF;

      OPEN p_CURSOR FOR
           SELECT   TOP.COD_TPO_OPE_AOS AS CODIGO,
                    TOP.COD_TPO_OPE_AOS AS DESCRIPCION
             FROM   PABS_DT_TIPO_OPRCN_ALMOT TOP,
                    PABS_DT_TIPO_OPRCN_SALID_MNSJ RTOP
            WHERE   TOP.COD_TPO_OPE_AOS = RTOP.COD_TPO_OPE_AOS
                    AND TOP.FLG_VGN = PKG_PAB_CONSTANTES.v_REG_VIG
                    AND TOP.COD_TPO_OPE_AOS NOT IN
                             (PKG_PAB_CONSTANTES.V_STR_TPO_OPE_DEVFDOS, --No muestra devoluciones
                              PKG_PAB_CONSTANTES.V_STR_TPO_OPE_OPB)
                    AND RTOP.FLG_VGN = PKG_PAB_CONSTANTES.v_REG_VIG
                    AND RTOP.COD_SIS_ENT_SAL =
                          NVL (p_COD_SIS_ENT_SAL, RTOP.COD_SIS_ENT_SAL)
                    AND RTOP.COD_MT_SWF = NVL (p_COD_MT_SWF, RTOP.COD_MT_SWF)
                    AND RTOP.FLG_MRD_UTZ_SWF =
                          NVL (p_FLG_MRD_UTZ_SWF, RTOP.FLG_MRD_UTZ_SWF)
         ORDER BY   TOP.COD_TPO_OPE_AOS;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   --
   END Sp_PAB_COMBO_TIPO_OPE_FILMT;

   --***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_COMBO_MONEDA_FILMT
   -- Objetivo: Devuelve el codigo del tipo de Fondo
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_MONEDA_ALMOT
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- p_COD_MT_SWF:
   -- Output:
   -- p_CURSOR -> SYS_REFCURSOR, RESULTADOS DE CONSULTA.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_COMBO_MONEDA_FILMT (p_COD_MT_SWF   IN     CHAR,
                                        p_CURSOR          OUT SYS_REFCURSOR)
   IS
      --
      v_NOM_SP   VARCHAR2 (30) := 'Sp_PAB_COMBO_MONEDA_FILMT';
   --
   BEGIN
      --
      IF p_COD_MT_SWF = PKG_PAB_CONSTANTES.V_COD_MT200
      THEN
         OPEN p_CURSOR FOR
              SELECT   MON.COD_DVI AS CODIGO, MON.COD_DVI AS DESCRIPCION
                FROM   PABS_DT_MONDA_ALMOT MON, TCDT080 MOH
               WHERE       MOH.CDDIVISS = MON.COD_DVI
                       AND MON.COD_DVI <> PKG_PAB_CONSTANTES.V_STR_DVI_CLP
                       AND MON.FLG_VGN = PKG_PAB_CONSTANTES.v_REG_VIG
            ORDER BY   MON.COD_DVI;
      ELSIF p_COD_MT_SWF = PKG_PAB_CONSTANTES.V_COD_MT205
      THEN
         OPEN p_CURSOR FOR
              SELECT   MON.COD_DVI AS CODIGO, MON.COD_DVI AS DESCRIPCION
                FROM   PABS_DT_MONDA_ALMOT MON, TCDT080 MOH
               WHERE       MOH.CDDIVISS = MON.COD_DVI
                       AND MON.COD_DVI = PKG_PAB_CONSTANTES.V_STR_DVI_CLP
                       AND FLG_VGN = PKG_PAB_CONSTANTES.v_REG_VIG
            ORDER BY   MON.COD_DVI;
      ELSE
         OPEN p_CURSOR FOR
              SELECT   MON.COD_DVI AS CODIGO, MON.COD_DVI AS DESCRIPCION
                FROM   PABS_DT_MONDA_ALMOT MON, TCDT080 MOH
               WHERE   MOH.CDDIVISS = MON.COD_DVI
                       AND MON.FLG_VGN = PKG_PAB_CONSTANTES.v_REG_VIG
            ORDER BY   MON.COD_DVI;
      END IF;
   --
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   --
   END Sp_PAB_COMBO_MONEDA_FILMT;

   --**********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_BUS_DTO_BFC_ING_UNI
   -- Objetivo: Busca los datos del beneficiario
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas:
   -- Fecha: 26/09/16
   -- Autor: Santander
   -- Input:
   -- p_COD_USR -> rut usuario
   -- p_COD_BIC -> BIC
   -- Output:
   -- P_DATOS_BFC --> Datos del beneficiario
   -- P_NUM_CTA_BFC --> Cuentas del beneficiario
   -- P_NUM_CTA_DCV_BFC --> Cuenta DCV Beneficiario
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_PAB_BUS_DTO_BFC_ING_UNI (
      -- ENTRADAS
      P_COD_USR           IN     CHAR,
      P_COD_BIC           IN     CHAR,
      -- SALIDAS
      P_DATOS_BFC            OUT SYS_REFCURSOR,
      P_NUM_CTA_BFC          OUT SYS_REFCURSOR,
      P_NUM_CTA_DCV_BFC      OUT SYS_REFCURSOR,
      P_ERROR                OUT NUMBER
   )
   IS
      --
      V_NOM_SP    VARCHAR2 (30) := 'SP_PAB_BUS_DTO_BFC_ING_UNI';
      V_COD_BIC   CHAR (11);
   --
   BEGIN
      --Seteo de variables
      V_COD_BIC := P_COD_BIC;

      BEGIN
         -- DATOS BENEFICIARIO
         OPEN P_DATOS_BFC FOR
              SELECT   OPRCN.NOM_BFC AS NOM_BFC,
                       OPCON.GLS_DIR_BFC AS GLS_DIR_BFC,
                       OPCON.NOM_CDD_BFC AS NOM_CDD_BFC,
                       OPCON.COD_PAS_BFC AS COD_PAS_BFC,
                       PAIS.TCMPAIS AS GLOSA_PAIS
                FROM   PABS_DT_DETLL_OPRCN OPRCN,
                       PABS_DT_OPRCN_INFCN_OPCON OPCON,
                       TGEN_0112 PAIS
               WHERE       OPRCN.NUM_DOC_BFC = P_COD_USR
                       AND OPCON.COD_PAS_BFC = PAIS.TCCPAIS4
                       AND OPCON.NUM_DOC_BFC = OPRCN.NUM_DOC_BFC
                       --AND OPCON.NUM_FOL_OPE = OPRCN.NUM_FOL_OPE
                       AND OPCON.FEC_ISR_OPE =
                             (SELECT   MAX (FEC_ISR_OPE)
                                FROM   PABS_DT_OPRCN_INFCN_OPCON OPCON
                               WHERE   OPCON.NUM_DOC_BFC = P_COD_USR)
                       AND ROWNUM = 1
            ORDER BY   OPRCN.FEC_ISR_OPE DESC;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            ERR_CODE := SQLCODE;
            ERR_MSG := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (ERR_CODE,
                                                V_NOM_PCK,
                                                ERR_MSG,
                                                V_NOM_SP);
            P_ERROR := PKG_PAB_CONSTANTES.V_ERROR;
         WHEN OTHERS
         THEN
            ERR_CODE := SQLCODE;
            ERR_MSG := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (ERR_CODE,
                                                V_NOM_PCK,
                                                ERR_MSG,
                                                V_NOM_SP);
            P_ERROR := PKG_PAB_CONSTANTES.V_ERROR;
            P_S_MENSAJE := ERR_CODE || '-' || ERR_MSG;
            RAISE_APPLICATION_ERROR (-20000, P_S_MENSAJE);
      END;

      BEGIN
         -- NUMERO CUENTAS BENEFICIARIO
         OPEN P_NUM_CTA_BFC FOR
            SELECT   DISTINCT (CUENTA.NUM_CTA_TRF) AS NUM_CTA_BFC
              FROM   PABS_DT_CENTA_TRNSC CUENTA
             WHERE   CUENTA.NUM_DOC_PSN = P_COD_USR
                     AND CUENTA.COD_BIC_BCO =
                           NVL (V_COD_BIC, CUENTA.COD_BIC_BCO)
                     AND CUENTA.FLG_DCV = PKG_PAB_CONSTANTES.V_FLG_DCV_NO;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            ERR_CODE := SQLCODE;
            ERR_MSG := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (ERR_CODE,
                                                V_NOM_PCK,
                                                ERR_MSG,
                                                V_NOM_SP);
            P_ERROR := PKG_PAB_CONSTANTES.V_ERROR;
         WHEN OTHERS
         THEN
            ERR_CODE := SQLCODE;
            ERR_MSG := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (ERR_CODE,
                                                V_NOM_PCK,
                                                ERR_MSG,
                                                V_NOM_SP);
            P_ERROR := PKG_PAB_CONSTANTES.V_ERROR;
            P_S_MENSAJE := ERR_CODE || '-' || ERR_MSG;
            RAISE_APPLICATION_ERROR (-20000, P_S_MENSAJE);
      END;

      BEGIN
         -- NUMERO CUENTAS BENEFICIARIO
         OPEN P_NUM_CTA_DCV_BFC FOR
            SELECT   DISTINCT (CUENTA.NUM_CTA_TRF) AS NUM_CTA_DCV_BFC
              FROM   PABS_DT_CENTA_TRNSC CUENTA
             WHERE   CUENTA.NUM_DOC_PSN = P_COD_USR
                     AND CUENTA.COD_BIC_BCO =
                           NVL (V_COD_BIC, CUENTA.COD_BIC_BCO)
                     AND CUENTA.FLG_DCV = PKG_PAB_CONSTANTES.V_FLG_DCV_SI;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            ERR_CODE := SQLCODE;
            ERR_MSG := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (ERR_CODE,
                                                V_NOM_PCK,
                                                ERR_MSG,
                                                V_NOM_SP);
            P_ERROR := PKG_PAB_CONSTANTES.V_ERROR;
         WHEN OTHERS
         THEN
            ERR_CODE := SQLCODE;
            ERR_MSG := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (ERR_CODE,
                                                V_NOM_PCK,
                                                ERR_MSG,
                                                V_NOM_SP);
            P_ERROR := PKG_PAB_CONSTANTES.V_ERROR;
            P_S_MENSAJE := ERR_CODE || '-' || ERR_MSG;
            RAISE_APPLICATION_ERROR (-20000, P_S_MENSAJE);
      END;

      --
      P_ERROR := PKG_PAB_CONSTANTES.V_OK;
   --
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         ERR_CODE := SQLCODE;
         ERR_MSG := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (ERR_CODE,
                                             V_NOM_PCK,
                                             ERR_MSG,
                                             V_NOM_SP);
         P_ERROR := PKG_PAB_CONSTANTES.V_ERROR;
      WHEN OTHERS
      THEN
         ERR_CODE := SQLCODE;
         ERR_MSG := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (ERR_CODE,
                                             V_NOM_PCK,
                                             ERR_MSG,
                                             V_NOM_SP);
         P_ERROR := PKG_PAB_CONSTANTES.V_ERROR;
         P_S_MENSAJE := ERR_CODE || '-' || ERR_MSG;
         RAISE_APPLICATION_ERROR (-20000, P_S_MENSAJE);
   --
   END SP_PAB_BUS_DTO_BFC_ING_UNI;

   --**********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUS_DTO_ODN_ING_UNI
   -- Objetivo: Busca los datos del Ordenante
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas:
   -- Fecha: 26/09/16
   -- Autor: Santander
   -- Input:
   -- p_COD_USR -> rut usuario
   -- Output:
   -- P_DATOS_ODN --> Datos del ordenante
   -- P_NUM_CTA_ODN --> Cuentas del ordenante
   -- P_NUM_CTA_DCV_ODN --> Cuenta DCV Ordenante
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_PAB_BUS_DTO_ODN_ING_UNI (
      -- ENTRADAS
      P_COD_USR           IN     CHAR,
      -- SALIDAS
      P_DATOS_ODN            OUT SYS_REFCURSOR,
      P_NUM_CTA_ODN          OUT SYS_REFCURSOR,
      P_NUM_CTA_DCV_ODN      OUT SYS_REFCURSOR,
      P_ERROR                OUT NUMBER
   )
   IS
      V_NOM_SP   VARCHAR2 (30) := 'SP_PAB_BUS_DTO_ODN_ING_UNI';
   BEGIN
      BEGIN
         -- DATOS ORDENANTE
         OPEN P_DATOS_ODN FOR
            SELECT   OPCON.NOM_ODN,
                     OPCON.GLS_DIR_ODN AS GLS_DIR_ODN,
                     OPCON.NOM_CDD_ODN AS NOM_CDD_ODN,
                     OPCON.COD_PAS_ODN AS COD_PAS_ODN
              FROM   PABS_DT_OPRCN_INFCN_OPCON OPCON
             WHERE   OPCON.NUM_DOC_ODN = P_COD_USR
                     AND OPCON.FEC_ISR_OPE =
                           (SELECT   MAX (FEC_ISR_OPE)
                              FROM   PABS_DT_OPRCN_INFCN_OPCON OPCON
                             WHERE   OPCON.NUM_DOC_ODN = P_COD_USR)
                     AND ROWNUM = 1;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            ERR_CODE := SQLCODE;
            ERR_MSG := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (ERR_CODE,
                                                V_NOM_PCK,
                                                ERR_MSG,
                                                V_NOM_SP);
            P_ERROR := PKG_PAB_CONSTANTES.V_ERROR;
         WHEN OTHERS
         THEN
            ERR_CODE := SQLCODE;
            ERR_MSG := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (ERR_CODE,
                                                V_NOM_PCK,
                                                ERR_MSG,
                                                V_NOM_SP);
            P_ERROR := PKG_PAB_CONSTANTES.V_ERROR;
            P_S_MENSAJE := ERR_CODE || '-' || ERR_MSG;
            RAISE_APPLICATION_ERROR (-20000, P_S_MENSAJE);
      END;

      BEGIN
         -- NUMERO CUENTAS ORDENANTE
         OPEN P_NUM_CTA_ODN FOR
            SELECT   DISTINCT (CUENTA.NUM_CTA_TRF) AS NUM_CTA_ODN
              FROM   PABS_DT_CENTA_TRNSC CUENTA
             WHERE   CUENTA.NUM_DOC_PSN = P_COD_USR
                     AND CUENTA.COD_BIC_BCO IN
                              (PKG_PAB_CONSTANTES.V_BIC_BANCO_SANTANDER,
                               PKG_PAB_CONSTANTES.V_BIC_BANCO_SANTANDER_HOM)
                     AND CUENTA.FLG_DCV = PKG_PAB_CONSTANTES.V_FLG_DCV_NO;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            ERR_CODE := SQLCODE;
            ERR_MSG := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (ERR_CODE,
                                                V_NOM_PCK,
                                                ERR_MSG,
                                                V_NOM_SP);
            P_ERROR := PKG_PAB_CONSTANTES.V_ERROR;
         WHEN OTHERS
         THEN
            ERR_CODE := SQLCODE;
            ERR_MSG := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (ERR_CODE,
                                                V_NOM_PCK,
                                                ERR_MSG,
                                                V_NOM_SP);
            P_ERROR := PKG_PAB_CONSTANTES.V_ERROR;
            P_S_MENSAJE := ERR_CODE || '-' || ERR_MSG;
            RAISE_APPLICATION_ERROR (-20000, P_S_MENSAJE);
      END;

      BEGIN
         -- NUMERO CUENTAS ORDENANTE
         OPEN P_NUM_CTA_DCV_ODN FOR
            SELECT   DISTINCT (CUENTA.NUM_CTA_TRF) AS NUM_CTA_DCV_ODN
              FROM   PABS_DT_CENTA_TRNSC CUENTA
             WHERE   CUENTA.NUM_DOC_PSN = P_COD_USR
                     AND CUENTA.COD_BIC_BCO IN
                              (PKG_PAB_CONSTANTES.V_BIC_BANCO_SANTANDER,
                               PKG_PAB_CONSTANTES.V_BIC_BANCO_SANTANDER_HOM)
                     AND CUENTA.FLG_DCV = PKG_PAB_CONSTANTES.V_FLG_DCV_SI;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            ERR_CODE := SQLCODE;
            ERR_MSG := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (ERR_CODE,
                                                V_NOM_PCK,
                                                ERR_MSG,
                                                V_NOM_SP);
            P_ERROR := PKG_PAB_CONSTANTES.V_ERROR;
         WHEN OTHERS
         THEN
            ERR_CODE := SQLCODE;
            ERR_MSG := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (ERR_CODE,
                                                V_NOM_PCK,
                                                ERR_MSG,
                                                V_NOM_SP);
            P_ERROR := PKG_PAB_CONSTANTES.V_ERROR;
            P_S_MENSAJE := ERR_CODE || '-' || ERR_MSG;
            RAISE_APPLICATION_ERROR (-20000, P_S_MENSAJE);
      END;

      P_ERROR := PKG_PAB_CONSTANTES.V_OK;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         ERR_CODE := SQLCODE;
         ERR_MSG := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (ERR_CODE,
                                             V_NOM_PCK,
                                             ERR_MSG,
                                             V_NOM_SP);
         P_ERROR := PKG_PAB_CONSTANTES.V_ERROR;
      WHEN OTHERS
      THEN
         ERR_CODE := SQLCODE;
         ERR_MSG := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (ERR_CODE,
                                             V_NOM_PCK,
                                             ERR_MSG,
                                             V_NOM_SP);
         P_ERROR := PKG_PAB_CONSTANTES.V_ERROR;
         P_S_MENSAJE := ERR_CODE || '-' || ERR_MSG;
         RAISE_APPLICATION_ERROR (-20000, P_S_MENSAJE);
   --
   END SP_PAB_BUS_DTO_ODN_ING_UNI;

   -- **********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_VAL_MONDA_ALMOT
   -- Objetivo: Procedimiento que informa si el importe ingresado excede o no el definido en la moneda.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_MONDA_ALMOT
   -- Fecha: 19/08/16
   -- Autor: Santander
   -- Input:
   -- p_COD_DVI -> Codigo Divisa
   -- p_IMP_INF -> Importe informado para consultar en tabla de moneda
   -- Output:
   -- p_FLG_IMP_VAL -> Flag monto importe valido = 1 o invalido = 0
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_VAL_MONDA_ALMOT (p_COD_DVI       IN     CHAR,
                                     p_IMP_INF       IN     NUMBER,
                                     p_FLG_IMP_VAL      OUT NUMBER,
                                     p_ERROR            OUT NUMBER)
   IS
      v_NOM_SP            VARCHAR2 (30) := 'Sp_PAB_VAL_MONDA_ALMOT';
      v_IMP_MAX_SIN_ATZ   NUMBER;
   BEGIN
      p_ERROR := PKG_PAB_CONSTANTES.v_OK; --Procedimiento ejecutado de forma correcta

      BEGIN
         SELECT   IMP_MAX_SIN_ATZ
           INTO   v_IMP_MAX_SIN_ATZ
           FROM   PABS_DT_MONDA_ALMOT
          WHERE   COD_DVI = p_COD_DVI
                  AND FLG_VGN = PKG_PAB_CONSTANTES.V_REG_VIG;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            p_s_mensaje := err_code || '-' || err_msg;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;

      --Si el monto es 0 se entiende que la moneda no tiene restriccion

      IF p_IMP_INF <= v_IMP_MAX_SIN_ATZ OR v_IMP_MAX_SIN_ATZ = 0
      THEN
         p_FLG_IMP_VAL := PKG_PAB_CONSTANTES.V_OK;
      ELSE
         p_FLG_IMP_VAL := PKG_PAB_CONSTANTES.V_error;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_VAL_MONDA_ALMOT;

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_OBT_DSC_SIS_ENT_SAL
   -- Objetivo: Funcion que devuelve la descripcion del codigo del sistema de entrada o salida
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_SISTM_ENTRD_SALID
   -- Fecha: 18/10/2016
   -- Autor: Santander
   -- Input:
   -- p_COD_SIS_ENT_SAL -> Codigo de sistema salida
   -- Output:
   -- N/A.
   -- Input/Output:
   -- N/A.
   -- Retorno:
   -- v_DSC_SIS_ENT_SAL -> Descripcion de codigo de sistema salida consultado
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   FUNCTION FN_PAB_OBT_DSC_SIS_ENT_SAL (p_COD_SIS_ENT_SAL IN CHAR)
      RETURN VARCHAR2
   IS
      v_NOM_SP            VARCHAR2 (30) := 'FN_PAB_OBT_DSC_SIS_ENT_SAL';
      v_DSC_SIS_ENT_SAL   VARCHAR2 (50);
      ERR_COD_INEX EXCEPTION;
   BEGIN
      IF (NVL (p_COD_SIS_ENT_SAL, '#') <> '#')
      THEN
         SELECT   DSC_SIS_ENT_SAL
           INTO   v_DSC_SIS_ENT_SAL
           FROM   PABS_DT_SISTM_ENTRD_SALID
          WHERE   COD_SIS_ENT_SAL = p_COD_SIS_ENT_SAL;
      ELSE
         v_DSC_SIS_ENT_SAL := NULL;
      END IF;

      RETURN v_DSC_SIS_ENT_SAL;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := 'No existe informacion para: ' || p_COD_SIS_ENT_SAL;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END FN_PAB_OBT_DSC_SIS_ENT_SAL;

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_OBT_COD_DSC_EST
   -- Objetivo: Funcion que devuelve codigo del estado de nomina u operacion consultando por la
   -- descripcion del estado
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PKG_PAB_CONSTANTES
   -- Fecha: 14/10/2016
   -- Autor: Santander
   -- Input:
   -- p_DSC_EST -> Descripcion del eatado de la operacion informada
   -- p_TIP_ORI -> Tipo origen que consulta estado
   -- Output:
   -- N/A.
   -- Input/Output:
   -- N/A.
   -- Retorno:
   -- v_COD_EST -> Codigo del estado de la operacion informada
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   FUNCTION FN_PAB_OBT_COD_DSC_EST (p_DSC_EST VARCHAR2, p_TIP_ORI IN NUMBER)
      RETURN NUMBER
   IS
      v_NOM_SP    VARCHAR2 (30) := 'FN_PAB_OBT_COD_DSC_EST';
      v_COD_EST   NUMBER (5);
      ERR_COD_INEX EXCEPTION;
   BEGIN
      IF p_TIP_ORI = PKG_PAB_CONSTANTES.V_IND_NMN
      THEN
         CASE
            /*Nominas*/
            WHEN p_DSC_EST = PKG_PAB_CONSTANTES.V_STR_EST_AOS_NMNVAL
            THEN
               --Nomina Valida
               v_COD_EST := PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNVAL;
            WHEN p_DSC_EST = PKG_PAB_CONSTANTES.V_STR_EST_AOS_NMNCUR
            THEN
               --Nomina Cursada(Para Aprobar)
               v_COD_EST := PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNCUR;
            WHEN p_DSC_EST = PKG_PAB_CONSTANTES.V_STR_EST_AOS_NMNINV
            THEN
               --Nomina Invalida
               v_COD_EST := PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNINV;
            WHEN p_DSC_EST = PKG_PAB_CONSTANTES.V_STR_EST_AOS_NMNAUT
            THEN
               --Nomina Autorizada
               v_COD_EST := PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNAUT;
            WHEN p_DSC_EST = PKG_PAB_CONSTANTES.V_STR_EST_AOS_NMNREC
            THEN
               --Nomina Rechazada
               v_COD_EST := PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNREC;
            WHEN p_DSC_EST = PKG_PAB_CONSTANTES.V_STR_EST_AOS_NMNELI
            THEN
               --Nomina Eliminada
               v_COD_EST := PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNELI;
            WHEN p_DSC_EST = PKG_PAB_CONSTANTES.V_STR_EST_AOS_NMNDUP
            THEN
               --Nomina Duplicada
               v_COD_EST := PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNDUP;
            ELSE
               RAISE ERR_COD_INEX;
         END CASE;
      ELSE
         CASE
            /*Operaciones*/
            WHEN p_DSC_EST = PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEVAL
            THEN
               --Operacion Valida(3)
               v_COD_EST := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEVAL;
            WHEN p_DSC_EST = PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEINV
            THEN
               --Operacion Invalida(2)
               v_COD_EST := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEINV;
            WHEN p_DSC_EST = PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPECUR
            THEN
               --Operacion Cursada(5)
               v_COD_EST := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPECUR;
            WHEN p_DSC_EST = PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEREC
            THEN
               --Operacion Rechazada(6)
               v_COD_EST := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEREC;
            WHEN p_DSC_EST = PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEAUT
            THEN
               --Operacion Autorizada(7)
               v_COD_EST := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT;
            WHEN p_DSC_EST = PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPELIB
            THEN
               --Operacion Liberada (10)
               v_COD_EST := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPELIB;
            WHEN p_DSC_EST = PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEENV
            THEN
               --Operacion Enviada (13)
               v_COD_EST := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEENV;
            WHEN p_DSC_EST = PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEPAG
            THEN
               --Operacion Pagada (12)
               v_COD_EST := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAG;
            WHEN p_DSC_EST = PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEDUP
            THEN
               --Operacion Duplicada (17)
               v_COD_EST := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDUP;
            WHEN p_DSC_EST = PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEELI
            THEN
               --Operacion Eliminada(4)
               v_COD_EST := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEELI;
            WHEN p_DSC_EST = PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEPVI
            THEN
               --Operacion Por Visar (8)
               v_COD_EST := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPVI;
            WHEN p_DSC_EST = PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEPAS
            THEN
               -- Operacion Por Asociar(19)
               v_COD_EST := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAS;
            WHEN p_DSC_EST = PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEPAB
            THEN
               -- Operacion Por Abonar(16)
               v_COD_EST := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAB;
            WHEN p_DSC_EST = PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEABN
            THEN
               -- Operacion ABONADA(15)
               v_COD_EST := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEABN;
            WHEN p_DSC_EST = PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEDEV
            THEN
               -- Operacion DEVUELTA(18)
               v_COD_EST := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDEV;
            WHEN p_DSC_EST = PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEPDV
            THEN
               -- Operacion Por Devolver(23)
               v_COD_EST := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPDV;
            WHEN p_DSC_EST = PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEINGAR
            THEN
               v_COD_EST := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEINGAR; --Ingreso garantia(20)
            WHEN p_DSC_EST = PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEPAGCFI
            THEN
               v_COD_EST := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAGCFI; --Pagada sinacofi (21)
            WHEN p_DSC_EST = PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEING
            THEN
               v_COD_EST := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEING; --Ingresada
            WHEN p_DSC_EST = PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPENDE
            THEN
               v_COD_EST := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPENDE; --No devolucion
            WHEN p_DSC_EST = PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEPDEB
            THEN
               v_COD_EST := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPDEB; --No devolucion
            WHEN p_DSC_EST = PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEASO
            THEN
               v_COD_EST := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEASO; --Asociado
            WHEN p_DSC_EST = PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEPAT
            THEN
               v_COD_EST := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAT; --Por Autorizar
            WHEN p_DSC_EST = PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEPROBLM
            THEN
               v_COD_EST := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPROBLM; --OPE CON PROB.
            WHEN p_DSC_EST = PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEPAT
            THEN
               v_COD_EST := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAT; --CONFIRMAR ABONO
            ELSE
               RAISE ERR_COD_INEX;
         END CASE;
      END IF;

      RETURN v_COD_EST;
   EXCEPTION
      WHEN ERR_COD_INEX
      THEN
         err_msg := 'Se esta invocando con codigo inexistente:' || p_DSC_EST;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (NULL,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END FN_PAB_OBT_COD_DSC_EST;

   --

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_OBT_DSC_COD_EST
   -- Objetivo: Funcion que devuelve descripcion del estado de nomina u operacion consultando por el
   -- codigo del estado
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PKG_PAB_CONSTANTES
   -- Fecha: 14/10/2016
   -- Autor: Santander
   -- Input:
   -- p_COD_EST -> Codigo del eatado de la operacion informada
   -- Output:
   -- N/A.
   -- Input/Output:
   -- N/A.
   -- Retorno:
   -- v_DSC_ESTADO -> Descripcion del estado de la operacion informada
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   FUNCTION FN_PAB_OBT_DSC_COD_EST (p_COD_EST IN VARCHAR2)
      RETURN VARCHAR2
   IS
      v_NOM_SP       VARCHAR2 (30) := 'FN_PAB_OBT_DSC_COD_EST';
      v_DSC_ESTADO   VARCHAR2 (30);
      ERR_COD_INEX EXCEPTION;
   BEGIN
      IF (NVL (p_COD_EST, 0) <> 0)
      THEN
         SELECT   DSC_EST_AOS
           INTO   v_DSC_ESTADO
           FROM   PABS_DT_ESTDO_ALMOT
          WHERE   COD_EST_AOS = p_COD_EST;
      ELSE
         v_DSC_ESTADO := NULL;
      END IF;

      RETURN v_DSC_ESTADO;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := 'No existe informacion para: ' || v_DSC_ESTADO;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END FN_PAB_OBT_DSC_COD_EST;

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_OBT_TIP_ING
   -- Objetivo: Funcion que devuelve la descripcion del codigo del tipo de ingreso
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_TIPO_INGRO_ALMOT
   -- Fecha: 18/10/2016
   -- Autor: Santander
   -- Input:
   -- p_COD_TPO_ING -> Codigo de tipo de ingreso
   -- Output:
   -- N/A.
   -- Input/Output:
   -- N/A.
   -- Retorno:
   -- v_DSC_TPO_ING -> Descripcion de codigo de tipo de ingreso
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   FUNCTION FN_PAB_OBT_TIP_ING (p_COD_TPO_ING IN NUMBER)
      RETURN VARCHAR2
   IS
      v_NOM_SP        VARCHAR2 (30) := 'FN_PAB_OBT_TIP_ING';
      v_DSC_TPO_ING   VARCHAR2 (25);
      ERR_COD_INEX EXCEPTION;
   BEGIN
      IF (NVL (p_COD_TPO_ING, -1) <> -1)
      THEN
         SELECT   DSC_TPO_ING
           INTO   v_DSC_TPO_ING
           FROM   PABS_DT_TIPO_INGRO_ALMOT
          WHERE   COD_TPO_ING = p_COD_TPO_ING
                  AND FLG_VGN = PKG_PAB_CONSTANTES.v_REG_VIG;
      ELSE
         v_DSC_TPO_ING := NULL;
      END IF;

      RETURN v_DSC_TPO_ING;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := 'No existe informacion para: ' || v_DSC_TPO_ING;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END FN_PAB_OBT_TIP_ING;

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_OBT_DSC_TIP_OPE
   -- Objetivo: Funcion que devuelve la descripcion del codigo del tipo de operacion
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_TIPO_OPRCN_ALMOT
   -- Fecha: 18/10/2016
   -- Autor: Santander
   -- Input:
   -- p_COD_TPO_OPE_AOS -> Codigo de tipo operacion
   -- Output:
   -- N/A.
   -- Input/Output:
   -- N/A.
   -- Retorno:
   -- v_DSC_TPO_OPE_AOS -> Retorna Descripcion de codigo de tipo operacion
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   FUNCTION FN_PAB_OBT_DSC_TIP_OPE (p_COD_TPO_OPE_AOS IN CHAR)
      RETURN VARCHAR2
   IS
      v_NOM_SP            VARCHAR2 (30) := 'FN_PAB_OBT_DSC_TIP_OPE';
      v_DSC_TPO_OPE_AOS   VARCHAR2 (100);
      ERR_COD_INEX EXCEPTION;
   BEGIN
      IF (NVL (p_COD_TPO_OPE_AOS, '#') <> '#')
      THEN
         SELECT   DSC_TPO_OPE_AOS
           INTO   v_DSC_TPO_OPE_AOS
           FROM   PABS_DT_TIPO_OPRCN_ALMOT
          WHERE   COD_TPO_OPE_AOS = p_COD_TPO_OPE_AOS
                  AND FLG_VGN = PKG_PAB_CONSTANTES.v_REG_VIG;
      ELSE
         v_DSC_TPO_OPE_AOS := NULL;
      END IF;

      RETURN v_DSC_TPO_OPE_AOS;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := 'No existe informacion para: ' || p_COD_TPO_OPE_AOS;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END FN_PAB_OBT_DSC_TIP_OPE;

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_OBT_DSC_BCO
   -- Objetivo: Funcion que devuelve la descripcion del codigo del banco
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: TCDTBAI
   -- Fecha: 18/10/2016
   -- Autor: Santander
   -- Input:
   -- p_TGCDSWSA -> Codigo de banco
   -- Output:
   -- N/A.
   -- Input/Output:
   -- N/A.
   -- Retorno:
   -- v_DES_BCO -> Descripcion de codigo de tipo de ingreso
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   FUNCTION FN_PAB_OBT_DSC_BCO (p_TGCDSWSA IN CHAR)
      RETURN CHAR
   IS
      v_NOM_SP    VARCHAR2 (30) := 'FN_PAB_OBT_DSC_BCO';
      v_DES_BCO   CHAR (40);
      ERR_COD_INEX EXCEPTION;
   BEGIN
      IF (NVL (p_TGCDSWSA, '#') <> '#')
      THEN
         SELECT   DISTINCT DES_BCO
           INTO   v_DES_BCO
           FROM   TCDTBAI
          WHERE   TGCDSWSA = p_TGCDSWSA;
      ELSE
         v_DES_BCO := NULL;
      END IF;

      RETURN v_DES_BCO;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := 'No existe informacion para: ' || p_TGCDSWSA;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         RETURN NULL;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END FN_PAB_OBT_DSC_BCO;

   --***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_COMBO_CRPSL
   -- Objetivo: PROCEDIMIENTO ALMACENADO QUE CONSULTA Y ENTREGA TODOS LOS BANCOS CORRESPONSALES DEL SISTEMA
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_ARCHv_ENTRD_SALID
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- N/A
   -- Output:
   -- p_CURSOR -> SYS_REFCURSOR, resultados de consulta.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_PAB_COMBO_CRPSL (p_CURSOR OUT SYS_REFCURSOR)
   IS
      v_NOM_SP           VARCHAR2 (30) := 'SP_PAB_COMBO_CRPSL';
      V_CORRESPONSAL_S   CHAR (1) := 'S';
   --

   BEGIN
      OPEN p_CURSOR FOR
           SELECT   BAN.COD_BIC_BCO, BAN.COD_BIC_BCO AS GLS_BAN_DEST
             FROM   PABS_DT_BANCO_ALMOT BAN, TCDTBAI TBAI
            WHERE       BAN.FLG_VGN = PKG_PAB_CONSTANTES.v_REG_VIG
                    AND BAN.COD_BIC_BCO = TBAI.TGCDSWSA
                    AND TBAI.FLG_COR = V_CORRESPONSAL_S
         ORDER BY   BAN.COD_BIC_BCO;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         ERR_CODE := SQLCODE;
         ERR_MSG := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (ERR_CODE,
                                             v_NOM_PCK,
                                             ERR_MSG,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         ERR_CODE := SQLCODE;
         ERR_MSG := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (ERR_CODE,
                                             v_NOM_PCK,
                                             ERR_MSG,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_COMBO_CRPSL;

   --
   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_COMBO_EST_DEV_EGR
   -- Objetivo: Procedimiento que consulta estados Por Abonar o Por Asociar
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_ESTDO_ALMOT
   -- Fecha: 27/10/16
   -- Autor: Santander
   -- Input:
   -- N/A
   -- Output:
   -- p_CURSOR -> Cursor con los registros de los 2 estados
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_COMBO_EST_DEV_EGR (p_CURSOR   OUT SYS_REFCURSOR,
                                       p_ERROR    OUT NUMBER)
   IS
      --Seteo de Variables
      V_NOM_SP   VARCHAR2 (30) := 'SP_PAB_COMBO_EST_DEV_EGR';
   BEGIN
      P_ERROR := PKG_PAB_CONSTANTES.V_OK;

      OPEN p_CURSOR FOR
           SELECT   COD_EST_AOS AS CODIGO, DSC_EST_AOS AS DESCRIPCION
             FROM   PABS_DT_ESTDO_ALMOT
            WHERE   COD_EST_AOS IN
                          (PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAB,
                           PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAS)
                    AND FLG_VGN = PKG_PAB_CONSTANTES.v_REG_VIG
         ORDER BY   1 ASC;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);

         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_COMBO_EST_DEV_EGR;

   --**********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_BTCOR_OPRCN
   -- Objetivo: Procedimiento que devuelve la bitacora de una operacion
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_BTCOR_OPRCN, PABS_DT_ESTDO_ALMOT
   -- Fecha: 07/12/2016
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_OPE
   -- p_FEC_ISR_OPE
   -- Output:
   -- p_CURSOR_BTCOR
   -- p_ERROR -> Indicador de errores (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_PAB_BTCOR_OPRCN (p_NUM_FOL_OPE    IN     NUMBER,
                                 p_FEC_ISR_OPE    IN     DATE,
                                 p_CURSOR_BTCOR      OUT SYS_REFCURSOR,
                                 p_ERROR             OUT NUMBER)
   IS
      v_NOM_SP   VARCHAR2 (30) := 'SP_PAB_BTCOR_OPRCN';
   BEGIN
      --Buscamos las nominas segun estado que solicita el front.
      OPEN p_CURSOR_BTCOR FOR
           SELECT   FEC_BTC,
                    NUM_FOL,
                    USUARIO,
                    FEC_OPE,
                    USUARIO,
                    ESTADO_INICIAL,
                    ESTADO_FINAL,
                    GLOSA
             FROM   (SELECT   BTCOR.FEC_ISR_BTC AS FEC_BTC,
                              BTCOR.NUM_FOL_OPE AS NUM_FOL,
                              BTCOR.FEC_ISR_OPE AS FEC_OPE,
                              BTCOR.COD_USR AS USUARIO,
                              ESTDO_ORG.DSC_EST_AOS AS ESTADO_INICIAL,
                              ESTDO_FIN.DSC_EST_AOS AS ESTADO_FINAL,
                              BTCOR.DSC_GLB_BTC AS GLOSA
                       FROM   PABS_DT_BTCOR_OPRCN BTCOR,
                              PABS_DT_ESTDO_ALMOT ESTDO_ORG,
                              PABS_DT_ESTDO_ALMOT ESTDO_FIN
                      WHERE       BTCOR.FEC_ISR_OPE = p_FEC_ISR_OPE
                              AND BTCOR.FEC_ISR_BTC > p_FEC_ISR_OPE
                              AND BTCOR.NUM_FOL_OPE = p_NUM_FOL_OPE
                              AND BTCOR.COD_EST_ICO = ESTDO_ORG.COD_EST_AOS
                              AND BTCOR.COD_EST_FIN = ESTDO_FIN.COD_EST_AOS
                     UNION
                     SELECT   BTCOR.FEC_ISR_BTC AS FEC_BTC,
                              BTCOR.NUM_FOL_OPE AS NUM_FOL,
                              BTCOR.FEC_ISR_OPE AS FEC_OPE,
                              BTCOR.COD_USR AS USUARIO,
                              ESTDO_ORG.DSC_EST_AOS AS ESTADO_INICIAL,
                              ESTDO_FIN.DSC_EST_AOS AS ESTADO_FINAL,
                              BTCOR.DSC_GLB_BTC AS GLOSA
                       FROM   PABS_DT_BTCOR_OPRCN_HTRCA BTCOR,
                              PABS_DT_ESTDO_ALMOT ESTDO_ORG,
                              PABS_DT_ESTDO_ALMOT ESTDO_FIN
                      WHERE       BTCOR.FEC_ISR_OPE = p_FEC_ISR_OPE
                              AND BTCOR.NUM_FOL_OPE = p_NUM_FOL_OPE
                              AND BTCOR.COD_EST_ICO = ESTDO_ORG.COD_EST_AOS
                              AND BTCOR.COD_EST_FIN = ESTDO_FIN.COD_EST_AOS
                              AND BTCOR.FEC_ISR_BTC > p_FEC_ISR_OPE)
         ORDER BY   FEC_BTC ASC;

      P_ERROR := PKG_PAB_CONSTANTES.V_OK; --Procedimiento ejecutado de forma correcta
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_BTCOR_OPRCN;

   --**********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_ELI_CTA_TRF
   -- Objetivo: Procedimiento que elimina cuenta corriente de las habituales de usuario en el ingreso de operacion
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_CENTA_TRNSC
   -- Fecha: 15/11/16
   -- Autor: Santander
   -- Input:
   -- p_NUM_CTA_TRF -> Numero de cuenta
   -- p_COD_BIC --> Codigo BIC de la cuenta
   -- p_FLG_DCV -> Indicado de cuenta DCV cuando es 1
   -- p_NUM_DOC_PSN -> Rut de usuario
   -- p_COD_TPD_PSN -> Dv usuario
   -- Output:
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_PAB_ELI_CTA_TRF (p_NUM_CTA_TRF   IN     CHAR,
                                 p_COD_BIC       IN     CHAR,
                                 p_FLG_DCV       IN     NUMBER,
                                 p_NUM_DOC_PSN   IN     CHAR,
                                 p_COD_TPD_PSN   IN     CHAR,
                                 p_ERROR            OUT NUMBER)
   IS
      v_NOM_SP            VARCHAR2 (30) := 'SP_PAB_ELI_CTA_TRF';
      v_NUM_CTA           NUMBER;
      v_COD_BIC           CHAR (11);
      v_FILAS_AFECTADAS   NUMBER;
   BEGIN
      --Seteamos la variable
      v_COD_BIC := p_COD_BIC;

      DELETE FROM   PABS_DT_CENTA_TRNSC CENTA
            WHERE       CENTA.NUM_CTA_TRF = p_NUM_CTA_TRF
                    AND CENTA.FLG_DCV = p_FLG_DCV
                    AND CENTA.NUM_DOC_PSN = p_NUM_DOC_PSN
                    AND CENTA.COD_TPD_PSN = p_COD_TPD_PSN
                    AND CENTA.COD_BIC_BCO =
                          NVL (v_COD_BIC, CENTA.COD_BIC_BCO);

      v_FILAS_AFECTADAS := sql%ROWCOUNT;

      IF (v_FILAS_AFECTADAS > 0)
      THEN
         p_ERROR := PKG_PAB_CONSTANTES.v_OK; --Procedimiento ejecutado de forma correcta
         COMMIT;
      ELSE
         p_ERROR := PKG_PAB_CONSTANTES.V_NO_ACTUALIZA;
      END IF;
   EXCEPTION
      WHEN DUP_VAL_ON_INDEX
      THEN
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_ELI_CTA_TRF;

   --***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_COMBO_MERCADO
   -- Objetivo: PROCEDIMIENTO ALMACENADO QUE DEVUELVE MERCADO.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: DUAL
   -- Fecha: 06/01/17
   -- Autor: Santander
   -- Input:
   -- N/A
   -- Output:
   -- p_CURSOR -> SYS_REFCURSOR, RESULTADOS DE CONSULTA.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_COMBO_MERCADO (p_CURSOR OUT SYS_REFCURSOR)
   IS
      --
      v_NOM_SP   VARCHAR2 (30) := 'Sp_PAB_COMBO_MERCADO';
   --
   BEGIN
      --
      OPEN p_CURSOR FOR
         --
         SELECT   PKG_PAB_CONSTANTES.V_FLG_MRD_MN AS COD_MERCADO,
                  'Mercado Nacional' AS GLS_MERCADO
           FROM   DUAL
         UNION
         SELECT   PKG_PAB_CONSTANTES.V_FLG_MRD_MX AS COD_MERCADO,
                  'Mercado Extranjero' AS GLS_MERCADO
           FROM   DUAL;
   --
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   --
   END Sp_PAB_COMBO_MERCADO;

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_CMB_TIP_ABO
   -- Objetivo: Procedimiento que consulta y lista las tipos de abonos.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: N/A
   -- Fecha: 07/11/16
   -- Autor: Santander
   -- Input:
   -- p_COD_MT_SWF -> Codigo SWIFT
   -- Output:
   -- p_CURSOR -> Cursor con loa tipos de abonos
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_CMB_TIP_ABO (p_COD_MT_SWF   IN     NUMBER,
                                 p_CURSOR          OUT SYS_REFCURSOR,
                                 p_ERROR           OUT NUMBER)
   IS
      -- p_COD_MT_SWF  IN NUMBER,
      --Seteo de Variables
      V_NOM_SP            VARCHAR2 (30) := 'SP_PAB_CMB_TIP_ABO';
      v_DSC_SIS_ENT_SAL   VARCHAR2 (50);
      v_AUX_SIS_ENT_SAL   VARCHAR2 (50) := 'AUX';
      v_FLG_SIS_ENT_SAL   NUMBER (1) := 2;
   BEGIN
      P_ERROR := PKG_PAB_CONSTANTES.V_OK;

      IF p_COD_MT_SWF = PKG_PAB_CONSTANTES.V_COD_MT103
      THEN
         v_AUX_SIS_ENT_SAL := 'CTACTE';
      END IF;

      OPEN p_CURSOR FOR
         SELECT   COD_SIS_ENT_SAL AS COD_TIP, DSC_SIS_ENT_SAL AS DSC_TIP
           FROM   PABS_DT_SISTM_ENTRD_SALID
          WHERE   (TRIM (COD_SIS_ENT_SAL) LIKE v_AUX_SIS_ENT_SAL
                   OR FLG_SIS_ENT_SAL = v_FLG_SIS_ENT_SAL);
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);

         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_CMB_TIP_ABO;

   --
   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_BUS_CTA_ABO
   -- Objetivo: Procedimiento almacenado que busca numero relacionado a la cuenta del tipo abono
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_CENTA_CNTBL
   -- Fecha: 07/11/16
   -- Autor: Santander
   -- Input:
   -- p_COD_SIS_ENT_SAL ->  codigo sistema entrada salida
   -- p_COD_DVI ->  Tipo de operaci?n Ingreso o Egreso
   -- Output:
   -- p_CURSOR -> Cursor con registro encontrado
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_BUS_CTA_ABO (p_COD_SIS_ENT_SAL   IN     CHAR,
                                 p_COD_DVI           IN     CHAR,
                                 p_CURSOR               OUT SYS_REFCURSOR,
                                 p_ERROR                OUT NUMBER)
   IS
      v_NOM_SP             VARCHAR2 (30) := 'SP_PAB_BUS_CTA_ABO';
      v_FLAG_MRD_UTZ_SWF   NUMBER (1);
   BEGIN
      P_ERROR := PKG_PAB_CONSTANTES.V_OK;

      IF p_COD_DVI = PKG_PAB_CONSTANTES.V_STR_DVI_CLP
      THEN
         v_FLAG_MRD_UTZ_SWF := PKG_PAB_CONSTANTES.V_FLG_MRD_MN;
      ELSE
         v_FLAG_MRD_UTZ_SWF := PKG_PAB_CONSTANTES.V_FLG_MRD_MX;
      END IF;

      OPEN p_CURSOR FOR
         SELECT   COD_SIS_ENT_SAL AS COD_SIS_ENT_SAL,
                  FLAG_MRD_UTZ_SWF AS FLAG_MRD_UTZ_SWF,
                  NUM_CTA_CTB AS NUM_CTA_CTB,
                  NUM_CRT_CTA_CTB AS NUM_CRT_CTA_CTB,
                  NUM_CNP_CTA_CTB AS NUM_CNP_CTA_CTB,
                  COD_SUC_DTN_TPN AS COD_SUC_DTN_TPN,
                  GLS_IFM_MVT_CTB AS GLS_IFM_MVT_CTB,
                  GLS_COM_CTA_CTB AS GLS_COM_CTA_CTB
           FROM   PABS_DT_CENTA_CNTBL
          WHERE       COD_SIS_ENT_SAL = p_COD_SIS_ENT_SAL
                  AND FLAG_MRD_UTZ_SWF = v_FLAG_MRD_UTZ_SWF
                  AND FLG_VGN = PKG_PAB_CONSTANTES.V_REG_VIG;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_BUS_CTA_ABO;

   --
   /********************************************************************************************************
   FUNCION/PROCEDIMIENTO: SP_PAB_COMBO_MT_SWIFT_CLP.
   OBJETIVO             : MUESTRA FILTRO EN
                           INGRESOS -> RECIBO OPERACIONES -> PROCESAR PAGOS RECIBIDOS MANUAL
                                    COMBO: MT SWIFT
                           INGRESOS -> RECIBO OPERACIONES -> CAMBIAR CANAL SALIDA
                                    COMBO: MT SWIFT
                           CONTINGENCIA -> ENVIADO -> BCCH UPLOAD - DESCARGAR ARCHIVO
                                     COMBO: MT SWIFT
   SISTEMA              : DBO_PAB.
   BASE DE DATOS        : DBO_PAB.
   TABLAS USADAS        : PABS_DT_TIPO_MT_SWIFT.
   FECHA                : 21/06/2016.
   AUTOR                : SANTANDER.
   INPUT                : NA
   OUTPUT               : P_CURSOR := DATOS CURSOR.
                          P_ERROR  := MANEJA DOS ESTADOS, 0 SIN ERRORES 1 EXISTEN ERRORES.
   OBSERVACIONES
   FECHA       USUARIO    DETALLE
   21/06/2016  SANTANDER  CREACIÓN.
   ********************************************************************************************************/
   PROCEDURE SP_PAB_COMBO_MT_SWIFT_CLP (p_CURSOR   OUT SYS_REFCURSOR,
                                        p_ERROR    OUT NUMBER)
   IS
      V_NOM_SP   VARCHAR2 (30) := 'SP_PAB_COMBO_MT_SWIFT_CLP';
   BEGIN
      P_ERROR := PKG_PAB_CONSTANTES.V_OK;

      OPEN P_CURSOR FOR
           SELECT   DISTINCT (COD_MT_SWF)
             FROM   PABS_DT_TIPO_MT_SWIFT
            WHERE       FLG_VGN = PKG_PAB_CONSTANTES.V_REG_VIG
                    AND FLG_TPO_MT = PKG_PAB_CONSTANTES.V_NUM_MT_PAGO --TIPO DE PAGO
                    AND COD_MT_SWF NOT IN (PKG_PAB_CONSTANTES.V_COD_MT200)
         ORDER BY   COD_MT_SWF;

      OPEN p_CURSOR FOR
           SELECT   DISTINCT (COD_MT_SWF)
             FROM   PABS_DT_TIPO_MT_SWIFT MT
            WHERE   MT.COD_MT_SWF IN
                          (PKG_PAB_CONSTANTES.V_COD_MT103,
                           PKG_PAB_CONSTANTES.V_COD_MT202,
                           PKG_PAB_CONSTANTES.V_COD_MT205)
         ORDER BY   COD_MT_SWF;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);

         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_COMBO_MT_SWIFT_CLP;

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_COMBO_AREA
   -- Objetivo: Procedimiento que busca Area
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_SISTM_ENTRD_SALID
   -- Fecha: 28/04/2017
   -- Autor: Santander- CAH
   -- Input:
   -- N/A
   -- Output:
   -- p_CURSOR -> Cursor con los registros de los 2 estados
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************                                    );
   PROCEDURE SP_PAB_COMBO_AREA (p_CURSOR   OUT SYS_REFCURSOR,
                                p_ERROR    OUT NUMBER)
   IS
      V_NOM_SP   VARCHAR2 (30) := 'SP_PAB_COMBO_AREA';
   BEGIN
      P_ERROR := PKG_PAB_CONSTANTES.V_OK;

      OPEN p_CURSOR FOR
           SELECT   COD_SIS_ENT_SAL AS CODIGO, DSC_SIS_ENT_SAL AS AREA
             FROM   PABS_DT_SISTM_ENTRD_SALID
            WHERE   FLG_SIS_ENT_SAL = PKG_PAB_CONSTANTES.V_FLG_SIS_ENT -- Valor 0, el cual muestra areas del Banco.
                    AND COD_SIS_ENT_SAL NOT IN
                             (PKG_PAB_CONSTANTES.V_COD_TIP_ABO_PUE, --se agregan cambios a solicitud
                              PKG_PAB_CONSTANTES.V_COD_TIP_ALC_LIQ,
                              PKG_PAB_CONSTANTES.V_COD_TIP_TECNO)
         ORDER BY   DSC_SIS_ENT_SAL;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);

         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_COMBO_AREA;

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_VLD_TXT
   -- Objetivo: Procedimiento que RETORNA EL TEXTO sin caracteres especiales
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_VLDCN_CRCTR_SWIFT
   -- Fecha: 28/04/2017
   -- Autor: SII Group
   -- Input: p_VLD_TXT_ENT = Texto a validar
   -- Output:
   -- N/A
   -- Input/Output: N/A
   -- Retorno: texto validado (V_VLD_TXT_ENT)
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   FUNCTION FN_PAB_VLD_TXT (p_VLD_TXT_ENT IN VARCHAR2)
      RETURN VARCHAR2
   IS
      V_VLD_TXT_ENT   VARCHAR2 (9000);
      v_NOM_SP        VARCHAR2 (30) := 'FN_PAB_VLD_TXT';
   BEGIN
      SELECT   UPPER(TRANSLATE (
                        LOWER (p_VLD_TXT_ENT),
                        (SELECT   LISTAGG (VAL_CHA_ORG, '')
                                     WITHIN GROUP (ORDER BY VAL_CHA_ORG)
                           FROM   PABS_DT_VLDCN_CRCTR_SWIFT),
                        (SELECT   LISTAGG (VAL_CHA_CVS, '')
                                     WITHIN GROUP (ORDER BY VAL_CHA_ORG)
                           FROM   PABS_DT_VLDCN_CRCTR_SWIFT)
                     ))
        INTO   V_VLD_TXT_ENT
        FROM   DUAL;

      RETURN V_VLD_TXT_ENT;
   EXCEPTION
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END FN_PAB_VLD_TXT;



   -- ***********************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_DEV_DIA_HABIL_ANT
   -- Objetivo: Procedimiento que devuelve el día habil anterior desde la fecha de entrada
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: N/A
   -- Fecha: 06-09-2017
   -- Autor: Santander- GCP
   -- Input:
   -- p_calendario -> Calendario a validar
   -- Output:
   -- Input/Output: N/A
   -- p_fecha -> Fecha que se valida
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   FUNCTION FN_PAB_DEV_DIA_HABIL_ANT (p_calendario IN CHAR, p_fecha IN DATE)
      RETURN NUMBER
   IS
      V_NOM_SP      VARCHAR2 (30) := 'FN_PAB_DEV_DIA_HABIL_ANT';
      v_COD_PAIS    CHAR (2);
      v_IND_HABIL   NUMBER (1) := 0;                 /*Habil = 1 Inhabil = 0*/
      v_loop        NUMBER (1) := 0;
      v_count       NUMBER := 0;
      v_ERRORCODE   NUMBER;
      v_ERRORMSG    VARCHAR2 (300);
      v_fecha       NUMERIC;
      v_fecha2      DATE;
      --  v_fecha2 DATE;
      err_GBO exception;
   BEGIN
      v_COD_PAIS := p_calendario;
      v_fecha2 := p_fecha;

      --DBMS_OUTPUT.PUT_LINE('Codigo Pais:' || v_COD_PAIS || ' Fecha Inicial:' || p_fecha);
      --  v_fecha2 := to_char(to_date(p_fecha,'yyyymmdd'),'mm-dd-yyyy');
      WHILE v_IND_HABIL = 0
      LOOP
         --Validamos si la fecha es habil
         PKG_CHI_CON_AOS.SP_CHI_CON_DHB_AOS (v_fecha2,
                                             v_COD_PAIS,
                                             v_IND_HABIL,
                                             v_ERRORCODE,
                                             v_ERRORMSG);


         --_fecha := p_fecha;
         IF (v_ERRORCODE = 1)
         THEN
            RAISE err_GBO;
         END IF;

         -- Si es habil salimos si no buscamos el siguiente
         IF (v_IND_HABIL = 1)
         THEN
            DBMS_OUTPUT.PUT_LINE (
               'Es habil.. salimos del loop -> ' || v_fecha2
            );
         ELSE
            --DBMS_OUTPUT.PUT_LINE('No es habil.. -> ' || p_fecha);
            v_fecha2 := v_fecha2 - 1;
         --DBMS_OUTPUT.PUT_LINE('Aumentamops fecha.. -> ' || p_fecha);
         END IF;
      END LOOP;

      v_fecha := TO_NUMBER (TO_CHAR (v_fecha2, 'RRRRMMDD'));
      RETURN v_fecha;
   EXCEPTION
      WHEN err_GBO
      THEN
         err_code := v_ERRORCODE;
         err_msg := v_ERRORMSG;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);

         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END FN_PAB_DEV_DIA_HABIL_ANT;



   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_COMBO_EST_DEB_PAT
   -- Objetivo: Procedimiento que consulta estados Por Abonar o Por Asociar
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_ESTDO_ALMOT
   -- Fecha: 27/10/16
   -- Autor: Santander
   -- Input:
   -- N/A
   -- Output:
   -- p_CURSOR -> Cursor con los registros de los 2 estados
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_COMBO_EST_DEB_PAT (p_CURSOR   OUT SYS_REFCURSOR,
                                       p_ERROR    OUT NUMBER)
   IS
      --Seteo de Variables
      V_NOM_SP   VARCHAR2 (30) := 'SP_PAB_COMBO_EST_DEB_PAT';
   BEGIN
      P_ERROR := PKG_PAB_CONSTANTES.V_OK;

      OPEN p_CURSOR FOR
           SELECT   COD_EST_AOS AS CODIGO, DSC_EST_AOS AS DESCRIPCION
             FROM   PABS_DT_ESTDO_ALMOT
            WHERE   COD_EST_AOS IN
                          (PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPDEB,
                           PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAT)
         ORDER BY   1 ASC;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);

         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_COMBO_EST_DEB_PAT;

   /*******************************************************************************************************
    FUNCION/PROCEDIMIENTO: SP_PAB_COMBO_CANAL_ENT_TODO.
    OBJETIVO             : MUESTRA FILTRO EN
                            CONSULTAS -> SWIFT -> OPERACIONES
                                      COMBO: CANAL DE ENTRADA.
    SISTEMA              : DBO_PAB.
    BASE DE DATOS        : DBO_PAB.
    TABLAS USADAS        : PABS_DT_SISTM_ENTRD_SALID.
    FECHA                : 20/09/2018.
    AUTOR                : VARAYA.
    INPUT                : NA.
    OUTPUT               : P_CURSOR := DATOS CURSOR.
    OBSERVACIONES
    FECHA       USUARIO    DETALLE
    20/09/2018  VARAYA     CREACIÓN.
    *******************************************************************************************************/
   PROCEDURE Sp_PAB_COMBO_CANAL_ENT_TODO (P_CURSOR OUT SYS_REFCURSOR)
   IS
      ---------------------------------------DECLARACIÓN DE VARIABLES Y CONSTANTES----------------------------
      V_NOM_SP   VARCHAR2 (30) := 'SP_PAB_COMBO_CANAL_ENT_TODO';
   --------------------------------------------------------------------------------------------------------
   BEGIN
      OPEN P_CURSOR FOR
           SELECT   COD_SIS_ENT_SAL, DSC_SIS_ENT_SAL
             FROM   PABS_DT_SISTM_ENTRD_SALID EN
            WHERE   EN.FLG_SIS_ENT_SAL IN
                          (PKG_PAB_CONSTANTES.V_FLG_SIS_ENT,
                           PKG_PAB_CONSTANTES.V_FLG_SIS_SAL)
                    AND EN.COD_SIS_ENT_SAL NOT IN
                             (PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_CTACTE,
                              PKG_PAB_CONSTANTES.V_COD_TIP_CTACTELBTR)
         ORDER BY   DSC_SIS_ENT_SAL;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         ERR_CODE := SQLCODE;
         ERR_MSG := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (ERR_CODE,
                                             V_NOM_PCK,
                                             ERR_MSG,
                                             V_NOM_SP);
      WHEN OTHERS
      THEN
         ERR_CODE := SQLCODE;
         ERR_MSG := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (ERR_CODE,
                                             V_NOM_PCK,
                                             ERR_MSG,
                                             V_NOM_SP);
         P_S_MENSAJE := ERR_CODE || '-' || ERR_MSG;
         RAISE_APPLICATION_ERROR (-20000, P_S_MENSAJE);
   END SP_PAB_COMBO_CANAL_ENT_TODO;

   /********************************************************************************************************
   FUNCION/PROCEDIMIENTO: SP_PAB_COMBO_CANAL_SAL_TODO.
   OBJETIVO             : MUESTRA FILTRO EN
                           CONSULTAS -> SWIFT -> OPERACIONES.
                                     COMBO: CANAL DE SALIDA.
   SISTEMA              : DBO_PAB.
   BASE DE DATOS        : DBO_PAB.
   TABLAS USADAS        : PABS_DT_SISTM_ENTRD_SALID.
   FECHA                : 20/09/2018.
   AUTOR                : VARAYA.
   OUTPUT               : P_CURSOR := DATOS CURSOR.
   OBSERVACIONES
   FECHA       USUARIO    DETALLE
   20-09-2018  VARAYA     CREACIÓN.
   ********************************************************************************************************/
   PROCEDURE SP_PAB_COMBO_CANAL_SAL_TODO (P_CURSOR OUT SYS_REFCURSOR)
   IS
      ---------------------------------------DECLARACIÓN DE VARIABLES Y CONSTANTES----------------------------
      V_NOM_SP   VARCHAR2 (30) := 'SP_PAB_COMBO_CANAL_SAL_TODO';
   --------------------------------------------------------------------------------------------------------
   BEGIN
      OPEN P_CURSOR FOR
           SELECT   COD_SIS_ENT_SAL, DSC_SIS_ENT_SAL
             FROM   PABS_DT_SISTM_ENTRD_SALID EN
            WHERE   EN.FLG_SIS_ENT_SAL IN
                          (PKG_PAB_CONSTANTES.V_FLG_SIS_ENT,               --0
                           PKG_PAB_CONSTANTES.V_FLG_SIS_SAL,               --1
                           PKG_PAB_CONSTANTES.V_FLG_SIS_ABN)               --2
                    AND EN.COD_SIS_ENT_SAL NOT IN
                             (PKG_PAB_CONSTANTES.V_COD_TIP_BANPRI,
                              PKG_PAB_CONSTANTES.V_COD_TIP_CORRBOL,
                              PKG_PAB_CONSTANTES.V_COD_TIP_CORRSEG,
                              PKG_PAB_CONSTANTES.V_COD_TIP_GTB,
                              PKG_PAB_CONSTANTES.V_COD_TIP_OB,
                              PKG_PAB_CONSTANTES.V_COD_TIP_LEAFAC,
                              PKG_PAB_CONSTANTES.V_COD_TIP_CAJA,
                              PKG_PAB_CONSTANTES.V_COD_TIP_CTACTELBTR,
                              PKG_PAB_CONSTANTES.V_COD_TIP_ABO_PUE)
         ORDER BY   DSC_SIS_ENT_SAL;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         ERR_CODE := SQLCODE;
         ERR_MSG := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (ERR_CODE,
                                             V_NOM_PCK,
                                             ERR_MSG,
                                             V_NOM_SP);
      WHEN OTHERS
      THEN
         ERR_CODE := SQLCODE;
         ERR_MSG := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (ERR_CODE,
                                             V_NOM_PCK,
                                             ERR_MSG,
                                             V_NOM_SP);
         P_S_MENSAJE := ERR_CODE || '-' || ERR_MSG;
         RAISE_APPLICATION_ERROR (-20000, P_S_MENSAJE);
   END SP_PAB_COMBO_CANAL_SAL_TODO;

   /********************************************************************************************************
   FUNCION/PROCEDIMIENTO: PKG_PAB_UTILITY.SP_PAB_COMBO_CANAL_ENT_INT.
   OBJETIVO             : MUESTRA FILTRO EN COMBO:
                           CONSULTAS -> INTERNAS -> OPERACIONES.
   SISTEMA              : DBO_PAB.
   BASE DE DATOS        : DBO_PAB.
   TABLAS USADAS        : PABS_DT_SISTM_ENTRD_SALID.
   FECHA                : 20/09/2018.
   AUTOR                : VARAYA.
   OUTPUT               : P_CURSOR := DATOS CURSOR.
   OBSERVACIONES
   FECHA       USUARIO    DETALLE
   20-09-2018  VARAYA     CREACIÓN.
   ********************************************************************************************************/
   PROCEDURE SP_PAB_COMBO_CANAL_ENT_INT (P_CURSOR OUT SYS_REFCURSOR)
   IS
      ---------------------------------------DECLARACIÓN DE VARIABLES Y CONSTANTES----------------------------
      V_NOM_SP            VARCHAR2 (30) := 'SP_PAB_COMBO_CANAL_ENT_INT';
      V_FLG_SIS_ENT_SAL   NUMBER := NULL;
   --------------------------------------------------------------------------------------------------------
   BEGIN
      OPEN P_CURSOR FOR
           SELECT   COD_SIS_ENT_SAL, DSC_SIS_ENT_SAL
             FROM   PABS_DT_SISTM_ENTRD_SALID
            WHERE   FLG_SIS_ENT_SAL = NVL (V_FLG_SIS_ENT_SAL, FLG_SIS_ENT_SAL)
                    AND COD_SIS_ENT_SAL IN
                             (PKG_PAB_CONSTANTES.V_COD_TIP_ABOMAS, --ABONOS MASIVOS
                              PKG_PAB_CONSTANTES.V_COD_TIP_BANPRI, --BANCA PRIVADA
                              PKG_PAB_CONSTANTES.V_COD_TIP_CANJE,      --CANJE
                              PKG_PAB_CONSTANTES.V_COD_TIP_CORRBOL, --CORREDORA DE BOLSA
                              PKG_PAB_CONSTANTES.V_COD_TIP_CORRSEG, --CORREDORA DE SEGURO
                              PKG_PAB_CONSTANTES.V_COD_TIP_CUSTODIA, --CUSTODIA
                              PKG_PAB_CONSTANTES.V_COD_TIP_FONMUT, --FONDOS MUTUOS
                              PKG_PAB_CONSTANTES.V_COD_TIP_GTB, --GERENCIA BANCA TRANSACCIONAL
                              PKG_PAB_CONSTANTES.V_COD_TIP_HIPOTE, --HIPOTECARIOS
                              PKG_PAB_CONSTANTES.V_COD_TIP_OB,          --OOBB
                              PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SE_OOFF, --OOFF
                              PKG_PAB_CONSTANTES.V_COD_TIP_CREDIT, --PROCESOS ACTIVOS
                              PKG_PAB_CONSTANTES.V_COD_TIP_TESCEN) --TESORERIA CENTRAL
         ORDER BY   DSC_SIS_ENT_SAL;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         ERR_CODE := SQLCODE;
         ERR_MSG := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (ERR_CODE,
                                             V_NOM_PCK,
                                             ERR_MSG,
                                             V_NOM_SP);
      WHEN OTHERS
      THEN
         ERR_CODE := SQLCODE;
         ERR_MSG := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (ERR_CODE,
                                             V_NOM_PCK,
                                             ERR_MSG,
                                             V_NOM_SP);
         P_S_MENSAJE := ERR_CODE || '-' || ERR_MSG;
         RAISE_APPLICATION_ERROR (-20000, P_S_MENSAJE);
   END SP_PAB_COMBO_CANAL_ENT_INT;

   /********************************************************************************************************
   FUNCION/PROCEDIMIENTO: PKG_PAB_UTILITY.SP_PAB_COMBO_CANAL_SAL_INT.
   OBJETIVO             : MUESTRA FILTRO EN COMBO:
                           CONSULTAS -> INTERNAS -> OPERACIONES.
   SISTEMA              : DBO_PAB.
   BASE DE DATOS        : DBO_PAB.
   TABLAS USADAS        : PABS_DT_SISTM_ENTRD_SALID.
   FECHA                : 20/09/2018.
   AUTOR                : VARAYA.
   OUTPUT               : P_CURSOR := DATOS CURSOR.
   OBSERVACIONES
   FECHA       USUARIO    DETALLE
   20-09-2018  VARAYA     CREACIÓN.
   ********************************************************************************************************/
   PROCEDURE SP_PAB_COMBO_CANAL_SAL_INT (P_CURSOR OUT SYS_REFCURSOR)
   IS
      ---------------------------------------DECLARACIÓN DE VARIABLES Y CONSTANTES----------------------------
      V_NOM_SP            VARCHAR2 (30) := 'SP_PAB_COMBO_CANAL_SAL_INT';
      V_FLG_SIS_ENT_SAL   NUMBER := NULL;
   --------------------------------------------------------------------------------------------------------
   BEGIN
      OPEN P_CURSOR FOR
           SELECT   COD_SIS_ENT_SAL, DSC_SIS_ENT_SAL
             FROM   PABS_DT_SISTM_ENTRD_SALID
            WHERE   FLG_SIS_ENT_SAL = NVL (V_FLG_SIS_ENT_SAL, FLG_SIS_ENT_SAL)
                    AND COD_SIS_ENT_SAL =
                          PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_CTACTE
         ORDER BY   DSC_SIS_ENT_SAL;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         ERR_CODE := SQLCODE;
         ERR_MSG := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (ERR_CODE,
                                             V_NOM_PCK,
                                             ERR_MSG,
                                             V_NOM_SP);
      WHEN OTHERS
      THEN
         ERR_CODE := SQLCODE;
         ERR_MSG := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (ERR_CODE,
                                             V_NOM_PCK,
                                             ERR_MSG,
                                             V_NOM_SP);
         P_S_MENSAJE := ERR_CODE || '-' || ERR_MSG;
         RAISE_APPLICATION_ERROR (-20000, P_S_MENSAJE);
   END SP_PAB_COMBO_CANAL_SAL_INT;

   /********************************************************************************************************
   FUNCION/PROCEDIMIENTO: SP_PAB_COMBO_MT_INT.
   OBJETIVO             : MUESTRA FILTRO EN
                           CONTINGENCIA -> ENVIADO -> BCCH UPLOAD - DESCARGAR ARCHIVO.
                           CONTINGENCIA -> ENVIADO -> COMBANC UPLOAD - DESCARGAR ARCHIVO.
   SISTEMA              : DBO_PAB.
   BASE DE DATOS        : DBO_PAB.
   TABLAS USADAS        : PABS_DT_TIPO_MT_SWIFT.
   FECHA                : 20/09/2018.
   AUTOR                : VARAYA.
   INPUT                : P_CANAL  := CANAL.
   OUTPUT               : P_CURSOR := DATOS CURSOR.
   OBSERVACIONES
   FECHA       USUARIO    DETALLE
   20-09-2018  VARAYA     CREACIÓN.
   ********************************************************************************************************/
   PROCEDURE SP_PAB_COMBO_MT_INT (p_CURSOR OUT SYS_REFCURSOR)
   IS
      ---------------------------------------DECLARACIÓN DE VARIABLES Y CONSTANTES----------------------------
      V_NOM_SP   VARCHAR2 (30) := 'SP_PAB_COMBO_MT_INT';
   --------------------------------------------------------------------------------------------------------
   BEGIN
      OPEN P_CURSOR FOR
         SELECT   COD_MT_SWF
           FROM   PABS_DT_TIPO_MT_SWIFT
          WHERE   COD_MT_SWF = PKG_PAB_CONSTANTES.V_COD_MT103 AND ROWNUM = 1;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         ERR_CODE := SQLCODE;
         ERR_MSG := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (ERR_CODE,
                                             V_NOM_PCK,
                                             ERR_MSG,
                                             V_NOM_SP);
      WHEN OTHERS
      THEN
         ERR_CODE := SQLCODE;
         ERR_MSG := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (ERR_CODE,
                                             V_NOM_PCK,
                                             ERR_MSG,
                                             V_NOM_SP);
         P_S_MENSAJE := ERR_CODE || '-' || ERR_MSG;
         RAISE_APPLICATION_ERROR (-20000, P_S_MENSAJE);
   END SP_PAB_COMBO_MT_INT;

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_COMBO_EGR_ING
   -- Objetivo: Procedimiento que devuelve tipo de Egreso
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_SISTM_ENTRD_SALID
   -- Fecha: 05/06/17
   -- Autor: Santander
   -- Input:
   -- Output:
   -- p_CURSOR   -> cursor
   -- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_COMBO_EGR_ING (p_CURSOR   OUT SYS_REFCURSOR,
                                   p_ERROR    OUT NUMBER)
   IS
      v_NOM_SP   VARCHAR2 (100) := 'Sp_PAB_COMBO_EGR_ING ';
   BEGIN
      P_ERROR := PKG_PAB_CONSTANTES.V_OK;

      OPEN p_CURSOR FOR
         SELECT   PKG_PAB_CONSTANTES.V_STR_FLG_EGR AS DESCRIPCION,
                  PKG_PAB_CONSTANTES.V_FLG_EGR AS COD_EGR
           FROM   DUAL
         UNION
         SELECT   PKG_PAB_CONSTANTES.V_STR_FLG_ING AS DESCRIPCION,
                  PKG_PAB_CONSTANTES.V_FLG_ING AS COD_EGR
           FROM   DUAL;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);

         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_COMBO_EGR_ING;

   --***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_COMBO_ESTADO_MSJ
   -- Objetivo: Consulta los tipos de estados de mensajes de protocolo
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_ESTDO_ALMOT
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- N/A
   -- Output:
   -- p_CURSOR -> Cursor que devuelve los estados de los mensajes de protocolo
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones:
   --***********************************************************************************************
   PROCEDURE SP_PAB_COMBO_ESTADO_MSJ (p_CURSOR OUT SYS_REFCURSOR)
   IS
      v_NOM_SP   VARCHAR2 (30) := 'SP_PAB_COMBO_ESTADO_MSJ';
   BEGIN
      OPEN p_CURSOR FOR
           SELECT   COD_EST_AOS CODIGO, DSC_EST_AOS GLOSA
             FROM   PABS_DT_ESTDO_ALMOT
            WHERE   COD_EST_AOS IN
                          (PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEVAL,
                           PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEELI,
                           PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPECUR,
                           PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEREC,
                           PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT,
                           PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAS,
                           PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEASO,
                           PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPENDE)
                    AND FLG_VGN = PKG_PAB_CONSTANTES.V_REG_VIG
         ORDER BY   DSC_EST_AOS;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_COMBO_ESTADO_MSJ;

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_COMBO_REC_ENV
   -- Objetivo: Procedimiento que devuelve tipo de Egreso
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: DUAL
   -- Fecha: 05/06/17
   -- Autor: Santander
   -- Input: N/A
   -- Output:
   -- p_CURSOR  -> Cursor que contiene los valores utilizados para el combo de flujo operacion (Enviado / Recibido)
   -- p_ERROR-> Indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones:
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_COMBO_REC_ENV (p_CURSOR   OUT SYS_REFCURSOR,
                                   p_ERROR    OUT NUMBER)
   IS
      v_NOM_SP   VARCHAR2 (100) := 'Sp_PAB_COMBO_REC_ENV ';
   BEGIN
      P_ERROR := PKG_PAB_CONSTANTES.V_OK;

      OPEN p_CURSOR FOR
         SELECT   PKG_PAB_CONSTANTES.V_STR_FLG_REC AS DESCRIPCION,
                  PKG_PAB_CONSTANTES.V_FLG_REC AS COD_EGR
           FROM   DUAL
         UNION
         SELECT   PKG_PAB_CONSTANTES.V_STR_FLG_ENV AS DESCRIPCION,
                  PKG_PAB_CONSTANTES.V_FLG_ENV AS COD_EGR
           FROM   DUAL;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);

         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_COMBO_REC_ENV;

   /************************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_GEN_FOL_AM
   -- Objetivo: Funcion que retorna el folio solicitado segun la variable de entrada
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: N/A
   -- Fecha: 05/06/2017
   -- Autor: GCP
   -- Input:
   -- P_STR_FOL: Valor de la secuencia solicitada ej (PEAM: Egreso Altos Montos)
   -- Output:
   -- N/A
   -- Input/Output: N/A
   -- Retorno: Retorna el folio solicitado
   -- Observaciones:
   --***********************************************************************************************/
   FUNCTION FN_PAB_GEN_FOL_AM (P_STR_FOL CHAR)
      RETURN NUMBER
   IS
      v_NOM_SP   VARCHAR2 (30) := 'FN_PAB_GEN_FOL_AM';
      v_fol_am   NUMBER;
      ERR_COD_NOEXI EXCEPTION;
   BEGIN
      --Rescatamos el folio segun la variable de envio
      CASE
         WHEN P_STR_FOL = 'PEAM'
         THEN
            --PAGO EGRESO ALTOS MONTOS
            v_fol_am := SPK_PAB_PAG_EAM.NEXTVAL;
         WHEN P_STR_FOL = 'PIAM'
         THEN
            --PAGO INGRESO ALTOS MONTOS
            v_fol_am := SPK_PAB_PAG_IAM.NEXTVAL;
         WHEN P_STR_FOL = 'MEAM'
         THEN
            --MENSAJERIA EGRESO ALTOS MONTOS
            v_fol_am := SPK_PAB_MSJ_EAM.NEXTVAL;
         WHEN P_STR_FOL = 'MIAM'
         THEN
            --MENSAJERIA INGRESO ALTOS MONTOS
            v_fol_am := SPK_PAB_MSJ_IAM.NEXTVAL;
         /* WHEN P_STR_FOL = 'NOAM' THEN
            --NOMINA ALTOS MONTOS
            v_fol_am := SPK_PAB_NOM_AM.nextval;*/
         WHEN (P_STR_FOL = 'NOAM' OR P_STR_FOL = 'NIAM')
         THEN
            --NOMINA ALTOS MONTOS O NOMINA INTERNA ALTO MONTO
            v_fol_am := SPK_PAB_NOM_AM.NEXTVAL;
         WHEN P_STR_FOL = 'CJAM'
         THEN
            --NOMINA CAJA ALTOS MONTOS
            v_fol_am := SPK_PAB_NOM_CJA_AM.NEXTVAL;
         ELSE
            RAISE ERR_COD_NOEXI;
      END CASE;

      RETURN v_fol_am;
   EXCEPTION
      WHEN ERR_COD_NOEXI
      THEN
         err_msg := 'No existe el codigo: ' || P_STR_FOL;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END FN_PAB_GEN_FOL_AM;

   /**********************************************************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_INS_MVNTO_CARGO_ABONO
   -- Objetivo: Procedimiento almacenado que inserta los movimientos de abono y cargo de Cuentas corrientes en la tabla de resgistro Abo/car
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_RGTRO_OPRCN_CARGO_ABON
   --
   -- Fecha: 01/09/2017
   -- Autor: Santander CAH
   -- Input:
   --  p_NUM_MOV_CTB   Movimiento contable
   --  p_NUM_FOL_OPE   Numero de operación
   --  p_FEC_ISR_OPE   Fecha de inserción de la operación
   --  p_FLG_CGO_ABN   Flag indicador de Abono o Cargo
   -- Output:
   --  p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --************************************************************************************************************************************************/
   PROCEDURE Sp_PAB_INS_MVNTO_CARGO_ABONO (p_NUM_MOV_CTB   IN     CHAR,
                                           p_NUM_FOL_OPE   IN     NUMBER,
                                           p_FEC_ISR_OPE   IN     DATE,
                                           p_FLG_CGO_ABN   IN     NUMBER,
                                           p_ERROR            OUT NUMBER)
   IS
      v_NOM_SP        VARCHAR2 (30) := 'Sp_PAB_INS_MVNTO_CARGO_ABONO';
      v_FLG_CGO_ABN   NUMBER (1);
   BEGIN
      --Segun Movimiento ABN o CGO
      IF p_FLG_CGO_ABN = PKG_PAB_CONSTANTES.V_FLG_MOV_ABN
      THEN
         --ABN = 1
         v_FLG_CGO_ABN := 1;
      ELSE
         v_FLG_CGO_ABN := 0;
      END IF;

      INSERT INTO PABS_DT_RGTRO_OPRCN_CARGO_ABON (NUM_MOV_CTB,
                                                  NUM_FOL_OPE,
                                                  FEC_ISR_OPE,
                                                  FLG_CGO_ABN,
                                                  FEC_CGO_ABN,
                                                  HRA_CGO_ABN)
        VALUES   (p_NUM_MOV_CTB,
                  p_NUM_FOL_OPE,
                  p_FEC_ISR_OPE,
                  v_FLG_CGO_ABN,
                  TRUNC (SYSDATE),
                  TO_CHAR (SYSDATE, 'HH24MI'));

      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   EXCEPTION
      WHEN DUP_VAL_ON_INDEX
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);

         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_INS_MVNTO_CARGO_ABONO;

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_COMBO_TPO_MT_CRTLA
   -- Objetivo: Procedimiento que consulta Por MT940 y MT950
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_TIPO_MT_SWIFT
   -- Fecha: 06-09-2017
   -- Autor: Santander- CAH
   -- Input:
   -- N/A
   -- Output:
   -- p_CURSOR -> Cursor con los registros de los 2 estados
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_COMBO_TPO_MT_CRTLA (p_CURSOR   OUT SYS_REFCURSOR,
                                        p_ERROR    OUT NUMBER)
   IS
      V_NOM_SP   VARCHAR2 (30) := 'SP_PAB_COMBO_TPO_MT_CRTLA';
   BEGIN
      OPEN p_CURSOR FOR
           SELECT   DISTINCT (COD_MT_SWF)
             FROM   PABS_DT_TIPO_MT_SWIFT MT
            WHERE   MT.COD_MT_SWF IN
                          (PKG_PAB_CONSTANTES.V_COD_MT940,
                           PKG_PAB_CONSTANTES.V_COD_MT950)
                    AND FLG_VGN = PKG_PAB_CONSTANTES.v_REG_VIG
         ORDER BY   COD_MT_SWF;

      P_ERROR := PKG_PAB_CONSTANTES.V_OK;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);

         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_COMBO_TPO_MT_CRTLA;

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_DEV_DIA_HABIL_SIG
   -- Objetivo: Procedimiento que devuelve el día habil desde la fecha de entrada
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: N/A
   -- Fecha: 06-09-2017
   -- Autor: Santander- GCP
   -- Input:
   -- p_calendario -> Calendario a validar
   -- Output:
   -- Input/Output: N/A
   -- p_fecha -> Fecha que se valida
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_DEV_DIA_HABIL_SIG (p_calendario   IN     CHAR,
                                       p_fecha        IN OUT DATE)
   AS
      V_NOM_SP      VARCHAR2 (30) := 'SP_PAB_DEV_DIA_HABIL_SIG';
      v_COD_PAIS    CHAR (2);
      v_IND_HABIL   NUMBER (1) := 0;                 /*Habil = 1 Inhabil = 0*/
      v_loop        NUMBER (1) := 0;
      v_count       NUMBER := 0;
      v_ERRORCODE   NUMBER;
      v_ERRORMSG    VARCHAR2 (300);
      err_GBO exception;
   BEGIN
      v_COD_PAIS := p_calendario;

      --DBMS_OUTPUT.PUT_LINE('Codigo Pais:' || v_COD_PAIS || ' Fecha Inicial:' || p_fecha);

      WHILE v_IND_HABIL = 0
      LOOP
         --Validamos si la fecha es habil
         PKG_CHI_CON_AOS.SP_CHI_CON_DHB_AOS (p_fecha,
                                             v_COD_PAIS,
                                             v_IND_HABIL,
                                             v_ERRORCODE,
                                             v_ERRORMSG);

         IF (v_ERRORCODE = 1)
         THEN
            RAISE err_GBO;
         END IF;

         -- Si es habil salimos si no buscamos el siguiente
         IF (v_IND_HABIL = 1)
         THEN
            DBMS_OUTPUT.PUT_LINE (
               'Es habil.. salimos del loop -> ' || p_fecha
            );
         ELSE
            --DBMS_OUTPUT.PUT_LINE('No es habil.. -> ' || p_fecha);
            p_fecha := p_fecha + 1;
         --DBMS_OUTPUT.PUT_LINE('Aumentamops fecha.. -> ' || p_fecha);
         END IF;
      END LOOP;
   EXCEPTION
      WHEN err_GBO
      THEN
         err_code := v_ERRORCODE;
         err_msg := v_ERRORMSG;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);

         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END;

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_DEV_DIA_HABIL_ANT
   -- Objetivo: Procedimiento que devuelve el día habil anterior desde la fecha de entrada
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: N/A
   -- Fecha: 06-09-2017
   -- Autor: Santander- GCP
   -- Input:
   -- p_calendario -> Calendario a validar
   -- Output:
   -- Input/Output: N/A
   -- p_fecha -> Fecha que se valida
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_DEV_DIA_HABIL_ANT (p_calendario   IN     CHAR,
                                       p_fecha        IN OUT DATE)
   AS
      V_NOM_SP      VARCHAR2 (30) := 'SP_PAB_DEV_DIA_HABIL_ANT';
      v_COD_PAIS    CHAR (2);
      v_IND_HABIL   NUMBER (1) := 0;                 /*Habil = 1 Inhabil = 0*/
      v_loop        NUMBER (1) := 0;
      v_count       NUMBER := 0;
      v_ERRORCODE   NUMBER;
      v_ERRORMSG    VARCHAR2 (300);
      err_GBO exception;
   BEGIN
      v_COD_PAIS := p_calendario;


      --DBMS_OUTPUT.PUT_LINE('Codigo Pais:' || v_COD_PAIS || ' Fecha Inicial:' || p_fecha);

      WHILE v_IND_HABIL = 0
      LOOP
         --Validamos si la fecha es habil
         PKG_CHI_CON_AOS.SP_CHI_CON_DHB_AOS (p_fecha,
                                             v_COD_PAIS,
                                             v_IND_HABIL,
                                             v_ERRORCODE,
                                             v_ERRORMSG);

         IF (v_ERRORCODE = 1)
         THEN
            RAISE err_GBO;
         END IF;

         -- Si es habil salimos si no buscamos el siguiente
         IF (v_IND_HABIL = 1)
         THEN
            DBMS_OUTPUT.PUT_LINE (
               'Es habil.. salimos del loop -> ' || p_fecha
            );
         ELSE
            --DBMS_OUTPUT.PUT_LINE('No es habil.. -> ' || p_fecha);
            p_fecha := p_fecha - 1;
         --DBMS_OUTPUT.PUT_LINE('Aumentamops fecha.. -> ' || p_fecha);
         END IF;
      END LOOP;
   EXCEPTION
      WHEN err_GBO
      THEN
         err_code := v_ERRORCODE;
         err_msg := v_ERRORMSG;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);

         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END;

   --***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_COMBO_PERFIL
   -- Objetivo: Procedimiento que devuelve los perfiles por Áarea ingresada
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_TIPO_ROL_ALMOT
   --              PABS_DT_RELCN_ROL_MENU

   -- Fecha:03-01-2018
   -- Autor: Santander CAH
   -- Input:
   -- p_COD_SIS_ENT ->cod sistema de entrada
   -- Output:
   -- p_CURSOR -> SYS_REFCURSOR, RESULTADOS DE CONSULTA.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_COMBO_PERFIL (p_COD_SIS_ENT   IN     CHAR,
                                  p_CURSOR           OUT SYS_REFCURSOR)
   IS
      --
      v_NOM_SP            VARCHAR2 (30) := 'Sp_PAB_COMBO_PERFIL';
      V_FLG_SIS_ENT_SAL   NUMBER := NULL;
   --
   BEGIN
      OPEN p_CURSOR FOR
         SELECT   DISTINCT
                  (ROL.NOM_TPO_ROL_AOS) AS PERFIL,
                  ROL.COD_TPO_ROL_AOS AS COD_PERFIL
           FROM   PABS_DT_TIPO_ROL_ALMOT ROL, PABS_DT_USRIO US
          WHERE   US.COD_TPO_ROL_AOS = ROL.COD_TPO_ROL_AOS
                  AND US.COD_SIS_ENT = p_COD_SIS_ENT;
   --
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_COMBO_PERFIL;

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_COMBO_MT_SWF_ABONO
   -- Objetivo: Procedimiento que consulta Por MT103 O MT202
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_TIPO_MT_SWIFT
   -- Fecha:05-01-2018
   -- Autor: Santander- CAH
   -- Input:
   -- N/A
   -- Output:
   -- p_CURSOR -> Cursor con los registros de los 2 estados
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_COMBO_MT_SWF_ABONO (p_CURSOR   OUT SYS_REFCURSOR,
                                        p_ERROR    OUT NUMBER)
   IS
      V_NOM_SP   VARCHAR2 (30) := 'SP_PAB_COMBO_MT_SWF_ABONO';
   BEGIN
      P_ERROR := PKG_PAB_CONSTANTES.V_OK;

      OPEN p_CURSOR FOR
           SELECT   DISTINCT (COD_MT_SWF)
             FROM   PABS_DT_TIPO_MT_SWIFT MT
            WHERE   MT.COD_MT_SWF IN
                          (PKG_PAB_CONSTANTES.V_COD_MT103,
                           PKG_PAB_CONSTANTES.V_COD_MT202)
         ORDER BY   COD_MT_SWF;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);

         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_COMBO_MT_SWF_ABONO;

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_COMBO_BCO_PAIS
   -- Objetivo: Procedimiento que retorna los banco por pais
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_TIPO_MT_SWIFT
   -- Fecha:05-01-2018
   -- Autor: Santander- CAH
   -- Input:
   --  p_PAIS --> Pais para buscar, código
   -- Output:
   -- p_CURSOR -> Cursor con los registros de los 2 estados
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_COMBO_BCO_PAIS (p_PAIS     IN     VARCHAR2,
                                    p_CURSOR      OUT SYS_REFCURSOR,
                                    p_ERROR       OUT NUMBER)
   IS
      V_NOM_SP   VARCHAR2 (30) := 'SP_PAB_COMBO_BCO_PAIS';
   BEGIN
      P_ERROR := PKG_PAB_CONSTANTES.V_OK;

      OPEN p_CURSOR FOR
           SELECT   BCO.COD_BIC_BCO
             FROM   PABS_DT_BANCO_ALMOT BCO, TCDTBAI TBAI, TGEN_0112 PAIS
            WHERE       BCO.COD_BIC_BCO = TBAI.TGCDSWSA
                    AND BCO.FLG_VGN = PKG_PAB_CONSTANTES.V_REG_VIG
                    AND PAIS.CODEPAIS = TBAI.COD_PAIS
                    AND PAIS.TCCPAIS4 = p_PAIS                    --'CL' Chile
         ORDER BY   BCO.COD_BIC_BCO;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);

         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_COMBO_BCO_PAIS;

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_COMBO_BCO_MERCADO
   -- Objetivo: Procedimiento que retorna los banco segun mercado
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_TIPO_MT_SWIFT
   -- Fecha:08-01-2018
   -- Autor: Santander- CAH
   -- Input:
   --  p_MRCDO -->  código mercado
   -- Output:
   -- p_CURSOR -> Cursor con los registros de los 2 estados
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_COMBO_BCO_MERCADO (p_MRCDO    IN     NUMBER,
                                       p_CURSOR      OUT SYS_REFCURSOR,
                                       p_ERROR       OUT NUMBER)
   IS
      V_NOM_SP   VARCHAR2 (30) := 'SP_PAB_COMBO_BCO_MERCADO';
   BEGIN
      P_ERROR := PKG_PAB_CONSTANTES.V_OK;

      IF p_MRCDO = PKG_PAB_CONSTANTES.V_FLG_MRD_MN
      THEN
         OPEN p_CURSOR FOR
              SELECT   BCO.COD_BIC_BCO
                FROM   PABS_DT_BANCO_ALMOT BCO, TCDTBAI TBAI, TGEN_0112 PAIS
               WHERE       BCO.COD_BIC_BCO = TBAI.TGCDSWSA
                       AND BCO.FLG_VGN = PKG_PAB_CONSTANTES.v_REG_VIG
                       AND PAIS.CODEPAIS = TBAI.COD_PAIS
                       AND PAIS.TCCPAIS4 =
                             PKG_PAB_CONSTANTES.V_STR_COD_PAS_CHILE --'CL' Chile
            ORDER BY   BCO.COD_BIC_BCO;
      ELSE
         OPEN p_CURSOR FOR
              SELECT   BCO.COD_BIC_BCO
                FROM   PABS_DT_BANCO_ALMOT BCO, TCDTBAI TBAI, TGEN_0112 PAIS
               WHERE       BCO.COD_BIC_BCO = TBAI.TGCDSWSA
                       AND BCO.FLG_VGN = PKG_PAB_CONSTANTES.v_REG_VIG
                       AND PAIS.CODEPAIS = TBAI.COD_PAIS
                       AND PAIS.TCCPAIS4 <>
                             PKG_PAB_CONSTANTES.V_STR_COD_PAS_CHILE --'CL' Chile
            ORDER BY   BCO.COD_BIC_BCO;
      END IF;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);

         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_COMBO_BCO_MERCADO;

   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_OBT_CNL_CON_TPO_OPE
   -- Objetivo: Obtiene el canal segun el tipo
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_TIPO_OPRCN_ALMOT
   -- Fecha: 16/08/2018
   -- Autor: Santander
   -- Input:
   -- P_COD_TPO_OPE_AOS -> Tipo de operación
   -- Output:
   -- P_COD_SIS_ENT_SAL -> Codigo sistema entrada/salida
   -- P_DSC_SIS_ENT_SAL -> Descripcion del canal entrada/salida
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: 27/09/2016, Creacion Procedimiento.
   --***********************************************************************************************
   PROCEDURE Sp_PAB_OBT_CNL_CON_TPO_OPE (P_COD_TPO_OPE_AOS   IN     CHAR,
                                         P_COD_SIS_ENT_SAL      OUT CHAR,
                                         P_DSC_SIS_ENT_SAL      OUT VARCHAR2,
                                         p_ERROR                OUT NUMBER)
   IS
      v_NOM_SP            VARCHAR2 (30) := 'Sp_PAB_OBT_CNL_CON_TPO_OPE';
      v_COD_TPO_OPE_AOS   CHAR (8);
   BEGIN
      --Limpiamos salto de linea y asignamos variable a  buscar.
      v_COD_TPO_OPE_AOS :=
         SUBSTR (REPLACE (P_COD_TPO_OPE_AOS, CHR (13)), 0, 8);

      --Obtenemos el canal de salida
      SELECT   TIOPE.COD_SIS_ENT_SAL, DSC_SIS_ENT_SAL
        INTO   P_COD_SIS_ENT_SAL, P_DSC_SIS_ENT_SAL
        FROM   PABS_DT_TIPO_OPRCN_ALMOT TIOPE,
               PABS_DT_SISTM_ENTRD_SALID SISENT
       WHERE   TIOPE.COD_TPO_OPE_AOS = v_COD_TPO_OPE_AOS
               AND SISENT.COD_SIS_ENT_SAL = TIOPE.COD_SIS_ENT_SAL;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         --Si no la encuentra es por que no existe o no esta parametrizada
         --Dejamos el tipo de operacion "Sin identicar"
         P_COD_SIS_ENT_SAL := PKG_PAB_CONSTANTES.V_STR_TPO_OPE_SIDEN;
         --No existe el tipo de operacion se deja por defecto
         err_code := SQLCODE;
         err_msg := 'No se encontro información:' || v_COD_TPO_OPE_AOS;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_OBT_CNL_CON_TPO_OPE;



   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_VAL_FEC_PROC_ABN
   -- Objetivo: procedimiento que debe validar que la fecha de abono sea correcto
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_TIPO_OPRCN_ALMOT
   -- Fecha: 16/08/2018
   -- Autor: Santander
   -- Input:
   -- P_COD_TPO_OPE_AOS -> Tipo de operación
   -- Output:
   -- P_COD_SIS_ENT_SAL -> Codigo sistema entrada/salida
   -- P_DSC_SIS_ENT_SAL -> Descripcion del canal entrada/salida
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: 27/09/2016, Creacion Procedimiento.
   --***********************************************************************************************
   PROCEDURE Sp_PAB_VAL_FEC_PROC_ABN (P_FEC_ISR_OPE   IN     DATE,
                                      P_FEC_VTA       IN     DATE,
                                      P_COD_DVI       IN     VARCHAR2,
                                      p_ERROR            OUT NUMBER)
   IS
      v_NOM_SP        VARCHAR2 (30) := 'Sp_PAB_VAL_FEC_PROC_ABN';

      v_FEC_ISR_OPE   DATE;
      v_FEC_VTA       DATE;
      v_COD_DVI       CHAR (3);
      v_FECHA_HOY     DATE;
      v_COD_PAIS      CHAR (2) := 'SN';
      v_IND_HABIL     NUMBER (1) := 0;
      v_ERRORCODE     NUMBER;
      v_ERRORMSG      VARCHAR2 (300);
      err_GBO EXCEPTION;
      err_ING EXCEPTION;
      err_VTA EXCEPTION;
   BEGIN
      v_FECHA_HOY := TRUNC (SYSDATE);
      v_FEC_ISR_OPE := TRUNC (P_FEC_ISR_OPE);
      v_FEC_VTA := TRUNC (P_FEC_VTA);
      v_COD_DVI := P_COD_DVI;
      p_ERROR := PKG_PAB_CONSTANTES.v_OK;

      -- VALIDAMOS QUE LA FECHA DE INGRESO SEA DEL DIA

      IF v_COD_DVI = PKG_PAB_CONSTANTES.V_STR_DVI_CLP
      THEN
         IF v_FEC_ISR_OPE = v_FECHA_HOY
         THEN
            -- VALIDAMOS QUE LA FECHA DE INGRESO SEA HABIL.

            PKG_CHI_CON_AOS.SP_CHI_CON_DHB_AOS (v_FEC_ISR_OPE,
                                                v_COD_PAIS,
                                                v_IND_HABIL,
                                                v_ERRORCODE,
                                                v_ERRORMSG);

            IF (v_ERRORCODE = 1)
            THEN
               RAISE err_GBO;
            END IF;

            IF (v_IND_HABIL = 0)
            THEN                                               -- DIA NO HABIL
               v_ERRORCODE := 22000;
               v_ERRORMSG :=
                  'Dia de Ingreso de Operacion NO VALIDO (Fecha dia Inhabil)';
               RAISE err_ING;
            END IF;


            IF v_FEC_VTA = v_FECHA_HOY
            THEN
               PKG_CHI_CON_AOS.SP_CHI_CON_DHB_AOS (v_FEC_VTA,
                                                   v_COD_PAIS,
                                                   v_IND_HABIL,
                                                   v_ERRORCODE,
                                                   v_ERRORMSG);

               IF (v_ERRORCODE = 1)
               THEN
                  RAISE err_GBO;
               END IF;

               IF (v_IND_HABIL = 0)
               THEN                                            -- DIA NO HABIL
                  v_ERRORCODE := 22000;
                  v_ERRORMSG :=
                     'Fecha de Valuta NO VALIDA (Fecha dia Inhabil)';
                  RAISE err_VTA;
               END IF;
            ELSE
               v_ERRORCODE := 22001;
               v_ERRORMSG :=
                  'Fecha de Valuta NO VALIDA (Fecha != Dia Proceso)';
               RAISE err_VTA;
            END IF;
         ELSE
            v_ERRORCODE := 22000;
            v_ERRORMSG :=
               'Dia de Ingreso de Operacion NO VALIDO (Fecha != Dia Proceso)';
            RAISE err_ING;
         END IF;
      END IF;
   EXCEPTION
      WHEN err_GBO
      THEN
         err_code := v_ERRORCODE;
         err_msg := v_ERRORMSG;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN err_ING
      THEN
         err_code := v_ERRORCODE;
         err_msg := v_ERRORMSG;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN err_VTA
      THEN
         err_code := v_ERRORCODE;
         err_msg := v_ERRORMSG;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;

         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_VAL_FEC_PROC_ABN;

   /************************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_BUS_VVV
   -- Objetivo: Buscar si existe el numero de cuenta en AAMM
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: N/A
   -- Fecha: 02/06/16
   -- Autor: Santander
   -- Input:      p_NUM_VVV: Numero Cuenta Corriente
   -- Output:
   --
   -- Input/Output: N/A
   -- Retorno: Retorna si un numero de VVV esta ya registrado en AAMM
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   FUNCTION FN_PAB_BUS_VVV (p_NUM_VVV IN VARCHAR2)
      RETURN INTEGER
   IS
      --Seteo de Variables
      v_NOM_SP        VARCHAR2 (30) := 'FN_PAB_BUS_VVV';
      v_NUM_VVV       CHAR (20);
      v_NUM_VVV_RES   CHAR (20);
      v_FINAL         INTEGER;
      v_FECHA_HOY     DATE := TRUNC (SYSDATE);
   BEGIN
      v_FINAL := 0;
      v_NUM_VVV := p_NUM_VVV;

      SELECT   DISTINCT (NUM_OPE_SIS_ENT)
        INTO   v_NUM_VVV_RES
        FROM   DBO_PAB.PABS_DT_DETLL_OPRCN
       WHERE   NUM_OPE_SIS_ENT = v_NUM_VVV AND FEC_VTA = v_FECHA_HOY;

      IF NVL (v_NUM_VVV_RES, '#') = '#'
      THEN
         v_FINAL := 0;               -- NO EXISTE NUMERO. ES VALIDO EL INGRESO
      ELSE
         v_FINAL := 1;
      END IF;


      RETURN v_FINAL;
   EXCEPTION
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         RETURN NULL;
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END;


   /************************************************************************************************
     -- Funcion/Procedimiento: FN_VAL_HOR_CMN
     -- Objetivo: Valida horario de COMBANC.
     -- Sistema: DBO_PAB.
     -- Base de Datos: DBO_PAB.
     -- Tablas Usadas: N/A
     -- Fecha: 02/06/16
     -- Autor: Santander
     -- Input:      p_NUM_VVV: Numero Cuenta Corriente
     -- Output:
     --
     -- Input/Output: N/A
     -- Retorno: Retorna si un numero de VVV esta ya registrado en AAMM
     -- Observaciones: <Fecha y Detalle de ultimos cambios>
     --***********************************************************************************************/
   FUNCTION FN_VAL_HOR_CMN
      RETURN VARCHAR2
   IS
      --Seteo de Variables
      v_NOM_SP        VARCHAR2 (30) := 'FN_VAL_HOR_CMN';
      v_COD_SIS_SAL   VARCHAR2 (10);
      v_HOR_CMN       NUMBER;
   BEGIN
      BEGIN
         SELECT   HOR_CRR_SIS
           INTO   v_HOR_CMN
           FROM   PABS_DT_SISTM_ENTRD_SALID
          WHERE   COD_SIS_ENT_SAL = PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_CMN;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            RETURN NULL;
            p_s_mensaje := err_code || '-' || err_msg;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            RETURN NULL;
            p_s_mensaje := err_code || '-' || err_msg;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;


      IF (v_HOR_CMN > TO_NUMBER (TO_CHAR (SYSDATE, 'HH24MI')))
      THEN
         v_COD_SIS_SAL := PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_CMN;
      ELSE
         v_COD_SIS_SAL := PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_LBTR;
      END IF;

      RETURN v_COD_SIS_SAL;
   EXCEPTION
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         RETURN NULL;
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END;

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_OBT_URL_EXTERNA
   -- Objetivo: Procedimiento que obtiene la ruta del una pagina externa al front .NET de AAMM en base al usuario y un ID
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: ??
   -- Fecha: 23/04/2020
   -- Autor: Santander
   -- Input:
   -- p_COD_USR -> rut usuario. (Obligatorio).
   -- p_COD_MENU_EXT -> Codigo de URL externa solicitada
   -- Output:
   -- p_URL:MENU_EXT -> URL asociada al usuario y código respectivo.
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_OBT_URL_EXTERNA (p_COD_USR        IN     VARCHAR2,
                                     p_COD_MENU_EXT   IN     NUMBER,
                                     p_URL_MENU_EXT      OUT VARCHAR2,
                                     p_ERROR             OUT NUMBER)
   IS
      v_NOM_SP   VARCHAR2 (30) := 'Sp_PAB_OBT_CAN_ENT_USU';
   BEGIN
      p_URL_MENU_EXT :=
         'https://liquidador-cliente.firebaseapp.com/liquidador/adjuntar-nominas';

      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := NULL;
         err_msg :=
               'No se encontro el usuario :'
            || p_COD_USR
            || ' o se encuentra en estado invalido';
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);

         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_OBT_URL_EXTERNA;

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_OBT_TPO_PROC_VIG
   -- Objetivo: Procedimiento que obtiene la vigencia de un tipo de proceso
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_TIPO_PRCSO
   -- Fecha: 21/07/2020
   -- Autor: Santander
   -- Input:
   -- p_TPO_COD -> codigo tipo de proceso. (Obligatorio.
   -- Output:
   -- p_TPO_COD_VIG -> codigo del canal de entrada.
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: 22/07/2020: Creacion Julio Villalobos (julio.villalobos@servexternos.santander.cl)
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_OBT_TPO_PROC_VIG (p_TPO_COD       IN     CHAR,
                                      p_TPO_COD_VIG      OUT NUMBER,
                                      p_ERROR            OUT NUMBER)
   IS
      v_NOM_SP   VARCHAR2 (30) := 'Sp_PAB_OBT_TPO_PROC_VIG';
   BEGIN
      --Obtenemos la vigencia segun el tipo de proceso
      SELECT   FLG_VGN
        INTO   p_TPO_COD_VIG
        FROM   PABS_DT_TIPO_PRCSO
       WHERE   COD_TPO_PSC = p_TPO_COD;

      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := NULL;
         err_msg := 'No se encontro proceso :' || p_TPO_COD;
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);

         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_OBT_TPO_PROC_VIG;
END PKG_PAB_UTILITY;