CREATE OR REPLACE PACKAGE BODY         PKG_PAB_NOMINA_VAL
IS
   /**  Package el cual contrendra todas las funciones y procedimientos relacionados con los perfiles
   -- @Santander */


   -- Funcion/Procedimiento: SP_PAB_ELI_MOT_NO_HABLT
   -- Objetivo: Procedimiento que elimina registro de errores encontrados por las validaciones de integracion
   -- en las n?minas
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_MOTVO_NO_HABLT
   -- Fecha: 19/08/16
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_NMN -> Numero de nomina
   -- Output:
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_PAB_ELI_MOT_NO_HABLT (p_NUM_FOL_NMN   IN     NUMBER,
                                      p_ERROR            OUT NUMBER)
   IS
      v_NOM_SP   VARCHAR2 (30) := 'SP_PAB_ELI_MOT_NO_HABLT';
   BEGIN
      p_ERROR := PKG_PAB_CONSTANTES.v_OK; --Procedimiento ejecutado de forma correcta

      DELETE FROM   PABS_DT_MOTVO_NO_HABLT
            WHERE   NUM_FOL_NMN = p_NUM_FOL_NMN;
   EXCEPTION
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_ELI_MOT_NO_HABLT;


   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_GES_EST_NOM_REC_INV
   -- Objetivo: Gestiona el cambio de estado de una nomina despues de su validacion
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_CBCRA_NOMNA
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_NOM -> foliador de la nomina
   -- Output:
   -- P_STR_EST_AOS -> string de estado de la operacion
   -- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_GES_EST_NOM_REC_INV (p_NUM_FOL_NOM   IN     NUMBER,
                                         P_STR_EST_AOS      OUT CHAR,
                                         p_ERROR            OUT NUMBER)
   IS
      v_NOM_SP          VARCHAR2 (30) := 'SP_PAB_GES_EST_NOM_REC_INV';
      v_RESUL_EST_VAL   NUMBER;
      v_RESUL_EST_INV   NUMBER;
   BEGIN
      v_RESUL_EST_INV :=
         PKG_PAB_NOMINA.FN_PAB_CONT_EST (
            p_NUM_FOL_NOM,
            PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEINV
         );

      IF v_RESUL_EST_INV >= 1
      THEN
         P_STR_EST_AOS := PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEINV;
      ELSE
         P_STR_EST_AOS := PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEVAL;
      END IF;

      p_ERROR := PKG_PAB_CONSTANTES.v_OK; --Procedimiento ejecutado de forma correcta
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_GES_EST_NOM_REC_INV;

   --**********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_VAL_RUT
   -- Objetivo: Procedimiento que valida RUT de acuerdo a su digito verificador
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: N/A
   -- Fecha: 19/08/16
   -- Autor: Santander
   -- Input:
   -- p_NUM_RUT -> Numero de RUT
   -- p_DIG_VERIF -> Digito Verificador
   -- Output:
   -- p_FLAG_VAL -> Flag cumple con las condiciones de hora y monto importe maximo -> 0 cumple y 1 no cumple
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_VAL_RUT (p_NUM_RUT     IN     NUMBER,
                             p_DIG_VERIF   IN     CHAR,
                             p_FLAG_VAL       OUT NUMBER,
                             p_ERROR          OUT NUMBER)
   IS
      v_NOM_SP   VARCHAR2 (30) := 'Sp_PAB_VAL_RUT';
      v_RUT      VARCHAR2 (10) := SUBSTR (LPAD (p_NUM_RUT, 9, '0'), 1, 10);
      v_RES      NUMBER (2);
      v_DIG      VARCHAR2 (1);
   BEGIN
      p_ERROR := PKG_PAB_CONSTANTES.v_OK;

      v_RES :=
         11
         - MOD (
                TO_NUMBER (SUBSTR (v_RUT, 1, 1)) * 4
              + TO_NUMBER (SUBSTR (v_RUT, 2, 1)) * 3
              + TO_NUMBER (SUBSTR (v_RUT, 3, 1)) * 2
              + TO_NUMBER (SUBSTR (v_RUT, 4, 1)) * 7
              + TO_NUMBER (SUBSTR (v_RUT, 5, 1)) * 6
              + TO_NUMBER (SUBSTR (v_RUT, 6, 1)) * 5
              + TO_NUMBER (SUBSTR (v_RUT, 7, 1)) * 4
              + TO_NUMBER (SUBSTR (v_RUT, 8, 1)) * 3
              + TO_NUMBER (SUBSTR (v_RUT, 9, 1)) * 2,
              11
           );

      IF v_RES = 10
      THEN
         v_DIG := 'K';
      ELSIF v_RES = 11
      THEN
         v_DIG := '0';
      ELSE
         v_DIG := LTRIM (v_RES);
      END IF;

      IF v_dig = p_DIG_VERIF
      THEN
         p_FLAG_VAL := PKG_PAB_CONSTANTES.V_OK;
      ELSE
         p_FLAG_VAL := PKG_PAB_CONSTANTES.V_error;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_VAL_RUT;

   --**********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUS_VAL_ARC
   -- Objetivo: Procedimiento que consultara la tabla donde se almacenaran las distintas relaciones
   -- que existen entre los tipos de mensajes, tipo de operaciones y  sistema de salida.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: N/A
   -- Fecha: 14/09/2016
   -- Autor: Santander
   -- Input:
   -- p_COD_ARC_ENT -> Codigo de archivo de Entrada
   -- Output:
   -- p_CURSOR  -> Cursor con todas las operaciones
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_BUS_VAL_ARC (p_COD_ARC_ENT   IN     CHAR,
                                 p_CURSOR           OUT SYS_REFCURSOR,
                                 p_ERROR            OUT NUMBER)
   IS
      v_NOM_SP   VARCHAR2 (30) := 'Sp_PAB_BUS_VAL_ARC';
   BEGIN
      OPEN p_CURSOR FOR
         SELECT   ARCH.COD_VAL_ARC                                    -- Llave
                                  ,
                  ARCH.COD_TPO_OPE_AOS,
                  ARCH.FLG_MRD_UTZ_SWF,
                  ARCH.COD_MT_SWF,
                  ARCH.COD_CAM_ARC,
                  ARCH.VAL_LST_DTS,
                  ARCH.VAL_ERS_RGL,
                  ARCH.VAL_LRG_ERS_RGL,
                  ARCH.FLG_TPO_ALR,
                  ARCH.FLG_VGN_VAL,
                  ARCH.COD_TBL_DPN,
                  ARCH.COD_CAM_DPN,
                  ARCHENT.NOM_TAG_CAM_ARC
           FROM   PABS_DT_VLDCN_CAMPO_ARCHV ARCH,
                  PABS_DT_ESTRT_ARCHv_ENTRD ARCHENT
          WHERE       ARCH.COD_ARC_ENT = p_COD_ARC_ENT
                  AND ARCH.COD_CAM_ARC = ARCHENT.COD_CAM_ARC
                  AND ARCHENT.COD_ARC_ENT = p_COD_ARC_ENT;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_BUS_VAL_ARC;

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_OBT_VAL_NOMINA
   -- Objetivo: Procedimiento que devuelve las validaciones de una nomina
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_BIBLI_ERROR_ALMOT
   -- Fecha: 24/02/17
   -- Autor: Santander
   -- Input:
   -- Output:
   -- p_validaciones-> Cursor con las validaciones
   -- p_ERROR              -> Indicador codigo error
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_OBT_VAL_NOMINA (p_validaciones   OUT SYS_REFCURSOR,
                                    p_ERROR          OUT NUMBER)
   IS
      --Seteo de Variables
      v_NOM_SP    VARCHAR2 (30) := 'Sp_PAB_OBT_VAL_NOMINA';
      v_org_stl   NUMBER := 1;
   BEGIN
      p_ERROR := PKG_PAB_CONSTANTES.v_ok;

      OPEN p_validaciones FOR
           SELECT   COD_ERR_BIB AS CODIGO_ERROR,
                    DECODE (FLG_VGN,
                            PKG_PAB_CONSTANTES.V_REG_VIG,
                            PKG_PAB_CONSTANTES.V_STR_SI_CORTO,
                            PKG_PAB_CONSTANTES.V_STR_NO_CORTO)
                       AS VIGENTE
             FROM   PABS_DT_BIBLI_ERROR_ALMOT
            WHERE   FLG_ORG_STL = v_org_stl
         ORDER BY   COD_ERR_BIB;
   EXCEPTION
      WHEN DUP_VAL_ON_INDEX
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_OBT_VAL_NOMINA;


   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_VAL_FEC
   -- Objetivo: Procedimiento que realiza validaciones segun el parametro de entrada
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_BIBLI_ERROR_ALMOT
   -- Fecha: 24/02/17
   -- Autor: Santander
   -- Input:
   -- p_tipoVal --> Tipo de validacion (1 - Mayor que fecha sistema)
   -- p_fecha   --> Fecha a Validar
   -- Output:
   -- p_ERROR              -> Indicador codigo error
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_VAL_FEC (p_tipoVal   IN     NUMBER,
                             p_fecha     IN     DATE,
                             p_ERROR        OUT NUMBER)
   IS
      --Seteo de Variables
      v_NOM_SP     VARCHAR2 (30) := 'Sp_PAB_VAL_FEC';
      v_fechaHoy   DATE := SYSDATE;
   BEGIN
      p_ERROR := PKG_PAB_CONSTANTES.v_ok;

      CASE
         --Fecha mayor que la de sistema
         WHEN p_tipoVal = 1
         THEN
            IF (TRUNC (p_fecha) < TRUNC (v_fechaHoy))
            THEN
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            END IF;
      END CASE;
   EXCEPTION
      WHEN DUP_VAL_ON_INDEX
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_VAL_FEC;

   --**********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_INS_ERR_VAL
   -- Objetivo: Procedimiento que registra los errores encontrados por las validaciones de integraci?n
   -- en las n?minas
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_MOTVO_NO_HABLT
   -- Fecha: 19/08/16
   -- Autor: Santander
   -- Input:
   -- p_FEC_ISR_OPE -> Fecha de una Operacion
   -- p_NUM_FOL_OPE -> Foliador del banco
   -- p_COD_ARC_ENT -> Codigo Archivo de Entrada
   -- p_COD_CAM_ARC -> Numero de campo del archivo
   -- p_COD_ERR_BIB -> Codifgo del error
   -- p_NUM_FOL_NMN -> Numero de folio de la nomina
   -- Output:
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_PAB_INS_ERR_VAL (p_FEC_ISR_OPE   IN     DATE,
                                 p_NUM_FOL_OPE   IN     NUMBER,
                                 p_COD_ARC_ENT   IN     CHAR,
                                 p_COD_CAM_ARC   IN     NUMBER,
                                 p_COD_ERR_BIB   IN     CHAR,
                                 p_NUM_FOL_NMN   IN     NUMBER,
                                 p_ERROR            OUT NUMBER)
   IS
      v_NOM_SP               VARCHAR2 (30) := 'Sp_PAB_INS_ERR_VAL';
      v_FEC_CNS              VARCHAR2 (19) := TO_CHAR (p_FEC_ISR_OPE, 'dd-mm-yyyy');
      v_FEC_ISR_DET_OPE_NM   DATE;
   BEGIN
      p_ERROR := PKG_PAB_CONSTANTES.v_OK; --Procedimiento ejecutado de forma correcta

      v_FEC_ISR_DET_OPE_NM := p_FEC_ISR_OPE;

      INSERT INTO PABS_DT_MOTVO_NO_HABLT (NUM_COR_MVO,
                                          FEC_ISR_OPE,
                                          NUM_FOL_OPE,
                                          COD_ARC_ENT,
                                          COD_CAM_ARC,
                                          COD_ERR_BIB,
                                          NUM_FOL_NMN)
        VALUES   (SPK_PAB_MOTVO_NO_HABLT.NEXTVAL,
                  v_FEC_ISR_DET_OPE_NM,
                  p_NUM_FOL_OPE,
                  p_COD_ARC_ENT,
                  p_COD_CAM_ARC,
                  p_COD_ERR_BIB,
                  p_NUM_FOL_NMN);
   EXCEPTION
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_INS_ERR_VAL;

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ACT_EST_OPE_NOMINA
   -- Objetivo: Procedimiento que actualiza el estado de una operacion de una nomina.
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_OPE -> foliador del banco, PK tabla operaciones nomina.
   -- p_FEC_ISR_OPE -> Fecha de insercion de operacion en el sistema, PK tabla operaciones n?mina.
   -- p_COD_EST_AOS -> Identificador de nuevo estado.
   -- p_GLS_ADC_EST -> motivo o glosa de la actualizacion
   -- Output:
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_ACT_EST_OPE_NOMINA (p_NUM_FOL_OPE   IN     NUMBER,
                                        p_FEC_ISR_OPE   IN     DATE,
                                        p_COD_EST_AOS   IN     NUMBER,
                                        p_GLS_ADC_EST   IN     VARCHAR2,
                                        p_ERROR            OUT NUMBER)
   IS
      v_NOM_SP            VARCHAR2 (100) := 'Sp_PAB_ACT_EST_OPE_NOMINA';
      v_COD_EST_AOS_ANT   NUMBER;
      v_COD_EST_AOS_NUE   NUMBER;
   BEGIN
      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
      v_COD_EST_AOS_NUE := p_COD_EST_AOS;

      --GCP
      --Rescatamos el estado actual de la operacion
      v_COD_EST_AOS_ANT :=
         PKG_PAB_NOMINA.Fn_PAB_BUSCA_EST_OPE (p_NUM_FOL_OPE, p_FEC_ISR_OPE);

      --Si es validacion
      IF (p_COD_EST_AOS = PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEVAL
          AND v_COD_EST_AOS_ANT = PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDUP)
      THEN
         v_COD_EST_AOS_NUE := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDUP;
      END IF;

      --Actualizamos la operacion
      UPDATE   PABS_DT_DETLL_NOMNA det
         SET   COD_EST_AOS = v_COD_EST_AOS_NUE,
               GLS_ADC_EST = p_GLS_ADC_EST,
               FEC_ACT_NMN = SYSDATE      --Fecha actualización para dashborad
       WHERE   NUM_FOL_OPE = p_NUM_FOL_OPE AND FEC_ISR_OPE = p_FEC_ISR_OPE;

      --Se llama al procedimiento de bitacora para registrar el usuario que modifica el estado de la operacion
      IF (v_COD_EST_AOS_ANT <> v_COD_EST_AOS_NUE)
      THEN
         v_DES_BIT := 'Cambio de estado de operacion';
         PKG_PAB_TRACKING.Sp_PAB_INS_BIT_OPE (p_NUM_FOL_OPE,
                                              p_FEC_ISR_OPE,
                                              'ESB',
                                              v_COD_EST_AOS_ANT,
                                              v_COD_EST_AOS_NUE,
                                              v_DES_BIT,
                                              p_ERROR);
      END IF;
   EXCEPTION
      WHEN DUP_VAL_ON_INDEX
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_ACT_EST_OPE_NOMINA;

   -- ************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUS_CAM_OBT_SWF
   -- Objetivo: Procedimiento que ingresa operaciones del DCV automaticos
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_BIBLI_CAMPO_OBLTO_MT
   --
   -- Fecha: 16/06/17
   -- Autor: SII GROUP
   -- Input:
   -- p_COD_MT_SWF         -> Codigo MT a validar
   -- p_FLG_MRD_UTZ_SWF    -> flag de mercado del mensaje a validar
   -- p_COD_TPO_OPE_AOS    -> codigo operacion a validar
   -- Output:
   -- p_CAM_OBT            -> Retorna campos a validar.
   -- p_ERROR              -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_BUS_CAM_OBT_SWF (
      p_COD_MT_SWF        IN     NUMBER,
      p_FLG_MRD_UTZ_SWF   IN     NUMBER,
      p_COD_TPO_OPE_AOS   IN     VARCHAR2,
      p_CAM_OBT              OUT SYS_REFCURSOR,
      p_ERROR                OUT NUMBER
   )
   IS
      v_NOM_SP            VARCHAR2 (30) := 'Sp_PAB_BUS_CAM_OBT_SWF';
      v_fechaHoy          DATE := SYSDATE;
      v_COD_TPO_OPE_AOS   CHAR (8) := p_COD_TPO_OPE_AOS;
   BEGIN
      OPEN p_CAM_OBT FOR
         SELECT   TAG.NOM_TAG_CAM_ARC, BIB.COD_CAM_ARC
           FROM   PABS_DT_ESTRT_ARCHV_ENTRD TAG,
                  PABS_DT_BIBLI_CAMPO_OBLTO_MT BIB
          WHERE       TAG.COD_CAM_ARC = BIB.COD_CAM_ARC
                  AND BIB.COD_MT_SWF = p_COD_MT_SWF
                  AND BIB.FLG_MRD_UTZ_SWF = p_FLG_MRD_UTZ_SWF
                  AND BIB.COD_TPO_OPE_AOS = v_COD_TPO_OPE_AOS
                  AND BIB.FLG_VGN = PKG_PAB_CONSTANTES.V_REG_VIG;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_BUS_CAM_OBT_SWF;

   -- ************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUS_MSJ_OBT_SWF
   -- Objetivo: Procedimiento que busca los mensajes a validar
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_BIBLI_CAMPO_OBLTO_MT
   --
   -- Fecha: 16/06/17
   -- Autor: SII GROUP
   -- Input: N/A
   -- Output:
   -- p_MSJ_OBT            -> Cursor de retorno de mensajes a validar.
   -- p_ERROR              -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_BUS_MSJ_OBT_SWF (p_MSJ_OBT   OUT SYS_REFCURSOR,
                                     p_ERROR     OUT NUMBER)
   IS
      v_NOM_SP     VARCHAR2 (30) := 'Sp_PAB_BUS_MSJ_OBT_SWF';
      v_fechaHoy   DATE := SYSDATE;
   BEGIN
      OPEN p_MSJ_OBT FOR
         SELECT   COD_MT_SWF, COD_TPO_OPE_AOS, FLG_MRD_UTZ_SWF
           FROM   PABS_DT_BIBLI_CAMPO_OBLTO_MT
          WHERE   FLG_VGN = PKG_PAB_CONSTANTES.V_REG_VIG AND COD_CAM_ARC = 1;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_BUS_MSJ_OBT_SWF;
END PKG_PAB_NOMINA_VAL;