CREATE OR REPLACE PACKAGE BODY         PKG_PAB_OPERACION_INT
IS
   --***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_COMBO_MONEDA
   -- Objetivo: Procedimiento que retorna las monedas, según Flag, 0 Todas , 1 extranjeras
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_MONDA_ALMOT
   -- Fecha: 23-03-2019
   -- Autor: Santander CAH
   -- Input:
   --  p_FLG_MON
   -- Output:
   -- p_CURSOR -> RESULTADOS DE CONSULTA.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_COMBO_MONEDA (p_FLG_MON   IN     NUMBER,
                                  p_CURSOR       OUT SYS_REFCURSOR)
   IS
      v_NOM_SP   VARCHAR2 (30) := 'Sp_PAB_COMBO_MONEDA';
   BEGIN
      IF p_FLG_MON = 0
      THEN                                                -- TODAS LAS MONEDAS
         OPEN p_CURSOR FOR
              SELECT   MON.COD_DVI AS CODIGO, MON.COD_DVI AS DESCRIPCION
                FROM   PABS_DT_MONDA_ALMOT MON
               WHERE   FLG_VGN = PKG_PAB_CONSTANTES.V_REG_VIG
            ORDER BY   COD_DVI;
      ELSE                                                          -- SIN CLP
         OPEN p_CURSOR FOR
              SELECT   MON.COD_DVI AS CODIGO, MON.COD_DVI AS DESCRIPCION
                FROM   PABS_DT_MONDA_ALMOT MON
               WHERE   FLG_VGN = PKG_PAB_CONSTANTES.V_REG_VIG
                       AND MON.COD_DVI <> PKG_PAB_CONSTANTES.v_STR_DVI_CLP
            ORDER BY   COD_DVI;
      END IF;
   --
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_COMBO_MONEDA;


   /************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUS_OPE_LIB_ITR
   -- Objetivo: PROCEDIMIENTO ALMACENADO QUE ENTREGA LOS TIPOS
   -- DE OPERACION LIBERADAS, SEGUN FILTROS INGRESADOS
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN_INTER
                     PABS_DT_SISTM_ENTRD_SALID
   -- Fecha: 26-03-2019
   -- Autor: Santander - CAH
   -- Input:
   --    p_COD_TPO_OPE_AOS_LQD   Tipo de Operación
   --    p_COD_SIS_ENT_ITR       Código sistema Entrada
   --    p_COD_SIS_SAL_ITR       Código Sistema Salida
   --    p_NUM_FOL_OPE_ITR       Folio Operación
   --    p_NUM_FOL_NMN_ITR       Folio Nómina
   --    p_FLG_EGR_ING_ITR       Flag de ingreso o Egreso
   --    p_COD_DVI_ITR           MOneda
   --    p_IMP_DSD               Monto desde
   --    p_IMP_HST               Monto Hasta
   --    p_NUM_DOC_BFC_ITR       Rut Beneficiario
   --    p_NUM_CTA_BFC_ITR       Cuenta Beneficiario
   --    p_NOM_BFC_ITR           Nombre Beneficiario
   --    p_NOM_ODN_ITR           Nombre Ordenante
   --    p_NUM_DOC_ODN_ITR       Rut Ordenante
   --    p_NUM_CTA_ODN_ITR       Cuenta Ordenante
   --    p_COD_TPO_FPA           Forma Pago
   -- Output:
   --        p_CURSOR-> SYS_REFCURSOR, RESULTADOS DE CONSULTA.
   --        p_ERROR-> Devuelve error
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_BUS_OPE_LIB_ITR (
      p_COD_TPO_OPE_AOS_LQD   IN     CHAR,
      p_COD_SIS_ENT_ITR       IN     CHAR,
      p_COD_SIS_SAL_ITR       IN     CHAR,
      p_NUM_FOL_OPE_ITR       IN     NUMBER,
      p_NUM_FOL_NMN_ITR       IN     NUMBER,
      p_FLG_EGR_ING_ITR       IN     NUMBER,
      p_COD_DVI_ITR           IN     CHAR,
      p_IMP_DSD               IN     NUMBER,
      p_IMP_HST               IN     NUMBER,
      p_NUM_DOC_BFC_ITR       IN     CHAR,
      p_NUM_CTA_BFC_ITR       IN     VARCHAR2,
      p_NOM_BFC_ITR           IN     CHAR,
      p_NOM_ODN_ITR           IN     CHAR,
      p_NUM_DOC_ODN_ITR       IN     CHAR,
      p_NUM_CTA_ODN_ITR       IN     VARCHAR2,
      p_COD_TPO_FPA           IN     CHAR,
      p_EST_AOS               IN     VARCHAR2,
      p_FLG                   IN     CHAR,
      p_CURSOR                   OUT SYS_REFCURSOR,
      p_ERROR                    OUT NUMBER
   )
   IS
      --Seteo de Variables
      v_NOM_SP            VARCHAR2 (30) := 'Sp_PAB_BUS_OPE_LIB_ITR';
      v_FLG               VARCHAR2 (3);
      v_FLG1              VARCHAR2 (3);
      v_FLG2              VARCHAR2 (3);
      v_FLG3              VARCHAR2 (3);
      v_FLG4              VARCHAR2 (3);
      v_IMP_DSD           NUMBER;
      v_IMP_HST           NUMBER;
      v_NUM_FOL_NMN_ITR   NUMBER;
      v_NUM_FOL_OPE_ITR   NUMBER;
      v_FLG_EGR_ING_ITR   NUMBER;
      V_FECHA_HOY         DATE := TRUNC (SYSDATE);
      v_NOM_BFC_ITR       PABS_DT_DETLL_OPRCN_INTER.NOM_BFC_ITR%TYPE;
      v_NOM_ODN_ITR       PABS_DT_DETLL_OPRCN_INTER.NOM_ODN_ITR%TYPE;
      v_COD_SIS_ENT_ITR   PABS_DT_DETLL_OPRCN_INTER.COD_SIS_ENT_ITR%TYPE;
      v_COD_SIS_SAL_ITR   PABS_DT_DETLL_OPRCN_INTER.COD_SIS_SAL_ITR%TYPE;
   BEGIN
      v_NOM_BFC_ITR := UPPER (p_NOM_BFC_ITR);
      v_NOM_ODN_ITR := UPPER (p_NOM_ODN_ITR);
      v_COD_SIS_ENT_ITR := P_COD_SIS_ENT_ITR;
      v_COD_SIS_SAL_ITR := P_COD_SIS_SAL_ITR;

      --Siempre data
      IF NVL (p_IMP_DSD, 0) = 0
      THEN                                                 -- VALIDACION NULOS
         v_IMP_DSD := 0;
      ELSE
         v_IMP_DSD := p_IMP_DSD;
      END IF;

      --Siempre data
      IF NVL (p_IMP_HST, 0) = 0
      THEN                                                 -- VALIDACION NULOS
         v_IMP_HST := 9999999999999;
      ELSE
         v_IMP_HST := p_IMP_HST;
      END IF;

      --Accion
      IF p_FLG = PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEPAT
      THEN                      -- PAT PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEPAT
         --Todos
         v_FLG := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAT;
         v_FLG1 := NULL;
         v_FLG2 := NULL;
         v_FLG3 := NULL;
         v_FLG4 := NULL;
      ELSE
         v_FLG := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAB;
         v_FLG1 := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEREC;
         v_FLG2 := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT;
         v_FLG3 := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPDV;
         v_FLG4 := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPECUR;
      END IF;

      IF NVL (p_EST_AOS, 0) = 0
      THEN                      -- PAT PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEPAT
         v_FLG := v_FLG;
         v_FLG1 := v_FLG1;
      ELSE
         v_FLG := p_EST_AOS;
         v_FLG1 := NULL;
      END IF;

      BEGIN
         OPEN p_CURSOR FOR
            SELECT   DECODE (OI.FLG_EGR_ING_ITR, 0, 'Egreso', 'Ingreso')
                        AS FLUJO,
                     PKG_PAB_OPERACION_INT.FN_PAB_OBT_DSC_CNL_PAGO (
                        OI.COD_TPO_FPA
                     )
                        AS FORMA_PAGO,
                     PKG_PAB_UTILITY.FN_PAB_OBT_DSC_SIS_ENT_SAL (
                        OI.COD_SIS_ENT_ITR
                     )
                        AS CANAL_ENTRADA,
                     PKG_PAB_UTILITY.FN_PAB_OBT_DSC_SIS_ENT_SAL (
                        OI.COD_SIS_SAL_ITR
                     )
                        AS CANAL_SALIDA,
                     OI.NUM_FOL_NMN_ITR AS FOL_NOMINA,
                     OI.NUM_FOL_OPE_ITR AS FOL_OPERACION,
                     PKG_PAB_UTILITY.FN_PAB_OBT_DSC_TIP_OPE (
                        OI.COD_TPO_OPE_AOS_LQD
                     )
                        AS TIPO_OPERACION,
                     OI.IMP_OPE_ITR AS MONTO,
                     OI.COD_DVI_ITR AS MONEDA,
                     OI.NUM_DOC_ODN_ITR AS RUT_ORDENANTE,
                     OI.COD_TPD_ODN_ITR AS DIG_ORDENANTE,
                     OI.NOM_ODN_ITR AS NOM_ORDENANTE,
                     OI.NUM_CTA_ODN_ITR AS CTA_ORDENANTE,
                     OI.NUM_DOC_BFC_ITR AS RUT_BENEFICIARIO,
                     OI.COD_TPD_BFC_ITR AS DIG_BENEFICIARIO,
                     OI.NOM_BFC_ITR AS NOM_BENEFICIARIO,
                     OI.NUM_CTA_BFC_ITR AS CTA_BENEFICIARIO,
                     OI.FEC_ISR_OPE_ITR AS FECHA_PAGO,
                     OI.COD_EST_AOS AS ESTADO,
                     DECODE (OI.COD_EST_AOS,
                             5,
                             0,
                             7,
                             0,
                             9,
                             0,
                             15,
                             0,
                             16,
                             0,
                             6,
                             1,
                             23,
                             1)
                        AS FLAG_REC
              FROM   PABS_DT_DETLL_OPRCN_INTER OI,
                     PABS_DT_SISTM_ENTRD_SALID ENT,
                     PABS_DT_SISTM_ENTRD_SALID SAL
             WHERE   OI.COD_SIS_ENT_ITR = ENT.COD_SIS_ENT_SAL -- Canal Entrada
                     AND OI.COD_SIS_SAL_ITR = SAL.COD_SIS_ENT_SAL -- Canal Salida
                     AND OI.COD_EST_AOS IN
                              (v_FLG, v_FLG1, v_FLG2, v_FLG3, v_FLG4)
                     --Campos de busqueda
                     --AND OI.COD_TPO_OPE_AOS_LQD = NVL (p_COD_TPO_OPE_AOS_LQD, OI.COD_TPO_OPE_AOS_LQD)
                     AND OI.COD_TPO_FPA = NVL (p_COD_TPO_FPA, OI.COD_TPO_FPA)
                     AND OI.COD_DVI_ITR = NVL (p_COD_DVI_ITR, OI.COD_DVI_ITR)
                     AND OI.IMP_OPE_ITR BETWEEN v_IMP_DSD AND v_IMP_HST
                     AND OI.NUM_FOL_OPE_ITR =
                           NVL (v_NUM_FOL_OPE_ITR, OI.NUM_FOL_OPE_ITR)
                     AND OI.COD_SIS_ENT_ITR =
                           NVL (v_COD_SIS_ENT_ITR, OI.COD_SIS_ENT_ITR)
                     AND OI.COD_SIS_SAL_ITR =
                           NVL (v_COD_SIS_SAL_ITR, OI.COD_SIS_SAL_ITR)
                     AND OI.FLG_EGR_ING_ITR =
                           NVL (v_FLG_EGR_ING_ITR, OI.FLG_EGR_ING_ITR)
                     AND OI.NUM_FOL_NMN_ITR =
                           NVL (v_NUM_FOL_NMN_ITR, OI.NUM_FOL_NMN_ITR)
                     AND UPPER (NVL (OI.NOM_ODN_ITR, '#')) LIKE
                           NVL ('%' || UPPER (p_NOM_ODN_ITR) || '%',
                                UPPER (NVL (OI.NOM_ODN_ITR, '#')))
                     AND NVL (OI.NUM_CTA_ODN_ITR, '#') =
                           NVL (p_NUM_CTA_ODN_ITR,
                                NVL (OI.NUM_CTA_ODN_ITR, '#'))
                     AND NVL (OI.NUM_DOC_ODN_ITR, '#') =
                           NVL (p_NUM_DOC_ODN_ITR,
                                NVL (OI.NUM_DOC_ODN_ITR, '#'))
                     --AND NVL(OI.NOM_ODN_ITR,'#')    = NVL (p_NOM_ODN_ITR, NVL(OI.NOM_ODN_ITR,'#'))
                     --AND UPPER(OI.NOM_BFC_ITR) LIKE NVL('%'||v_NOM_BFC_ITR||'%', UPPER(OI.NOM_BFC_ITR))
                     --AND NVL(OI.NUM_DOC_BFC_ITR,'#')= NVL (p_NUM_DOC_BFC_ITR, NVL(OI.NUM_DOC_BFC_ITR,'#'))

                     --                     AND NVL(OI.NOM_BFC_ITR,'#')    = NVL (p_NOM_BFC_ITR, NVL(OI.NOM_BFC_ITR,'#'))
                     --AND UPPER(OI.NOM_ODN_ITR) LIKE NVL('%'||v_NOM_ODN_ITR||'%', '%'||UPPER(OI.NOM_ODN_ITR))||'%'
                     --AND NVL(OI.NUM_CTA_BFC_ITR,'#')= NVL (p_NUM_CTA_BFC_ITR, NVL(OI.NUM_CTA_BFC_ITR,'#'))
                     AND TRUNC (OI.FEC_ISR_OPE_ITR) = V_FECHA_HOY;
      END;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         RAISE_APPLICATION_ERROR (-20000, err_code || p_s_mensaje);
   END Sp_PAB_BUS_OPE_LIB_ITR;


   -- ***********************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_OBT_DSC_SIS_ENT_SAL
   -- Objetivo: Funcion que devuelve la descripcion del código de pago de una operación interna
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_TIPO_FRMPG
   -- Fecha: 06-04-2019
   -- Autor: Santander CAH
   -- Input:
   --  p_COD_SIS_ENT_SAL -> Codigo de sistema salida
   -- Output:
   -- N/A.
   -- Input/Output:
   -- N/A.
   -- Retorno:
   -- v_DSC_SIS_ENT_SAL -> Descripcion de codigo de sistema salida consultado
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   FUNCTION FN_PAB_OBT_DSC_CNL_PAGO (p_COD_SIS_ENT_SAL IN CHAR)
      RETURN VARCHAR2
   IS
      v_NOM_SP            VARCHAR2 (30) := 'FN_PAB_OBT_DSC_CNL_PAGO';
      v_DSC_SIS_ENT_SAL   VARCHAR2 (50);
      ERR_COD_INEX EXCEPTION;
   BEGIN
      IF (NVL (p_COD_SIS_ENT_SAL, '#') <> '#')
      THEN
         SELECT   TP.DSC_TPO_FPA
           INTO   v_DSC_SIS_ENT_SAL
           FROM   PABS_DT_TIPO_FRMPG TP
          WHERE   TP.FLG_VGN = PKG_PAB_CONSTANTES.V_REG_VIG
                  AND TP.COD_TPO_FPA = p_COD_SIS_ENT_SAL;
      ELSE
         v_DSC_SIS_ENT_SAL := NULL;
      END IF;

      RETURN v_DSC_SIS_ENT_SAL;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := 'No existe informacion para: ' || p_COD_SIS_ENT_SAL;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END FN_PAB_OBT_DSC_CNL_PAGO;

   /************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_CAMBIA_CNL_PAGO
   -- Objetivo: Procedimiento almacenado que cambia de de canal de pago a operaciones interna
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN_INTER

   -- Fecha: 27-03-2019
   -- Autor: Santander - CAH
   -- Input:
   --    p_NUM_FOL_OPE_ITR     Folio operación
   --    p_FEC_ISR_OPE_ITR     Fecha de inserción
   --    p_COD_EST_AOS         Nuevo estado operación
   --    p_COD_USR             Código usuario
   -- Output:
   --        p_ERROR-> Devuelve error
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_PAB_CAMBIA_CNL_PAGO (p_NUM_FOL_OPE_ITR   IN     NUMBER,
                                     p_FEC_ISR_OPE_ITR   IN     DATE,
                                     p_CANAL_PAGO        IN     CHAR,
                                     p_CANAL_SAL         IN     CHAR,
                                     p_NUM_CTA           IN     VARCHAR2,
                                     p_COD_USR           IN     VARCHAR2,
                                     p_ERROR                OUT NUMBER)
   IS
      v_NOM_SP            VARCHAR2 (30) := 'SP_PAB_CAMBIA_CNL_PAGO';
      v_COD_EST_AOS       PABS_DT_DETLL_OPRCN_INTER.COD_EST_AOS%TYPE;
      v_COD_EST_AOS_ACT   NUMBER;
   BEGIN
      -- Rescatamos el estado actual de la operación.
      PKG_PAB_OPERACION_INT.SP_PAB_OBT_EST_ACT_OPE_ITR (p_NUM_FOL_OPE_ITR,
                                                        p_FEC_ISR_OPE_ITR,
                                                        v_COD_EST_AOS_ACT);

      --Actualizamos la operación con el nuevo canal de pago
      UPDATE   PABS_DT_DETLL_OPRCN_INTER OI
         SET   OI.COD_TPO_FPA = p_CANAL_PAGO,
               OI.COD_SIS_SAL_ITR = p_CANAL_SAL,
               OI.NUM_CTA_BFC_ITR = p_NUM_CTA, --- Revisar si podemos dejar la cta destino aca
               OI.FEC_ACT_OPE_ITR = SYSDATE             -- Fecha actualización
       WHERE   OI.NUM_FOL_OPE_ITR = p_NUM_FOL_OPE_ITR
               AND OI.FEC_ISR_OPE_ITR = p_FEC_ISR_OPE_ITR;

      --Llamamos al procedimiento de Bitacora
      v_DES_BIT := 'Cambio de canal de pago a: ' || p_CANAL_PAGO;

      PKG_PAB_TRACKING.Sp_PAB_INS_BIT_OPE (p_NUM_FOL_OPE_ITR,
                                           p_FEC_ISR_OPE_ITR,
                                           p_COD_USR,
                                           v_COD_EST_AOS_ACT,
                                           v_COD_EST_AOS_ACT,
                                           v_DES_BIT,
                                           p_ERROR);
      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         RAISE_APPLICATION_ERROR (-20000, err_code || p_s_mensaje);
   END SP_PAB_CAMBIA_CNL_PAGO;

   /************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_OBT_EST_ACT_OPE_ITR
   -- Objetivo: Procedimiento que retorna el estado actual de la operación interna
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN_INTER
   -- Fecha: 26-03-2019
   -- Autor: Santander CAH
   -- Input:    p_NUM_FOL_OPE: Numero de Folio de una Operacion
   --           p_FEC_ISR_OPE: Fecha de una Operacion
   -- Output:
   --           p_COD_EST_AOS: Estado actual de la operacion
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_PAB_OBT_EST_ACT_OPE_ITR (p_NUM_FOL_OPE_ITR   IN     NUMBER,
                                         p_FEC_ISR_OPE_ITR   IN     DATE,
                                         p_COD_EST_AOS          OUT NUMBER)
   IS
      v_NOM_SP   VARCHAR2 (30) := 'SP_PAB_OBT_EST_ACT_OPE_ITR';
   BEGIN
      --Obtenemos el estado actual de la operacion
      SELECT   COD_EST_AOS
        INTO   p_COD_EST_AOS
        FROM   PABS_DT_DETLL_OPRCN_INTER
       WHERE   NUM_FOL_OPE_ITR = p_NUM_FOL_OPE_ITR
               AND FEC_ISR_OPE_ITR = p_FEC_ISR_OPE_ITR;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg :=
               'No se encontro operación folio:'
            || p_NUM_FOL_OPE_ITR
            || ' Fecha:'
            || TO_CHAR (p_FEC_ISR_OPE_ITR, 'dd-mm-yyyy hh24:mi:ss');
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_OBT_EST_ACT_OPE_ITR;

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_BUS_CUENTA_PAGO
   -- Objetivo: Procedimiento almacenado que busca numero relacionado a la cuenta del tipo abono
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_CENTA_CNTBL
   -- Fecha: 01-04-2019
   -- Autor: Santander - CAH
   -- Input:
   -- p_COD_SIS_SAL ->  codigo sistema entrada salida
   -- p_COD_DVI      >  Tipo de operaci?n Ingreso o Egreso
   -- Output:
   -- p_CURSOR     -> Cursor con registro encontrado
   -- p_ERROR      -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_BUS_CUENTA_PAGO (p_COD_SIS_SAL   IN     CHAR,
                                     p_COD_DVI       IN     CHAR,
                                     p_CURSOR           OUT SYS_REFCURSOR,
                                     p_ERROR            OUT NUMBER)
   IS
      v_NOM_SP             VARCHAR2 (30) := 'SP_PAB_BUS_CUENTA_PAGO';
      v_FLAG_MRD_UTZ_SWF   NUMBER (1);
   BEGIN
      P_ERROR := PKG_PAB_CONSTANTES.V_OK;

      IF p_COD_DVI = PKG_PAB_CONSTANTES.V_STR_DVI_CLP
      THEN
         v_FLAG_MRD_UTZ_SWF := PKG_PAB_CONSTANTES.V_FLG_MRD_MN;
      ELSE
         v_FLAG_MRD_UTZ_SWF := PKG_PAB_CONSTANTES.V_FLG_MRD_MX;
      END IF;


      IF p_COD_SIS_SAL = 'CONVIG'
      THEN
         OPEN p_CURSOR FOR
            SELECT   '1234567890' AS NUM_CTA_CTB
              FROM   PABS_DT_CENTA_CNTBL
             WHERE       COD_SIS_ENT_SAL = 'VIGVARIOS'
                     AND FLAG_MRD_UTZ_SWF = v_FLAG_MRD_UTZ_SWF
                     AND FLG_VGN = PKG_PAB_CONSTANTES.V_REG_VIG;
      ELSE
         OPEN p_CURSOR FOR
            SELECT   NUM_CTA_CTB AS NUM_CTA_CTB
              FROM   PABS_DT_CENTA_CNTBL
             WHERE       COD_SIS_ENT_SAL = p_COD_SIS_SAL
                     AND FLAG_MRD_UTZ_SWF = v_FLAG_MRD_UTZ_SWF
                     AND FLG_VGN = PKG_PAB_CONSTANTES.V_REG_VIG;
      END IF;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg :=
               'No hay registos asociados al canal: '
            || p_COD_SIS_SAL
            || ' -flag mercado: '
            || v_FLAG_MRD_UTZ_SWF;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);

         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_BUS_CUENTA_PAGO;



   -- **************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_ELI_OPE_ITR
   -- Objetivo: Procedimiento que actualiza el estado a eliminada una operación interna
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN_INTER
   -- Fecha: 01-04-2019
   -- Autor: Santander - CAH
   -- Input:
   --        p_NUM_FOL_OPE_ITR -> Foliador del banco
   --        p_FEC_ISR_OPE_ITR -> Fecha de Operación
   --        p_COD_USR         -> Rut usuario
   --        p_GLS_EST_ELI     -> Motivo o glosa de eliminación
   -- Output:
   --        p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- **************************************************************************************************
   PROCEDURE SP_PAB_ELI_OPE_ITR (p_NUM_FOL_NMN       IN     NUMBER,
                                 p_FEC_ISR_OPE_ITR   IN     DATE,
                                 p_NUM_FOL_OPE_ITR   IN     NUMBER,
                                 p_COD_USR           IN     VARCHAR2,
                                 p_GLS_EST_ELI       IN     VARCHAR2,
                                 p_ERROR                OUT NUMBER)
   IS
      v_NOM_SP            VARCHAR2 (30) := 'SP_PAB_ELI_OPE_ITR';
      v_COD_EST_AOS_ACT   NUMBER;
   BEGIN
      P_ERROR := PKG_PAB_CONSTANTES.V_OK;


      -- Rescatamos el estado actual de la operación.
      SP_PAB_OBT_EST_ACT_OPE_ITR (p_NUM_FOL_OPE_ITR,
                                  p_FEC_ISR_OPE_ITR,
                                  v_COD_EST_AOS_ACT);

      IF p_NUM_FOL_NMN = 0
      THEN
         BEGIN
            --Actualiza el estado de operación interna a eliminada
            UPDATE   PABS_DT_DETLL_OPRCN_INTER OIT
               SET   OIT.COD_EST_AOS = PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEELI,
                     OIT.GLS_ADC_EST = p_GLS_EST_ELI,
                     OIT.FEC_ACT_OPE_ITR = SYSDATE
             WHERE   OIT.NUM_FOL_OPE_ITR = p_NUM_FOL_OPE_ITR
                     AND OIT.FEC_ISR_OPE_ITR = p_FEC_ISR_OPE_ITR;
         EXCEPTION
            WHEN OTHERS
            THEN
               err_code := SQLCODE;
               err_msg := SUBSTR (SQLERRM, 1, 300);
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
               p_s_mensaje := err_code || '-' || err_msg;
               RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
         END;
      END IF;

      --Se registra en bitacora el estado eliminada de la operación interna.
      PKG_PAB_TRACKING.Sp_PAB_INS_BIT_OPE (
         p_NUM_FOL_OPE_ITR,
         p_FEC_ISR_OPE_ITR,
         p_COD_USR,
         v_COD_EST_AOS_ACT,                                    --Estado Actual
         PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEELI,                 -- Eliminada
         p_GLS_EST_ELI,
         p_ERROR
      );
   EXCEPTION
      WHEN DUP_VAL_ON_INDEX
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         P_ERROR := PKG_PAB_CONSTANTES.V_ERROR;
         PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (err_code,
                                             V_NOM_PCK,
                                             err_msg,
                                             V_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_ELI_OPE_ITR;

   -- **************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_EST_OPE_ITR
   -- Objetivo: Procedimiento que actualiza el estado de una operación interna
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN_INTER
   -- Fecha: 09-07-2019
   -- Autor: Santander - CAH
   -- Input:
   --        p_NUM_FOL_OPE_ITR -> Foliador del banco
   --        p_FEC_ISR_OPE_ITR -> Fecha de Operación
   --        p_COD_USR         -> Rut usuario
   --        p_GLS_MTV_EST     -> Motivo o glosa de rechazo
   -- Output:
   --        p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- **************************************************************************************************
   PROCEDURE SP_PAB_EST_OPE_ITR (p_NUM_FOL_NMN       IN     NUMBER,
                                 p_FEC_ISR_OPE_ITR   IN     DATE,
                                 p_NUM_FOL_OPE_ITR   IN     NUMBER,
                                 p_COD_USR           IN     VARCHAR2,
                                 p_GLS_MTV_EST       IN     VARCHAR2,
                                 p_FLAG              IN     VARCHAR2,
                                 p_ERROR                OUT NUMBER)
   IS
      v_NOM_SP            VARCHAR2 (30) := 'SP_PAB_REC_OPE_ITR';
      v_COD_EST_AOS_ACT   NUMBER;
      v_EST               VARCHAR2 (3);
   BEGIN
      P_ERROR := PKG_PAB_CONSTANTES.V_OK;

      CASE p_FLAG
         WHEN PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEREC
         THEN
            v_EST := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEREC;
         WHEN PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEPAT
         THEN
            v_EST := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAT;
         WHEN PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEAUT
         THEN
            v_EST := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT;
         WHEN PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEELI
         THEN
            v_EST := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEELI;
         WHEN PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPELIB
         THEN
            v_EST := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPELIB;
         WHEN PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEPAB
         THEN
            v_EST := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAB;
      END CASE;

      -- Rescatamos el estado actual de la operación.
      SP_PAB_OBT_EST_ACT_OPE_ITR (p_NUM_FOL_OPE_ITR,
                                  p_FEC_ISR_OPE_ITR,
                                  v_COD_EST_AOS_ACT);

      IF p_NUM_FOL_NMN = 0
      THEN
         BEGIN
            --Actualiza el estado de operación interna
            UPDATE   PABS_DT_DETLL_OPRCN_INTER OIT
               SET   OIT.COD_EST_AOS = v_EST,
                     OIT.GLS_ADC_EST = p_GLS_MTV_EST,
                     OIT.FEC_ACT_OPE_ITR = SYSDATE
             WHERE   OIT.NUM_FOL_OPE_ITR = p_NUM_FOL_OPE_ITR
                     AND OIT.FEC_ISR_OPE_ITR = p_FEC_ISR_OPE_ITR;
         EXCEPTION
            WHEN OTHERS
            THEN
               err_code := SQLCODE;
               err_msg := SUBSTR (SQLERRM, 1, 300);
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
               p_s_mensaje := err_code || '-' || err_msg;
               RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
         END;
      END IF;

      --Se registra en bitacora el estado rechazada de la operación interna.
      PKG_PAB_TRACKING.Sp_PAB_INS_BIT_OPE (p_NUM_FOL_OPE_ITR,
                                           p_FEC_ISR_OPE_ITR,
                                           p_COD_USR,
                                           v_COD_EST_AOS_ACT,  --Estado Actual
                                           v_EST,              -- Nuevo estado
                                           p_GLS_MTV_EST,
                                           p_ERROR);
   EXCEPTION
      WHEN DUP_VAL_ON_INDEX
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         P_ERROR := PKG_PAB_CONSTANTES.V_ERROR;
         PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (err_code,
                                             V_NOM_PCK,
                                             err_msg,
                                             V_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_EST_OPE_ITR;

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_GES_ELI_OPE_ITR
   -- Objetivo: Procedimiento que recibe operaciones internas para eliminar
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: N/A
   -- Fecha:  03-04-2019
   -- Autor: Santander
   -- Input:
   --        p_REG_OPER -> Tipo registro que contiene el numeros y fecha operaciones internas
   --        p_COD_USR -> Rut usuario
   --        p_GLS_EST_ELI -> Motivo o glosa de eliminacion
   -- Output:
   --      p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_GES_ELI_OPE_ITR (p_REG_OPER      IN     REG_OPER,
                                     p_COD_USR       IN     VARCHAR2,
                                     p_GLS_EST_ELI   IN     VARCHAR2,
                                     p_ERROR            OUT NUMBER)
   IS
      --
      v_NOM_SP        VARCHAR2 (31) := 'SP_PAB_GES_ELI_OPE_ITR';
      v_FEC_ISR_OPE   DATE;
      v_EST_NMN       NUMBER;
   BEGIN
      p_ERROR := PKG_PAB_CONSTANTES.v_OK;

      IF p_REG_OPER.COUNT > 0
      THEN
         FOR i IN p_REG_OPER.FIRST .. p_REG_OPER.LAST
         LOOP
            PKG_PAB_OPERACION_INT.Sp_PAB_ELI_OPE_ITR (
               p_REG_OPER (i).NUM_FOL_NMN,
               p_REG_OPER (i).FEC_ISR_OPE,
               p_REG_OPER (i).NUM_FOL_OPE,
               p_COD_USR,
               p_GLS_EST_ELI,
               p_ERROR
            );
         END LOOP;
      END IF;

      COMMIT;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_GES_ELI_OPE_ITR;


   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_GES_EST_OPE_ITR
   -- Objetivo: Procedimiento que recibe operaciones internas para cambiar archivo
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: N/A
   -- Fecha:  09-07-2019
   -- Autor: Santander
   -- Input:
   --        p_REG_OPER -> Tipo registro que contiene el numeros y fecha operaciones internas
   --        p_COD_USR -> Rut usuario
   --        p_GLS_EST -> Motivo o glosa
   -- Output:
   --      p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_GES_EST_OPE_ITR (p_REG_OPER   IN     REG_OPER,
                                     p_COD_USR    IN     VARCHAR2,
                                     p_GLS_EST    IN     VARCHAR2,
                                     p_FLAG       IN     VARCHAR2,
                                     p_ERROR         OUT NUMBER)
   IS
      --
      v_NOM_SP        VARCHAR2 (31) := 'SP_PAB_GES_REC_OPE_ITR';
      v_FEC_ISR_OPE   DATE;
      v_EST_NMN       NUMBER;
   BEGIN
      p_ERROR := PKG_PAB_CONSTANTES.v_OK;

      IF p_REG_OPER.COUNT > 0
      THEN
         FOR i IN p_REG_OPER.FIRST .. p_REG_OPER.LAST
         LOOP
            PKG_PAB_OPERACION_INT.Sp_PAB_EST_OPE_ITR (
               p_REG_OPER (i).NUM_FOL_NMN,
               p_REG_OPER (i).FEC_ISR_OPE,
               p_REG_OPER (i).NUM_FOL_OPE,
               p_COD_USR,
               p_GLS_EST,
               p_FLAG,
               p_ERROR
            );
         END LOOP;
      END IF;

      COMMIT;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_GES_EST_OPE_ITR;

   /************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUS_DET_OPE
   -- Objetivo: Procedimiento almacenado que muestra el detalle de una operación interna
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas:  PABS_DT_DETLL_OPRCN_INTER
   --
   -- Fecha: 27-03-2019
   -- Autor: Santander
   -- Input: p_NUM_FOL_OPE_ITR   Folio de la operación interna.
   --        p_FEC_ISR_OPE_ITR: Fecha de inserción de .
   -- Output:
   --        p_CURSOR : SYS_REFCURSOR, resultados de consulta.
   --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_PAB_BUS_DETLL_OPE_INTR (
      p_NUM_FOL_OPE_ITR   IN     NUMBER,
      p_FEC_ISR_OPE_ITR   IN     DATE,
      p_CURSOR               OUT SYS_REFCURSOR,
      p_ERROR                OUT NUMBER
   )
   IS
      v_NOM_SP   VARCHAR2 (30) := 'SP_PAB_BUS_DETLL_OPE_INTR';
   BEGIN
      p_ERROR := '1';
      err_code := '1';
      err_msg :=
            'dato que llega: '
         || p_NUM_FOL_OPE_ITR
         || ' - '
         || p_FEC_ISR_OPE_ITR;
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                          v_NOM_PCK,
                                          err_msg,
                                          v_NOM_SP);

      OPEN p_CURSOR FOR
         SELECT   DECODE (FLG_EGR_ING_ITR,
                          PKG_PAB_CONSTANTES.V_FLG_EGR,
                          PKG_PAB_CONSTANTES.V_STR_FLG_EGR,
                          PKG_PAB_CONSTANTES.V_STR_FLG_ING)
                     AS TIPO_FLUJO,
                  PKG_PAB_OPERACION_INT.FN_PAB_OBT_DSC_COD_TIP_PAG (
                     OI.COD_TPO_FPA
                  )
                     AS TIPO_PAGO,
                  COD_TPO_FPA AS FORMA_PAGO,
                  IMP_OPE_ITR AS MONTO_OPERACION,
                  COD_DVI_ITR AS CODIGO_MONEDA,
                  GLS_OPC_ITR AS OBSERVACION,
                  NUM_DOC_BFC_ITR AS RUT_BENEFICIARIO,
                  COD_TPD_BFC_ITR AS DIG_BENEFICIARIO,
                  NOM_BFC_ITR AS NOMBRE_BENEFICIARIO,
                  NUM_CTA_BFC_ITR AS CUENTA_BENEFICIARIO,
                  NUM_DOC_ODN_ITR AS RUT_ORDENANTE,
                  COD_TPD_ODN_ITR AS DIG_ORDENANTE,
                  NOM_ODN_ITR AS NOMBRE_ORDENANTE,
                  NUM_CTA_ODN_ITR AS CUENTA_ORDENANTE,
                  DECODE (OI.COD_TPO_FPA, 'CONVIG', NUM_CTA_BFC_ITR, '0')
                     AS NUMERO_CUENTA,
                  OI.NUM_MVT_CTB_ITR AS NUM_MOVIMIENTO,
                  OI.NUM_REF_CTB_ITR AS REF_CONTABLE,
                  PKG_PAB_UTILITY.FN_PAB_OBT_DSC_SIS_ENT_SAL (
                     COD_SIS_ENT_ITR
                  )
                     AS CANAL_ENTRADA,
                  PKG_PAB_UTILITY.FN_PAB_OBT_DSC_SIS_ENT_SAL (
                     COD_SIS_SAL_ITR
                  )
                     AS CANAL_SALIDA,
                  NUM_FOL_OPE_ITR AS NUMERO_OPERACION,
                  NUM_FOL_NMN_ITR AS NUMERO_NOMINA,
                  FEC_ISR_OPE_ITR AS FECHA_OPERACION,
                  COD_SUC_ITR AS CODIGO_SUCURSAL,
                  PKG_PAB_UTILITY.FN_PAB_OBT_DSC_COD_EST (COD_EST_AOS)
                     AS ESTADO_OPERACION,
                  NUM_OPE_SIS_ENT_ITR AS NUMERO_ORIGEN,
                  DECODE (COD_TPO_ING,
                          PKG_PAB_CONSTANTES.V_COD_TIP_ING_MAS,
                          PKG_PAB_CONSTANTES.V_STR_TIP_ING_MAS,
                          PKG_PAB_CONSTANTES.V_COD_TIP_ING_UNI,
                          PKG_PAB_CONSTANTES.V_STR_TIP_ING_UNI,
                          PKG_PAB_CONSTANTES.V_COD_TIP_ING_DIR,
                          PKG_PAB_CONSTANTES.V_STR_TIP_ING_DIR)
                     AS TIPO_INGRESO,
                  GLS_ADC_EST AS INFO_ESTADO_OPERACION
           FROM   PABS_DT_DETLL_OPRCN_INTER OI
          WHERE   OI.NUM_FOL_OPE_ITR = p_NUM_FOL_OPE_ITR
                  AND TO_DATE (OI.FEC_ISR_OPE_ITR, 'DD/MM/YYYY HH:MI:SS') =
                        TO_DATE (p_FEC_ISR_OPE_ITR, 'DD/MM/YYYY HH:MI:SS')
                  AND TRUNC (OI.FEC_ISR_OPE_ITR) = TRUNC (SYSDATE) -- crear variables de inciio y fin día
                  AND OI.COD_EST_AOS <>
                        PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEELI; -- 4revisar estados

      p_ERROR := PKG_PAB_CONSTANTES.v_OK; --Procedimiento ejecutado de forma correcta
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := 'No se encuentra la operación: ' || p_NUM_FOL_OPE_ITR;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_BUS_DETLL_OPE_INTR;

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_OBT_DSC_COD_TIP_PAG
   -- Objetivo: Funcion que devuelve la descripcion del toipo de forma de pago
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_SISTM_ENTRD_SALID
   -- Fecha: 40-04-2019
   -- Autor: Santander CAH
   -- Input:
   --  p_COD_TPO_FPA -> Codigo de tipo pago
   -- Output:
   -- N/A.
   -- Input/Output:
   -- N/A.
   -- Retorno:
   -- v_DSC_TPO_FPA -> Descripcion de Tipo pago
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   FUNCTION FN_PAB_OBT_DSC_COD_TIP_PAG (p_COD_TPO_FPA IN CHAR)
      RETURN VARCHAR2
   IS
      v_NOM_SP        VARCHAR2 (30) := 'FN_PAB_OBT_DSC_COD_TIP_PAG';
      v_DSC_TPO_FPA   VARCHAR2 (50);
      ERR_COD_INEX EXCEPTION;
   BEGIN
      IF (NVL (p_COD_TPO_FPA, '#') <> '#')
      THEN
         SELECT   TP.DSC_TPO_FPA
           INTO   v_DSC_TPO_FPA
           FROM   PABS_DT_TIPO_FRMPG TP
          WHERE   TP.FLG_VGN = 1 AND TP.COD_TPO_FPA = p_COD_TPO_FPA;
      ELSE
         v_DSC_TPO_FPA := NULL;
      END IF;

      RETURN v_DSC_TPO_FPA;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := 'No existe informacion para: ' || p_COD_TPO_FPA;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END FN_PAB_OBT_DSC_COD_TIP_PAG;


   --**********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_BTCOR_OPRCN
   -- Objetivo: Procedimiento que devuelve la bitacora de una operacion
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_BTCOR_OPRCN, PABS_DT_ESTDO_ALMOT
   -- Fecha: 07/12/2016
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_OPE
   -- p_FEC_ISR_OPE
   -- Output:
   -- p_CURSOR_BTCOR
   -- p_ERROR -> Indicador de errores (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_PAB_BTCOR_OPRCN (p_NUM_FOL_OPE    IN     NUMBER,
                                 p_FEC_ISR_OPE    IN     DATE,
                                 p_CURSOR_BTCOR      OUT SYS_REFCURSOR,
                                 p_ERROR             OUT NUMBER)
   IS
      v_NOM_SP   VARCHAR2 (30) := 'SP_PAB_BTCOR_OPRCN';
   BEGIN
      --Buscamos las nominas segun estado que solicita el front.
      OPEN p_CURSOR_BTCOR FOR
           SELECT   FEC_BTC,
                    NUM_FOL,
                    USUARIO,
                    FEC_OPE,
                    USUARIO,
                    ESTADO_INICIAL,
                    ESTADO_FINAL,
                    GLOSA
             FROM   (SELECT   BTCOR.FEC_ISR_BTC AS FEC_BTC,
                              BTCOR.NUM_FOL_OPE AS NUM_FOL,
                              BTCOR.FEC_ISR_OPE AS FEC_OPE,
                              BTCOR.COD_USR AS USUARIO,
                              ESTDO_ORG.DSC_EST_AOS AS ESTADO_INICIAL,
                              ESTDO_FIN.DSC_EST_AOS AS ESTADO_FINAL,
                              BTCOR.DSC_GLB_BTC AS GLOSA
                       FROM   PABS_DT_BTCOR_OPRCN BTCOR,
                              PABS_DT_ESTDO_ALMOT ESTDO_ORG,
                              PABS_DT_ESTDO_ALMOT ESTDO_FIN
                      WHERE       BTCOR.FEC_ISR_OPE = p_FEC_ISR_OPE
                              AND BTCOR.FEC_ISR_BTC > p_FEC_ISR_OPE
                              AND BTCOR.NUM_FOL_OPE = p_NUM_FOL_OPE
                              AND BTCOR.COD_EST_ICO = ESTDO_ORG.COD_EST_AOS
                              AND BTCOR.COD_EST_FIN = ESTDO_FIN.COD_EST_AOS
                     UNION
                     SELECT   BTCOR.FEC_ISR_BTC AS FEC_BTC,
                              BTCOR.NUM_FOL_OPE AS NUM_FOL,
                              BTCOR.FEC_ISR_OPE AS FEC_OPE,
                              BTCOR.COD_USR AS USUARIO,
                              ESTDO_ORG.DSC_EST_AOS AS ESTADO_INICIAL,
                              ESTDO_FIN.DSC_EST_AOS AS ESTADO_FINAL,
                              BTCOR.DSC_GLB_BTC AS GLOSA
                       FROM   PABS_DT_BTCOR_OPRCN_HTRCA BTCOR,
                              PABS_DT_ESTDO_ALMOT ESTDO_ORG,
                              PABS_DT_ESTDO_ALMOT ESTDO_FIN
                      WHERE       BTCOR.FEC_ISR_OPE = p_FEC_ISR_OPE
                              AND BTCOR.NUM_FOL_OPE = p_NUM_FOL_OPE
                              AND BTCOR.COD_EST_ICO = ESTDO_ORG.COD_EST_AOS
                              AND BTCOR.COD_EST_FIN = ESTDO_FIN.COD_EST_AOS
                              AND BTCOR.FEC_ISR_BTC > p_FEC_ISR_OPE)
         ORDER BY   FEC_BTC ASC;

      P_ERROR := PKG_PAB_CONSTANTES.V_OK; --Procedimiento ejecutado de forma correcta
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_BTCOR_OPRCN;

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_GES_PAG_OPE_ITR
   -- Objetivo: Procedimiento actualiza estado de la operación interna a pagada, segun canal de pago
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: N/A
   -- Fecha: 28-03-2019
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_OPE_ORI    -> Numero de Operación interna
   -- p_FEC_ISR_OPE_ORI    -> Fecha de insercion de operación
   -- p_GLS_ESTADO         -> Glosa de estado
   -- p_TIP_ABO_MAN        -> Codigo de tipo Abono
   -- p_NUM_MVNTO      -> Numero devuelto por WS de Vigencia Salida
   -- p_COD_USR            -> Codigo de Usuario que se registrara en bitacora
   -- Output:
   -- p_ERROR              -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_GES_PAG_OPE_ITR (p_NUM_FOL_OPE_ITR   IN     NUMBER,
                                     p_FEC_ISR_OPE_ITR   IN     DATE,
                                     p_COD_EST_AOS       IN     CHAR,
                                     p_GLS_ESTADO        IN     VARCHAR2,
                                     p_NUM_MVNTO         IN     VARCHAR2,
                                     p_COD_USR           IN     CHAR,
                                     p_ERROR                OUT NUMBER)
   IS
      v_NOM_SP            VARCHAR2 (30) := 'SP_PAB_GES_PAG_OPE_ITR';
      ERR_COD_INEX EXCEPTION;
      v_DSC_GLB_BTC       VARCHAR2 (300);
      v_NUM_CTA           PABS_DT_DETLL_OPRCN_INTER.NUM_CTA_BFC_ITR%TYPE;
      v_CANAL_SALIDA      PABS_DT_DETLL_OPRCN_INTER.COD_SIS_SAL_ITR%TYPE;
      v_TIPO_PAGO         PABS_DT_DETLL_OPRCN_INTER.COD_TPO_FPA%TYPE;
      v_COD_EST_AOS       PABS_DT_DETLL_OPRCN_INTER.COD_EST_AOS%TYPE;
      v_COD_EST_AOS_ACT   NUMBER;
   BEGIN
      P_ERROR := PKG_PAB_CONSTANTES.V_OK;

      -- Estado al que se quiere actualizar
      v_COD_EST_AOS :=
         PKG_PAB_UTILITY.FN_PAB_OBT_COD_DSC_EST (
            p_COD_EST_AOS,
            PKG_PAB_CONSTANTES.V_IND_OPE
         );

      BEGIN
         SELECT   OP.NUM_CTA_BFC_ITR, OP.COD_SIS_SAL_ITR, OP.COD_TPO_FPA
           INTO   v_NUM_CTA, v_CANAL_SALIDA, v_TIPO_PAGO
           FROM   PABS_DT_DETLL_OPRCN_INTER OP
          WHERE   OP.NUM_FOL_OPE_ITR = p_NUM_FOL_OPE_ITR
                  AND TO_DATE (OP.FEC_ISR_OPE_ITR, 'DD/MM/YYYY HH:MI:SS') =
                        TO_DATE (p_FEC_ISR_OPE_ITR, 'DD/MM/YYYY HH:MI:SS');
      EXCEPTION
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            p_s_mensaje := err_code || '-' || err_msg;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;

      IF v_COD_EST_AOS = PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAB
      THEN                             -- OPERACION RECHAZADA POR WEB SERVICE.
         BEGIN --MODIFICAR ACTUALZIACIÓN PARA QUE RECIBA TODOS LOS DATOS ACTUALIZADOS
            UPDATE   PABS_DT_DETLL_OPRCN_INTER OP
               SET   OP.GLS_ADC_EST = p_GLS_ESTADO
             WHERE   OP.NUM_FOL_OPE_ITR = p_NUM_FOL_OPE_ITR
                     AND OP.FEC_ISR_OPE_ITR = p_FEC_ISR_OPE_ITR;
         EXCEPTION
            WHEN OTHERS
            THEN
               err_code := SQLCODE;
               err_msg := SUBSTR (SQLERRM, 1, 300);
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
               p_s_mensaje := err_code || '-' || err_msg;
               RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
         END;
      ELSE                                                 -- ABONADA O PAGADA
         IF v_TIPO_PAGO = 'CONPUE'
         THEN                     -- Abono Puente Contable(Modificar códigos.)
            v_DSC_GLB_BTC :=
               'Se abona en cuenta Puente Contable N: ' || p_NUM_MVNTO;
         ELSIF v_TIPO_PAGO = 'VALEVI'
         THEN                                                    -- Vale vista
            v_DSC_GLB_BTC := 'Se crea en Vale vista N: ' || p_NUM_MVNTO;
         ELSIF v_TIPO_PAGO = 'CONVIG'
         THEN                                                 -- Abono Vigente
            v_DSC_GLB_BTC := 'Se da de alta la Vigente N: ' || p_NUM_MVNTO;
         ELSE                                                       --'CTACTE'
            v_DSC_GLB_BTC :=
                  'Se abono en la Cuenta Corriente N: '
               || v_NUM_CTA
               || ', Abono N: '
               || p_NUM_MVNTO;

            --Se inserta registro de tabla de cargos y abonos
            PKG_PAB_UTILITY.SP_PAB_INS_MVNTO_CARGO_ABONO (
               p_NUM_MVNTO,
               p_NUM_FOL_OPE_ITR,
               p_FEC_ISR_OPE_ITR,
               PKG_PAB_CONSTANTES.V_FLG_MOV_ABN,
               p_ERROR
            );

            -- Si abono cta. cte. guardamos la cuenta en NUM_REF_CTB y el movimiento Abono NUM_MVT_CTB


            BEGIN
               UPDATE   PABS_DT_DETLL_OPRCN_INTER OP
                  SET   OP.NUM_REF_CTB_ITR = LPAD (v_NUM_CTA, 12, 0), --0000 + N_cuenta completando 12 caracteres
                        OP.NUM_MVT_CTB_ITR = p_NUM_MVNTO
                WHERE   OP.NUM_FOL_OPE_ITR = p_NUM_FOL_OPE_ITR
                        AND OP.FEC_ISR_OPE_ITR = p_FEC_ISR_OPE_ITR;
            EXCEPTION
               WHEN OTHERS
               THEN
                  err_code := SQLCODE;
                  err_msg := SUBSTR (SQLERRM, 1, 300);
                  PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                      v_NOM_PCK,
                                                      err_msg,
                                                      v_NOM_SP);
                  p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
                  p_s_mensaje := err_code || '-' || err_msg;
                  RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
            END;

            P_ERROR := PKG_PAB_CONSTANTES.V_OK;
         END IF;
      END IF;



      --Actualizamos la operación  interna
      PKG_PAB_OPERACION_INT.Sp_PAB_ACT_OPE_ITR (p_NUM_FOL_OPE_ITR,
                                                p_FEC_ISR_OPE_ITR,
                                                p_COD_EST_AOS,
                                                p_COD_USR,
                                                p_ERROR);

      --MODIFICAR ACTUALZIACIÓN PARA QUE RECIBA TODOS LOS DATOS ACTUALIZADOS !!!!!!!!

      COMMIT;
   EXCEPTION
      WHEN ERR_COD_INEX
      THEN
         err_msg := 'Problemas con gestión Pago Interno';
         P_ERROR := PKG_PAB_CONSTANTES.V_ERROR;
         PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (err_code,
                                             V_NOM_PCK,
                                             err_msg,
                                             V_NOM_SP);
      WHEN DUP_VAL_ON_INDEX
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         P_ERROR := PKG_PAB_CONSTANTES.V_ERROR;
         PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (err_code,
                                             V_NOM_PCK,
                                             err_msg,
                                             V_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_GES_PAG_OPE_ITR;

   /************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ACT_OPE_ITR
   -- Objetivo: PROCEDIMIENTO ALMACENADO QUE ENTREGA LOS TIPOS
   -- DE OPERACION LIBERADAS, SEGUN FILTROS INGRESADOS
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN_INTER
                     PABS_DT_SISTM_ENTRD_SALID
   -- Fecha: 26-03-2019
   -- Autor: Santander - CAH
   -- Input:
   --    p_NUM_FOL_OPE_ITR     Folio operación
   --    p_FEC_ISR_OPE_ITR     Fecha de inserción
   --    p_COD_EST_AOS         Nuevo estado operación
   --    p_COD_USR             Código usuario
   -- Output:
   --        p_ERROR-> Devuelve error
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_ACT_OPE_ITR (p_NUM_FOL_OPE_ITR   IN     NUMBER,
                                 p_FEC_ISR_OPE_ITR   IN     DATE,
                                 p_COD_EST_AOS       IN     CHAR,
                                 p_COD_USR           IN     VARCHAR2,
                                 p_ERROR                OUT NUMBER)
   IS
      v_NOM_SP            VARCHAR2 (30) := 'Sp_PAB_ACT_OPE_ITR';
      v_COD_EST_AOS       PABS_DT_DETLL_OPRCN_INTER.COD_EST_AOS%TYPE;
      v_COD_EST_AOS_ACT   NUMBER;
      v_RESULT            NUMBER;
      v_COD_EST_AOS_OLD   NUMBER;
      v_COD_SIS_SAL_ITR   CHAR (10);
   BEGIN
      -- SE AGREGA VALIDACION QUE SI LA OPERACION ES DE ENTRADA OOFF, SE DEBE ACTUALIZAR A OOFF SOLO EN EL PRIMER INGRESO.
      BEGIN
         SELECT   COD_EST_AOS, COD_SIS_SAL_ITR
           INTO   v_COD_EST_AOS_OLD, v_COD_SIS_SAL_ITR
           FROM   PABS_DT_DETLL_OPRCN_INTER
          WHERE   NUM_FOL_OPE_ITR = p_NUM_FOL_OPE_ITR
                  AND TO_DATE (FEC_ISR_OPE_ITR, 'DD/MM/YYYY HH:MI:SS') =
                        TO_DATE (p_FEC_ISR_OPE_ITR, 'DD/MM/YYYY HH:MI:SS');
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
      END;

      -- Estado al que se quiere actualizar
      v_COD_EST_AOS :=
         PKG_PAB_UTILITY.FN_PAB_OBT_COD_DSC_EST (
            p_COD_EST_AOS,
            PKG_PAB_CONSTANTES.V_IND_OPE
         );

      -- Rescatamos el estado actual de la operación.
      SP_PAB_OBT_EST_ACT_OPE_ITR (p_NUM_FOL_OPE_ITR,
                                  p_FEC_ISR_OPE_ITR,
                                  v_COD_EST_AOS_ACT);

      /*
             IF v_COD_EST_AOS_OLD = PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEING AND v_COD_SIS_SAL_ITR = PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SE_OOFF THEN


                 UPDATE   PABS_DT_DETLL_OPRCN_INTER OI
                  SET   OI.COD_EST_AOS = PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAB,
                  OI.FEC_ACT_OPE_ITR = SYSDATE, -- Fecha actualización
                  OI.COD_TPO_FPA = TRIM(PKG_PAB_CONSTANTES.V_COD_TIP_FPA_CPU)
                 WHERE   OI.NUM_FOL_OPE_ITR = p_NUM_FOL_OPE_ITR
                 AND TO_DATE(OI.FEC_ISR_OPE_ITR,'DD/MM/YYYY HH:MI:SS') = TO_DATE(p_FEC_ISR_OPE_ITR,'DD/MM/YYYY HH:MI:SS');

                           ELSE


                 UPDATE   PABS_DT_DETLL_OPRCN_INTER OI
                  SET   OI.COD_EST_AOS = v_COD_EST_AOS,
                        OI.FEC_ACT_OPE_ITR = SYSDATE -- Fecha actualización
                  WHERE OI.NUM_FOL_OPE_ITR = p_NUM_FOL_OPE_ITR
                  AND TO_DATE(OI.FEC_ISR_OPE_ITR,'DD/MM/YYYY HH:MI:SS') = TO_DATE(p_FEC_ISR_OPE_ITR,'DD/MM/YYYY HH:MI:SS');

              END IF;
      */

      --Actualizamos estado nuevo de la operacion.enviada
      UPDATE   PABS_DT_DETLL_OPRCN_INTER OI
         SET   OI.COD_EST_AOS = v_COD_EST_AOS, OI.FEC_ACT_OPE_ITR = SYSDATE -- Fecha actualización
       WHERE   OI.NUM_FOL_OPE_ITR = p_NUM_FOL_OPE_ITR
               AND OI.FEC_ISR_OPE_ITR = p_FEC_ISR_OPE_ITR;



      --Llamamos al procedimiento de Bitacora
      v_DES_BIT := 'Cambio de estado operación';
      PKG_PAB_TRACKING.Sp_PAB_INS_BIT_OPE (p_NUM_FOL_OPE_ITR,
                                           p_FEC_ISR_OPE_ITR,
                                           p_COD_USR,
                                           v_COD_EST_AOS_ACT,
                                           v_COD_EST_AOS,
                                           v_DES_BIT,
                                           p_ERROR);

      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         RAISE_APPLICATION_ERROR (-20000, err_code || p_s_mensaje);
   END Sp_PAB_ACT_OPE_ITR;


   --***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_COMBO_CANALES
   -- Objetivo: Procedimiento que retorna los canales internos y para Swift
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_SISTM_ENTRD_SALID
   -- Fecha: 27-03-2019
   -- Autor: Santander CAH
   -- Input:
   --  p_FLG_CNL   Flag para indicar tipo de canal, 0 internos, 1 Swift
   -- Output:
   -- p_CURSOR -> RESULTADOS DE CONSULTA.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_PAB_COMBO_CANALES (p_FLG_CNL   IN     NUMBER,
                                   p_CURSOR       OUT SYS_REFCURSOR)
   IS
      v_NOM_SP   VARCHAR2 (30) := 'SP_PAB_COMBO_CANALES';
   BEGIN
      IF p_FLG_CNL = 0
      THEN
         OPEN p_CURSOR FOR
            SELECT   COD_SIS_ENT_SAL AS CODIGO,
                     DSC_SIS_ENT_SAL AS DESCRIPCION
              FROM   PABS_DT_SISTM_ENTRD_SALID
             WHERE       FLG_SIS_ENT_SAL NOT IN (4, 6)
                     AND COD_SIS_ENT_SAL NOT IN ('SWF', 'LBTR', 'CML')
                     AND FLG_VGN = PKG_PAB_CONSTANTES.V_REG_VIG
            --ORDER BY DSC_SIS_ENT_SAL
            UNION
            SELECT   'GBO' AS CODIGO, 'GBO - SUN' AS DESCRIPCION
              FROM   PABS_DT_SISTM_ENTRD_SALID SI
             WHERE   SI.COD_SIS_ENT_SAL =
                        PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SE_MESA;
      ELSE
         OPEN p_CURSOR FOR
            SELECT   COD_SIS_ENT_SAL AS CODIGO,
                     DSC_SIS_ENT_SAL AS DESCRIPCION
              FROM   PABS_DT_SISTM_ENTRD_SALID
             WHERE   FLG_SIS_ENT_SAL = 0
                     AND FLG_VGN = PKG_PAB_CONSTANTES.V_REG_VIG;
      END IF;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_COMBO_CANALES;

   --***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_COMBO_TIPO_FRMPG
   -- Objetivo: Procedimiento que retorna los tipos de pagos
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_TIPO_FRMPG
   -- Fecha: 23-03-2019
   -- Autor: Santander CAH
   -- Input:
   --  p_FLG_MON
   -- Output:
   -- p_CURSOR -> RESULTADOS DE CONSULTA.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_PAB_COMBO_TIPO_FRMPG (p_FLG_EXT   IN     NUMBER,
                                      p_CURSOR       OUT SYS_REFCURSOR)
   IS
      v_NOM_SP   VARCHAR2 (30) := 'SP_PAB_COMBO_TIPO_FRMPG';
   BEGIN
      IF p_FLG_EXT = 0
      THEN                                                   -- Filtro liberar
         OPEN p_CURSOR FOR
            SELECT   TP.COD_TPO_FPA AS CODIGO, TP.DSC_TPO_FPA AS DESCRIPCION
              FROM   PABS_DT_TIPO_FRMPG TP
             WHERE   TP.FLG_VGN = PKG_PAB_CONSTANTES.V_REG_VIG
                     AND TP.COD_TPO_FPA <> 'EXTERN';
      ELSIF p_FLG_EXT = 1
      THEN                                               -- Combo Cambio canal
         OPEN p_CURSOR FOR
            SELECT   TP.COD_TPO_FPA AS CODIGO, TP.DSC_TPO_FPA AS DESCRIPCION
              FROM   PABS_DT_TIPO_FRMPG TP
             WHERE   TP.FLG_VGN = PKG_PAB_CONSTANTES.V_REG_VIG
                     AND TP.COD_TPO_FPA IN
                              (PKG_PAB_CONSTANTES.V_COD_TIP_FPA_CTA,
                               PKG_PAB_CONSTANTES.V_COD_TIP_FPA_CVG,
                               PKG_PAB_CONSTANTES.V_COD_TIP_FPA_CPU);
      ELSE
         OPEN p_CURSOR FOR
            SELECT   TP.COD_TPO_FPA AS CODIGO, TP.DSC_TPO_FPA AS DESCRIPCION
              FROM   PABS_DT_TIPO_FRMPG TP
             WHERE   TP.FLG_VGN = PKG_PAB_CONSTANTES.V_REG_VIG;
      END IF;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_COMBO_TIPO_FRMPG;

   -- ***********************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_COMBO_FLUJO
   -- Objetivo: Procedimiento que retorna flujo de operación, Egreso - Ingreso
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_SISTM_ENTRD_SALID
   -- Fecha: 04-04-2019
   -- Autor: Santander CAH
   -- Input:
   -- Output:
   -- p_CURSOR   -> Cursor con data de salida
   -- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   --          (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- *********************************************************************************************************
   PROCEDURE Sp_PAB_COMBO_FLUJO (p_CURSOR   OUT SYS_REFCURSOR,
                                 p_ERROR    OUT NUMBER)
   IS
      v_NOM_SP   VARCHAR2 (100) := 'Sp_PAB_COMBO_FLUJO ';
   BEGIN
      P_ERROR := PKG_PAB_CONSTANTES.V_OK;

      OPEN p_CURSOR FOR
         SELECT   PKG_PAB_CONSTANTES.V_FLG_EGR AS CODIGO,
                  PKG_PAB_CONSTANTES.V_STR_FLG_EGR AS DESCRIPCION
           FROM   DUAL
         UNION
         SELECT   PKG_PAB_CONSTANTES.V_FLG_ING AS CODIGO,
                  PKG_PAB_CONSTANTES.V_STR_FLG_ING AS DESCRIPCION
           FROM   DUAL;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);

         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_COMBO_FLUJO;

   /************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_INS_OPE_ITR
   -- Objetivo: Procedimiento que inserta la informacion que se encuentra en la nomina
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN_INTER
   -- Fecha: 25/03/2019
   -- Autor: Santander CAH
   -- Input:
   --      p_FEC_ISR_OPE_ITR           Fecha de inserción
   --      p_NUM_FOL_OPE_ITR           Folio de la operación,
   --      p_COD_TPO_OPE_AOS_LQD       Código de liquidación,
   --      p_COD_SIS_ENT_ITR           Código de sistema de entrada,
   --      p_COD_SIS_SAL_ITR           Código de sistema de salida,
   --      p_COD_EST_AOS               Código de estado,
   --      p_COD_TPO_ING               Tipo de ingreso, 0 Egreso, 1 Ingreso
   --      p_NUM_FOL_NMN_ITR           Numero de folio nómina interna,
   --      p_FLG_EGR_ING_ITR           Tipo de ingreso, 0 Egreso, 1 Ingreso,
   --      p_NUM_OPE_SIS_ENT_ITR       Numero de operación de sistema origen,
   --      p_COD_DVI_ITR               Código de divisa,
   --      p_IMP_OPE_ITR               Monto de la operación,
   --      p_NUM_DOC_BFC_ITR           Numero de documento beneficiario,
   --      p_COD_TPD_BFC_ITR           Digito verificador beneficiario,
   --      p_NUM_CTA_BFC_ITR           Numero de cta beneficiario,
   --      p_COD_SUC_ITR               Código de sucursal,
   --      p_NUM_REF_CTB_ITR           Numero de referencia contable,
   --      p_NUM_FOL_OPE_ORG_ITR       Numero de operación interna origen,
   --      p_FEC_ISR_OPE_ORG_ITR       Fecha de inserción operación interna origen,
   --      p_NUM_MVT_CTB_ITR           Numero de movimiento contable,
   --      p_FLG_TPO_REG               Flag tipo de registro,
   --      p_GLS_OPC_ITR               Glosa opcional,
   --      p_NOM_BFC_ITR               Nombre beneficiario,
   --      p_GLS_ADC_EST               Gloza adicional de estado operación
   --      p_NOM_ODN_ITR               Nombre ordenate,
   --      p_NUM_DOC_ODN_ITR           Numero de documento ordenante,
   --      p_COD_TPD_ODN_ITR           Digito verificador ordenante,
   --      p_NUM_CTA_ODN_ITR           Numero de cta ordenante,
   --      p_COD_TPO_FPA               Código de forma de pago,
   --      p_FEC_ACT_OPE_ITR           Fecha de actualización de la operación.
   -- Output:
   --          p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_PAB_INS_OPE_ITR (p_FEC_ISR_OPE_ITR       IN     DATE,
                                 p_NUM_FOL_OPE_ITR       IN     NUMBER,
                                 p_COD_TPO_OPE_AOS_LQD   IN     CHAR,
                                 p_COD_SIS_ENT_ITR       IN     CHAR,
                                 p_COD_SIS_SAL_ITR       IN     CHAR,
                                 p_COD_EST_AOS           IN     CHAR,
                                 p_COD_TPO_ING           IN     NUMBER,
                                 p_NUM_FOL_NMN_ITR       IN     NUMBER,
                                 p_FLG_EGR_ING_ITR       IN     NUMBER,
                                 p_NUM_OPE_SIS_ENT_ITR   IN     NUMBER,
                                 p_COD_DVI_ITR           IN     CHAR,
                                 p_IMP_OPE_ITR           IN     NUMBER,
                                 p_NUM_DOC_BFC_ITR       IN     CHAR,
                                 p_COD_TPD_BFC_ITR       IN     CHAR,
                                 p_NUM_CTA_BFC_ITR       IN     VARCHAR2,
                                 p_COD_SUC_ITR           IN     CHAR,
                                 p_NUM_REF_CTB_ITR       IN     CHAR,
                                 p_NUM_FOL_OPE_ORG_ITR   IN     NUMBER,
                                 p_FEC_ISR_OPE_ORG_ITR   IN     DATE,
                                 p_NUM_MVT_CTB_ITR       IN     CHAR,
                                 p_FLG_TPO_REG           IN     NUMBER,
                                 p_GLS_OPC_ITR           IN     VARCHAR2,
                                 p_NOM_BFC_ITR           IN     CHAR,
                                 p_GLS_ADC_EST           IN     VARCHAR2,
                                 p_NOM_ODN_ITR           IN     CHAR,
                                 p_NUM_DOC_ODN_ITR       IN     CHAR,
                                 p_COD_TPD_ODN_ITR       IN     CHAR,
                                 p_NUM_CTA_ODN_ITR       IN     VARCHAR2,
                                 p_COD_TPO_FPA           IN     CHAR,
                                 p_FEC_ACT_OPE_ITR       IN     DATE,
                                 p_ERROR                    OUT NUMBER)
   IS
      v_NOM_SP            VARCHAR2 (30) := 'Sp_PAB_INS_OPE_ITR';
      v_FEC_ISR_OPE_ITR   DATE;
      v_cod_bco_org       CHAR (11);
      v_FEC_AZA_SWF       DATE;                              --Para actualizar
      v_HOR_AZA_SWF       NUMBER;   --Para actualizar fecha de FEC_ACT_OPE_ITR
   BEGIN
      --Verificamos si viene la fecha de inserción
      IF (NVL (TO_CHAR (p_FEC_ISR_OPE_ITR), '#') = '#')
      THEN
         v_FEC_ISR_OPE_ITR := SYSDATE;
      ELSE
         v_FEC_ISR_OPE_ITR := p_FEC_ISR_OPE_ITR;
      END IF;

      --Si es un egreso
      IF (p_COD_TPO_ING = PKG_PAB_CONSTANTES.V_FLG_EGR)
      THEN
         v_FEC_AZA_SWF := TO_DATE (TO_CHAR (SYSDATE), 'DD-MM-YYY');
         v_HOR_AZA_SWF := TO_CHAR (SYSDATE, 'HH24MI');
      END IF;

      --Separamos el numero de documento y el digito verificador del rut Beneficiario
      --  PKG_PAB_UTILITY.Sp_PAB_DESCOMp_RUT (p_NUM_DOC_BFC, v_NUM_DOC_BFC, v_COD_TPD_BFC);

      --                  --Verificamos si existe la cuenta del beneficiario
      --                PKG_PAB_UTILITY.Sp_PAB_MAN_CTA_BFC_ODN (v_NUM_DOC_BFC,
      --                                                        v_COD_TPD_BFC,
      --                                                        v_NUM_CTA_BFC,
      --                                                        PKG_PAB_CONSTANTES.v_FLG_DCv_NO,
      --                                                        p_COD_BCO_DTN);

      --Insert de operación interna
      INSERT INTO PABS_DT_DETLL_OPRCN_INTER (FEC_ISR_OPE_ITR,
                                             NUM_FOL_OPE_ITR,
                                             COD_TPO_OPE_AOS_LQD,
                                             COD_SIS_ENT_ITR,
                                             COD_SIS_SAL_ITR,
                                             COD_EST_AOS,
                                             COD_TPO_ING,
                                             NUM_FOL_NMN_ITR,
                                             FLG_EGR_ING_ITR,
                                             NUM_OPE_SIS_ENT_ITR,
                                             COD_DVI_ITR,
                                             IMP_OPE_ITR,
                                             NUM_DOC_BFC_ITR,
                                             COD_TPD_BFC_ITR,
                                             NUM_CTA_BFC_ITR,
                                             COD_SUC_ITR,
                                             NUM_REF_CTB_ITR,
                                             NUM_FOL_OPE_ORG_ITR,
                                             FEC_ISR_OPE_ORG_ITR,
                                             NUM_MVT_CTB_ITR,
                                             FLG_TPO_REG,
                                             GLS_OPC_ITR,
                                             NOM_BFC_ITR,
                                             GLS_ADC_EST,
                                             NOM_ODN_ITR,
                                             NUM_DOC_ODN_ITR,
                                             COD_TPD_ODN_ITR,
                                             NUM_CTA_ODN_ITR,
                                             COD_TPO_FPA,
                                             FEC_ACT_OPE_ITR)
        VALUES   (p_FEC_ISR_OPE_ITR,
                  p_NUM_FOL_OPE_ITR,
                  p_COD_TPO_OPE_AOS_LQD,
                  p_COD_SIS_ENT_ITR,
                  p_COD_SIS_SAL_ITR,
                  p_COD_EST_AOS,
                  p_COD_TPO_ING,
                  p_NUM_FOL_NMN_ITR,
                  p_FLG_EGR_ING_ITR,
                  p_NUM_OPE_SIS_ENT_ITR,
                  p_COD_DVI_ITR,
                  p_IMP_OPE_ITR,
                  p_NUM_DOC_BFC_ITR,
                  p_COD_TPD_BFC_ITR,
                  p_NUM_CTA_BFC_ITR,
                  p_COD_SUC_ITR,
                  p_NUM_REF_CTB_ITR,
                  p_NUM_FOL_OPE_ORG_ITR,
                  p_FEC_ISR_OPE_ORG_ITR,
                  p_NUM_MVT_CTB_ITR,
                  p_FLG_TPO_REG,
                  p_GLS_OPC_ITR,
                  p_NOM_BFC_ITR,
                  p_GLS_ADC_EST,
                  p_NOM_ODN_ITR,
                  p_NUM_DOC_ODN_ITR,
                  p_COD_TPD_ODN_ITR,
                  p_NUM_CTA_ODN_ITR,
                  p_COD_TPO_FPA,
                  p_FEC_ACT_OPE_ITR);


      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
      COMMIT;
   EXCEPTION
      WHEN DUP_VAL_ON_INDEX
      THEN
         err_code := SQLCODE;
         err_msg :=
               'Ya existe operación'
            || p_NUM_FOL_OPE_ITR
            || ' '
            || TO_CHAR (v_FEC_ISR_OPE_ITR, 'dd-mm-yyyy hh24:mi:ss');
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);

         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_INS_OPE_ITR;

   --***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_COMBO_TIPO_OPE_LQD
   -- Objetivo: Procedimiento que retorna los tipos de operación de liquidación
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_TIPO_OPRCN_ALMOT_LIQDC
   -- Fecha: 23-03-2019
   -- Autor: Santander CAH
   -- Input:
   --
   -- Output:
   -- p_CURSOR -> RESULTADOS DE CONSULTA.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_COMBO_TIPO_OPE_LQD (p_CURSOR OUT SYS_REFCURSOR)
   IS
      v_NOM_SP   VARCHAR2 (30) := 'Sp_PAB_COMBO_TIPO_OPE_LQD';
   BEGIN
      OPEN p_CURSOR FOR
           SELECT   TQ.COD_TPO_OPE_AOS_LQD AS CODIGO,
                    TQ.DSC_TPO_OPE_AOS_LQD AS DESCRIPCION
             FROM   PABS_DT_TIPO_OPRCN_ALMOT_LIQDC TQ
            WHERE   TQ.FLG_VGN = PKG_PAB_CONSTANTES.V_REG_VIG
         ORDER BY   TQ.DSC_TPO_OPE_AOS_LQD ASC;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_COMBO_TIPO_OPE_LQD;
END PKG_PAB_OPERACION_INT;