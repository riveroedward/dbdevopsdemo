CREATE OR REPLACE PACKAGE BODY         PKG_PAB_CIERRE
IS
   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_GESTION_CIERRE
   -- Objetivo: Gestiona el cierre del sistema orquestando la llamada a los procedimientos especificos
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas:
   -- Fecha: 14/09/2016
   -- Autor: Santander
   -- Input:
   -- p_FEC_PCS -> Fecha Proceso.
   -- Output: p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   --         P_ERR_MSG -> Glosa que indica el error
   -- Retorno: N/A.
   -- Observaciones: 14/09/2016, Creacion Procedimiento.
   --***********************************************************************************************
   PROCEDURE Sp_PAB_GESTION_CIERRE (p_FEC_PCS   IN     VARCHAR2,
                                    p_ERROR        OUT NUMBER,
                                    P_ERR_MSG      OUT VARCHAR2)
   IS
      v_NOM_SP   VARCHAR2 (30) := 'Sp_PAB_GESTION_CIERRE';
      ERROR_PROCEDIMIENTO exception;
   BEGIN
      -- INICIALIZACION DE VARIABLES Y PARAMETROS
      p_ERROR := PKG_PAB_CONSTANTES.V_OK;

      --Inicia Logica Gestion, Traza
      p_GLS_DET_PCS := 'Iniciando proceso cierre del sistema';
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_CAB_PROCESO (p_COD_TPO_PSC,
                                                   p_FEC_ICO_PCS,
                                                   p_NUM_TCK_PCS,
                                                   p_ERROR);

      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);

      /****************TRAZA PROCESOS*********************/
      -- Eliminacion Log Base de datos, Traza

      --Subproceso
      p_COD_UNI_PSC := 'BITACORA PROCESO';

      --Inicio
      P_GLS_DET_PCS :=
         p_COD_UNI_PSC || ': Iniciando borrado de bitacora proceso';
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);

      --Llamado a proceso
      PKG_PAB_CIERRE.Sp_PAB_ELI_BITACORA_PROCESO (P_FEC_PCS, P_ERROR);

      --Valido la ejecución correcta del procedimiento.
      IF p_ERROR = PKG_PAB_CONSTANTES.V_ERROR
      THEN
         RAISE ERROR_PROCEDIMIENTO;
      END IF;

      --Fin
      P_GLS_DET_PCS := p_COD_UNI_PSC || ': Fin borrado de bitacora proceso';
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);

      /****************FIN PROCESOS*********************/

      /****************LOG BASE DE DATOS*********************/
      -- Eliminacion Log Base de datos, Traza

      --Subproceso
      p_COD_UNI_PSC := 'BITACORA LOG BASE';

      --Inicio
      P_GLS_DET_PCS :=
         p_COD_UNI_PSC || ': Iniciando borrado de log de base de datos';
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);

      --Llamado a proceso
      PKG_PAB_CIERRE.Sp_PAB_ELI_LOG_CIERRE (P_FEC_PCS, P_ERROR);

      --Valido la ejecución correcta del procedimiento.
      IF p_ERROR = PKG_PAB_CONSTANTES.V_ERROR
      THEN
         RAISE ERROR_PROCEDIMIENTO;
      END IF;

      --Fin
      P_GLS_DET_PCS :=
         p_COD_UNI_PSC || ': Fin borrado de log de base de datos';
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);

      /****************FIN LOG BASE DE DATOS*********************/

      /****************CAMPOS INVALIDOS NOMINA EXTERNA******************/
      -- Eliminacion Log Base de datos, Traza

      --Subproceso
      p_COD_UNI_PSC := 'ELIMINA NOMINA EXTERNA';

      --Inicio
      P_GLS_DET_PCS :=
         p_COD_UNI_PSC
         || ': Iniciando borrado de validaciones nomina externa';
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);

      --Llamado a proceso
      PKG_PAB_CIERRE.Sp_PAB_ELI_VAL_NOM_EXT (P_FEC_PCS, P_ERROR);

      --Valido la ejecución correcta del procedimiento.
      IF p_ERROR = PKG_PAB_CONSTANTES.V_ERROR
      THEN
         RAISE ERROR_PROCEDIMIENTO;
      END IF;

      --Fin
      P_GLS_DET_PCS :=
         p_COD_UNI_PSC || ': Fin borrado de validaciones nomina externa';
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);

      /****************CAMPOS INVALIDOS NOMINA EXTERNA****************/


      /****************CONTABILIDAD*********************/
      -- Eliminacion contabilidad antigua

      --Subproceso
      p_COD_UNI_PSC := 'ELIMINA CONTA';

      --Inicio
      P_GLS_DET_PCS :=
         p_COD_UNI_PSC
         || ': Iniciando borrado de contabilidad de días anteriores';
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);

      --Llamado a proceso
      PKG_PAB_CIERRE.Sp_PAB_ELI_CONTA (P_FEC_PCS, P_ERROR);

      --Valido la ejecución correcta del procedimiento.
      IF p_ERROR = PKG_PAB_CONSTANTES.V_ERROR
      THEN
         RAISE ERROR_PROCEDIMIENTO;
      END IF;

      --Fin
      P_GLS_DET_PCS :=
         p_COD_UNI_PSC || ': Fin borrado de contabilidad de días anteriores';
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);

      /****************FIN CONTABILIDAD*********************/

      /****************MOVIMIENTOS FILIALES*********************/
      -- Eliminacion Log Base de datos, Traza

      --Subproceso
      p_COD_UNI_PSC := 'PROYECCION FILIALES';

      --Inicio
      P_GLS_DET_PCS :=
         p_COD_UNI_PSC
         || ': Iniciando borrado de los mov. proyección filiales';
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);

      --Llamado a proceso
      PKG_PAB_CIERRE.Sp_PAB_ELI_MOV_PRO_FIL (P_FEC_PCS, P_ERROR);

      --Valido la ejecución correcta del procedimiento.
      IF p_ERROR = PKG_PAB_CONSTANTES.V_ERROR
      THEN
         RAISE ERROR_PROCEDIMIENTO;
      END IF;

      --Fin
      P_GLS_DET_PCS :=
         p_COD_UNI_PSC || ': Fin borrado de los mov. proyección filiales';
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);

      /****************FIN MOVIMIENTOS FILIALES*********************/

      /****************NOTIFICACIONES*********************/
      -- Eliminacion registros de tabla notificaciones

      --Subproceso
      p_COD_UNI_PSC := 'NOTIFICACIONES';

      --Inicio
      P_GLS_DET_PCS :=
         p_COD_UNI_PSC
         || ': Iniciando borrado de los registros de notificaciones';
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);

      --Llamado a proceso
      PKG_PAB_CIERRE.SP_PAB_ELI_NOTIFICACION (P_FEC_PCS, P_ERROR);

      --Valido la ejecución correcta del procedimiento.
      IF p_ERROR = PKG_PAB_CONSTANTES.V_ERROR
      THEN
         RAISE ERROR_PROCEDIMIENTO;
      END IF;

      --Fin
      P_GLS_DET_PCS :=
         p_COD_UNI_PSC || ': Fin borrado de los registros de notificaciones';
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);

      /****************FIN NOTIFICACIONES*********************/

      /****************DETALLE DCV*********************/
      -- Eliminacion registros de tabla detalle dcv

      --Subproceso
      p_COD_UNI_PSC := 'DETALLE OPERACION DCV';

      --Inicio
      P_GLS_DET_PCS :=
         p_COD_UNI_PSC || ': Iniciando borrado de los registros de DCV';
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);

      --Llamado a proceso
      PKG_PAB_CIERRE.Sp_PAB_ELI_DET_OPERACION_DCV (P_FEC_PCS, P_ERROR);

      --Valido la ejecución correcta del procedimiento.
      IF p_ERROR = PKG_PAB_CONSTANTES.V_ERROR
      THEN
         RAISE ERROR_PROCEDIMIENTO;
      END IF;

      --Fin
      P_GLS_DET_PCS :=
         p_COD_UNI_PSC || ': Fin borrado de los registros de DCV';
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);

      /****************FIN DETALLE DCV**********************/

      /****************SALDO BILATERALES*********************/
      -- Eliminacion registros de tabla Saldos bilaterales

      --Subproceso
      p_COD_UNI_PSC := 'SALDOS BILATERALES';

      --Inicio
      P_GLS_DET_PCS :=
         p_COD_UNI_PSC
         || ': Iniciando borrado de los registros de saldos bilaterales';
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);

      --Llamado a proceso
      PKG_PAB_CIERRE.Sp_PAB_ELI_SALDO_BILAT (p_FEC_PCS, p_ERROR);

      --Valido la ejecución correcta del procedimiento.
      IF p_ERROR = PKG_PAB_CONSTANTES.V_ERROR
      THEN
         RAISE ERROR_PROCEDIMIENTO;
      END IF;

      --Fin
      P_GLS_DET_PCS :=
         p_COD_UNI_PSC
         || ': Fin borrado de los registros de saldos bilaterales';
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);

      /****************FIN SALDO BILATERALES*********************/

      /****************SALDO AREAS*********************/
      -- Eliminacion registros de tabla Saldos bilaterales

      --Subproceso
      p_COD_UNI_PSC := 'SALDOS AREAS';

      --Inicio
      P_GLS_DET_PCS :=
         p_COD_UNI_PSC
         || ': Iniciando borrado de los registros de saldos áreas';
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);

      --Llamado a proceso
      PKG_PAB_CIERRE.Sp_PAB_ELI_SALDO_AREA (p_FEC_PCS, p_ERROR);

      --Valido la ejecución correcta del procedimiento.
      IF p_ERROR = PKG_PAB_CONSTANTES.V_ERROR
      THEN
         RAISE ERROR_PROCEDIMIENTO;
      END IF;

      --Fin
      P_GLS_DET_PCS :=
         p_COD_UNI_PSC || ': Fin borrado de los registros de saldos áreas';
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);

      /****************FIN SALDO AREAS*********************/

      /*******************ACTUALIZACIÓN 900/910*********************/
      -- Actualización de canales de entrada y salida.

      --Subproceso
      p_COD_UNI_PSC := 'Actualización 900/910';

      --Inicio
      P_GLS_DET_PCS :=
         p_COD_UNI_PSC
         || ': Inicio actualización para canales de entrada y salida MT 900/910';
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);

      --Llamado a proceso
      PKG_PAB_CIERRE.Sp_PAB_ACT_900_910 (p_FEC_PCS, p_ERROR);

      --Valido la ejecución correcta del procedimiento.
      IF p_ERROR = PKG_PAB_CONSTANTES.V_ERROR
      THEN
         RAISE ERROR_PROCEDIMIENTO;
      END IF;

      --Fin
      P_GLS_DET_PCS :=
         p_COD_UNI_PSC
         || ': Fin actualización para canales de entrada y salida MT 900/910';
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);

      /*******************FIN ACTUALIZACIÓN 900/910*********************/

      /****************900 / 910*********************/
      -- Eliminacion registros de tabla Saldos bilaterales

      --Subproceso
      p_COD_UNI_PSC := '900/910';

      --Inicio
      P_GLS_DET_PCS :=
         p_COD_UNI_PSC || ': Iniciando borrado de los registros de 900/910';
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);

      --Llamado a proceso
      PKG_PAB_CIERRE.Sp_PAB_ELI_900_910 (p_FEC_PCS, p_ERROR);

      --Valido la ejecución correcta del procedimiento.
      IF p_ERROR = PKG_PAB_CONSTANTES.V_ERROR
      THEN
         RAISE ERROR_PROCEDIMIENTO;
      END IF;

      --Fin
      P_GLS_DET_PCS :=
         p_COD_UNI_PSC || ': Fin borrado de los registros de 900/910';
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);

      /*******************FIN 900 / 910*********************/

      /****************REGISTRO DE CARGO Y ABONO*********************/
      -- Eliminacion registros de tabla Saldos bilaterales

      --Subproceso
      p_COD_UNI_PSC := 'REGISTRO CARGO Y ABONO';

      --Inicio
      P_GLS_DET_PCS :=
         p_COD_UNI_PSC
         || ': Iniciando borrado de los registros de cargo y abono';
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);

      --Llamado a proceso
      PKG_PAB_CIERRE.Sp_PAB_ELI_RGTRO_CARGO_ABONO (p_FEC_PCS, p_ERROR);

      --Valido la ejecución correcta del procedimiento.
      IF p_ERROR = PKG_PAB_CONSTANTES.V_ERROR
      THEN
         RAISE ERROR_PROCEDIMIENTO;
      END IF;

      --Fin
      P_GLS_DET_PCS :=
         p_COD_UNI_PSC || ': Fin borrado de los registros de cargo y abono';
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);

      /****************FIN DE REGISTRO CARGO Y ABONO*********************/

      /****************ACTUALIZACION DE SALDOS MX********************/

      --Subproceso
      p_COD_UNI_PSC := 'ACTUALIZACION DE SALDOS MX';

      --Inicio
      P_GLS_DET_PCS :=
         p_COD_UNI_PSC
         || ': Iniciando borrado de los registros de movimientos caja';
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);

      --Llamado a proceso
      PKG_PAB_CIERRE.Sp_PAB_ACT_SLDO_MX (p_ERROR);

      --Valido la ejecución correcta del procedimiento.
      IF p_ERROR = PKG_PAB_CONSTANTES.V_ERROR
      THEN
         RAISE ERROR_PROCEDIMIENTO;
      END IF;

      --Fin
      P_GLS_DET_PCS :=
         p_COD_UNI_PSC
         || ': Fin borrado de los registros de movimientos caja';
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);

      /****************FIN ACTUALIZACION DE SALDOS MX********************/

      /****************ELIMINACION CARTOLAS********************/

      --Subproceso
      p_COD_UNI_PSC := 'ELIMINACION CARTOLAS';

      --Inicio
      P_GLS_DET_PCS :=
         p_COD_UNI_PSC || ': Iniciando borrado de los registros de catolas';
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);

      --Llamado a proceso
      PKG_PAB_CIERRE.Sp_PAB_ELI_CART (p_FEC_PCS, p_ERROR);

      --Valido la ejecución correcta del procedimiento.
      IF p_ERROR = PKG_PAB_CONSTANTES.V_ERROR
      THEN
         RAISE ERROR_PROCEDIMIENTO;
      END IF;

      --Fin
      P_GLS_DET_PCS :=
         p_COD_UNI_PSC || ': Fin borrado de los registros de cartolas';
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);

      /****************FIN ELIMINACION CARTOLAS*********************/

      /****************ELIMINACION MOVIIENTOS CAJA********************/

      --Subproceso
      p_COD_UNI_PSC := 'ELIMINACION MOVIMIENTOS CAJA';

      --Inicio
      P_GLS_DET_PCS :=
         p_COD_UNI_PSC
         || ': Iniciando borrado de los registros de movimientos caja';
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);

      --Llamado a proceso
      PKG_PAB_CIERRE.Sp_PAB_ELI_MOV_CAJA (p_FEC_PCS, p_ERROR);

      --Valido la ejecución correcta del procedimiento.
      IF p_ERROR = PKG_PAB_CONSTANTES.V_ERROR
      THEN
         RAISE ERROR_PROCEDIMIENTO;
      END IF;

      --Fin
      P_GLS_DET_PCS :=
         p_COD_UNI_PSC
         || ': Fin borrado de los registros de movimientos caja';
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);

      /****************FIN ELIMINACION MOVIMIENTOS CAJAS*********************/

      /**************NOMINAS***********************/

      --Eliminacion Nominas y operaciones nominas sin finalizar, Traza 2

      --Subproceso
      p_COD_UNI_PSC := 'ESTADO NOMINAS';

      --Inicio
      p_GLS_DET_PCS :=
         p_COD_UNI_PSC
         || ': Inicio cambio estado de nominas y sus operaciones que no fueron gestionadas';
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);

      --Llamado a proceso
      PKG_PAB_CIERRE.Sp_PAB_ACT_NOMINAS_CIERRE (p_FEC_PCS, P_ERROR);

      --Valido la ejecución correcta del procedimiento.
      IF p_ERROR = PKG_PAB_CONSTANTES.V_ERROR
      THEN
         RAISE ERROR_PROCEDIMIENTO;
      END IF;

      --Fin
      P_GLS_DET_PCS :=
         p_COD_UNI_PSC || ': Fin cambio estado de nominas y operaciones';
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);

      /**************FIN NOMINAS***********************/

      /**************OPERACIONES***********************/

      --Subproceso
      p_COD_UNI_PSC := 'ESTADO OPERACIONES';

      --Inicio
      p_GLS_DET_PCS :=
         p_COD_UNI_PSC
         || ': Inicio cambio estado de operaciones que no fueron gestionadas';
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);

      --Llamado a proceso
      PKG_PAB_CIERRE.Sp_PAB_ACT_OPERACIONES_CIERRE (p_FEC_PCS, P_ERROR);

      --Valido la ejecución correcta del procedimiento.
      IF p_ERROR = PKG_PAB_CONSTANTES.V_ERROR
      THEN
         RAISE ERROR_PROCEDIMIENTO;
      END IF;

      --Fin
      P_GLS_DET_PCS := p_COD_UNI_PSC || ': Fin cambio estado de operaciones';
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);

      /**************FIN OPERACIONES***********************/

      /**************HISTORIFICACION - NOMINA***********************/

      --Subproceso
      p_COD_UNI_PSC := 'HISTORI NOMINA';

      --Inicio
      P_GLS_DET_PCS :=
         p_COD_UNI_PSC || ': Inicio proceso de Historificación Nomina';
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);

      --Llamado a proceso
      PKG_PAB_CIERRE.Sp_PAB_HIS_NOMINAS_CIERRE (p_FEC_PCS, p_ERROR);

      --Valido la ejecución correcta del procedimiento.
      IF p_ERROR = PKG_PAB_CONSTANTES.V_ERROR
      THEN
         RAISE ERROR_PROCEDIMIENTO;
      END IF;

      --Fin
      P_GLS_DET_PCS :=
         p_COD_UNI_PSC || ': Fin proceso de Historificación Nomina';
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);
      /**************FIN HISTORIFICACION NOMINA***********************/

      /**************HISTORIFICACION - OPERACIONES***********************/
      --Subproceso
      p_COD_UNI_PSC := 'HISTORI OPERACION';

      --Inicio
      P_GLS_DET_PCS :=
         p_COD_UNI_PSC || ': Inicio proceso de Historificación operaciones';
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);

      --Llamado a proceso
      PKG_PAB_CIERRE.Sp_PAB_HIS_OPERACIONES_CIERRE (P_FEC_PCS, P_ERROR);

      --Valido la ejecución correcta del procedimiento.
      IF p_ERROR = PKG_PAB_CONSTANTES.V_ERROR
      THEN
         RAISE ERROR_PROCEDIMIENTO;
      END IF;

      --Fin
      P_GLS_DET_PCS :=
         p_COD_UNI_PSC || ': Fin proceso de Historificación operaciones';
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);
      /**************FIN HISTORIFICACION - OPERACIONES***********************/

      /**************HISTORIFICACION - MENSAJERIA***********************/
      --Subproceso
      p_COD_UNI_PSC := 'HISTORI PROTOCOLO';

      --Inicio
      P_GLS_DET_PCS :=
         p_COD_UNI_PSC || ': Inicio proceso de Historificación de Mensajeria';
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);

      --Llamado a proceso
      PKG_PAB_CIERRE.Sp_PAB_HIS_MENSAJERIA_CIERRE (P_FEC_PCS, P_ERROR);

      --Valido la ejecución correcta del procedimiento.
      IF p_ERROR = PKG_PAB_CONSTANTES.V_ERROR
      THEN
         RAISE ERROR_PROCEDIMIENTO;
      END IF;

      --Fin
      P_GLS_DET_PCS :=
         p_COD_UNI_PSC || ': Fin proceso de Historificación Mensajeria';
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);

      /**************FIN HISTORIFICACION - MENSAJERIA***********************/

      /**************HISTORIFICACION - DETALLE MENSAJERIA***********************/
      --Subproceso
      p_COD_UNI_PSC := 'HISTORI DETALLE PROTOCOLO';

      --Inicio
      P_GLS_DET_PCS :=
         p_COD_UNI_PSC
         || ': Inicio proceso de Historificación de detalle Mensajeria detalle';
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);

      --Llamado a proceso
      PKG_PAB_CIERRE.Sp_PAB_HIS_DET_MNSJE_CIERRE (P_FEC_PCS, P_ERROR);

      --Valido la ejecución correcta del procedimiento.
      IF p_ERROR = PKG_PAB_CONSTANTES.V_ERROR
      THEN
         RAISE ERROR_PROCEDIMIENTO;
      END IF;

      --Fin
      P_GLS_DET_PCS :=
         p_COD_UNI_PSC
         || ': Fin proceso de Historificación de detalle Mensajeria detalle';
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);
      /**************HISTORIFICACION - DETALLE MENSAJERIA***********************/

      /**************BITACORA DE OPERACIONES***********************/
      --Subproceso
      p_COD_UNI_PSC := 'HISTORI BITACORA OPERACION';

      --Inicio
      P_GLS_DET_PCS :=
         p_COD_UNI_PSC
         || ': Inicio proceso de Historificacion de bitacora de operaciones';
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);

      --Llamado a proceso
      PKG_PAB_CIERRE.Sp_PAB_HIS_BITACORA_CIERRE (P_FEC_PCS, P_ERROR);

      --Valido la ejecución correcta del procedimiento.
      IF p_ERROR = PKG_PAB_CONSTANTES.V_ERROR
      THEN
         RAISE ERROR_PROCEDIMIENTO;
      END IF;

      --Fin
      P_GLS_DET_PCS :=
         p_COD_UNI_PSC
         || ': Fin proceso de Historificación bitacora de operaciones';
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);
      /**************FIN BITACORA DE OPERACIONES***********************/


      /****************PAGOS ACUMULADOS EGRESOS*********************/
      -- Eliminacion registros de tabla notificaciones

      --Subproceso
      p_COD_UNI_PSC := 'EGRESOS REALIZADOS';

      --Inicio
      P_GLS_DET_PCS :=
         p_COD_UNI_PSC
         || ': Iniciando borrado de los registros de pagos acumulados';
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);

      --Llamado a proceso
      PKG_PAB_CIERRE.SP_PAB_ELI_PAGOS_ACMDO (P_FEC_PCS, P_ERROR);

      --Valido la ejecución correcta del procedimiento.
      IF p_ERROR = PKG_PAB_CONSTANTES.V_ERROR
      THEN
         RAISE ERROR_PROCEDIMIENTO;
      END IF;

      --Fin
      P_GLS_DET_PCS :=
         p_COD_UNI_PSC
         || ': Fin borrado de los registros de pagos acumulados';
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);

      /****************FIN PAGOS ACUMULADOS EGRESOS*********************/

      /**************SALDOS MAX - MIN*********************************/

      --Subproceso
      p_COD_UNI_PSC := 'ACT SALDOS MAX - MIN DIA';

      --Inicio
      p_GLS_DET_PCS :=
         p_COD_UNI_PSC
         || ': Inicio de actualización saldos máx y mín del dia';
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);
      --Llamado a proceso
      PKG_PAB_CIERRE.SP_PAB_ACT_SALDOS_DARIO (p_FEC_PCS, P_ERROR);

      --Valido la ejecución correcta del procedimiento.
      IF p_ERROR = PKG_PAB_CONSTANTES.V_ERROR
      THEN
         RAISE ERROR_PROCEDIMIENTO;
      END IF;

      --Fin
      P_GLS_DET_PCS :=
         p_COD_UNI_PSC || ': Fin de actualización saldos máx - mín.';
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);
      /**************FIN SALDOS MAX - MIN****************************/


      --Fin Proceso
      p_GLS_DET_PCS := 'Fin proceso cierre del sistema';
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);
      PKG_PAB_TRACKING.Sp_PAB_UPD_BIT_CAB_PROCESO (p_FEC_ICO_PCS,
                                                   p_COD_TPO_PSC,
                                                   p_NUM_TCK_PCS,
                                                   p_ERROR,
                                                   p_ERROR);

      COMMIT;
   EXCEPTION
      WHEN ERROR_PROCEDIMIENTO
      THEN
         err_code := SQLCODE;
         err_msg := 'ERROR_PROCEDIMIENTO: ' || SUBSTR (SQLERRM, 1, 300);
         P_ERR_MSG := err_msg;
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         PKG_PAB_TRACKING.Sp_PAB_UPD_BIT_CAB_PROCESO (
            p_FEC_ICO_PCS,
            p_COD_TPO_PSC,
            p_NUM_TCK_PCS,
            PKG_PAB_CONSTANTES.V_ERROR,
            p_ERROR
         );
      WHEN DUP_VAL_ON_INDEX
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         P_ERR_MSG := err_msg;
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         PKG_PAB_TRACKING.Sp_PAB_UPD_BIT_CAB_PROCESO (
            p_FEC_ICO_PCS,
            p_COD_TPO_PSC,
            p_NUM_TCK_PCS,
            PKG_PAB_CONSTANTES.V_ERROR,
            p_ERROR
         );
      WHEN OTHERS
      THEN
         ROLLBACK;
         err_code := SQLCODE;
         err_msg := 'OTHERS: ' || SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         PKG_PAB_TRACKING.Sp_PAB_UPD_BIT_CAB_PROCESO (
            p_FEC_ICO_PCS,
            p_COD_TPO_PSC,
            p_NUM_TCK_PCS,
            PKG_PAB_CONSTANTES.V_ERROR,
            p_ERROR
         );
         P_ERR_MSG := err_msg;
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_GESTION_CIERRE;

   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ACTUALIZA_NOMINAS_CIERRE
   -- Objetivo: Al cierre del sistema busca nominas que no fueron autorizadas por el usuario y cambia su estado a eliminadas
   --          Estado Nominas buscadas: Valida, Invalida, Cursada, Rechazada, Duplicada
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_CBCRA_NOMNA
   -- Fecha: 14/09/2016
   -- Autor: Santander
   -- Input:
   -- p_FEC_PCS -> Fecha Proceso.
   -- Output: p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: 14/09/2016, Creacion Procedimiento.
   --***********************************************************************************************
   PROCEDURE Sp_PAB_ACT_NOMINAS_CIERRE (p_FEC_PCS   IN     VARCHAR2,
                                        p_ERROR        OUT NUMBER)
   IS
      v_NOM_SP            VARCHAR2 (30) := 'Sp_PAB_ACT_NOMINAS_CIERRE';
      --Declara lista de folio utilizados por PKG_PAB_NOMINA
      p_NUM_FOL_NMN       PKG_PAB_NOMINA.T_NUM_FOL_NMN;
      v_CONT              NUMBER;
      p_COD_USR           VARCHAR2 (11) := 'CIERRE AM';

      -- ********************************************************************************
      --      Declaracion Cursor C_CONSULTA_NOMINA
      -- ********************************************************************************
      CURSOR C_CONSULTA_NOMINA
      IS
         SELECT   CAB.NUM_FOL_NMN, CAB.COD_EST_AOS, EST.DSC_EST_AOS DES_EST
           FROM   PABS_DT_CBCRA_NOMNA CAB, PABS_DT_ESTDO_ALMOT EST
          WHERE   CAB.COD_EST_AOS IN
                        (PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNVAL,
                         PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNCUR,
                         PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNINV,
                         PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNREC,
                         PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNDUP)
                  AND CAB.FEC_ISR_NMN BETWEEN TO_DATE (
                                                 p_FEC_PCS || ' 00:00:00',
                                                 'DD-MM-YYYY hh24:mi:ss'
                                              )
                                          AND  TO_DATE (
                                                  p_FEC_PCS || ' 23:59:59',
                                                  'DD-MM-YYYY hh24:mi:ss'
                                               )
                  AND CAB.COD_EST_AOS = EST.COD_EST_AOS;

      R_CONSULTA_NOMINA   C_CONSULTA_NOMINA%ROWTYPE;
   -- *********************************************************************************************

   BEGIN
      v_CONT := 0;

      --Eliminacion de registros Bitacora detalle Proceso
      p_GLS_DET_PCS :=
            p_COD_UNI_PSC
         || ': Se eliminara las nominas no gestionadas con fecha  :'
         || p_FEC_PCS;
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);

      --Se carga cursor con las nominas a eliminar
      BEGIN
         OPEN C_CONSULTA_NOMINA;
      EXCEPTION
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);

            --Verificamos si el cursor se encuentra abierto en caso de error.
            IF (C_CONSULTA_NOMINA%ISOPEN)
            THEN
               CLOSE C_CONSULTA_NOMINA;
            END IF;

            RAISE_APPLICATION_ERROR (-20000, err_msg);
      END;

      BEGIN
         FETCH C_CONSULTA_NOMINA INTO   R_CONSULTA_NOMINA;
      EXCEPTION
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);

            --Verificamos si el cursor se encuentra abierto en caso de error.
            IF (C_CONSULTA_NOMINA%ISOPEN)
            THEN
               CLOSE C_CONSULTA_NOMINA;
            END IF;

            RAISE_APPLICATION_ERROR (-20000, err_msg);
      END;

      --Recorremos las nominas a eliminar
      WHILE C_CONSULTA_NOMINA%FOUND
      LOOP
         IF v_CONT = 0
         THEN
            v_CONT := 1;
         END IF;

         p_NUM_FOL_NMN (v_CONT) := R_CONSULTA_NOMINA.NUM_FOL_NMN;

         p_GLS_DET_PCS :=
               p_COD_UNI_PSC
            || ': Cambiando estado de nomina '
            || R_CONSULTA_NOMINA.NUM_FOL_NMN
            || ' a eliminada -> Por estar en estado:'
            || R_CONSULTA_NOMINA.DES_EST;
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                      p_FEC_ICO_PCS,
                                                      p_COD_ERR_BIB,
                                                      p_COD_TPO_PSC,
                                                      p_ERROR);

         FETCH C_CONSULTA_NOMINA INTO   R_CONSULTA_NOMINA;

         v_CONT := v_CONT + 1;
      END LOOP;

      CLOSE C_CONSULTA_NOMINA;

      IF v_CONT > 0
      THEN
         PKG_PAB_NOMINA.Sp_PAB_GES_CAM_EST_NOMINA (
            p_NUM_FOL_NMN,
            PKG_PAB_CONSTANTES.V_STR_EST_AOS_NMNELI,
            p_COD_USR,
            PKG_PAB_CONSTANTES.V_GLS_EST_CIE_NMN,
            p_ERROR
         );
      END IF;

      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   --Fin Logica Gestion
   EXCEPTION
      WHEN DUP_VAL_ON_INDEX
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (
            p_COD_UNI_PSC || ': ' || err_msg,
            p_FEC_ICO_PCS,
            p_COD_ERR_BIB,
            p_COD_TPO_PSC,
            p_ERROR
         );
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;

         --Verificamos si el cursor se encuentra abierto en caso de error.
         IF (C_CONSULTA_NOMINA%ISOPEN)
         THEN
            CLOSE C_CONSULTA_NOMINA;
         END IF;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (
            p_COD_UNI_PSC || ': ' || err_msg,
            p_FEC_ICO_PCS,
            p_COD_ERR_BIB,
            p_COD_TPO_PSC,
            p_ERROR
         );
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;

         --Verificamos si el cursor se encuentra abierto en caso de error.
         IF (C_CONSULTA_NOMINA%ISOPEN)
         THEN
            CLOSE C_CONSULTA_NOMINA;
         END IF;

         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_ACT_NOMINAS_CIERRE;

   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ACT_OPERACIONES_CIERRE
   -- Objetivo: Al cierre del sistema busca operaciones que no fueron pagadas por el usuario y cambia su estado a eliminadas
   --          Estado Operaciones buscadas: Valida, Invalida, Cursada, Rechazada, Duplicada
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN
   -- Fecha: 26/09/2016
   -- Autor: Santander
   -- Input:
   -- p_FEC_PCS -> Fecha Proceso.
   -- Output: p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: 14/09/2016, Creacion Procedimiento.
   --***********************************************************************************************
   PROCEDURE Sp_PAB_ACT_OPERACIONES_CIERRE (p_FEC_PCS   IN     VARCHAR2,
                                            p_ERROR        OUT NUMBER)
   IS
      v_NOM_SP               VARCHAR2 (30) := 'Sp_PAB_ACT_OPERACIONES_CIERRE';
      v_CONT                 NUMBER := 0;
      p_COD_USR              VARCHAR2 (11) := 'CIERRE AM';

      -- Declara cursor para almacenar nominas

      -- ********************************************************************************
      --      Declaracion Cursor C_CONSULTA_OPERACION
      -- ********************************************************************************
      CURSOR C_CONSULTA_OPERACION
      IS
         SELECT   OPE.FEC_ISR_OPE,
                  OPE.NUM_FOL_OPE,
                  OPE.NUM_FOL_NMN,
                  OPE.NUM_REF_SWF,
                  OPE.FEC_VTA,
                  EST.DSC_EST_AOS DES_EST
           FROM   PABS_DT_DETLL_OPRCN OPE, PABS_DT_ESTDO_ALMOT EST
          WHERE   OPE.COD_EST_AOS IN
                        (PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT,
                         PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPVI,
                         PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEINGAR,
                         PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAT,
                         PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPELIB,
                         PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEENV,
                         PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPDV,
                         PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEREG,
                         PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAS,
                         PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAB)
                  AND OPE.FEC_VTA <=
                        TO_DATE (p_FEC_PCS || ' 23:59:59',
                                 'DD-MM-YYYY hh24:mi:ss')
                  AND OPE.COD_EST_AOS = EST.COD_EST_AOS;

      R_CONSULTA_OPERACION   C_CONSULTA_OPERACION%ROWTYPE;
   -- *********************************************************************************************

   BEGIN
      --Eliminacion de registros Bitacora detalle Proceso
      p_GLS_DET_PCS :=
         p_COD_UNI_PSC
         || ': Se eliminaran las operaciones no gestionadas con fecha valuta menor o igual a :'
         || p_FEC_PCS;
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);

      --Se carga cursor con las nominas a eliminar
      BEGIN
         OPEN C_CONSULTA_OPERACION;
      EXCEPTION
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            RAISE_APPLICATION_ERROR (-20000, err_msg);
      END;

      BEGIN
         FETCH C_CONSULTA_OPERACION INTO   R_CONSULTA_OPERACION;
      EXCEPTION
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);

            --Verificamos si el cursor se encuentra abierto en caso de error.
            IF (C_CONSULTA_OPERACION%ISOPEN)
            THEN
               CLOSE C_CONSULTA_OPERACION;
            END IF;

            RAISE_APPLICATION_ERROR (-20000, err_msg);
      END;

      --Recorrer operaciones
      WHILE C_CONSULTA_OPERACION%FOUND
      LOOP
         --Eliminacion de registros Bitacora detalle Proceso
         p_GLS_DET_PCS :=
               p_COD_UNI_PSC
            || ': Cambiando estado de operación a eliminada: '
            || R_CONSULTA_OPERACION.NUM_FOL_OPE
            || ' -> Estado Actual:'
            || TRIM (R_CONSULTA_OPERACION.DES_EST)
            || ' ->Fecha Valuta:'
            || R_CONSULTA_OPERACION.FEC_VTA;
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                      p_FEC_ICO_PCS,
                                                      p_COD_ERR_BIB,
                                                      p_COD_TPO_PSC,
                                                      p_ERROR);

         --Actualiza estado de la operacion y glosa en sp de package operacion
         PKG_PAB_OPERACION.SP_PAB_ACT_EST_GLS_OPE (
            R_CONSULTA_OPERACION.NUM_FOL_OPE,
            R_CONSULTA_OPERACION.FEC_ISR_OPE,
            PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEELI,
            PKG_PAB_CONSTANTES.V_GLS_EST_CIE_OPE,
            p_COD_USR,
            p_ERROR
         );

         FETCH C_CONSULTA_OPERACION INTO   R_CONSULTA_OPERACION;

         --Saber la cantidad
         v_CONT := v_CONT + 1;
      END LOOP;

      CLOSE C_CONSULTA_OPERACION;

      --Eliminacion de registros Bitacora detalle Proceso
      p_GLS_DET_PCS := p_COD_UNI_PSC || ': Operaciones eliminadas: ' || v_CONT;
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);

      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   EXCEPTION
      WHEN DUP_VAL_ON_INDEX
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (
            p_COD_UNI_PSC || ': ' || err_msg,
            p_FEC_ICO_PCS,
            p_COD_ERR_BIB,
            p_COD_TPO_PSC,
            p_ERROR
         );
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;

         --Verificamos si el cursor se encuentra abierto en caso de error.
         IF (C_CONSULTA_OPERACION%ISOPEN)
         THEN
            CLOSE C_CONSULTA_OPERACION;
         END IF;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (
            p_COD_UNI_PSC || ': ' || err_msg,
            p_FEC_ICO_PCS,
            p_COD_ERR_BIB,
            p_COD_TPO_PSC,
            p_ERROR
         );
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;

         --Verificamos si el cursor se encuentra abierto en caso de error.
         IF (C_CONSULTA_OPERACION%ISOPEN)
         THEN
            CLOSE C_CONSULTA_OPERACION;
         END IF;

         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_ACT_OPERACIONES_CIERRE;

   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_HIS_NOMINAS_CIERRE
   -- Objetivo: Al cierre del sistema se historifican las operaciones para la fecha ingresada
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_CBCRA_NOMNA_HTRCA, PABS_DT_CBCRA_NOMNA, PABS_DT_DETLL_NOMNA_HTRCA,
   --                PABS_DT_DETLL_NOMNA, PABS_DT_NOMNA_OPCON_HTRCA, PABS_DT_NOMNA_INFCN_OPCON
   -- Fecha: 26/09/2016
   -- Autor: Santander
   -- Input: p_FEC_PCS -> Fecha Proceso.
   -- Output: p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: 27/09/2016, Creacion Procedimiento.
   --***********************************************************************************************
   PROCEDURE Sp_PAB_HIS_NOMINAS_CIERRE (p_FEC_PCS   IN     VARCHAR2,
                                        p_ERROR        OUT NUMBER)
   IS
      v_NOM_SP        VARCHAR2 (30) := 'Sp_PAB_HIS_NOMINAS_CIERRE';
      v_FEC_CGA_HIS   DATE := TO_DATE (p_FEC_PCS, 'DD-MM-YYYY');
      v_FEC_RES_HIS DATE
            := TO_DATE (p_FEC_PCS || ' 23:59:59', 'DD-MM-YYYY hh24:mi:ss')
               - PKG_PAB_CONSTANTES.V_DIA_RES_HIS ;
      v_cant_ope      NUMBER;
   BEGIN
      --Inicia Logica Historificacion
      BEGIN
         p_ERROR := PKG_PAB_CONSTANTES.v_OK;

         --Historificacion Nominas
         p_GLS_DET_PCS :=
               p_COD_UNI_PSC
            || ': Se historificaran las nominas con fecha menor o igual a  :'
            || v_FEC_RES_HIS;
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                      p_FEC_ICO_PCS,
                                                      p_COD_ERR_BIB,
                                                      p_COD_TPO_PSC,
                                                      p_ERROR);

         --Se traspada las nominas a las tablas historicas
         INSERT INTO PABS_DT_CBCRA_NOMNA_HTRCA (NUM_FOL_NMN,
                                                FEC_CGA_HIS,
                                                NOM_NMN,
                                                COD_SIS_ENT,
                                                COD_USR,
                                                COD_EST_AOS,
                                                GLS_MTV_EST,
                                                FLG_DVI,
                                                IMP_TOT_NMN,
                                                FEC_ISR_NMN,
                                                HOR_ISR_NMN)
            SELECT   NUM_FOL_NMN,
                     v_FEC_CGA_HIS,                   -- FECHA DE LA INSERCION
                     NOM_NMN,
                     COD_SIS_ENT,
                     COD_USR,
                     COD_EST_AOS,
                     GLS_MTV_EST,
                     FLG_DVI,
                     IMP_TOT_NMN,
                     FEC_ISR_NMN,
                     HOR_ISR_NMN
              FROM   PABS_DT_CBCRA_NOMNA
             WHERE   FEC_ISR_NMN <= v_FEC_RES_HIS;

         v_cant_ope := SQL%ROWCOUNT; -- obtengo el numero de datos insertados.

         --Historificacion Nominas
         p_GLS_DET_PCS :=
               p_COD_UNI_PSC
            || ': Cabecera Nominas historificadas:'
            || v_cant_ope;
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                      p_FEC_ICO_PCS,
                                                      p_COD_ERR_BIB,
                                                      p_COD_TPO_PSC,
                                                      p_ERROR);
      EXCEPTION
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            RAISE_APPLICATION_ERROR (-20000, err_msg);
      END;

      BEGIN
         --Detalle de nomina
         INSERT INTO PABS_DT_DETLL_NOMNA_HTRCA (FEC_ISR_OPE,
                                                NUM_FOL_OPE,
                                                FEC_CGA_HIS,
                                                NOM_BFC,
                                                NUM_FOL_NMN,
                                                NUM_OPE_SIS_ENT,
                                                FLG_EGR_ING,
                                                COD_SIS_SAL,
                                                COD_EST_AOS,
                                                COD_TPO_ING,
                                                FEC_ENV_RCP_SWF,
                                                HOR_ENV_RCP_SWF,
                                                COD_MT_SWF,
                                                FLG_MRD_UTZ_SWF,
                                                COD_TPO_OPE_AOS,
                                                FEC_VTA,
                                                COD_DVI,
                                                IMP_OPE,
                                                COD_BCO_DTN,
                                                NUM_DOC_BFC,
                                                COD_TPD_BFC,
                                                NUM_CTA_BFC,
                                                COD_SUC,
                                                NUM_REF_SWF,
                                                FLG_OPE_DUP,
                                                NUM_REF_EXT,
                                                GLS_ADC_EST,
                                                NUM_REF_CTB)
            SELECT   FEC_ISR_OPE,
                     NUM_FOL_OPE,
                     v_FEC_CGA_HIS,                   -- FECHA DE LA INSERCION
                     NOM_BFC,
                     NUM_FOL_NMN,
                     NUM_OPE_SIS_ENT,
                     FLG_EGR_ING,
                     COD_SIS_SAL,
                     COD_EST_AOS,
                     COD_TPO_ING,
                     FEC_ENV_RCP_SWF,
                     HOR_ENV_RCP_SWF,
                     COD_MT_SWF,
                     FLG_MRD_UTZ_SWF,
                     COD_TPO_OPE_AOS,
                     FEC_VTA,
                     COD_DVI,
                     IMP_OPE,
                     NVL (COD_BCO_DTN,
                          PKG_PAB_CONSTANTES.V_BIC_BANCO_SANTANDER), -- en caso de tener el campo nulo dejamos santander por defecto.
                     NUM_DOC_BFC,
                     NVL (COD_TPD_BFC, ' '), --Temporal mientras se deja como nulo el campo en la tabla
                     NUM_CTA_BFC,
                     COD_SUC,
                     NUM_REF_SWF,
                     FLG_OPE_DUP,
                     NUM_REF_EXT,
                     GLS_ADC_EST,
                     NUM_REF_CTB
              FROM   PABS_DT_DETLL_NOMNA
             WHERE   FEC_ISR_OPE <= v_FEC_RES_HIS;

         v_cant_ope := SQL%ROWCOUNT; -- obtengo el numero de datos insertados.

         --Historificacion Nominas
         p_GLS_DET_PCS :=
            p_COD_UNI_PSC || ': Detalle Nomina historificadas:' || v_cant_ope;
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                      p_FEC_ICO_PCS,
                                                      p_COD_ERR_BIB,
                                                      p_COD_TPO_PSC,
                                                      p_ERROR);
      EXCEPTION
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            RAISE_APPLICATION_ERROR (-20000, err_msg);
      END;

      BEGIN
         INSERT INTO PABS_DT_NOMNA_OPCON_HTRCA (FEC_ISR_OPE,
                                                NUM_FOL_OPE,
                                                FEC_CGA_HIS,
                                                OBS_OPC_SWF,
                                                COD_BCO_BFC,
                                                COD_BCO_ITD,
                                                NUM_DOC_BFC,
                                                COD_TPD_BFC,
                                                NUM_CTA_DCV_BFC,
                                                GLS_DIR_BFC,
                                                NOM_CDD_BFC,
                                                COD_PAS_BFC,
                                                NOM_ODN,
                                                NUM_DOC_ODN,
                                                COD_TPD_ODN,
                                                NUM_CTA_ODN,
                                                NUM_CTA_DCV_ODN,
                                                GLS_DIR_ODN,
                                                NOM_CDD_ODN,
                                                COD_PAS_ODN,
                                                NUM_CLV_NGC,
                                                NUM_AGT,
                                                COD_FND_CCLV,
                                                COD_TPO_SDO,
                                                COD_TPO_CMA,
                                                COD_TPO_FND,
                                                FEC_TPO_OPE_CCLV,
                                                NUM_CLV_IDF,
                                                GLS_EST_RCH,
                                                GLS_MTV_VSD)
            SELECT   FEC_ISR_OPE,
                     NUM_FOL_OPE,
                     v_FEC_CGA_HIS,                   -- FECHA DE LA INSERCION
                     OBS_OPC_SWF,
                     COD_BCO_BFC,
                     COD_BCO_ITD,
                     NUM_DOC_BFC,
                     COD_TPD_BFC,
                     NUM_CTA_DCV_BFC,
                     GLS_DIR_BFC,
                     NOM_CDD_BFC,
                     COD_PAS_BFC,
                     NOM_ODN,
                     NUM_DOC_ODN,
                     COD_TPD_ODN,
                     NUM_CTA_ODN,
                     NUM_CTA_DCV_ODN,
                     GLS_DIR_ODN,
                     NOM_CDD_ODN,
                     COD_PAS_ODN,
                     NUM_CLV_NGC,
                     NUM_AGT,
                     COD_FND_CCLV,
                     COD_TPO_SDO,
                     COD_TPO_CMA,
                     COD_TPO_FND,
                     FEC_TPO_OPE_CCLV,
                     NUM_CLV_IDF,
                     GLS_EST_RCH,
                     GLS_MTV_VSD
              FROM   PABS_DT_NOMNA_INFCN_OPCON
             WHERE   FEC_ISR_OPE <= v_FEC_RES_HIS;

         v_cant_ope := SQL%ROWCOUNT; -- obtengo el numero de datos insertados.

         --Historificacion Nominas
         p_GLS_DET_PCS :=
               p_COD_UNI_PSC
            || ': Detalle Operaciones Nomina opcionales historificadas:'
            || v_cant_ope;
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                      p_FEC_ICO_PCS,
                                                      p_COD_ERR_BIB,
                                                      p_COD_TPO_PSC,
                                                      p_ERROR);
      EXCEPTION
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            RAISE_APPLICATION_ERROR (-20000, err_msg);
      END;

      --Fin Logica Historificacion

      p_GLS_DET_PCS :=
         p_COD_UNI_PSC
         || ': Inicio de eliminacion de registro de historificacion de nominas';
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);

      BEGIN
         --Eliminamos la informacion opcional
         DELETE FROM   PABS_DT_NOMNA_INFCN_OPCON
               WHERE   FEC_ISR_OPE <= v_FEC_RES_HIS;

         v_cant_ope := SQL%ROWCOUNT; -- obtengo el numero de datos insertados.

         --Historificacion Nominas
         p_GLS_DET_PCS :=
               p_COD_UNI_PSC
            || ': Detalle Operaciones Nomina opcionales eliminadas:'
            || v_cant_ope;
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                      p_FEC_ICO_PCS,
                                                      p_COD_ERR_BIB,
                                                      p_COD_TPO_PSC,
                                                      p_ERROR);
      EXCEPTION
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            RAISE_APPLICATION_ERROR (-20000, err_msg);
      END;

      BEGIN
         --Eliminamos la operaciones de las nominas
         DELETE FROM   PABS_DT_DETLL_NOMNA
               WHERE   FEC_ISR_OPE <= v_FEC_RES_HIS;

         v_cant_ope := SQL%ROWCOUNT; -- obtengo el numero de datos insertados.

         --
         p_GLS_DET_PCS :=
            p_COD_UNI_PSC || ': Detalle Nomina eliminadas:' || v_cant_ope;
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                      p_FEC_ICO_PCS,
                                                      p_COD_ERR_BIB,
                                                      p_COD_TPO_PSC,
                                                      p_ERROR);
      EXCEPTION
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            RAISE_APPLICATION_ERROR (-20000, err_msg);
      END;

      BEGIN
         --Eliminamos la las nominas
         DELETE FROM   PABS_DT_CBCRA_NOMNA
               WHERE   FEC_ISR_NMN <= v_FEC_RES_HIS;

         v_cant_ope := SQL%ROWCOUNT; -- obtengo el numero de datos insertados.

         --Historificacion Nominas
         p_GLS_DET_PCS :=
            p_COD_UNI_PSC || ': Nominas cabecera eliminadas:' || v_cant_ope;
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                      p_FEC_ICO_PCS,
                                                      p_COD_ERR_BIB,
                                                      p_COD_TPO_PSC,
                                                      p_ERROR);
      EXCEPTION
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            RAISE_APPLICATION_ERROR (-20000, err_msg);
      END;
   ---Fin Eliminacion de registros tabla origen

   EXCEPTION
      WHEN DUP_VAL_ON_INDEX
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (
            p_COD_UNI_PSC || ': ' || err_msg,
            p_FEC_ICO_PCS,
            p_COD_ERR_BIB,
            p_COD_TPO_PSC,
            p_ERROR
         );
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (
            p_COD_UNI_PSC || ': ' || err_msg,
            p_FEC_ICO_PCS,
            p_COD_ERR_BIB,
            p_COD_TPO_PSC,
            p_ERROR
         );
         RAISE_APPLICATION_ERROR (-20000, err_msg);
   END Sp_PAB_HIS_NOMINAS_CIERRE;

   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_HIS_OPERACIONES_CIERRE
   -- Objetivo: Al cierre del sistema se historifican las operaciones para la fecha ingresada
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN_HTRCA, PABS_DT_DETLL_OPRCN, PABS_DT_OPRCN_OPCON_HTRCA, PABS_DT_OPRCN_INFCN_OPCON
   -- Fecha: 26/09/2016
   -- Autor: Santander
   -- Input: p_FEC_PCS -> Fecha Proceso.
   -- Output: p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: 14/09/2016, Creacion Procedimiento.
   --***********************************************************************************************
   PROCEDURE Sp_PAB_HIS_OPERACIONES_CIERRE (p_FEC_PCS   IN     VARCHAR2,
                                            p_ERROR        OUT NUMBER)
   IS
      v_NOM_SP        VARCHAR2 (30) := 'Sp_PAB_HIS_OPERACIONES_CIERRE';
      v_FEC_CGA_HIS   DATE := TO_DATE (p_FEC_PCS, 'DD-MM-YYYY');
      v_FEC_RES_HIS DATE
            := TO_DATE (p_FEC_PCS || ' 23:59:59', 'DD-MM-YYYY hh24:mi:ss')
               - PKG_PAB_CONSTANTES.V_DIA_RES_HIS ;
      v_cant_OPE      NUMBER;
   BEGIN
      BEGIN
         p_ERROR := PKG_PAB_CONSTANTES.v_OK;

         --Historificacion Nominas
         p_GLS_DET_PCS :=
            p_COD_UNI_PSC
            || ': Se historificaran las operaciones con fecha menor o igual a  :'
            || v_FEC_RES_HIS;
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                      p_FEC_ICO_PCS,
                                                      p_COD_ERR_BIB,
                                                      p_COD_TPO_PSC,
                                                      p_ERROR);

         INSERT INTO PABS_DT_DETLL_OPRCN_HTRCA (FEC_ISR_OPE,
                                                NUM_FOL_OPE,
                                                FEC_CGA_HIS,
                                                NOM_BFC,
                                                NUM_FOL_NMN,
                                                NUM_FOL_OPE_ORG,
                                                NUM_OPE_SIS_ENT,
                                                FLG_EGR_ING,
                                                COD_SIS_SAL,
                                                COD_SIS_ENT,
                                                COD_EST_AOS,
                                                COD_TPO_ING,
                                                FEC_ENV_RCP_SWF,
                                                HOR_ENV_RCP_SWF,
                                                COD_MT_SWF,
                                                FLG_MRD_UTZ_SWF,
                                                COD_TPO_OPE_AOS,
                                                FEC_VTA,
                                                COD_DVI,
                                                IMP_OPE,
                                                COD_BCO_DTN,
                                                NUM_DOC_BFC,
                                                COD_TPD_BFC,
                                                NUM_CTA_BFC,
                                                COD_SUC,
                                                NUM_REF_SWF,
                                                NUM_REF_EXT,
                                                GLS_ADC_EST,
                                                NUM_REF_CTB,
                                                COD_BCO_ORG,
                                                NUM_IDF_GTR_DOC,
                                                NUM_SRV_TPO_UID,         --GPI
                                                NUM_IDF_UNC_UID,        --GPI)
                                                FEC_ACT_OPE)
            SELECT   FEC_ISR_OPE,
                     NUM_FOL_OPE,
                     v_FEC_CGA_HIS,                   -- FECHA DE LA INSERCION
                     NOM_BFC,
                     NUM_FOL_NMN,
                     NUM_FOL_OPE_ORG,
                     NUM_OPE_SIS_ENT,
                     FLG_EGR_ING,
                     COD_SIS_SAL,
                     COD_SIS_ENT,
                     COD_EST_AOS,
                     COD_TPO_ING,
                     FEC_ENV_RCP_SWF,
                     HOR_ENV_RCP_SWF,
                     COD_MT_SWF,
                     FLG_MRD_UTZ_SWF,
                     COD_TPO_OPE_AOS,
                     FEC_VTA,
                     COD_DVI,
                     IMP_OPE,
                     NVL (COD_BCO_DTN,
                          PKG_PAB_CONSTANTES.V_BIC_BANCO_SANTANDER), -- en caso de tener el campo nulo dejamos santander por defecto.
                     NUM_DOC_BFC,
                     COD_TPD_BFC,
                     NUM_CTA_BFC,
                     COD_SUC,
                     NUM_REF_SWF,
                     NUM_REF_EXT,
                     GLS_ADC_EST,
                     NUM_REF_CTB,
                     COD_BCO_ORG,
                     NUM_IDF_GTR_DOC,
                     NUM_SRV_TPO_UID,                                    --GPI
                     NUM_IDF_UNC_UID,                                    --GPI
                     FEC_ACT_OPE                                        -- FEC
              FROM   PABS_DT_DETLL_OPRCN
             WHERE   FEC_ISR_OPE <= v_FEC_RES_HIS;

         v_cant_OPE := SQL%ROWCOUNT; -- obtengo el numero de datos insertados.

         p_GLS_DET_PCS :=
               p_COD_UNI_PSC
            || ': Detalle Operaciones historificadas:'
            || v_cant_OPE;
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                      p_FEC_ICO_PCS,
                                                      p_COD_ERR_BIB,
                                                      p_COD_TPO_PSC,
                                                      p_ERROR);
      EXCEPTION
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            RAISE_APPLICATION_ERROR (-20000, err_msg);
      END;

      BEGIN
         INSERT INTO PABS_DT_OPRCN_OPCON_HTRCA (FEC_ISR_OPE,
                                                NUM_FOL_OPE,
                                                FEC_CGA_HIS,
                                                OBS_OPC_SWF,
                                                COD_BCO_BFC,
                                                COD_BCO_ITD,
                                                NUM_DOC_BFC,
                                                COD_TPD_BFC,
                                                NUM_CTA_DCV_BFC,
                                                GLS_DIR_BFC,
                                                NOM_CDD_BFC,
                                                COD_PAS_BFC,
                                                NOM_ODN,
                                                NUM_DOC_ODN,
                                                COD_TPD_ODN,
                                                NUM_CTA_ODN,
                                                NUM_CTA_DCV_ODN,
                                                GLS_DIR_ODN,
                                                NOM_CDD_ODN,
                                                COD_PAS_ODN,
                                                NUM_CLV_NGC,
                                                NUM_AGT,
                                                COD_FND_CCLV,
                                                COD_TPO_SDO,
                                                COD_TPO_CMA,
                                                COD_TPO_FND,
                                                FEC_TPO_OPE_CCLV,
                                                NUM_CLV_IDF,
                                                GLS_EST_RCH,
                                                GLS_MTV_VSD)
            SELECT   FEC_ISR_OPE,
                     NUM_FOL_OPE,
                     v_FEC_CGA_HIS,                   -- FECHA DE LA INSERCION
                     OBS_OPC_SWF,
                     COD_BCO_BFC,
                     COD_BCO_ITD,
                     NUM_DOC_BFC,
                     COD_TPD_BFC,
                     NUM_CTA_DCV_BFC,
                     GLS_DIR_BFC,
                     NOM_CDD_BFC,
                     COD_PAS_BFC,
                     NOM_ODN,
                     NUM_DOC_ODN,
                     COD_TPD_ODN,
                     NUM_CTA_ODN,
                     NUM_CTA_DCV_ODN,
                     GLS_DIR_ODN,
                     NOM_CDD_ODN,
                     COD_PAS_ODN,
                     NUM_CLV_NGC,
                     NUM_AGT,
                     COD_FND_CCLV,
                     COD_TPO_SDO,
                     COD_TPO_CMA,
                     COD_TPO_FND,
                     FEC_TPO_OPE_CCLV,
                     NUM_CLV_IDF,
                     GLS_EST_RCH,
                     GLS_MTV_VSD
              FROM   PABS_DT_OPRCN_INFCN_OPCON
             WHERE   FEC_ISR_OPE <= v_FEC_RES_HIS;

         v_cant_OPE := SQL%ROWCOUNT; -- obtengo el numero de datos insertados.

         p_GLS_DET_PCS :=
               p_COD_UNI_PSC
            || ': Detalle Operaciones opcionales historificadas:'
            || v_cant_OPE;
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                      p_FEC_ICO_PCS,
                                                      p_COD_ERR_BIB,
                                                      p_COD_TPO_PSC,
                                                      p_ERROR);
      EXCEPTION
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            RAISE_APPLICATION_ERROR (-20000, err_msg);
      END;

      BEGIN
         --
         DELETE FROM   PABS_DT_OPRCN_INFCN_OPCON
               WHERE   FEC_ISR_OPE <= v_FEC_RES_HIS;

         v_cant_OPE := SQL%ROWCOUNT; -- obtengo el numero de datos insertados.

         --Historificacion Operaciones
         p_GLS_DET_PCS :=
               p_COD_UNI_PSC
            || ': Detalle Operaciones opcionales eliminadas:'
            || v_cant_OPE;
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                      p_FEC_ICO_PCS,
                                                      p_COD_ERR_BIB,
                                                      p_COD_TPO_PSC,
                                                      p_ERROR);
      EXCEPTION
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            RAISE_APPLICATION_ERROR (-20000, err_msg);
      END;

      BEGIN
         DELETE FROM   PABS_DT_DETLL_OPRCN
               WHERE   FEC_ISR_OPE <= v_FEC_RES_HIS;

         v_cant_OPE := SQL%ROWCOUNT; -- obtengo el numero de datos insertados.

         --Historificacion Operaciones
         p_GLS_DET_PCS :=
               p_COD_UNI_PSC
            || ': Detalle Operaciones eliminadas:'
            || v_cant_OPE;
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                      p_FEC_ICO_PCS,
                                                      p_COD_ERR_BIB,
                                                      p_COD_TPO_PSC,
                                                      p_ERROR);
      EXCEPTION
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            RAISE_APPLICATION_ERROR (-20000, err_msg);
      END;
   ---Fin Eliminacion de registros tabla origen

   EXCEPTION
      WHEN DUP_VAL_ON_INDEX
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (
            p_COD_UNI_PSC || ': ' || err_msg,
            p_FEC_ICO_PCS,
            p_COD_ERR_BIB,
            p_COD_TPO_PSC,
            p_ERROR
         );
         RAISE_APPLICATION_ERROR (-20000, err_msg);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (
            p_COD_UNI_PSC || ': ' || err_msg,
            p_FEC_ICO_PCS,
            p_COD_ERR_BIB,
            p_COD_TPO_PSC,
            p_ERROR
         );
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_HIS_OPERACIONES_CIERRE;

   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_HIS_BITACORA_CIERRE
   -- Objetivo: Al cierre del sistema se historifican las operaciones para la fecha ingresada
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_BTCOR_OPRCN_HTRCA, PABS_DT_BTCOR_OPRCN.
   -- Fecha: 26/09/2016
   -- Autor: Santander
   -- Input: p_FEC_PCS -> Fecha Proceso.
   -- Output: p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: 14/09/2016, Creacion Procedimiento.
   --***********************************************************************************************
   PROCEDURE Sp_PAB_HIS_BITACORA_CIERRE (p_FEC_PCS   IN     VARCHAR2,
                                         p_ERROR        OUT NUMBER)
   IS
      v_NOM_SP        VARCHAR2 (30) := 'Sp_PAB_HIS_BITACORA_CIERRE';
      v_FEC_CGA_HIS   DATE := TO_DATE (p_FEC_PCS, 'DD-MM-YYYY');
      v_FEC_RES_HIS DATE
            := TO_DATE (p_FEC_PCS || ' 00:00:00', 'DD-MM-YYYY hh24:mi:ss')
               - PKG_PAB_CONSTANTES.V_DIA_RES_HIS ;
      v_NUM_OPE       NUMBER;
   BEGIN
      p_ERROR := PKG_PAB_CONSTANTES.v_OK;

      --Historificacion Nominas
      p_GLS_DET_PCS :=
         p_COD_UNI_PSC
         || ': Se historificaran la bitacora de las operaciones con fecha menor o igual a  :'
         || v_FEC_RES_HIS;
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);

      BEGIN
         INSERT INTO PABS_DT_BTCOR_OPRCN_HTRCA (FEC_ISR_BTC,
                                                NUM_FOL_OPE,
                                                FEC_CGA_HIS,
                                                FEC_ISR_OPE,
                                                COD_USR,
                                                COD_EST_ICO,
                                                COD_EST_FIN,
                                                DSC_GLB_BTC)
            SELECT   FEC_ISR_BTC,
                     NUM_FOL_OPE,
                     V_FEC_CGA_HIS,
                     FEC_ISR_OPE,
                     COD_USR,
                     COD_EST_ICO,
                     COD_EST_FIN,
                     DSC_GLB_BTC
              FROM   PABS_DT_BTCOR_OPRCN
             WHERE   FEC_ISR_BTC <= v_FEC_RES_HIS;

         v_NUM_OPE := SQL%ROWCOUNT;  -- obtengo el numero de datos insertados.

         p_GLS_DET_PCS :=
               p_COD_UNI_PSC
            || ': Bitacora de operaciones historificadas:'
            || v_NUM_OPE;
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                      p_FEC_ICO_PCS,
                                                      p_COD_ERR_BIB,
                                                      p_COD_TPO_PSC,
                                                      p_ERROR);
      EXCEPTION
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            RAISE_APPLICATION_ERROR (-20000, err_msg);
      END;

      --Fin Logica Historificacion

      BEGIN
         --Eliminación de la data
         DELETE FROM   PABS_DT_BTCOR_OPRCN
               WHERE   FEC_ISR_BTC <= v_FEC_RES_HIS;

         v_NUM_OPE := SQL%ROWCOUNT;  -- obtengo el numero de datos insertados.

         p_GLS_DET_PCS :=
               p_COD_UNI_PSC
            || ': Bitacora de operaciones eliminadas:'
            || v_NUM_OPE;
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                      p_FEC_ICO_PCS,
                                                      p_COD_ERR_BIB,
                                                      p_COD_TPO_PSC,
                                                      p_ERROR);
      EXCEPTION
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            RAISE_APPLICATION_ERROR (-20000, err_msg);
      END;
   --Fin Eliminacion de registros tabla origen

   EXCEPTION
      WHEN DUP_VAL_ON_INDEX
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (
            p_COD_UNI_PSC || ': ' || err_msg,
            p_FEC_ICO_PCS,
            p_COD_ERR_BIB,
            p_COD_TPO_PSC,
            p_ERROR
         );
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (
            p_COD_UNI_PSC || ': ' || err_msg,
            p_FEC_ICO_PCS,
            p_COD_ERR_BIB,
            p_COD_TPO_PSC,
            p_ERROR
         );
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_HIS_BITACORA_CIERRE;

   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ELI_BITACORA_PROCESO
   -- Objetivo: Al cierre del sistema se eliminan los registros de la tabla bitacora proceso para evitar acumulaci?n de registros de  traza
   --           el prerior eliminado es segun lo indicado por la constante PKG_PAB_CONSTANTES.V_DIA_ELI_LOG.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_BTCOR_PRCSO, PABS_DT_BTCOR_DETLL_PRCSO
   -- Fecha: 14/11/2016
   -- Autor: Santander
   -- Input: p_FEC_PCS -> Fecha Proceso.
   -- Output: p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: 14/11/2016, Creacion Procedimiento.
   --***********************************************************************************************
   PROCEDURE Sp_PAB_ELI_BITACORA_PROCESO (p_FEC_PCS   IN     VARCHAR2,
                                          p_ERROR        OUT NUMBER)
   IS
      v_NOM_SP    VARCHAR2 (30) := 'Sp_PAB_ELI_BITACORA_PROCESO';
      v_FEC_ELI_HIS DATE
            := TO_DATE (p_FEC_PCS || ' 23:59:59', 'DD-MM-YYYY hh24:mi:ss')
               - PKG_PAB_CONSTANTES.V_DIA_ELI_LOG ;
      err_code    NUMBER := 0;
      V_NUM_ELI   NUMBER := 0;
   BEGIN
      BEGIN
         P_ERROR := PKG_PAB_CONSTANTES.V_OK;

         --Eliminacion de registros Bitacora detalle Proceso
         p_GLS_DET_PCS :=
               p_COD_UNI_PSC
            || ': Se eliminaran los registros con fecha menor o igual a:'
            || TO_CHAR (v_FEC_ELI_HIS, 'DD-MM-YYYY HH24:MI:SS');
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                      p_FEC_ICO_PCS,
                                                      p_COD_ERR_BIB,
                                                      p_COD_TPO_PSC,
                                                      p_ERROR);

         --Eliminamos detalle de proceso
         DELETE FROM   PABS_DT_BTCOR_DETLL_PRCSO
               WHERE   FEC_ICO_PCS <= v_FEC_ELI_HIS;

         V_NUM_ELI := SQL%ROWCOUNT;  -- obtengo el numero de datos insertados.

         p_GLS_DET_PCS :=
               p_COD_UNI_PSC
            || ': Trazas eliminadas PABS_DT_BTCOR_DETLL_PRCSO: '
            || V_NUM_ELI;
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                      p_FEC_ICO_PCS,
                                                      p_COD_ERR_BIB,
                                                      p_COD_TPO_PSC,
                                                      p_ERROR);
      EXCEPTION
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            RAISE_APPLICATION_ERROR (-20000, err_msg);
      END;

      --Fin de registros Bitacora detalle Proceso

      --Eliminacion de registros Bitacora Proceso
      BEGIN
         DELETE FROM   PABS_DT_BTCOR_PRCSO
               WHERE   FEC_ICO_PCS <= v_FEC_ELI_HIS;

         V_NUM_ELI := SQL%ROWCOUNT;  -- obtengo el numero de datos insertados.

         p_GLS_DET_PCS :=
               p_COD_UNI_PSC
            || ': Trazas eliminadas PABS_DT_BTCOR_PRCSO: '
            || V_NUM_ELI;
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                      p_FEC_ICO_PCS,
                                                      p_COD_ERR_BIB,
                                                      p_COD_TPO_PSC,
                                                      p_ERROR);
      EXCEPTION
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            RAISE_APPLICATION_ERROR (-20000, err_msg);
      END;
   --Fin de registros Bitacora Proceso

   EXCEPTION
      WHEN DUP_VAL_ON_INDEX
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (
            p_COD_UNI_PSC || ': ' || err_msg,
            p_FEC_ICO_PCS,
            p_COD_ERR_BIB,
            p_COD_TPO_PSC,
            p_ERROR
         );
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (
            p_COD_UNI_PSC || ': ' || err_msg,
            p_FEC_ICO_PCS,
            p_COD_ERR_BIB,
            p_COD_TPO_PSC,
            p_ERROR
         );
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_ELI_BITACORA_PROCESO;

   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ELI_LOG_CIERRE
   -- Objetivo: Al cierre del sistema se eliminan los registros del log de sistema, se conservan X dias segun
   --           lo indicado por la constante PKG_PAB_CONSTANTES.V_DIA_ELI_LOG.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_BTCOR_BASE_DATO_ALMOT
   -- Fecha: 26/09/2016
   -- Autor: Santander
   -- Input: p_FEC_PCS -> Fecha Proceso.
   -- Output: p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: 14/09/2016, Creacion Procedimiento.
   --***********************************************************************************************
   PROCEDURE Sp_PAB_ELI_LOG_CIERRE (p_FEC_PCS   IN     VARCHAR2,
                                    p_ERROR        OUT NUMBER)
   IS
      v_NOM_SP    VARCHAR2 (30) := 'Sp_PAB_ELI_LOG_CIERRE';
      v_FEC_ELI DATE
            := TO_DATE (p_FEC_PCS || ' 23:59:59', 'DD-MM-YYYY hh24:mi:ss')
               - PKG_PAB_CONSTANTES.V_DIA_ELI_LOG ;
      v_NUM_LOG   NUMBER;
   BEGIN
      p_ERROR := PKG_PAB_CONSTANTES.v_OK;

      --Historificacion Nominas
      p_GLS_DET_PCS :=
         p_COD_UNI_PSC
         || ': Se eliminara todo el log de la base de datos con fecha menor o igual a  :'
         || TO_CHAR (v_FEC_ELI, 'DD-MM-YYYY HH24:MI:SS');
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);

      BEGIN
         DELETE FROM   PABS_DT_BTCOR_BASE_DATO_ALMOT
               WHERE   FEC_ISR_BTC_BSE_DTS <= v_FEC_ELI;

         v_NUM_LOG := SQL%ROWCOUNT;  -- obtengo el numero de datos insertados.

         p_GLS_DET_PCS :=
               p_COD_UNI_PSC
            || ': Log eliminado de la base de datos:'
            || v_NUM_LOG;
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                      p_FEC_ICO_PCS,
                                                      p_COD_ERR_BIB,
                                                      p_COD_TPO_PSC,
                                                      p_ERROR);
      EXCEPTION
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            RAISE_APPLICATION_ERROR (-20000, err_msg);
      END;
   --Fin Eliminacion de registros tabla LOG

   EXCEPTION
      WHEN DUP_VAL_ON_INDEX
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (
            p_COD_UNI_PSC || ': ' || err_msg,
            p_FEC_ICO_PCS,
            p_COD_ERR_BIB,
            p_COD_TPO_PSC,
            p_ERROR
         );
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (
            p_COD_UNI_PSC || ': ' || err_msg,
            p_FEC_ICO_PCS,
            p_COD_ERR_BIB,
            p_COD_TPO_PSC,
            p_ERROR
         );
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_ELI_LOG_CIERRE;

   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ELI_NOM_EXT
   -- Objetivo: Al cierre del sistema se eliminan los registros de validacion nomina externa
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_MOTVO_NO_HABLT
   -- Fecha: 30/01/2019
   -- Autor: Santander
   -- Input: p_FEC_PCS -> Fecha Proceso.
   -- Output: p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: 30/01/2019, Creacion Procedimiento.
   --***********************************************************************************************
   PROCEDURE Sp_PAB_ELI_VAL_NOM_EXT (p_FEC_PCS   IN     VARCHAR2,
                                     p_ERROR        OUT NUMBER)
   IS
      v_NOM_SP    VARCHAR2 (30) := 'Sp_PAB_ELI_VAL_NOM_EXT';
      v_FEC_ELI DATE
            := TO_DATE (p_FEC_PCS || ' 23:59:59', 'DD-MM-YYYY hh24:mi:ss') ;
      v_NUM_LOG   NUMBER;
   BEGIN
      p_ERROR := PKG_PAB_CONSTANTES.v_OK;

      --Historificacion Nominas
      p_GLS_DET_PCS :=
         p_COD_UNI_PSC
         || ': Se eliminara todas las validaciones de las nominas externas con fecha menor o igual a  :'
         || TO_CHAR (v_FEC_ELI, 'DD-MM-YYYY HH24:MI:SS');
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);

      BEGIN
         DELETE FROM   PABS_DT_MOTVO_NO_HABLT
               WHERE   FEC_ISR_OPE <= v_FEC_ELI;

         v_NUM_LOG := SQL%ROWCOUNT;  -- obtengo el numero de datos insertados.

         p_GLS_DET_PCS :=
               p_COD_UNI_PSC
            || ': registros de validacion de nomina externa :'
            || v_NUM_LOG;
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                      p_FEC_ICO_PCS,
                                                      p_COD_ERR_BIB,
                                                      p_COD_TPO_PSC,
                                                      p_ERROR);
      EXCEPTION
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            RAISE_APPLICATION_ERROR (-20000, err_msg);
      END;
   --Fin Eliminacion de registros tabla LOG

   EXCEPTION
      WHEN DUP_VAL_ON_INDEX
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (
            p_COD_UNI_PSC || ': ' || err_msg,
            p_FEC_ICO_PCS,
            p_COD_ERR_BIB,
            p_COD_TPO_PSC,
            p_ERROR
         );
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (
            p_COD_UNI_PSC || ': ' || err_msg,
            p_FEC_ICO_PCS,
            p_COD_ERR_BIB,
            p_COD_TPO_PSC,
            p_ERROR
         );
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_ELI_VAL_NOM_EXT;

   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ELI_MOV_PRO_FIL
   -- Objetivo: Al cierre del sistema se eliminan los registros de proyeccion de filiales con fechas antiguas
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_MVNTO_FLIAL
   -- Fecha: 26/09/2016
   -- Autor: Santander
   -- Input: p_FEC_PCS -> Fecha Proceso.
   -- Output: p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: 14/09/2016, Creacion Procedimiento.
   --***********************************************************************************************
   PROCEDURE Sp_PAB_ELI_MOV_PRO_FIL (p_FEC_PCS   IN     VARCHAR2,
                                     p_ERROR        OUT NUMBER)
   IS
      v_NOM_SP    VARCHAR2 (30) := 'Sp_PAB_ELI_MOV_PRO_FIL';
      v_FEC_ELI   DATE := TO_DATE (p_FEC_PCS, 'DD-MM-YYYY');
      v_NUM_LOG   NUMBER;
   BEGIN
      p_ERROR := PKG_PAB_CONSTANTES.v_OK;

      --Eliminación
      p_GLS_DET_PCS :=
         p_COD_UNI_PSC
         || ': Se eliminara todos los mov de filiales proyeccion con fecha menor o igual a :'
         || p_FEC_PCS;
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);

      BEGIN
         DELETE FROM   PABS_DT_MVNTO_FLIAL
               WHERE   FEC_VTA_FIL <= v_FEC_ELI;

         v_NUM_LOG := SQL%ROWCOUNT;  -- obtengo el numero de datos eliminados.

         p_GLS_DET_PCS :=
               p_COD_UNI_PSC
            || ': Mov eliminado de proyección filiales:'
            || v_NUM_LOG;
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                      p_FEC_ICO_PCS,
                                                      p_COD_ERR_BIB,
                                                      p_COD_TPO_PSC,
                                                      p_ERROR);
      EXCEPTION
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            RAISE_APPLICATION_ERROR (-20000, err_msg);
      END;
   --Fin Eliminacion de registros tabla LOG

   EXCEPTION
      WHEN DUP_VAL_ON_INDEX
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (
            p_COD_UNI_PSC || ': ' || err_msg,
            p_FEC_ICO_PCS,
            p_COD_ERR_BIB,
            p_COD_TPO_PSC,
            p_ERROR
         );
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (
            p_COD_UNI_PSC || ': ' || err_msg,
            p_FEC_ICO_PCS,
            p_COD_ERR_BIB,
            p_COD_TPO_PSC,
            p_ERROR
         );
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_ELI_MOV_PRO_FIL;

   --**************************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ELI_NOTIFICACION
   -- Objetivo: Al cierre del sistema se eliminan los registros de la tabla Notificaciones para evitar acumulación
   --          de registros, esto se hará eliminando registros que cumplan más de 7 días, para eliminar se crea la
   --          constante PKG_PAB_CONSTANTES.V_DIA_ELI_NTF que inicialmente tendra valor 7.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_BTCOR_PRCSO, PABS_DT_BTCOR_DETLL_PRCSO
   -- Fecha: 03/11/2017
   -- Autor: Santander-CAH
   -- Input: p_FEC_PCS -> Fecha Proceso.
   -- Output: p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: 14/11/2016, Creacion Procedimiento.
   --***************************************************************************************************************
   PROCEDURE Sp_PAB_ELI_NOTIFICACION (p_FEC_PCS   IN     VARCHAR2,
                                      p_ERROR        OUT NUMBER)
   IS
      v_NOM_SP    VARCHAR2 (30) := 'Sp_PAB_ELI_BITACORA_PROCESO';
      v_FEC_ELI_HIS DATE
            := TO_DATE (p_FEC_PCS, 'DD-MM-YYYY')
               - PKG_PAB_CONSTANTES.V_DIA_ELI_NTF ;
      err_code    NUMBER := 0;
      V_NUM_ELI   NUMBER := 0;
   BEGIN
      BEGIN
         P_ERROR := PKG_PAB_CONSTANTES.V_OK;

         --Eliminacion de registros Bitacora detalle Proceso
         p_GLS_DET_PCS :=
            p_COD_UNI_PSC
            || ': Se eliminaran los registros de notificaciones, con fecha menor o igual a:'
            || TO_CHAR (v_FEC_ELI_HIS, 'DD-MM-YYYY HH24:MI:SS');
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                      p_FEC_ICO_PCS,
                                                      p_COD_ERR_BIB,
                                                      p_COD_TPO_PSC,
                                                      p_ERROR);

         --Eliminamos detalle de proceso
         DELETE FROM   PABS_DT_NTFCC_ALMOT NT
               WHERE   NT.FEC_ING_NTF <= v_FEC_ELI_HIS;

         V_NUM_ELI := SQL%ROWCOUNT;  -- obtengo el numero de datos insertados.

         p_GLS_DET_PCS :=
               p_COD_UNI_PSC
            || ': Notificaciones eliminadas PABS_DT_NTFCC_ALMOT: '
            || V_NUM_ELI;
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                      p_FEC_ICO_PCS,
                                                      p_COD_ERR_BIB,
                                                      p_COD_TPO_PSC,
                                                      p_ERROR);
      EXCEPTION
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            RAISE_APPLICATION_ERROR (-20000, err_msg);
      END;
   --Fin de eliminación notificaciones.

   EXCEPTION
      WHEN DUP_VAL_ON_INDEX
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (
            p_COD_UNI_PSC || ': ' || err_msg,
            p_FEC_ICO_PCS,
            p_COD_ERR_BIB,
            p_COD_TPO_PSC,
            p_ERROR
         );
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (
            p_COD_UNI_PSC || ': ' || err_msg,
            p_FEC_ICO_PCS,
            p_COD_ERR_BIB,
            p_COD_TPO_PSC,
            p_ERROR
         );
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_ELI_NOTIFICACION;

   --**************************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ELI_DET_OPERACION_DCV
   -- Objetivo: Al cierre del sistema se eliminan los registros de la tabla detalle operaciones dcv para evitar acumulación
   --          de registros, esto se hará eliminando registros que cumplan más de 1 día.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN_DCV
   -- Fecha: 06/08/2018
   -- Autor: Santander-CAH
   -- Input: p_FEC_PCS -> Fecha Proceso.
   -- Output: p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: 06/08/2018, Creacion Procedimiento.
   --***************************************************************************************************************
   PROCEDURE Sp_PAB_ELI_DET_OPERACION_DCV (p_FEC_PCS   IN     VARCHAR2,
                                           p_ERROR        OUT NUMBER)
   IS
      v_NOM_SP        VARCHAR2 (30) := 'Sp_PAB_ELI_DETALLE_OPE_DCV';
      v_FEC_ELI_HIS   DATE := TO_DATE (p_FEC_PCS, 'DD-MM-YYYY');
      err_code        NUMBER := 0;
      V_NUM_ELI       NUMBER := 0;
   BEGIN
      P_ERROR := PKG_PAB_CONSTANTES.V_OK;

      --Eliminacion de registros Detalle Proceso Operacion DCV
      p_GLS_DET_PCS :=
         p_COD_UNI_PSC
         || ': Se eliminaran los registros de detalle, con fecha menor o igual a:'
         || TO_CHAR (v_FEC_ELI_HIS, 'DD-MM-YYYY HH24:MI:SS');
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);

      --Eliminamos detalle de proceso
      DELETE FROM   PABS_DT_DETLL_OPRCN_DCV DC
            WHERE   DC.FEC_ISR_MSJ_SWF <= v_FEC_ELI_HIS;

      V_NUM_ELI := SQL%ROWCOUNT;     -- obtengo el numero de datos insertados.

      p_GLS_DET_PCS :=
            p_COD_UNI_PSC
         || ': Detalle Operacion eliminada PABS_DT_DETLL_OPRCN_DCV: '
         || V_NUM_ELI;
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);
   --Fin de eliminación Detalle Operacion.

   EXCEPTION
      WHEN DUP_VAL_ON_INDEX
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (
            p_COD_UNI_PSC || ': ' || err_msg,
            p_FEC_ICO_PCS,
            p_COD_ERR_BIB,
            p_COD_TPO_PSC,
            p_ERROR
         );
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (
            p_COD_UNI_PSC || ': ' || err_msg,
            p_FEC_ICO_PCS,
            p_COD_ERR_BIB,
            p_COD_TPO_PSC,
            p_ERROR
         );
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_ELI_DET_OPERACION_DCV;

   --**************************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ELI_SALDO_BILAT
   -- Objetivo: Al cierre del sistema se eliminan los registros de la tabla Saldos bilaterales para evitar acumulación
   --          de registros, esto se hará eliminando registros que cumplan más de 1 día.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_SALDO_BILAT
   -- Fecha: 16/11/2017
   -- Autor: Santander-CAH
   -- Input: p_FEC_PCS -> Fecha Proceso.
   -- Output: p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: 16/11/2016, Creacion Procedimiento.
   --***************************************************************************************************************

   PROCEDURE Sp_PAB_ELI_SALDO_BILAT (p_FEC_PCS   IN     VARCHAR2,
                                     p_ERROR        OUT NUMBER)
   IS
      v_NOM_SP    VARCHAR2 (30) := 'Sp_PAB_ELI_SALDO_BILAT';
      v_FEC_ELI   DATE := TO_DATE (p_FEC_PCS, 'DD-MM-YYYY');
      err_code    NUMBER := 0;
      V_NUM_ELI   NUMBER := 0;
   BEGIN
      BEGIN
         P_ERROR := PKG_PAB_CONSTANTES.V_OK;

         --Eliminacion de registros Bitacora detalle Proceso
         p_GLS_DET_PCS :=
            p_COD_UNI_PSC
            || ': Se eliminaran los registros de saldos bilaterales, con fecha menor o igual a:'
            || TO_CHAR (v_FEC_ELI, 'DD-MM-YYYY HH24:MI:SS');
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                      p_FEC_ICO_PCS,
                                                      p_COD_ERR_BIB,
                                                      p_COD_TPO_PSC,
                                                      p_ERROR);

         --Eliminamos detalle de proceso
         DELETE FROM   PABS_DT_SALDO_BILAT SB
               WHERE   SB.FEC_ING_CTL <= v_FEC_ELI;

         V_NUM_ELI := SQL%ROWCOUNT;  -- obtengo el numero de datos insertados.

         p_GLS_DET_PCS :=
               p_COD_UNI_PSC
            || ': Movimientos eliminados de PABS_DT_SALDO_BILAT: '
            || V_NUM_ELI;
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                      p_FEC_ICO_PCS,
                                                      p_COD_ERR_BIB,
                                                      p_COD_TPO_PSC,
                                                      p_ERROR);
      EXCEPTION
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            RAISE_APPLICATION_ERROR (-20000, err_msg);
      END;
   --Fin de eliminación saldos bilaterales.

   EXCEPTION
      WHEN DUP_VAL_ON_INDEX
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (
            p_COD_UNI_PSC || ': ' || err_msg,
            p_FEC_ICO_PCS,
            p_COD_ERR_BIB,
            p_COD_TPO_PSC,
            p_ERROR
         );
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (
            p_COD_UNI_PSC || ': ' || err_msg,
            p_FEC_ICO_PCS,
            p_COD_ERR_BIB,
            p_COD_TPO_PSC,
            p_ERROR
         );
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_ELI_SALDO_BILAT;

   --**************************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ELI_SALDO_AREA
   -- Objetivo: Al cierre del sistema se eliminan los registros de la tabla Saldos áreas para evitar acumulación
   --          de registros, esto se hará eliminando todos los registros
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_SALDO_BILAT
   -- Fecha: 22-11-2017
   -- Autor: Santander-CAH
   -- Input: p_FEC_PCS -> Fecha Proceso.
   -- Output: p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: 16/11/2016, Creacion Procedimiento.
   --***************************************************************************************************************
   PROCEDURE Sp_PAB_ELI_SALDO_AREA (p_FEC_PCS   IN     VARCHAR2,
                                    p_ERROR        OUT NUMBER)
   IS
      v_NOM_SP    VARCHAR2 (30) := 'Sp_PAB_ELI_SALDO_AREA';
      err_code    NUMBER := 0;
      V_NUM_ELI   NUMBER := 0;
      v_FEC_PCS   DATE := TO_DATE (p_FEC_PCS, 'DD-MM-YYYY');
   BEGIN
      BEGIN
         P_ERROR := PKG_PAB_CONSTANTES.V_OK;

         --Eliminacion de registros Bitacora detalle Proceso
         p_GLS_DET_PCS :=
               p_COD_UNI_PSC
            || ': Se eliminaran los registros de saldos áreas'
            || TO_CHAR (v_FEC_PCS, 'DD-MM-YYYY');
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                      p_FEC_ICO_PCS,
                                                      p_COD_ERR_BIB,
                                                      p_COD_TPO_PSC,
                                                      p_ERROR);

         --Eliminamos todos los registros de la tabla saldo áreas
         DELETE FROM   PABS_DT_SALDO_AREA SA
               WHERE   1 = 1;

         --esta tabla no tiene where, dado que tendra pocos registros y se necesita limpiar todos los días.

         V_NUM_ELI := SQL%ROWCOUNT;  -- obtengo el numero de datos eliminados.

         p_GLS_DET_PCS :=
               p_COD_UNI_PSC
            || ': Resgistros eliminados de  PABS_DT_SALDO_AREA: '
            || V_NUM_ELI;
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                      p_FEC_ICO_PCS,
                                                      p_COD_ERR_BIB,
                                                      p_COD_TPO_PSC,
                                                      p_ERROR);
      EXCEPTION
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            RAISE_APPLICATION_ERROR (-20000, err_msg);
      END;
   --Fin de eliminación saldos bilaterales.

   EXCEPTION
      WHEN DUP_VAL_ON_INDEX
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (
            p_COD_UNI_PSC || ': ' || err_msg,
            p_FEC_ICO_PCS,
            p_COD_ERR_BIB,
            p_COD_TPO_PSC,
            p_ERROR
         );
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (
            p_COD_UNI_PSC || ': ' || err_msg,
            p_FEC_ICO_PCS,
            p_COD_ERR_BIB,
            p_COD_TPO_PSC,
            p_ERROR
         );
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_ELI_SALDO_AREA;

   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_HIS_MENSAJERIA_CIERRE
   -- Objetivo: Al cierre del sistema se historifican las operaciones para la fecha ingresada
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas:PABS_DT_MNSJE_PRTCZ_HTRCA, PABS_DT_MNSJE_PRTCZ
   -- Fecha: 26/09/2016
   -- Autor: Santander
   -- Input: p_FEC_PCS -> Fecha Proceso.
   -- Output: p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: 27/04/2017, Creacion Procedimiento.
   --***********************************************************************************************
   PROCEDURE Sp_PAB_HIS_MENSAJERIA_CIERRE (p_FEC_PCS   IN     VARCHAR2,
                                           p_ERROR        OUT NUMBER)
   IS
      v_NOM_SP        VARCHAR2 (30) := 'Sp_PAB_HIS_MENSAJERIA_CIERRE';
      v_FEC_CGA_HIS   DATE := TO_DATE (p_FEC_PCS, 'DD-MM-YYYY');
      v_FEC_RES_HIS DATE
            := TO_DATE (p_FEC_PCS || ' 23:59:59', 'DD-MM-YYYY hh24:mi:ss')
               - PKG_PAB_CONSTANTES.V_DIA_RES_HIS ;
      v_NUM_OPE       NUMBER;
   BEGIN
      p_ERROR := PKG_PAB_CONSTANTES.v_OK;

      --Historificacion Nominas
      p_GLS_DET_PCS :=
            p_COD_UNI_PSC
         || ': Se historificaran los protocolos con fecha menor o igual a  :'
         || v_FEC_RES_HIS;
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);

      BEGIN
         INSERT INTO PABS_DT_MNSJE_PRTCZ_HTRCA (FEC_ISR_MSJ_PRZ,
                                                NUM_FOL_MSJ_PRZ,
                                                FEC_CGA_HIS,
                                                COD_ADC_EST,
                                                FLG_EGR_ING,
                                                COD_EST_AOS,
                                                COD_MT_SWF,
                                                FEC_ENV_RCP_SWF,
                                                HOR_ENV_REC_SWF,
                                                FLG_MRD_UTZ_SWF,
                                                NUM_SUB_MSJ_RLD,
                                                NUM_REF_SWF,
                                                NUM_REF_SWF_RLD,
                                                COD_BCO_BFC,
                                                GLS_EST_RCH,
                                                NUM_FOL_OPE_ORG,
                                                COD_TPO_OPE,
                                                COD_BCO_ORI,
                                                FEC_ISR_OPE_ORG)
            SELECT   FEC_ISR_MSJ_PRZ,
                     NUM_FOL_MSJ_PRZ,
                     v_FEC_CGA_HIS,
                     COD_ADC_EST,
                     FLG_EGR_ING,
                     COD_EST_AOS,
                     COD_MT_SWF,
                     FEC_ENV_RCP_SWF,
                     HOR_ENV_RCP_SWF,
                     FLG_MRD_UTZ_SWF,
                     NUM_SUB_MSJ_RLD,
                     NUM_REF_SWF,
                     NUM_REF_SWF_RLD,
                     COD_BCO_BFC,
                     GLS_EST_RCH,
                     NUM_FOL_OPE_ORI,
                     COD_TPO_OPE,
                     COD_BCO_ORI,
                     FEC_ISR_OPE_ORG
              FROM   PABS_DT_MNSJE_PRTCZ
             WHERE   FEC_ISR_MSJ_PRZ <= v_FEC_RES_HIS;

         v_NUM_OPE := SQL%ROWCOUNT;  -- obtengo el numero de datos insertados.

         p_GLS_DET_PCS :=
            p_COD_UNI_PSC || ': Mensajes historificadas:' || v_NUM_OPE;
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                      p_FEC_ICO_PCS,
                                                      p_COD_ERR_BIB,
                                                      p_COD_TPO_PSC,
                                                      p_ERROR);
      EXCEPTION
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            RAISE_APPLICATION_ERROR (-20000, err_msg);
      END;

      --Fin Logica Historificacion

      BEGIN
         --Eliminamos registros
         DELETE FROM   PABS_DT_MNSJE_PRTCZ
               WHERE   FEC_ISR_MSJ_PRZ <= v_FEC_RES_HIS;

         v_NUM_OPE := SQL%ROWCOUNT;  -- obtengo el numero de datos insertados.

         p_GLS_DET_PCS :=
               p_COD_UNI_PSC
            || ': Mensajes historificadas eliminados:'
            || v_NUM_OPE;
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                      p_FEC_ICO_PCS,
                                                      p_COD_ERR_BIB,
                                                      p_COD_TPO_PSC,
                                                      p_ERROR);
      EXCEPTION
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            RAISE_APPLICATION_ERROR (-20000, err_msg);
      END;
   ---Fin Eliminacion de registros tabla origen

   EXCEPTION
      WHEN DUP_VAL_ON_INDEX
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (
            p_COD_UNI_PSC || ': ' || err_msg,
            p_FEC_ICO_PCS,
            p_COD_ERR_BIB,
            p_COD_TPO_PSC,
            p_ERROR
         );
         RAISE_APPLICATION_ERROR (-20000, err_msg);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (
            p_COD_UNI_PSC || ': ' || err_msg,
            p_FEC_ICO_PCS,
            p_COD_ERR_BIB,
            p_COD_TPO_PSC,
            p_ERROR
         );
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_HIS_MENSAJERIA_CIERRE;

   /************************************************************************************************
    Funcion/Procedimiento: Sp_PAB_GESTION_CIERRE_MANUAL
    Objetivo: Gestiona el proceso de cierre manual
    Sistema: DBO_PAB.
    Base de Datos: DBO_PAB.
    Tablas Usadas:
    Fecha: 29/05/2017
    Autor: Santander
    Input: p_FEC_PCS -> Fecha Proceso.
    p_COD_USR      IN  VARCHAR2,
    p_COD_SIS_ENT  IN  CHAR,
    p_CURSOR       OUT SYS_REFCURSOR,
    p_DIA          OUT VARCHAR2,
    p_HORA         OUT VARCHAR2,
    p_ERROR        OUT NUMBER  )
    Output: p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
    Retorno: N/A.
    Observaciones: 14/09/2016, Creacion Procedimiento.
    FECHA        USER     MODIFICACIÓN
    16/08/2018   VARAYA   Se modifica p_CURSOR incorporando MT 900/910.
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_GESTION_CIERRE_MANUAL (
      p_COD_USR       IN     VARCHAR2,
      p_COD_SIS_ENT   IN     CHAR,
      p_CURSOR           OUT SYS_REFCURSOR,
      p_CURSOR2          OUT SYS_REFCURSOR,
      p_DIA              OUT VARCHAR2,
      p_HORA             OUT VARCHAR2,
      p_ERROR            OUT NUMBER
   )
   IS
      v_NOM_SP        VARCHAR2 (300) := 'Sp_PAB_GESTION_CIERRE_MANUAL';
      v_DES           VARCHAR2 (300);
      p_FEC_ICO_PCS   VARCHAR2 (30);
      p_NUM_TCK_PCS   NUMBER;
      p_GLS_DET_PCS   VARCHAR2 (300);
      p_COD_ERR_BIB   CHAR (6);
      p_COD_TPO_PSC   CHAR (3);
      v_CANT_REG      NUMBER;
   BEGIN
      -- INICIALIZACION DE VARIABLES Y PARAMETROS
      p_ERROR := 0;
      v_DES :=
         'Se lanza gestion cierre manual, fecha proceso: ' || p_COD_SIS_ENT;
      p_COD_ERR_BIB := '0';
      p_COD_TPO_PSC := 'PCM';

      --Inicia Logica Gestion, Traza 1
      p_GLS_DET_PCS := 'Iniciando proceso cierre manual';
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_CAB_PROCESO (p_COD_TPO_PSC,
                                                   p_FEC_ICO_PCS,
                                                   p_NUM_TCK_PCS,
                                                   p_ERROR);

      BEGIN
         OPEN p_CURSOR FOR
              SELECT   COUNT (DETOPE.COD_EST_AOS) AS CANT,
                       EST.DSC_EST_AOS AS DSC
                FROM   PABS_DT_DETLL_OPRCN DETOPE,
                       PABS_DT_ESTDO_ALMOT EST,
                       PABS_DT_SISTM_ENTRD_SALID ENT
               WHERE   DETOPE.COD_EST_AOS = EST.COD_EST_AOS
                       AND DETOPE.COD_SIS_ENT = ENT.COD_SIS_ENT_SAL
                       AND DETOPE.COD_EST_AOS IN
                                (PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT, -- Por autorizar
                                 PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPVI, -- Por visar
                                 PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEENV, -- Por enviar
                                 PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAT, -- Por Autorizar
                                 PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPELIB, -- Liberada
                                 PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPDV, -- Por Devolver
                                 PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAB, -- Por abonar
                                 PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAS, -- Por asociar
                                 PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEREG) -- Recibida)
                       AND TO_DATE (DETOPE.FEC_ISR_OPE, 'DD/MM/RR') =
                             TO_DATE (SYSDATE, 'DD/MM/RR')
                       AND DETOPE.FEC_VTA = TO_DATE (SYSDATE, 'DD/MM/RR') --Fecha Valuta Igual
            GROUP BY   DETOPE.COD_EST_AOS, EST.DSC_EST_AOS
            UNION
              SELECT   COUNT (DETCAJ.COD_MT_SWF) CANT, '900 Sin Identificar'
                FROM   PABS_DT_DETLL_CARGO_ABONO_CAJA DETCAJ
               WHERE   DETCAJ.COD_TPO_OPE_AOS =
                          PKG_PAB_CONSTANTES.V_STR_TPO_OPE_SIDEN
                       AND DETCAJ.COD_SIS_ENT IS NULL
                       AND DETCAJ.COD_MT_SWF = PKG_PAB_CONSTANTES.V_COD_MT900
            GROUP BY   DETCAJ.COD_MT_SWF
            UNION
              SELECT   COUNT (DETCAJ.COD_MT_SWF) CANT, '910 Sin Identificar'
                FROM   PABS_DT_DETLL_CARGO_ABONO_CAJA DETCAJ
               WHERE   DETCAJ.COD_TPO_OPE_AOS =
                          PKG_PAB_CONSTANTES.V_STR_TPO_OPE_SIDEN
                       AND DETCAJ.COD_SIS_SAL IS NULL
                       AND DETCAJ.COD_MT_SWF = PKG_PAB_CONSTANTES.V_COD_MT910
            GROUP BY   DETCAJ.COD_MT_SWF;


         v_CANT_REG := SQL%ROWCOUNT; -- obtengo el numero de datos seleccionados
      END;

      BEGIN
         OPEN p_CURSOR2 FOR
            SELECT   REPLACE (
                        TO_CHAR (SYSDATE,
                                 'Day  dd month yyyy',
                                 'NLS_DATE_LANGUAGE=SPANISH'),
                        '  '
                     )
                        AS p_DIA,
                     TO_CHAR (SYSDATE, 'HH24:MI:SS') AS p_HORA
              FROM   DUAL;
      END;

      IF (v_CANT_REG > 0)
      THEN
         -- Inserta Detalle
         p_GLS_DET_PCS :=
            'Cierre manual correcto, ejecutado por  ' || TRIM (p_COD_USR);
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                      p_FEC_ICO_PCS,
                                                      p_COD_ERR_BIB,
                                                      p_COD_TPO_PSC,
                                                      p_ERROR);
      ELSE
         -- Inserta Detalle
         p_GLS_DET_PCS :=
            'Cierre manual con operaciones pendientes, ejecutado por  '
            || TRIM (p_COD_USR);
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                      p_FEC_ICO_PCS,
                                                      p_COD_ERR_BIB,
                                                      p_COD_TPO_PSC,
                                                      p_ERROR);
      END IF;

      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
      p_GLS_DET_PCS := 'Fin proceso cierre manual exitoso';

      --Fin Cabecera
      PKG_PAB_TRACKING.Sp_PAB_UPD_BIT_CAB_PROCESO (p_FEC_ICO_PCS,
                                                   p_COD_TPO_PSC,
                                                   p_NUM_TCK_PCS,
                                                   p_ERROR,
                                                   p_ERROR);
   --Fin Logica Gestion

   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (err_msg,
                                                      p_FEC_ICO_PCS,
                                                      p_COD_ERR_BIB,
                                                      p_COD_TPO_PSC,
                                                      p_ERROR);
         PKG_PAB_TRACKING.Sp_PAB_UPD_BIT_CAB_PROCESO (
            p_FEC_ICO_PCS,
            p_COD_TPO_PSC,
            p_NUM_TCK_PCS,
            PKG_PAB_CONSTANTES.V_ERROR,
            p_ERROR
         );

         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (err_msg,
                                                      p_FEC_ICO_PCS,
                                                      p_COD_ERR_BIB,
                                                      p_COD_TPO_PSC,
                                                      p_ERROR);
         PKG_PAB_TRACKING.Sp_PAB_UPD_BIT_CAB_PROCESO (
            p_FEC_ICO_PCS,
            p_COD_TPO_PSC,
            p_NUM_TCK_PCS,
            PKG_PAB_CONSTANTES.V_ERROR,
            p_ERROR
         );
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_GESTION_CIERRE_MANUAL;

   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_HIS_DETALLE_MNSJE_CIERRE
   -- Objetivo: Al cierre del sistema se historifican las operaciones para la fecha ingresada
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas:PABS_DT_DETLL_MNSJE_PRTCZ_HTRCA, PABS_DT_DETLL_MNSJE_PRTCZ
   -- Fecha: 26/09/2016
   -- Autor: Santander
   -- Input: p_FEC_PCS -> Fecha Proceso.
   -- Output: p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: 27/04/2017, Creacion Procedimiento.
   --***********************************************************************************************
   PROCEDURE Sp_PAB_HIS_DET_MNSJE_CIERRE (p_FEC_PCS   IN     VARCHAR2,
                                          p_ERROR        OUT NUMBER)
   IS
      v_NOM_SP        VARCHAR2 (30) := 'Sp_PAB_HIS_DET_MNSJE_CIERRE';
      v_FEC_CGA_HIS   DATE := TO_DATE (p_FEC_PCS, 'DD-MM-YYYY');
      v_FEC_RES_HIS DATE
            := TO_DATE (p_FEC_PCS || ' 23:59:59', 'DD-MM-YYYY hh24:mi:ss')
               - PKG_PAB_CONSTANTES.V_DIA_RES_HIS ;
      v_NUM_OPE       NUMBER;
   BEGIN
      p_ERROR := PKG_PAB_CONSTANTES.v_OK;

      --Historificacion  detalle Nominas
      p_GLS_DET_PCS :=
         p_COD_UNI_PSC
         || ': Se historificaran el detalle de mensajeria con fecha menor o igual a  :'
         || v_FEC_RES_HIS;
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);

      BEGIN
         INSERT INTO PABS_DT_DETLL_MNSJE_PRTCZ_HTRC (FEC_ISR_MSJ_PRZ,
                                                     NUM_FOL_MSJ_PRZ,
                                                     NUM_CAM_REG,
                                                     FEC_CGA_HIS,
                                                     GLS_TXT_LBE_MSJ)
            SELECT   FEC_ISR_MSJ_PRZ,
                     NUM_FOL_MSJ_PRZ,
                     NUM_CAM_REG,
                     v_FEC_CGA_HIS,
                     GLS_TXT_LBE_MSJ
              FROM   PABS_DT_DETLL_MNSJE_PRTCZ
             WHERE   FEC_ISR_MSJ_PRZ <= v_FEC_RES_HIS;

         v_NUM_OPE := SQL%ROWCOUNT;  -- obtengo el numero de datos insertados.

         --Historificacion  detalle Nominas
         p_GLS_DET_PCS :=
               p_COD_UNI_PSC
            || ': Detalle Mensajes historificados:'
            || v_NUM_OPE;
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                      p_FEC_ICO_PCS,
                                                      p_COD_ERR_BIB,
                                                      p_COD_TPO_PSC,
                                                      p_ERROR);
      EXCEPTION
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            RAISE_APPLICATION_ERROR (-20000, err_msg);
      END;

      --Fin Logica Historificacion

      BEGIN
         --Eliminamos los mensajes
         DELETE FROM   PABS_DT_DETLL_MNSJE_PRTCZ
               WHERE   FEC_ISR_MSJ_PRZ <= v_FEC_RES_HIS;

         v_NUM_OPE := SQL%ROWCOUNT;  -- obtengo el numero de datos insertados.

         p_GLS_DET_PCS :=
               p_COD_UNI_PSC
            || ': Detalle Mensajes historificados eliminados:'
            || v_NUM_OPE;
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                      p_FEC_ICO_PCS,
                                                      p_COD_ERR_BIB,
                                                      p_COD_TPO_PSC,
                                                      p_ERROR);
      EXCEPTION
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            RAISE_APPLICATION_ERROR (-20000, err_msg);
      END;
   ---Fin Eliminacion de registros tabla origen

   EXCEPTION
      WHEN DUP_VAL_ON_INDEX
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (
            p_COD_UNI_PSC || ': ' || err_msg,
            p_FEC_ICO_PCS,
            p_COD_ERR_BIB,
            p_COD_TPO_PSC,
            p_ERROR
         );
         RAISE_APPLICATION_ERROR (-20000, err_msg);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (
            p_COD_UNI_PSC || ': ' || err_msg,
            p_FEC_ICO_PCS,
            p_COD_ERR_BIB,
            p_COD_TPO_PSC,
            p_ERROR
         );
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_HIS_DET_MNSJE_CIERRE;

   --**************************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ELI_900_910
   -- Objetivo: Al cierre del sistema se eliminan los registros de los protocolos 900/910
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_CARGO_ABONO_CAJA
   -- Fecha: 16/11/2017
   -- Autor: Gonzalo Correa
   -- Input: p_FEC_PCS -> Fecha Proceso.
   -- Output: p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: 28/11/2017, Creacion Procedimiento.
   --***************************************************************************************************************
   PROCEDURE Sp_PAB_ELI_900_910 (p_FEC_PCS IN VARCHAR2, p_ERROR OUT NUMBER)
   IS
      v_NOM_SP    VARCHAR2 (30) := 'Sp_PAB_ELI_900_910';
      v_NUM_LOG   NUMBER;
      v_FEC_ELI DATE
            := TO_DATE (p_FEC_PCS || ' 23:59:59', 'DD-MM-YYYY hh24:mi:ss')
               - PKG_PAB_CONSTANTES.V_DIA_ELI_900_910 ;
   BEGIN
      p_ERROR := PKG_PAB_CONSTANTES.v_OK;

      p_GLS_DET_PCS :=
         p_COD_UNI_PSC || ': Se eliminaran todos los movimientos de 900/910 ';
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);

      BEGIN
         DELETE FROM   PABS_DT_DETLL_CARGO_ABONO_CAJA
               WHERE   NUM_FOL_OPE > 0 AND FEC_ING_OPE <= v_FEC_ELI;

         v_NUM_LOG := SQL%ROWCOUNT;  -- obtengo el numero de datos insertados.

         p_GLS_DET_PCS :=
               p_COD_UNI_PSC
            || ': Movimientos de 900/910 eliminados:'
            || v_NUM_LOG;
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                      p_FEC_ICO_PCS,
                                                      p_COD_ERR_BIB,
                                                      p_COD_TPO_PSC,
                                                      p_ERROR);
      EXCEPTION
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            RAISE_APPLICATION_ERROR (-20000, err_msg);
      END;
   --Fin Eliminacion de registros tabla LOG

   EXCEPTION
      WHEN DUP_VAL_ON_INDEX
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (
            p_COD_UNI_PSC || ': ' || err_msg,
            p_FEC_ICO_PCS,
            p_COD_ERR_BIB,
            p_COD_TPO_PSC,
            p_ERROR
         );
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (
            p_COD_UNI_PSC || ': ' || err_msg,
            p_FEC_ICO_PCS,
            p_COD_ERR_BIB,
            p_COD_TPO_PSC,
            p_ERROR
         );
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_ELI_900_910;

   --**************************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ELI_MOV_CAJA
   -- Objetivo: Al cierre del sistema se eliminan los registros de la tabla Movimientos de Caja para evitar acumulación
   --          de registros, esto se hará eliminando registros que sean menores a la fecha de proceso en su fecha
   --           valuta
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_BTCOR_PRCSO, PABS_DT_BTCOR_DETLL_PRCSO, PABS_DT_DETLL_MVNTO_CAJA
   -- Fecha: 03/11/2017
   -- Autor: Santander-CAH
   -- Input: p_FEC_PCS -> Fecha Proceso.
   -- Output: p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: 14/11/2016, Creacion Procedimiento.
   --***************************************************************************************************************
   PROCEDURE Sp_PAB_ELI_MOV_CAJA (p_FEC_PCS IN VARCHAR2, p_ERROR OUT NUMBER)
   IS
      v_NOM_SP        VARCHAR2 (30) := 'Sp_PAB_ELI_MOV_CAJA';
      v_FEC_ELI_HIS   DATE := TO_DATE (p_FEC_PCS, 'DD-MM-YYYY');
      V_NUM_ELI       NUMBER;
   BEGIN
      P_ERROR := PKG_PAB_CONSTANTES.V_OK;

      --Eliminacion de registros Bitacora detalle Proceso
      p_GLS_DET_PCS :=
            p_COD_UNI_PSC
         || ': Se eliminaran los registros de movimientos de Caja'
         || TO_CHAR (v_FEC_ELI_HIS, 'DD-MM-YYYY');
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);

      --Eliminamos detalle de proceso
      DELETE FROM   PABS_DT_DETLL_MVNTO_CAJA MVT
            WHERE   MVT.FEC_VTA <= v_FEC_ELI_HIS;

      V_NUM_ELI := SQL%ROWCOUNT;     -- obtengo el numero de datos insertados.

      p_GLS_DET_PCS :=
            p_COD_UNI_PSC
         || ': Movimientos eliminadas PABS_DT_DETLL_MVNTO_CAJA: '
         || V_NUM_ELI;
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);
   EXCEPTION
      WHEN DUP_VAL_ON_INDEX
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (
            p_COD_UNI_PSC || ': ' || err_msg,
            p_FEC_ICO_PCS,
            p_COD_ERR_BIB,
            p_COD_TPO_PSC,
            p_ERROR
         );
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (
            p_COD_UNI_PSC || ': ' || err_msg,
            p_FEC_ICO_PCS,
            p_COD_ERR_BIB,
            p_COD_TPO_PSC,
            p_ERROR
         );
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_ELI_MOV_CAJA;

   --**************************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ELI_CART
   -- Objetivo: Al cierre del sistema se eliminan los registros de la tabla CARTOLAS para evitar acumulación
   --          de registros, esto se hará eliminando registros que cumplan más de x días, para eliminar se crea la
   --          constante PKG_PAB_CONSTANTES.V_DIA_ELI_CAR que inicialmente tendra valor 7.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_BTCOR_PRCSO, PABS_DT_BTCOR_DETLL_PRCSO, PABS_DT_CBCRA_CRTLA, PABS_DT_DETLL_CRTLA
   -- Fecha: 03/11/2017
   -- Autor: Santander-CAH
   -- Input: p_FEC_PCS -> Fecha Proceso.
   -- Output: p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: 14/11/2016, Creacion Procedimiento.
   --***************************************************************************************************************
   PROCEDURE Sp_PAB_ELI_CART (p_FEC_PCS IN VARCHAR2, p_ERROR OUT NUMBER)
   IS
      v_NOM_SP    VARCHAR2 (30) := 'Sp_PAB_ELI_CART';
      v_FEC_ELI_HIS DATE
            := TO_DATE (p_FEC_PCS, 'DD-MM-YYYY')
               - PKG_PAB_CONSTANTES.V_DIA_ELI_CAR ;
      err_code    NUMBER := 0;
      V_NUM_ELI   NUMBER := 0;
   BEGIN
      BEGIN
         P_ERROR := PKG_PAB_CONSTANTES.V_OK;

         --Eliminacion de registros Bitacora detalle Proceso
         p_GLS_DET_PCS :=
            p_COD_UNI_PSC
            || ': Se eliminaran los registros de CARTOLAS, con fecha menor o igual a:'
            || TO_CHAR (v_FEC_ELI_HIS, 'DD-MM-YYYY HH24:MI:SS');
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                      p_FEC_ICO_PCS,
                                                      p_COD_ERR_BIB,
                                                      p_COD_TPO_PSC,
                                                      p_ERROR);

         --Eliminamos cabecera de proceso
         DELETE FROM   PABS_DT_CBCRA_CRTLA
               WHERE   FEC_ING_CTL <= v_FEC_ELI_HIS;

         V_NUM_ELI := SQL%ROWCOUNT;  -- obtengo el numero de datos insertados.

         p_GLS_DET_PCS :=
               p_COD_UNI_PSC
            || ': CABECERA CARTOLAS eliminadas PABS_DT_CBCRA_CRTLA: '
            || V_NUM_ELI;
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                      p_FEC_ICO_PCS,
                                                      p_COD_ERR_BIB,
                                                      p_COD_TPO_PSC,
                                                      p_ERROR);

         --Eliminamos cabecera de proceso
         DELETE FROM   PABS_DT_DETLL_CRTLA
               WHERE   FEC_TRN_OPE <= v_FEC_ELI_HIS AND NUM_FOL_OPE > 0;

         V_NUM_ELI := SQL%ROWCOUNT;  -- obtengo el numero de datos insertados.

         p_GLS_DET_PCS :=
               p_COD_UNI_PSC
            || ': CARTOLAS eliminadas PABS_DT_DETLL_CRTLA: '
            || V_NUM_ELI;
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                      p_FEC_ICO_PCS,
                                                      p_COD_ERR_BIB,
                                                      p_COD_TPO_PSC,
                                                      p_ERROR);
      EXCEPTION
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            RAISE_APPLICATION_ERROR (-20000, err_msg);
      END;
   --Fin de eliminación notificaciones.

   EXCEPTION
      WHEN DUP_VAL_ON_INDEX
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (
            p_COD_UNI_PSC || ': ' || err_msg,
            p_FEC_ICO_PCS,
            p_COD_ERR_BIB,
            p_COD_TPO_PSC,
            p_ERROR
         );
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (
            p_COD_UNI_PSC || ': ' || err_msg,
            p_FEC_ICO_PCS,
            p_COD_ERR_BIB,
            p_COD_TPO_PSC,
            p_ERROR
         );
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_ELI_CART;

   --********************************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ELI_RGTRO_CARGO_ABONO
   -- Objetivo: Al cierre del sistema se eliminan los registros de la tabla de registro de cargo y abono del aplicativo
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_RGTRO_OPRCN_CARGO_ABON
   -- Fecha: 06-12-2017
   -- Autor: Santander - CAH
   -- Input: p_FEC_PCS -> Fecha Proceso.
   -- Output: p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: 06-12-2017, Creacion Procedimiento.
   --********************************************************************************************************************
   PROCEDURE Sp_PAB_ELI_RGTRO_CARGO_ABONO (p_FEC_PCS   IN     VARCHAR2,
                                           p_ERROR        OUT NUMBER)
   IS
      v_NOM_SP    VARCHAR2 (30) := 'Sp_PAB_ELI_RGTRO_CARGO_ABONO';
      v_NUM_LOG   NUMBER;
      v_FEC_ELI   DATE := TO_DATE (p_FEC_PCS, 'DD-MM-YYYY') - 1;
   BEGIN
      p_ERROR := PKG_PAB_CONSTANTES.v_OK;

      p_GLS_DET_PCS :=
         p_COD_UNI_PSC
         || ': Se eliminaran todos los registros de cargo y abono';
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);

      BEGIN
         DELETE FROM   PABS_DT_RGTRO_OPRCN_CARGO_ABON RB
               WHERE       RB.FEC_CGO_ABN < v_FEC_ELI
                       AND TRUNC (FEC_ISR_OPE) < v_FEC_ELI
                       AND NUM_MOV_CTB > '0';

         v_NUM_LOG := SQL%ROWCOUNT;  -- obtengo el numero de datos insertados.

         p_GLS_DET_PCS :=
               p_COD_UNI_PSC
            || ': Registros de cargo y abono eliminados:'
            || v_NUM_LOG;
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                      p_FEC_ICO_PCS,
                                                      p_COD_ERR_BIB,
                                                      p_COD_TPO_PSC,
                                                      p_ERROR);
      EXCEPTION
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            RAISE_APPLICATION_ERROR (-20000, err_msg);
      END;
   --Fin Eliminacion de registros tabla Cargo y Abono

   EXCEPTION
      WHEN DUP_VAL_ON_INDEX
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (
            p_COD_UNI_PSC || ': ' || err_msg,
            p_FEC_ICO_PCS,
            p_COD_ERR_BIB,
            p_COD_TPO_PSC,
            p_ERROR
         );
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (
            p_COD_UNI_PSC || ': ' || err_msg,
            p_FEC_ICO_PCS,
            p_COD_ERR_BIB,
            p_COD_TPO_PSC,
            p_ERROR
         );
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_ELI_RGTRO_CARGO_ABONO;

   --**************************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ACT_SLDO_MX
   -- Objetivo: PROCEDIMIENTO QUE ACTUALIZA LOS SALDOS AL CIERRE DEL DIA, CAMBIANDO LOS SALDOS PROYECTADOS DIA, 24H, 48H.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_BTCOR_PRCSO, PABS_DT_BTCOR_DETLL_PRCSO, PABS_DT_CBCRA_CRTLA, PABS_DT_DETLL_CRTLA
   -- Fecha: 03/11/2017
   -- Autor: Santander-CAH
   -- Input: N/A
   -- Output: p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: 14/11/2016, Creacion Procedimiento.
   --***************************************************************************************************************
   PROCEDURE Sp_PAB_ACT_SLDO_MX (p_ERROR OUT NUMBER)
   IS
      v_NOM_SP            VARCHAR2 (30) := 'Sp_PAB_ACT_SLDO_MX';
      err_code            NUMBER := 0;
      V_NUM_ELI           NUMBER := 0;
      v_FEC_INS_OPE       DATE := TRUNC (SYSDATE);        -- FECHA DEL PROCESO
      v_FEC_48HS          DATE := TRUNC (SYSDATE + 2);

      v_IMP_INI_CTL       NUMBER (15, 2);
      v_IMP_ING_CTL       NUMBER (15, 2);
      v_IMP_EGR_CTL       NUMBER (15, 2);
      v_IMP_FNL_CTL       NUMBER (15, 2);
      v_IMP_APR_AOS       NUMBER (15, 2);
      v_IMP_ING_DIA       NUMBER (15, 2);
      v_IMP_EGR_DIA       NUMBER (15, 2);
      v_IMP_RAL_AOS       NUMBER (15, 2);
      v_IMP_ING_CPR       NUMBER (15, 2);
      v_IMP_EGR_CPR       NUMBER (15, 2);
      v_IMP_TOT_FNL_DIA   NUMBER (15, 2);
      v_IMP_ING_24        NUMBER (15, 2);
      v_IMP_EGR_24        NUMBER (15, 2);
      v_IMP_TOT_24        NUMBER (15, 2);
      v_IMP_ING_48        NUMBER (15, 2);
      v_IMP_EGR_48        NUMBER (15, 2);
      v_IMP_TOT_48        NUMBER (15, 2);
      v_IMP_ING_OVR       NUMBER (15, 2);
      v_IMP_EGR_OVR       NUMBER (15, 2);
      v_IMP_TOT_OVR       NUMBER (15, 2);
      v_FEC_PROC          DATE;
      v_FEC_CIERRE        DATE := TRUNC (SYSDATE);
   BEGIN
      P_ERROR := PKG_PAB_CONSTANTES.V_OK;

      --Eliminacion de registros Bitacora detalle Proceso
      p_GLS_DET_PCS :=
         p_COD_UNI_PSC || ': Se actualizaran saldos Corresponsales MX';
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);

      BEGIN
         OPEN C_CONSULTA_CTAS_MX;
      EXCEPTION
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            RAISE_APPLICATION_ERROR (-20000, err_msg);
      END;

      BEGIN
         FETCH C_CONSULTA_CTAS_MX INTO   R_CONSULTA_CTAS_MX;
      EXCEPTION
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);

            --Verificamos si el cursor se encuentra abierto en caso de error.
            IF (C_CONSULTA_CTAS_MX%ISOPEN)
            THEN
               CLOSE C_CONSULTA_CTAS_MX;
            END IF;

            RAISE_APPLICATION_ERROR (-20000, err_msg);
      END;

      WHILE C_CONSULTA_CTAS_MX%FOUND
      LOOP
         --PROCESO DE TRASPASO DE LA INFORMACION.

         -- SOLO SE DEBEN TOMAR LOS SALDOS DE LAS CUENTAS QUE NO TENGAN CARTOLA EL MISMO DIA.
         BEGIN
            SELECT   TRUNC (FEC_ING_CTL)
              INTO   v_FEC_PROC
              FROM   PABS_DT_DETLL_SALDO_CAJA_MN_MX
             WHERE       COD_BCO_ORG = R_CONSULTA_CTAS_MX.COD_BCO_ORG
                     AND NUM_CTA_CTE = R_CONSULTA_CTAS_MX.NUM_CTA_CTE
                     AND COD_DVI = R_CONSULTA_CTAS_MX.COD_DVI;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               err_code := SQLCODE;
               err_msg :=
                     'No se pudo obtener la información de banco :'
                  || R_CONSULTA_CTAS_MX.COD_BCO_ORG
                  || ' - cuenta : '
                  || R_CONSULTA_CTAS_MX.NUM_CTA_CTE
                  || ' - divisa : '
                  || R_CONSULTA_CTAS_MX.COD_DVI;
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            WHEN OTHERS
            THEN
               err_code := SQLCODE;
               err_msg := SUBSTR (SQLERRM, 1, 300);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);

               --Verificamos si el cursor se encuentra abierto en caso de error.
               IF (C_CONSULTA_CTAS_MX%ISOPEN)
               THEN
                  CLOSE C_CONSULTA_CTAS_MX;
               END IF;

               RAISE_APPLICATION_ERROR (-20000, err_msg);
         END;

         IF v_FEC_PROC <> v_FEC_CIERRE
         THEN
            -- SOLO PROCESA LPS BANCOS QUE NO TENGAN CARTOLAS DEL DIA.
            BEGIN
               SELECT   IMP_INI_CTL,
                        IMP_ING_CTL,
                        IMP_EGR_CTL,
                        IMP_FNL_CTL,
                        IMP_APR_AOS,
                        IMP_ING_DIA,
                        IMP_EGR_DIA,
                        IMP_RAL_AOS,
                        IMP_ING_CPR,
                        IMP_EGR_CPR,
                        IMP_TOT_FNL_DIA,
                        IMP_ING_24,
                        IMP_EGR_24,
                        IMP_TOT_24,
                        IMP_ING_48,
                        IMP_EGR_48,
                        IMP_TOT_48,
                        IMP_ING_OVR,
                        IMP_EGR_OVR,
                        IMP_TOT_OVR
                 INTO   v_IMP_INI_CTL,
                        v_IMP_ING_CTL,
                        v_IMP_EGR_CTL,
                        v_IMP_FNL_CTL,
                        v_IMP_APR_AOS,
                        v_IMP_ING_DIA,
                        v_IMP_EGR_DIA,
                        v_IMP_RAL_AOS,
                        v_IMP_ING_CPR,
                        v_IMP_EGR_CPR,
                        v_IMP_TOT_FNL_DIA,
                        v_IMP_ING_24,
                        v_IMP_EGR_24,
                        v_IMP_TOT_24,
                        v_IMP_ING_48,
                        v_IMP_EGR_48,
                        v_IMP_TOT_48,
                        v_IMP_ING_OVR,
                        v_IMP_EGR_OVR,
                        v_IMP_TOT_OVR
                 FROM   PABS_DT_DETLL_SALDO_CAJA_MN_MX
                WHERE       COD_BCO_ORG = R_CONSULTA_CTAS_MX.COD_BCO_ORG
                        AND NUM_CTA_CTE = R_CONSULTA_CTAS_MX.NUM_CTA_CTE
                        AND COD_DVI = R_CONSULTA_CTAS_MX.COD_DVI;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  err_code := SQLCODE;
                  err_msg :=
                        'No se pudo obtener la información de banco :'
                     || R_CONSULTA_CTAS_MX.COD_BCO_ORG
                     || ' - cuenta : '
                     || R_CONSULTA_CTAS_MX.NUM_CTA_CTE
                     || ' - divisa : '
                     || R_CONSULTA_CTAS_MX.COD_DVI;
                  PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                      v_NOM_PCK,
                                                      err_msg,
                                                      v_NOM_SP);
                  p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
               WHEN OTHERS
               THEN
                  err_code := SQLCODE;
                  err_msg := SUBSTR (SQLERRM, 1, 300);
                  p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
                  PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                      v_NOM_PCK,
                                                      err_msg,
                                                      v_NOM_SP);

                  --Verificamos si el cursor se encuentra abierto en caso de error.
                  IF (C_CONSULTA_CTAS_MX%ISOPEN)
                  THEN
                     CLOSE C_CONSULTA_CTAS_MX;
                  END IF;

                  RAISE_APPLICATION_ERROR (-20000, err_msg);
            END;

            -- ACTUALIZACION DE SALDOS
            UPDATE   PABS_DT_DETLL_SALDO_CAJA_MN_MX
               SET   IMP_APR_AOS = v_IMP_FNL_CTL,
                     IMP_ING_CPR = v_IMP_ING_24,
                     IMP_EGR_CPR = v_IMP_EGR_24,
                     IMP_TOT_FNL_DIA =
                        v_IMP_FNL_CTL + v_IMP_ING_24 + v_IMP_EGR_24, --v_IMP_TOT_24,
                     IMP_ING_24 = v_IMP_ING_48,
                     IMP_EGR_24 = v_IMP_EGR_48,
                     IMP_TOT_24 =
                          v_IMP_FNL_CTL
                        + v_IMP_ING_24
                        + v_IMP_EGR_24
                        + v_IMP_ING_48
                        + v_IMP_EGR_48,                        --v_IMP_TOT_48,
                     IMP_ING_48 = 0,
                     IMP_EGR_48 = 0,
                     IMP_TOT_48 =
                        NVL (
                             v_IMP_FNL_CTL
                           + v_IMP_ING_24
                           + v_IMP_EGR_24
                           + v_IMP_ING_48
                           + v_IMP_EGR_48,
                           0
                        ),
                     --------SALDO INTRADIA REAL-------------
                     IMP_ING_DIA = 0,
                     IMP_EGR_DIA = 0,
                     IMP_RAL_AOS = v_IMP_FNL_CTL
             ----------------------
             WHERE       COD_BCO_ORG = R_CONSULTA_CTAS_MX.COD_BCO_ORG
                     AND NUM_CTA_CTE = R_CONSULTA_CTAS_MX.NUM_CTA_CTE
                     AND COD_DVI = R_CONSULTA_CTAS_MX.COD_DVI;
         END IF;

         FETCH C_CONSULTA_CTAS_MX INTO   R_CONSULTA_CTAS_MX;
      END LOOP;

      CLOSE C_CONSULTA_CTAS_MX;
   EXCEPTION
      WHEN OTHERS
      THEN
         --Verificamos si el cursor se encuentra abierto en caso de error.
         IF (C_CONSULTA_CTAS_MX%ISOPEN)
         THEN
            CLOSE C_CONSULTA_CTAS_MX;
         END IF;

         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_ACT_SLDO_MX;


   /********************************************************************************************
    FUNCION/PROCEDIMIENTO: SP_PAB_ACT_900_910.
    OBJETIVO             : ACTUALIZA EL CAMPO COD_TPO_OPE_AOS A 'OOFF' CUANDO EL MT := 900/910
                           Y EL CANAL DE ENTRADA O SALIDA SEA NULL.
    SISTEMA              : DBO_PAB.
    BASE DE DATOS        : DBO_PAB.
    TABLAS USADAS        : PABS_DT_DETLL_CARGO_ABONO_CAJA.
    FECHA                : 17/08/2018.
    AUTOR                : VARAYA.
    INPUT                : P_FEC_PCS := FECHA PROCESO.
    OUTPUT               : P_ERROR   := INDICADOR DE ERRORES, MANEJA DOS ESTADOS, 0 SIN ERRORES 1
                           EXISTEN ERRORES.
                           P_ERR_MSG := GLOSA QUE INDICA EL ERROR.
    OBSERVACIONES        : 17-08-2018, CREACIÓN SP.
   *********************************************************************************************/
   PROCEDURE SP_PAB_ACT_900_910 (P_FEC_PCS IN VARCHAR2, P_ERROR OUT NUMBER)
   IS
      V_NOM_SP      VARCHAR2 (30) := 'SP_PAB_ACT_900_910';
      V_CANT_OPE    NUMBER;
      V_CANT_OPE1   NUMBER;
   BEGIN
      P_ERROR := PKG_PAB_CONSTANTES.V_OK;

      --ACTUALIZO LOS CAMPOS DE ENTRADA Y SALIDA SEGUN MT.

      UPDATE   PABS_DT_DETLL_CARGO_ABONO_CAJA
         SET   COD_SIS_ENT = PKG_PAB_CONSTANTES.V_COD_TIP_BOLSA        --Bolsa
       WHERE       COD_MT_SWF = PKG_PAB_CONSTANTES.V_COD_MT900
               AND COD_TPO_OPE_AOS = PKG_PAB_CONSTANTES.V_STR_TPO_OPE_SIDEN
               AND COD_SIS_ENT IS NULL;

      --GUARDO LA CANTIDAD DE REGISTROS ACTUALIZADOS (MT900).
      V_CANT_OPE := SQL%ROWCOUNT;

      UPDATE   PABS_DT_DETLL_CARGO_ABONO_CAJA
         SET   COD_SIS_SAL = PKG_PAB_CONSTANTES.V_COD_TIP_BOLSA
       WHERE       COD_MT_SWF = PKG_PAB_CONSTANTES.V_COD_MT910
               AND COD_TPO_OPE_AOS = PKG_PAB_CONSTANTES.V_STR_TPO_OPE_SIDEN
               AND COD_SIS_SAL IS NULL;

      --GUARDO LA CANTIDAD DE REGISTROS ACTUALIZADOS (MT910).
      V_CANT_OPE1 := SQL%ROWCOUNT;

      --INFORMO LOS REGISTROS QUE FUERON ACTUALIZADOS PARA EL MT900 Y MT910.
      IF V_CANT_OPE > 0 AND V_CANT_OPE1 > 0
      THEN
         P_GLS_DET_PCS :=
               p_COD_UNI_PSC
            || ': Actualización: '
            || V_CANT_OPE
            || ' folio(s) (MT900) y '
            || V_CANT_OPE1
            || ' folio(s) (MT910) del día '
            || P_FEC_PCS;
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (P_GLS_DET_PCS,
                                                      p_FEC_ICO_PCS,
                                                      P_COD_ERR_BIB,
                                                      P_COD_TPO_PSC,
                                                      P_ERROR);
      --INFORMO LOS REGISTROS QUE SOLO FUERON ACTUALIZADOS PARA EL MT900.
      ELSIF V_CANT_OPE > 0 AND V_CANT_OPE1 = 0
      THEN
         P_GLS_DET_PCS :=
               p_COD_UNI_PSC
            || ': Actualización: '
            || V_CANT_OPE
            || ' folio(s) (MT900) del día '
            || P_FEC_PCS;
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (P_GLS_DET_PCS,
                                                      p_FEC_ICO_PCS,
                                                      P_COD_ERR_BIB,
                                                      P_COD_TPO_PSC,
                                                      P_ERROR);
      --INFORMO LOS REGISTROS QUE SOLO FUERON ACTUALIZADOS PARA EL MT910.
      ELSIF V_CANT_OPE = 0 AND V_CANT_OPE1 > 0
      THEN
         P_GLS_DET_PCS :=
               p_COD_UNI_PSC
            || ': Actualización: '
            || V_CANT_OPE1
            || ' folio(s) (MT910) del día '
            || P_FEC_PCS;
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (P_GLS_DET_PCS,
                                                      p_FEC_ICO_PCS,
                                                      P_COD_ERR_BIB,
                                                      P_COD_TPO_PSC,
                                                      P_ERROR);
      --INFORMO CERO REGISTROS.
      ELSIF V_CANT_OPE = 0 AND V_CANT_OPE1 = 0
      THEN
         P_GLS_DET_PCS :=
               p_COD_UNI_PSC
            || ': No hay folios MT900 y MT910 del día '
            || P_FEC_PCS;
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (P_GLS_DET_PCS,
                                                      p_FEC_ICO_PCS,
                                                      P_COD_ERR_BIB,
                                                      P_COD_TPO_PSC,
                                                      P_ERROR);
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         ERR_CODE := SQLCODE;
         ERR_MSG := SUBSTR (SQLERRM, 1, 300);
         P_ERROR := PKG_PAB_CONSTANTES.V_ERROR;
         PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (ERR_CODE,
                                             V_NOM_PCK,
                                             ERR_MSG,
                                             V_NOM_SP);
         RAISE_APPLICATION_ERROR (-20000, ERR_MSG);
   END SP_PAB_ACT_900_910;


   --************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_ELI_PAGOS_ACMDO
   -- Objetivo: Al cierre del sistema se eliminan los registros del los pagos realizados
   --        que se conservan por mas 30 días
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_PAGOS_RLZAR_ACMDO
   -- Fecha: 02-10-2018
   -- Autor: Santander CAH
   -- Input: p_FEC_PCS -> Fecha Proceso.
   -- Output: p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: 02-10-2018, Creacion Procedimiento.
   --***********************************************************************************************
   PROCEDURE SP_PAB_ELI_PAGOS_ACMDO (p_FEC_PCS   IN     VARCHAR2,
                                     p_ERROR        OUT NUMBER)
   IS
      v_NOM_SP    VARCHAR2 (30) := 'SP_PAB_ELI_PAGOS_ACMDO';
      v_FEC_ELI DATE
            := TO_DATE (p_FEC_PCS || ' 23:59:59', 'DD-MM-YYYY hh24:mi:ss')
               - PKG_PAB_CONSTANTES.V_DIA_ELI_PAGOS ;
      v_NUM_LOG   NUMBER;
   --v_COD_UNI_PSC VARCHAR2(30):= 'PAGOS REALIZADOS';

   BEGIN
      p_ERROR := PKG_PAB_CONSTANTES.v_OK;


      p_GLS_DET_PCS :=
         p_COD_UNI_PSC
         || ': Se eliminara todos los registros menores a contar de la fecha :'
         || v_FEC_ELI;
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);

      BEGIN
         DELETE FROM   PABS_DT_PAGOS_RLZAR_ACMDO
               WHERE   FEC_PAG_RLZ <= v_FEC_ELI;

         v_NUM_LOG := SQL%ROWCOUNT;  -- obtengo el numero de datos insertados.

         p_GLS_DET_PCS :=
               p_COD_UNI_PSC
            || ': Registros eliminados de la base de datos:'
            || v_NUM_LOG;
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                      p_FEC_ICO_PCS,
                                                      p_COD_ERR_BIB,
                                                      p_COD_TPO_PSC,
                                                      p_ERROR);
      EXCEPTION
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            RAISE_APPLICATION_ERROR (-20000, err_msg);
      END;
   --Fin Eliminacion de registros tabla Pagos realizados

   EXCEPTION
      WHEN DUP_VAL_ON_INDEX
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (
            p_COD_UNI_PSC || ': ' || err_msg,
            p_FEC_PCS,                                        --p_FEC_ICO_PCS,
            p_COD_ERR_BIB,
            p_COD_TPO_PSC,
            p_ERROR
         );
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (
            p_COD_UNI_PSC || ': ' || err_msg,
            p_FEC_PCS,                                        --p_FEC_ICO_PCS,
            p_COD_ERR_BIB,
            p_COD_TPO_PSC,
            p_ERROR
         );
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_ELI_PAGOS_ACMDO;

   /*******************************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_ACT_SALDOS_DARIO
   -- Objetivo: Procedimiento almacenado que recorre los saldos diarios e inserta maximos y minimos.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_MONDA_ALMOT
   --
   -- Fecha: 08/08/2018
   -- Autor: Santander CAH
   -- Input:
   --      p_FEC_PCS -> Fecha Proceso.
   -- Output:
   --      p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --*****************************************************************************************************************/
   PROCEDURE SP_PAB_ACT_SALDOS_DARIO (p_FEC_PCS   IN     VARCHAR2,
                                      p_ERROR        OUT NUMBER)
   IS
      v_NOM_SP            VARCHAR2 (30) := 'SP_PAB_ACT_SALDOS_DARIO';
      v_ING_BCCH          NUMBER;
      v_EGR_BCCH          NUMBER;
      v_SALDO_BBCH        NUMBER;
      v_INGRESO_PROY      NUMBER;
      v_EGRESO_PROY       NUMBER;
      v_NETO_REAL         NUMBER;
      v_NETO_PROY         NUMBER;
      v_MONEDA            CHAR;
      v_CONT              NUMBER;

      --*********************Declaracion Cursor C_CONSULTA_MONEDA************************
      CURSOR C_CONSULTA_MONEDA
      IS
         SELECT   COD_DVI
           FROM   PABS_DT_MONDA_ALMOT MON;

      R_CONSULTA_MONEDA   C_CONSULTA_MONEDA%ROWTYPE;
   --*********************Declaracion Cursor C_CONSULTA_BANCOS************************

   BEGIN
      v_CONT := 0;
      --Actualización dde máximos y mínimos (saldos del día).
      p_GLS_DET_PCS :=
            p_COD_UNI_PSC
         || ': Se actualizará los saldos máx y mín del día: '
         || p_FEC_PCS;
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);

      BEGIN
         OPEN C_CONSULTA_MONEDA;
      EXCEPTION
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            RAISE_APPLICATION_ERROR (-20000, err_msg);
      END;

      BEGIN
         FETCH C_CONSULTA_MONEDA INTO   R_CONSULTA_MONEDA;
      EXCEPTION
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);

            --Verificamos si el cursor se encuentra abierto en caso de error.
            IF (C_CONSULTA_MONEDA%ISOPEN)
            THEN
               CLOSE C_CONSULTA_MONEDA;
            END IF;

            RAISE_APPLICATION_ERROR (-20000, err_msg);
      END;

      WHILE C_CONSULTA_MONEDA%FOUND
      LOOP
         -- err_msg := 'Recorrimos las monedas '||R_CONSULTA_MONEDA.COD_DVI;
         --PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code, v_NOM_PCK, err_msg, v_NOM_SP);

         --Se registra un nuevo saldo por moneda
         PKG_PAB_LIQUIDEZ.SP_PAB_RGTRO_SALDOS_DARIO (
            R_CONSULTA_MONEDA.COD_DVI,
            p_ERROR
         );

         FETCH C_CONSULTA_MONEDA INTO   R_CONSULTA_MONEDA;
      END LOOP;

      CLOSE C_CONSULTA_MONEDA;

      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   EXCEPTION
      WHEN OTHERS
      THEN
         --Verificamos si el cursor se encuentra abierto en caso de error.
         IF (C_CONSULTA_MONEDA%ISOPEN)
         THEN
            CLOSE C_CONSULTA_MONEDA;
         END IF;

         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_ACT_SALDOS_DARIO;

   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ELI_CONTA
   -- Objetivo: Al cierre del sistema se eliminan los registros de la contabilidad, se conservan X dias segun
   --           lo indicado por la constante PKG_PAB_CONSTANTES.V_DIA_CONT_HIS.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_CNTBL_ALMOT
   -- Fecha: 27/12/2018
   -- Autor: Santander
   -- Input: p_FEC_PCS -> Fecha Proceso.
   -- Output: p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones:
   --***********************************************************************************************
   PROCEDURE Sp_PAB_ELI_CONTA (p_FEC_PCS IN VARCHAR2, p_ERROR OUT NUMBER)
   IS
      v_NOM_SP    VARCHAR2 (30) := 'Sp_PAB_ELI_CONTA';
      v_FEC_ELI DATE
            := TO_DATE (p_FEC_PCS || ' 23:59:59', 'DD-MM-YYYY hh24:mi:ss')
               - PKG_PAB_CONSTANTES.V_DIA_CONT_HIS ;
      v_NUM_LOG   NUMBER;
   BEGIN
      p_ERROR := PKG_PAB_CONSTANTES.v_OK;

      --Historificacion Nominas
      p_GLS_DET_PCS :=
         p_COD_UNI_PSC
         || ': Se eliminara todos los registros de contabilidad con fecha menor o igual a  :'
         || TO_CHAR (v_FEC_ELI, 'DD-MM-YYYY HH24:MI:SS');
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);

      BEGIN
         DELETE FROM   PABS_DT_CNTBL_ALMOT
               WHERE   TRUNC (FEC_ISR_OPE) <= v_FEC_ELI;

         v_NUM_LOG := SQL%ROWCOUNT;  -- obtengo el numero de datos insertados.

         p_GLS_DET_PCS :=
               p_COD_UNI_PSC
            || ': Registros eliminado de la contabilidad:'
            || v_NUM_LOG;
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                      p_FEC_ICO_PCS,
                                                      p_COD_ERR_BIB,
                                                      p_COD_TPO_PSC,
                                                      p_ERROR);
      EXCEPTION
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            RAISE_APPLICATION_ERROR (-20000, err_msg);
      END;
   --Fin Eliminacion de registros tabla LOG

   EXCEPTION
      WHEN DUP_VAL_ON_INDEX
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (
            p_COD_UNI_PSC || ': ' || err_msg,
            p_FEC_ICO_PCS,
            p_COD_ERR_BIB,
            p_COD_TPO_PSC,
            p_ERROR
         );
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (
            p_COD_UNI_PSC || ': ' || err_msg,
            p_FEC_ICO_PCS,
            p_COD_ERR_BIB,
            p_COD_TPO_PSC,
            p_ERROR
         );
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_ELI_CONTA;


   /******************************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_GESTN_PART_SALDO_HTRCO
   -- Objetivo: Procedimiento almacenado gestor que se ejecuta para crear o eliminar particiones de la tabla
   --           PABS_DT_RGTRO_SALDO_HTRCO , Se ejecutrá al cierre del sistema.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_RGTRO_SALDO_HTRCO
   --
   -- Fecha: 11-02-2019
   -- Autor: Santander CAH
   -- Input:
   --      p_FEC_PCS   Fecha de Proceso
   --
   -- Output:
   --
   --       p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --*****************************************************************************************************************/
   PROCEDURE SP_PAB_GESTN_PART_SALDO_HTRCO (p_FEC_PCS   IN     VARCHAR2,
                                            p_ERROR        OUT NUMBER)
   IS
      v_NOM_SP         VARCHAR2 (30) := 'SP_PAB_GESTN_PART_SALDO_HTRCO';
      v_ULTIMO_DIA     DATE := LAST_DAY (TRUNC (SYSDATE));
      v_FECHA_ACTUAL   DATE := TO_DATE (p_FEC_PCS, 'DD-MM-YYYY');
      v_NOM_TABLA      VARCHAR2 (30) := 'PABS_DT_RGTRO_SALDO_HTRCO';
      v_NOM_PART       VARCHAR2 (11);
      v_FEC_PART       VARCHAR2 (8);
      v_ULTIMA_PART    NUMBER;
      v_PRIMERA_PART   NUMBER;
      v_24_PARTICION   NUMBER := 200;                    --Equivale a 24 meses
      v_NOM_ULT_PART   VARCHAR2 (11);
      v_SBT_INI        NUMBER := 0;
      v_SBT_FIN        NUMBER := 6;
      v_30_DIAS        NUMBER := 30;
      v_COD_PAIS       CHAR (2) := 'SN';
   BEGIN
      --Obtenemos si la fecha actual es habil
      PKG_PAB_UTILITY.SP_PAB_DEV_DIA_HABIL_SIG (v_COD_PAIS, v_FECHA_ACTUAL);

      --Obtenemos el ultimo día habil del mes
      PKG_PAB_UTILITY.SP_PAB_DEV_DIA_HABIL_ANT (v_COD_PAIS, v_ULTIMO_DIA);

      BEGIN
         SELECT   SUBSTR (MAX (SH.FEC_REG_SDO), v_SBT_INI, v_SBT_FIN),
                  SUBSTR (MIN (SH.FEC_REG_SDO), v_SBT_INI, v_SBT_FIN)
           INTO   v_ULTIMA_PART, v_PRIMERA_PART
           FROM   PABS_DT_RGTRO_SALDO_HTRCO SH;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_s_mensaje := err_code || '-' || err_msg;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;

      IF v_ULTIMO_DIA = v_FECHA_ACTUAL
      THEN
         --Si creamos la partición
         v_NOM_PART :=
            'PMES_' || TO_NUMBER (TO_CHAR ( (SYSDATE) + v_30_DIAS, 'YYYYMM'));
         v_FEC_PART := TO_CHAR (LAST_DAY ( (SYSDATE) + v_30_DIAS), 'YYYYMMDD');

         PKG_PAB_LIQUIDEZ.SP_PAB_CREA_PART_SALDO_HTRCO (v_NOM_TABLA,
                                                        v_NOM_PART,
                                                        v_FEC_PART,
                                                        p_ERROR);

         --Verificamos si hay mas de 24 particiones
         IF v_ULTIMA_PART - v_PRIMERA_PART > v_24_PARTICION
         THEN
            v_NOM_ULT_PART := 'PMES_' || v_PRIMERA_PART;
            --Eliminamos la ultima particion, si hay mas de 24 meses
            PKG_PAB_LIQUIDEZ.SP_PAB_ELI_PART_SALDO_HTRCO (v_NOM_TABLA,
                                                          v_NOM_ULT_PART,
                                                          p_ERROR);
         END IF;
      END IF;

      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_GESTN_PART_SALDO_HTRCO;
END PKG_PAB_CIERRE;