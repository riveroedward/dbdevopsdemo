CREATE OR REPLACE PACKAGE BODY         PKG_PAB_OPERACION IS
  --

  /**  Package el cual contrendra todas las funciones y procedimientos relacionados con las operaciones
  -- @santander */
  --
  --
  /************************************************************************************************
  -- Funcion/Procedimiento: Sp_PAB_BUS_OPE_LIB
  -- Objetivo: PROCEDIMIENTO ALMACENADO QUE ENTREGA LOS TIPOS
  -- DE OPERACION LIBERADAS, SEGUN FILTROS INGRESADOS
  -- Sistema: DBO_PAB.
  -- Base de Datos: DBO_PAB.
  -- Tablas Usadas: PABS_DT_DETLL_OPRCN
  --        , PABS_DT_OPRCN_INFCN_OPCON
  --        , PABS_DT_SISTM_ENTRD_SALID
  --        , PABS_DT_SISTM_ENTRD_SALID
  -- Fecha: 02/06/16
  -- Autor: Santander
  -- Input: p_TIPO_OP -> tipo operacion
  --        p_MT_SWIFT-> codigo Mt Swift
  --        p_COD_DVI-> moneda
  --        p_NOM_BFC-> nombre beneficiario
  --        p_COD_ENT-> canal origen
  --        p_COD_SAL-> canal pago
  --        p_BCO_DTN-> banco
  --        p_MONTO_DESDE-> monto desde
  --        p_MONTO_HASTA-> monto hasta
  --        p_NUM_NOM-> numero nomina
  --        p_NUM_OPE-> numero operacion
  --        p_RUT-> rut beneficiario
  --        p_COD_TIP_SEG -> Codigo de tipo de sistema
  -- Output:
  --        p_CURSOR-> SYS_REFCURSOR, RESULTADOS DE CONSULTA.
  --        p_ERROR-> Devuelve error
  -- Input/Output: N/A
  -- Retorno: N/A.
  -- Observaciones: <Fecha y Detalle de ultimos cambios>
  --***********************************************************************************************/
  PROCEDURE Sp_PAB_BUS_OPE_LIB(p_TIPO_OP     IN CHAR,
                               p_MT_SWIFT    IN NUMBER,
                               p_COD_DVI     IN CHAR,
                               p_NOM_BFC     IN VARCHAR2,
                               p_COD_ENT     IN CHAR,
                               p_COD_SAL     IN CHAR,
                               p_BCO_DTN     IN CHAR,
                               p_MONTO_DESDE IN NUMBER,
                               p_MONTO_HASTA IN NUMBER,
                               p_NUM_NOM     IN NUMBER,
                               p_NUM_OPE     IN NUMBER,
                               p_RUT         IN CHAR,
                               p_COD_TIP_SEG IN NUMBER,
                               p_CURSOR      OUT SYS_REFCURSOR,
                               p_ERROR       OUT NUMBER) IS

    --Seteo de Variables
    v_NOM_SP VARCHAR2(30) := 'Sp_PAB_BUS_OPE_LIB';

    v_TIPO_OP     CHAR(8);
    v_COD_ENT     CHAR(10);
    v_COD_SAL     CHAR(10);
    v_BCO_DTN     CHAR(11);
    v_NUM_RUT     CHAR(11);
    v_IMp_DSD     NUMBER;
    v_IMp_HST     NUMBER;
    v_NUM_NOM     NUMBER;
    v_NUM_OPE     NUMBER;
    v_Dv_RUT      VARCHAR2(1);
    v_MT_SWIFT    NUMBER;
    v_COD_TIP_SEG NUMBER;
    V_FECHA_HOY   DATE := TRUNC(SYSDATE);

  BEGIN

    --Seteamos las variableas
    v_TIPO_OP := p_TIPO_OP; --Siempre data
    v_COD_ENT := p_COD_ENT; --Siempre data
    v_COD_SAL := p_COD_SAL; --Siempre data
    v_BCO_DTN := p_BCO_DTN; --Siempre data

    --Puede venir un nulo
    IF (NVL(p_RUT, '#') <> '#') OR NVL(p_RUT, '-1') <> '-1' THEN

      PKG_PAB_UTILITY.Sp_PAB_DESCOMp_RUT(p_RUT, v_NUM_RUT, v_Dv_RUT);
    ELSE
      v_NUM_RUT := p_RUT;
    END IF;

    --Siempre data
    IF p_MT_SWIFT = -1 THEN
      v_MT_SWIFT := NULL;
    ELSE
      v_MT_SWIFT := p_MT_SWIFT;
    END IF;

    --Siempre data
    IF p_MONTO_DESDE = -1 THEN
      v_IMp_DSD := 0;
    ELSE
      v_IMp_DSD := p_MONTO_DESDE;
    END IF;

    --Siempre data
    IF p_MONTO_HASTA IS NULL OR p_MONTO_HASTA = 0 THEN
      v_IMp_HST := 9999999999999;
    ELSE
      v_IMp_HST := p_MONTO_HASTA;
    END IF;

    --Puede ser nulo
    IF p_NUM_NOM = -1 THEN
      v_NUM_NOM := NULL;
    ELSE
      v_NUM_NOM := p_NUM_NOM;
    END IF;

    --Siempre data
    IF p_NUM_OPE = -1 THEN
      v_NUM_OPE := NULL;
    ELSE
      v_NUM_OPE := p_NUM_OPE;
    END IF;

    --Siempre data
    IF p_COD_TIP_SEG = -1 THEN
      v_COD_TIP_SEG := NULL;
    ELSE
      v_COD_TIP_SEG := p_COD_TIP_SEG;
    END IF;

    BEGIN
      --  PABS_DT_BNFCR_ORDNT
      OPEN p_CURSOR FOR
        SELECT SAL.DSC_SIS_ENT_SAL AS CANAL_PAGO,
               OPE.COD_MT_SWF AS MT_SWF,
               OPE.COD_TPO_OPE_AOS AS TIPO_OP,
               ENS.DSC_SIS_ENT_SAL AS CANAL_ORIGEN,
               OPE.IMp_OPE AS MONTO,
               OPE.COD_DVI AS MONEDA,
               OPE.NOM_BFC AS NOMBRE_BENEF,
               OPE.COD_BCO_DTN AS BANCO_DEST,
               OPE.FEC_VTA AS FECHA_VALUTA,
               --OPE.FEC_VTA || ' ' || '10:00' AS FECHA_VALUTA,
               OPE.FEC_ISR_OPE AS FECHA_OPERACION,
               OPE.NUM_FOL_OPE AS FOLIO_OPERACION,
               OPE.NUM_FOL_NMN AS NUMERO_NOMINA,
               PKG_PAB_UTILITY.FN_PAB_TRAN_COD_EST_GLOSA(OPE.COD_EST_AOS,
                                                         PKG_PAB_CONSTANTES.V_IND_OPE) AS COD_EST_AOS, -- se agrego para la validacion de estado de
               BAI.DES_BCO AS NOM_BANCO
          FROM PABS_DT_DETLL_OPRCN       OPE,
               PABS_DT_OPRCN_INFCN_OPCON OPC,
               PABS_DT_SISTM_ENTRD_SALID ENS,
               PABS_DT_SISTM_ENTRD_SALID SAL,
               PABS_DT_BNFCR_ORDNT       BEN,
               TCDTBAI                   BAI
         WHERE OPE.FEC_ISR_OPE = OPC.FEC_ISR_OPE(+)
           AND OPE.NUM_FOL_OPE = OPC.NUM_FOL_OPE(+)
           AND OPE.COD_BCO_DTN = BAI.TGCDSWSA(+)
           AND OPE.COD_EST_AOS IN (PKG_PAB_CONSTANTES.v_COD_EST_AOS_OPEAUT, PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPROBLM)
           AND OPE.COD_SIS_ENT = ENS.COD_SIS_ENT_SAL
           AND OPE.COD_SIS_SAL = SAL.COD_SIS_ENT_SAL
           AND OPE.COD_TPO_OPE_AOS = NVL(v_TIPO_OP, OPE.COD_TPO_OPE_AOS)
           AND OPE.COD_MT_SWF = NVL(v_MT_SWIFT, OPE.COD_MT_SWF)
           AND OPE.COD_DVI = NVL(p_COD_DVI, OPE.COD_DVI)
           AND OPE.NOM_BFC LIKE NVL(p_NOM_BFC, OPE.NOM_BFC) || '%'
           AND OPE.COD_SIS_ENT = NVL(v_COD_ENT, OPE.COD_SIS_ENT)
           AND OPE.COD_SIS_SAL = NVL(v_COD_SAL, OPE.COD_SIS_SAL)
           AND NVL(OPE.COD_BCO_DTN,'#') = NVL(NVL(v_BCO_DTN, OPE.COD_BCO_DTN),'#')
           AND OPE.IMp_OPE BETWEEN v_IMp_DSD AND v_IMp_HST
           AND NVL(OPE.NUM_FOL_NMN, 0) =
               NVL(v_NUM_NOM, NVL(OPE.NUM_FOL_NMN, 0))
           AND OPE.NUM_FOL_OPE LIKE NVL(v_NUM_OPE, OPE.NUM_FOL_OPE) || '%'
           AND NVL(OPC.NUM_DOC_ODN, '#') =
               NVL(v_NUM_RUT, NVL(OPC.NUM_DOC_ODN, '#'))
           AND BEN.COD_TPO_SEG_AOS(+) =
               NVL(v_COD_TIP_SEG, BEN.COD_TPO_SEG_AOS(+)) -- Seccion para filtrar por el Segmento ingresado por parametro
           AND OPC.COD_TPD_BFC = BEN.COD_TPD_PSN(+) -- Relacion 1 entre la tabla operaciones y la de beneficiario ordenante
           AND OPC.NUM_DOC_BFC = BEN.NUM_DOC_PSN(+)
           AND OPE.FEC_VTA >= V_FECHA_HOY; -- Relacion 2 entre la tabla operaciones y la de beneficiario ordenante

    END;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      err_code := SQLCODE;
      err_msg  := SUBSTR(SQLERRM, 1, 300);
      p_ERROR  := PKG_PAB_CONSTANTES.v_ERROR;
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                         v_NOM_PCK,
                                         err_msg,
                                         v_NOM_SP);
    WHEN OTHERS THEN
      err_code    := SQLCODE;
      err_msg     := SUBSTR(SQLERRM, 1, 300);
      p_s_mensaje := err_code || '-' || err_msg;
      p_ERROR     := PKG_PAB_CONSTANTES.v_ERROR;
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                         v_NOM_PCK,
                                         err_msg,
                                         v_NOM_SP);
      RAISE_APPLICATION_ERROR(-20000, err_code || p_s_mensaje);

  END Sp_PAB_BUS_OPE_LIB;

  /* ******************************************************************************
  *  IDENTIFICACION PROCEDIMIENTO: Sp_PAB_CAMBIAR_CANAL
  * -----------------------------------------------------------------------------
  * Version :    1.0
  * Descripcion: Procedimiento que cambia el canal de pago a todas las operaciones seleccionadas desde la aplicacion.
  * -----------------------------------------------------------------------------
  * Parametros Entrada:
  *       p_FEC_ISR_OPE:     Fecha de insercion de operacion en el sistema, PK tabla detalle operaciones.
  *       p_NUM_FOL_OPE:     foliador del banco, PK tabla detalle operaciones.
  *       p_COD_SIS_SAL:     Codigo del sistema de salida utilizado en la operacion.
  *       p_COD_MT_SWF:   Codigo de Mensaje Swift.
  *       p_COD_TPO_OPE_AOS:Codigo tipo de operacion utilizado en el sistema de altos montos.
  *       p_COD_SIS_ENT_SAL:Codigo del sistema de salida, correspondiente al cambio de canal.
  *       p_COD_DVI       : Codigo de la divisa.
  *       p_GLS_ERROR     : Glosa con error
  *       p_COD_USR:        rut usuario.
  * Par?metros Salida:
  *        p_GLS_ERROR: mensaje de slida que indica el motivo por el que no se ejecuto el cambio de canal.
  *       p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
  * Variables globales:
  *******************************************************************************
  * Control de Modificaciones
  * -------------------------
  * Fecha         Nombre           Descripcion
  * ----------    --------------   ----------------------------------
  * 26/05/16      Santander           Primera Version
  ***************************************************************************** */
  PROCEDURE Sp_PAB_CAMBIAR_CANAL(p_FEC_ISR_OPE     IN DATE,
                                 p_NUM_FOL_OPE     IN NUMBER,
                                 p_COD_SIS_SAL     IN CHAR,
                                 p_COD_MT_SWF      IN NUMBER,
                                 p_COD_TPO_OPE_AOS IN CHAR,
                                 p_COD_SIS_ENT_SAL IN CHAR,
                                 p_COD_DVI         IN CHAR,
                                 p_COD_USR         IN CHAR,
                                 p_GLS_ERROR       OUT VARCHAR2,
                                 p_ERROR           OUT NUMBER) IS

    v_NOM_SP      VARCHAR2(30) := 'Sp_PAB_CAMBIAR_CANAL';
    v_HORA_ACTUAL NUMBER(4) := TO_NUMBER(TO_CHAR(SYSDATE, 'HH24MI'));
    v_HORA_CIERRE NUMBER(4);
    v_CANAL       CHAR(10);
    V_COD_DVI     CHAR(3);
    err_bus_can_sal exception;

    V_COD_SIS_SAL VARCHAR2(30);

  BEGIN

    err_msg := 'p_COD_SIS_SAL:' || p_COD_SIS_SAL || ' p_COD_TPO_OPE_AOS:' ||
               p_COD_TPO_OPE_AOS;
    PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                       v_NOM_PCK,
                                       err_msg,
                                       v_NOM_SP);

    v_COD_DVI     := p_COD_DVI;
    p_GLS_ERROR   := NULL;
    v_COD_SIS_SAL := p_COD_SIS_SAL;

    BEGIN
      --Busca si existe el canal de salida para el producto del mensaje
      SELECT COD_SIS_ENT_SAL
        INTO v_CANAL
        FROM PABS_DT_TIPO_OPRCN_SALID_MNSJ
       WHERE COD_SIS_ENT_SAL = p_COD_SIS_ENT_SAL
         AND COD_TPO_OPE_AOS = p_COD_TPO_OPE_AOS
         AND COD_MT_SWF = p_COD_MT_SWF
         AND FLG_VGN = PKG_PAB_CONSTANTES.v_REG_VIG;

    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        RAISE err_bus_can_sal;

      WHEN OTHERS THEN
        err_code    := SQLCODE;
        err_msg     := SUBSTR(SQLERRM, 1, 300);
        p_s_mensaje := err_code || '-' || err_msg;
        PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                           v_NOM_PCK,
                                           err_msg,
                                           v_NOM_SP);
        p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
        RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);
    END;

    BEGIN

      --Rescatamos el horario de cierre del canal
      SELECT HOR_CRR_SIS
        INTO v_HORA_CIERRE
        FROM PABS_DT_SISTM_ENTRD_SALID
       WHERE COD_SIS_ENT_SAL = p_COD_SIS_ENT_SAL;

    EXCEPTION
      WHEN NO_DATA_FOUND THEN

        err_msg := 'Error al obtener el horario de cierre del canal:' ||
                   p_COD_SIS_ENT_SAL;
        p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
        RAISE;

      WHEN OTHERS THEN
        err_code    := SQLCODE;
        err_msg     := SUBSTR(SQLERRM, 1, 300);
        p_s_mensaje := err_code || '-' || err_msg;
        PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                           v_NOM_PCK,
                                           err_msg,
                                           v_NOM_SP);
        p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
        RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);
    END;

    IF v_HORA_CIERRE > v_HORA_ACTUAL THEN


      BEGIN
        --Actualiza

        UPDATE PABS_DT_DETLL_OPRCN
           SET COD_SIS_SAL     = p_COD_SIS_ENT_SAL,
               FLG_CBO_CNL_ALC = PKG_PAB_CONSTANTES.V_REG_NOVIG -- VALOR 0, OOFF
         WHERE FEC_ISR_OPE = p_FEC_ISR_OPE
           AND NUM_FOL_OPE = p_NUM_FOL_OPE;

        p_ERROR := PKG_PAB_CONSTANTES.v_OK;
        err_msg := 'Cambio canal';
        PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                           v_NOM_PCK,
                                           err_msg,
                                           v_NOM_SP);

        --Guardamos en la bitacora el cambio
        --Llamamos al procedimiento de Bitacora
        v_DES_BIT := 'Cambio de canal de la operacion ' ||
                     p_COD_SIS_ENT_SAL;
        PKG_PAB_TRACKING.Sp_PAB_INS_BIT_OPE(p_NUM_FOL_OPE,
                                            p_FEC_ISR_OPE,
                                            p_COD_USR,
                                            PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT,
                                            PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT,
                                            v_DES_BIT,
                                            p_ERROR);
        COMMIT;

      EXCEPTION

        WHEN NO_DATA_FOUND THEN

          err_code := SQLCODE;
          err_msg  := SUBSTR(SQLERRM, 1, 300) ||
                      'Error Update cambio de canal de la operacion ' ||
                      p_COD_SIS_ENT_SAL;
          PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
          p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
          RAISE;

        WHEN OTHERS THEN
          err_code    := SQLCODE;
          err_msg     := SUBSTR(SQLERRM, 1, 300);
          p_s_mensaje := err_code || '-' || err_msg;
          PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
          p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
          RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);

      END;
    ELSE

      --Fuera de Horario
      p_ERROR := PKG_PAB_CONSTANTES.V_COD_CMB_CNL_NOHOR;

    END IF;

  EXCEPTION
    WHEN err_bus_can_sal THEN
      err_msg := 'Error al buscar el canal de salida para la combinacion:' ||
                 p_COD_TPO_OPE_AOS || ' ' || p_COD_SIS_ENT_SAL || ' ' ||
                 p_COD_MT_SWF;
      p_ERROR := PKG_PAB_CONSTANTES.V_COD_CMB_CNL_NOCOMB;
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                         v_NOM_PCK,
                                         err_msg,
                                         v_NOM_SP);

    WHEN OTHERS THEN
      err_code    := SQLCODE;
      err_msg     := SUBSTR(SQLERRM, 1, 300);
      p_s_mensaje := err_code || '-' || err_msg;
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                         v_NOM_PCK,
                                         err_msg,
                                         v_NOM_SP);
      RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);

  END Sp_PAB_CAMBIAR_CANAL;

  /************************************************************************************************
  -- Funcion/Procedimiento: Sp_PAB_BUS_DET_OPE
  -- Objetivo: Procedimiento almacenado que retorna el detalle de una operaci?n seleccionada
  -- Sistema: DBO_PAB.
  -- Base de Datos: DBO_PAB.
  -- Tablas Usadas: PABS_DT_DETLL_OPRCN, PABS_DT_OPRCN_INFCN_OPCON
  --          PABS_DT_ESTDO_ALMOT, PABS_DT_SISTM_ENTRD_SALID
  -- Fecha: 02/06/16
  -- Autor: Santander
  -- Input: p_NUM_FOL_OPE: Foliador del banco, PK tabla detalle operaciones.
  --        p_FEC_ISR_OPE: Fecha de inserci?n de operaci?n en el sistema, PK tabla detalle operaciones.
  -- Output:
  --        p_CURSOR : SYS_REFCURSOR, resultados de consulta.
  --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
  -- Input/Output: N/A
  -- Retorno: N/A.
  -- Observaciones: <Fecha y Detalle de ultimos cambios>
  --***********************************************************************************************/
  PROCEDURE Sp_PAB_BUS_DET_OPE(p_NUM_FOL_OPE IN NUMBER,
                               p_FEC_ISR_OPE IN DATE,
                               p_CURSOR      OUT SYS_REFCURSOR,
                               p_ERROR       OUT NUMBER) IS

    v_NOM_SP VARCHAR2(30) := 'Sp_PAB_BUS_DET_OPE';
    v_cuaCer CONSTANT CHAR(4) := '0000';
    v_GLS_CMN VARCHAR2(9) := 'CSBSPESO1';
    v_ES_MOV_HISTORICO NUMBER := 0;

  BEGIN

    -- Revisa si este movimiento debe estar en las tablas historicas
    IF (TRUNC(SYSDATE) - TRUNC(p_FEC_ISR_OPE) > PKG_PAB_CONSTANTES.V_DIA_RES_HIS) THEN
        v_ES_MOV_HISTORICO := 1;
    END IF;

    IF (v_ES_MOV_HISTORICO = 0) THEN
        OPEN p_CURSOR FOR
          SELECT ESTALM.DSC_EST_AOS AS ESTADO,
                 DETOPE.FEC_ISR_OPE AS FEC_INS,
                 DETOPE.NUM_FOL_OPE AS NUM_OPE,
                 DETOPE.NUM_FOL_NMN AS NUM_NOM,
                 DETOPE.NUM_FOL_OPE_ORG AS NUM_OPE_ORG,
                 DETOPE.NUM_OPE_SIS_ENT AS NUM_OPE_ENT,
                 DECODE(DETOPE.FLG_EGR_ING,
                        PKG_PAB_CONSTANTES.v_FLG_EGR,
                        PKG_PAB_CONSTANTES.v_STR_FLG_EGR,
                        PKG_PAB_CONSTANTES.v_STR_FLG_ING) AS EGRESO_INGRESO,
                 DETOPE.COD_SIS_ENT AS COD_CANAL_ENT,
                 ENT.DSC_SIS_ENT_SAL AS CANAL_ENTRADA,
                 DETOPE.COD_SIS_SAL AS COD_CANAL_SAL,
                 SAL.DSC_SIS_ENT_SAL AS CANAL_SALIDA,
                 DECODE(DETOPE.COD_TPO_ING,
                        PKG_PAB_CONSTANTES.v_COD_TIp_ING_MAS,
                        PKG_PAB_CONSTANTES.v_STR_TIp_ING_MAS,
                        PKG_PAB_CONSTANTES.V_COD_TIP_ING_UNI,
                        PKG_PAB_CONSTANTES.V_STR_TIP_ING_UNI,
                        PKG_PAB_CONSTANTES.V_COD_TIP_ING_DIR,
                        PKG_PAB_CONSTANTES.V_STR_TIP_ING_DIR) AS TIPO_INGRESO,
                 DETOPE.FEC_ENv_RCp_SWF AS FECHA_SWIFT,
                 DETOPE.HOR_ENv_RCp_SWF AS HORA_SWIFT,
                 DETOPE.COD_MT_SWF AS COD_MT,
                 DECODE(DETOPE.FLG_MRD_UTZ_SWF,
                        PKG_PAB_CONSTANTES.v_FLG_MRD_MN,
                        PKG_PAB_CONSTANTES.v_STR_FLG_MRD_MN,
                        PKG_PAB_CONSTANTES.v_STR_FLG_MRD_MX) AS MERCADO,
                 DETOPE.COD_TPO_OPE_AOS AS TIPO_OPERACION,
                 DETOPE.FEC_VTA AS FECHA_PAGO,
                 DETOPE.COD_DVI AS DIVISA,
                 DETOPE.IMp_OPE AS MONTO_TOTAL,
                 DETOPE.COD_BCO_DTN AS BIC_DESTINO,
                 DETOPE.COD_BCO_ORG AS BIC_ORIGEN,
                 DETOPE.NUM_DOC_BFC AS DOC_BENEFICIARIO,
                 DETOPE.COD_TPD_BFC AS TIPO_DOC_BENEFICIARIO,
                 SUBSTR(PKG_PAB_UTILITY.FN_PAB_VLD_TXT(DETOPE.NOM_BFC), 0, 35) AS BENEFICIARIO, --CAFF
                 DETOPE.NUM_CTA_BFC AS CTA_BENEFICIARIO,
                 DETOPE.COD_SUC AS COD_SUCURSAL,
                 DETOPE.NUM_REF_SWF AS REF_SWIFT,
                 DETOPE.NUM_REF_EXT AS REF_EXT_SWIFT,
                 DECODE(DETOPE.COD_EST_AOS,
                        PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEREC,
                        DETOPC.GLS_EST_RCH,
                        PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPVI,
                        DETOPC.GLS_MTv_VSD,
                        DETOPE.GLS_ADC_EST) AS GLOSA_ESTADO,
                 PKG_PAB_CONSTANTES.v_COD_ENT_BAN_SANT || v_cuaCer ||
                 LPAD(DETOPE.NUM_CTA_BFC, 12, '0') AS REF_CONTABLE,
                 DETOPC.COD_BCO_BFC AS BIC_BENEFICIARIO,
                 DETOPC.COD_BCO_ITD AS BIC_INTERMEDIARIO,
                 DETOPC.NUM_DOC_BFC AS DOC_BENEFICIARIO_OPC,
                 DETOPC.COD_TPD_BFC AS TIPO_DOC_BENEFICIARIO_OPC,
                 DETOPC.NUM_CTA_DCv_BFC AS CTA_DCv_BENEFICIARIO,
                 SUBSTR(PKG_PAB_UTILITY.FN_PAB_VLD_TXT(DETOPC.GLS_DIR_BFC),
                        0,
                        35) AS DIRECCION_BENEFICIARIO, --CAFF
                 SUBSTR(PKG_PAB_UTILITY.FN_PAB_VLD_TXT(DETOPC.NOM_CDD_BFC),
                        0,
                        35) AS CIUDAD_BENEFICIARIO, --CAFF
                 DETOPC.COD_PAS_BFC AS PAIS_BENEFICIARIO,
                 SUBSTR(PKG_PAB_UTILITY.FN_PAB_VLD_TXT(DETOPC.NOM_ODN), 0, 35) AS NOMBRE_ORDENANTE,
                 DETOPC.NUM_DOC_ODN AS DOC_ORDENANTE,
                 DETOPC.COD_TPD_ODN AS TIPO_DOC_ORDENANTE,
                 DETOPC.NUM_CTA_ODN AS CTA_ORDENANTE,
                 DETOPC.NUM_CTA_DCv_ODN AS CTA_DCv_ORDENANTE,
                 SUBSTR(PKG_PAB_UTILITY.FN_PAB_VLD_TXT(DETOPC.GLS_DIR_ODN),
                        0,
                        35) AS DIRECCION_ORDENANTE, --CAFF
                 SUBSTR(PKG_PAB_UTILITY.FN_PAB_VLD_TXT(DETOPC.NOM_CDD_ODN),
                        0,
                        35) AS CIUDAD_ORDENANTE, --CAFF
                 DETOPC.COD_PAS_ODN AS PAIS_ORDENANTE,
                 DETOPC.NUM_CLv_NGC AS CLAVE_NEGOCIO,
                 DETOPC.NUM_AGT AS NUM_AGENTE,
                 DETOPC.COD_FND_CCLV AS FONDO_CCLV,
                 DETOPC.COD_TPO_SDO AS TIPO_SALDO,
                 DETOPC.COD_TPO_CMA AS TIPO_CAMARA,
                 DETOPC.COD_TPO_FND AS CODIGO_FONDO,
                 TFAM.GLS_TPO_FND AS GLOSA_FONDO,
                 DETOPC.FEC_TPO_OPE_CCLV AS FECHA_CCLV,
                 DETOPC.NUM_CLv_IDF AS NUMERO_IDENTIFICATORIO,
                 DETOPC.GLS_EST_RCH AS GLOSA_RECHAZO,
                 DETOPC.GLS_MTv_VSD AS GLOSA_VISADA,
                 PKG_PAB_UTILITY.FN_PAB_VLD_TXT(DETOPC.OBS_OPC_SWF) AS OBSERVACION_SWIFT,
                 TO_CHAR(SYSDATE, 'HH24MI') AS INDHOR,
                 AOS.DSC_TPO_OPE_AOS AS DSC_TPO_OPE_AOS
            FROM PABS_DT_DETLL_OPRCN       DETOPE,
                 PABS_DT_OPRCN_INFCN_OPCON DETOPC,
                 PABS_DT_ESTDO_ALMOT       ESTALM,
                 PABS_DT_SISTM_ENTRD_SALID ENT,
                 PABS_DT_SISTM_ENTRD_SALID SAL,
                 PABS_DT_TIPO_OPRCN_ALMOT  AOS,
                 PABS_DT_TIPO_FONDO_ALMOT  TFAM
           WHERE DETOPE.NUM_FOL_OPE = p_NUM_FOL_OPE
             AND DETOPE.FEC_ISR_OPE = p_FEC_ISR_OPE
             AND DETOPE.NUM_FOL_OPE = DETOPC.NUM_FOL_OPE(+)
             AND DETOPE.FEC_ISR_OPE = DETOPC.FEC_ISR_OPE(+)
             AND DETOPE.COD_EST_AOS = ESTALM.COD_EST_AOS
             AND DETOPE.COD_SIS_ENT = ENT.COD_SIS_ENT_SAL
             AND DETOPE.COD_SIS_SAL = SAL.COD_SIS_ENT_SAL(+)
             AND AOS.COD_TPO_OPE_AOS = DETOPE.COD_TPO_OPE_AOS
             AND DETOPC.COD_TPO_FND = TFAM.COD_TPO_FND(+);
    ELSE
        OPEN p_CURSOR FOR
            SELECT  ESTALM.DSC_EST_AOS AS ESTADO,
                    DETOPE_HTRCA.FEC_ISR_OPE AS FEC_INS,
                    DETOPE_HTRCA.NUM_FOL_OPE AS NUM_OPE,
                    DETOPE_HTRCA.NUM_FOL_NMN AS NUM_NOM,
                    DETOPE_HTRCA.NUM_FOL_OPE_ORG AS NUM_OPE_ORG,
                    DETOPE_HTRCA.NUM_OPE_SIS_ENT AS NUM_OPE_ENT,
                    DECODE(DETOPE_HTRCA.FLG_EGR_ING,
                        PKG_PAB_CONSTANTES.v_FLG_EGR,
                        PKG_PAB_CONSTANTES.v_STR_FLG_EGR,
                        PKG_PAB_CONSTANTES.v_STR_FLG_ING) AS EGRESO_INGRESO,
                    DETOPE_HTRCA.COD_SIS_ENT AS COD_CANAL_ENT,
                    ENT.DSC_SIS_ENT_SAL AS CANAL_ENTRADA,
                    DETOPE_HTRCA.COD_SIS_SAL AS COD_CANAL_SAL,
                    SAL.DSC_SIS_ENT_SAL AS CANAL_SALIDA,
                    DECODE(DETOPE_HTRCA.COD_TPO_ING,
                        PKG_PAB_CONSTANTES.v_COD_TIp_ING_MAS,
                        PKG_PAB_CONSTANTES.v_STR_TIp_ING_MAS,
                        PKG_PAB_CONSTANTES.V_COD_TIP_ING_UNI,
                        PKG_PAB_CONSTANTES.V_STR_TIP_ING_UNI,
                        PKG_PAB_CONSTANTES.V_COD_TIP_ING_DIR,
                        PKG_PAB_CONSTANTES.V_STR_TIP_ING_DIR) AS TIPO_INGRESO,
                    DETOPE_HTRCA.FEC_ENv_RCp_SWF AS FECHA_SWIFT,
                    DETOPE_HTRCA.HOR_ENv_RCp_SWF AS HORA_SWIFT,
                    DETOPE_HTRCA.COD_MT_SWF AS COD_MT,
                    DECODE(DETOPE_HTRCA.FLG_MRD_UTZ_SWF,
                        PKG_PAB_CONSTANTES.v_FLG_MRD_MN,
                        PKG_PAB_CONSTANTES.v_STR_FLG_MRD_MN,
                        PKG_PAB_CONSTANTES.v_STR_FLG_MRD_MX) AS MERCADO,
                    DETOPE_HTRCA.COD_TPO_OPE_AOS AS TIPO_OPERACION,
                    DETOPE_HTRCA.FEC_VTA AS FECHA_PAGO,
                    DETOPE_HTRCA.COD_DVI AS DIVISA,
                    DETOPE_HTRCA.IMp_OPE AS MONTO_TOTAL,
                    DETOPE_HTRCA.COD_BCO_DTN AS BIC_DESTINO,
                    DETOPE_HTRCA.COD_BCO_ORG AS BIC_ORIGEN,
                    DETOPE_HTRCA.NUM_DOC_BFC AS DOC_BENEFICIARIO,
                    DETOPE_HTRCA.COD_TPD_BFC AS TIPO_DOC_BENEFICIARIO,
                    SUBSTR(PKG_PAB_UTILITY.FN_PAB_VLD_TXT(DETOPE_HTRCA.NOM_BFC), 0, 35) AS BENEFICIARIO, --CAFF
                    DETOPE_HTRCA.NUM_CTA_BFC AS CTA_BENEFICIARIO,
                    DETOPE_HTRCA.COD_SUC AS COD_SUCURSAL,
                    DETOPE_HTRCA.NUM_REF_SWF AS REF_SWIFT,
                    DETOPE_HTRCA.NUM_REF_EXT AS REF_EXT_SWIFT,
                    DECODE(DETOPE_HTRCA.COD_EST_AOS,
                        PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEREC,
                        DETOPC_HTRCA.GLS_EST_RCH,
                        PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPVI,
                        DETOPC_HTRCA.GLS_MTv_VSD,
                        DETOPE_HTRCA.GLS_ADC_EST) AS GLOSA_ESTADO,
                    PKG_PAB_CONSTANTES.v_COD_ENT_BAN_SANT || v_cuaCer ||
                    LPAD(DETOPE_HTRCA.NUM_CTA_BFC, 12, '0') AS REF_CONTABLE,
                    DETOPC_HTRCA.COD_BCO_BFC AS BIC_BENEFICIARIO,
                    DETOPC_HTRCA.COD_BCO_ITD AS BIC_INTERMEDIARIO,
                    DETOPC_HTRCA.NUM_DOC_BFC AS DOC_BENEFICIARIO_OPC,
                    DETOPC_HTRCA.COD_TPD_BFC AS TIPO_DOC_BENEFICIARIO_OPC,
                    DETOPC_HTRCA.NUM_CTA_DCv_BFC AS CTA_DCv_BENEFICIARIO,
                    SUBSTR(PKG_PAB_UTILITY.FN_PAB_VLD_TXT(DETOPC_HTRCA.GLS_DIR_BFC),
                        0,
                        35) AS DIRECCION_BENEFICIARIO, --CAFF
                    SUBSTR(PKG_PAB_UTILITY.FN_PAB_VLD_TXT(DETOPC_HTRCA.NOM_CDD_BFC),
                        0,
                        35) AS CIUDAD_BENEFICIARIO, --CAFF
                    DETOPC_HTRCA.COD_PAS_BFC AS PAIS_BENEFICIARIO,
                    SUBSTR(PKG_PAB_UTILITY.FN_PAB_VLD_TXT(DETOPC_HTRCA.NOM_ODN), 0, 35) AS NOMBRE_ORDENANTE,
                    DETOPC_HTRCA.NUM_DOC_ODN AS DOC_ORDENANTE,
                    DETOPC_HTRCA.COD_TPD_ODN AS TIPO_DOC_ORDENANTE,
                    DETOPC_HTRCA.NUM_CTA_ODN AS CTA_ORDENANTE,
                    DETOPC_HTRCA.NUM_CTA_DCv_ODN AS CTA_DCv_ORDENANTE,
                    SUBSTR(PKG_PAB_UTILITY.FN_PAB_VLD_TXT(DETOPC_HTRCA.GLS_DIR_ODN),
                        0,
                        35) AS DIRECCION_ORDENANTE, --CAFF
                    SUBSTR(PKG_PAB_UTILITY.FN_PAB_VLD_TXT(DETOPC_HTRCA.NOM_CDD_ODN),
                        0,
                        35) AS CIUDAD_ORDENANTE, --CAFF
                    DETOPC_HTRCA.COD_PAS_ODN AS PAIS_ORDENANTE,
                    DETOPC_HTRCA.NUM_CLv_NGC AS CLAVE_NEGOCIO,
                    DETOPC_HTRCA.NUM_AGT AS NUM_AGENTE,
                    DETOPC_HTRCA.COD_FND_CCLV AS FONDO_CCLV,
                    DETOPC_HTRCA.COD_TPO_SDO AS TIPO_SALDO,
                    DETOPC_HTRCA.COD_TPO_CMA AS TIPO_CAMARA,
                    DETOPC_HTRCA.COD_TPO_FND AS CODIGO_FONDO,
                    TFAM.GLS_TPO_FND AS GLOSA_FONDO,
                    DETOPC_HTRCA.FEC_TPO_OPE_CCLV AS FECHA_CCLV,
                    DETOPC_HTRCA.NUM_CLv_IDF AS NUMERO_IDENTIFICATORIO,
                    DETOPC_HTRCA.GLS_EST_RCH AS GLOSA_RECHAZO,
                    DETOPC_HTRCA.GLS_MTv_VSD AS GLOSA_VISADA,
                    PKG_PAB_UTILITY.FN_PAB_VLD_TXT(DETOPC_HTRCA.OBS_OPC_SWF) AS OBSERVACION_SWIFT,
                    TO_CHAR(SYSDATE, 'HH24MI') AS INDHOR,
                    AOS.DSC_TPO_OPE_AOS AS DSC_TPO_OPE_AOS
            FROM    PABS_DT_DETLL_OPRCN_HTRCA DETOPE_HTRCA,
                    PABS_DT_OPRCN_OPCON_HTRCA DETOPC_HTRCA,
                    PABS_DT_ESTDO_ALMOT       ESTALM,
                    PABS_DT_SISTM_ENTRD_SALID ENT,
                    PABS_DT_SISTM_ENTRD_SALID SAL,
                    PABS_DT_TIPO_OPRCN_ALMOT  AOS,
                    PABS_DT_TIPO_FONDO_ALMOT  TFAM
           WHERE DETOPE_HTRCA.NUM_FOL_OPE = p_NUM_FOL_OPE
           AND DETOPE_HTRCA.FEC_ISR_OPE = p_FEC_ISR_OPE
           AND DETOPE_HTRCA.NUM_FOL_OPE = DETOPC_HTRCA.NUM_FOL_OPE(+)
           AND DETOPE_HTRCA.FEC_ISR_OPE = DETOPC_HTRCA.FEC_ISR_OPE(+)
           AND DETOPE_HTRCA.COD_EST_AOS = ESTALM.COD_EST_AOS
           AND DETOPE_HTRCA.COD_SIS_ENT = ENT.COD_SIS_ENT_SAL
           AND DETOPE_HTRCA.COD_SIS_SAL = SAL.COD_SIS_ENT_SAL(+)
           AND AOS.COD_TPO_OPE_AOS = DETOPE_HTRCA.COD_TPO_OPE_AOS
           AND DETOPC_HTRCA.COD_TPO_FND = TFAM.COD_TPO_FND(+);
    END IF;

    p_ERROR := PKG_PAB_CONSTANTES.v_OK; --Procedimiento ejecutado de forma correcta

  EXCEPTION

    WHEN NO_DATA_FOUND THEN

      err_code := SQLCODE;
      err_msg  := SUBSTR(SQLERRM, 1, 300) ||
                  'Error en consulta de detalle de operación: : ' ||
                  p_NUM_FOL_OPE;
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                         v_NOM_PCK,
                                         err_msg,
                                         v_NOM_SP);
      p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;

    WHEN OTHERS THEN

      err_code    := SQLCODE;
      err_msg     := SUBSTR(SQLERRM, 1, 300);
      p_s_mensaje := err_code || '-' || err_msg;
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                         v_NOM_PCK,
                                         err_msg,
                                         v_NOM_SP);
      p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);
  END Sp_PAB_BUS_DET_OPE;

  /************************************************************************************************

  -- Funcion/Procedimiento: Sp_PAB_ACT_EST_OPERACION
  -- Objetivo: Procedimiento que actualiza el estado de una operacion
  -- Sistema: DBO_PAB.
  -- Base de Datos: DBO_PAB.
  -- Tablas Usadas: PABS_DT_DETLL_OPRCN
  -- Fecha: 02/06/16
  -- Autor: Santander
  -- Input:    p_NUM_FOL_NMN->   foliador del banco
  --           p_FEC_ISR_OPE->   Fecha de insercion de la operacion
  --           p_NUM_REF_SWF->   Numero referencia Swift
  --           p_COD_EST_AOS->   Identificador de nuevo estado.
  --           p_COD_USR->       rut usuario
  -- Output:
  --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
  -- Input/Output: N/A
  -- Retorno: N/A.
  -- Observaciones: <Fecha y Detalle de ultimos cambios>
  --***********************************************************************************************/
  PROCEDURE Sp_PAB_ACT_EST_OPERACION(p_NUM_FOL_OPE IN NUMBER,
                                     p_FEC_ISR_OPE IN DATE,
                                     p_NUM_REF_SWF IN CHAR,
                                     p_COD_EST_AOS IN CHAR,
                                     p_COD_USR     IN VARCHAR2,
                                     p_ERROR       OUT NUMBER) IS

    v_NOM_SP          VARCHAR2(30) := 'Sp_PAB_ACT_EST_OPERACION';
    v_COD_EST_AOS     NUMBER;
    v_COD_EST_AOS_ACT NUMBER;
    v_RESULT          NUMBER;
    v_NUM_FOL_OPE     NUMBER;
    v_FEC_ISR_OPE     DATE;
    v_NUM_REF_SWF     CHAR(16);
    V_GLS_ADC_EST     VARCHAR2(210) := NULL;
    v_COD_SIS_SAL     CHAR(10);
    v_COD_DVI         CHAR(3);
    v_IMP_OPE         NUMBER;
    v_COD_BCO_DTN     VARCHAR2(11);

    --
    v_NUM_FOL_OPE_NOM NUMBER;
    ERROR_REFSWF EXCEPTION;

    --variables para actualizar operacion de origen
    v_REF_DEVFDOS         CHAR(8); -- Referencia MT202 Devolucion
    v_NUM_FOL_OPE_REL     NUMBER; -- Operacion relacionada
    v_FEC_ISR_OPE_REL     DATE; -- Fecha operacion relacionada
    v_NUM_REF_SWF_REL     CHAR(16);
    v_COD_EST_AOS_REL     NUMBER;
    v_COD_EST_AOS_ACT_REL NUMBER;

    v_NUM_FOL_OPE_ORG NUMBER;
    v_FEC_ISR_OPE_ORG DATE;
    v_NUM_REF_SWF_ORG CHAR(16);

    v_FEC_CFM_SWF DATE; --(Fecha Respondio Swift)
    v_HOR_CFM_SWF NUMBER; --(Fecha Respondio Swift)

  BEGIN

    --Si la busqueda es por numero de referencia Swift, buscamos el numero de operacion y la fecha.
    -- El numero de operacion si es igual a 0 significa nulo para tibco.
    IF (NVL(p_NUM_REF_SWF, '#') <> '#' AND NVL(p_NUM_FOL_OPE, 0) = 0) THEN

      v_NUM_REF_SWF := p_NUM_REF_SWF;

      BEGIN

        --Obtenemos la informacion de la operacion origen
        SELECT NUM_FOL_OPE, FEC_ISR_OPE
          INTO v_NUM_FOL_OPE, v_FEC_ISR_OPE
          FROM PABS_DT_DETLL_OPRCN
         WHERE NUM_REF_SWF = v_NUM_REF_SWF
           AND FLG_EGR_ING = PKG_PAB_CONSTANTES.V_FLG_EGR;

        IF (v_NUM_FOL_OPE IS NULL OR v_FEC_ISR_OPE IS NULL) THEN
          RAISE ERROR_REFSWF;
        END IF;

      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          err_code := SQLCODE;
          err_msg  := 'No se pudo obtener la información de la operación origen:' ||
                      v_NUM_REF_SWF;
          RAISE;

        WHEN OTHERS THEN
          err_code    := SQLCODE;
          err_msg     := SUBSTR(SQLERRM, 1, 300);
          p_s_mensaje := err_code || '-' || err_msg;
          PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
          p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
          RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);

      END;

    ELSE
      v_NUM_FOL_OPE := p_NUM_FOL_OPE;
      v_FEC_ISR_OPE := p_FEC_ISR_OPE;
    END IF;


    --SE AGREGA VALIDACION DE ESTADO PARA CONTROLAR ENVIADA - PAGADA

    IF p_COD_EST_AOS = PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEENV THEN

         SP_PAB_OBT_EST_ACT_OPE(v_NUM_FOL_OPE, v_FEC_ISR_OPE, v_COD_EST_AOS_ACT);

         v_COD_EST_AOS := PKG_PAB_UTILITY.FN_PAB_OBT_COD_DSC_EST(p_COD_EST_AOS,
                                                            PKG_PAB_CONSTANTES.V_IND_OPE);

        IF v_COD_EST_AOS_ACT =   PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAG THEN -- SI EL ESTADO ES PAGADO
        --Actualizamos estado nuevo de la operacion.enviada


        --Llamamos al procedimiento de Bitacora
        v_DES_BIT := 'Operacion en Estado Pagada. no se puede actualizar a Enviada.';
        PKG_PAB_TRACKING.Sp_PAB_INS_BIT_OPE(v_NUM_FOL_OPE,
                                            v_FEC_ISR_OPE,
                                            p_COD_USR,
                                            v_COD_EST_AOS_ACT,
                                            v_COD_EST_AOS_ACT,
                                            v_DES_BIT,
                                            v_RESULT);

          ELSE

               UPDATE PABS_DT_DETLL_OPRCN op
               SET COD_EST_AOS = v_COD_EST_AOS, FEC_ACT_OPE = SYSDATE -- Fecha actualización para dashborad
               WHERE NUM_FOL_OPE = v_NUM_FOL_OPE
               AND FEC_ISR_OPE = v_FEC_ISR_OPE;

               v_DES_BIT := 'Cambio de estado operacion';
               PKG_PAB_TRACKING.Sp_PAB_INS_BIT_OPE(v_NUM_FOL_OPE,
                                            v_FEC_ISR_OPE,
                                            p_COD_USR,
                                            v_COD_EST_AOS_ACT,
                                            v_COD_EST_AOS,
                                            v_DES_BIT,
                                            v_RESULT);

           END IF;

    ELSE

    -- Estados al que se quiere actualizar
        v_COD_EST_AOS := PKG_PAB_UTILITY.FN_PAB_OBT_COD_DSC_EST(p_COD_EST_AOS,
                                                                PKG_PAB_CONSTANTES.V_IND_OPE);

        -- Obtener el estado actual de la operacion enviada
        SP_PAB_OBT_EST_ACT_OPE(v_NUM_FOL_OPE, v_FEC_ISR_OPE, v_COD_EST_AOS_ACT);

        --Actualizamos estado nuevo de la operacion.enviada
        UPDATE PABS_DT_DETLL_OPRCN op
           SET COD_EST_AOS = v_COD_EST_AOS, FEC_ACT_OPE = SYSDATE -- Fecha actualización para dashborad
         WHERE NUM_FOL_OPE = v_NUM_FOL_OPE
           AND FEC_ISR_OPE = v_FEC_ISR_OPE;

        --Llamamos al procedimiento de Bitacora
        v_DES_BIT := 'Cambio de estado operacion';
        PKG_PAB_TRACKING.Sp_PAB_INS_BIT_OPE(v_NUM_FOL_OPE,
                                            v_FEC_ISR_OPE,
                                            p_COD_USR,
                                            v_COD_EST_AOS_ACT,
                                            v_COD_EST_AOS,
                                            v_DES_BIT,
                                            v_RESULT);

    END IF;

    -- Obtenemos informacion de la operacion
    BEGIN

      SELECT SUBSTR(NUM_REF_SWF, 1, 7),
             NUM_FOL_OPE,
             FEC_ISR_OPE,
             NUM_FOL_OPE_ORG,
             NUM_REF_EXT,
             NUM_FOL_NMN,
             COD_SIS_SAL,
             COD_DVI,
             IMP_OPE,
             COD_BCO_DTN
        INTO V_REF_DEVFDOS,
             v_NUM_FOL_OPE,
             v_FEC_ISR_OPE,
             v_NUM_FOL_OPE_REL,
             v_NUM_REF_SWF_REL,
             v_NUM_FOL_OPE_NOM,
             v_COD_SIS_SAL,
             v_COD_DVI,
             v_IMP_OPE,
             v_COD_BCO_DTN
        FROM PABS_DT_DETLL_OPRCN
       WHERE NUM_FOL_OPE = v_NUM_FOL_OPE
         AND FEC_ISR_OPE = v_FEC_ISR_OPE;

    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        err_code := SQLCODE;
        err_msg  := 'No se pudo obtener informacion de la operacion:' ||
                    v_NUM_FOL_OPE;
        RAISE;
      WHEN OTHERS THEN
        err_code    := SQLCODE;
        err_msg     := SUBSTR(SQLERRM, 1, 300);
        p_s_mensaje := err_code || '-' || err_msg;
        PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                           v_NOM_PCK,
                                           err_msg,
                                           v_NOM_SP);
        p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
        RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);

    END;

    --Si existe en nomina actualizamos al estado nuevo
    IF v_NUM_FOL_OPE_NOM > 0 THEN

      --Actualizamos el estado de la operacion en la tabla Nomina.
      PKG_PAB_NOMINA.Sp_PAB_ACT_EST_OPE_NOMINA(v_NUM_FOL_OPE,
                                               v_FEC_ISR_OPE,
                                               v_COD_EST_AOS,
                                               V_GLS_ADC_EST,
                                               v_RESULT);

      --Actualizamos el canal de salida solo cuando el estado nuevo es liberada
      IF (PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPELIB = v_COD_EST_AOS) THEN

        --Actualizamos
        BEGIN

          UPDATE PABS_DT_DETLL_NOMNA
             SET COD_SIS_SAL = v_COD_SIS_SAL, FEC_ACT_NMN = SYSDATE -- Fecha actualización para dashborad
           WHERE NUM_FOL_OPE = v_NUM_FOL_OPE
             AND FEC_ISR_OPE = v_FEC_ISR_OPE;

        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            err_code := SQLCODE;
            err_msg  := 'No se pudo obtener informacion de la operacion:' ||
                        v_NUM_FOL_OPE;
            RAISE;
          WHEN OTHERS THEN
            err_code    := SQLCODE;
            err_msg     := SUBSTR(SQLERRM, 1, 300);
            p_s_mensaje := err_code || '-' || err_msg;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                               v_NOM_PCK,
                                               err_msg,
                                               v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);

        END;

      END IF;

    END IF;

    --Si el estado nuevo es "por devoler" debemos actualizar (Para pagos recibidos)
    IF p_COD_EST_AOS = PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEPDV THEN

      --Actualizamos
      BEGIN

        UPDATE PABS_DT_DETLL_OPRCN
           SET COD_SIS_SAL = NVL(COD_SIS_SAL,
                                 PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SE_OOFF),
               FEC_ACT_OPE = SYSDATE -- Fecha actualización para dashborad
         WHERE NUM_FOL_OPE = v_NUM_FOL_OPE
           AND FEC_ISR_OPE = v_FEC_ISR_OPE;

      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          err_code := SQLCODE;
          err_msg  := 'No se pudo obtener informacion de la operacion:' ||
                      v_NUM_FOL_OPE;
          RAISE;
        WHEN OTHERS THEN
          err_code    := SQLCODE;
          err_msg     := SUBSTR(SQLERRM, 1, 300);
          p_s_mensaje := err_code || '-' || err_msg;
          PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
          p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
          RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);

      END;

    END IF;

    --Si el estado nuevo es pagada debemos actualizar (Para pagos enviados)
    IF p_COD_EST_AOS = PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEPAG THEN

      IF v_REF_DEVFDOS = PKG_PAB_CONSTANTES.V_STR_TPO_OPE_DEVFDOS THEN
        --Si la referencia es 'DEVFDOS'

        --Buscamos datos de la operacion Origen (103,202 o 202 Dev) 12009   REL
        SELECT FEC_ISR_OPE
          INTO v_FEC_ISR_OPE_REL
          FROM PABS_DT_DETLL_OPRCN
         WHERE NUM_FOL_OPE = v_NUM_FOL_OPE_REL
           AND NUM_REF_SWF = v_NUM_REF_SWF_REL;

        --Obtener el estado actual
        SP_PAB_OBT_EST_ACT_OPE(v_NUM_FOL_OPE_REL,
                               v_FEC_ISR_OPE_REL,
                               v_COD_EST_AOS_ACT_REL);

        v_COD_EST_AOS_REL := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDEV;

        --Actualizamos estado nuevo de la operacion. a Devuelta
        UPDATE PABS_DT_DETLL_OPRCN
           SET COD_EST_AOS = v_COD_EST_AOS_REL, FEC_ACT_OPE = SYSDATE -- Fecha actualización para dashborad
         WHERE NUM_FOL_OPE = v_NUM_FOL_OPE_REL
           AND FEC_ISR_OPE = v_FEC_ISR_OPE_REL;

        --Llamamos al procedimiento de Bitacora
        v_DES_BIT := 'Cambio de estado operacion';
        PKG_PAB_TRACKING.Sp_PAB_INS_BIT_OPE(v_NUM_FOL_OPE_REL,
                                            v_FEC_ISR_OPE_REL,
                                            p_COD_USR,
                                            v_COD_EST_AOS_ACT_REL,
                                            v_COD_EST_AOS_REL,
                                            v_DES_BIT,
                                            v_RESULT);

      END IF;

    END IF;

    p_ERROR := PKG_PAB_CONSTANTES.v_OK;

    --Guardamos
    COMMIT;

  EXCEPTION
    WHEN ERROR_REFSWF THEN
      err_msg := 'No se encontro la operacion con el codigo de referencia:' ||
                 p_NUM_REF_SWF;
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(NULL,
                                         v_NOM_PCK,
                                         err_msg,
                                         v_NOM_SP);
      p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;

    WHEN NO_DATA_FOUND THEN

      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                         v_NOM_PCK,
                                         err_msg,
                                         v_NOM_SP);
      p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;

    WHEN OTHERS THEN

      err_code    := SQLCODE;
      err_msg     := SUBSTR(SQLERRM, 1, 300);
      p_s_mensaje := err_code || '-' || err_msg;
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                         v_NOM_PCK,
                                         err_msg,
                                         v_NOM_SP);
      p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);

  END Sp_PAB_ACT_EST_OPERACION;

  /************************************************************************************************
  -- Funcion/Procedimiento: Sp_PAB_ACT_EST_OPERACION_BTC
  -- Objetivo: Procedimiento que actualiza el estado de una operacion y permite recibir la bitacora que se desea agregar
  -- Sistema: DBO_PAB.
  -- Base de Datos: DBO_PAB.
  -- Tablas Usadas: PABS_DT_DETLL_OPRCN
  -- Fecha: 02/06/16
  -- Autor: Santander
  -- Input:    p_NUM_FOL_NMN->   foliador del banco
  --           p_FEC_ISR_OPE->   Fecha de insercion de la operacion
  --           p_NUM_REF_SWF->   Numero referencia Swift
  --           p_COD_EST_AOS->   Identificador de nuevo estado.
  --           p_COD_USR->       rut usuario
  --           p_DSC_GLB_BTC->   Bitacora de la operación
  -- Output:
  --           p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
  -- Input/Output: N/A
  -- Retorno: N/A.
  -- Observaciones: <Fecha y Detalle de ultimos cambios>
  --***********************************************************************************************/
  PROCEDURE Sp_PAB_ACT_EST_OPERACION_BTC(p_NUM_FOL_OPE IN NUMBER,
                                         p_FEC_ISR_OPE IN DATE,
                                         p_NUM_REF_SWF IN CHAR,
                                         p_COD_EST_AOS IN CHAR,
                                         p_COD_USR     IN VARCHAR2,
                                         p_DSC_GLB_BTC IN VARCHAR2,
                                         p_ERROR       OUT NUMBER) IS

    v_NOM_SP          VARCHAR2(30) := 'Sp_PAB_ACT_EST_OPERACION_BTC';
    v_COD_EST_AOS     NUMBER;
    v_COD_EST_AOS_ACT NUMBER;
    v_RESULT          NUMBER;
    v_NUM_FOL_OPE     NUMBER;
    v_FEC_ISR_OPE     DATE;
    v_NUM_REF_SWF     CHAR(16);
    V_GLS_ADC_EST     VARCHAR2(210) := NULL;
    v_COD_SIS_SAL     CHAR(10);
    --
    v_NUM_FOL_OPE_NOM NUMBER;
    ERROR_REFSWF EXCEPTION;

    --variables para actualizar operacion de origen
    v_REF_DEVFDOS         CHAR(8); -- Referencia MT202 Devolucion
    v_NUM_FOL_OPE_REL     NUMBER; -- Operacion relacionada
    v_FEC_ISR_OPE_REL     DATE; -- Fecha operacion relacionada
    v_NUM_REF_SWF_REL     CHAR(16);
    v_COD_EST_AOS_REL     NUMBER;
    v_COD_EST_AOS_ACT_REL NUMBER;

    v_NUM_FOL_OPE_ORG NUMBER;
    v_FEC_ISR_OPE_ORG DATE;
    v_NUM_REF_SWF_ORG CHAR(16);

    --Registro envio a Swift
    v_FEC_ENV_RCP_SWF DATE; --(Fecha envio operación)
    v_HOR_ENV_RCP_SWF NUMBER; --(Hora envio operación)
    v_FEC_AZA_SWF     DATE; --(Fecha Autorizo operación)
    v_HOR_AZA_SWF     NUMBER; --(Hora Autorizo operación)

  BEGIN

    --Si la busqueda es por numero de referencia Swift, buscamos el numero de operacion y la fecha.
    -- El numero de operacion si es igual a 0 significa nulo para tibco.
    IF (NVL(p_NUM_REF_SWF, '#') <> '#' AND NVL(p_NUM_FOL_OPE, 0) = 0) THEN

      v_NUM_REF_SWF := p_NUM_REF_SWF;

      BEGIN

        --Obtenemos la informacion de la operacion origen
        SELECT NUM_FOL_OPE, FEC_ISR_OPE
          INTO v_NUM_FOL_OPE, v_FEC_ISR_OPE
          FROM PABS_DT_DETLL_OPRCN OP
         WHERE NUM_REF_SWF = v_NUM_REF_SWF;

        IF (v_NUM_FOL_OPE IS NULL OR v_FEC_ISR_OPE IS NULL) THEN
          RAISE ERROR_REFSWF;
        END IF;

      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          err_code := SQLCODE;
          err_msg  := 'No se pudo obtener la información de la operación origen:' ||
                      v_NUM_REF_SWF;
          PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
          p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
          RAISE;

        WHEN OTHERS THEN
          err_code    := SQLCODE;
          err_msg     := SUBSTR(SQLERRM, 1, 300);
          p_s_mensaje := err_code || '-' || err_msg;
          PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
          p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
          RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);

      END;

    ELSE
      v_NUM_FOL_OPE := p_NUM_FOL_OPE;
      v_FEC_ISR_OPE := p_FEC_ISR_OPE;
    END IF;

    -- Estados al que se quiere actualizar
    v_COD_EST_AOS := PKG_PAB_UTILITY.FN_PAB_OBT_COD_DSC_EST(p_COD_EST_AOS,
                                                            PKG_PAB_CONSTANTES.V_IND_OPE);

    --Si es pago por contingencia se actuliza la hora en envio a swift
    IF v_COD_EST_AOS = PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAGCFI THEN
      v_FEC_AZA_SWF     := TO_DATE(TO_CHAR(SYSDATE), 'DD-MM-YYY');
      v_HOR_AZA_SWF     := TO_CHAR(SYSDATE, 'HH24MI');
      v_FEC_ENV_RCP_SWF := TO_DATE(TO_CHAR(SYSDATE), 'DD-MM-YYY');
      v_HOR_ENV_RCP_SWF := TO_CHAR(SYSDATE, 'HH24MI');

    END IF;

    -- Obtener el estado actual de la operacion enviada
    SP_PAB_OBT_EST_ACT_OPE(v_NUM_FOL_OPE, v_FEC_ISR_OPE, v_COD_EST_AOS_ACT);

    --Actualizamos estado nuevo de la operacion.enviada
    UPDATE PABS_DT_DETLL_OPRCN op
       SET COD_EST_AOS     = v_COD_EST_AOS,
           FEC_AZA_SWF     = v_FEC_AZA_SWF,
           HOR_AZA_SWF     = v_HOR_AZA_SWF,
           FEC_ENV_RCP_SWF = v_FEC_ENV_RCP_SWF,
           HOR_ENV_RCP_SWF = v_HOR_ENV_RCP_SWF,
           FEC_ACT_OPE     = SYSDATE -- Fecha actualización para dashborad
     WHERE NUM_FOL_OPE = v_NUM_FOL_OPE
       AND FEC_ISR_OPE = v_FEC_ISR_OPE;

    --Llamamos al procedimiento de Bitacora
    v_DES_BIT := p_DSC_GLB_BTC;
    PKG_PAB_TRACKING.Sp_PAB_INS_BIT_OPE(v_NUM_FOL_OPE,
                                        v_FEC_ISR_OPE,
                                        p_COD_USR,
                                        v_COD_EST_AOS_ACT,
                                        v_COD_EST_AOS,
                                        v_DES_BIT,
                                        v_RESULT);

    -- Obtenemos informacion de la operacion
    BEGIN

      SELECT SUBSTR(NUM_REF_SWF, 1, 7),
             NUM_FOL_OPE,
             FEC_ISR_OPE,
             NUM_FOL_OPE_ORG,
             NUM_REF_EXT,
             NUM_FOL_NMN,
             COD_SIS_SAL
        INTO V_REF_DEVFDOS,
             v_NUM_FOL_OPE,
             v_FEC_ISR_OPE,
             v_NUM_FOL_OPE_REL,
             v_NUM_REF_SWF_REL,
             v_NUM_FOL_OPE_NOM,
             v_COD_SIS_SAL
        FROM PABS_DT_DETLL_OPRCN
       WHERE NUM_FOL_OPE = v_NUM_FOL_OPE
         AND FEC_ISR_OPE = v_FEC_ISR_OPE;

    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        err_msg := 'No se pudo obtener informacion de la operacion:' ||
                   v_NUM_FOL_OPE;
        PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(NULL,
                                           v_NOM_PCK,
                                           err_msg,
                                           v_NOM_SP);
      WHEN OTHERS THEN
        err_code    := SQLCODE;
        err_msg     := SUBSTR(SQLERRM, 1, 300);
        p_s_mensaje := err_code || '-' || err_msg;
        PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                           v_NOM_PCK,
                                           err_msg,
                                           v_NOM_SP);
        p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
        RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);

    END;

    --Si existe en nomina actualizamos al estado nuevo
    IF v_NUM_FOL_OPE_NOM > 0 THEN

      --Actualizamos el estado de la operacion en la tabla Nomina.
      PKG_PAB_NOMINA.Sp_PAB_ACT_EST_OPE_NOMINA(v_NUM_FOL_OPE,
                                               v_FEC_ISR_OPE,
                                               v_COD_EST_AOS,
                                               V_GLS_ADC_EST,
                                               v_RESULT);

      --Actualizamos el canal de salida solo cuando el estado nuevo es liberada
      IF (PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPELIB = v_COD_EST_AOS) THEN

        --Actualizamos
        BEGIN

          UPDATE PABS_DT_DETLL_NOMNA
             SET COD_SIS_SAL = v_COD_SIS_SAL, FEC_ACT_NMN = SYSDATE -- Fecha actualización para dashborad
           WHERE NUM_FOL_OPE = v_NUM_FOL_OPE
             AND FEC_ISR_OPE = v_FEC_ISR_OPE;

        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            err_msg := 'No se pudo obtener informacion de la operacion:' ||
                       v_NUM_FOL_OPE;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(NULL,
                                               v_NOM_PCK,
                                               err_msg,
                                               v_NOM_SP);
          WHEN OTHERS THEN
            err_code    := SQLCODE;
            err_msg     := SUBSTR(SQLERRM, 1, 300);
            p_s_mensaje := err_code || '-' || err_msg;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                               v_NOM_PCK,
                                               err_msg,
                                               v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);

        END;

      END IF;

    END IF;

    --Si el estado nuevo es "por devoler" debemos actualizar (Para pagos recibidos)
    IF p_COD_EST_AOS = PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEPDV THEN

      --Actualizamos
      BEGIN

        UPDATE PABS_DT_DETLL_OPRCN
           SET COD_SIS_SAL = NVL(COD_SIS_SAL,
                                 PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SE_OOFF),
               FEC_ACT_OPE = SYSDATE -- Fecha actualización para dashborad
         WHERE NUM_FOL_OPE = v_NUM_FOL_OPE
           AND FEC_ISR_OPE = v_FEC_ISR_OPE;

      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          err_msg := 'No se pudo obtener informacion de la operacion:' ||
                     v_NUM_FOL_OPE;
          PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(NULL,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
        WHEN OTHERS THEN
          err_code    := SQLCODE;
          err_msg     := SUBSTR(SQLERRM, 1, 300);
          p_s_mensaje := err_code || '-' || err_msg;
          PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
          p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
          RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);

      END;

    END IF;

    --Si el estado nuevo es pagada debemos actualizar (Para pagos enviados)
    IF p_COD_EST_AOS = PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEPAG THEN

      IF v_REF_DEVFDOS = PKG_PAB_CONSTANTES.V_STR_TPO_OPE_DEVFDOS THEN
        --Si la referencia es 'DEVFDOS'

        --Buscamos datos de la operacion Origen (103,202 o 202 Dev) 12009   REL
        SELECT FEC_ISR_OPE
          INTO v_FEC_ISR_OPE_REL
          FROM PABS_DT_DETLL_OPRCN
         WHERE NUM_FOL_OPE = v_NUM_FOL_OPE_REL
           AND NUM_REF_SWF = v_NUM_REF_SWF_REL;

        --Obtener el estado actual
        SP_PAB_OBT_EST_ACT_OPE(v_NUM_FOL_OPE_REL,
                               v_FEC_ISR_OPE_REL,
                               v_COD_EST_AOS_ACT_REL);

        v_COD_EST_AOS_REL := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDEV;

        --Actualizamos estado nuevo de la operacion. a Devuelta
        UPDATE PABS_DT_DETLL_OPRCN
           SET COD_EST_AOS = v_COD_EST_AOS_REL, FEC_ACT_OPE = SYSDATE -- Fecha actualización para dashborad
         WHERE NUM_FOL_OPE = v_NUM_FOL_OPE_REL
           AND FEC_ISR_OPE = v_FEC_ISR_OPE_REL;

        --Llamamos al procedimiento de Bitacora
        v_DES_BIT := p_DSC_GLB_BTC;
        PKG_PAB_TRACKING.Sp_PAB_INS_BIT_OPE(v_NUM_FOL_OPE_REL,
                                            v_FEC_ISR_OPE_REL,
                                            p_COD_USR,
                                            v_COD_EST_AOS_ACT_REL,
                                            v_COD_EST_AOS_REL,
                                            v_DES_BIT,
                                            v_RESULT);

      END IF;

    END IF;

    p_ERROR := PKG_PAB_CONSTANTES.v_OK;

    --Guardamos
    COMMIT;

  EXCEPTION
    WHEN ERROR_REFSWF THEN
      err_msg := 'No se encontro la operacion con el codigo de referencia:' ||
                 p_NUM_REF_SWF;
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(NULL,
                                         v_NOM_PCK,
                                         err_msg,
                                         v_NOM_SP);
      p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;

    WHEN NO_DATA_FOUND THEN
      err_code := SQLCODE;
      err_msg  := 'No se puedo realizad cambio de estado a la operacion:' ||
                  v_NUM_FOL_OPE_REL;
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                         v_NOM_PCK,
                                         err_msg,
                                         v_NOM_SP);
      p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;

    WHEN OTHERS THEN

      err_code    := SQLCODE;
      err_msg     := SUBSTR(SQLERRM, 1, 300);
      p_s_mensaje := err_code || '-' || err_msg;
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                         v_NOM_PCK,
                                         err_msg,
                                         v_NOM_SP);
      p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);
  END Sp_PAB_ACT_EST_OPERACION_BTC;

  /************************************************************************************************
  -- Funcion/Procedimiento: SP_PAB_OBT_EST_ACT_OPE
  -- Objetivo: Procedimiento que obtiene el estado actual de la operacion
  -- Sistema: DBO_PAB.
  -- Base de Datos: DBO_PAB.
  -- Tablas Usadas: PABS_DT_DETLL_OPRCN
  -- Fecha: 02/06/16
  -- Autor: Santander
  -- Input:    p_NUM_FOL_OPE: Numero de Folio de una Operacion
  --           p_FEC_ISR_OPE: Fecha de una Operacion
  -- Output:
  --           p_COD_EST_AOS: Estado actual de la operacion
  -- Input/Output: N/A
  -- Retorno: N/A.
  -- Observaciones: <Fecha y Detalle de ultimos cambios>
  --***********************************************************************************************/
  PROCEDURE SP_PAB_OBT_EST_ACT_OPE(p_NUM_FOL_OPE IN NUMBER,
                                   p_FEC_ISR_OPE IN DATE,
                                   p_COD_EST_AOS OUT NUMBER) IS
    v_NOM_SP VARCHAR2(30) := 'SP_PAB_OBT_EST_ACT_OPE';
  BEGIN
    --Obtenemos el estado actual de la operacion
    SELECT COD_EST_AOS
      INTO p_COD_EST_AOS
      FROM PABS_DT_DETLL_OPRCN
     WHERE NUM_FOL_OPE = p_NUM_FOL_OPE
       AND FEC_ISR_OPE = p_FEC_ISR_OPE;

  EXCEPTION
    WHEN NO_DATA_FOUND THEN

      err_code := SQLCODE;
      err_msg  := 'No se encontro operación folio:' || p_NUM_FOL_OPE ||
                  ' Fecha:' ||
                  TO_CHAR(p_FEC_ISR_OPE, 'dd-mm-yyyy hh24:mi:ss');
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                         v_NOM_PCK,
                                         err_msg,
                                         v_NOM_SP);

    WHEN OTHERS THEN

      err_code    := SQLCODE;
      err_msg     := SUBSTR(SQLERRM, 1, 300);
      p_s_mensaje := err_code || '-' || err_msg;
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                         v_NOM_PCK,
                                         err_msg,
                                         v_NOM_SP);
      RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);
  END SP_PAB_OBT_EST_ACT_OPE;

  /************************************************************************************************
  -- Funcion/Procedimiento: Sp_PAB_ACT_DATOS_OPE
  -- Objetivo: Procedimiento que actualiza informacion de una operacion
  -- Sistema: DBO_PAB.
  -- Base de Datos: DBO_PAB.
  -- Tablas Usadas: PABS_DT_DETLL_OPRCN, PABS_DT_OPRCN_INFCN_OPCON
  -- Fecha: 02/06/16
  -- Autor: Santander
  -- Input:    p_NUM_FOL_NMN->     foliador del banco
  --           p_FEC_ISR_OPE->     Fecha de insercion de la operacion
  --           p_NUM_REF_SWF->     Numero de referencia del swift
  --           p_FEC_ENv_RCp_SWF-> Fecha Swift
  --           p_HOR_ENv_RCp_SWF-> Hora Swift
  --           p_GLS_ADC_EST->     Glosa Adicional Estado
  --           p_GLS_EST_RCH->     Glosa Estado Rechazado
  --           p_GLS_MTv_VSD->     Glosa Motivo Visacion
  --           p_COD_USR->         rut usuario
  -- Output:
  --          p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
  -- Input/Output: N/A
  -- Retorno: N/A.
  -- Observaciones: <Fecha y Detalle de ultimos cambios>
  --***********************************************************************************************/
  PROCEDURE Sp_PAB_ACT_DATOS_OPE(p_NUM_FOL_OPE     IN NUMBER,
                                 p_FEC_ISR_OPE     IN DATE,
                                 p_NUM_REF_SWF     IN VARCHAR2,
                                 p_FEC_ENv_RCp_SWF IN DATE,
                                 p_HOR_ENv_RCp_SWF IN NUMBER,
                                 p_GLS_ADC_EST     IN VARCHAR2,
                                 p_GLS_EST_RCH     IN VARCHAR2,
                                 p_GLS_MTV_VSD     IN VARCHAR2,
                                 p_COD_USR         IN VARCHAR2,
                                 p_ERROR           OUT NUMBER) IS
    v_NOM_SP          VARCHAR2(30) := 'Sp_PAB_ACT_DATOS_OPE';
    v_NUM_REF_SWF     CHAR(16);
    v_FEC_ENv_RCp_SWF DATE;
    v_HOR_ENv_RCp_SWF NUMBER(4);
    v_GLS_ADC_EST     VARCHAR2(210);
    v_GLS_EST_RCH     VARCHAR2(300);
    v_GLS_MTv_VSD     VARCHAR2(300);
    v_NUM_FOL_OPE     NUMBER;
    v_FEC_ISR_OPE     DATE;
    v_NUM_FOL_NMN     NUMBER;
    v_COD_USR         CHAR(11);
    ERROR_REFSWF EXCEPTION;

  BEGIN

    v_COD_USR := p_COD_USR;

    --Si la busqueda es por numero de referencia Swift, buscamos el numero de operacion y la fecha.
    IF (NVL(p_NUM_REF_SWF, '#') <> '#' AND NVL(p_NUM_FOL_OPE, 0) = 0) THEN

      BEGIN

        v_NUM_REF_SWF := p_NUM_REF_SWF;

        SELECT NUM_FOL_OPE, FEC_ISR_OPE
          INTO v_NUM_FOL_OPE, v_FEC_ISR_OPE
          FROM PABS_DT_DETLL_OPRCN
         WHERE NUM_REF_SWF = v_NUM_REF_SWF
           AND FLG_EGR_ING = PKG_PAB_CONSTANTES.V_FLG_EGR; -- Solo operaciones de egreso
        --AND TRUNC(FEC_ISR_OPE) = TRUNC(SYSDATE); --Solo permitimos a la integracion actualizar operaciones del dia

        IF (v_NUM_FOL_OPE IS NULL OR v_FEC_ISR_OPE IS NULL) THEN
          RAISE ERROR_REFSWF;
        END IF;

      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          err_code := SQLCODE;
          err_msg  := 'Rescatando fecha: No se encontro informacion con v_NUM_REF_SWF: ' ||
                      v_NUM_REF_SWF;
          PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
          p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
          RAISE;

        WHEN OTHERS THEN
          err_code    := SQLCODE;
          err_msg     := SUBSTR(SQLERRM, 1, 300);
          p_s_mensaje := err_code || '-' || err_msg;
          PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
          p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
          RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);

      END;
    ELSE
      v_NUM_FOL_OPE := p_NUM_FOL_OPE;
      v_FEC_ISR_OPE := p_FEC_ISR_OPE;
    END IF;

    --Rescatamos valores de la operacion.
    BEGIN

      SELECT OPE.NUM_REF_SWF,
             OPE.FEC_ENv_RCp_SWF,
             OPE.HOR_ENv_RCp_SWF,
             OPE.GLS_ADC_EST,
             DETOPE.GLS_EST_RCH,
             DETOPE.GLS_MTv_VSD,
             NUM_FOL_NMN
        INTO v_NUM_REF_SWF,
             v_FEC_ENv_RCp_SWF,
             v_HOR_ENv_RCp_SWF,
             v_GLS_ADC_EST,
             v_GLS_EST_RCH,
             v_GLS_MTv_VSD,
             v_NUM_FOL_NMN
        FROM PABS_DT_DETLL_OPRCN OPE, PABS_DT_OPRCN_INFCN_OPCON DETOPE
       WHERE OPE.FEC_ISR_OPE = DETOPE.FEC_ISR_OPE(+)
         AND OPE.NUM_FOL_OPE = DETOPE.NUM_FOL_OPE(+)
         AND OPE.NUM_FOL_OPE = v_NUM_FOL_OPE
         AND OPE.FEC_ISR_OPE = v_FEC_ISR_OPE;

    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        err_code := SQLCODE;
        err_msg  := 'Rescatando Informacion: No se encontro informacion con v_NUM_FOL_OPE: ' ||
                    v_NUM_FOL_OPE || ' v_FEC_ISR_OPE:' ||
                    TO_CHAR(v_FEC_ISR_OPE, 'dd-mm-yyyy hh24:mi:ss');
        PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                           v_NOM_PCK,
                                           err_msg,
                                           v_NOM_SP);
        p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
        RAISE;

      WHEN OTHERS THEN
        err_code    := SQLCODE;
        err_msg     := SUBSTR(SQLERRM, 1, 300);
        p_s_mensaje := err_code || '-' || err_msg;
        PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                           v_NOM_PCK,
                                           err_msg,
                                           v_NOM_SP);
        p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
        RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);
    END;

    --Verificamos que hay que actualizar

    --Si viene informacion en el codigo referencia Swift
    IF (NVL(p_NUM_REF_SWF, '#') <> '#') THEN
      v_NUM_REF_SWF := p_NUM_REF_SWF;
    END IF;

    --Si viene informacion en la fecha de envio
    IF (NVL(p_FEC_ENv_RCp_SWF, TO_DATE('01/01/1900', 'DD-MM-YYYY')) <>
       TO_DATE('01/01/1900', 'DD-MM-YYYY')) THEN
      v_FEC_ENv_RCp_SWF := p_FEC_ENv_RCp_SWF;
    END IF;

    --Si viene informacion la hora de envio swift
    IF (NVL(p_HOR_ENv_RCp_SWF, 0) <> 0) THEN
      v_HOR_ENv_RCp_SWF := p_HOR_ENv_RCp_SWF;
    END IF;

    --Si viene informacion en glosa adicional de estado
    IF (NVL(p_GLS_ADC_EST, '#') <> '#') THEN
      v_GLS_ADC_EST := p_GLS_ADC_EST;
    END IF;

    --Si viene informacion en el glosa adicional rechazot
    IF (NVL(p_GLS_EST_RCH, '#') <> '#') THEN
      v_GLS_EST_RCH := p_GLS_EST_RCH;
    END IF;

    --Si viene informacion en glosa motivo visada
    IF (NVL(p_GLS_MTv_VSD, '#') <> '#') THEN
      v_GLS_MTv_VSD := p_GLS_MTv_VSD;
    END IF;

    --Actualizamos informacion de la operacion
    IF (NVL(p_NUM_REF_SWF, '#') <> '#' OR NVL(p_HOR_ENv_RCp_SWF, 0) <> 0 OR
       NVL(p_GLS_ADC_EST, '#') <> '#') THEN

      BEGIN

        --Actualizamos  Operacion
        UPDATE PABS_DT_DETLL_OPRCN OPE
           SET NUM_REF_SWF     = v_NUM_REF_SWF,
               FEC_ENv_RCp_SWF = v_FEC_ENv_RCp_SWF,
               HOR_ENv_RCp_SWF = v_HOR_ENv_RCp_SWF,
               GLS_ADC_EST     = v_GLS_ADC_EST
         WHERE OPE.NUM_FOL_OPE = v_NUM_FOL_OPE
           AND OPE.FEC_ISR_OPE = v_FEC_ISR_OPE;

      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          err_code := SQLCODE;
          err_msg  := 'Act. Operacion: No se encontro informacion con v_NUM_FOL_OPE: ' ||
                      v_NUM_FOL_OPE || ' v_FEC_ISR_OPE:' ||
                      TO_CHAR(v_FEC_ISR_OPE, 'dd-mm-yyyy hh24:mi:ss');
          PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
          p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
          RAISE;

        WHEN OTHERS THEN
          err_code    := SQLCODE;
          err_msg     := SUBSTR(SQLERRM, 1, 300);
          p_s_mensaje := err_code || '-' || err_msg;
          PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
          p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
          RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);
      END;

    END IF;

    --Actualizamos informacion opcional de la operacion
    IF (NVL(p_GLS_EST_RCH, '#') <> '#' OR NVL(p_GLS_MTv_VSD, '#') <> '#') THEN

      BEGIN

        --
        UPDATE PABS_DT_OPRCN_INFCN_OPCON OPEOP
           SET GLS_EST_RCH = v_GLS_EST_RCH, GLS_MTv_VSD = v_GLS_MTv_VSD
         WHERE OPEOP.NUM_FOL_OPE = v_NUM_FOL_OPE
           AND OPEOP.FEC_ISR_OPE = v_FEC_ISR_OPE;

      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          err_code := SQLCODE;
          err_msg  := 'Act. Operacion Opcional: No se encontro informacion con v_NUM_FOL_OPE: ' ||
                      v_NUM_FOL_OPE || ' v_FEC_ISR_OPE:' ||
                      TO_CHAR(v_FEC_ISR_OPE, 'dd-mm-yyyy hh24:mi:ss');
          PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
          p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
          RAISE;

        WHEN OTHERS THEN
          err_code    := SQLCODE;
          err_msg     := SUBSTR(SQLERRM, 1, 300);
          p_s_mensaje := err_code || '-' || err_msg;
          PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
          p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
          RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);
      END;

    END IF;

    --Si pertenece a una nomina se debe actualizar su informacion)
    IF (v_NUM_FOL_NMN IS NOT NULL) THEN

      PKG_PAB_NOMINA.Sp_PAB_ACT_DATOS_OPE_NOM(v_NUM_FOL_OPE,
                                              v_FEC_ISR_OPE,
                                              v_NUM_REF_SWF,
                                              v_FEC_ENv_RCp_SWF,
                                              v_HOR_ENv_RCp_SWF,
                                              v_GLS_ADC_EST,
                                              v_GLS_EST_RCH,
                                              v_GLS_MTv_VSD,
                                              p_ERROR);

    END IF;

    p_ERROR := PKG_PAB_CONSTANTES.v_OK;
    COMMIT;

  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      err_code := SQLCODE;
      err_msg  := SUBSTR(SQLERRM, 1, 300);
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                         v_NOM_PCK,
                                         err_msg,
                                         v_NOM_SP);
      p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;

    WHEN OTHERS THEN
      err_code    := SQLCODE;
      err_msg     := SUBSTR(SQLERRM, 1, 300);
      p_s_mensaje := err_code || '-' || err_msg;
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                         v_NOM_PCK,
                                         err_msg,
                                         v_NOM_SP);
      p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);
  END Sp_PAB_ACT_DATOS_OPE;

  /************************************************************************************************
  -- Funcion/Procedimiento: Sp_PAB_TRAS_NOM_OPE
  -- Objetivo: Procedimiento Transfiere la informacion de las nominas a las operaciones
  -- Sistema: DBO_PAB.
  -- Base de Datos: DBO_PAB.
  -- Tablas Usadas: PABS_DT_BANCO_ALMOT
  -- Fecha: 02/06/16
  -- Autor: Santander
  -- Input: p_NUM_FOL_OPE: Numero de Folio de una Operacion
  --        p_FEC_ISR_OPE: Fecha de una Operacion
  --        p_COD_USR: Codigo de Usuario.
  --        p_COD_BCO_DTN: Bic Banco Destino
  --        p_COD_MT_SWF : Codigo MT Swift
  --        p_COD_TPO_OPE_AOS: Codigo Tipo operación AOS
  --        p_COD_DVI: Codigo DVI
  -- Output:
  --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
  -- Input/Output: N/A
  -- Retorno: N/A.
  -- Observaciones: <Fecha y Detalle de ultimos cambios>
  --***********************************************************************************************/
  PROCEDURE Sp_PAB_TRAS_NOM_OPE(p_NUM_FOL_OPE     IN NUMBER,
                                p_FEC_ISR_OPE     IN DATE,
                                p_COD_USR         IN VARCHAR2,
                                p_COD_BCO_DTN     IN CHAR,
                                p_COD_MT_SWF      IN NUMBER,
                                p_COD_TPO_OPE_AOS IN CHAR,
                                p_COD_DVI         IN CHAR,
                                p_ERROR           OUT NUMBER) IS

    v_NOM_SP              VARCHAR2(30) := 'Sp_PAB_TRAS_NOM_OPE';
    v_COD_SIS_SAL_FIN     CHAR(10);
    v_COD_SIS_SAL_BCO     CHAR(10);
    v_COD_SIS_SAL_TPO_OPE CHAR(10);
    v_COD_EST_OPE         NUMBER;
    v_RESULT              NUMBER;
    v_COD_SIS_ENT         CHAR(10);
    v_CANTIDAD            NUMBER;
    v_NUM                 NUMBER := 1;
    v_COD_USR             CHAR(11);
    v_FEC_ISR_OPE         TIMESTAMP(6) := SYSTIMESTAMP;

    -- *** Variables para seccion de validacion de horario sistema y monto importe
    v_IMP_INF     NUMBER;
    v_RESP_VAL    NUMBER;
    v_DES_BIT     VARCHAR2(300);
    V_GLS_ADC_EST VARCHAR2(210);

    v_NUM_DOC_BFC       CHAR(11);
    v_COD_TPD_BFC       CHAR(1);
    v_NUM_DOC_ODN       CHAR(11);
    v_COD_TPD_ODN       CHAR(1);
    v_NUM_CTA_ODN       VARCHAR2(30 BYTE);
    v_NUM_CTA_BFC       VARCHAR2(30 BYTE);
    V_RUT_BANCO_DESTINO CHAR(9);

    -- ***

  BEGIN

    v_COD_USR := p_COD_USR;
    p_ERROR   := PKG_PAB_CONSTANTES.v_OK;

    -- Se crea procedimiento que busca Canal de Salida
    Sp_PAB_BUS_CANAL_SALIDA(p_COD_BCO_DTN,
                            p_COD_MT_SWF,
                            NULL, --p_MONTO_OPE, Campo se debe modificar
                            p_COD_TPO_OPE_AOS,
                            p_COD_DVI,
                            v_COD_SIS_SAL_FIN,
                            p_ERROR);

    --Consultamos por la operacion
    BEGIN
      OPEN PKG_PAB_NOMINA.C_CONSULTA_OPE_NOM(p_NUM_FOL_OPE, p_FEC_ISR_OPE);
    EXCEPTION
      WHEN OTHERS THEN
        err_code    := SQLCODE;
        err_msg     := SUBSTR(SQLERRM, 1, 300);
        p_s_mensaje := err_code || '-' || err_msg;
        p_ERROR     := PKG_PAB_CONSTANTES.v_ERROR;
        PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                           v_NOM_PCK,
                                           err_msg,
                                           v_NOM_SP);
        RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);
    END;

    BEGIN
      FETCH PKG_PAB_NOMINA.C_CONSULTA_OPE_NOM
        INTO PKG_PAB_NOMINA.R_CONSULTA_OPE_NOM;
    EXCEPTION
      WHEN OTHERS THEN
        err_code    := SQLCODE;
        err_msg     := SUBSTR(SQLERRM, 1, 300);
        p_s_mensaje := err_code || '-' || err_msg;
        p_ERROR     := PKG_PAB_CONSTANTES.v_ERROR;
        PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                           v_NOM_PCK,
                                           err_msg,
                                           v_NOM_SP);

        --Verificamos si el cursor se encuentra abierto en caso de error.
        IF (PKG_PAB_NOMINA.C_CONSULTA_OPE_NOM %ISOPEN) THEN
          CLOSE PKG_PAB_NOMINA.C_CONSULTA_OPE_NOM;
        END IF;

        RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);
    END;

    WHILE PKG_PAB_NOMINA.C_CONSULTA_OPE_NOM %FOUND LOOP

      -- ***************************** INICIO ********************************************************** --
      -- Seccion que llama a Sp_PAB_GES_VAL_HORA_MONDA para saber si cumple validaciones de hora y monto
      -- 0 cumple y 1 no cumple

      PKG_PAB_NOMINA.Sp_PAB_GES_VAL_HORA_MONDA(p_COD_USR,
                                               PKG_PAB_NOMINA.R_CONSULTA_OPE_NOM.COD_DVI,
                                               PKG_PAB_NOMINA.R_CONSULTA_OPE_NOM.IMp_OPE,
                                               v_DES_BIT,
                                               v_RESP_VAL,
                                               p_ERROR);

      IF v_RESP_VAL = PKG_PAB_CONSTANTES.v_ERROR THEN

        v_COD_EST_OPE                 := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPVI;
        PKG_PAB_NOMINA.v_cont_est_vis := PKG_PAB_NOMINA.v_cont_est_vis + 1;
        PKG_PAB_NOMINA.v_est_ope_vis  := 1; --Marcamos la operación indicando que se visara

        V_GLS_ADC_EST := v_DES_BIT;
        --p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;

        --Actualizamos el estado de la operacion en la tabla Nomina.
        PKG_PAB_NOMINA.Sp_PAB_ACT_EST_OPE_NOMINA(PKG_PAB_NOMINA.R_CONSULTA_OPE_NOM.NUM_FOL_OPE,
                                                 PKG_PAB_NOMINA.R_CONSULTA_OPE_NOM.FEC_ISR_OPE,
                                                 v_COD_EST_OPE,
                                                 v_DES_BIT,
                                                 v_RESULT);

        --Se llama al procedimiento de bitacora
        PKG_PAB_TRACKING.Sp_PAB_INS_BIT_OPE(PKG_PAB_NOMINA.R_CONSULTA_OPE_NOM.NUM_FOL_OPE,
                                            PKG_PAB_NOMINA.R_CONSULTA_OPE_NOM.FEC_ISR_OPE,
                                            p_COD_USR,
                                            PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPECUR,
                                            v_COD_EST_OPE,
                                            v_DES_BIT,
                                            v_RESULT);

      ELSE
        v_COD_EST_OPE := PKG_PAB_NOMINA.R_CONSULTA_OPE_NOM.COD_EST_AOS;
        V_GLS_ADC_EST := PKG_PAB_NOMINA.R_CONSULTA_OPE_NOM.GLS_ADC_EST;

      END IF;

      -- *****************************   FIN   ********************************************************** --

      v_NUM_DOC_ODN := PKG_PAB_NOMINA.R_CONSULTA_OPE_NOM.NUM_DOC_ODN;
      v_COD_TPD_ODN := PKG_PAB_NOMINA.R_CONSULTA_OPE_NOM.COD_TPD_ODN;
      v_NUM_CTA_ODN := PKG_PAB_NOMINA.R_CONSULTA_OPE_NOM.NUM_CTA_ODN;
      v_NUM_CTA_BFC := PKG_PAB_NOMINA.R_CONSULTA_OPE_NOM.NUM_CTA_BFC;
      v_NUM_DOC_BFC := PKG_PAB_NOMINA.R_CONSULTA_OPE_NOM.NUM_DOC_BFC;
      v_COD_TPD_BFC := PKG_PAB_NOMINA.R_CONSULTA_OPE_NOM.COD_TPD_BFC;

      --Validamos la informacion de un mt202 CLP en caso de no venir
      IF ((p_COD_MT_SWF = PKG_PAB_CONSTANTES.V_COD_MT202 OR
         p_COD_MT_SWF = PKG_PAB_CONSTANTES.V_COD_MT205) AND
         p_COD_DVI = PKG_PAB_CONSTANTES.V_STR_DVI_CLP) THEN

        --Verificamos si viene informacion en los campos

        --Ordenante
        IF (NVL(TO_CHAR(v_NUM_DOC_ODN), '#') = '#') THEN
          v_NUM_DOC_ODN := PKG_PAB_CONSTANTES.V_NUM_DOC_BANCO_SANTANDER;
        END IF;

        IF (NVL(TO_CHAR(v_COD_TPD_ODN), '#') = '#') THEN
          v_COD_TPD_ODN := PKG_PAB_CONSTANTES.V_COD_TPD_BANCO_SANTANDER;
        END IF;

        IF (NVL(TO_CHAR(v_NUM_CTA_ODN), '#') = '#') THEN
          v_NUM_CTA_ODN := PKG_PAB_CONSTANTES.V_CTA_BANCO_SANTANDER;
        END IF;

        --Beneficiario
        IF (NVL(TO_CHAR(v_NUM_CTA_BFC), '#') = '#') THEN
          v_NUM_CTA_BFC := 0;
        END IF;

        BEGIN

          --Obtenemos rut del banco destino
          SELECT TCNIFENT
            INTO V_RUT_BANCO_DESTINO
            FROM tcdt040
           WHERE TGCDSWSA = p_COD_BCO_DTN
             AND TCNIFENT(+) <> v_BANCO;

          --SEPARAMOS RUT
          PKG_PAB_UTILITY.Sp_PAB_DESCOMp_RUT(V_RUT_BANCO_DESTINO,
                                             v_NUM_DOC_BFC,
                                             v_COD_TPD_BFC);

        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            err_code := SQLCODE;
            err_msg  := 'No se pudo obtener el rut del banco:' ||
                        p_COD_BCO_DTN;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                               v_NOM_PCK,
                                               err_msg,
                                               v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
          WHEN OTHERS THEN
            err_code := SQLCODE;
            err_msg  := SUBSTR(SQLERRM, 1, 300);
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                               v_NOM_PCK,
                                               err_msg,
                                               v_NOM_SP);
            p_ERROR     := PKG_PAB_CONSTANTES.v_ERROR;
            p_s_mensaje := err_code || '-' || err_msg;
            --Verificamos si el cursor se encuentra abierto en caso de error.
            IF (PKG_PAB_NOMINA.C_CONSULTA_OPE_NOM %ISOPEN) THEN
              CLOSE PKG_PAB_NOMINA.C_CONSULTA_OPE_NOM;
            END IF;
            RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);
        END;

      END IF;

      --Insertamos la Operacion
      PKG_PAB_OPERACION.Sp_PAB_INS_OPE(PKG_PAB_NOMINA.R_CONSULTA_OPE_NOM.FEC_ISR_OPE,
                                       PKG_PAB_NOMINA.R_CONSULTA_OPE_NOM.NUM_FOL_OPE,
                                       PKG_PAB_NOMINA.R_CONSULTA_OPE_NOM.NUM_FOL_NMN,
                                       NULL,
                                       PKG_PAB_NOMINA.R_CONSULTA_OPE_NOM.NUM_OPE_SIS_ENT,
                                       PKG_PAB_NOMINA.R_CONSULTA_OPE_NOM.FLG_EGR_ING,
                                       v_COD_SIS_SAL_FIN,
                                       PKG_PAB_NOMINA.R_CONSULTA_OPE_NOM.COD_SIS_ENT,
                                       v_COD_EST_OPE,
                                       PKG_PAB_NOMINA.R_CONSULTA_OPE_NOM.COD_TPO_ING,
                                       PKG_PAB_NOMINA.R_CONSULTA_OPE_NOM.COD_MT_SWF,
                                       PKG_PAB_NOMINA.R_CONSULTA_OPE_NOM.FLG_MRD_UTZ_SWF,
                                       PKG_PAB_NOMINA.R_CONSULTA_OPE_NOM.COD_TPO_OPE_AOS,
                                       PKG_PAB_NOMINA.R_CONSULTA_OPE_NOM.FEC_VTA,
                                       PKG_PAB_NOMINA.R_CONSULTA_OPE_NOM.COD_DVI,
                                       PKG_PAB_NOMINA.R_CONSULTA_OPE_NOM.IMp_OPE,
                                       PKG_PAB_NOMINA.R_CONSULTA_OPE_NOM.COD_BCO_DTN,
                                       PKG_PAB_CONSTANTES.V_BIC_BANCO_SANTANDER,
                                       NULL,
                                       NULL,
                                       NULL,
                                       NULL,
                                       v_NUM_DOC_BFC, --PKG_PAB_NOMINA.R_CONSULTA_OPE_NOM.NUM_DOC_BFC         ,
                                       v_COD_TPD_BFC, --PKG_PAB_NOMINA.R_CONSULTA_OPE_NOM.COD_TPD_BFC         ,
                                       PKG_PAB_NOMINA.R_CONSULTA_OPE_NOM.NOM_BFC,
                                       v_NUM_CTA_BFC, --PKG_PAB_NOMINA.R_CONSULTA_OPE_NOM.NUM_CTA_BFC         ,
                                       PKG_PAB_NOMINA.R_CONSULTA_OPE_NOM.COD_SUC,
                                       V_GLS_ADC_EST,
                                       PKG_PAB_NOMINA.R_CONSULTA_OPE_NOM.NUM_REF_CTB,
                                       PKG_PAB_NOMINA.R_CONSULTA_OPE_NOM.COD_BCO_BFC,
                                       PKG_PAB_NOMINA.R_CONSULTA_OPE_NOM.COD_BCO_ITD,
                                       PKG_PAB_NOMINA.R_CONSULTA_OPE_NOM.NUM_CTA_DCv_BFC,
                                       PKG_PAB_NOMINA.R_CONSULTA_OPE_NOM.GLS_DIR_BFC,
                                       PKG_PAB_NOMINA.R_CONSULTA_OPE_NOM.NOM_CDD_BFC,
                                       PKG_PAB_NOMINA.R_CONSULTA_OPE_NOM.COD_PAS_BFC,
                                       PKG_PAB_NOMINA.R_CONSULTA_OPE_NOM.NOM_ODN,
                                       v_NUM_DOC_ODN, --PKG_PAB_NOMINA.R_CONSULTA_OPE_NOM.NUM_DOC_ODN         ,
                                       v_COD_TPD_ODN, --PKG_PAB_NOMINA.R_CONSULTA_OPE_NOM.COD_TPD_ODN         ,
                                       V_NUM_CTA_ODN, --PKG_PAB_NOMINA.R_CONSULTA_OPE_NOM.NUM_CTA_ODN         ,
                                       PKG_PAB_NOMINA.R_CONSULTA_OPE_NOM.NUM_CTA_DCv_ODN,
                                       PKG_PAB_NOMINA.R_CONSULTA_OPE_NOM.GLS_DIR_ODN,
                                       PKG_PAB_NOMINA.R_CONSULTA_OPE_NOM.NOM_CDD_ODN,
                                       PKG_PAB_NOMINA.R_CONSULTA_OPE_NOM.COD_PAS_ODN,
                                       PKG_PAB_NOMINA.R_CONSULTA_OPE_NOM.NUM_CLv_NGC,
                                       PKG_PAB_NOMINA.R_CONSULTA_OPE_NOM.NUM_AGT,
                                       PKG_PAB_NOMINA.R_CONSULTA_OPE_NOM.COD_FND_CCLV,
                                       PKG_PAB_NOMINA.R_CONSULTA_OPE_NOM.COD_TPO_SDO,
                                       PKG_PAB_NOMINA.R_CONSULTA_OPE_NOM.COD_TPO_CMA,
                                       PKG_PAB_NOMINA.R_CONSULTA_OPE_NOM.COD_TPO_FND,
                                       PKG_PAB_NOMINA.R_CONSULTA_OPE_NOM.FEC_TPO_OPE_CCLV,
                                       PKG_PAB_NOMINA.R_CONSULTA_OPE_NOM.NUM_CLv_IDF,
                                       PKG_PAB_NOMINA.R_CONSULTA_OPE_NOM.OBS_OPC_SWF,
                                       NULL,
                                       NULL,
                                       PKG_PAB_NOMINA.R_CONSULTA_OPE_NOM.COD_USR,
                                       v_RESULT);

      FETCH PKG_PAB_NOMINA.C_CONSULTA_OPE_NOM
        INTO PKG_PAB_NOMINA.R_CONSULTA_OPE_NOM;

    END LOOP;

    CLOSE PKG_PAB_NOMINA.C_CONSULTA_OPE_NOM;

  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      ERR_CODE := SQLCODE;
      ERR_MSG  := SUBSTR(SQLERRM, 1, 300);
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(ERR_CODE,
                                         v_NOM_PCK,
                                         ERR_MSG,
                                         v_NOM_SP);
      p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;

      --Verificamos si el cursor se encuentra abierto en caso de error.
      IF (PKG_PAB_NOMINA.C_CONSULTA_OPE_NOM %ISOPEN) THEN
        CLOSE PKG_PAB_NOMINA.C_CONSULTA_OPE_NOM;
      END IF;

    WHEN OTHERS THEN

      --Verificamos si el cursor se encuentra abierto en caso de error.
      IF (PKG_PAB_NOMINA.C_CONSULTA_OPE_NOM%ISOPEN) THEN
        CLOSE PKG_PAB_NOMINA.C_CONSULTA_OPE_NOM;
      END IF;

      ERR_CODE    := SQLCODE;
      ERR_MSG     := SUBSTR(SQLERRM, 1, 300);
      p_s_mensaje := err_code || '-' || err_msg;
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(ERR_CODE,
                                         v_NOM_PCK,
                                         ERR_MSG,
                                         v_NOM_SP);
      p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);

  END Sp_PAB_TRAS_NOM_OPE;

  /************************************************************************************************
  -- Funcion/Procedimiento: Sp_PAB_INS_OPE
  -- Objetivo: Procedimiento que inserta la informacion que se encuentra en la nomina
  -- Sistema: DBO_PAB.
  -- Base de Datos: DBO_PAB.
  -- Tablas Usadas: PABS_DT_DETLL_OPRCN
  -- Fecha: 02/06/16
  -- Autor: Santander
  -- Input:
  --            p_FEC_ISR_OPE     -> Fecha de insercion de la operación
  --            p_NUM_FOL_OPE     -> Numero Foliador de la operacion
  --            p_NUM_FOL_NMN     -> Numero Foliador de la nomina
  --            p_NUM_FOL_OPE_ORG -> Numero que tiene el numero original de la operacion relacionada
  --            p_NUM_OPE_SIS_ENT -> Numero de operacion de la operacion en el sistema origen
  --            p_FLG_EGR_ING     -> Flag de egreso e ingreso
  --            p_COD_SIS_SAL     -> Codigo de sistema de salida
  --            p_COD_SIS_ENT     -> Codigo de sistema de entrada
  --            p_COD_EST_OPE     -> Estado de la operación
  --            p_COD_TPO_ING     -> Tipo de ingreso
  --            p_COD_MT_SWF      -> Numero del tipo de mensaje swift
  --            p_FLG_MRD_UTZ_SWF -> Flasg de mercado
  --            p_COD_TPO_OPE_AOS -> Tipo de operacion.
  --            p_FEC_VTA         -> Fecha de vencimiento
  --            p_COD_DVI         -> Codigo de la divisa
  --            p_IMp_OPE         -> Importe de la operacion
  --            p_COD_BCO_DTN     -> BIC del banco destino
  --            p_COD_BCO_ORG     -> BIC Banco Origen
  --            p_FEC_ENV_RCP_SWF -> Fecha de envio / recepcion swfit
  --            p_HOR_ENV_RCP_SWF -> Hora de envio / recepcion swfit
  --            p_NUM_REF_SWF     -> Numero de referencia Swift
  --            p_NUM_REF_EXT     -> Numero de referencia swift externa
  --            p_NUM_DOC_BFC     -> Numero rut del beneficiario
  --            p_COD_TPD_BFC     -> Rut tipo de beneficiario
  --            p_NOM_BFC         -> Nombre del Beneficiario
  --            p_NUM_CTA_BFC     -> Numero de cuenta del beneficiario
  --            p_COD_SUC         -> Codigo de la sucursal
  --            p_GLS_ADC_EST     -> Informacion adicional
  --            p_NUM_REF_CTB     -> Numero de referencia contable
  --            p_COD_BCO_BFC     -> BIB Banco Beneficario
  --            p_COD_BCO_ITD     -> BIC Banco Intermediario
  --            p_NUM_CTA_DCv_BFC -> Numero cuenta DCV Beneficiario
  --            p_GLS_DIR_BFC     -> Direccion Beneficario
  --            p_NOM_CDD_BFC     -> Nombre ciudad beneficiario
  --            p_COD_PAS_BFC     -> Codigo pais beneficiario
  --            p_NOM_ODN         -> Nombre ordenante
  --            p_NUM_DOC_ODN     -> Numero rut ordenante
  --            p_COD_TPD_ODN     -> Tipo documento de ordenante
  --            p_NUM_CTA_ODN     -> Numero cuenta ordenante
  --            p_NUM_CTA_DCv_ODN -> Numero cuenta DCV ordenante
  --            p_GLS_DIR_ODN     -> Direccion ordenante
  --            p_NOM_CDD_ODN     -> Nombre ciudad ordenante
  --            p_COD_PAS_ODN     -> Codigo pais ordenante
  --            p_NUM_CLv_NGC     -> Numero de clave de negocio
  --            p_NUM_AGT         -> Numero de agente
  --            p_COD_FND_CCLV    -> Codigo Fondo (Producto CCLV)
  --            p_COD_TPO_SDO     -> Tipo de saldo
  --            p_COD_TPO_CMA     -> Tipo de camara
  --            p_COD_TPO_FND     -> Tipo de fondo
  --            p_FEC_TPO_OPE_CCLV-> Fecha operacion CCLV
  --            p_NUM_CLv_IDF     -> Numero clave identificatorio
  --            p_OBS_OPC_SWF   -> Observacion Opcional Swift
  --            p_NUM_IDF_GTR_DOC -> Identificador gestor documental
  --            p_NUM_MVT         -> Numero de Movimiento (Vigente, Cuenta Corriente)
  --            p_COD_USR     -> rut usuario
  -- Output:
  --          p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
  -- Input/Output: N/A
  -- Retorno: N/A.
  -- Observaciones: <Fecha y Detalle de ultimos cambios>
  --***********************************************************************************************/
  PROCEDURE Sp_PAB_INS_OPE(p_FEC_ISR_OPE     IN DATE,
                           p_NUM_FOL_OPE     IN NUMBER,
                           p_NUM_FOL_NMN     IN NUMBER,
                           p_NUM_FOL_OPE_ORG IN NUMBER,
                           p_NUM_OPE_SIS_ENT IN CHAR,
                           p_FLG_EGR_ING     IN NUMBER,
                           p_COD_SIS_SAL     IN CHAR,
                           p_COD_SIS_ENT     IN CHAR,
                           p_COD_EST_OPE     IN NUMBER,
                           p_COD_TPO_ING     IN NUMBER,
                           p_COD_MT_SWF      IN NUMBER,
                           p_FLG_MRD_UTZ_SWF IN NUMBER,
                           p_COD_TPO_OPE_AOS IN CHAR,
                           p_FEC_VTA         IN DATE,
                           p_COD_DVI         IN CHAR,
                           p_IMp_OPE         IN NUMBER,
                           p_COD_BCO_DTN     IN CHAR,
                           p_COD_BCO_ORG     IN CHAR,
                           p_FEC_ENV_RCP_SWF IN DATE,
                           p_HOR_ENV_RCP_SWF IN NUMBER,
                           p_NUM_REF_SWF     IN CHAR,
                           p_NUM_REF_EXT     IN CHAR,
                           ----------Beneficiario------------
                           p_NUM_DOC_BFC IN CHAR,
                           p_COD_TPD_BFC IN CHAR,
                           p_NOM_BFC     IN VARCHAR2,
                           p_NUM_CTA_BFC IN VARCHAR2,
                           -------------------------------------
                           p_COD_SUC     IN VARCHAR2,
                           p_GLS_ADC_EST IN VARCHAR2,
                           p_NUM_REF_CTB IN CHAR,
                           ----------Opcionales--------------
                           p_COD_BCO_BFC      IN CHAR,
                           p_COD_BCO_ITD      IN CHAR,
                           p_NUM_CTA_DCv_BFC  IN VARCHAR2,
                           p_GLS_DIR_BFC      IN VARCHAR2,
                           p_NOM_CDD_BFC      IN VARCHAR2,
                           p_COD_PAS_BFC      IN CHAR,
                           p_NOM_ODN          IN VARCHAR2,
                           p_NUM_DOC_ODN      IN CHAR,
                           p_COD_TPD_ODN      IN CHAR,
                           p_NUM_CTA_ODN      IN VARCHAR2,
                           p_NUM_CTA_DCv_ODN  IN VARCHAR2,
                           p_GLS_DIR_ODN      IN VARCHAR2,
                           p_NOM_CDD_ODN      IN VARCHAR2,
                           p_COD_PAS_ODN      IN CHAR,
                           p_NUM_CLv_NGC      IN VARCHAR2,
                           p_NUM_AGT          IN CHAR,
                           p_COD_FND_CCLV     IN CHAR,
                           p_COD_TPO_SDO      IN CHAR,
                           p_COD_TPO_CMA      IN CHAR,
                           p_COD_TPO_FND      IN VARCHAR2,
                           p_FEC_TPO_OPE_CCLV IN DATE,
                           p_NUM_CLv_IDF      IN VARCHAR2,
                           p_OBS_OPC_SWF      IN VARCHAR2,
                           p_NUM_IDF_GTR_DOC  IN VARCHAR2,
                           p_NUM_MVT          IN CHAR,
                           --------------------------------
                           p_COD_USR IN CHAR,
                           p_ERROR   OUT NUMBER) IS
    --Seteo de Variables
    v_NOM_SP      VARCHAR2(30) := 'Sp_PAB_INS_OPE';
    v_FEC_ISR_OPE DATE;
    v_cod_bco_org char(11);
    v_FEC_AZA_SWF DATE; --(Fecha Autorizo operación)
    v_HOR_AZA_SWF NUMBER; --(Hora Autorizo operación)
    v_COD_EST_OPE NUMBER;

  BEGIN

    v_COD_EST_OPE := p_COD_EST_OPE ;

    --Verificamos si viene la fecha de insercion
    IF (NVL(TO_CHAR(p_FEC_ISR_OPE), '#') = '#') THEN
      v_FEC_ISR_OPE := SYSDATE;
    ELSE
      v_FEC_ISR_OPE := p_FEC_ISR_OPE;
    END IF;

    --Si es un egreso
    IF (p_FLG_EGR_ING = PKG_PAB_CONSTANTES.V_FLG_EGR) THEN
      v_FEC_AZA_SWF := TO_DATE(TO_CHAR(SYSDATE), 'DD-MM-YYY');
      v_HOR_AZA_SWF := TO_CHAR(SYSDATE, 'HH24MI');
    END IF;

    -- VALIDAMOS EL BANCO DESTINO SI TIENE PROBLEMAS.

    IF (NVL(p_COD_BCO_DTN,'#') = '#') THEN

        v_COD_EST_OPE := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPROBLM;


    END IF;

    --INSERTAMOS DATOS DE LA OPERACION
    INSERT INTO PABS_DT_DETLL_OPRCN
      (FEC_ISR_OPE,
       NUM_FOL_OPE,
       NUM_FOL_NMN,
       NUM_OPE_SIS_ENT,
       FLG_EGR_ING,
       COD_SIS_SAL,
       COD_SIS_ENT,
       COD_EST_AOS,
       COD_TPO_ING,
       COD_MT_SWF,
       FLG_MRD_UTZ_SWF,
       COD_TPO_OPE_AOS,
       FEC_VTA,
       COD_DVI,
       IMp_OPE,
       COD_BCO_DTN,
       NUM_DOC_BFC,
       COD_TPD_BFC,
       NUM_CTA_BFC,
       NOM_BFC,
       COD_SUC,
       GLS_ADC_EST,
       NUM_REF_CTB,
       NUM_FOL_OPE_ORG,
       COD_BCO_ORG,
       FEC_ENV_RCP_SWF,
       HOR_ENV_RCP_SWF,
       NUM_REF_SWF,
       NUM_REF_EXT,
       NUM_IDF_GTR_DOC,
       NUM_MVT_CTB,
       FEC_AZA_SWF,
       HOR_AZA_SWF,
       FEC_ACT_OPE -- Fecha actualización para dashborad
       )
    VALUES
      (v_FEC_ISR_OPE,
       p_NUM_FOL_OPE,
       NVL(p_NUM_FOL_NMN, 0),
       NVL(p_NUM_OPE_SIS_ENT, 0),
       p_FLG_EGR_ING,
       p_COD_SIS_SAL,
       p_COD_SIS_ENT,
       v_COD_EST_OPE,
       p_COD_TPO_ING,
       p_COD_MT_SWF,
       p_FLG_MRD_UTZ_SWF,
       p_COD_TPO_OPE_AOS,
       p_FEC_VTA,
       p_COD_DVI,
       p_IMp_OPE,
       p_COD_BCO_DTN,
       p_NUM_DOC_BFC,
       p_COD_TPD_BFC,
       p_NUM_CTA_BFC,
       NVL(p_NOM_BFC, ' '),
       p_COD_SUC,
       p_GLS_ADC_EST,
       NVL(p_NUM_REF_CTB, ' '),
       NVL(p_NUM_FOL_OPE_ORG, 0),
       p_COD_BCO_ORG,
       p_FEC_ENV_RCP_SWF,
       p_HOR_ENV_RCP_SWF,
       p_NUM_REF_SWF,
       p_NUM_REF_EXT,
       p_NUM_IDF_GTR_DOC,
       p_NUM_MVT,
       v_FEC_AZA_SWF,
       v_HOR_AZA_SWF,
       SYSDATE -- Fecha actualización para dashborad
       );

    --Verificamos si vienen a los menos un datos opcional
    IF ((NVL(p_COD_BCO_BFC, '#') <> '#') OR
       (NVL(p_COD_BCO_ITD, '#') <> '#') OR
       (NVL(p_NUM_CTA_DCv_BFC, '#') <> '#') OR
       (NVL(p_GLS_DIR_BFC, '#') <> '#') OR
       (NVL(p_NOM_CDD_BFC, '#') <> '#') OR
       (NVL(p_COD_PAS_BFC, '#') <> '#') OR (NVL(p_NOM_ODN, '#') <> '#') OR
       (NVL(p_NUM_DOC_ODN, '#') <> '#') OR
       (NVL(p_NUM_CTA_ODN, '#') <> '#') OR
       (NVL(p_NUM_CTA_DCv_ODN, '#') <> '#') OR
       (NVL(p_GLS_DIR_ODN, '#') <> '#') OR
       (NVL(p_NOM_CDD_ODN, '#') <> '#') OR
       (NVL(p_COD_PAS_ODN, '#') <> '#') OR
       (NVL(p_NUM_CLv_NGC, '#') <> '#') OR (NVL(p_NUM_AGT, '#') <> '#') OR
       (NVL(p_COD_FND_CCLV, '#') <> '#') OR
       (NVL(p_COD_TPO_SDO, '#') <> '#') OR
       (NVL(p_COD_TPO_CMA, '#') <> '#') OR
       (NVL(p_COD_TPO_FND, '#') <> '#') OR
       (NVL(p_NUM_CLv_IDF, '#') <> '#') OR
       (NVL(p_OBS_OPC_SWF, '#') <> '#')
       -- OR (NVL (p_NUM_IDF_GTR_DOC , '#') <> '#')
       ) THEN

      Sp_PAB_INS_OPE_OPC(v_FEC_ISR_OPE,
                         p_NUM_FOL_OPE,
                         p_NUM_DOC_BFC,
                         p_COD_TPD_BFC,
                         p_COD_BCO_BFC,
                         p_COD_BCO_ITD,
                         p_NUM_CTA_DCv_BFC,
                         p_GLS_DIR_BFC,
                         p_NOM_CDD_BFC,
                         p_COD_PAS_BFC,
                         p_NOM_ODN,
                         p_NUM_DOC_ODN,
                         p_COD_TPD_ODN,
                         p_NUM_CTA_ODN,
                         p_NUM_CTA_DCv_ODN,
                         p_GLS_DIR_ODN,
                         p_NOM_CDD_ODN,
                         p_COD_PAS_ODN,
                         p_NUM_CLv_NGC,
                         p_NUM_AGT,
                         p_COD_FND_CCLV,
                         p_COD_TPO_SDO,
                         p_COD_TPO_CMA,
                         p_COD_TPO_FND,
                         p_FEC_TPO_OPE_CCLV,
                         p_NUM_CLv_IDF,
                         p_OBS_OPC_SWF,
                         --p_NUM_IDF_GTR_DOC    ,
                         p_COD_USR,
                         p_ERROR);
    END IF;

    p_ERROR := PKG_PAB_CONSTANTES.v_OK;
    COMMIT;

  EXCEPTION
    WHEN DUP_VAL_ON_INDEX THEN
      err_code := SQLCODE;
      err_msg  := 'Ya existe operación' || p_NUM_FOL_OPE || ' ' ||
                  TO_CHAR(v_FEC_ISR_OPE, 'dd-mm-yyyy hh24:mi:ss');
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                         v_NOM_PCK,
                                         err_msg,
                                         v_NOM_SP);
      p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
    WHEN OTHERS THEN
      err_code    := SQLCODE;
      err_msg     := SUBSTR(SQLERRM, 1, 300);
      p_s_mensaje := err_code || '-' || err_msg;
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                         v_NOM_PCK,
                                         err_msg,
                                         v_NOM_SP);

      p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);
  END Sp_PAB_INS_OPE;

  /************************************************************************************************
  -- Funcion/Procedimiento: Sp_PAB_INS_OPE_OPC
  -- Objetivo: Procedimiento que inserta la informacion que se encuentra en la nomina
  -- Sistema: DBO_PAB.
  -- Base de Datos: DBO_PAB.
  -- Tablas Usadas: PABS_DT_OPRCN_INFCN_OPCON
  -- Fecha: 02/06/16
  -- Autor: Santander
  -- Input:     p_FEC_ISR_OPE     -> Fecha Insercion Operacion
  --            p_NUM_FOL_OPE     -> Numero Foliador de la operacion
  --            p_NUM_DOC_BFC     -> Numero rut del beneficiario
  --            p_COD_TPD_BFC     -> Codigo tipo documento beneficiario.
  --            p_COD_BCO_BFC     -> BIB Banco Beneficario
  --            p_COD_BCO_ITD     -> BIC Banco Intermediario
  --            p_NUM_CTA_DCv_BFC -> Numero cuenta DCV Beneficiario
  --            p_GLS_DIR_BFC     -> Direccion Beneficario
  --            p_NOM_CDD_BFC     -> Nombre ciudad beneficiario
  --            p_COD_PAS_BFC     -> Codigo pais beneficiario
  --            p_NOM_ODN         -> Nombre ordenante
  --            p_NUM_DOC_ODN     -> Numero rut ordenante
  --            p_NUM_CTA_ODN     -> Numero cuenta ordenante
  --            p_NUM_CTA_DCv_ODN -> Numero cuenta DCV ordenante
  --            p_GLS_DIR_ODN    -> Direccion ordenante
  --            p_NOM_CDD_ODN    -> Nombre ciudad ordenante
  --            p_COD_PAS_ODN    -> Codigo pais ordenante
  --            p_NUM_CLv_NGC    -> Numero de clave de negocio
  --            p_NUM_AGT        -> Numero de agente
  --            p_COD_FND_CCLV   -> Codigo Fondo (Producto CCLV)
  --            p_COD_TPO_SDO    -> Tipo de saldo
  --            p_COD_TPO_CMA    -> Tipo de camara
  --            p_COD_TPO_FND     -> Tipo de fondo
  --            p_FEC_TPO_OPE_CCLV -> Fecha operacion CCLV
  --            p_NUM_CLv_IDF     -> Numero clave identificatorio
  --            p_OBS_OPC_SWF   -> Observacion Opcional Swift
  --            p_COD_USR     -> rut usuario
  -- Output:
  --          p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
  -- Input/Output: N/A
  -- Retorno: N/A.
  -- Observaciones: <Fecha y Detalle de ultimos cambios>
  --***********************************************************************************************/
  PROCEDURE Sp_PAB_INS_OPE_OPC(p_FEC_ISR_OPE IN DATE,
                               p_NUM_FOL_OPE IN NUMBER,
                               ------------Beneficiario------------
                               p_NUM_DOC_BFC IN CHAR,
                               p_COD_TPD_BFC IN CHAR,
                               ------------Opcionales--------------
                               p_COD_BCO_BFC      IN CHAR,
                               p_COD_BCO_ITD      IN CHAR,
                               p_NUM_CTA_DCv_BFC  IN VARCHAR2,
                               p_GLS_DIR_BFC      IN VARCHAR2,
                               p_NOM_CDD_BFC      IN VARCHAR2,
                               p_COD_PAS_BFC      IN CHAR,
                               p_NOM_ODN          IN VARCHAR2,
                               p_NUM_DOC_ODN      IN CHAR,
                               p_COD_TPD_ODN      IN CHAR,
                               p_NUM_CTA_ODN      IN VARCHAR2,
                               p_NUM_CTA_DCv_ODN  IN VARCHAR2,
                               p_GLS_DIR_ODN      IN VARCHAR2,
                               p_NOM_CDD_ODN      IN VARCHAR2,
                               p_COD_PAS_ODN      IN CHAR,
                               p_NUM_CLv_NGC      IN VARCHAR2,
                               p_NUM_AGT          IN CHAR,
                               p_COD_FND_CCLV     IN CHAR,
                               p_COD_TPO_SDO      IN CHAR,
                               p_COD_TPO_CMA      IN CHAR,
                               p_COD_TPO_FND      IN VARCHAR2,
                               p_FEC_TPO_OPE_CCLV IN DATE,
                               p_NUM_CLv_IDF      IN VARCHAR2,
                               p_OBS_OPC_SWF      IN VARCHAR2,
                               --------------------------------
                               p_COD_USR IN CHAR,
                               p_ERROR   OUT NUMBER) IS
    --Seteo de Variables
    v_NOM_SP  VARCHAR2(30) := 'Sp_PAB_INS_OPE_OPC';
    v_COD_USR CHAR(11);

  BEGIN

    v_COD_USR := p_COD_USR;

    INSERT INTO PABS_DT_OPRCN_INFCN_OPCON
      (FEC_ISR_OPE,
       NUM_FOL_OPE,
       OBS_OPC_SWF,
       COD_BCO_BFC,
       COD_BCO_ITD,
       NUM_DOC_BFC,
       COD_TPD_BFC,
       NUM_CTA_DCv_BFC,
       GLS_DIR_BFC,
       NOM_CDD_BFC,
       COD_PAS_BFC,
       NOM_ODN,
       NUM_DOC_ODN,
       COD_TPD_ODN,
       NUM_CTA_ODN,
       NUM_CTA_DCv_ODN,
       GLS_DIR_ODN,
       NOM_CDD_ODN,
       COD_PAS_ODN,
       NUM_CLv_NGC,
       NUM_AGT,
       COD_FND_CCLV,
       COD_TPO_SDO,
       COD_TPO_CMA,
       COD_TPO_FND,
       FEC_TPO_OPE_CCLV,
       NUM_CLv_IDF)
    VALUES
      (p_FEC_ISR_OPE,
       p_NUM_FOL_OPE,
       p_OBS_OPC_SWF,
       p_COD_BCO_BFC,
       p_COD_BCO_ITD,
       p_NUM_DOC_BFC,
       p_COD_TPD_BFC,
       p_NUM_CTA_DCv_BFC,
       p_GLS_DIR_BFC,
       p_NOM_CDD_BFC,
       p_COD_PAS_BFC,
       p_NOM_ODN,
       p_NUM_DOC_ODN,
       p_COD_TPD_ODN,
       p_NUM_CTA_ODN,
       p_NUM_CTA_DCv_ODN,
       p_GLS_DIR_ODN,
       p_NOM_CDD_ODN,
       p_COD_PAS_ODN,
       p_NUM_CLv_NGC,
       p_NUM_AGT,
       p_COD_FND_CCLV,
       p_COD_TPO_SDO,
       p_COD_TPO_CMA,
       p_COD_TPO_FND,
       p_FEC_TPO_OPE_CCLV,
       p_NUM_CLv_IDF);

    p_ERROR := PKG_PAB_CONSTANTES.v_OK;
    COMMIT;

  EXCEPTION
    WHEN DUP_VAL_ON_INDEX THEN
      err_code := SQLCODE;
      err_msg  := SUBSTR(SQLERRM, 1, 300);
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                         v_NOM_PCK,
                                         err_msg,
                                         v_NOM_SP);
      p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
    WHEN OTHERS THEN
      err_code    := SQLCODE;
      err_msg     := SUBSTR(SQLERRM, 1, 300);
      p_s_mensaje := err_code || '-' || err_msg;
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                         v_NOM_PCK,
                                         err_msg,
                                         v_NOM_SP);

      p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);
  END Sp_PAB_INS_OPE_OPC;

  /************************************************************************************************
  -- Funcion/Procedimiento: FN_PAB_GEN_REF_SWF
  -- Objetivo: Fusion que retorne el codifo de referencia Swift
  -- Sistema: DBO_PAB.
  -- Base de Datos: DBO_PAB.
  -- Tablas Usadas: N/A
  -- Fecha: 02/06/16
  -- Autor: Santander
  -- Input:      p_COD_SIS_SAL:      Sistema de salida de pago
  --             p_COD_TPO_OPE_AOS:  Sistema de operaciones en altos montos
  --             p_NUM_FOL_OPE       Numero de Folio Operacion
  --             p_COD_MT_SWF        Codigo de MT Swift
  -- Output:
  --          Retorna el codigo de referencia Swift
  -- Input/Output: N/A
  -- Retorno: N/A.
  -- Observaciones: <Fecha y Detalle de ultimos cambios>
  --***********************************************************************************************/
  FUNCTION FN_PAB_GEN_REF_SWF(p_COD_SIS_SAL     IN CHAR,
                              p_COD_TPO_OPE_AOS IN CHAR,
                              p_NUM_FOL_OPE     IN NUMBER,
                              p_COD_MT_SWF      IN NUMBER) RETURN VARCHAR2 IS
    v_REF_SWF         VARCHAR2(16);
    v_NOM_SP          VARCHAR2(30) := 'FN_PAB_GEN_REF_SWF';
    v_COD_TIPO_OPE_AS VARCHAR2(7);
  BEGIN

    IF (p_COD_MT_SWF = PKG_PAB_CONSTANTES.v_COD_MT205) THEN
      v_COD_TIPO_OPE_AS := 'CMN'; --Compra Moneda Nacional
    ELSE
      v_COD_TIPO_OPE_AS := p_COD_TPO_OPE_AOS;
    END IF;

    --Estados
    CASE
      WHEN p_COD_SIS_SAL = PKG_PAB_CONSTANTES.v_SRT_COD_SIS_SAL_SWF THEN

        --Swift
        v_REF_SWF := p_COD_TPO_OPE_AOS || p_NUM_FOL_OPE;

      WHEN p_COD_SIS_SAL = PKG_PAB_CONSTANTES.v_SRT_COD_SIS_SAL_CMN THEN

        --Combanc
        v_REF_SWF := v_COD_TIPO_OPE_AS || '037' ||
                     SUBSTR(LPAD(SPK_PAB_COD_REF_LBTR.NEXTVAL, 6, '0'),
                            1,
                            6);

      WHEN p_COD_SIS_SAL = PKG_PAB_CONSTANTES.v_SRT_COD_SIS_SAL_LBTR THEN

        --Lbtr
        v_REF_SWF := v_COD_TIPO_OPE_AS || '037' ||
                     SUBSTR(LPAD(SPK_PAB_COD_REF_LBTR.NEXTVAL, 6, '0'),
                            1,
                            6);

      WHEN p_COD_SIS_SAL = PKG_PAB_CONSTANTES.v_SRT_COD_SIS_SAL_CTACTE THEN

        --Cuentas Corrientes
        v_REF_SWF := NULL;

    END CASE;

    RETURN v_REF_SWF;

  EXCEPTION
    WHEN OTHERS THEN
      err_code    := SQLCODE;
      err_msg     := SUBSTR(SQLERRM, 1, 300);
      p_s_mensaje := err_code || '-' || err_msg;
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                         v_NOM_PCK,
                                         err_msg,
                                         v_NOM_SP);
      RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);
  END FN_PAB_GEN_REF_SWF;

  -- ***********************************************************************************************
  -- Funcion/Procedimiento: SP_PAB_BUS_TOT_DET_OPE
  -- Objetivo: Procedimiento almacenado que retorna el detalle de una operacion seleccionada
  -- Base de Datos: DGBMSEGDB01_NGALTM
  -- Tablas Usadas: PABS_DT_DETLL_NOMNA
  -- Fecha: 21/06/16
  -- Autor: Santander
  -- Input:
  -- p_NUM_FOL_OPE->   foliador del banco, PK tabla operaciones nomina.
  -- p_FEC_ISR_OPE->   Fecha de insercion de operacion en el sistema, PK tabla operaciones nomina.
  -- Output:
  -- p_CURSOR -> Informacion de la operacion
  -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
  -- Input/Output: N/A
  -- Retorno: N/A.
  -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
  -- ***********************************************************************************************
  PROCEDURE SP_PAB_BUS_TOT_DET_OPE(p_NUM_FOL_OPE IN NUMBER,
                                   p_FEC_ISR_OPE IN DATE,
                                   p_CURSOR      OUT SYS_REFCURSOR,
                                   p_ERROR       OUT NUMBER) IS
    v_NOM_SP VARCHAR2(30) := 'SP_PAB_BUS_TOT_DET_OPE';

  BEGIN

    OPEN p_CURSOR FOR
      SELECT DETOPE.NUM_FOL_OPE AS NUM_OPE,
             DETOPE.NUM_REF_SWF AS REF_SWIFT,
             DETOPE.COD_TPO_OPE_AOS AS TIPO_OPERACION,
             DETOPE.NUM_REF_EXT AS REF_EXT_SWIFT,
             DETOPE.COD_MT_SWF AS COD_MT,
             DETOPE.COD_DVI AS DIVISA,
             SAL.DSC_SIS_ENT_SAL AS CANAL_SALIDA,
             DETOPE.COD_BCO_DTN AS BIC_DESTINO,
             DETOPE.COD_BCO_ORG AS BIC_ORIGEN,
             DETOPC.GLS_DIR_ODN AS DIRECCION_ORDENANTE,
             NVL2(DETOPC.NUM_DOC_ODN,
                  TRIM(DETOPC.NUM_DOC_ODN) || '-' || DETOPC.COD_TPD_ODN,
                  NULL) AS DOC_ORDENANTE,
             DETOPC.NOM_ODN AS NOMBRE_ORDENANTE,
             DETOPC.NUM_CTA_ODN AS CTA_ORDENANTE,
             DETOPC.NOM_CDD_ODN AS CIUDAD_ORDENANTE,
             PKG_PAB_UTILITY.FN_PAB_COMBO_PAIS(DETOPC.COD_PAS_ODN) AS PAIS_ORDENANTE,
             DETOPC.NUM_CTA_DCv_ODN AS CTA_DCv_ORDENANTE,
             DETOPC.COD_BCO_BFC AS BIC_BENEFICIARIO,
             NVL2(DETOPE.NUM_DOC_BFC,
                  TRIM(DETOPE.NUM_DOC_BFC) || '-' || DETOPE.COD_TPD_BFC,
                  NULL) AS DOC_BENEFICIARIO,
             DETOPE.NOM_BFC AS NOMBRE_BENEFICIARIO,
             DETOPE.NUM_CTA_BFC AS CTA_BENEFICIARIO,
             DETOPC.GLS_DIR_BFC AS DIRECCION_BENEFICIARIO,
             DETOPC.NOM_CDD_BFC AS CIUDAD_BENEFICIARIO,
             PKG_PAB_UTILITY.FN_PAB_COMBO_PAIS(DETOPC.COD_PAS_BFC) AS PAIS_BENEFICIARIO,
             DETOPC.NUM_CTA_DCv_BFC AS CTA_DCv_BENEFICIARIO,
             DETOPC.COD_BCO_ITD AS BIC_INTERMEDIARIO,
             DETOPE.IMp_OPE AS MONTO_OPE,
             DETOPE.COD_SUC AS COD_SUCURSAL,
             DETOPC.FEC_TPO_OPE_CCLV AS FECHA_CCLV,
             PKG_PAB_UTILITY.Fn_PAB_COMBO_CAMARA(DETOPC.COD_TPO_CMA) AS TIPO_CAMARA,
             DETOPC.NUM_CLv_NGC AS CLAVE_NEGOCIO,
             DETOPE.FEC_VTA AS FECHA_VALUTA,
             DETOPE.NUM_REF_CTB AS REF_CONTABLE,
             PKG_PAB_UTILITY.Fn_PAB_COMBO_TSALDO(DETOPC.COD_TPO_SDO) AS TIPO_SALDO,
             DETOPC.NUM_AGT AS NUM_AGENTE,
             DETOPC.COD_FND_CCLV AS FONDO_CCLV,
             DETOPC.COD_TPO_FND AS CODIGO_FONDO,
             PKG_PAB_UTILITY.FN_PAB_TRAN_COD_FONDO(DETOPC.COD_TPO_FND) AS GLOSA_FONDO,
             DETOPC.OBS_OPC_SWF AS OBSERVACION_SWIFT,
             -------------------------------------------------
             ESTALM.DSC_EST_AOS AS ESTADO,
             DETOPE.FEC_ISR_OPE AS FEC_INS,
             DETOPE.NUM_FOL_NMN AS NUM_NOM,
             DETOPE.NUM_OPE_SIS_ENT AS NUM_OPE_ENT,
             DECODE(DETOPE.FLG_EGR_ING,
                    PKG_PAB_CONSTANTES.v_FLG_EGR,
                    PKG_PAB_CONSTANTES.v_STR_FLG_EGR,
                    PKG_PAB_CONSTANTES.v_STR_FLG_ING) AS EGRESO_INGRESO,
             DETOPE.COD_SIS_SAL AS COD_CANAL_ENT,
             ENT.DSC_SIS_ENT_SAL AS CANAL_ENTRADA,
             DETOPE.COD_SIS_SAL AS COD_CANAL_SAL,
             DECODE(DETOPE.COD_TPO_ING,
                    PKG_PAB_CONSTANTES.v_COD_TIp_ING_MAS,
                    PKG_PAB_CONSTANTES.v_STR_TIp_ING_MAS,
                    PKG_PAB_CONSTANTES.V_COD_TIP_ING_UNI,
                    PKG_PAB_CONSTANTES.V_STR_TIP_ING_UNI,
                    PKG_PAB_CONSTANTES.V_COD_TIP_ING_DIR,
                    PKG_PAB_CONSTANTES.V_STR_TIP_ING_DIR) AS TIPO_INGRESO,
             DETOPE.FEC_ENv_RCp_SWF AS FECHA_SWIFT,
             DETOPE.HOR_ENv_RCp_SWF AS HORA_SWIFT,
             DECODE(DETOPE.FLG_MRD_UTZ_SWF,
                    PKG_PAB_CONSTANTES.v_FLG_MRD_MN,
                    PKG_PAB_CONSTANTES.v_STR_FLG_MRD_MN,
                    PKG_PAB_CONSTANTES.v_STR_FLG_MRD_MX) AS MERCADO,
             DECODE(DETOPE.COD_EST_AOS,
                    PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEREC,
                    DETOPC.GLS_EST_RCH,
                    PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPVI,
                    DETOPC.GLS_MTv_VSD,
                    DETOPE.GLS_ADC_EST) AS GLOSA_ESTADO,
             DETOPC.NUM_DOC_BFC AS DOC_BENEFICIARIO_OPC,
             DETOPC.COD_TPD_BFC AS TIPO_DOC_BENEFICIARIO_OPC,
             DETOPC.NUM_CLv_IDF AS NUMERO_IDENTIFICATORIO,
             DETOPC.GLS_EST_RCH AS GLOSA_RECHAZO,
             DETOPC.GLS_MTv_VSD AS GLOSA_VISADA
        FROM PABS_DT_DETLL_OPRCN       DETOPE,
             PABS_DT_OPRCN_INFCN_OPCON DETOPC,
             PABS_DT_ESTDO_ALMOT       ESTALM,
             PABS_DT_SISTM_ENTRD_SALID ENT,
             PABS_DT_SISTM_ENTRD_SALID SAL
       WHERE DETOPE.NUM_FOL_OPE = p_NUM_FOL_OPE
         AND DETOPE.FEC_ISR_OPE = p_FEC_ISR_OPE
         AND DETOPE.NUM_FOL_OPE = DETOPC.NUM_FOL_OPE(+)
         AND DETOPE.FEC_ISR_OPE = DETOPC.FEC_ISR_OPE(+)
         AND DETOPE.COD_EST_AOS = ESTALM.COD_EST_AOS
         AND DETOPE.COD_SIS_ENT = ENT.COD_SIS_ENT_SAL
         AND DETOPE.COD_SIS_SAL = SAL.COD_SIS_ENT_SAL(+);

    p_ERROR := PKG_PAB_CONSTANTES.v_OK; --Procedimiento ejecutado de forma correcta

  EXCEPTION

    WHEN NO_DATA_FOUND THEN

      err_code := SQLCODE;
      err_msg  := SUBSTR(SQLERRM, 1, 300);
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                         v_NOM_PCK,
                                         err_msg,
                                         v_NOM_SP);
      p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;

    WHEN OTHERS THEN

      err_code := SQLCODE;
      err_msg  := SUBSTR(SQLERRM, 1, 300);
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                         v_NOM_PCK,
                                         err_msg,
                                         v_NOM_SP);
      p_ERROR     := PKG_PAB_CONSTANTES.v_ERROR;
      p_s_mensaje := err_code || '-' || err_msg;
      RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);

  END SP_PAB_BUS_TOT_DET_OPE;

  -- ***********************************************************************************************
  -- Funcion/Procedimiento: Sp_PAB_CON_OPE_HIST
  -- Objetivo: Procedimiento almacenado que retorna las operaciones de una nomina
  -- Sistema: PAB
  -- Base de Datos: DGBMSEGDB01_NGALTM
  -- Tablas Usadas: PABS_DT_DETLL_OPRCN, PABS_DT_OPRCN_INFCN_OPCON, PABS_DT_DETLL_OPRCN_HTRCA, PABS_DT_OPRCN_OPCON_HTRCA, PABS_DT_ESTDO_ALMOT
  -- Fecha: 21/06/16
  -- Autor: Santander
  -- Input:
  -- p_NUM_FOL_NMN -> Numero de la nomina
  -- p_NUM_FOL_OPE -> Numero de folio operacion
  -- p_FEC_DESDE ->  fecha desde
  -- p_FEC_HASTA ->  fecha hasta
  -- p_COD_TPO_OPE_AOS ->  codigo tipo operacion estado
  -- p_COD_EST_AOS ->  codigo estado
  -- p_COD_SIS_ENT ->  codigo sistema entrada
  -- p_COD_SIS_SAL ->  codigo de sistema salida
  -- p_COD_BCO_DTN ->  codigo banco destino
  -- p_COD_BCO_ORG -> Codigo Bic banco origen
  -- p_COD_MT_SWF ->  codigo MT swf
  -- p_FLG_EGR_ING -> Flasg egreso ingreso
  -- p_IMP_OPE_DESDE ->  importe operacion desde
  -- p_IMP_OPE_HASTA ->  importe operacion hasta
  -- p_COD_DVI -> codigo de divisa
  -- p_NUM_REF_SWF ->  numero referencia swf
  -- Output:
  -- p_CURSOR -> Cursor con las nominas encontradas
  -- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
  -- Input/Output: N/A
  -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
  -- ***********************************************************************************************
PROCEDURE Sp_PAB_CON_OPE_HIST(p_NUM_FOL_NMN     IN NUMBER,
                                p_NUM_FOL_OPE     IN NUMBER,
                                p_FEC_DESDE       IN DATE,
                                p_FEC_HASTA       IN DATE,
                                p_COD_TPO_OPE_AOS IN CHAR,
                                p_COD_EST_AOS     IN NUMBER,
                                p_COD_SIS_ENT     IN CHAR,
                                p_COD_SIS_SAL     IN CHAR ---
                               ,
                                p_COD_BCO_DTN     IN CHAR,
                                p_COD_BCO_ORG     IN CHAR ---
                               ,
                                p_COD_MT_SWF      IN NUMBER,
                                p_FLG_EGR_ING     IN NUMBER ---
                               ,
                                p_IMP_OPE_DESDE   IN NUMBER,
                                p_IMP_OPE_HASTA   IN NUMBER,
                                p_COD_DVI         IN CHAR,
                                p_NUM_REF_SWF     IN CHAR,
                                p_CURSOR          OUT SYS_REFCURSOR,
                                p_ERROR           OUT NUMBER) IS
    --
    v_NOM_SP          VARCHAR2(30) := 'Sp_PAB_CON_OPE_HIST';
    v_IMp_DSD         NUMBER(15, 2);
    v_IMp_HST         NUMBER(15, 2);
    v_COD_DVI         CHAR(3);
    v_COD_EST_AOS     NUMBER;
    v_COD_MT          NUMBER;
    v_COD_BCO_DTN     CHAR(11);
    v_COD_BCO_ORG     CHAR(11); -- CAH
    v_NUM_FOL_OPE     NUMBER(12);
    v_NUM_FOL_NMN     NUMBER(12);
    v_COD_TPO_OPE_AOS CHAR(8);
    v_NUM_REF_SWF     VARCHAR2(16);
    v_COD_SIS_ENT     CHAR(10);
    v_COD_SIS_SAL     CHAR(10);
    v_FLG_EGR_ING     NUMBER(1); ---CAH
    v_VER_INTERNAS    NUMBER(1);
    v_TGCDSWSA         NUMBER;
    v_BIC_BSCHCL       CHAR(11);
    --
  BEGIN

    --FOLIO DE NOMINA
    IF p_NUM_FOL_NMN = -1 THEN
      v_NUM_FOL_NMN := NULL;
    ELSE
      v_NUM_FOL_NMN := p_NUM_FOL_NMN;
    END IF;

    --FOLIO DE OPERACION
    IF p_NUM_FOL_OPE = -1 THEN
      v_NUM_FOL_OPE := NULL;
    ELSE
      v_NUM_FOL_OPE := p_NUM_FOL_OPE;
    END IF;

    --TIPO DE OPERACION
    IF p_COD_TPO_OPE_AOS = '-1' THEN
      v_COD_TPO_OPE_AOS := NULL;
    ELSE
      v_COD_TPO_OPE_AOS := p_COD_TPO_OPE_AOS;
    END IF;

    --CODIGO BANCO ORIGEN
    IF p_COD_BCO_ORG = '-1' THEN
      v_COD_BCO_ORG := NULL;
    ELSE
      v_COD_BCO_ORG := p_COD_BCO_ORG; ---CAH
    END IF;

    --CODIGO BANCO DESTINO
    IF p_COD_BCO_DTN = '-1' THEN
      v_COD_BCO_DTN := NULL;
    ELSE
      v_COD_BCO_DTN := p_COD_BCO_DTN;
    END IF;

    --CODIGO MT
    IF p_COD_MT_SWF = -1 THEN
      v_COD_MT := NULL;
    ELSE
      v_COD_MT := p_COD_MT_SWF;
    END IF;

    --FLAG INGRESO
    IF p_FLG_EGR_ING = -1 THEN
      --
      v_FLG_EGR_ING := NULL;
    ELSE
      v_FLG_EGR_ING := p_FLG_EGR_ING; --- CAH
    END IF;

    --VALIDACION MONTO DESDE
    IF p_IMP_OPE_DESDE = -1 THEN
      v_IMp_DSD := 0;
    ELSE
      v_IMp_DSD := p_IMP_OPE_DESDE;
    END IF;

    --VALIDACION MONTO HASTA
    IF p_IMP_OPE_HASTA = -1 THEN
      v_IMp_HST := 9999999999999;
    ELSE
      v_IMp_HST := p_IMP_OPE_HASTA;
    END IF;

    --TIPO MONEDA
    IF p_COD_DVI = '-1' THEN
      v_COD_DVI := NULL;
    ELSE
      v_COD_DVI := p_COD_DVI;
    END IF;

    --NUMERO REFERENCIA SWIFT
    IF p_NUM_REF_SWF = '-1' THEN
      v_NUM_REF_SWF := NULL;

    ELSE
      v_NUM_REF_SWF := UPPER(p_NUM_REF_SWF);
    END IF;

    --ESTADO
    IF p_COD_EST_AOS = -1 THEN
      v_COD_EST_AOS := NULL;
    ELSE
      v_COD_EST_AOS := p_COD_EST_AOS;
    END IF;

    --p_COD_SIS_ENT
    IF p_COD_SIS_ENT = '-1' THEN
      v_COD_SIS_ENT := NULL;
    ELSE
      v_COD_SIS_ENT := p_COD_SIS_ENT;
    END IF;

    --p_COD_SIS_SAL
    IF p_COD_SIS_SAL = '-1' THEN
      v_COD_SIS_SAL := NULL;
    ELSE
      v_COD_SIS_SAL := p_COD_SIS_SAL;
    END IF;

    -- Verifica si el santander es BSCHCLRM
    SELECT COUNT(TGCDSWSA)
    INTO v_TGCDSWSA FROM TCDT040
    WHERE (TRIM(TGCDSWSA) = PKG_PAB_CONSTANTES.V_BIC_BANCO_SANTANDER_HOM);

    IF (v_TGCDSWSA > 0) THEN
        v_BIC_BSCHCL := PKG_PAB_CONSTANTES.V_BIC_BANCO_SANTANDER_HOM;
    ELSE
        v_BIC_BSCHCL := PKG_PAB_CONSTANTES.V_BIC_BANCO_SANTANDER;
    END IF;

    -- ****** OJO!!! TEMPORAL
    -- Esta condicion esta con un parametro de codigo de banco en duro que se debe eliminar, asi como el select final dado que es un parche (para poder ver operaciones internas)
    -- ******
    IF ((TRIM(NVL(v_COD_BCO_DTN, '#')) = TRIM(PKG_PAB_CONSTANTES.V_BIC_BANCO_SANTANDER) OR TRIM(NVL(v_COD_BCO_DTN,'#')) = '#') AND (TRIM(NVL(v_COD_BCO_ORG,'#')) = TRIM(PKG_PAB_CONSTANTES.V_BIC_BANCO_SANTANDER) OR TRIM(NVL(v_COD_BCO_ORG,'#')) = '#')) OR
       ((TRIM(NVL(v_COD_BCO_DTN, '#')) = TRIM(PKG_PAB_CONSTANTES.V_BIC_BANCO_SANTANDER_HOM) OR TRIM(NVL(v_COD_BCO_DTN,'#')) = '#') AND (TRIM(NVL(v_COD_BCO_ORG,'#')) = TRIM(PKG_PAB_CONSTANTES.V_BIC_BANCO_SANTANDER_HOM) OR TRIM(NVL(v_COD_BCO_ORG,'#')) = '#')) THEN
        v_VER_INTERNAS := 1;
    ELSE
        v_VER_INTERNAS := 0;
    END IF;

    OPEN p_CURSOR FOR
      SELECT
            (SELECT DSC_SIS_ENT_SAL FROM DBO_PAB.PABS_DT_SISTM_ENTRD_SALID WHERE COD_SIS_ENT_SAL = DETOP.COD_SIS_SAL) AS CANAL_SALIDA,
            --PKG_PAB_UTILITY.FN_PAB_OBT_DSC_SIS_ENT_SAL('') AS CANAL_SALIDA,
             DETOP.COD_MT_SWF AS MT_SWIFT,
             DETOP.COD_TPO_OPE_AOS AS TIPO_OPERACION,
             EST.DSC_EST_AOS AS ESTADO,
             --0'HOL' || 'A CASA' AS FECHA_VALUTA,
             --DETOP.FEC_VTA AS FECHA_VALUTA,
             --CAH CAMPO NUEVO TIPO MOVIMIENTO
             TO_CHAR(DETOP.FEC_VTA,'DD/MM/YYYY') || ' ' || TO_CHAR(DETOP.FEC_ACT_OPE,'HH24:MI:SS') AS FECHA_VALUTA,
             DETOP.NUM_REF_SWF AS NUM_REF_SWIFT,
             DETOP.COD_DVI AS MONEDA,
             (SELECT DSC_SIS_ENT_SAL FROM DBO_PAB.PABS_DT_SISTM_ENTRD_SALID WHERE COD_SIS_ENT_SAL = DETOP.COD_SIS_ENT) AS CANAL_ENTRADA -- EX ORIGEN
            ,
             DETOP.COD_BCO_DTN AS BANCO_DESTINO,
             (SELECT DES_BCO FROM TCDTBAI WHERE TGCDSWSA = DETOP.COD_BCO_DTN) AS DSC_BCO_DTN,
             /* ************************************************************************************************************************** */
             DETOP.NOM_BFC  AS NOMBRE_BENEFICIARIO,
             LPAD((TRIM(DETOP.NUM_DOC_BFC) || DETOP.COD_TPD_BFC),11,'0') AS RUT_BENEFICIARIO,
             DETOP.NUM_CTA_BFC   AS CTA_BENEFICIARIO,
             /* ************************************************************************************************************************** */
             DETOP.COD_BCO_ORG AS BANCO_ORIGEN,
             (SELECT DES_BCO FROM TCDTBAI WHERE TGCDSWSA = DETOP.COD_BCO_ORG) AS DSC_BCO_ORG,
             /* ************************************************************************************************************************** */
             DETPINF.NOM_ODN AS NOMBRE_ORDENANTE,
             LPAD(TRIM(DETPINF.NUM_DOC_ODN) || DETPINF.COD_TPD_ODN,11,'0') AS RUT_ORDENANTE,
             DETPINF.NUM_CTA_ODN  AS CTA_ORDENANTE,
             /* ************************************************************************************************************************** */
             DETOP.IMP_OPE AS MONTO,
             DECODE(DETOP.NUM_FOL_NMN, 0, 0, DETOP.NUM_FOL_NMN) AS NUM_NOMINA,
             DETOP.NUM_FOL_OPE AS NUM_OPERACION,
             DETOP.FEC_ISR_OPE AS FECHA_INS_OPE,
             'N' AS ORIGEN,
             DECODE(DETOP.FLG_EGR_ING,
             PKG_PAB_CONSTANTES.V_FLG_EGR,
             PKG_PAB_CONSTANTES.V_STR_FLG_EGR,
            PKG_PAB_CONSTANTES.V_STR_FLG_ING) AS TIPO_MOVIMIENTO --CAH CAMPO NUEVO TIPO MOVIMIENTO
            ,
             DETOP.NUM_IDF_GTR_DOC AS NUM_IDF_GTR_DOC, --CAH ID GESTOR DOCUMENTAL
             /* ************************************************************************************************************************** */
             DETOP.FEC_ENV_RCP_SWF AS FECHA_ENV_REC_SWT,
             CASE WHEN DETOP.HOR_ENV_RCP_SWF < 1000  THEN
                TO_CHAR(TO_DATE('0'|| DETOP.HOR_ENV_RCP_SWF,'hh24:mi'),'hh24:mi')
             ELSE
                TO_CHAR(TO_DATE(DETOP.HOR_ENV_RCP_SWF,'hh24:mi'),'hh24:mi')
             END AS HORA_ENV_REC_SWT
             /* ************************************************************************************************************************** */
        FROM PABS_DT_DETLL_OPRCN DETOP
        LEFT JOIN PABS_DT_OPRCN_INFCN_OPCON DETPINF
          ON DETOP.NUM_FOL_OPE = DETPINF.NUM_FOL_OPE
         AND DETOP.FEC_ISR_OPE = DETPINF.FEC_ISR_OPE
        LEFT JOIN PABS_DT_ESTDO_ALMOT EST
          ON EST.COD_EST_AOS = DETOP.COD_EST_AOS
       WHERE TRUNC(DETOP.FEC_VTA) BETWEEN p_FEC_DESDE AND p_FEC_HASTA
         AND DETOP.NUM_FOL_OPE LIKE
             NVL(v_NUM_FOL_OPE, DETOP.NUM_FOL_OPE) || '%'
         AND DETOP.NUM_FOL_NMN LIKE
             NVL(v_NUM_FOL_NMN, DETOP.NUM_FOL_NMN) || '%'
         AND DETOP.COD_TPO_OPE_AOS =
             NVL(v_COD_TPO_OPE_AOS, DETOP.COD_TPO_OPE_AOS)
         AND DETOP.COD_EST_AOS = NVL(v_COD_EST_AOS, DETOP.COD_EST_AOS)
         AND NVL(DETOP.COD_SIS_ENT, '#') LIKE
             NVL(v_COD_SIS_ENT, NVL(DETOP.COD_SIS_ENT, '#')) || '%'
         AND NVL(DETOP.COD_SIS_SAL, '#') LIKE
             NVL(v_COD_SIS_SAL, NVL(DETOP.COD_SIS_SAL, '#')) || '%'
         AND DETOP.COD_BCO_DTN = NVL(v_COD_BCO_DTN, DETOP.COD_BCO_DTN)
         AND DETOP.COD_BCO_ORG = NVL(v_COD_BCO_ORG, DETOP.COD_BCO_ORG) --Nuevo campo Bco Origen
         AND DETOP.COD_MT_SWF = NVL(v_COD_MT, DETOP.COD_MT_SWF)
         AND DETOP.FLG_EGR_ING = NVL(v_FLG_EGR_ING, DETOP.FLG_EGR_ING) --Nuevo campo Flag ingreso-egreso
         AND UPPER(NVL(DETOP.NUM_REF_SWF, '#')) LIKE
             NVL(v_NUM_REF_SWF, NVL(UPPER(DETOP.NUM_REF_SWF), '#')) || '%'
         AND DETOP.IMP_OPE BETWEEN NVL(v_IMp_DSD, DETOP.IMP_OPE) AND
             NVL(v_IMp_HST, DETOP.IMP_OPE)
         AND DETOP.COD_DVI LIKE NVL(v_COD_DVI, DETOP.COD_DVI) || '%'
      UNION ALL
      SELECT (SELECT DSC_SIS_ENT_SAL FROM DBO_PAB.PABS_DT_SISTM_ENTRD_SALID WHERE COD_SIS_ENT_SAL = DETOPHT.COD_SIS_SAL) AS CANAL_SALIDA --exPago
            ,
             DETOPHT.COD_MT_SWF AS MT_SWIFT,
             DETOPHT.COD_TPO_OPE_AOS AS TIPO_OPERACION,
             EST.DSC_EST_AOS AS ESTADO,
             --DETOPHT.FEC_VTA AS FECHA_VALUTA,
             TO_CHAR(DETOPHT.FEC_VTA,'DD/MM/YYYY') || ' ' || TO_CHAR(DETOPHT.FEC_ACT_OPE,'HH24:MI:SS') AS FECHA_VALUTA,
             DETOPHT.NUM_REF_SWF AS NUM_REF_SWIFT,
             DETOPHT.COD_DVI AS MONEDA,
             (SELECT DSC_SIS_ENT_SAL FROM DBO_PAB.PABS_DT_SISTM_ENTRD_SALID WHERE COD_SIS_ENT_SAL = DETOPHT.COD_SIS_ENT) AS CANAL_ENTRADA --EX ORIGEN
            ,
             DETOPHT.COD_BCO_DTN AS BANCO_DESTINO,
             (SELECT DES_BCO FROM TCDTBAI WHERE TGCDSWSA = DETOPHT.COD_BCO_DTN) AS DSC_BCO_DTN,
             /* ************************************************************************************************************************** */
             DETOPHT.NOM_BFC  AS NOMBRE_BENEFICIARIO,
             LPAD((TRIM(DETOPHT.NUM_DOC_BFC) || DETOPHT.COD_TPD_BFC),11,'0')  AS RUT_BENEFICIARIO,
             DETOPHT.NUM_CTA_BFC  AS CTA_BENEFICIARIO,
             /* ************************************************************************************************************************** */
             DETOPHT.COD_BCO_ORG AS BANCO_ORIGEN -- Nuevo campo bco origen
            ,
             (SELECT DES_BCO FROM TCDTBAI WHERE TGCDSWSA = DETOPHT.COD_BCO_ORG) AS DSC_BCO_DTN,
             /* ************************************************************************************************************************** */
             OPEOPHT.NOM_ODN  AS NOMBRE_ORDENANTE,
             LPAD(TRIM(OPEOPHT.NUM_DOC_ODN) || OPEOPHT.COD_TPD_ODN,11,'0')  AS RUT_ORDENANTE,
             OPEOPHT.NUM_CTA_ODN   AS CTA_ORDENANTE,
             /* ************************************************************************************************************************** */
             DETOPHT.IMP_OPE AS MONTO,
             DECODE(DETOPHT.NUM_FOL_NMN, 0, 0, DETOPHT.NUM_FOL_NMN) AS NUM_NOMINA,
             DETOPHT.NUM_FOL_OPE AS NUM_OPERACION,
             DETOPHT.FEC_ISR_OPE AS FECHA_INS_OPE,
             'H' AS ORIGEN,
             DECODE(DETOPHT.FLG_EGR_ING,
                    PKG_PAB_CONSTANTES.V_FLG_EGR,
                    PKG_PAB_CONSTANTES.V_STR_FLG_EGR,
                 PKG_PAB_CONSTANTES.V_STR_FLG_ING) AS TIPO_MOVIMIENTO --CAH CAMPO NUEVO TIPO MOVIMIENTO
            ,
             DETOPHT.NUM_IDF_GTR_DOC AS NUM_IDF_GTR_DOC, --CAH ID GESTOR DOCUMENTAL
             /* ************************************************************************************************************************** */
             DETOPHT.FEC_ENV_RCP_SWF AS FECHA_ENV_REC_SWT,
             CASE WHEN DETOPHT.HOR_ENV_RCP_SWF < 1000  THEN
                TO_CHAR(TO_DATE('0'||DETOPHT.HOR_ENV_RCP_SWF,'hh24:mi'),'hh24:mi')
             ELSE
                TO_CHAR(TO_DATE(DETOPHT.HOR_ENV_RCP_SWF,'hh24:mi'),'hh24:mi')
             END  AS HORA_ENV_REC_SWT
             /* ************************************************************************************************************************** */
        FROM PABS_DT_DETLL_OPRCN_HTRCA DETOPHT
        LEFT JOIN PABS_DT_OPRCN_OPCON_HTRCA OPEOPHT --DETPINF
          ON DETOPHT.NUM_FOL_OPE = OPEOPHT.NUM_FOL_OPE
         AND DETOPHT.FEC_ISR_OPE = OPEOPHT.FEC_ISR_OPE
        LEFT JOIN PABS_DT_ESTDO_ALMOT EST
          ON EST.COD_EST_AOS = DETOPHT.COD_EST_AOS
       WHERE TRUNC(DETOPHT.FEC_VTA) BETWEEN p_FEC_DESDE AND p_FEC_HASTA
         AND DETOPHT.NUM_FOL_OPE LIKE
             NVL(v_NUM_FOL_OPE, DETOPHT.NUM_FOL_OPE) || '%'
         AND DETOPHT.NUM_FOL_NMN LIKE
             NVL(v_NUM_FOL_NMN, DETOPHT.NUM_FOL_NMN) || '%'
         AND DETOPHT.COD_TPO_OPE_AOS =
             NVL(v_COD_TPO_OPE_AOS, DETOPHT.COD_TPO_OPE_AOS)
         AND DETOPHT.COD_EST_AOS = NVL(v_COD_EST_AOS, DETOPHT.COD_EST_AOS)
         AND NVL(DETOPHT.COD_SIS_ENT, '#') LIKE
             NVL(v_COD_SIS_ENT, NVL(DETOPHT.COD_SIS_ENT, '#')) || '%'
         AND NVL(DETOPHT.COD_SIS_SAL, '#') LIKE
             NVL(v_COD_SIS_SAL, NVL(DETOPHT.COD_SIS_SAL, '#')) || '%'
         AND DETOPHT.COD_BCO_DTN = NVL(v_COD_BCO_DTN, DETOPHT.COD_BCO_DTN)
         AND DETOPHT.COD_BCO_ORG = NVL(v_COD_BCO_ORG, DETOPHT.COD_BCO_ORG) --Nuevo campo bco Origen
         AND DETOPHT.COD_MT_SWF = NVL(v_COD_MT, DETOPHT.COD_MT_SWF)
         AND DETOPHT.FLG_EGR_ING = NVL(v_FLG_EGR_ING, DETOPHT.FLG_EGR_ING) --Nuevo campo Flag ingreso-egreso
         AND NVL(DETOPHT.NUM_REF_SWF, '#') LIKE
             NVL(v_NUM_REF_SWF, NVL(DETOPHT.NUM_REF_SWF, '#')) || '%'
         AND DETOPHT.IMP_OPE BETWEEN NVL(v_IMp_DSD, DETOPHT.IMP_OPE) AND
             NVL(v_IMp_HST, DETOPHT.IMP_OPE)
         AND DETOPHT.COD_DVI LIKE NVL(v_COD_DVI, DETOPHT.COD_DVI) || '%'

      UNION ALL

      SELECT PKG_PAB_UTILITY.FN_PAB_OBT_DSC_SIS_ENT_SAL(DETOPINT.COD_SIS_SAL_ITR) AS CANAL_SALIDA, --exPago
             0 AS MT_SWIFT,
             DETOPINT.COD_TPO_OPE_AOS_LQD AS TIPO_OPERACION,
             EST.DSC_EST_AOS AS ESTADO,
             --DETOPINT.FEC_VTA AS FECHA_VALUTA,
             TO_CHAR(FEC_ISR_OPE_ITR,'DD/MM/YYYY') AS FECHA_VALUTA,
             '' AS NUM_REF_SWIFT,
             DETOPINT.COD_DVI_ITR AS MONEDA,
             PKG_PAB_UTILITY.FN_PAB_OBT_DSC_SIS_ENT_SAL(DETOPINT.COD_SIS_ENT_ITR) AS CANAL_ENTRADA, --EX ORIGEN
             v_BIC_BSCHCL AS BANCO_DESTINO,
             '' AS DSC_BCO_DTN,
             /* ************************************************************************************************************************** */
             DETOPINT.NOM_BFC_ITR  AS NOMBRE_BENEFICIARIO,
             LPAD((TRIM(DETOPINT.NUM_DOC_BFC_ITR) || DETOPINT.COD_TPD_BFC_ITR),11,'0')  AS RUT_BENEFICIARIO,
             DETOPINT.NUM_CTA_BFC_ITR  AS CTA_BENEFICIARIO,
             /* ************************************************************************************************************************** */
             v_BIC_BSCHCL AS BANCO_ORIGEN,
             '' AS DSC_BCO_DTN,
             /* ************************************************************************************************************************** */
             ''  AS NOMBRE_ORDENANTE,
             ''  AS RUT_ORDENANTE,
             ''   AS CTA_ORDENANTE,
             /* ************************************************************************************************************************** */
             DETOPINT.IMP_OPE_ITR AS MONTO,
             DECODE(DETOPINT.NUM_FOL_NMN_ITR, 0, 0, DETOPINT.NUM_FOL_NMN_ITR) AS NUM_NOMINA,
             DETOPINT.NUM_FOL_OPE_ITR AS NUM_OPERACION,
             DETOPINT.FEC_ISR_OPE_ITR AS FECHA_INS_OPE,
             'I' AS ORIGEN,
             DECODE(DETOPINT.FLG_EGR_ING_ITR,
                    PKG_PAB_CONSTANTES.V_FLG_EGR,
                    PKG_PAB_CONSTANTES.V_STR_FLG_EGR,
                    PKG_PAB_CONSTANTES.V_STR_FLG_ING) AS TIPO_MOVIMIENTO, --CAH CAMPO NUEVO TIPO MOVIMIENTO
                '' AS NUM_IDF_GTR_DOC, --CAH ID GESTOR DOCUMENTAL
                /* ************************************************************************************************************************** */
             NULL AS FECHA_ENV_REC_SWT,
             NULL AS HORA_ENV_REC_SWT
             /* ************************************************************************************************************************** */
        FROM PABS_DT_DETLL_OPRCN_INTER DETOPINT
        LEFT JOIN PABS_DT_ESTDO_ALMOT EST
          ON EST.COD_EST_AOS = DETOPINT.COD_EST_AOS
       WHERE TRUNC(DETOPINT.FEC_ISR_OPE_ITR) BETWEEN p_FEC_DESDE AND p_FEC_HASTA -- Como operaciones internas no tienen valuta, se filtra por la fecha ingreso de operacion
         AND DETOPINT.NUM_FOL_OPE_ITR LIKE
             NVL(v_NUM_FOL_OPE, DETOPINT.NUM_FOL_OPE_ITR) || '%'
         AND DETOPINT.NUM_FOL_NMN_ITR LIKE
             NVL(v_NUM_FOL_NMN, DETOPINT.NUM_FOL_NMN_ITR) || '%'
         AND DETOPINT.COD_TPO_OPE_AOS_LQD =
             NVL(v_COD_TPO_OPE_AOS, DETOPINT.COD_TPO_OPE_AOS_LQD)
         AND DETOPINT.COD_EST_AOS = NVL(v_COD_EST_AOS, DETOPINT.COD_EST_AOS)
         AND NVL(DETOPINT.COD_SIS_ENT_ITR, '#') LIKE
             NVL(v_COD_SIS_ENT, NVL(DETOPINT.COD_SIS_ENT_ITR, '#')) || '%'
         AND NVL(DETOPINT.COD_SIS_SAL_ITR, '#') LIKE
             NVL(v_COD_SIS_SAL, NVL(DETOPINT.COD_SIS_SAL_ITR, '#')) || '%'
         AND v_VER_INTERNAS = 1 -- Si en los parametros de entrada se buscan todos los bancos solo el origen y destino santander
         /* AND DETOPHT.COD_MT_SWF = NVL(v_COD_MT, DETOPHT.COD_MT_SWF) */
         AND DETOPINT.FLG_EGR_ING_ITR = NVL(v_FLG_EGR_ING, DETOPINT.FLG_EGR_ING_ITR) --Nuevo campo Flag ingreso-egreso
        -- AND NVL(DETOPHT.NUM_REF_SWF, '#') LIKE
        --     NVL(v_NUM_REF_SWF, NVL(DETOPHT.NUM_REF_SWF, '#')) || '%'
         AND DETOPINT.IMP_OPE_ITR BETWEEN NVL(v_IMp_DSD, DETOPINT.IMP_OPE_ITR) AND
             NVL(v_IMp_HST, DETOPINT.IMP_OPE_ITR)
         AND DETOPINT.COD_DVI_ITR LIKE NVL(v_COD_DVI, DETOPINT.COD_DVI_ITR) || '%';

    --
    --
    p_ERROR := PKG_PAB_CONSTANTES.v_OK; --Procedimiento ejecutado de forma correcta
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      err_code := SQLCODE;
      err_msg  := SUBSTR(SQLERRM, 1, 300);
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                         v_NOM_PCK,
                                         err_msg,
                                         v_NOM_SP);
      p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
    WHEN OTHERS THEN
      err_code := SQLCODE;
      err_msg  := SUBSTR(SQLERRM, 1, 300);
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                         v_NOM_PCK,
                                         err_msg,
                                         v_NOM_SP);
      p_ERROR     := PKG_PAB_CONSTANTES.v_ERROR;
      p_s_mensaje := err_code || '-' || err_msg;
      RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);
  END Sp_PAB_CON_OPE_HIST;

 -- ***********************************************************************************************
  -- Funcion/Procedimiento: Sp_PAB_BUS_DET_OPE_CON
  -- Objetivo: Procedimiento almacenado que retorna el detalle de una operacion historica seleccionada
  -- Base de Datos: DGBMSEGDB01_NGALTM
  -- Tablas Usadas: PABS_DT_DETLL_OPRCN, PABS_DT_OPRCN_INFCN_OPCON, PABS_DT_ESTDO_ALMOT, PABS_DT_SISTM_ENTRD_SALID, PABS_DT_DETLL_OPRCN_HTRCA, PABS_DT_OPRCN_OPCON_HTRCA
  -- Fecha: 21/06/16
  -- Autor: Santander
  -- Input:
  -- p_NUM_FOL_OPE->   foliador del banco, PK tabla operaciones nomina.
  -- p_FEC_ISR_OPE->   Fecha de insercion de la operacion.
  -- p_TIP_BUS_HIST-> tipo busqueda de historificacion
  -- Output:
  -- p_CURSOR -> Informacion de la operacion
  -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
  -- Input/Output: N/A
  -- Retorno: N/A.
  -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
  -- ***********************************************************************************************
  PROCEDURE Sp_PAB_BUS_DET_OPE_CON(p_NUM_FOL_OPE  IN NUMBER,
                                   p_FEC_ISR_OPE  IN date,
                                   p_TIP_BUS_HIST IN CHAR,
                                   p_CURSOR       OUT SYS_REFCURSOR,
                                   p_ERROR        OUT NUMBER) IS
    v_NOM_SP     VARCHAR2(30) := 'Sp_PAB_BUS_DET_OPE_CON';
    v_TPO_BUS_H  CHAR := 'H';
    v_TPO_BUS_N  CHAR := 'N';
    v_TPO_BUS_I  CHAR := 'I';
    v_TGCDSWSA   NUMBER;
    v_BIC_BSCHCL CHAR(11);
    ERR_COD_INEX EXCEPTION;
  BEGIN

    CASE
      WHEN p_TIP_BUS_HIST = v_TPO_BUS_N THEN
        OPEN p_CURSOR FOR
          SELECT DETOPE.NUM_FOL_OPE AS NUM_OPE,
                 DETOPE.NUM_REF_SWF AS REF_SWIFT,
                 DETOPE.COD_TPO_OPE_AOS AS TIPO_OPERACION,
                 DETOPE.NUM_REF_EXT AS REF_EXT_SWIFT,
                 DETOPE.COD_MT_SWF AS COD_MT,
                 DETOPE.COD_DVI AS DIVISA,
                 SAL.DSC_SIS_ENT_SAL AS CANAL_SALIDA,
                 DETOPE.COD_BCO_DTN AS BIC_DESTINO,
                 DETOPE.COD_BCO_ORG AS BIC_ORIGEN,
                 DETOPC.GLS_DIR_ODN AS DIRECCION_ORDENANTE,
                 NVL2(DETOPC.NUM_DOC_ODN,
                      TRIM(DETOPC.NUM_DOC_ODN) || '-' || DETOPC.COD_TPD_ODN,
                      NULL) AS DOC_ORDENANTE,
                 DETOPC.NOM_ODN AS NOMBRE_ORDENANTE,
                 DETOPC.NUM_CTA_ODN AS CTA_ORDENANTE,
                 DETOPC.NOM_CDD_ODN AS CIUDAD_ORDENANTE,
                 PKG_PAB_UTILITY.FN_PAB_COMBO_PAIS(DETOPC.COD_PAS_ODN) AS PAIS_ORDENANTE,
                 DETOPC.NUM_CTA_DCv_ODN AS CTA_DCv_ORDENANTE,
                 DETOPC.COD_BCO_BFC AS BIC_BENEFICIARIO,
                 NVL2(DETOPE.NUM_DOC_BFC,
                      TRIM(DETOPE.NUM_DOC_BFC) || '-' || DETOPE.COD_TPD_BFC,
                      NULL) AS DOC_BENEFICIARIO,
                 DETOPE.NOM_BFC AS NOMBRE_BENEFICIARIO,
                 DETOPE.NUM_CTA_BFC AS CTA_BENEFICIARIO,
                 DETOPC.GLS_DIR_BFC AS DIRECCION_BENEFICIARIO,
                 DETOPC.NOM_CDD_BFC AS CIUDAD_BENEFICIARIO,
                 PKG_PAB_UTILITY.FN_PAB_COMBO_PAIS(DETOPC.COD_PAS_BFC) AS PAIS_BENEFICIARIO,
                 DETOPC.NUM_CTA_DCv_BFC AS CTA_DCv_BENEFICIARIO,
                 DETOPC.COD_BCO_ITD AS BIC_INTERMEDIARIO,
                 DETOPE.IMp_OPE AS MONTO_OPE,
                 DETOPE.COD_SUC AS COD_SUCURSAL,
                 DETOPC.FEC_TPO_OPE_CCLV AS FECHA_CCLV,
                 PKG_PAB_UTILITY.Fn_PAB_COMBO_CAMARA(DETOPC.COD_TPO_CMA) AS TIPO_CAMARA,
                 DETOPC.NUM_CLv_NGC AS CLAVE_NEGOCIO,
                 DETOPE.FEC_VTA AS FECHA_VALUTA,
                 DETOPE.NUM_REF_CTB AS REF_CONTABLE,
                 PKG_PAB_UTILITY.Fn_PAB_COMBO_TSALDO(DETOPC.COD_TPO_SDO) AS TIPO_SALDO,
                 DETOPC.NUM_AGT AS NUM_AGENTE,
                 DETOPC.COD_FND_CCLV AS FONDO_CCLV,
                 DETOPC.OBS_OPC_SWF AS OBSERVACION_SWIFT,
                 -------------------------------------------------
                 ESTALM.DSC_EST_AOS AS ESTADO,
                 DETOPE.FEC_ISR_OPE AS FEC_INS,
                 DETOPE.NUM_FOL_NMN AS NUM_NOM,
                 DETOPE.NUM_OPE_SIS_ENT AS NUM_OPE_ENT,
                 DECODE(DETOPE.FLG_EGR_ING,
                        PKG_PAB_CONSTANTES.v_FLG_EGR,
                        PKG_PAB_CONSTANTES.v_STR_FLG_EGR,
                        PKG_PAB_CONSTANTES.v_STR_FLG_ING) AS EGRESO_INGRESO,
                 DETOPE.COD_SIS_ENT AS COD_CANAL_ENT,
                 ENT.DSC_SIS_ENT_SAL AS CANAL_ENTRADA,
                 DETOPE.COD_SIS_SAL AS COD_CANAL_SAL,
                 DECODE(DETOPE.COD_TPO_ING,
                        PKG_PAB_CONSTANTES.v_COD_TIp_ING_MAS,
                        PKG_PAB_CONSTANTES.v_STR_TIp_ING_MAS,
                        PKG_PAB_CONSTANTES.V_COD_TIP_ING_UNI,
                        PKG_PAB_CONSTANTES.V_STR_TIP_ING_UNI,
                        PKG_PAB_CONSTANTES.V_COD_TIP_ING_DIR,
                        PKG_PAB_CONSTANTES.V_STR_TIP_ING_DIR) AS TIPO_INGRESO,
                 DETOPE.FEC_ENv_RCp_SWF AS FECHA_SWIFT,
                 DETOPE.HOR_ENv_RCp_SWF AS HORA_SWIFT,
                 DECODE(DETOPE.FLG_MRD_UTZ_SWF,
                        PKG_PAB_CONSTANTES.v_FLG_MRD_MN,
                        PKG_PAB_CONSTANTES.v_STR_FLG_MRD_MN,
                        PKG_PAB_CONSTANTES.v_STR_FLG_MRD_MX) AS MERCADO,
                 DECODE(DETOPE.COD_EST_AOS,
                        PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEREC,
                        DETOPC.GLS_EST_RCH,
                        PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPVI,
                        DETOPC.GLS_MTv_VSD,
                        DETOPE.GLS_ADC_EST) AS GLOSA_ESTADO,
                 DETOPC.NUM_DOC_BFC AS DOC_BENEFICIARIO_OPC,
                 DETOPC.COD_TPD_BFC AS TIPO_DOC_BENEFICIARIO_OPC,
                 DETOPC.COD_TPO_FND AS CODIGO_FONDO,
                 DETOPC.NUM_CLv_IDF AS NUMERO_IDENTIFICATORIO,
                 DETOPC.GLS_EST_RCH AS GLOSA_RECHAZO,
                 DETOPC.GLS_MTv_VSD AS GLOSA_VISADA,
                 DETOPE.NUM_IDF_UNC_UID AS GPI
            FROM PABS_DT_DETLL_OPRCN       DETOPE,
                 PABS_DT_OPRCN_INFCN_OPCON DETOPC,
                 PABS_DT_ESTDO_ALMOT       ESTALM,
                 PABS_DT_SISTM_ENTRD_SALID ENT,
                 PABS_DT_SISTM_ENTRD_SALID SAL
           WHERE DETOPE.NUM_FOL_OPE = p_NUM_FOL_OPE
             AND DETOPE.FEC_ISR_OPE = p_FEC_ISR_OPE
             AND DETOPE.NUM_FOL_OPE = DETOPC.NUM_FOL_OPE(+)
             AND DETOPE.FEC_ISR_OPE = DETOPC.FEC_ISR_OPE(+)
             AND DETOPE.COD_EST_AOS = ESTALM.COD_EST_AOS
             AND DETOPE.COD_SIS_ENT = ENT.COD_SIS_ENT_SAL
             AND DETOPE.COD_SIS_SAL = SAL.COD_SIS_ENT_SAL(+);
      WHEN p_TIP_BUS_HIST = v_TPO_BUS_H THEN
        OPEN p_CURSOR FOR
          SELECT DETOPETH.NUM_FOL_OPE AS NUM_OPE,
                 DETOPETH.NUM_REF_SWF AS REF_SWIFT,
                 DETOPETH.COD_TPO_OPE_AOS AS TIPO_OPERACION,
                 DETOPETH.NUM_REF_EXT AS REF_EXT_SWIFT,
                 DETOPETH.COD_MT_SWF AS COD_MT,
                 DETOPETH.COD_DVI AS DIVISA,
                 SAL.DSC_SIS_ENT_SAL AS CANAL_SALIDA,
                 DETOPETH.COD_BCO_DTN AS BIC_DESTINO,
                 DETOPETH.COD_BCO_ORG AS BIC_ORIGEN,
                 DETOPCTH.GLS_DIR_ODN AS DIRECCION_ORDENANTE,
                 NVL2(DETOPCTH.NUM_DOC_ODN,
                      TRIM(DETOPCTH.NUM_DOC_ODN) || '-' ||
                      DETOPCTH.COD_TPD_ODN,
                      NULL) AS DOC_ORDENANTE,
                 DETOPCTH.NOM_ODN AS NOMBRE_ORDENANTE,
                 DETOPCTH.NUM_CTA_ODN AS CTA_ORDENANTE,
                 DETOPCTH.NOM_CDD_ODN AS CIUDAD_ORDENANTE,
                 PKG_PAB_UTILITY.FN_PAB_COMBO_PAIS(DETOPCTH.COD_PAS_ODN) AS PAIS_ORDENANTE,
                 DETOPCTH.NUM_CTA_DCv_ODN AS CTA_DCv_ORDENANTE,
                 DETOPCTH.COD_BCO_BFC AS BIC_BENEFICIARIO,
                 NVL2(DETOPETH.NUM_DOC_BFC,
                      TRIM(DETOPETH.NUM_DOC_BFC) || '-' ||
                      DETOPETH.COD_TPD_BFC,
                      NULL) AS DOC_BENEFICIARIO,
                 DETOPETH.NOM_BFC AS NOMBRE_BENEFICIARIO,
                 DETOPETH.NUM_CTA_BFC AS CTA_BENEFICIARIO,
                 DETOPCTH.GLS_DIR_BFC AS DIRECCION_BENEFICIARIO,
                 DETOPCTH.NOM_CDD_BFC AS CIUDAD_BENEFICIARIO,
                 PKG_PAB_UTILITY.FN_PAB_COMBO_PAIS(DETOPCTH.COD_PAS_BFC) AS PAIS_BENEFICIARIO,
                 DETOPCTH.NUM_CTA_DCv_BFC AS CTA_DCv_BENEFICIARIO,
                 DETOPCTH.COD_BCO_ITD AS BIC_INTERMEDIARIO,
                 DETOPETH.IMp_OPE AS MONTO_OPE,
                 DETOPETH.COD_SUC AS COD_SUCURSAL,
                 DETOPCTH.FEC_TPO_OPE_CCLV AS FECHA_CCLV,
                 PKG_PAB_UTILITY.Fn_PAB_COMBO_CAMARA(DETOPCTH.COD_TPO_CMA) AS TIPO_CAMARA,
                 DETOPCTH.NUM_CLv_NGC AS CLAVE_NEGOCIO,
                 DETOPETH.FEC_VTA AS FECHA_VALUTA,
                 DETOPETH.NUM_REF_CTB AS REF_CONTABLE,
                 PKG_PAB_UTILITY.Fn_PAB_COMBO_TSALDO(DETOPCTH.COD_TPO_SDO) AS TIPO_SALDO,
                 DETOPCTH.NUM_AGT AS NUM_AGENTE,
                 DETOPCTH.COD_FND_CCLV AS FONDO_CCLV,
                 DETOPCTH.OBS_OPC_SWF AS OBSERVACION_SWIFT,
                 -------------------------------------------------
                 ESTALM.DSC_EST_AOS AS ESTADO,
                 DETOPETH.FEC_ISR_OPE AS FEC_INS,
                 DETOPETH.NUM_FOL_NMN AS NUM_NOM,
                 DETOPETH.NUM_OPE_SIS_ENT AS NUM_OPE_ENT,
                 DECODE(DETOPETH.FLG_EGR_ING,
                        PKG_PAB_CONSTANTES.v_FLG_EGR,
                        PKG_PAB_CONSTANTES.v_STR_FLG_EGR,
                        PKG_PAB_CONSTANTES.v_STR_FLG_ING) AS EGRESO_INGRESO,
                 DETOPETH.COD_SIS_ENT AS COD_CANAL_ENT,
                 ENT.DSC_SIS_ENT_SAL AS CANAL_ENTRADA,
                 DETOPETH.COD_SIS_SAL AS COD_CANAL_SAL,
                 DECODE(DETOPETH.COD_TPO_ING,
                        PKG_PAB_CONSTANTES.v_COD_TIp_ING_MAS,
                        PKG_PAB_CONSTANTES.v_STR_TIp_ING_MAS,
                        PKG_PAB_CONSTANTES.V_COD_TIP_ING_UNI,
                        PKG_PAB_CONSTANTES.V_STR_TIP_ING_UNI,
                        PKG_PAB_CONSTANTES.V_COD_TIP_ING_DIR,
                        PKG_PAB_CONSTANTES.V_STR_TIP_ING_DIR) AS TIPO_INGRESO,
                 DETOPETH.FEC_ENv_RCp_SWF AS FECHA_SWIFT,
                 DETOPETH.HOR_ENv_RCp_SWF AS HORA_SWIFT,
                 DECODE(DETOPETH.FLG_MRD_UTZ_SWF,
                        PKG_PAB_CONSTANTES.v_FLG_MRD_MN,
                        PKG_PAB_CONSTANTES.v_STR_FLG_MRD_MN,
                        PKG_PAB_CONSTANTES.v_STR_FLG_MRD_MX) AS MERCADO,
                 DECODE(DETOPETH.COD_EST_AOS,
                        PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEREC,
                        DETOPCTH.GLS_EST_RCH,
                        PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPVI,
                        DETOPCTH.GLS_MTv_VSD,
                        DETOPETH.GLS_ADC_EST) AS GLOSA_ESTADO,
                 DETOPCTH.NUM_DOC_BFC AS DOC_BENEFICIARIO_OPC,
                 DETOPCTH.COD_TPD_BFC AS TIPO_DOC_BENEFICIARIO_OPC,
                 DETOPCTH.COD_TPO_FND AS CODIGO_FONDO,
                 DETOPCTH.NUM_CLv_IDF AS NUMERO_IDENTIFICATORIO,
                 DETOPCTH.GLS_EST_RCH AS GLOSA_RECHAZO,
                 DETOPCTH.GLS_MTv_VSD AS GLOSA_VISADA,
                 DETOPETH.NUM_IDF_UNC_UID AS GPI
            FROM PABS_DT_DETLL_OPRCN_HTRCA DETOPETH,
                 PABS_DT_OPRCN_OPCON_HTRCA DETOPCTH,
                 PABS_DT_ESTDO_ALMOT       ESTALM,
                 PABS_DT_SISTM_ENTRD_SALID ENT,
                 PABS_DT_SISTM_ENTRD_SALID SAL
           WHERE DETOPETH.NUM_FOL_OPE = p_NUM_FOL_OPE
             AND DETOPETH.FEC_ISR_OPE = p_FEC_ISR_OPE
             AND DETOPETH.NUM_FOL_OPE = DETOPCTH.NUM_FOL_OPE(+)
             AND DETOPETH.FEC_ISR_OPE = DETOPCTH.FEC_ISR_OPE(+)
             AND DETOPETH.COD_EST_AOS = ESTALM.COD_EST_AOS
             AND DETOPETH.COD_SIS_ENT = ENT.COD_SIS_ENT_SAL
             AND DETOPETH.COD_SIS_SAL = SAL.COD_SIS_ENT_SAL(+);
      WHEN p_TIP_BUS_HIST = v_TPO_BUS_I THEN
        -- Verifica si el santander es BSCHCLRM
        SELECT COUNT(TGCDSWSA)
        INTO v_TGCDSWSA FROM TCDT040
        WHERE (TRIM(TGCDSWSA) = PKG_PAB_CONSTANTES.V_BIC_BANCO_SANTANDER_HOM);

        IF (v_TGCDSWSA > 0) THEN
            v_BIC_BSCHCL := PKG_PAB_CONSTANTES.V_BIC_BANCO_SANTANDER_HOM;
        ELSE
            v_BIC_BSCHCL := PKG_PAB_CONSTANTES.V_BIC_BANCO_SANTANDER;
        END IF;

        OPEN p_CURSOR FOR
          SELECT DETOPETI.NUM_FOL_OPE_ITR AS NUM_OPE,
                 '' AS REF_SWIFT,
                 DETOPETI.COD_TPO_OPE_AOS_LQD AS TIPO_OPERACION,
                 '' AS REF_EXT_SWIFT,
                 0 AS COD_MT,
                 DETOPETI.COD_DVI_ITR AS DIVISA,
                 SAL.DSC_SIS_ENT_SAL AS CANAL_SALIDA,
                 v_BIC_BSCHCL AS BIC_DESTINO,
                 v_BIC_BSCHCL AS BIC_ORIGEN,
                 '' AS DIRECCION_ORDENANTE, --DETOPCTH.GLS_DIR_ODN AS DIRECCION_ORDENANTE,
                 NVL2(DETOPETI.NUM_DOC_ODN_ITR,
                     TRIM(DETOPETI.NUM_DOC_ODN_ITR) || '-' ||
                     DETOPETI.COD_TPD_ODN_ITR,
                     NULL) AS DOC_ORDENANTE,
                 DETOPETI.NOM_ODN_ITR AS NOMBRE_ORDENANTE,
                 DETOPETI.NUM_CTA_ODN_ITR AS CTA_ORDENANTE,
                 '' AS CIUDAD_ORDENANTE,
                 '' AS PAIS_ORDENANTE, -- PKG_PAB_UTILITY.FN_PAB_COMBO_PAIS(DETOPCTH.COD_PAS_ODN) AS PAIS_ORDENANTE,
                 '' AS CTA_DCv_ORDENANTE, --DETOPCTH.NUM_CTA_DCv_ODN AS CTA_DCv_ORDENANTE,
                 '' AS BIC_BENEFICIARIO, --DETOPCTH.COD_BCO_BFC AS BIC_BENEFICIARIO,
                 NVL2(DETOPETI.NUM_DOC_BFC_ITR,
                      TRIM(DETOPETI.NUM_DOC_BFC_ITR) || '-' ||
                      DETOPETI.COD_TPD_BFC_ITR,
                      NULL) AS DOC_BENEFICIARIO,
                 DETOPETI.NOM_BFC_ITR AS NOMBRE_BENEFICIARIO,
                 DETOPETI.NUM_CTA_BFC_ITR AS CTA_BENEFICIARIO,
                 '' AS DIRECCION_BENEFICIARIO, --DETOPCTH.GLS_DIR_BFC AS DIRECCION_BENEFICIARIO,
                 '' AS CIUDAD_BENEFICIARIO, --DETOPCTH.NOM_CDD_BFC AS CIUDAD_BENEFICIARIO,
                 '' AS PAIS_BENEFICIARIO, --PKG_PAB_UTILITY.FN_PAB_COMBO_PAIS(DETOPCTH.COD_PAS_BFC) AS PAIS_BENEFICIARIO,
                 '' AS CTA_DCv_BENEFICIARIO, --DETOPCTH.NUM_CTA_DCv_BFC AS CTA_DCv_BENEFICIARIO,
                 '' AS BIC_INTERMEDIARIO, --DETOPCTH.COD_BCO_ITD AS BIC_INTERMEDIARIO,
                 DETOPETI.IMp_OPE_ITR AS MONTO_OPE,
                 DETOPETI.COD_SUC_ITR AS COD_SUCURSAL,
                 '' AS FECHA_CCLV, --DETOPCTH.FEC_TPO_OPE_CCLV AS FECHA_CCLV,
                 '' AS TIPO_CAMARA, -- PKG_PAB_UTILITY.Fn_PAB_COMBO_CAMARA(DETOPCTH.COD_TPO_CMA) AS TIPO_CAMARA,
                 '' AS CLAVE_NEGOCIO, --DETOPCTH.NUM_CLv_NGC AS CLAVE_NEGOCIO,
                 DETOPETI.FEC_ISR_OPE_ITR AS FECHA_VALUTA,
                 DETOPETI.NUM_REF_CTB_ITR AS REF_CONTABLE,
                 '' AS TIPO_SALDO, --PKG_PAB_UTILITY.Fn_PAB_COMBO_TSALDO(DETOPCTH.COD_TPO_SDO) AS TIPO_SALDO,
                 '' AS NUM_AGENTE, --DETOPCTH.NUM_AGT AS NUM_AGENTE,
                 '' AS FONDO_CCLV, --DETOPCTH.COD_FND_CCLV AS FONDO_CCLV,
                 '' AS OBSERVACION_SWIFT, --DETOPCTH.OBS_OPC_SWF AS OBSERVACION_SWIFT,
                 -------------------------------------------------
                 ESTALM.DSC_EST_AOS AS ESTADO,
                 DETOPETI.FEC_ISR_OPE_ITR AS FEC_INS,
                 DETOPETI.NUM_FOL_NMN_ITR AS NUM_NOM,
                 DETOPETI.NUM_OPE_SIS_ENT_ITR AS NUM_OPE_ENT,
                 DECODE(DETOPETI.FLG_EGR_ING_ITR,
                        PKG_PAB_CONSTANTES.v_FLG_EGR,
                        PKG_PAB_CONSTANTES.v_STR_FLG_EGR,
                        PKG_PAB_CONSTANTES.v_STR_FLG_ING) AS EGRESO_INGRESO,
                 DETOPETI.COD_SIS_ENT_ITR AS COD_CANAL_ENT,
                 ENT.DSC_SIS_ENT_SAL AS CANAL_ENTRADA,
                 DETOPETI.COD_SIS_SAL_ITR AS COD_CANAL_SAL,
                 DECODE(DETOPETI.COD_TPO_ING,
                        PKG_PAB_CONSTANTES.v_COD_TIp_ING_MAS,
                        PKG_PAB_CONSTANTES.v_STR_TIp_ING_MAS,
                        PKG_PAB_CONSTANTES.V_COD_TIP_ING_UNI,
                        PKG_PAB_CONSTANTES.V_STR_TIP_ING_UNI,
                        PKG_PAB_CONSTANTES.V_COD_TIP_ING_DIR,
                        PKG_PAB_CONSTANTES.V_STR_TIP_ING_DIR) AS TIPO_INGRESO,
                 NULL AS FECHA_SWIFT,
                 NULL AS HORA_SWIFT,
                 PKG_PAB_CONSTANTES.v_STR_FLG_MRD_MN AS MERCADO,
                 DETOPETI.GLS_ADC_EST AS GLOSA_ESTADO, --DECODE(DETOPETH.COD_EST_AOS,
                 --       PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEREC,
                 --       DETOPCTH.GLS_EST_RCH,
                 --       PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPVI,
                 --       DETOPCTH.GLS_MTv_VSD,
                 --       DETOPETH.GLS_ADC_EST) AS GLOSA_ESTADO,
                 '' AS DOC_BENEFICIARIO_OPC, -- DETOPCTH.NUM_DOC_BFC AS DOC_BENEFICIARIO_OPC,
                 '' AS TIPO_DOC_BENEFICIARIO_OPC, -- DETOPCTH.COD_TPD_BFC AS TIPO_DOC_BENEFICIARIO_OPC,
                 '' AS CODIGO_FONDO, -- DETOPCTH.COD_TPO_FND AS CODIGO_FONDO,
                 '' AS NUMERO_IDENTIFICATORIO,-- DETOPCTH.NUM_CLv_IDF AS NUMERO_IDENTIFICATORIO,
                 '' AS GLOSA_RECHAZO,-- DETOPCTH.GLS_EST_RCH AS GLOSA_RECHAZO,
                 '' AS GLOSA_VISADA,-- DETOPCTH.GLS_MTv_VSD AS GLOSA_VISADA,
                 '' AS GPI
            FROM PABS_DT_DETLL_OPRCN_INTER DETOPETI,
                 PABS_DT_ESTDO_ALMOT       ESTALM,
                 PABS_DT_SISTM_ENTRD_SALID ENT,
                 PABS_DT_SISTM_ENTRD_SALID SAL
           WHERE DETOPETI.NUM_FOL_OPE_ITR = p_NUM_FOL_OPE
             AND DETOPETI.FEC_ISR_OPE_ITR = p_FEC_ISR_OPE
             AND DETOPETI.COD_EST_AOS = ESTALM.COD_EST_AOS
             AND DETOPETI.COD_SIS_ENT_ITR = ENT.COD_SIS_ENT_SAL
             AND DETOPETI.COD_SIS_SAL_ITR = SAL.COD_SIS_ENT_SAL(+);
      ELSE
        RAISE ERR_COD_INEX;
    END CASE;
    p_ERROR := PKG_PAB_CONSTANTES.v_OK; --Procedimiento ejecutado de forma correcta
  EXCEPTION
    WHEN ERR_COD_INEX THEN
      err_msg := 'Se esta invocando con codigo historificacion inexistente:' ||
                 p_TIP_BUS_HIST;
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(NULL,
                                         v_NOM_PCK,
                                         err_msg,
                                         v_NOM_SP);
    WHEN NO_DATA_FOUND THEN
      err_code := SQLCODE;
      err_msg  := SUBSTR(SQLERRM, 1, 300);
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                         v_NOM_PCK,
                                         err_msg,
                                         v_NOM_SP);
      p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
    WHEN OTHERS THEN
      err_code := SQLCODE;
      err_msg  := SUBSTR(SQLERRM, 1, 300);
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                         v_NOM_PCK,
                                         err_msg,
                                         v_NOM_SP);
      p_ERROR     := PKG_PAB_CONSTANTES.v_ERROR;
      p_s_mensaje := err_code || '-' || err_msg;
      RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);
  END Sp_PAB_BUS_DET_OPE_CON;

  /************************************************************************************************

  -- Funcion/Procedimiento: Sp_PAB_BUS_DET_OPE_REF
  -- Objetivo: Procedimiento almacenado que retorna el detalle de una operaci?n seleccionada
  -- Sistema: DBO_PAB.
  -- Base de Datos: DBO_PAB.
  -- Tablas Usadas: PABS_DT_DETLL_OPRCN, PABS_DT_OPRCN_INFCN_OPCON
  --          PABS_DT_ESTDO_ALMOT, PABS_DT_SISTM_ENTRD_SALID
  -- Fecha: 02/06/16
  -- Autor: Santander
  -- Input:   p_NUM_FOL_OPE : Numero de folio de la operación
  --          p_NUM_REF_SWF: Numero de referencia de una operacion swfit
  --          p_FLG_EGR_ING: Flag de egreso o ingreso
  -- Output:
  --        p_CURSOR : SYS_REFCURSOR, resultados de consulta.
  --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
  -- Input/Output: N/A
  -- Retorno: N/A.
  -- Observaciones: <Fecha y Detalle de ultimos cambios>
  --***********************************************************************************************/
  PROCEDURE Sp_PAB_BUS_DET_OPE_REF(p_NUM_FOL_OPE IN NUMBER,
                                   p_NUM_REF_SWF IN CHAR,
                                   p_FLG_EGR_ING IN NUMBER,
                                   p_CURSOR      OUT SYS_REFCURSOR,
                                   p_ERROR       OUT NUMBER) IS
    v_NOM_SP VARCHAR2(30) := 'Sp_PAB_BUS_DET_OPE_REF';
    v_cuaCer CONSTANT CHAR(4) := '0000';
    v_NUM_REF_SWF char(16);
    v_NUM_FOL_OPE NUMBER;

  BEGIN

    v_NUM_REF_SWF := p_NUM_REF_SWF;

    --Integracion no envia nulos por lo que el 0 representa un nulo
    IF (p_NUM_FOL_OPE > 0) THEN
      v_NUM_FOL_OPE := p_NUM_FOL_OPE;
    END IF;

    OPEN p_CURSOR FOR
      SELECT ESTALM.DSC_EST_AOS AS ESTADO,
             DETOPE.FEC_ISR_OPE AS FEC_INS,
             DETOPE.NUM_FOL_OPE AS NUM_OPE,
             DETOPE.NUM_FOL_NMN AS NUM_NOM,
             DETOPE.NUM_FOL_OPE_ORG AS NUM_OPE_ORG,
             DETOPE.NUM_OPE_SIS_ENT AS NUM_OPE_ENT,
             DECODE(DETOPE.FLG_EGR_ING,
                    PKG_PAB_CONSTANTES.v_FLG_EGR,
                    PKG_PAB_CONSTANTES.v_STR_FLG_EGR,
                    PKG_PAB_CONSTANTES.v_STR_FLG_ING) AS EGRESO_INGRESO,
             DETOPE.COD_SIS_ENT AS COD_CANAL_ENT,
             ENT.DSC_SIS_ENT_SAL AS CANAL_ENTRADA,
             DETOPE.COD_SIS_SAL AS COD_CANAL_SAL,
             SAL.DSC_SIS_ENT_SAL AS CANAL_SALIDA,
             DECODE(DETOPE.COD_TPO_ING,
                    PKG_PAB_CONSTANTES.v_COD_TIp_ING_MAS,
                    PKG_PAB_CONSTANTES.v_STR_TIp_ING_MAS,
                    PKG_PAB_CONSTANTES.V_COD_TIP_ING_UNI,
                    PKG_PAB_CONSTANTES.V_STR_TIP_ING_UNI,
                    PKG_PAB_CONSTANTES.V_COD_TIP_ING_DIR,
                    PKG_PAB_CONSTANTES.V_STR_TIP_ING_DIR) AS TIPO_INGRESO,
             DETOPE.FEC_ENv_RCp_SWF AS FECHA_SWIFT,
             DETOPE.HOR_ENv_RCp_SWF AS HORA_SWIFT,
             DETOPE.COD_MT_SWF AS COD_MT,
             DECODE(DETOPE.FLG_MRD_UTZ_SWF,
                    PKG_PAB_CONSTANTES.v_FLG_MRD_MN,
                    PKG_PAB_CONSTANTES.v_STR_FLG_MRD_MN,
                    PKG_PAB_CONSTANTES.v_STR_FLG_MRD_MX) AS MERCADO,
             DETOPE.COD_TPO_OPE_AOS AS TIPO_OPERACION,
             DETOPE.FEC_VTA AS FECHA_PAGO,
             DETOPE.COD_DVI AS DIVISA,
             DETOPE.IMp_OPE AS MONTO_TOTAL,
             DETOPE.COD_BCO_DTN AS BIC_DESTINO,
             DETOPE.COD_BCO_ORG AS BIC_ORIGEN,
             DETOPE.NUM_DOC_BFC AS DOC_BENEFICIARIO,
             DETOPE.COD_TPD_BFC AS TIPO_DOC_BENEFICIARIO,
             DETOPE.NOM_BFC AS BENEFICIARIO,
             DETOPE.NUM_CTA_BFC AS CTA_BENEFICIARIO,
             DETOPE.COD_SUC AS COD_SUCURSAL,
             DETOPE.NUM_REF_SWF AS REF_SWIFT,
             DETOPE.NUM_REF_EXT AS REF_EXT_SWIFT,
             DECODE(DETOPE.COD_EST_AOS,
                    PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEREC,
                    DETOPC.GLS_EST_RCH,
                    PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPVI,
                    DETOPC.GLS_MTv_VSD,
                    DETOPE.GLS_ADC_EST) AS GLOSA_ESTADO,
             PKG_PAB_CONSTANTES.v_COD_ENT_BAN_SANT || v_cuaCer ||
             DETOPE.NUM_REF_CTB AS REF_CONTABLE,
             DETOPC.COD_BCO_BFC AS BIC_BENEFICIARIO,
             DETOPC.COD_BCO_ITD AS BIC_INTERMEDIARIO,
             DETOPC.NUM_DOC_BFC AS DOC_BENEFICIARIO_OPC,
             DETOPC.COD_TPD_BFC AS TIPO_DOC_BENEFICIARIO_OPC,
             DETOPC.NUM_CTA_DCv_BFC AS CTA_DCv_BENEFICIARIO,
             DETOPC.GLS_DIR_BFC AS DIRECCION_BENEFICIARIO,
             DETOPC.NOM_CDD_BFC AS CIUDAD_BENEFICIARIO,
             DETOPC.COD_PAS_BFC AS PAIS_BENEFICIARIO,
             DETOPC.NOM_ODN AS NOMBRE_ORDENANTE,
             DETOPC.NUM_DOC_ODN AS DOC_ORDENANTE,
             DETOPC.COD_TPD_ODN AS TIPO_DOC_ORDENANTE,
             DETOPC.NUM_CTA_ODN AS CTA_ORDENANTE,
             DETOPC.NUM_CTA_DCv_ODN AS CTA_DCv_ORDENANTE,
             DETOPC.GLS_DIR_ODN AS DIRECCION_ORDENANTE,
             DETOPC.NOM_CDD_ODN AS CIUDAD_ORDENANTE,
             DETOPC.COD_PAS_ODN AS PAIS_ORDENANTE,
             DETOPC.NUM_CLv_NGC AS CLAVE_NEGOCIO,
             DETOPC.NUM_AGT AS NUM_AGENTE,
             DETOPC.COD_FND_CCLV AS FONDO_CCLV,
             DETOPC.COD_TPO_SDO AS TIPO_SALDO,
             DETOPC.COD_TPO_CMA AS TIPO_CAMARA,
             DETOPC.COD_TPO_FND AS CODIGO_FONDO,
             DETOPC.FEC_TPO_OPE_CCLV AS FECHA_CCLV,
             DETOPC.NUM_CLv_IDF AS NUMERO_IDENTIFICATORIO,
             DETOPC.GLS_EST_RCH AS GLOSA_RECHAZO,
             DETOPC.GLS_MTv_VSD AS GLOSA_VISADA,
             DETOPC.OBS_OPC_SWF AS OBSERVACION_SWIFT,
             TO_CHAR(SYSDATE, 'HH24MI') AS INDHOR,
             DETOPE.NOM_BFC AS NOMBRE_ORI,
             DETOPE.COD_BCO_ORG AS CUENTA_ORI,
             DETOPE.NUM_REF_SWF AS REF_SWF_ORI,
             DETOPE.NUM_DOC_BFC AS RUT_ORI,
             DETOPE.IMP_OPE AS MONTO_ORI,
             DETOPE.COD_DVI AS MONEDA_ORI,
             DETOPE.COD_MT_SWF AS MT_SWIFT_ORI
        FROM PABS_DT_DETLL_OPRCN       DETOPE,
             PABS_DT_OPRCN_INFCN_OPCON DETOPC,
             PABS_DT_ESTDO_ALMOT       ESTALM,
             PABS_DT_SISTM_ENTRD_SALID ENT,
             PABS_DT_SISTM_ENTRD_SALID SAL
       WHERE DETOPE.NUM_FOL_OPE = NVL(v_NUM_FOL_OPE, DETOPE.NUM_FOL_OPE)
         AND DETOPE.NUM_REF_SWF = NVL(v_NUM_REF_SWF, DETOPE.NUM_REF_SWF)
         AND DETOPE.NUM_FOL_OPE = DETOPC.NUM_FOL_OPE(+)
         AND DETOPE.FEC_ISR_OPE = DETOPC.FEC_ISR_OPE(+)
         AND DETOPE.COD_EST_AOS = ESTALM.COD_EST_AOS(+)
         AND DETOPE.COD_SIS_ENT = ENT.COD_SIS_ENT_SAL(+)
         AND DETOPE.COD_SIS_SAL = SAL.COD_SIS_ENT_SAL(+)
         AND DETOPE.FLG_EGR_ING = p_FLG_EGR_ING; --PKG_PAB_CONSTANTES.V_FLG_EGR;

    p_ERROR := PKG_PAB_CONSTANTES.v_OK; --Procedimiento ejecutado de forma correcta

  EXCEPTION

    WHEN NO_DATA_FOUND THEN

      err_code := SQLCODE;
      err_msg  := SUBSTR(SQLERRM, 1, 300);
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                         v_NOM_PCK,
                                         err_msg,
                                         v_NOM_SP);
      p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;

    WHEN OTHERS THEN

      err_code    := SQLCODE;
      err_msg     := SUBSTR(SQLERRM, 1, 300);
      p_s_mensaje := err_code || '-' || err_msg;
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                         v_NOM_PCK,
                                         err_msg,
                                         v_NOM_SP);
      p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);
  END Sp_PAB_BUS_DET_OPE_REF;

  -- ***********************************************************************************************
  -- Funcion/Procedimiento: Sp_PAB_GES_VIS_OPE
  -- Objetivo: Procedimiento que recorre listado de operaciones a visar
  -- Sistema: PAB
  -- Base de Datos: DGBMSEGDB01_NGALTM
  -- Tablas Usadas: N/A
  -- Fecha: 21/09/16
  -- Autor: Santander
  -- Input:
  -- p_REG_OPER -> parametro tipo registro que contiene el numero, fecha operacion y n?mina
  -- p_COD_USR -> rut usuario
  -- p_GLS_MTV_EST -> motivo o glosa de eliminacion
  -- Output:
  -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
  -- Input/Output: N/A
  -- Retorno: N/A.
  -- Observaciones: <Fecha y Detalle de ultimos cambios>
  -- ***********************************************************************************************
  PROCEDURE Sp_PAB_GES_VIS_OPE(p_REG_OPER    IN REG_OPER,
                               p_COD_USR     IN VARCHAR2,
                               p_GLS_MTV_EST IN VARCHAR2,
                               p_ERROR       OUT NUMBER) IS

    v_NOM_SP      VARCHAR2(31) := 'Sp_PAB_GES_VIS_OPE';
    v_FEC_ISR_OPE date;

  BEGIN

    p_ERROR := PKG_PAB_CONSTANTES.v_OK;

    IF p_REG_OPER.COUNT > 0 THEN

      FOR i IN p_REG_OPER.FIRST .. p_REG_OPER.LAST LOOP

        PKG_PAB_OPERACION.Sp_PAB_ACT_VIS_OPE(p_REG_OPER   (i).FEC_ISR_OPE,
                                             p_REG_OPER   (i).NUM_FOL_OPE,
                                             p_REG_OPER   (i).NUM_FOL_NMN,
                                             p_COD_USR,
                                             p_GLS_MTV_EST,
                                             p_ERROR);
        p_ERROR := PKG_PAB_CONSTANTES.v_OK;

      END LOOP;

    END IF;

    COMMIT;

  EXCEPTION
    WHEN OTHERS THEN
      err_code := SQLCODE;
      err_msg  := SUBSTR(SQLERRM, 1, 300);
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                         v_NOM_PCK,
                                         err_msg,
                                         v_NOM_SP);
      p_ERROR     := PKG_PAB_CONSTANTES.v_ERROR;
      p_s_mensaje := err_code || '-' || err_msg;
      RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);

  END Sp_PAB_GES_VIS_OPE;

  -- ***********************************************************************************************
  -- Funcion/Procedimiento: Sp_PAB_ACT_VIS_OPE
  -- Objetivo: Procedimiento que actualiza el estado de una operacion desde Por Visar a Autorizada
  -- Sistema: PAB
  -- Base de Datos: DGBMSEGDB01_NGALTM
  -- Tablas Usadas: PABS_DT_DETLL_NOMNA, PABS_DT_DETLL_OPRCN, PABS_DT_OPRCN_INFCN_OPCON, PABS_DT_NOMNA_INFCN_OPCON
  -- Fecha: 01/10/16
  -- Autor: Santander
  -- Input:
  -- p_FEC_ISR_OPE -> Fecha de ingreso operacion
  -- p_NUM_FOL_OPE -> Numero de operacion
  -- p_NUM_FOL_NMN -> foliador del banco
  -- p_COD_USR -> rut usuario
  -- p_GLS_MTV_EST -> motivo o glosa de visacion
  -- Output:
  -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
  -- Input/Output: N/A
  -- Retorno: N/A.
  -- Observaciones: <Fecha y Detalle de ultimos cambios>
  -- ************************************************************************************************
  PROCEDURE Sp_PAB_ACT_VIS_OPE(p_FEC_ISR_OPE IN DATE,
                               p_NUM_FOL_OPE IN NUMBER,
                               p_NUM_FOL_NMN IN NUMBER,
                               p_COD_USR     IN VARCHAR2,
                               p_GLS_MTV_EST IN VARCHAR2,
                               p_ERROR       OUT NUMBER) IS

    --Seteo de Variables
    V_NOM_SP VARCHAR2(30) := 'Sp_PAB_ACT_VIS_OPE';

  BEGIN

    P_ERROR := PKG_PAB_CONSTANTES.V_OK;

    --Si viene del mundo operacion se debe modificar las tablas de operacion y de nomina

    IF (NVL(p_NUM_FOL_NMN, 0) <> 0) THEN

      -- Se debe distinguir si la operacion proviene del mundo directo

      BEGIN
        --Actualiza el estado de la operacion perteneciente a una nomina a autorizada
        UPDATE PABS_DT_DETLL_NOMNA
           SET COD_EST_AOS = PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT,
               GLS_ADC_EST = NULL,
               FEC_ACT_NMN = SYSDATE -- Fecha actualización para dashborad
         WHERE NUM_FOL_OPE = p_NUM_FOL_OPE
           AND FEC_ISR_OPE = p_FEC_ISR_OPE
           AND NUM_FOL_NMN = p_NUM_FOL_NMN;

      EXCEPTION
        WHEN OTHERS THEN
          err_code := SQLCODE;
          err_msg  := SUBSTR(SQLERRM, 1, 300);
          PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
          p_ERROR     := PKG_PAB_CONSTANTES.v_ERROR;
          p_s_mensaje := err_code || '-' || err_msg;
          RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);

      END;

      --Indicamos el motivo por el cual fue visado
      BEGIN
        UPDATE PABS_DT_NOMNA_INFCN_OPCON
           SET GLS_MTV_VSD = p_GLS_MTV_EST
         WHERE NUM_FOL_OPE = p_NUM_FOL_OPE
           AND FEC_ISR_OPE = p_FEC_ISR_OPE;

      EXCEPTION
        WHEN OTHERS THEN
          err_code := SQLCODE;
          err_msg  := SUBSTR(SQLERRM, 1, 300);
          PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
          p_ERROR     := PKG_PAB_CONSTANTES.v_ERROR;
          p_s_mensaje := err_code || '-' || err_msg;
          RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);
      END;

    END IF;

    --Actualiza el estado de la Operacion
    BEGIN
      UPDATE PABS_DT_DETLL_OPRCN
         SET COD_EST_AOS = PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT,
             GLS_ADC_EST = CASE WHEN GLS_ADC_EST = 'Operación de origen AGP' THEN GLS_ADC_EST ELSE NULL END,
             FEC_ACT_OPE = SYSDATE -- Fecha actualización para dashborad
       WHERE NUM_FOL_OPE = p_NUM_FOL_OPE
         AND FEC_ISR_OPE = p_FEC_ISR_OPE;

    EXCEPTION
      WHEN OTHERS THEN
        err_code := SQLCODE;
        err_msg  := SUBSTR(SQLERRM, 1, 300);
        PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                           v_NOM_PCK,
                                           err_msg,
                                           v_NOM_SP);
        p_ERROR     := PKG_PAB_CONSTANTES.v_ERROR;
        p_s_mensaje := err_code || '-' || err_msg;
        RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);
    END;

    --Indicamos el motivo por el cual fue visado
    BEGIN
      UPDATE PABS_DT_OPRCN_INFCN_OPCON
         SET GLS_MTV_VSD = p_GLS_MTV_EST
       WHERE NUM_FOL_OPE = p_NUM_FOL_OPE
         AND FEC_ISR_OPE = p_FEC_ISR_OPE;

    EXCEPTION
      WHEN OTHERS THEN
        err_code := SQLCODE;
        err_msg  := SUBSTR(SQLERRM, 1, 300);
        PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                           v_NOM_PCK,
                                           err_msg,
                                           v_NOM_SP);
        p_ERROR     := PKG_PAB_CONSTANTES.v_ERROR;
        p_s_mensaje := err_code || '-' || err_msg;
        RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);
    END;

    --Se llama al procedimiento de bitacora para registrar el usuario que modifica el estado de la operacion
    PKG_PAB_TRACKING.Sp_PAB_INS_BIT_OPE(p_NUM_FOL_OPE,
                                        p_FEC_ISR_OPE,
                                        p_COD_USR,
                                        PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPVI,
                                        PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT,
                                        p_GLS_MTV_EST,
                                        p_ERROR);

  EXCEPTION
    WHEN DUP_VAL_ON_INDEX THEN
      err_code := SQLCODE;
      err_msg  := SUBSTR(SQLERRM, 1, 300);
      P_ERROR  := PKG_PAB_CONSTANTES.V_ERROR;
      PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD(err_code,
                                         V_NOM_PCK,
                                         err_msg,
                                         V_NOM_SP);
    WHEN OTHERS THEN
      err_code := SQLCODE;
      err_msg  := SUBSTR(SQLERRM, 1, 300);
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                         v_NOM_PCK,
                                         err_msg,
                                         v_NOM_SP);
      p_ERROR     := PKG_PAB_CONSTANTES.v_ERROR;
      p_s_mensaje := err_code || '-' || err_msg;
      RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);

  END Sp_PAB_ACT_VIS_OPE;

  -- ***********************************************************************************************
  -- Funcion/Procedimiento: Sp_PAB_CON_OPE
  -- Objetivo: Procedimiento que consulta y lista las operaciones que se deben visar
  -- Sistema: PAB
  -- Base de Datos: DGBMSEGDB01_NGALTM
  -- Tablas Usadas: PABS_DT_DETLL_NOMNA, PABS_DT_DETLL_OPRCN
  -- Fecha: 27/10/16
  -- Autor: Santander
  -- Input:
  -- p_COD_EST_AOS -> Estado de la operacion Por Visar
  -- p_COD_SIS_SAL -> Codigo del sistema de salida
  -- p_FECHA_DES -> Fecha ingreso operacion para filtro desde
  -- p_FECHA_HAS -> Fecha ingreso operacion para filtro hasta
  -- Output:
  -- p_CURSOR -> Cursor con las nominas encontradas
  -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
  -- Input/Output: N/A
  -- Retorno: N/A.
  -- Observaciones: <Fecha y Detalle de ultimos cambios>
  -- ***********************************************************************************************
  PROCEDURE Sp_PAB_CON_OPE(p_COD_EST_AOS IN CHAR,
                           p_COD_SIS_SAL IN CHAR,
                           p_FECHA_DES   IN DATE,
                           p_FECHA_HAS   IN DATE,
                           p_CURSOR      OUT SYS_REFCURSOR,
                           p_ERROR       OUT NUMBER) IS

    --Seteo de Variables
    V_NOM_SP    VARCHAR2(30) := 'Sp_PAB_CON_OPE';
    V_COD_EST   NUMBER(5) := PKG_PAB_UTILITY.FN_PAB_OBT_COD_DSC_EST(p_COD_EST_AOS,
                                                                    PKG_PAB_CONSTANTES.V_IND_OPE);
    v_FECHA_DES DATE := SYSDATE;
    v_FECHA_HAS DATE := SYSDATE;

  BEGIN

    P_ERROR := PKG_PAB_CONSTANTES.V_OK;

    IF (p_FECHA_DES IS NOT NULL) THEN
      v_FECHA_DES := p_FECHA_DES;
    END IF;

    IF (p_FECHA_HAS IS NOT NULL) THEN
      v_FECHA_HAS := p_FECHA_HAS;
    END IF;

    OPEN p_CURSOR FOR
      SELECT OPD.FEC_ISR_OPE,
             OPD.NUM_FOL_OPE AS Nro_Operacion,
             OPD.NOM_BFC AS Nombre_Beneficiario,
             OPD.NUM_FOL_NMN AS Numero_nomina,
             OPD.NUM_FOL_OPE_ORG AS NUM_FOL_OPE_ORG,
             OPD.NUM_OPE_SIS_ENT AS NUM_OPE_SIS_ENT,
             OPD.FLG_EGR_ING AS FLG_EGR_ING,
             DECODE(OPD.FLG_EGR_ING,
                    PKG_PAB_CONSTANTES.v_FLG_EGR,
                    PKG_PAB_CONSTANTES.v_STR_FLG_EGR,
                    PKG_PAB_CONSTANTES.v_STR_FLG_ING) AS DSC_EGR_ING,
             OPD.COD_SIS_SAL AS COD_SIS_SAL,
             PKG_PAB_UTILITY.FN_PAB_OBT_DSC_SIS_ENT_SAL(OPD.COD_SIS_SAL) AS DSC_SAL_DET,
             OPD.COD_SIS_ENT AS COD_SIS_ENT,
             PKG_PAB_UTILITY.FN_PAB_OBT_DSC_SIS_ENT_SAL(OPD.COD_SIS_ENT) AS Canal_Origen,
             OPD.COD_EST_AOS AS COD_EST_AOS,
             PKG_PAB_UTILITY.FN_PAB_OBT_DSC_COD_EST(OPD.COD_EST_AOS) AS DSC_EST_AOS,
             OPD.COD_TPO_ING AS COD_TPO_ING,
             PKG_PAB_UTILITY.FN_PAB_OBT_TIP_ING(OPD.COD_TPO_ING) AS DSC_TPO_ING,
             OPD.FEC_ENV_RCP_SWF AS FEC_ENV_RCP_SWF,
             OPD.HOR_ENV_RCP_SWF AS HOR_ENV_RCP_SWF,
             OPD.COD_MT_SWF AS MT_SWIFT,
             OPD.FLG_MRD_UTZ_SWF AS FLG_MRD_UTZ_SWF,
             DECODE(OPD.FLG_MRD_UTZ_SWF,
                    PKG_PAB_CONSTANTES.v_FLG_MRD_MN,
                    PKG_PAB_CONSTANTES.v_STR_FLG_MRD_MN,
                    PKG_PAB_CONSTANTES.v_STR_FLG_MRD_MX) AS DSC_MRD_UTZ_SWF,
             OPD.COD_TPO_OPE_AOS AS Tipo_Operacion,
             PKG_PAB_UTILITY.FN_PAB_OBT_DSC_TIP_OPE(OPD.COD_TPO_OPE_AOS) AS DSC_TPO_OPE_AOS,
             OPD.FEC_VTA AS Fecha_Valuta,
             OPD.COD_DVI AS Moneda,
             OPD.IMP_OPE AS Monto,
             OPD.COD_BCO_DTN AS COD_BCO_DTN,
             PKG_PAB_UTILITY.FN_PAB_OBT_DSC_BCO(OPD.COD_BCO_DTN) AS Banco_Destino,
             DECODE(TRIM(OPI.NUM_DOC_BFC),
                    NULL,
                    TRIM(OPI.NUM_DOC_BFC) ||
                    DECODE(OPI.NUM_DOC_BFC, NULL, NULL, OPI.COD_TPD_BFC),
                    TRIM(OPI.NUM_DOC_BFC) || '-' ||
                    DECODE(OPI.NUM_DOC_BFC, NULL, NULL, OPI.COD_TPD_BFC)) AS Rut_Beneficiario,
             OPD.NUM_CTA_BFC AS Cta_Beneficiario,
             OPD.COD_SUC AS Sucursal,
             OPD.NUM_REF_SWF AS NUM_REF_SWF,
             OPD.NUM_REF_EXT AS NUM_REF_EXT,
             OPD.GLS_ADC_EST AS Motivo,
             OPD.NUM_REF_CTB AS NUM_REF_CTB,
             ----------------    PABS_DT_NOMNA_INFCN_OPCON   -------------------------------
             OPI.OBS_OPC_SWF AS OBS_OPC_SWF,
             OPI.COD_BCO_BFC AS COD_BCO_BFC,
             OPI.COD_BCO_ITD AS COD_BCO_ITD,
             PKG_PAB_UTILITY.FN_PAB_OBT_DSC_BCO(OPI.COD_BCO_ITD) AS DSC_BCO_ITD,
             OPI.NUM_DOC_BFC AS NUM_DOC_BFC,
             OPI.COD_TPD_BFC AS COD_TPD_BFC,
             OPI.NUM_CTA_DCV_BFC AS NUM_CTA_DCV_BFC,
             OPI.GLS_DIR_BFC AS GLS_DIR_BFC,
             OPI.NOM_CDD_BFC AS NOM_CDD_BFC,
             OPI.COD_PAS_BFC AS COD_PAS_BFC,
             PKG_PAB_UTILITY.Fn_PAB_COMBO_PAIS(OPI.COD_PAS_BFC) AS DSC_PAS_BFC,
             OPI.NOM_ODN AS Nombre_Ordenante,
             DECODE(TRIM(OPI.NUM_DOC_ODN),
                    NULL,
                    TRIM(OPI.NUM_DOC_ODN) ||
                    DECODE(OPI.NUM_DOC_ODN, NULL, NULL, OPI.COD_TPD_ODN),
                    TRIM(OPI.NUM_DOC_ODN) || '-' ||
                    DECODE(OPI.NUM_DOC_ODN, NULL, NULL, OPI.COD_TPD_ODN)) AS Rut_Ordenante,
             OPI.NUM_CTA_ODN AS Cta_Ordenante,
             OPI.NUM_CTA_DCV_ODN AS NUM_CTA_DCV_ODN,
             OPI.GLS_DIR_ODN AS GLS_DIR_ODN,
             OPI.NOM_CDD_ODN AS NOM_CDD_ODN,
             OPI.COD_PAS_ODN AS COD_PAS_ODN,
             PKG_PAB_UTILITY.Fn_PAB_COMBO_PAIS(OPI.COD_PAS_ODN) AS DSC_PAS_ODN,
             OPI.NUM_CLV_NGC AS NUM_CLV_NGC,
             OPI.NUM_AGT AS NUM_AGT,
             OPI.COD_FND_CCLV AS COD_FND_CCLV,
             OPI.COD_TPO_SDO AS COD_TPO_SDO,
             PKG_PAB_UTILITY.Fn_PAB_COMBO_TSALDO(OPI.COD_TPO_SDO) AS DSC_TPO_SDO,
             OPI.COD_TPO_CMA AS COD_TPO_CMA,
             PKG_PAB_UTILITY.Fn_PAB_COMBO_CAMARA(OPI.COD_TPO_CMA) AS DSC_TPO_CMA,
             OPI.COD_TPO_FND AS COD_TPO_FND,
             PKG_PAB_UTILITY.FN_PAB_TRAN_COD_FONDO(OPI.COD_TPO_FND) AS DSC_TPO_FND,
             DECODE(OPI.FEC_TPO_OPE_CCLV, '01-01-3000', NULL) AS FEC_TPO_OPE_CCLV,
             OPI.NUM_CLV_IDF AS NUM_CLV_IDF,
             OPI.GLS_EST_RCH AS GLS_EST_RCH,
             OPI.GLS_MTV_VSD AS GLS_MTV_VSD
        FROM PABS_DT_DETLL_OPRCN OPD, PABS_DT_NOMNA_INFCN_OPCON OPI
       WHERE OPD.COD_EST_AOS = v_COD_EST
         AND OPD.COD_SIS_SAL = NVL(p_COD_SIS_SAL, OPD.COD_SIS_SAL)
         AND TRUNC(OPD.FEC_ISR_OPE) BETWEEN TRUNC(v_FECHA_DES) AND
             TRUNC(v_FECHA_HAS)
         AND OPD.NUM_FOL_OPE = OPI.NUM_FOL_OPE(+)
         AND OPD.FEC_ISR_OPE = OPI.FEC_ISR_OPE(+)
         ORDER BY OPD.NUM_FOL_OPE ASC;
     --    AND OPD.NUM_FOL_OPE = OPI.NUM_FOL_OPE(+)
      --   AND OPD.FEC_ISR_OPE = OPI.FEC_ISR_OPE(+)
     --  ORDER BY OPD.NUM_FOL_OPE ASC;

  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      err_code := SQLCODE;
      err_msg  := SUBSTR(SQLERRM, 1, 300) ||
                  'Error en consulta operaciones por visar' ||
                  p_COD_SIS_SAL;
      p_ERROR  := PKG_PAB_CONSTANTES.v_ERROR;
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                         v_NOM_PCK,
                                         err_msg,
                                         v_NOM_SP);
    WHEN OTHERS THEN
      err_code    := SQLCODE;
      err_msg     := SUBSTR(SQLERRM, 1, 300);
      p_s_mensaje := err_code || '-' || err_msg;
      p_ERROR     := PKG_PAB_CONSTANTES.v_ERROR;
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                         v_NOM_PCK,
                                         err_msg,
                                         v_NOM_SP);
      RAISE_APPLICATION_ERROR(-20000, err_code || p_s_mensaje);

  END Sp_PAB_CON_OPE;

  --************************************************************************************************
  -- Funcion/Procedimiento: Sp_PAB_ACT_EST_GLS_OPE
  -- Objetivo: Procedimiento que actualiza el estado y glosa de una operacion al ser liberada por contingencia.
  -- Sistema: DBO_PAB.
  -- Base de Datos: DBO_PAB.
  -- Tablas Usadas: PABS_DT_DETLL_OPRCN
  -- Fecha: 02/06/16
  -- Autor: Santander
  -- Input:    p_NUM_FOL_NMN->   foliador del banco
  --           p_FEC_ISR_OPE->   Fecha de insercion de la operacion
  --           p_COD_EST_AOS->   Identificador de nuevo estado.
  --           p_GLS_ADC_EST->   Glosa Cambio estado
  --           p_COD_USR->       rut usuario
  -- Output:
  --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
  -- Input/Output: N/A
  -- Retorno: N/A.
  -- Observaciones: <Fecha y Detalle de ultimos cambios>
  --***********************************************************************************************/
  PROCEDURE Sp_PAB_ACT_EST_GLS_OPE(p_NUM_FOL_OPE IN NUMBER,
                                   p_FEC_ISR_OPE IN DATE,
                                   p_COD_EST_AOS IN CHAR,
                                   p_GLS_ADC_EST IN VARCHAR2,
                                   p_COD_USR     IN VARCHAR2,
                                   p_ERROR       OUT NUMBER) IS

    v_NOM_SP          VARCHAR2(30) := 'Sp_PAB_ACT_EST_GLS_OPE';
    v_COD_EST_AOS     NUMBER;
    v_COD_EST_AOS_ACT NUMBER;
    v_RESULT          NUMBER;
    V_GLS_ADC_EST     VARCHAR2(210) := NULL;
    v_DES_BIT         VARCHAR2(300);

  BEGIN

    --Estados
    CASE
      WHEN p_COD_EST_AOS = PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEAUT THEN
        v_COD_EST_AOS := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT; --Autorizada
      WHEN p_COD_EST_AOS = PKG_PAB_CONSTANTES.v_STR_EST_AOS_OPEREC THEN
        v_COD_EST_AOS := PKG_PAB_CONSTANTES.v_COD_EST_AOS_OPEREC; --Rechazada
      WHEN p_COD_EST_AOS = PKG_PAB_CONSTANTES.v_STR_EST_AOS_OPELIB THEN
        v_COD_EST_AOS := PKG_PAB_CONSTANTES.v_COD_EST_AOS_OPELIB; --Liberardas
      WHEN p_COD_EST_AOS = PKG_PAB_CONSTANTES.v_STR_EST_AOS_OPEENV THEN
        v_COD_EST_AOS := PKG_PAB_CONSTANTES.v_COD_EST_AOS_OPEENV; --Enviada
      WHEN p_COD_EST_AOS = PKG_PAB_CONSTANTES.v_STR_EST_AOS_OPEPAG THEN
        v_COD_EST_AOS := PKG_PAB_CONSTANTES.v_COD_EST_AOS_OPEPAG; --Pagada
      WHEN p_COD_EST_AOS = PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEELI THEN
        v_COD_EST_AOS := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEELI; --Eliminada
      WHEN p_COD_EST_AOS = PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEPVI THEN
        v_COD_EST_AOS := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPVI; --Por Visar
      WHEN p_COD_EST_AOS = PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEPAB THEN
        v_COD_EST_AOS := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAB; --Por Abonada
      WHEN p_COD_EST_AOS = PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEABN THEN
        v_COD_EST_AOS := PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEPAS; --Por Asociar
      WHEN p_COD_EST_AOS = PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEPAS THEN
        v_COD_EST_AOS := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEABN; --Abonada
      WHEN p_COD_EST_AOS = PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEDEV THEN
        v_COD_EST_AOS := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDEV; --Abonada
      WHEN p_COD_EST_AOS = PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEASO THEN
        v_COD_EST_AOS := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEASO; --Asociada
    END CASE;

    --Glosa estado
    V_GLS_ADC_EST := NVL(p_GLS_ADC_EST, '');

    --Obtener el estado actual
    PKG_PAB_OPERACION.SP_PAB_OBT_EST_ACT_OPE(p_NUM_FOL_OPE,
                                             p_FEC_ISR_OPE,
                                             v_COD_EST_AOS_ACT);

    --Actualizamos estado nuevo de la operacion.
    V_GLS_ADC_EST := p_GLS_ADC_EST; --'Operacion Pagada por contingencia';
    UPDATE PABS_DT_DETLL_OPRCN
       SET COD_EST_AOS = V_COD_EST_AOS,
           GLS_ADC_EST = V_GLS_ADC_EST,
           FEC_ACT_OPE = SYSDATE -- Fecha actualización para dashborad
     WHERE NUM_FOL_OPE = p_NUM_FOL_OPE
       AND FEC_ISR_OPE = p_FEC_ISR_OPE;

    --Llamamos al procedimiento de Bitacora
    v_DES_BIT := 'Cambio de estado: ' || p_GLS_ADC_EST;
    PKG_PAB_TRACKING.Sp_PAB_INS_BIT_OPE(p_NUM_FOL_OPE,
                                        p_FEC_ISR_OPE,
                                        p_COD_USR,
                                        v_COD_EST_AOS_ACT,
                                        v_COD_EST_AOS,
                                        v_DES_BIT,
                                        v_RESULT);

    --Actualizamos el estado de la operacion en la tabla Nomina.
    PKG_PAB_NOMINA.Sp_PAB_ACT_EST_OPE_NOMINA(p_NUM_FOL_OPE,
                                             p_FEC_ISR_OPE,
                                             v_COD_EST_AOS,
                                             v_GLS_ADC_EST,
                                             v_RESULT);

    p_ERROR := PKG_PAB_CONSTANTES.v_ok;

    --Guardamos
    COMMIT;

  EXCEPTION
    WHEN NO_DATA_FOUND THEN

      err_code := SQLCODE;
      err_msg  := 'No se pudo actualizar estado de operación  al ser liberada por contingencia';
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                         v_NOM_PCK,
                                         err_msg,
                                         v_NOM_SP);
      p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;

    WHEN OTHERS THEN

      err_code    := SQLCODE;
      err_msg     := SUBSTR(SQLERRM, 1, 300);
      p_s_mensaje := err_code || '-' || err_msg;
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                         v_NOM_PCK,
                                         err_msg,
                                         v_NOM_SP);
      p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);
  END Sp_PAB_ACT_EST_GLS_OPE;

  /************************************************************************************************
  -- Funcion/Procedimiento: Sp_PAB_GES_INS_OPE
  -- Objetivo: Procedimiento que inserta una operacion en tabla operacion
  -- Sistema: DBO_PAB.
  -- Base de Datos: DBO_PAB.
  -- Tablas Usadas: PABS_DT_DETLL_OPRCN
  -- Fecha: 02/06/16
  -- Autor: Santander
  -- Input:     p_FEC_ISR_OPE     -> Fecha Insercion (se puede enviar nulo y lo genera el sistema)
  --            p_NUM_FOL_OPE     -> Numero Foliador de la operacion
  --            p_NUM_FOL_NMN     -> Numero Foliador de la nomina
  --            p_NUM_FOL_OPE_ORG -> Numero que tiene el numero original de la operacion relacionada
  --            p_NUM_OPE_SIS_ENT -> Numero de operacion de la operacion en el sistema origen
  --            p_FLG_EGR_ING     -> Flag que indica si es egreso (0) o ingreso (1)
  --            p_COD_SIS_SAL     -> Sistema de salida
  --            p_COD_SIS_ENT     -> Sistema de entrada
  --            p_FLG_EGR_ING     -> Flag que indica si es egreso (0) o ingreso (1)
  --            p_EST_OPE         -> Estado de la operacion (PAS (Por Asociar))
  --            p_COD_MT_SWF      -> Numero del tipo de mensaje swift
  --            p_COD_TPO_OPE_AOS -> Tipo de operacion.
  --            p_FEC_VTA         -> Fecha de vencimiento
  --            p_COD_DVI         -> Codigo de la divisa
  --            p_IMp_OPE         -> Importe de la operacion
  --            p_COD_BCO_DTN     -> BIC del banco destino
  --            p_COD_BCO_ORG     -> BIC Banco Origen
  --            p_FEC_ENV_RCP_SWF -> Fecha Envio/Recepcion Swift
  --            p_HOR_ENV_RCP_SWF -> Hora Envio/Recepcion Swift
  --            p_NUM_REF_SWF     -> Referencia Swfit
  --            p_NUM_REF_EXT     -> Referencia Swift Relacionada
  --            p_NUM_DOC_BFC     -> Numero rut del beneficiario
  --            p_COD_TPD_BFC     -> Digito verificador del Beneficiario
  --            p_NOM_BFC         -> Nombre del Beneficiario
  --            p_NUM_CTA_BFC     -> Numero de cuenta del beneficiario
  --            p_COD_SUC         -> Codigo de la sucursal
  --            p_GLS_ADC_EST     -> Informacion adicional
  --            p_NUM_REF_CTB     -> Numero de referencia contable
  --            p_COD_BCO_BFC     -> BIB Banco Beneficario
  --            p_COD_BCO_ITD     -> BIC Banco Intermediario
  --            p_NUM_CTA_DCv_BFC -> Numero cuenta DCV Beneficiario
  --            p_GLS_DIR_BFC     -> Direccion Beneficario
  --            p_NOM_CDD_BFC     -> Nombre ciudad beneficiario
  --            p_COD_PAS_BFC     -> Codigo pais beneficiario
  --            p_NOM_ODN         -> Nombre ordenante
  --            p_NUM_DOC_ODN     -> Numero rut ordenante
  --            p_COD_TPD_ODN     -> codigo tipo documento ordenante
  --            p_NUM_CTA_ODN     -> Numero cuenta ordenante
  --            p_NUM_CTA_DCv_ODN -> Numero cuenta DCV ordenante
  --            p_GLS_DIR_ODN     -> Direccion ordenante
  --            p_NOM_CDD_ODN     -> Nombre ciudad ordenante
  --            p_COD_PAS_ODN     -> Codigo pais ordenante
  --            p_NUM_CLv_NGC     -> Numero de clave de negocio
  --            p_NUM_AGT         -> Numero de agente
  --            p_COD_FND_CCLV    -> Codigo Fondo (Producto CCLV)
  --            p_COD_TPO_SDO     -> Tipo de saldo
  --            p_COD_TPO_CMA     -> Tipo de camara
  --            p_COD_TPO_FND     -> Tipo de fondo
  --            p_FEC_TPO_OPE_CCLV-> Fecha operacion CCLV
  --            p_NUM_CLv_IDF     -> Numero clave identificatorio
  --            p_OBS_OPC_SWF     -> Observacion Opcional Swift
  --            p_NUM_MVT         -> Numero de Movimiento (Vigente, Cuenta Corriente)
  --            p_COD_USR     -> rut usuario
  -- Output:
  --          p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
  -- Input/Output: N/A
  -- Retorno: N/A.
  -- Observaciones: <Fecha y Detalle de ultimos cambios>
  --***********************************************************************************************/
  PROCEDURE Sp_PAB_GES_INS_OPE(p_FEC_ISR_OPE     IN DATE,
                               p_NUM_FOL_OPE     IN NUMBER,
                               p_NUM_FOL_NMN     IN NUMBER,
                               p_NUM_FOL_OPE_ORG IN NUMBER,
                               p_NUM_OPE_SIS_ENT IN CHAR,
                               p_FLG_EGR_ING     IN NUMBER,
                               p_COD_SIS_SAL     IN CHAR,
                               p_COD_SIS_ENT     IN CHAR,
                               P_COD_TPO_ING     IN NUMBER,
                               p_EST_OPE         IN CHAR,
                               p_COD_MT_SWF      IN NUMBER,
                               p_COD_TPO_OPE_AOS IN CHAR,
                               p_FEC_VTA         IN DATE,
                               p_COD_DVI         IN CHAR,
                               p_IMp_OPE         IN NUMBER,
                               p_COD_BCO_DTN     IN CHAR,
                               p_COD_BCO_ORG     IN CHAR,
                               p_FEC_ENV_RCP_SWF IN DATE,
                               p_HOR_ENV_RCP_SWF IN NUMBER,
                               p_NUM_REF_SWF     IN CHAR,
                               p_NUM_REF_EXT     IN CHAR,
                               ------------Beneficiario------------
                               p_NUM_DOC_BFC IN CHAR,
                               p_COD_TPD_BFC IN CHAR,
                               p_NOM_BFC     IN VARCHAR2,
                               p_NUM_CTA_BFC IN VARCHAR2,
                               -------------------------------------
                               p_COD_SUC     IN VARCHAR2,
                               p_GLS_ADC_EST IN VARCHAR2,
                               p_NUM_REF_CTB IN CHAR,
                               ----------Opcionales--------------
                               p_COD_BCO_BFC      IN CHAR,
                               p_COD_BCO_ITD      IN CHAR,
                               p_NUM_CTA_DCv_BFC  IN VARCHAR2,
                               p_GLS_DIR_BFC      IN VARCHAR2,
                               p_NOM_CDD_BFC      IN VARCHAR2,
                               p_COD_PAS_BFC      IN CHAR,
                               p_NOM_ODN          IN VARCHAR2,
                               p_NUM_DOC_ODN      IN CHAR,
                               p_COD_TPD_ODN      IN CHAR,
                               p_NUM_CTA_ODN      IN VARCHAR2,
                               p_NUM_CTA_DCv_ODN  IN VARCHAR2,
                               p_GLS_DIR_ODN      IN VARCHAR2,
                               p_NOM_CDD_ODN      IN VARCHAR2,
                               p_COD_PAS_ODN      IN CHAR,
                               p_NUM_CLv_NGC      IN VARCHAR2,
                               p_NUM_AGT          IN CHAR,
                               p_COD_FND_CCLV     IN CHAR,
                               p_COD_TPO_SDO      IN CHAR,
                               p_COD_TPO_CMA      IN CHAR,
                               p_COD_TPO_FND      IN VARCHAR2,
                               p_FEC_TPO_OPE_CCLV IN DATE,
                               p_NUM_CLv_IDF      IN VARCHAR2,
                               p_OBS_OPC_SWF      IN VARCHAR2,
                               p_NUM_MVT          IN CHAR,
                               --------------------------------
                               p_COD_USR IN CHAR,
                               p_ERROR   OUT NUMBER) IS
    v_NOM_SP          VARCHAR2(30) := 'Sp_PAB_GES_INS_OPE';
    v_FLG_MRD_UTZ_SWF NUMBER;
    v_FEC_ISR_OPE     DATE := SYSDATE;
    v_COD_EST_OPE     NUMBER;
    V_GLS_ADC_EST     VARCHAR2(210);
    V_NUM_CTA_BFC     VARCHAR2(30);
    v_COD_SIS_SAL     CHAR(10) := NULL;
    v_COD_TPO_OPE_AOS CHAR(8);
    v_cant            NUMBER;
    v_NUM_REF_CTB     VARCHAR2(20);
    v_NUM_REF_SWF     char(16);
    v_NUM_REF_EXT     char(16);
    V_DSC_SIS_ENT_SAL varchar2(50);
    v_TGCDSWSA        char(11);
    v_COD_FLG         NUMBER;

  BEGIN

    err_msg := 'Insertando operación N°:' || p_NUM_FOL_OPE || ' Monto:' ||
               p_IMp_OPE || ' Canal E:' || p_COD_SIS_ENT ||
               ' p_COD_TPO_OPE_AOS:' || p_COD_TPO_OPE_AOS;
    PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                       v_NOM_PCK,
                                       err_msg,
                                       v_NOM_SP);

    p_ERROR       := PKG_PAB_CONSTANTES.v_OK;
    V_NUM_CTA_BFC := P_NUM_CTA_BFC;

    --Calculamos referencia contable (TPN)
    IF (NVL(TO_CHAR(p_NUM_REF_CTB), '#') = '#') THEN
      v_NUM_REF_CTB := LPAD(V_NUM_CTA_BFC, 12, '0');
    ELSE
      v_NUM_REF_CTB := p_NUM_REF_CTB;
    END IF;

    v_NUM_REF_SWF := REPLACE(REPLACE(p_NUM_REF_SWF, CHR(13)), CHR(10));
    v_NUM_REF_EXT := SUBSTR(REPLACE(REPLACE(p_NUM_REF_EXT, CHR(13)), CHR(10)),0,16);

    --Verificamos si viene la fecha de insercion
    IF (NVL(TO_CHAR(p_FEC_ISR_OPE), '#') = '#') THEN
      v_FEC_ISR_OPE := SYSDATE;
    ELSE
      v_FEC_ISR_OPE := p_FEC_ISR_OPE;
    END IF;

    --Calculamos Mercado segun la moneda.
    PKG_PAB_UTILITY.Sp_PAB_CAL_MER(p_COD_DVI, v_FLG_MRD_UTZ_SWF);

    --Seteamos la variable de glosa de estado en caso de venir vacia
    IF (P_GLS_ADC_EST IS NOT NULL AND P_GLS_ADC_EST <> 'CNT-COMBANC') THEN
      V_GLS_ADC_EST := P_GLS_ADC_EST;
    END IF;

    IF (P_GLS_ADC_EST = 'CNT-COMBANC') THEN
      V_GLS_ADC_EST := 'Operación ingresada por contigencia Combanc';
    END IF;

    --Obtenemos el estado al que se quiere pasar
    v_COD_EST_OPE := PKG_PAB_UTILITY.FN_PAB_OBT_COD_DSC_EST(p_EST_OPE,
                                                            PKG_PAB_CONSTANTES.V_IND_OPE);

    --Verificamos si es un ingreso
    IF (p_FLG_EGR_ING = PKG_PAB_CONSTANTES.V_FLG_ING) THEN

      -- Dependiendo del estado es la glosa que dejamos en la bitacora y en la glosa adicional estado
      IF (p_EST_OPE = 'ING') THEN
        v_DES_BIT := 'Se registra operacion en el sistema de Altos Montos';
      END IF;

      PKG_PAB_UTILITY.Sp_PAB_OBT_CNL_CON_TPO_OPE(p_COD_TPO_OPE_AOS,
                                                 v_COD_SIS_SAL,
                                                 V_DSC_SIS_ENT_SAL,
                                                 p_ERROR);

      --Asignamos resultado
      IF (v_COD_SIS_SAL <> PKG_PAB_CONSTANTES.V_STR_TPO_OPE_SIDEN) THEN
        v_COD_TPO_OPE_AOS := REPLACE(p_COD_TPO_OPE_AOS, CHR(13));
      ELSE
        v_COD_TPO_OPE_AOS := PKG_PAB_CONSTANTES.V_STR_TPO_OPE_SIDEN;
        v_COD_SIS_SAL     := NULL;
      END IF;

      err_msg := 'p_COD_TPO_OPE_AOS:' || p_COD_TPO_OPE_AOS ||
                 ' v_COD_SIS_SAL:' || v_COD_SIS_SAL ||
                 ' V_DSC_SIS_ENT_SAL:' || V_DSC_SIS_ENT_SAL;
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                         v_NOM_PCK,
                                         err_msg,
                                         v_NOM_SP);

      --Verificamos si existe el tipo de operacion que fue obtenido desde la integracion
      --            BEGIN
      --
      --                --Existe tipo de operacion la asignamos para guardar
      --                v_COD_TPO_OPE_AOS := REPLACE(p_COD_TPO_OPE_AOS,CHR(13));
      --
      --                --Obtenemos el canal de salida para los mt202
      --                SELECT TIOPE.COD_SIS_ENT_SAL,
      --                       DSC_SIS_ENT_SAL
      --                INTO v_COD_SIS_SAL,
      --                     V_DSC_SIS_ENT_SAL
      --                FROM PABS_DT_TIPO_OPRCN_ALMOT TIOPE,
      --                     PABS_DT_SISTM_ENTRD_SALID SISENT
      --                WHERE TIOPE.COD_TPO_OPE_AOS = v_COD_TPO_OPE_AOS
      --                  AND SISENT.COD_SIS_ENT_SAL = TIOPE.COD_SIS_ENT_SAL;
      --
      --            EXCEPTION
      --                WHEN NO_DATA_FOUND THEN
      --                    --Si no la encuentra es por que no existe o no esta parametrizada
      --                    --Dejamos el tipo de operacion "Sin identicar"
      --                    v_COD_TPO_OPE_AOS := PKG_PAB_CONSTANTES.V_STR_TPO_OPE_SIDEN;
      --                   --No existe el tipo de operacion se deja por defecto
      --                    err_code := SQLCODE;
      --                    err_msg := 'No se encontro información:' || v_COD_TPO_OPE_AOS ;
      --                    PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);

      --                WHEN OTHERS THEN
      --                    err_code := SQLCODE;
      --                    err_msg := SUBSTR(SQLERRM, 1, 300);
      --                    p_s_mensaje := err_code || '-' || err_msg;
      --                    PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
      --                    p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
      --                    RAISE_APPLICATION_ERROR(-20000,p_s_mensaje);
      --            END;

      --Si el pago es un mensaje mt202/205 recibido y es distinto de una devolcuion
      IF ((P_COD_MT_SWF = PKG_PAB_CONSTANTES.V_COD_MT202 OR
         P_COD_MT_SWF = PKG_PAB_CONSTANTES.V_COD_MT205) AND
         v_COD_TPO_OPE_AOS != PKG_PAB_CONSTANTES.V_STR_TPO_OPE_DEVFDOS) THEN

        --Si no la encuentra es por que no existe o no esta parametrizada
        --Si no se encontro el canal de salida
        IF (v_COD_SIS_SAL IS NULL) THEN

          --No se encontro canal de salida de deja x abonar
          v_COD_EST_OPE := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAB;
          v_DES_BIT     := 'No fue posible abonar de forma automatica';
          V_GLS_ADC_EST := 'No fue posible abonar de forma automatica debido a que no pudo obtener el área destino ';

        ELSE
          --Encontro canal de salida de deja abonada
          v_COD_EST_OPE := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEABN;
          V_GLS_ADC_EST := 'Se abono en la Cuenta Contable del area: ' ||
                           V_DSC_SIS_ENT_SAL;
          v_DES_BIT     := V_GLS_ADC_EST;

          --Calculamos el monto de area
          PKG_PAB_CAJA_MN.SP_BUS_CALCULA_AREA_MN(v_COD_SIS_SAL,
                                                 p_IMp_OPE,
                                                 v_cero,
                                                 p_ERROR);

        END IF;

      END IF;

      --Si el pago es un mensaje mt202 y es devolucion
      IF (P_COD_MT_SWF = PKG_PAB_CONSTANTES.V_COD_MT202 AND
         v_COD_TPO_OPE_AOS = PKG_PAB_CONSTANTES.V_STR_TPO_OPE_DEVFDOS) THEN
        v_COD_SIS_SAL     := NULL;
        V_DSC_SIS_ENT_SAL := NULL;

      END IF;

      --Si es un 103
      IF (P_COD_MT_SWF = PKG_PAB_CONSTANTES.V_COD_MT103) THEN

        --Si viene la cuenta en 0 se debe contabilizar contra ooff
        --A solicitud del usuario se deja para abono manual.
        IF (V_NUM_CTA_BFC = '000000000000' OR V_NUM_CTA_BFC = '0') THEN
          --                    v_COD_EST_OPE := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEABN; --Abonada
          --                    v_COD_SIS_SAL := PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SE_OOFF; --OOFF
          --                    V_GLS_ADC_EST := 'Operacion cuenta 000000000000, abonada a Cuenta Contable de Operaciones Financieras';
          --                    v_DES_BIT := 'Operacion cuenta 000000000000, abonada a Cuenta Contable de Operaciones Financieras';
          --                ELSE

          v_COD_SIS_SAL := NULL;
          v_COD_EST_OPE := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAB;

        END IF;

      END IF;

      /************Funcionalidades de la caja *********************************/
      -- Inserta operación en tabla de saldos bilaterales
      PKG_PAB_CAJA_MN.SP_PAB_CALCULA_SALDO_BILATERAL(p_IMp_OPE,
                                                     p_COD_BCO_ORG,
                                                     PKG_PAB_CONSTANTES.V_FLG_ING,
                                                     p_COD_SIS_ENT,
                                                     p_ERROR);

      -- LBTR
      IF (p_COD_SIS_ENT = PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_LBTR) THEN
        --Actualizamos posicion banco central
        PKG_PAB_CAJA_MN.SP_PAB_CAL_SLDO_POS_LBTR(p_IMp_OPE,
                                                 v_cero,
                                                 v_cero,
                                                 v_cero,
                                                 p_ERROR);
      ELSE
        --Combanc
        PKG_PAB_CAJA_MN.SP_PAB_CAL_SLDO_POS_CMN(p_IMp_OPE, v_cero, p_ERROR);
      END IF;

      --Actualizamos los saldos monitor swift
      CASE
        WHEN P_COD_MT_SWF = PKG_PAB_CONSTANTES.V_COD_MT103 THEN
          PKG_PAB_CAJA_MN.SP_PAB_CAL_SLDO_MNT_SWF(p_IMp_OPE,
                                                  v_cero,
                                                  v_cero,
                                                  v_cero,
                                                  v_cero,
                                                  PKG_PAB_CONSTANTES.V_FLG_ING,
                                                  p_ERROR);
        WHEN P_COD_MT_SWF = PKG_PAB_CONSTANTES.V_COD_MT202 THEN
          PKG_PAB_CAJA_MN.SP_PAB_CAL_SLDO_MNT_SWF(v_cero,
                                                  p_IMp_OPE,
                                                  v_cero,
                                                  v_cero,
                                                  v_cero,
                                                  PKG_PAB_CONSTANTES.V_FLG_ING,
                                                  p_ERROR);
        WHEN P_COD_MT_SWF = PKG_PAB_CONSTANTES.V_COD_MT205 THEN
          PKG_PAB_CAJA_MN.SP_PAB_CAL_SLDO_MNT_SWF(v_cero,
                                                  v_cero,
                                                  p_IMp_OPE,
                                                  v_cero,
                                                  v_cero,
                                                  PKG_PAB_CONSTANTES.V_FLG_ING,
                                                  p_ERROR);
      END CASE;

      /************Funcionalidades de la caja *********************************/

    ELSE
      /* LOGICA PARA MT 200 */

      v_COD_TPO_OPE_AOS := p_COD_TPO_OPE_AOS;
      v_COD_SIS_SAL     := p_COD_SIS_SAL;
      v_DES_BIT         := 'Se Ingresa MT200 en el sistema de Altos Montos';

    END IF;

    v_COD_FLG := FN_PAB_BUS_VAL_BIC(P_COD_BCO_ORG);

    IF (v_COD_FLG = PKG_PAB_CONSTANTES.V_FLG_MRD_MX AND P_COD_DVI = PKG_PAB_CONSTANTES.V_STR_DVI_CLP) THEN

    err_code := SQLCODE;
    err_msg  := SUBSTR(SQLERRM, 1, 300) ||
                  ' Operacion No Valida. Banco Extranjero en CLP no se procesa por AAMM -> ' ||
                  P_COD_BCO_ORG;
    PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                         v_NOM_PCK,
                                         err_msg,
                                         v_NOM_SP);
    p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;

    ELSE

        -- SE AGREGA LOGICA DE CONTROL DE CANAL DE SALIDA PARA TODO TIPO DE EGRESOS.

         IF v_COD_SIS_SAL <> PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_CMN AND P_FLG_EGR_ING = PKG_PAB_CONSTANTES.V_FLG_EGR THEN

            IF v_COD_SIS_SAL = PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_SWF THEN

                v_COD_SIS_SAL := PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_SWF;

            ELSE

                v_COD_SIS_SAL := PKG_PAB_UTILITY.FN_VAL_HOR_CMN;

            END IF ;
       END IF;

    PKG_PAB_OPERACION.SP_PAB_INS_OPE(v_FEC_ISR_OPE,
                                     P_NUM_FOL_OPE,
                                     P_NUM_FOL_NMN,
                                     P_NUM_FOL_OPE_ORG,
                                     P_NUM_OPE_SIS_ENT,
                                     P_FLG_EGR_ING,
                                     v_COD_SIS_SAL,
                                     P_COD_SIS_ENT,
                                     v_COD_EST_OPE,
                                     P_COD_TPO_ING,
                                     P_COD_MT_SWF,
                                     v_FLG_MRD_UTZ_SWF,
                                     v_COD_TPO_OPE_AOS,
                                     P_FEC_VTA,
                                     P_COD_DVI,
                                     P_IMP_OPE,
                                     P_COD_BCO_DTN,
                                     P_COD_BCO_ORG,
                                     P_FEC_ENV_RCP_SWF,
                                     P_HOR_ENV_RCP_SWF,
                                     v_NUM_REF_SWF,
                                     v_NUM_REF_EXT,
                                     P_NUM_DOC_BFC,
                                     P_COD_TPD_BFC,
                                     P_NOM_BFC,
                                     V_NUM_CTA_BFC,
                                     P_COD_SUC,
                                     V_GLS_ADC_EST,
                                     v_NUM_REF_CTB,
                                     P_COD_BCO_BFC,
                                     P_COD_BCO_ITD,
                                     P_NUM_CTA_DCV_BFC,
                                     P_GLS_DIR_BFC,
                                     P_NOM_CDD_BFC,
                                     P_COD_PAS_BFC,
                                     P_NOM_ODN,
                                     P_NUM_DOC_ODN,
                                     P_COD_TPD_ODN,
                                     P_NUM_CTA_ODN,
                                     P_NUM_CTA_DCV_ODN,
                                     P_GLS_DIR_ODN,
                                     P_NOM_CDD_ODN,
                                     P_COD_PAS_ODN,
                                     P_NUM_CLV_NGC,
                                     P_NUM_AGT,
                                     P_COD_FND_CCLV,
                                     P_COD_TPO_SDO,
                                     P_COD_TPO_CMA,
                                     P_COD_TPO_FND,
                                     P_FEC_TPO_OPE_CCLV,
                                     P_NUM_CLV_IDF,
                                     P_OBS_OPC_SWF,
                                     NULL,
                                     p_NUM_MVT,
                                     P_COD_USR,
                                     P_ERROR);

    -- Dependiendo del estado es la glosa que dejamos en la bitacora y en la glosa adicional estado
    IF (P_GLS_ADC_EST = 'CNT-COMBANC' AND
       p_FLG_EGR_ING = PKG_PAB_CONSTANTES.V_FLG_ING) THEN
      v_DES_BIT := 'Operación ingresada por contigencia Combanc';
    END IF;

    --Guardamos en la bitacora
    PKG_PAB_TRACKING.Sp_PAB_INS_BIT_OPE(p_NUM_FOL_OPE,
                                        v_FEC_ISR_OPE,
                                        p_COD_USR,
                                        v_COD_EST_OPE,
                                        v_COD_EST_OPE,
                                        v_DES_BIT,
                                        P_ERROR);

     END IF;

  EXCEPTION
    WHEN NO_DATA_FOUND THEN

      err_code := SQLCODE;
      err_msg  := SUBSTR(SQLERRM, 1, 300) ||
                  'No se pudo almacenar operacion en tabla operacion' ||
                  p_NUM_FOL_OPE;
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                         v_NOM_PCK,
                                         err_msg,
                                         v_NOM_SP);
      p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;

    WHEN OTHERS THEN

      err_code    := SQLCODE;
      err_msg     := SUBSTR(SQLERRM, 1, 300);
      p_s_mensaje := err_code || '-' || err_msg;
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                         v_NOM_PCK,
                                         err_msg,
                                         v_NOM_SP);
      p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);
  END Sp_PAB_GES_INS_OPE;

  -- ***********************************************************************************************
  -- Funcion/Procedimiento: Sp_PAB_BUS_PAG_RECIB
  -- Objetivo: Busca las operaciones que no fue posible realizar un abono automatico.
  -- Sistema: PAB
  -- Base de Datos: DGBMSEGDB01_NGALTM
  -- Tablas Usadas: PABS_DT_DETLL_NOMNA
  -- Fecha: 24/10/16
  -- Autor: Santander
  -- Input:
  --      p_COD_MT_SWF -> Codigo de MT Swift
  --      p_MNTO_DES -> Monto operacion desde
  --      p_MNTO_HAS -> Monto operacion hasta
  --      p_COD_DVI-> Codigo de moneda
  --      p_NUM_REF_SWF -> Numero Referencia SWIFT
  --      p_COD_BCO -> Codigo de banco
  -- Output:  p_CURSOR -> Cursor que tiene los pagos recibidos
  --          p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
  --
  -- Input/Output: N/A
  -- Retorno: N/A.
  -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
  -- ***********************************************************************************************
  PROCEDURE Sp_PAB_BUS_PAG_RECIB(p_COD_MT_SWF  IN NUMBER,
                                 p_MNTO_DES    IN NUMBER,
                                 p_MNTO_HAS    IN NUMBER,
                                 p_COD_DVI     IN CHAR,
                                 p_NUM_REF_SWF IN CHAR,
                                 p_COD_BCO     IN CHAR,
                                 p_CURSOR      OUT SYS_REFCURSOR,
                                 p_ERROR       OUT NUMBER) IS

    v_NOM_SP      VARCHAR2(30) := 'Sp_PAB_BUS_PAG_RECIB';
    v_IMP_DES     NUMBER;
    v_IMP_HAS     NUMBER;
    v_COD_MT103   NUMBER;
    v_COD_MT202   NUMBER;
    v_COD_MT205   NUMBER;
    v_NUM_REF_SWF CHAR(16);
    v_COD_BCO     CHAR(11);
    v_COD_DVI     CHAR(3);

  BEGIN
    P_ERROR := PKG_PAB_CONSTANTES.V_OK;

    --Siempre data
    IF p_MNTO_DES = -1 THEN
      v_IMP_DES := 0;
    ELSE
      v_IMP_DES := p_MNTO_DES;
    END IF;

    --Siempre data
    IF p_MNTO_HAS = -1 THEN
      v_IMP_HAS := 9999999999999;
    ELSE
      v_IMP_HAS := p_MNTO_HAS;
    END IF;

    --Tipo de MT
    IF p_COD_MT_SWF = -1 THEN
      v_COD_MT103 := PKG_PAB_CONSTANTES.V_COD_MT103;
      v_COD_MT202 := PKG_PAB_CONSTANTES.V_COD_MT202;
      v_COD_MT205 := PKG_PAB_CONSTANTES.V_COD_MT205;
    ELSE
      v_COD_MT103 := p_COD_MT_SWF;
      v_COD_MT202 := p_COD_MT_SWF;
      v_COD_MT205 := p_COD_MT_SWF;
    END IF;

    IF p_NUM_REF_SWF IS NULL THEN
      v_NUM_REF_SWF := NULL;
    ELSE
      v_NUM_REF_SWF := UPPER(p_NUM_REF_SWF);
    END IF;

    IF p_COD_BCO IS NULL THEN
      v_COD_BCO := NULL;
    ELSE
      v_COD_BCO := p_COD_BCO;
    END IF;

    --
    IF p_COD_DVI IS NULL THEN
      v_COD_DVI := NULL;
    ELSE
      v_COD_DVI := p_COD_DVI;
    END IF;

    OPEN p_CURSOR FOR
      SELECT OPD.FEC_ISR_OPE AS Fecha_Insercion,
             OPD.NUM_FOL_OPE AS Nro_Operacion,
             OPD.NOM_BFC AS Nombre_Beneficiario,
             OPD.NUM_FOL_NMN AS Numero_nomina,
             OPD.NUM_FOL_OPE_ORG AS NUM_FOL_OPE_ORG,
             OPD.NUM_OPE_SIS_ENT AS NUM_OPE_SIS_ENT,
             OPD.FLG_EGR_ING AS FLG_EGR_ING,
             DECODE(OPD.FLG_EGR_ING,
                    PKG_PAB_CONSTANTES.v_FLG_EGR,
                    PKG_PAB_CONSTANTES.v_STR_FLG_EGR,
                    PKG_PAB_CONSTANTES.v_STR_FLG_ING) AS DSC_EGR_ING,
             OPD.COD_SIS_SAL AS COD_SIS_SAL,
             PKG_PAB_UTILITY.FN_PAB_OBT_DSC_SIS_ENT_SAL(OPD.COD_SIS_SAL) AS DSC_SAL_DET,
             OPD.COD_SIS_ENT AS COD_SIS_ENT,
             PKG_PAB_UTILITY.FN_PAB_OBT_DSC_SIS_ENT_SAL(OPD.COD_SIS_ENT) AS Canal_Origen,
             OPD.COD_EST_AOS AS COD_EST_AOS,
             PKG_PAB_UTILITY.FN_PAB_OBT_DSC_COD_EST(OPD.COD_EST_AOS) AS Motivo,
             OPD.COD_TPO_ING AS COD_TPO_ING,
             PKG_PAB_UTILITY.FN_PAB_OBT_TIP_ING(OPD.COD_TPO_ING) AS DSC_TPO_ING,
             OPD.FEC_ENV_RCP_SWF AS FEC_ENV_RCP_SWF,
             OPD.HOR_ENV_RCP_SWF AS HOR_ENV_RCP_SWF,
             OPD.FEC_ENV_RCP_SWF || ' ' || OPD.HOR_ENV_RCP_SWF AS FECHA_DEVOLUCION,
             OPD.COD_MT_SWF AS MT_SWIFT,
             OPD.FLG_MRD_UTZ_SWF AS FLG_MRD_UTZ_SWF,
             DECODE(OPD.FLG_MRD_UTZ_SWF,
                    PKG_PAB_CONSTANTES.v_FLG_MRD_MN,
                    PKG_PAB_CONSTANTES.v_STR_FLG_MRD_MN,
                    PKG_PAB_CONSTANTES.v_STR_FLG_MRD_MX) AS DSC_MRD_UTZ_SWF,
             OPD.COD_TPO_OPE_AOS AS COD_TPO_OPE_AOS,
             -- PKG_PAB_UTILITY.FN_PAB_OBT_DSC_TIP_OPE(OPD.COD_TPO_OPE_AOS)     AS Tipo_Operacion,  -- Falta codigo en tabla maestra
             OPD.FEC_VTA     AS Fecha_Valuta,
             OPD.COD_DVI     AS Moneda,
             OPD.IMP_OPE     AS Monto,
             OPD.COD_BCO_ORG AS COD_BCO_PAG, -- Codigo del Banco de Pago
             TI.DES_BCO      AS DES_BCO,
             --PKG_PAB_UTILITY.FN_PAB_OBT_DSC_BCO(OPD.COD_BCO_DTN)             AS Banco_Destino,  -- Falta codigo en tabla maestra
             DECODE(TRIM(OPI.NUM_DOC_BFC),
                    NULL,
                    TRIM(OPI.NUM_DOC_BFC) ||
                    DECODE(OPI.NUM_DOC_BFC, NULL, NULL, OPI.COD_TPD_BFC),
                    TRIM(OPI.NUM_DOC_BFC) || '-' ||
                    DECODE(OPI.NUM_DOC_BFC, NULL, NULL, OPI.COD_TPD_BFC)) AS Rut_Beneficiario,
             OPD.NUM_CTA_BFC AS Cta_Beneficiario,
             OPD.COD_SUC AS Sucursal,
             OPD.NUM_REF_SWF AS NUM_REF_SWF,
             OPD.NUM_REF_EXT AS NUM_REF_EXT,
             OPD.GLS_ADC_EST AS GLS_ADC_EST,
             OPD.NUM_REF_CTB AS NUM_REF_CTB,
             ----------------    PABS_DT_NOMNA_INFCN_OPCON   -------------------------------
             OPI.OBS_OPC_SWF AS OBS_OPC_SWF,
             OPI.COD_BCO_BFC AS COD_BCO_BFC,
             OPI.COD_BCO_ITD AS COD_BCO_ITD,
             PKG_PAB_UTILITY.FN_PAB_OBT_DSC_BCO(OPI.COD_BCO_ITD) AS DSC_BCO_ITD,
             OPI.NUM_DOC_BFC AS NUM_DOC_BFC,
             OPI.COD_TPD_BFC AS COD_TPD_BFC,
             OPI.NUM_CTA_DCV_BFC AS NUM_CTA_DCV_BFC,
             OPI.GLS_DIR_BFC AS GLS_DIR_BFC,
             OPI.NOM_CDD_BFC AS NOM_CDD_BFC,
             OPI.COD_PAS_BFC AS COD_PAS_BFC,
             PKG_PAB_UTILITY.Fn_PAB_COMBO_PAIS(OPI.COD_PAS_BFC) AS DSC_PAS_BFC,
             OPI.NOM_ODN AS Nombre_Ordenante,
             DECODE(TRIM(OPI.NUM_DOC_ODN),
                    NULL,
                    TRIM(OPI.NUM_DOC_ODN) ||
                    DECODE(OPI.NUM_DOC_ODN, NULL, NULL, OPI.COD_TPD_ODN),
                    TRIM(OPI.NUM_DOC_ODN) || '-' ||
                    DECODE(OPI.NUM_DOC_ODN, NULL, NULL, OPI.COD_TPD_ODN)) AS Rut_Ordenante,
             OPI.NUM_CTA_ODN AS Cta_Ordenante,
             OPI.NUM_CTA_DCV_ODN AS NUM_CTA_DCV_ODN,
             OPI.GLS_DIR_ODN AS GLS_DIR_ODN,
             OPI.NOM_CDD_ODN AS NOM_CDD_ODN,
             OPI.COD_PAS_ODN AS COD_PAS_ODN,
             PKG_PAB_UTILITY.Fn_PAB_COMBO_PAIS(OPI.COD_PAS_ODN) AS DSC_PAS_ODN,
             OPI.NUM_CLV_NGC AS NUM_CLV_NGC,
             OPI.NUM_AGT AS NUM_AGT,
             OPI.COD_FND_CCLV AS COD_FND_CCLV,
             OPI.COD_TPO_SDO AS COD_TPO_SDO,
             PKG_PAB_UTILITY.Fn_PAB_COMBO_TSALDO(OPI.COD_TPO_SDO) AS DSC_TPO_SDO,
             OPI.COD_TPO_CMA AS COD_TPO_CMA,
             PKG_PAB_UTILITY.Fn_PAB_COMBO_CAMARA(OPI.COD_TPO_CMA) AS DSC_TPO_CMA,
             OPI.COD_TPO_FND AS COD_TPO_FND,
             PKG_PAB_UTILITY.FN_PAB_TRAN_COD_FONDO(OPI.COD_TPO_FND) AS DSC_TPO_FND,
             DECODE(OPI.FEC_TPO_OPE_CCLV, '01-01-3000', NULL) AS FEC_TPO_OPE_CCLV,
             OPI.NUM_CLV_IDF AS NUM_CLV_IDF,
             OPI.GLS_EST_RCH AS GLS_EST_RCH,
             OPI.GLS_MTV_VSD AS GLS_MTV_VSD
        FROM PABS_DT_DETLL_OPRCN       OPD,
             PABS_DT_OPRCN_INFCN_OPCON OPI,
             TCDTBAI                   TI
       WHERE OPD.COD_BCO_ORG = TI.TGCDSWSA
         AND OPD.COD_EST_AOS = PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAB --Filtramos por PAB
         AND OPD.COD_MT_SWF IN (V_COD_MT103, V_COD_MT202, v_COD_MT205)
         AND OPD.IMP_OPE BETWEEN v_IMP_DES AND v_IMP_HAS
         AND OPD.COD_DVI = NVL(p_COD_DVI, OPD.COD_DVI)
         AND UPPER(NVL(OPD.NUM_REF_SWF, '#')) =
             NVL(v_NUM_REF_SWF, NVL(UPPER(OPD.NUM_REF_SWF), '#'))
         AND NVL(OPD.COD_BCO_ORG, '#') =
             NVL(v_COD_BCO, NVL(OPD.COD_BCO_ORG, '#'))
         AND OPD.FLG_EGR_ING = PKG_PAB_CONSTANTES.V_FLG_ING
         AND OPD.COD_TPO_OPE_AOS NOT IN
             (PKG_PAB_CONSTANTES.V_STR_TPO_OPE_DEVFDOS)
         AND OPD.NUM_FOL_OPE = OPI.NUM_FOL_OPE(+)
         AND OPD.FEC_ISR_OPE = OPI.FEC_ISR_OPE(+)
       ORDER BY OPD.NUM_FOL_OPE ASC;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      err_code := SQLCODE;
      err_msg  := 'Error en búsqueda de operaciones que no fue posible realizar un abono automatico';
      p_ERROR  := PKG_PAB_CONSTANTES.v_ERROR;
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                         v_NOM_PCK,
                                         err_msg,
                                         v_NOM_SP);
    WHEN OTHERS THEN
      err_code    := SQLCODE;
      err_msg     := SUBSTR(SQLERRM, 1, 300);
      p_s_mensaje := err_code || '-' || err_msg;
      p_ERROR     := PKG_PAB_CONSTANTES.v_ERROR;
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                         v_NOM_PCK,
                                         err_msg,
                                         v_NOM_SP);
      RAISE_APPLICATION_ERROR(-20000, err_code || p_s_mensaje);
      --
  END Sp_PAB_BUS_PAG_RECIB;

  -- ***********************************************************************************************
  -- Funcion/Procedimiento: SP_PAB_GES_ABO_PAG_RCB
  -- Objetivo: Procedimiento actualiza estado de la operacion Por pago recibido
  -- Sistema: PAB
  -- Base de Datos: DGBMSEGDB01_NGALTM
  -- Tablas Usadas: N/A
  -- Fecha: 04/05/17
  -- Autor: Santander-CAH
  -- Input:
  --      p_NUM_FOL_OPE        -> Numero de Operacion Origen
  --      p_FEC_ISR_OPE        -> Fecha de insercion de operacion origen
  --      p_SIS_ENT_ORI        -> Nombre Canal de Entrada origen
  --      p_NUM_REF_SWF        -> Numero de referencia Swift
  --      p_TIP_ABO_MAN        -> Codigo de tipo Abono
  --      p_ARE_DTN            -> Area destino del Abono, si p_TIP_ABO_MAN es Puente Contable
  --      p_NUM_CTA              -> Numero de cuenta
  --      p_NUM_DEV_TRANS      -> Numero devuelto por WS de Vigencia Salida
  --      p_COD_USR            -> Codigo de Usuario que se registrara en bitacora
  -- Output:
  --      p_ERROR              -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
  -- Input/Output: N/A
  -- Retorno: N/A.
  -- Observaciones: <Fecha y Detalle de ultimos cambios>
  -- ***********************************************************************************************
  PROCEDURE SP_PAB_GES_ABO_PAG_RCB(p_NUM_FOL_OPE   IN NUMBER,
                                   p_FEC_ISR_OPE   IN DATE,
                                   p_SIS_ENT_ORI   IN CHAR,
                                   p_NUM_REF_SWF   IN CHAR,
                                   p_TIP_ABO_MAN   IN CHAR,
                                   p_ARE_DTN       IN CHAR,
                                   p_NUM_CTA       IN CHAR,
                                   p_NUM_DEV_TRANS IN VARCHAR2,
                                   p_COD_USR       IN CHAR,
                                   p_ERROR         OUT NUMBER) IS

    v_NOM_SP          VARCHAR2(30) := 'SP_PAB_GES_ABO_PAG_RCB ';
    v_TIP_ABO_MAN     CHAR(10);
    v_CAN_SAL         CHAR(10);
    v_COD_EST_OPE_ABN NUMBER(5) := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEABN;
    v_DSC_EST_OPE_ABN CHAR(3) := PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEABN;
    v_DSC_GLB_BTC     VARCHAR2(100);
    v_NUM_CTA         VARCHAR2(100) := p_NUM_CTA; --Numero de cuenta corriente de la Operacion de Pago recibido
    v_DSC_SIS_ENT_SAL VARCHAR2(50);
    v_NUM_REF_SWF     VARCHAR2(100);
    V_IMP_OPE         NUMBER;
    V_NUM_REF_CTB     CHAR(12);
    V_NUM_DEV_TRANS   VARCHAR2(30) := NULL;

    ERR_COD_INEX EXCEPTION;

  BEGIN

    --GCP
    err_msg := 'p_TIP_ABO_MAN:' || p_TIP_ABO_MAN || ' p_ARE_DTN:' ||
               p_ARE_DTN || ' p_NUM_CTA:' || p_NUM_CTA ||
               ' p_NUM_DEV_TRANS:' || p_NUM_DEV_TRANS;
    PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                       v_NOM_PCK,
                                       err_msg,
                                       v_NOM_SP);

    BEGIN

      SELECT IMP_OPE
        INTO V_IMP_OPE
        FROM PABS_DT_DETLL_OPRCN
       WHERE FEC_ISR_OPE = p_FEC_ISR_OPE
         AND NUM_FOL_OPE = p_NUM_FOL_OPE
         AND FLG_EGR_ING = PKG_PAB_CONSTANTES.V_FLG_ING;

    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        err_code := SQLCODE;
        err_msg  := 'No ser encontro información de la operación: ' ||
                    p_NUM_FOL_OPE || ' Fecha:' || TO_CHAR(p_FEC_ISR_OPE);
        RAISE;
      WHEN OTHERS THEN
        err_code := SQLCODE;
        err_msg  := SUBSTR(SQLERRM, 1, 300);
        RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);
    END;

    P_ERROR := PKG_PAB_CONSTANTES.V_OK;

    IF p_TIP_ABO_MAN = PKG_PAB_CONSTANTES.V_COD_TIP_ABO_PUE THEN
      -- Abono Puente Contable 'PUENTECONT'

      v_CAN_SAL         := p_ARE_DTN;
      v_DSC_SIS_ENT_SAL := PKG_PAB_UTILITY.FN_PAB_OBT_DSC_SIS_ENT_SAL(p_ARE_DTN);
      v_DSC_GLB_BTC     := 'Se abono en la Cuenta Contable del área: ' ||
                           v_DSC_SIS_ENT_SAL || ' N: ' || p_NUM_DEV_TRANS;

    ELSIF p_TIP_ABO_MAN = PKG_PAB_CONSTANTES.V_COD_TIP_ABO_VXP THEN
      -- Abono Pendientes Por Pagar o Varios Acreedor  'VIGPENPPAG'

      --Vigente
      v_CAN_SAL     := PKG_PAB_CONSTANTES.V_COD_TIP_ABO_XDIS;
      v_DSC_GLB_BTC := 'Se da de alta la vigente N: ' || p_NUM_DEV_TRANS ||
                       ' Pendientes por pagar';

    ELSIF p_TIP_ABO_MAN = PKG_PAB_CONSTANTES.V_COD_TIP_ABO_VAC THEN

      --Vigente
      v_CAN_SAL     := PKG_PAB_CONSTANTES.V_COD_TIP_ABO_XDIS;
      v_DSC_GLB_BTC := 'Se da de alta la vigente N: ' || p_NUM_DEV_TRANS ||
                       ' Varios Acreedores';

    ELSIF p_TIP_ABO_MAN = PKG_PAB_CONSTANTES.V_COD_TIP_ABO_CTA THEN

      v_CAN_SAL       := PKG_PAB_CONSTANTES.V_COD_TIP_ABO_CTA;
      v_DSC_GLB_BTC   := 'Se abono en la Cuenta Corriente N: ' || v_NUM_CTA ||
                         ' , N: ' || p_NUM_DEV_TRANS;
      V_NUM_DEV_TRANS := p_NUM_DEV_TRANS;

      --Se inserta registro de tabla de cargos y abonos
      PKG_PAB_UTILITY.SP_PAB_INS_MVNTO_CARGO_ABONO(p_NUM_DEV_TRANS,
                                                   p_NUM_FOL_OPE,
                                                   p_FEC_ISR_OPE,
                                                   PKG_PAB_CONSTANTES.V_FLG_MOV_ABN,
                                                   p_ERROR);

      V_NUM_REF_CTB := LPAD(p_NUM_CTA, 12, 0);

    END IF;

    -- Actualiza datos de la operación
    UPDATE PABS_DT_DETLL_OPRCN
       SET COD_SIS_SAL = v_CAN_SAL,
           FEC_ACT_OPE = SYSDATE, -- Fecha de actualización para dashboard
           GLS_ADC_EST = v_DSC_GLB_BTC,
           NUM_REF_CTB = V_NUM_REF_CTB,
           COD_EST_AOS = v_COD_EST_OPE_ABN
     WHERE NUM_FOL_OPE = p_NUM_FOL_OPE
       AND FEC_ISR_OPE = p_FEC_ISR_OPE;

    --Insertamos en Bitacora
    PKG_PAB_TRACKING.Sp_PAB_INS_BIT_OPE(p_NUM_FOL_OPE,
                                        p_FEC_ISR_OPE,
                                        p_COD_USR,
                                        PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAB,
                                        v_COD_EST_OPE_ABN,
                                        v_DSC_GLB_BTC,
                                        p_ERROR);

    --Actualizamos estado de la operación
    --        PKG_PAB_OPERACION.Sp_PAB_ACT_DATOS_OPE_REC(p_NUM_FOL_OPE,
    --                                                   p_FEC_ISR_OPE,
    --                                                   PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEABN,
    --                                                   NULL, --
    --                                                   NULL, --
    --                                                   NULL,
    --                                                   NULL,
    --                                                   NULL,
    --                                                   V_NUM_DEV_TRANS,
    --                                                   NULL,
    --                                                   NULL,
    --                                                   NULL,
    --                                                   p_COD_USR,
    --                                                   v_CAN_SAL,
    --                                                   p_ERROR
    --                                                   );
    --
    --Calculamos el saldo por area
    PKG_PAB_CAJA_MN.SP_BUS_CALCULA_AREA_MN(v_CAN_SAL,
                                           V_IMP_OPE,
                                           v_cero,
                                           p_ERROR);

    COMMIT;

  EXCEPTION
    WHEN ERR_COD_INEX THEN
      err_msg := 'Problemas con gestion de abono manual' || p_SIS_ENT_ORI;
      P_ERROR := PKG_PAB_CONSTANTES.V_ERROR;
      PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD(err_code,
                                         V_NOM_PCK,
                                         err_msg,
                                         V_NOM_SP);
    WHEN DUP_VAL_ON_INDEX THEN
      err_code := SQLCODE;
      err_msg  := SUBSTR(SQLERRM, 1, 300);
      P_ERROR  := PKG_PAB_CONSTANTES.V_ERROR;
      PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD(err_code,
                                         V_NOM_PCK,
                                         err_msg,
                                         V_NOM_SP);
    WHEN OTHERS THEN
      err_code := SQLCODE;
      err_msg  := SUBSTR(SQLERRM, 1, 300);
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                         v_NOM_PCK,
                                         err_msg,
                                         v_NOM_SP);
      p_ERROR     := PKG_PAB_CONSTANTES.v_ERROR;
      p_s_mensaje := err_code || '-' || err_msg;
      RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);

  END SP_PAB_GES_ABO_PAG_RCB;

  /************************************************************************************************

  -- Funcion/Procedimiento: Sp_PAB_ACT_DATOS_OPE_REC
  -- Objetivo: Procedimiento que actualiza informacion de una operacion recibida
  -- Sistema: DBO_PAB.
  -- Base de Datos: DBO_PAB.
  -- Tablas Usadas: PABS_DT_DETLL_OPRCN, PABS_DT_OPRCN_INFCN_OPCON
  -- Fecha: 02/06/16
  -- Autor: Santander
  -- Input:    p_NUM_FOL_NMN->     foliador del banco
  --           p_FEC_ISR_OPE->     Fecha de insercion de la operacion
  --           p_COD_EST_AOS->     Estado nuevo de la operacion
  --           p_NUM_REF_SWF->     Numero de referencia del swift
  --           p_NUM_FOL_OPE_ORG-> Numero folio operacion origen
  --           p_FEC_ISR_OPE_ORG-> Fecha de operación original
  --           p_NUM_REF_EXT ->    Numero de referencia operacion origen
  --           p_NUM_IDF_GTR_DOC-> Numero identificador gestor documental VARCHAR2(40 BYTE),
  --           p_NUM_MVT_CTB ->    Numero movimiento contable CHAR(12 BYTE)
  --           p_GLS_ADC_EST->     Glosa Adicional Estado
  --           p_GLS_EST_RCH->     Glosa Estado Rechazado
  --           p_GLS_MTv_VSD->     Glosa Motivo Visacion
  --           p_COD_EST_AOS->     Identificador de nuevo estado.
  --           p_COD_USR->         rut usuario
  -- Output:
  --          p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
  -- Input/Output: N/A
  -- Retorno: N/A.
  -- Observaciones: <Fecha y Detalle de ultimos cambios>
  --***********************************************************************************************/
  PROCEDURE Sp_PAB_ACT_DATOS_OPE_REC(p_NUM_FOL_OPE     IN NUMBER,
                                     p_FEC_ISR_OPE     IN DATE,
                                     p_COD_EST_AOS     IN CHAR,
                                     p_NUM_REF_SWF     IN VARCHAR2,
                                     p_NUM_FOL_OPE_ORG IN NUMBER,
                                     p_FEC_ISR_OPE_ORG IN DATE,
                                     p_NUM_REF_EXT     IN VARCHAR2,
                                     p_NUM_IDF_GTR_DOC IN VARCHAR2,
                                     p_NUM_MVT_CTB     IN CHAR,
                                     p_GLS_ADC_EST     IN VARCHAR2,
                                     p_GLS_EST_RCH     IN VARCHAR2,
                                     p_GLS_MTV_VSD     IN VARCHAR2,
                                     p_COD_USR         IN VARCHAR2,
                                     P_COD_SIS_ENT_ORG IN VARCHAR2,
                                     p_ERROR           OUT NUMBER) IS
    v_NOM_SP          VARCHAR2(30) := 'Sp_PAB_ACT_DATOS_OPE_REC';
    v_NUM_REF_SWF     CHAR(16);
    v_GLS_ADC_EST     VARCHAR2(210);
    v_GLS_EST_RCH     VARCHAR2(50);
    v_GLS_MTv_VSD     VARCHAR2(50);
    v_NUM_FOL_OPE     NUMBER;
    v_FEC_ISR_OPE     DATE;
    v_COD_USR         CHAR(11);
    v_COD_EST_OPE_NUE NUMBER;
    v_COD_EST_OPE_ANT NUMBER;
    v_NUM_FOL_OPE_ANT NUMBER;
    v_NUM_REF_EXT_ANT CHAR(16);
    v_NUM_REF_EXT_NUE CHAR(16);
    v_NUM_IDF_GTR_DOC VARCHAR2(40);
    V_NUM_MVT_CTB     CHAR(12);
    V_NUM_CTA_BFC     VARCHAR2(30);
    V_NUM_CTA_ORD_ORG VARCHAR2(30);
    v_COD_SIS_SAL     CHAR(10);
    v_COD_SIS_SAL_ANT CHAR(10);
    v_COD_MT_SWF      NUMBER;
    v_COD_MT_SWF_ORG  NUMBER;
    v_DSC_SIS_ENT_SAL VARCHAR2(50);
    v_COD_TPO_OPE_AOS CHAR(8);
    v_NUM_REF_EXT_ORG CHAR(16);
    v_NUM_FOL_OPE_ORG NUMBER;
    v_IMP_OPE         NUMBER;
    v_FEC_ISR_OPE_ORG DATE := NULL;

    ERROR_REFSWF EXCEPTION;

  BEGIN

    v_COD_USR         := p_COD_USR;
    v_NUM_FOL_OPE     := p_NUM_FOL_OPE;
    v_FEC_ISR_OPE     := p_FEC_ISR_OPE;
    v_NUM_REF_SWF     := p_NUM_REF_SWF;
    v_NUM_FOL_OPE_ORG := p_NUM_FOL_OPE_ORG;
    v_GLS_EST_RCH     := p_GLS_EST_RCH;
    v_GLS_MTv_VSD     := p_GLS_MTV_VSD;
    v_NUM_IDF_GTR_DOC := p_NUM_IDF_GTR_DOC;

    --Rescatamos valores de la operacion.
    BEGIN

      SELECT OPE.NUM_REF_SWF,
             OPE.GLS_ADC_EST,
             DETOPE.GLS_EST_RCH,
             DETOPE.GLS_MTv_VSD,
             OPE.COD_EST_AOS,
             OPE.NUM_FOL_OPE_ORG,
             OPE.NUM_REF_EXT,
             OPE.NUM_IDF_GTR_DOC,
             OPE.NUM_MVT_CTB,
             OPE.NUM_CTA_BFC,
             OPE.COD_SIS_SAL,
             OPE.COD_MT_SWF,
             OPE.COD_TPO_OPE_AOS,
             OPE.IMP_OPE
        INTO v_NUM_REF_SWF,
             v_GLS_ADC_EST,
             v_GLS_EST_RCH,
             v_GLS_MTv_VSD,
             v_COD_EST_OPE_ANT,
             v_NUM_FOL_OPE_ANT,
             v_NUM_REF_EXT_ANT,
             v_NUM_IDF_GTR_DOC,
             V_NUM_MVT_CTB,
             V_NUM_CTA_BFC,
             v_COD_SIS_SAL_ANT,
             v_COD_MT_SWF,
             V_COD_TPO_OPE_AOS,
             V_IMP_OPE
        FROM PABS_DT_DETLL_OPRCN OPE, PABS_DT_OPRCN_INFCN_OPCON DETOPE
       WHERE OPE.FEC_ISR_OPE = DETOPE.FEC_ISR_OPE(+)
         AND OPE.NUM_FOL_OPE = DETOPE.NUM_FOL_OPE(+)
         AND OPE.NUM_FOL_OPE = v_NUM_FOL_OPE
         AND OPE.FEC_ISR_OPE = v_FEC_ISR_OPE;

    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        err_code := SQLCODE;
        err_msg  := 'No se encontro informacion con v_NUM_FOL_OPE: ' ||
                    v_NUM_FOL_OPE || ' v_FEC_ISR_OPE:' ||
                    TO_CHAR(v_FEC_ISR_OPE, 'dd-mm-yyyy hh24:mi:ss');
        RAISE;

      WHEN OTHERS THEN
        err_code    := SQLCODE;
        err_msg     := SUBSTR(SQLERRM, 1, 300);
        p_s_mensaje := err_code || '-' || err_msg;
        PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                           v_NOM_PCK,
                                           err_msg,
                                           v_NOM_SP);
        p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
        RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);
    END;

    --        --GCP------------------------------------------------------------------------
    --        err_msg := 'p_NUM_REF_EXT:' || p_NUM_REF_EXT || ' v_COD_SIS_SAL_ANT:' || v_COD_SIS_SAL_ANT || ' p_NUM_MVT_CTB:' || p_NUM_MVT_CTB || ' p_NUM_FOL_OPE_ORG:' || p_NUM_FOL_OPE_ORG || ' p_FEC_ISR_OPE_ORG:' || to_char(p_FEC_ISR_OPE_ORG,'dd-mm-yyyy hh24:mi:ss');
    --        PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
    --        ---------------------------------------------------------------------------
    --
    --Verificamos si envia una nueva referencia externa
    IF (NVL(p_NUM_REF_EXT, '#') <> '#') THEN
      v_NUM_REF_EXT_NUE := p_NUM_REF_EXT;
    ELSE
      v_NUM_REF_EXT_NUE := v_NUM_REF_EXT_ANT;
    END IF;

    --GCP------------------------------------------------------------------------
    --err_msg := 'v_NUM_REF_EXT_NUE' || v_NUM_REF_EXT_NUE || ' v_NUM_FOL_OPE:' || v_NUM_FOL_OPE || ' p_NUM_REF_EXT:' || TO_CHAR(p_NUM_REF_EXT) || ' v_COD_EST_OPE_NUE' || v_COD_EST_OPE_NUE;
    --PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
    -----------------------------------------------------------------------------

    --Verificamos si existe un campo fecha
    --IF( p_FEC_ISR_OPE_ORG <> TO_DATE('01-01-1900','DD-MM-YYYY') ) THEN
    IF (p_FEC_ISR_OPE_ORG IS NOT NULL) THEN

      v_FEC_ISR_OPE_ORG := p_FEC_ISR_OPE_ORG;

    END IF;

    --Verificamos si envia un nuevo estado
    IF (NVL(p_COD_EST_AOS, '#') <> '#') THEN
      v_COD_EST_OPE_NUE := PKG_PAB_UTILITY.FN_PAB_OBT_COD_DSC_EST(p_COD_EST_AOS,
                                                                  PKG_PAB_CONSTANTES.V_IND_OPE);
    ELSE
      --Si entra por este flujo es por que no se pudo abonar la operacion y debe devolver al banco origen
      v_COD_EST_OPE_NUE := v_COD_EST_OPE_ANT; --No se modificara el estado
      v_COD_SIS_SAL     := v_COD_SIS_SAL_ANT; --Mantenemos el antiguo
    END IF;

    --Si viene informacion en glosa adicional de estado
    IF (NVL(p_GLS_ADC_EST, '#') <> '#') THEN
      v_GLS_ADC_EST := p_GLS_ADC_EST;
    END IF;

    --Actualizamos informacion de la operacion
    BEGIN

      --Si se modifica el estado se debe guardar en la bitacora
      IF (v_COD_EST_OPE_NUE <> v_COD_EST_OPE_ANT) THEN

        --Dependiendo del estado es la glosa que dejamos en la bitacora y en la glosa adicional estado
        CASE
          WHEN v_COD_EST_OPE_NUE = PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAS THEN
            --Por Asociar

            --Bitacora
            v_DES_BIT     := 'Devolución a la espera de ser asociada';
            v_GLS_ADC_EST := v_DES_BIT;

          WHEN v_COD_EST_OPE_NUE = PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAB THEN
            --Por Abonar

            --Bitacora
            v_DES_BIT := 'Se modifico el estado de la operacion';

        --Glosa Estado
        --Integracion envia el motivo por el cual queda por abonar la operacion

          WHEN v_COD_EST_OPE_NUE = PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEABN THEN
            --Abonada

            IF (v_COD_MT_SWF = PKG_PAB_CONSTANTES.V_COD_MT103 AND
               V_NUM_CTA_BFC <> '000000000000' AND
               p_NUM_MVT_CTB IS NOT NULL) THEN

              --Bitacora
              v_DES_BIT := 'Se abono en la Cuenta Corriente: ' ||
                           V_NUM_CTA_BFC || ', N:' || p_NUM_MVT_CTB;

              --Glosa Estado
              v_GLS_ADC_EST := v_DES_BIT;

              --Guardamos el numero de movimiento contable
              V_NUM_MVT_CTB := p_NUM_MVT_CTB;

              --Pagada por cuentas corrientes
              v_COD_SIS_SAL := PKG_PAB_CONSTANTES.V_COD_TIP_ABO_CTA;

              --Actualizamos motno por area de MN
              PKG_PAB_CAJA_MN.SP_BUS_CALCULA_AREA_MN(v_COD_SIS_SAL,
                                                     v_IMp_OPE,
                                                     v_cero,
                                                     p_ERROR);

            ELSE
              --Verificamos si es una devolución
              IF (V_COD_TPO_OPE_AOS =
                 PKG_PAB_CONSTANTES.V_STR_TPO_OPE_DEVFDOS) THEN

                --Buscamos información de la operacion origen para decidir
                BEGIN

                  v_NUM_REF_EXT_ORG := p_NUM_REF_EXT;

                  SELECT COD_MT_SWF, NUM_CTA_ODN
                    INTO v_COD_MT_SWF_ORG, V_NUM_CTA_ORD_ORG
                    FROM PABS_DT_DETLL_OPRCN       OPE,
                         PABS_DT_OPRCN_INFCN_OPCON DETOPE
                   WHERE OPE.NUM_FOL_OPE = p_NUM_FOL_OPE_ORG
                     AND OPE.FEC_ISR_OPE = p_FEC_ISR_OPE_ORG
                     AND OPE.NUM_REF_SWF = v_NUM_REF_EXT_ORG
                     AND OPE.NUM_FOL_OPE = DETOPE.NUM_FOL_OPE
                     AND OPE.FEC_ISR_OPE = DETOPE.FEC_ISR_OPE;

                EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                    err_code := SQLCODE;
                    err_msg  := 'No se encontro informacion ope original con NUM_FOL_OPE_ORG: ' ||
                                p_NUM_FOL_OPE_ORG || ' p_NUM_REF_EXT:' ||
                                p_NUM_REF_EXT;
                    RAISE;

                  WHEN OTHERS THEN
                    err_code    := SQLCODE;
                    err_msg     := SUBSTR(SQLERRM, 1, 300);
                    p_s_mensaje := err_code || '-' || err_msg;
                    PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                                       v_NOM_PCK,
                                                       err_msg,
                                                       v_NOM_SP);
                    p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
                    RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);
                END;

                --Cambiamos el estado de la operacion original por devuelta
                Sp_PAB_ACT_EST_OPERACION(0,
                                         NULL,
                                         v_NUM_REF_EXT_ORG,
                                         PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEDEV,
                                         p_COD_USR,
                                         p_error);

                --Si el origen es un MT103 es por que la devolución se realizo por cuentas corrientes
                IF (v_COD_MT_SWF_ORG = PKG_PAB_CONSTANTES.V_COD_MT103) THEN

                  --Dejamos el canal de salida como cuentas corrientes
                  v_COD_SIS_SAL := PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_CTACTE;

                  --Bitacora
                  v_DES_BIT := 'Se abono en la Cuenta Corriente: ' ||
                               V_NUM_CTA_ORD_ORG || ', N:' || p_NUM_MVT_CTB;

                  --Glosa Estado
                  v_GLS_ADC_EST := v_DES_BIT;

                  --actualizamos el numero de cuenta operacion.
                  V_NUM_CTA_BFC := V_NUM_CTA_ORD_ORG;

                ELSE
                  --Si esta en este flujo es por que es un 202 o 205 de devolución

                  --Dejamos el canal de salida como cuentas corrientes
                  v_COD_SIS_SAL     := P_COD_SIS_ENT_ORG;
                  v_DSC_SIS_ENT_SAL := PKG_PAB_UTILITY.FN_PAB_OBT_DSC_SIS_ENT_SAL(v_COD_SIS_SAL);

                  --Glosa adicional estado
                  V_GLS_ADC_EST := 'Se abono en la Cuenta Contable del area: ' ||
                                   v_DSC_SIS_ENT_SAL;

                  --Bitacora
                  v_DES_BIT := 'Se abono en la Cuenta Contable del area: ' ||
                               v_DSC_SIS_ENT_SAL;

                END IF;

              ELSE
                --Dejamos el canal de salida de la operacion origen
                v_COD_SIS_SAL     := P_COD_SIS_ENT_ORG;
                v_DSC_SIS_ENT_SAL := PKG_PAB_UTILITY.FN_PAB_OBT_DSC_SIS_ENT_SAL(v_COD_SIS_SAL);

                --Glosa adicional estado
                V_GLS_ADC_EST := 'Se abono en la Cuenta Contable del area: ' ||
                                 v_DSC_SIS_ENT_SAL;

                --Bitacora
                v_DES_BIT := 'Se abono en la Cuenta Contable del area: ' ||
                             v_DSC_SIS_ENT_SAL;
              END IF;

            END IF;

          WHEN v_COD_EST_OPE_NUE = PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEING THEN
            --Ingresada

            --Bitacora
            v_DES_BIT := 'Se registra operacion en el sistema de Altos Montos';

          ELSE

            v_DES_BIT := 'Se recibe un codigo que no esta parametrizado:' ||
                         v_COD_EST_OPE_NUE;

        END CASE;

        --Insertamos en Bitacora
        PKG_PAB_TRACKING.Sp_PAB_INS_BIT_OPE(p_NUM_FOL_OPE,
                                            p_FEC_ISR_OPE,
                                            p_COD_USR,
                                            v_COD_EST_OPE_ANT,
                                            v_COD_EST_OPE_NUE,
                                            v_DES_BIT,
                                            p_ERROR);

      END IF;

      --Actualizamos  Operacion
      UPDATE PABS_DT_DETLL_OPRCN OPE
         SET COD_EST_AOS     = v_COD_EST_OPE_NUE,
             GLS_ADC_EST     = v_GLS_ADC_EST,
             NUM_MVT_CTB     = V_NUM_MVT_CTB, --Nula/Numero Mov.
             COD_SIS_SAL     = v_COD_SIS_SAL,
             NUM_REF_EXT     = v_NUM_REF_EXT_NUE,
             FEC_ACT_OPE     = SYSDATE, -- Fecha actualización para dashborad
             FEC_ISR_OPE_ORG = NVL(v_FEC_ISR_OPE_ORG, FEC_ISR_OPE_ORG),
             NUM_FOL_OPE_ORG = NVL(p_NUM_FOL_OPE_ORG, NUM_FOL_OPE_ORG),
             NUM_CTA_BFC     = V_NUM_CTA_BFC -- se actualiza numero de cuenta beneficiario
      --
       WHERE OPE.NUM_FOL_OPE = v_NUM_FOL_OPE
         AND OPE.FEC_ISR_OPE = v_FEC_ISR_OPE;

    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        err_code := SQLCODE;
        err_msg  := 'No se pudo actualizar operación:' || v_NUM_FOL_OPE;
        PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                           v_NOM_PCK,
                                           err_msg,
                                           v_NOM_SP);
        p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
        RAISE;

      WHEN OTHERS THEN
        err_code    := SQLCODE;
        err_msg     := SUBSTR(SQLERRM, 1, 300);
        p_s_mensaje := err_code || '-' || err_msg;
        PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                           v_NOM_PCK,
                                           err_msg,
                                           v_NOM_SP);
        p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
        RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);
    END;

    COMMIT;

  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      err_code := SQLCODE;
      err_msg  := 'Error en procedimiento que actualiza informacion de una operacion recibida';
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                         v_NOM_PCK,
                                         err_msg,
                                         v_NOM_SP);
      p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;

    WHEN OTHERS THEN
      err_code    := SQLCODE;
      err_msg     := SUBSTR(SQLERRM, 1, 300);
      p_s_mensaje := err_code || '-' || err_msg;
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                         v_NOM_PCK,
                                         err_msg,
                                         v_NOM_SP);
      p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);
  END Sp_PAB_ACT_DATOS_OPE_REC;

  /************************************************************************************************
  -- Funcion/Procedimiento: Sp_PAB_BUS_CANAL_SALIDA
  -- Objetivo: Procedimiento que busca el Canal de Salida segun, banco, moneda, tipo operacion y horario
  -- Sistema: DBO_PAB.
  -- Base de Datos: DBO_PAB.
  -- Tablas Usadas: PABS_DT_BANCO_ALMOT
  -- Fecha: 02/06/16
  -- Autor: Santander
  -- Input:
  --                       p_COD_BCO_DTN      Banco  destino
  --                       p_COD_MT_SWF       Cod.MT
  --                       p_MONTO_OPE        Monto
  --                       p_COD_TPO_OPE_AOS  Tipo operacion
  --                       p_COD_DVI          Moneda
  -- Output:
  --          p_COD_SIS_SAL   canal de salida
  --          p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
  -- Input/Output: N/A
  -- Retorno: N/A.
  -- Observaciones: <Fecha y Detalle de ultimos cambios>
  --***********************************************************************************************/
  PROCEDURE Sp_PAB_BUS_CANAL_SALIDA(p_COD_BCO_DTN     IN CHAR,
                                    p_COD_MT_SWF      IN NUMBER,
                                    p_MONTO_OPE       IN NUMBER,
                                    p_COD_TPO_OPE_AOS IN CHAR,
                                    p_COD_DVI         IN CHAR,
                                    p_COD_SIS_SAL     OUT CHAR,
                                    p_ERROR           OUT NUMBER) IS

    v_NOM_SP              VARCHAR2(30) := 'Sp_PAB_BUS_CANAL_SALIDA';
    v_COD_SIS_SAL_FIN     CHAR(10);
    v_COD_SIS_SAL_BCO     CHAR(10);
    v_COD_SIS_SAL_TPO_OPE CHAR(10);
    v_MONTO_OPE           NUMBER; -- Variable para ocupar a futuro segun saldo banco

    --Variables de Horario
    v_HOR_SIS NUMBER(4);
    v_HOR_INI NUMBER(4);
    v_HOR_CRR NUMBER(4);

  BEGIN

    v_MONTO_OPE := p_MONTO_OPE; -- Variable para ocupar a futuro segun saldo banco

    --Si Banco destino Santander
    IF ((PKG_PAB_CONSTANTES.V_BIC_BANCO_SANTANDER = p_COD_BCO_DTN OR
       PKG_PAB_CONSTANTES.V_BIC_BANCO_SANTANDER_HOM = p_COD_BCO_DTN) AND
       p_COD_TPO_OPE_AOS <> PKG_PAB_CONSTANTES.V_STR_TPO_OPE_DVP) THEN

      v_COD_SIS_SAL_FIN := PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_CTACTE; -- Canal de salida es cuenta corriente CTACTE
      p_COD_SIS_SAL     := v_COD_SIS_SAL_FIN;

    ELSE

      --Si la operacion es CLP
      IF p_COD_DVI = PKG_PAB_CONSTANTES.v_STR_DVI_CLP THEN
        --Calculamos el canal de pago de la operacion

        --Criterio 1: Obtenemos el canal de salida segun el banco
        BEGIN
          SELECT COD_SIS_SAL
            INTO v_COD_SIS_SAL_BCO
            FROM PABS_DT_BANCO_ALMOT BAN
           WHERE BAN.COD_BIC_BCO = p_COD_BCO_DTN;

        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            ERR_MSG  := 'No se encontro informacion del canal de salida para el banco ' ||
                        p_COD_BCO_DTN; --Si no encuentra el Canal
            ERR_CODE := SQLCODE;
            RAISE;

          WHEN OTHERS THEN
            ERR_CODE    := SQLCODE;
            ERR_MSG     := SUBSTR(SQLERRM, 1, 300);
            p_s_mensaje := err_code || '-' || err_msg;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(NULL,
                                               v_NOM_PCK,
                                               ERR_MSG,
                                               v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);

        END;

        --Criterio 2: Verificamos si para el tipo de canal por banco lo soporta la operacion
        BEGIN

          SELECT COD_SIS_ENT_SAL
            INTO v_COD_SIS_SAL_TPO_OPE
            FROM PABS_DT_TIPO_OPRCN_SALID_MNSJ
           WHERE COD_SIS_ENT_SAL = v_COD_SIS_SAL_BCO
             AND COD_TPO_OPE_AOS = p_COD_TPO_OPE_AOS
             AND COD_MT_SWF = p_COD_MT_SWF
             AND FLG_MRD_UTZ_SWF = PKG_PAB_CONSTANTES.v_FLG_MRD_MN
             AND FLG_VGN = PKG_PAB_CONSTANTES.v_REG_VIG;

        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            ERR_MSG  := 'No se encontro informacion del canal de salida por tipo de opercion: ' ||
                        p_COD_TPO_OPE_AOS || ' y para el banco ' ||
                        p_COD_BCO_DTN || ' p_COD_MT_SWF:' || p_COD_MT_SWF;
            ERR_CODE := SQLCODE;
            --RAISE; CAFF PARA PRUEBAS
          WHEN OTHERS THEN

            ERR_CODE    := SQLCODE;
            ERR_MSG     := SUBSTR(SQLERRM, 1, 300);
            p_s_mensaje := err_code || '-' || err_msg;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(NULL,
                                               v_NOM_PCK,
                                               ERR_MSG,
                                               v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);
        END;

        --Verificamos que exista informacion del canal del tipo de operacion
        IF (NVL(v_COD_SIS_SAL_TPO_OPE, '#') <> '#') THEN

          --Busca los horarios Cierre de Canal de Salida
          BEGIN

            --SELECT HOR_ICO_SIS, HOR_CRR_SIS INTO p_HOR_INI, p_HOR_CRR
            SELECT HOR_CRR_SIS
              INTO v_HOR_CRR
              FROM PABS_DT_SISTM_ENTRD_SALID
             WHERE COD_SIS_ENT_SAL = v_COD_SIS_SAL_TPO_OPE
               AND FLG_VGN = PKG_PAB_CONSTANTES.V_REG_VIG;

          EXCEPTION
            WHEN NO_DATA_FOUND THEN
              err_code := SQLCODE;
              err_msg  := SUBSTR(SQLERRM, 1, 300);
              RAISE;
            WHEN OTHERS THEN
              err_code := SQLCODE;
              err_msg  := SUBSTR(SQLERRM, 1, 300);
              PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                                 v_NOM_PCK,
                                                 err_msg,
                                                 v_NOM_SP);
              p_ERROR     := PKG_PAB_CONSTANTES.v_ERROR;
              p_s_mensaje := err_code || '-' || err_msg;
              RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);

          END;

          BEGIN
            --Obtenemos la hora del sistema
            SELECT TO_NUMBER(REPLACE(TO_CHAR(SYSDATE, 'HH24:MI'), ':', ''))
              INTO v_HOR_SIS
              FROM DUAL;

          EXCEPTION
            WHEN NO_DATA_FOUND THEN
              err_code := SQLCODE;
              err_msg  := SUBSTR(SQLERRM, 1, 300);
              RAISE;
            WHEN OTHERS THEN
              err_code := SQLCODE;
              err_msg  := SUBSTR(SQLERRM, 1, 300);
              PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                                 v_NOM_PCK,
                                                 err_msg,
                                                 v_NOM_SP);
              p_ERROR     := PKG_PAB_CONSTANTES.v_ERROR;
              p_s_mensaje := err_code || '-' || err_msg;
              RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);

          END;

          IF v_HOR_SIS < v_HOR_CRR THEN
            v_COD_SIS_SAL_FIN := v_COD_SIS_SAL_TPO_OPE; -- Si Hora Sistema menor a Hora Cierre, Devuelve COMBANC
            p_COD_SIS_SAL     := v_COD_SIS_SAL_FIN;

          ELSE
            v_COD_SIS_SAL_FIN := PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_LBTR; -- Devuelve LBTR
            p_COD_SIS_SAL     := v_COD_SIS_SAL_FIN;

          END IF;

          --PKG_PAB_OPERACION_DEV.Sp_PAB_BUS_SIS_SAL (p_COD_BCO_DTN,PKG_PAB_CONSTANTES.V_COD_MT202, v_COD_SIS_SAL_FIN, p_ERROR);-- Valida Horario CIERRE !!!

        ELSE
          --Por defecto debe ser canal LBTR si no lo encuentra en la parametria.
          v_COD_SIS_SAL_FIN := PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_LBTR;
          p_COD_SIS_SAL     := v_COD_SIS_SAL_FIN;

        END IF;

      ELSE
        --Por defecto debe ser canal swift si es distinto de CLP
        v_COD_SIS_SAL_FIN := PKG_PAB_CONSTANTES.v_SRT_COD_SIS_SAL_SWF;
        p_COD_SIS_SAL     := v_COD_SIS_SAL_FIN;

      END IF;

    END IF;

  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                         v_NOM_PCK,
                                         err_msg,
                                         v_NOM_SP);
      p_COD_SIS_SAL := PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_LBTR;
    WHEN OTHERS THEN
      err_code := SQLCODE;
      err_msg  := SUBSTR(SQLERRM, 1, 300);
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                         v_NOM_PCK,
                                         err_msg,
                                         v_NOM_SP);
      p_ERROR     := PKG_PAB_CONSTANTES.v_ERROR;
      p_s_mensaje := err_code || '-' || err_msg;
      RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);

  END Sp_PAB_BUS_CANAL_SALIDA;

  --***********************************************************************************************
  -- Funcion/Procedimiento: SP_ACT_ID_OPE
  -- Objetivo: PROCEDIMIENTO QUE ACTUALIZA ID POR OPERACION
  -- Sistema: PAB
  -- Base de Datos: DGBMSEGDB01_NGALTM
  -- Tablas Usadas: PABS_DT_DETLL_OPRCN
  -- Fecha: 29/12/16
  -- Autor: Santander
  -- Input:
  -- p_NUM_REF_SWF : Referencia Swift
  -- p_NUM_FOL_OPE : Numero folio operacion
  -- p_FEC_ISR_OPE  : Fecha insercion operacion
  -- p_NUM_IDF_GTR_DOC : Numero Documento Gestor
  -- p_FLG_EGR_ING    : Flag Egreso o Ingreso
  -- p_COD_MT_SWF     : Codigo de mt Swift
  -- Output:
  -- p_ERROR -> Inidca si hay error
  -- Input/Output: N/A
  -- Retorno: N/A.
  -- Observaciones: <Fecha y Detalle de ultimos cambios>
  PROCEDURE SP_ACT_ID_OPE(p_NUM_REF_SWF     IN VARCHAR2,
                          p_NUM_FOL_OPE     IN NUMBER,
                          p_FEC_ISR_OPE     IN DATE,
                          p_NUM_IDF_GTR_DOC IN VARCHAR2,
                          p_FLG_EGR_ING     IN NUMBER,
                          p_COD_MT_SWF      IN NUMBER,
                          p_ERROR           OUT NUMBER) IS
    --
    v_NOM_SP      VARCHAR2(30) := 'SP_ACT_ID_OPE';
    V_FEC_ISR_OPE DATE;
    v_NUM_REF_SWF CHAR(16) := p_NUM_REF_SWF;
    v_COD_MT_SWF  NUMBER(12) := p_COD_MT_SWF;

  BEGIN

    --103 / 202 / 205 / 200
    IF (v_COD_MT_SWF = PKG_PAB_CONSTANTES.V_COD_MT505) THEN

      UPDATE PABS_DT_MNSJE_503
         SET NUM_IDF_GTR_DOC = p_NUM_IDF_GTR_DOC
       WHERE NUM_FOL_MSJ_503 = p_NUM_FOL_OPE
         AND FEC_ISR_MSJ_503 = p_FEC_ISR_OPE;

      --MT 503
    ELSE

      IF (p_NUM_FOL_OPE = 0) THEN

        UPDATE PABS_DT_DETLL_OPRCN
           SET NUM_IDF_GTR_DOC = p_NUM_IDF_GTR_DOC
         WHERE NUM_REF_SWF = v_NUM_REF_SWF
           AND COD_MT_SWF = v_COD_MT_SWF
           AND FLG_EGR_ING = p_FLG_EGR_ING;

      ELSE

        UPDATE PABS_DT_DETLL_OPRCN
           SET NUM_IDF_GTR_DOC = p_NUM_IDF_GTR_DOC
         WHERE NUM_FOL_OPE = p_NUM_FOL_OPE
           AND FEC_ISR_OPE = p_FEC_ISR_OPE
           AND COD_MT_SWF = v_COD_MT_SWF
           AND FLG_EGR_ING = p_FLG_EGR_ING;
      END IF;

    END IF;

    COMMIT;

  EXCEPTION
    WHEN OTHERS THEN
      err_code    := SQLCODE;
      err_msg     := SUBSTR(SQLERRM, 1, 300);
      p_s_mensaje := err_code || '-' || err_msg;
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                         v_NOM_PCK,
                                         err_msg,
                                         v_NOM_SP);
      p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);

  END SP_ACT_ID_OPE;

  -- ***********************************************************************************************
  -- Funcion/Procedimiento: Sp_PAB_OBT_REF_MVTO
  -- Objetivo: Procedimiento almacenado que crea el campo ReferMvto para contabilidad para Abono cta. cte.
  -- Sistema: PAB
  -- Base de Datos: DGBMSEGDB01_NGALTM
  -- Tablas Usadas: PABS_DT_DETLL_NOMNA, PABS_DT_SISTM_ENTRD_SALID, PABS_DT_CBCRA_NOMNA, TCDT040, TCDTBAI
  -- Fecha: 21/06/16
  -- Autor: Santander
  -- Input:
  -- p_COD_BCO_STD    -> Codigo banco Santander '0035' por defecto
  -- p_COD_SCR        -> Codigo Sucursal '0000' por defecto.
  -- p_NUM_CTA_CTE    -> Numero de cuenta corriente
  -- Output:
  -- p_REFER_MVTO     -> Referencia Movimiento contable
  -- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
  -- Input/Output: N/A
  -- Retorno: N/A.
  -- Observaciones: <Fecha y Detalle de ultimos cambios>
  -- ***********************************************************************************************      --
  PROCEDURE Sp_PAB_OBT_REF_MVTO(p_COD_BCO_STR IN CHAR,
                                p_COD_SCR     IN CHAR,
                                p_NUM_CTA_CTE IN CHAR,
                                p_REFER_MVTO  OUT CHAR,
                                p_ERROR       OUT NUMBER) IS

    v_NOM_SP      VARCHAR2(30) := 'Sp_PAB_OBT_REF_MVTO';
    v_NUM_CTA_CTE VARCHAR2(100);
    v_COD_BCO_STR VARCHAR2(4);
    v_COD_SCR     VARCHAR2(4);

    v_REFER_MVTO VARCHAR2(100); -- Campo compuesto a devolver(TPN)

  BEGIN

    --Banco
    IF p_COD_BCO_STR IS NULL THEN
      v_COD_BCO_STR := PKG_PAB_CONSTANTES.V_COD_ENT_BAN_SANT;
    ELSE
      v_COD_BCO_STR := p_COD_BCO_STR;
    END IF;

    --Sucursal
    IF p_COD_SCR IS NULL THEN
      v_COD_SCR := '0000';
    ELSE
      v_COD_SCR := p_COD_SCR;
    END IF;

    p_REFER_MVTO := v_COD_BCO_STR || v_COD_SCR ||
                    LPAD(p_NUM_CTA_CTE, 12, 0);
    p_ERROR      := PKG_PAB_CONSTANTES.v_OK;

  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      err_code := SQLCODE;
      err_msg  := SUBSTR(SQLERRM, 1, 300);
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                         v_NOM_PCK,
                                         err_msg,
                                         v_NOM_SP);
      p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
    WHEN OTHERS THEN
      err_code := SQLCODE;
      err_msg  := SUBSTR(SQLERRM, 1, 300);
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                         v_NOM_PCK,
                                         err_msg,
                                         v_NOM_SP);
      p_ERROR     := PKG_PAB_CONSTANTES.v_ERROR;
      p_s_mensaje := err_code || '-' || err_msg;
      RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);

  END Sp_PAB_OBT_REF_MVTO;

  --***********************************************************************************************
  -- Funcion/Procedimiento: FN_PAB_BUS_EST_OPE
  -- Objetivo: Compara el estado de una operación con la que figura en la tabla de detalle de nómina.
  -- Sistema: PAB
  -- Base de Datos: DGBMSEGDB01
  -- Tablas Usadas: PABS_DT_DETLL_NOMNA
  -- Fecha: 09/11/2016
  -- Autor: Santander
  -- Input:
  -- P_NUM_FOL_OPE -> numero de nomina
  -- P_COD_EST_AOS -> codigo estado nomina
  -- P_FEC_ISR_OPE -> fecha insercion operacion
  -- N/A
  -- Output:
  -- N/A.
  -- Input/Output:
  -- N/A.
  -- Retorno:
  -- P_FLAG_EST -> valor de retorno flag de comparacion
  -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
  --***********************************************************************************************
  FUNCTION FN_PAB_BUS_EST_OPE(P_NUM_FOL_OPE IN NUMBER,
                              P_COD_EST_AOS IN CHAR,
                              P_FEC_ISR_OPE IN DATE) RETURN NUMBER IS
    --
    V_NOM_SP         VARCHAR2(22) := 'FN_PAB_BUS_EST_OPE';
    P_FLAG_EST_OPE   NUMBER;
    V_COD_EST_AOS    NUMBER;
    v_COD_EST_RESULT NUMBER;
  BEGIN
    --
    v_COD_EST_RESULT := DBO_PAB.PKG_PAB_UTILITY.FN_PAB_OBT_COD_DSC_EST(P_COD_EST_AOS,
                                                                       PKG_PAB_CONSTANTES.V_IND_OPE);
    --
    BEGIN
      SELECT OP.COD_EST_AOS
        INTO V_COD_EST_AOS
        FROM PABS_DT_DETLL_NOMNA OP
       WHERE NUM_FOL_OPE = P_NUM_FOL_OPE
         AND FEC_ISR_OPE = P_FEC_ISR_OPE
         AND OP.COD_EST_AOS = v_COD_EST_RESULT;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        V_COD_EST_AOS := 0;
    END;

    --
    IF V_COD_EST_AOS = v_COD_EST_RESULT THEN
      P_FLAG_EST_OPE := 0;
    ELSE
      P_FLAG_EST_OPE := 1;
    END IF;

    --
    RETURN P_FLAG_EST_OPE;
    --
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      err_code := SQLCODE;
      err_msg  := SUBSTR(SQLERRM, 1, 300);
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                         v_NOM_PCK,
                                         err_msg,
                                         v_NOM_SP);
    WHEN OTHERS THEN
      err_code := SQLCODE;
      err_msg  := SUBSTR(SQLERRM, 1, 300);
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                         v_NOM_PCK,
                                         err_msg,
                                         v_NOM_SP);
      p_s_mensaje := err_code || '-' || err_msg;
      RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);
      --
  END FN_PAB_BUS_EST_OPE;

  --***********************************************************************************************
  -- Funcion/Procedimiento: FN_PAB_BUS_EST_OPE_LIB
  -- Objetivo: Comprara el estado de una nomina con la que figura en la tabla de nominas.
  -- Sistema: PAB
  -- Base de Datos: DGBMSEGDB01
  -- Tablas Usadas: PABS_DT_DETLL_NOMNA
  -- Fecha: 09/11/2016
  -- Autor: Santander
  -- Input:
  -- P_NUM_FOL_OPE -> numero de nomina
  -- P_COD_EST_AOS -> codigo estado nomina
  -- P_FEC_ISR_OPE -> fecha insercion operacion
  -- N/A
  -- Output:
  -- N/A.
  -- Input/Output:
  -- N/A.
  -- Retorno:
  -- P_FLAG_EST -> valor de retorno flag de comparacion
  -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
  --***********************************************************************************************
  FUNCTION FN_PAB_BUS_EST_OPE_LIB(P_NUM_FOL_OPE IN NUMBER,
                                  P_COD_EST_AOS IN CHAR,
                                  P_FEC_ISR_OPE IN DATE) RETURN NUMBER IS
    --
    V_NOM_SP         VARCHAR2(22) := 'FN_PAB_BUS_EST_OPE_LIB';
    P_FLAG_EST_OPE   NUMBER;
    V_COD_EST_AOS    NUMBER;
    v_COD_EST_RESULT NUMBER;
  BEGIN
    --
    v_COD_EST_RESULT := DBO_PAB.PKG_PAB_UTILITY.FN_PAB_OBT_COD_DSC_EST(P_COD_EST_AOS,
                                                                       PKG_PAB_CONSTANTES.V_IND_OPE);
    --
    BEGIN
      SELECT OP.COD_EST_AOS
        INTO V_COD_EST_AOS
        FROM PABS_DT_DETLL_OPRCN OP
       WHERE OP.NUM_FOL_OPE = P_NUM_FOL_OPE
         AND OP.FEC_ISR_OPE = P_FEC_ISR_OPE
         AND OP.COD_EST_AOS = v_COD_EST_RESULT;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        V_COD_EST_AOS := 0;
    END;

    --
    IF V_COD_EST_AOS = v_COD_EST_RESULT THEN
      P_FLAG_EST_OPE := 0;
    ELSE
      P_FLAG_EST_OPE := 1;
    END IF;

    --
    RETURN P_FLAG_EST_OPE;
    --
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      err_code := SQLCODE;
      err_msg  := SUBSTR(SQLERRM, 1, 300);
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                         v_NOM_PCK,
                                         err_msg,
                                         v_NOM_SP);
    WHEN OTHERS THEN
      err_code := SQLCODE;
      err_msg  := SUBSTR(SQLERRM, 1, 300);
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                         v_NOM_PCK,
                                         err_msg,
                                         v_NOM_SP);
      p_s_mensaje := err_code || '-' || err_msg;
      RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);
      --
  END FN_PAB_BUS_EST_OPE_LIB;

  /************************************************************************************************
  -- Funcion/Procedimiento: Sp_PAB_GES_REC_CMN
  -- Objetivo: Procedimiento que gestiona los rechazos de mensajes a combanc que pueden volver a ser gestionados
  -- Sistema: DBO_PAB.
  -- Base de Datos: DBO_PAB.
  -- Tablas Usadas: PABS_DT_DETLL_OPRCN
  --
  -- Fecha: 30/06/17
  -- Autor: GCP
  -- Input: p_NUM_REF_SWF      Referencia de la operación
  --        p_NUM_FOL_OPE      Numero de Folio
  --        p_FEC_ISR_OPE      Fecha de inserción
  -- Output:
  --       p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
  -- Input/Output: N/A
  -- Retorno: N/A.
  -- Observaciones: <Fecha y Detalle de ultimos cambios>
  --***********************************************************************************************/
  PROCEDURE Sp_PAB_GES_REC_CMN(p_NUM_REF_SWF IN CHAR,
                               p_NUM_FOL_OPE OUT NUMBER,
                               p_FEC_ISR_OPE OUT DATE,
                               p_ERROR       OUT NUMBER) IS
    v_NOM_SP          VARCHAR2(30) := 'Sp_PAB_GES_REC_CMN';
    v_NUM_REF_SWF     CHAR(16) := p_NUM_REF_SWF;
    v_COD_EST_AOS     NUMBER;
    v_RESULT          NUMBER;
    v_NUM_FOL_OPE_NOM PABS_DT_DETLL_NOMNA.NUM_FOL_NMN%TYPE;

  BEGIN

    BEGIN

      --Obtenemos la información de la operación
      SELECT DETOPE.NUM_FOL_OPE,
             DETOPE.FEC_ISR_OPE,
             DETOPE.COD_EST_AOS,
             DETOPE.NUM_FOL_NMN
        INTO p_NUM_FOL_OPE, p_FEC_ISR_OPE, v_COD_EST_AOS, v_NUM_FOL_OPE_NOM
        FROM PABS_DT_DETLL_OPRCN       DETOPE,
             PABS_DT_OPRCN_INFCN_OPCON DETOPC,
             PABS_DT_SISTM_ENTRD_SALID ENT,
             PABS_DT_SISTM_ENTRD_SALID SAL
       WHERE DETOPE.NUM_REF_SWF = v_NUM_REF_SWF
         AND DETOPE.NUM_FOL_OPE = DETOPC.NUM_FOL_OPE(+)
         AND DETOPE.FEC_ISR_OPE = DETOPC.FEC_ISR_OPE(+)
         AND DETOPE.COD_SIS_ENT = ENT.COD_SIS_ENT_SAL
         AND DETOPE.COD_SIS_SAL = SAL.COD_SIS_ENT_SAL
         AND DETOPE.FLG_EGR_ING = PKG_PAB_CONSTANTES.V_FLG_EGR --Solo operaciones egreso
         AND DETOPE.COD_EST_AOS = PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEREC; --Solo operaciones rechazadas

    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        err_code := SQLCODE;
        err_msg  := 'No se encontro la operación de egreso con Referencia: ' ||
                    p_NUM_REF_SWF;
        PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                           v_NOM_PCK,
                                           err_msg,
                                           v_NOM_SP);
        p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
        RAISE;

      WHEN OTHERS THEN
        err_code    := SQLCODE;
        err_msg     := SUBSTR(SQLERRM, 1, 300);
        p_s_mensaje := err_code || '-' || err_msg;
        PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                           v_NOM_PCK,
                                           err_msg,
                                           v_NOM_SP);
        p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
        RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);
    END;

    --Revivimos la operación
    BEGIN

      UPDATE PABS_DT_DETLL_OPRCN
         SET NUM_REF_SWF     = NULL,
             FEC_ENv_RCp_SWF = NULL,
             HOR_ENv_RCp_SWF = NULL,
             COD_SIS_SAL     = PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_LBTR,
             COD_EST_AOS     = PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT,
             NUM_IDF_GTR_DOC = NULL
       WHERE NUM_FOL_OPE = p_NUM_FOL_OPE
         AND FEC_ISR_OPE = p_FEC_ISR_OPE;

      --Grabamos en la bitacora el cambio
      v_DES_BIT := 'Operación revivida por rechazo Combanc';
      PKG_PAB_TRACKING.Sp_PAB_INS_BIT_OPE(p_NUM_FOL_OPE,
                                          p_FEC_ISR_OPE,
                                          'ESB',
                                          v_COD_EST_AOS,
                                          PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT,
                                          v_DES_BIT,
                                          v_RESULT);

    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        err_code := SQLCODE;
        err_msg  := SUBSTR(SQLERRM, 1, 300);
        PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                           v_NOM_PCK,
                                           err_msg,
                                           v_NOM_SP);
        p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
        RAISE;

      WHEN OTHERS THEN
        err_code    := SQLCODE;
        err_msg     := SUBSTR(SQLERRM, 1, 300);
        p_s_mensaje := err_code || '-' || err_msg;
        PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                           v_NOM_PCK,
                                           err_msg,
                                           v_NOM_SP);
        p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
    END;

    --Si existe en nomina actualizamos su información
    IF v_NUM_FOL_OPE_NOM > 0 THEN

      --Actualizamos la operacion
      UPDATE PABS_DT_DETLL_NOMNA
         SET NUM_REF_SWF     = NULL,
             FEC_ENv_RCp_SWF = NULL,
             HOR_ENv_RCp_SWF = NULL,
             COD_SIS_SAL     = PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_LBTR,
             COD_EST_AOS     = PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT
       WHERE NUM_FOL_OPE = p_NUM_FOL_OPE
         AND FEC_ISR_OPE = p_FEC_ISR_OPE;

    END IF;

  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      err_code := SQLCODE;
      err_msg  := SUBSTR(SQLERRM, 1, 300);
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                         v_NOM_PCK,
                                         err_msg,
                                         v_NOM_SP);
      p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;

    WHEN OTHERS THEN
      err_code    := SQLCODE;
      err_msg     := SUBSTR(SQLERRM, 1, 300);
      p_s_mensaje := err_code || '-' || err_msg;
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                         v_NOM_PCK,
                                         err_msg,
                                         v_NOM_SP);
      p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);

  END Sp_PAB_GES_REC_CMN;

  /************************************************************************************************
  -- Funcion/Procedimiento: Sp_PAB_GES_REC_DVPDCV
  -- Objetivo: Procedimiento que gestiona los rechazos de mensajes a combanc de operaciones que son DVPDCV
  -- Sistema: DBO_PAB.
  -- Base de Datos: DBO_PAB.
  -- Tablas Usadas: PABS_DT_DETLL_OPRCN
  --
  -- Fecha: 30/06/17
  -- Autor: GCP
  -- Input: p_NUM_FOL_OPE      Numero de folio de operación
  --        p_FEC_ISR_OPE      Fecha de inserción de operación
  --        p_GLS_MTV_RSH      Glosa Motivo de Rechazo
  --        p_COD_SIS_ENT      Sistema de entrada
  --        p_NUM_MOV_CTB      Numero de movimiento Contable (Cuentas Corrientes)
  -- Output:
  --       p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
  -- Input/Output: N/A
  -- Retorno: N/A.
  -- Observaciones: <Fecha y Detalle de ultimos cambios>
  --***********************************************************************************************/
  PROCEDURE Sp_PAB_GES_REC_DVPDCV(p_NUM_FOL_OPE IN NUMBER,
                                  p_FEC_ISR_OPE IN DATE,
                                  p_GLS_MTV_RSH IN VARCHAR2,
                                  p_COD_SIS_ENT IN CHAR,
                                  p_NUM_MOV_CTB IN CHAR,
                                  p_ERROR       OUT NUMBER) IS
    v_NOM_SP VARCHAR2(30) := 'Sp_PAB_GES_REC_DVPDCV';
    v_RESULT NUMBER;

  BEGIN

    --
    BEGIN

      UPDATE PABS_DT_DETLL_OPRCN
         SET COD_EST_AOS = PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDEV, --Devuelta
             NUM_MVT_CTB = p_NUM_MOV_CTB,
             COD_SIS_SAL = PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_CTACTE
       WHERE NUM_FOL_OPE = p_NUM_FOL_OPE
         AND FEC_ISR_OPE = p_FEC_ISR_OPE;

      --Grabamos en la bitacora el cambio
      v_DES_BIT := 'Operación abonada al ordenante N° Movimiento:' ||
                   p_NUM_MOV_CTB;
      PKG_PAB_TRACKING.Sp_PAB_INS_BIT_OPE(p_NUM_FOL_OPE,
                                          p_FEC_ISR_OPE,
                                          'ESB',
                                          PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNREC, -- Rechazada
                                          PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDEV, -- Devuelta
                                          v_DES_BIT,
                                          v_RESULT);

    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        err_code := SQLCODE;
        err_msg  := SUBSTR(SQLERRM, 1, 300);
        PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                           v_NOM_PCK,
                                           err_msg,
                                           v_NOM_SP);
        p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
        RAISE;

      WHEN OTHERS THEN
        err_code    := SQLCODE;
        err_msg     := SUBSTR(SQLERRM, 1, 300);
        p_s_mensaje := err_code || '-' || err_msg;
        PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                           v_NOM_PCK,
                                           err_msg,
                                           v_NOM_SP);
        p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
    END;

  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      err_code := SQLCODE;
      err_msg  := SUBSTR(SQLERRM, 1, 300);
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                         v_NOM_PCK,
                                         err_msg,
                                         v_NOM_SP);
      p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;

    WHEN OTHERS THEN
      err_code    := SQLCODE;
      err_msg     := SUBSTR(SQLERRM, 1, 300);
      p_s_mensaje := err_code || '-' || err_msg;
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                         v_NOM_PCK,
                                         err_msg,
                                         v_NOM_SP);
      p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);

  END Sp_PAB_GES_REC_DVPDCV;

  /******************************************************************************************************************************
  -- Funcion/Procedimiento: Sp_PAB_BUS_OPE_CAMBIO_CANAL
  -- Objetivo: Procedimiento almacenado que retorna operaciones abonadas para el cambio de canal
  -- Sistema: DBO_PAB.
  -- Base de Datos: DBO_PAB.
  -- Tablas Usadas: PABS_DT_DETLL_OPRCN
  --
  -- Fecha: 04-01-2018
  -- Autor: Santander CAH
  -- Input:
  --  p_COD_SIS_SAL -->Area de abono
  --  p_COD_BCO     -->Banco Origen
  --  p_COD_MT_SW   -->Código mt
  --  p_NUM_REF_SWF -->referencia Operación
  -- Output:
  --       p_CURSOR        Cursor de salida con operaciones
  --       p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
  -- Input/Output: N/A
  -- Retorno: N/A.
  -- Observaciones: <Fecha y Detalle de ultimos cambios>
  --******************************************************************************************************************************/
  PROCEDURE Sp_PAB_BUS_OPE_CAMBIO_CANAL(p_COD_SIS_SAL IN CHAR,
                                        p_COD_BCO     IN CHAR,
                                        p_COD_MT_SWF  IN NUMBER,
                                        p_NUM_REF_SWF IN CHAR,
                                        p_CURSOR      OUT SYS_REFCURSOR,
                                        p_ERROR       OUT NUMBER) IS

    V_NOM_SP      VARCHAR2(30) := 'Sp_PAB_BUS_OPE_CAMBIO_CANAL';
    v_COD_SIS_SAL CHAR(10);
    v_COD_BCO     CHAR(11);
    v_COD_MT_SWF  NUMBER;
    v_NUM_REF_SWF CHAR(16);

  BEGIN

    --AREA
    IF p_COD_SIS_SAL = '-1' THEN
      v_COD_SIS_SAL := NULL;
    ELSE
      v_COD_SIS_SAL := p_COD_SIS_SAL;
    END IF;

    --BANCO
    IF p_COD_BCO = '-1' THEN
      v_COD_BCO := NULL;
    ELSE
      v_COD_BCO := p_COD_BCO;
    END IF;

    --MT
    IF p_COD_MT_SWF = -1 THEN
      v_COD_MT_SWF := NULL;
    ELSE
      v_COD_MT_SWF := p_COD_MT_SWF;
    END IF;

    IF NVL(p_NUM_REF_SWF, '#') = '#' THEN
      v_NUM_REF_SWF := NULL;
    ELSE
      v_NUM_REF_SWF := UPPER(p_NUM_REF_SWF);
    END IF;

    OPEN p_CURSOR FOR
      SELECT OP.COD_MT_SWF AS MT,
             OP.NUM_FOL_OPE AS FOLIO,
             OP.NUM_REF_SWF AS REFERENCIA,
             OP.COD_SIS_SAL AS COD_AREA,
             PKG_PAB_UTILITY.FN_PAB_OBT_DSC_SIS_ENT_SAL(OP.COD_SIS_SAL) AS DESC_AREA,
             OP.IMP_OPE AS MONTO,
             OP.COD_DVI AS MONEDA,
             OP.COD_BCO_ORG AS BCO_ORIGEN,
             PKG_PAB_UTILITY.FN_PAB_OBT_DSC_BCO(OP.COD_BCO_ORG) AS DSC_BCO_ORG,
             OP.FEC_ISR_OPE AS FECHA
        FROM PABS_DT_DETLL_OPRCN OP
       WHERE OP.COD_EST_AOS = PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEABN -- 15 Abonada
         AND OP.COD_TPO_OPE_AOS <> PKG_PAB_CONSTANTES.V_STR_TPO_OPE_DEVFDOS -- No 'DEVFDOS'
         AND OP.COD_SIS_SAL NOT IN
             (PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_CTACTE,
              PKG_PAB_CONSTANTES.V_COD_TIP_ABO_XDIS) -- Cuenta Corriente no puede ser
         AND OP.FLG_EGR_ING = PKG_PAB_CONSTANTES.V_FLG_ING -- 1 Ingresos
         AND OP.COD_SIS_SAL = NVL(v_COD_SIS_SAL, OP.COD_SIS_SAL)
         AND NVL(OP.COD_BCO_ORG, '#') =
             NVL(v_COD_BCO, NVL(OP.COD_BCO_ORG, '#'))
         AND OP.COD_MT_SWF = NVL(v_COD_MT_SWF, OP.COD_MT_SWF)
         AND UPPER(NVL(OP.NUM_REF_SWF, '#')) =
             NVL(v_NUM_REF_SWF, NVL(OP.NUM_REF_SWF, '#'))
         AND OP.FEC_ISR_OPE BETWEEN v_dia_ini AND v_dia_fin;

    p_ERROR := PKG_PAB_CONSTANTES.v_OK;
    --Procedimiento ejecutado de forma correcta
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      err_code := SQLCODE;
      err_msg  := SUBSTR(SQLERRM, 1, 300);
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                         v_NOM_PCK,
                                         err_msg,
                                         v_NOM_SP);
      p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
    WHEN OTHERS THEN
      err_code := SQLCODE;
      err_msg  := SUBSTR(SQLERRM, 1, 300);
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                         v_NOM_PCK,
                                         err_msg,
                                         v_NOM_SP);

      p_ERROR     := PKG_PAB_CONSTANTES.v_ERROR;
      p_s_mensaje := err_code || '-' || err_msg;
      RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);

  END Sp_PAB_BUS_OPE_CAMBIO_CANAL;

  /******************************************************************************************************************************
  -- Funcion/Procedimiento: Sp_PAB_ACT_CANAL_ABONO
  -- Objetivo: Procedimiento almacenado que actualiza el canal de salida de la operación
  -- Sistema: DBO_PAB.
  -- Base de Datos: DBO_PAB.
  -- Tablas Usadas: PABS_DT_USRIO
  --
  -- Fecha: 03-01-2018
  -- Autor: Santander CAH
  -- Input:
  --  p_NUM_FOL_OPE  --> Folio operación
  --  p_FEC_ISR_OPE  --> Fecha operación
  --  p_COD_SIS_SAL  --> Cod. sistema entrada nuevo
  --  p_COD_USR      --> Usuario sistema
  -- Output:
  --       p_CURSOR  --> Cursor de salida con operaciones
  --       p_ERROR: --> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
  -- Input/Output: N/A
  -- Retorno: N/A.
  -- Observaciones: <Fecha y Detalle de ultimos cambios>
  --******************************************************************************************************************************/
  PROCEDURE Sp_PAB_ACT_CANAL_ABONO(p_NUM_FOL_OPE IN NUMBER,
                                   p_FEC_ISR_OPE IN DATE,
                                   p_COD_SIS_SAL IN CHAR,
                                   p_COD_USR     IN CHAR,
                                   p_ERROR       OUT NUMBER) IS

    v_NOM_SP      VARCHAR2(30) := 'Sp_PAB_ACT_CANAL_ABONO';
    v_DSC_CANAL   VARCHAR2(50);
    v_CANAL_USU   CHAR(10);
    v_HORA_CIERRE NUMBER(4);
    v_HORA_ACTUAL NUMBER(4) := TO_NUMBER(TO_CHAR(SYSDATE, 'HH24MI'));
    err_bus_can_sal exception;

    --Variables de notificación.
    v_FLG_NTF     NUMBER;
    v_CAN_OPE_NOT NUMBER;
    p_ADJUNTO     NUMBER;
    p_ID          VARCHAR2(10);
    p_MENSAJE_SWF VARCHAR2(300);
    p_ASUNTO      VARCHAR2(300);
    p_CORREO      VARCHAR2(300);
    v_GLS_ADC_EST VARCHAR2(210);

  BEGIN

    p_ERROR := PKG_PAB_CONSTANTES.v_OK;

    BEGIN

      --Busca descripción del canal
      SELECT SIS.DSC_SIS_ENT_SAL
        INTO v_DSC_CANAL
        FROM PABS_DT_SISTM_ENTRD_SALID SIS
       WHERE SIS.COD_SIS_ENT_SAL = p_COD_SIS_SAL;

    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        RAISE err_bus_can_sal;

      WHEN OTHERS THEN
        err_code    := SQLCODE;
        err_msg     := SUBSTR(SQLERRM, 1, 300);
        p_s_mensaje := err_code || '-' || err_msg;
        PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                           v_NOM_PCK,
                                           err_msg,
                                           v_NOM_SP);
        p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
        RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);
    END;

    IF PKG_PAB_CONSTANTES.V_HORA_LIMITE > v_HORA_ACTUAL THEN

      v_GLS_ADC_EST := 'Se abono en la Cuenta Contable del área: ' ||
                       v_DSC_CANAL;

      BEGIN

        UPDATE PABS_DT_DETLL_OPRCN OP
           SET OP.COD_SIS_SAL = p_COD_SIS_SAL,
               OP.GLS_ADC_EST = v_GLS_ADC_EST --Se abono en la Cuenta Contable del area: Abonos Masivos
         WHERE OP.NUM_FOL_OPE = p_NUM_FOL_OPE
           AND OP.FEC_ISR_OPE = p_FEC_ISR_OPE;

        COMMIT;

        --Llamamos al procedimiento de Bitacora
        v_DES_BIT := 'Cambio de canal de la operación, al área ' ||
                     v_DSC_CANAL;
        PKG_PAB_TRACKING.Sp_PAB_INS_BIT_OPE(p_NUM_FOL_OPE,
                                            p_FEC_ISR_OPE,
                                            p_COD_USR,
                                            PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEABN,
                                            PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEABN,
                                            v_DES_BIT,
                                            p_ERROR);

        --Se notifica el cambio de canal a la operación abonada
        --                PKG_PAB_NOTIFICACION.SP_PAB_DET_CRRO_TIBCO(NULL,
        --                                                p_NUM_FOL_OPE,
        --                                                p_FEC_ISR_OPE,
        --                                                'CCABN',
        --                                                NULL,-- p_ERROR_SWF s,
        --                                                p_COD_SIS_SAL,--p_COD_SIS_ENT_SAL,
        --                                                p_ADJUNTO,
        --                                                p_ID ,
        --                                                p_MENSAJE_SWF,
        --                                                p_ASUNTO,
        --                                                p_CORREO,
        --                                                p_ERROR);

        p_ERROR := PKG_PAB_CONSTANTES.V_COD_CMB_CNL_OK;

      EXCEPTION
        WHEN DUP_VAL_ON_INDEX THEN
          err_code := SQLCODE;
          err_msg  := SUBSTR(SQLERRM, 1, 300);
          P_ERROR  := PKG_PAB_CONSTANTES.V_ERROR;
          PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD(err_code,
                                             V_NOM_PCK,
                                             err_msg,
                                             V_NOM_SP);
        WHEN OTHERS THEN
          err_code := SQLCODE;
          err_msg  := SUBSTR(SQLERRM, 1, 300);
          PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
          p_ERROR     := PKG_PAB_CONSTANTES.v_ERROR;
          p_s_mensaje := err_code || '-' || err_msg;
          RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);

      END;

    ELSE
      --Fuera de Horario
      p_ERROR := PKG_PAB_CONSTANTES.V_COD_CMB_CNL_NOHOR;

    END IF;

  END Sp_PAB_ACT_CANAL_ABONO;

  --***********************************************************************************************
  -- Funcion/Procedimiento: SP_VRFCA_NUM_SWF
  -- Objetivo: Consultar si el numero de referencia swf ya se encuentra procesado.
  -- Sistema: PAB
  -- Base de Datos: DGBMSEGDB01
  -- Tablas Usadas: PABS_DT_DETLL_OPRCN, PABS_DT_DETLL_OPRCN_DCV
  -- Fecha: 07/08/2018
  -- Autor: jbarboza
  -- Input:
  -- P_NUM_REF_SWF -> NUMERO DE REFERENCIA SWIFT
  -- P_BIC_ORIGEN -> BIC BANCARIO
  -- P_CASOS_SWF -> EL CASO QUE DETERMINARA EL CRITERIO DE BUSQUEDA LOS CUALES PUEDEN SER : INGRESO,EGRESO Y DCP
  -- p_fec_vta   -> Fecha del pago
  -- Output:
  -- P_VALIDACION -> VARIABLE VALIDADOR, SI ES 1 EL NUMERO DE REFERENCIA SWIFT YA ESTA REGISTRADO DE LO CONTRARIO SERA 0
  -- P_ERROR      -> DETERMINA SI HUBO UN ERROR EN LA EJECUCION : 1 SI OCURRIO UN  ERROR DE LO CONTRARIO SERA 0
  -- P_MENSAJE    -> MENSAJE QUE SE USARA COM LOG DEL PROCESO
  -- Input/Output:
  -- N/A.
  -- Retorno:
  -- N/A.
  -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
  --***********************************************************************************************
  PROCEDURE sp_vrfca_num_swf(p_num_ref_swf IN CHAR,
                             p_bic_origen  IN CHAR,
                             p_casos_swf   IN NUMBER,
                             p_fec_vta     IN DATE,
                             p_validacion  OUT NUMBER,
                             p_error       OUT NUMBER,
                             p_mensaje     OUT VARCHAR2) IS
    v_cont_existe     NUMBER(2);
    v_nom_sp          VARCHAR2(30) := 'SP_VERIFICAR_NUM_SWF';
    v_fecha_vta       DATE := TRUNC(SYSDATE);
    v_cantidad        NUMBER(1) := 0;
    v_caso_swf_ing    NUMBER(1) := 1;
    v_caso_swf_eg     NUMBER(1) := 2;
    v_caso_swf_otros  NUMBER(1) := 3;
    v_CASO_900_910    NUMBER(1) := 4;
    v_num_ref_swf     CHAR(16) := REPLACE(p_num_ref_swf, CHR(13)); --Limpiamos salto de linea
    v_bic_origen      CHAR(11) := p_bic_origen;
    v_COD_DVI         CHAR(11) := p_bic_origen;
    v_dato_no_existe  NUMBER(1) := 0;
    v_dato_existe     NUMBER(1) := 1;
    v_dato_existe_prt NUMBER(1) := 2;
    v_dato_existe_ope NUMBER(1) := 3;
    err_caso_invalido EXCEPTION;

  BEGIN

    CASE

      WHEN p_casos_swf = v_caso_swf_ing -- caso 1
       THEN

        BEGIN

          SELECT COUNT(1) AS contador
            INTO v_cont_existe
            FROM dbo_pab.pabs_dt_detll_oprcn
           WHERE num_ref_swf = v_num_ref_swf
             AND fec_vta = p_fec_vta
             AND cod_bco_org = v_bic_origen
             AND flg_egr_ing = pkg_pab_constantes.v_flg_ing; -- 1

          IF (v_cont_existe > v_cantidad) THEN
            p_validacion := v_dato_existe;
            p_error      := pkg_pab_constantes.v_ok;
            p_mensaje    := 'El numero de referencia swift : ' ||
                            v_num_ref_swf || ' y el codigo BIC: ' ||
                            v_bic_origen || 'Para la fecha:' ||
                            TO_CHAR(p_fec_vta, 'dd-mm-yyyy') ||
                            ' ya se encuentran registrados.';
          ELSE
            p_validacion := v_dato_no_existe;
            p_error      := pkg_pab_constantes.v_ok;
            p_mensaje    := 'El numero de referencia swift : ' ||
                            v_num_ref_swf || ' y el codigo BIC: ' ||
                            v_bic_origen || 'Para la fecha:' ||
                            TO_CHAR(p_fec_vta, 'dd-mm-yyyy') ||
                            ' son validos para ser procesados.';
          END IF;
        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            err_code  := SQLCODE;
            err_msg   := SUBSTR(SQLERRM, 1, 300) ||
                         'Error en la busquedad del num_ref_swf : ' ||
                         v_num_ref_swf ||
                         'en el caso de las operaciones  de  Ingreso.';
            p_mensaje := err_msg;
            p_error   := pkg_pab_constantes.v_error;
            pkg_pab_tracking.sp_pab_ins_log_bd(err_code,
                                               v_NOM_PCK,
                                               err_msg,
                                               v_nom_sp);
          WHEN OTHERS THEN
            err_code    := SQLCODE;
            err_msg     := SUBSTR(SQLERRM, 1, 300);
            p_s_mensaje := err_code || '-' || err_msg;
            p_error     := pkg_pab_constantes.v_error;
            p_mensaje   := err_msg;
            pkg_pab_tracking.sp_pab_ins_log_bd(err_code,
                                               v_NOM_PCK,
                                               err_msg,
                                               v_nom_sp);
            RAISE_APPLICATION_ERROR(-20000, err_code || p_s_mensaje);
        END;

      WHEN p_casos_swf = v_caso_swf_eg THEN
        -- caso 2

        BEGIN
          SELECT COUNT(1) AS contador
            INTO v_cont_existe
            FROM dbo_pab.pabs_dt_detll_oprcn
           WHERE num_ref_swf = v_num_ref_swf
             AND fec_vta = v_fecha_vta
             AND flg_egr_ing = pkg_pab_constantes.v_flg_egr
             AND cod_est_aos IN
                 (pkg_pab_constantes.v_cod_est_aos_opepag,
                  pkg_pab_constantes.V_COD_EST_AOS_OPEREC);

          IF (v_cont_existe > v_cantidad) THEN
            p_validacion := v_dato_existe;
            p_error      := pkg_pab_constantes.v_ok;
            p_mensaje    := 'La respuesta swift de la referencia: ' ||
                            v_num_ref_swf ||
                            ' ya se encuentra procesada (pagada, rechaza) ';
          ELSE
            p_validacion := v_dato_no_existe;
            p_error      := pkg_pab_constantes.v_ok;
            p_mensaje    := 'La respuesta swift de la referencia: ' ||
                            v_num_ref_swf ||
                            ' es valida para ser procesada ';
          END IF;

        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            err_code  := SQLCODE;
            err_msg   := SUBSTR(SQLERRM, 1, 300) ||
                         'Error en la busquedad del num_ref_swf : ' ||
                         v_num_ref_swf ||
                         'en el caso de las operaciones de  Egreso.';
            p_error   := pkg_pab_constantes.v_error;
            p_mensaje := err_msg;
            pkg_pab_tracking.sp_pab_ins_log_bd(err_code,
                                               v_NOM_PCK,
                                               err_msg,
                                               v_nom_sp);
          WHEN OTHERS THEN
            err_code    := SQLCODE;
            err_msg     := SUBSTR(SQLERRM, 1, 300);
            p_s_mensaje := err_code || '-' || err_msg;
            p_error     := pkg_pab_constantes.v_error;
            p_mensaje   := err_msg;
            pkg_pab_tracking.sp_pab_ins_log_bd(err_code,
                                               v_NOM_PCK,
                                               err_msg,
                                               v_nom_sp);
            RAISE_APPLICATION_ERROR(-20000, err_code || p_s_mensaje);
        END;

      WHEN p_casos_swf = v_caso_swf_otros THEN
        -- caso 3

        BEGIN

          SELECT COUNT(1) AS contador
            INTO v_cont_existe
            FROM dbo_pab.pabs_dt_detll_oprcn_dcv p
           WHERE num_ref_ide_dcv = v_num_ref_swf
             AND fec_isr_msj_swf = v_fecha_vta;

          IF (v_cont_existe > v_cantidad) THEN
            p_validacion := v_dato_existe;
            p_error      := pkg_pab_constantes.v_ok;
            p_mensaje    := 'El numero de referencia swift : ' ||
                            v_num_ref_swf ||
                            ' para el caso de los DVC ya se encuentra registrado.';
          ELSE
            p_validacion := v_dato_no_existe;
            p_error      := pkg_pab_constantes.v_ok;
            p_mensaje    := 'El numero de referencia swift : ' ||
                            v_num_ref_swf ||
                            ' para el caso de los DVC, es valido para ser procesado.';
          END IF;

        EXCEPTION
          WHEN NO_DATA_FOUND THEN

            err_code  := SQLCODE;
            err_msg   := SUBSTR(SQLERRM, 1, 300);
            p_error   := pkg_pab_constantes.v_error;
            p_mensaje := err_msg;
            pkg_pab_tracking.sp_pab_ins_log_bd(err_code,
                                               v_NOM_PCK,
                                               err_msg,
                                               v_nom_sp);
          WHEN OTHERS THEN

            err_code    := SQLCODE;
            err_msg     := SUBSTR(SQLERRM, 1, 300) ||
                           'Error en la busquedad del num_ref_swf : ' ||
                           v_num_ref_swf ||
                           'en el caso de las operaciones DVP.';
            p_s_mensaje := err_code || '-' || err_msg;
            p_error     := pkg_pab_constantes.v_error;
            p_mensaje   := err_msg;
            pkg_pab_tracking.sp_pab_ins_log_bd(err_code,
                                               v_NOM_PCK,
                                               err_msg,
                                               v_nom_sp);
            RAISE_APPLICATION_ERROR(-20000, err_code || p_s_mensaje);
        END;

      WHEN p_casos_swf = v_CASO_900_910 THEN
        -- caso 4

        BEGIN
            SELECT COUNT(1) AS CONTADOR
            INTO v_cont_existe
            FROM DBO_PAB.PABS_DT_DETLL_CARGO_ABONO_CAJA -- VALIDAMOS QUE REGISTRO NO EXISTA COMO COPIA
            WHERE NUM_REF_EXT = v_num_ref_swf -- EL CAMPO DEBE SER REFERENCIA EXT. DE MT900 - 910
            AND FEC_VTA = p_fec_vta -- Fecha enviada como entrada, originalmente usa la fecha del día
            AND COD_MT_SWF = NVL(TRIM(v_COD_DVI), COD_MT_SWF);

            /*SELECT COUNT(1) AS CONTADOR
            INTO v_cont_existe
            FROM DBO_PAB.PABS_DT_DETLL_CARGO_ABONO_CAJA -- VALIDAMOS QUE REGISTRO NO EXISTA COMO COPIA
            WHERE NUM_REF_EXT = v_num_ref_swf -- EL CAMPO DEBE SER REFERENCIA EXT. DE MT900 - 910
            AND FEC_VTA = v_fecha_vta;*/

          IF (v_cont_existe > v_cantidad) THEN
            p_validacion := v_dato_existe_prt;
            p_error      := pkg_pab_constantes.v_ok;
            p_mensaje    := 'El numero de referencia swift : ' ||
                            v_num_ref_swf ||
                            ' ya se encuentra registrado por otro sistema.';
          ELSE

            BEGIN

              SELECT COUNT(1) AS CONTADOR
                INTO v_cont_existe
                FROM DBO_PAB.PABS_DT_DETLL_OPRCN -- VALIDAMOS QUE REGISTRO NO EXISTA COMO OPERACION ENVIADA
               WHERE NUM_REF_SWF = v_num_ref_swf -- EL CAMPO DEBE SER REFERENCIA EXT. DE MT900 - 910
                 AND FEC_VTA = v_fecha_vta;

              IF (v_cont_existe > v_cantidad) THEN
                p_validacion := v_dato_existe_ope;
                p_error      := pkg_pab_constantes.v_ok;
                p_mensaje    := 'El numero de referencia swift : ' ||
                                v_num_ref_swf ||
                                ' ya se encuentra registrado como envio de nuestro sistema';

              ELSE

                p_validacion := v_dato_no_existe;
                p_error      := pkg_pab_constantes.v_ok;
                p_mensaje    := 'El numero de referencia swift : ' ||
                                v_num_ref_swf || ' puede ser procesado.';
              END IF;

            EXCEPTION
              WHEN NO_DATA_FOUND THEN

                err_code  := SQLCODE;
                err_msg   := SUBSTR(SQLERRM, 1, 300);
                p_error   := pkg_pab_constantes.v_error;
                p_mensaje := err_msg;
                pkg_pab_tracking.sp_pab_ins_log_bd(err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_nom_sp);
            END;

          END IF;

        EXCEPTION
          WHEN NO_DATA_FOUND THEN

            err_code  := SQLCODE;
            err_msg   := SUBSTR(SQLERRM, 1, 300);
            p_error   := pkg_pab_constantes.v_error;
            p_mensaje := err_msg;
            pkg_pab_tracking.sp_pab_ins_log_bd(err_code,
                                               v_NOM_PCK,
                                               err_msg,
                                               v_nom_sp);
          WHEN OTHERS THEN

            err_code    := SQLCODE;
            err_msg     := SUBSTR(SQLERRM, 1, 300) ||
                           'Error en la busquedad del num_ref_swf : ' ||
                           v_num_ref_swf ||
                           'en el caso de las operaciones DVP.';
            p_s_mensaje := err_code || '-' || err_msg;
            p_error     := pkg_pab_constantes.v_error;
            p_mensaje   := err_msg;
            pkg_pab_tracking.sp_pab_ins_log_bd(err_code,
                                               v_NOM_PCK,
                                               err_msg,
                                               v_nom_sp);
            RAISE_APPLICATION_ERROR(-20000, err_code || p_s_mensaje);
        END;

      ELSE
        p_error := pkg_pab_constantes.v_error;
        RAISE err_caso_invalido;
    END CASE;

  EXCEPTION
    WHEN err_caso_invalido THEN
      p_error   := pkg_pab_constantes.v_error;
      err_msg   := 'El caso que ingreso "' || p_casos_swf ||
                   '" es invalido.';
      p_mensaje := err_msg;
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(NULL,
                                         v_NOM_PCK,
                                         err_msg,
                                         v_NOM_SP);
    WHEN OTHERS THEN
      err_code    := SQLCODE;
      err_msg     := SUBSTR(SQLERRM, 1, 300);
      p_error     := pkg_pab_constantes.v_error;
      p_s_mensaje := err_code || '-' || err_msg;
      p_mensaje   := err_msg;
      pkg_pab_tracking.sp_pab_ins_log_bd(err_code,
                                         v_nom_pck,
                                         err_msg,
                                         v_nom_sp);
      RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);
  END sp_vrfca_num_swf;

  /********************************************************************************************************
  FUNCION/PROCEDIMIENTO: SP_PAB_COMBO_AREA_PAG_REC
  OBJETIVO             : MUESTRA FILTRO EN: INGRESOS -> RECIBO OPERACIONES -> CAMBIAR CANAL SALIDA
                           COMBO: CANAL DE SALIDA, CAMBIO CANAL.
  SISTEMA              : DBO_PAB.
  BASE DE DATOS        : DBO_PAB.
  TABLAS USADAS        : PABS_DT_SISTM_ENTRD_SALID.
  FECHA                : 03/07/2018.
  AUTOR                : SANTANDER
  INPUT                : N/A
  OUTPUT               : P_CURSOR := DATOS CURSOR.
                         P_ERROR  := DETERMINA SI HUBO UN ERROR EN LA EJECUCION.
                                     1 := CON ERROR
                                     0 := SIN ERROR
  OBSERVACIONES
  FECHA       USUARIO    DETALLE
  03/07/2018  SANTANDER  CREACIÓN.
  ********************************************************************************************************/
  PROCEDURE SP_PAB_COMBO_AREA_PAG_REC(p_CURSOR OUT SYS_REFCURSOR,
                                      p_ERROR  OUT NUMBER) IS

    ---------------------------------------DECLARACIÓN DE VARIABLES Y CONSTANTES----------------------------
    V_NOM_SP VARCHAR2(30) := 'SP_PAB_COMBO_AREA_PAG_REC';
    --------------------------------------------------------------------------------------------------------
  BEGIN

    P_ERROR := PKG_PAB_CONSTANTES.V_OK;

    OPEN P_CURSOR FOR
      SELECT COD_SIS_ENT_SAL AS CODIGO, DSC_SIS_ENT_SAL AS AREA
        FROM PABS_DT_SISTM_ENTRD_SALID
       WHERE FLG_SIS_ENT_SAL = PKG_PAB_CONSTANTES.V_FLG_SIS_ENT
         AND COD_SIS_ENT_SAL IN
             (PKG_PAB_CONSTANTES.V_COD_TIP_ABOMAS, --ABONOS MASIVOS
              PKG_PAB_CONSTANTES.V_COD_TIP_CANJE, --CANJE
              PKG_PAB_CONSTANTES.V_COD_TIP_CUSTODIA, --CUSTODIA
              PKG_PAB_CONSTANTES.V_COD_TIP_FONMUT, --FONDOS MUTUOS
              PKG_PAB_CONSTANTES.V_COD_TIP_HIPOTE, --HIPOTECARIOS
              PKG_PAB_CONSTANTES.V_COD_TIP_CONTA, --MOVIMIENTOS BANCO CENTRAL
              PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SE_OOFF, --OOFF
              PKG_PAB_CONSTANTES.V_COD_TIP_CREDIT, --PROCESOS ACTIVOS
              PKG_PAB_CONSTANTES.V_COD_TIP_TESCEN) --TESORERIA CENTRAL
         AND FLG_VGN = PKG_PAB_CONSTANTES.V_FLG_VGN_SI
       ORDER BY DSC_SIS_ENT_SAL;

  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      ERR_CODE := SQLCODE;
      ERR_MSG  := SUBSTR(SQLERRM, 1, 300);
      PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD(ERR_CODE,
                                         V_NOM_PCK,
                                         ERR_MSG,
                                         V_NOM_SP);
    WHEN OTHERS THEN
      ERR_CODE := SQLCODE;
      ERR_MSG  := SUBSTR(SQLERRM, 1, 300);
      PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD(ERR_CODE,
                                         V_NOM_PCK,
                                         ERR_MSG,
                                         V_NOM_SP);

      P_S_MENSAJE := ERR_CODE || '-' || ERR_MSG;
      RAISE_APPLICATION_ERROR(-20000, P_S_MENSAJE);
  END SP_PAB_COMBO_AREA_PAG_REC;

  /************************************************************************************************
  -- Funcion/Procedimiento: Sp_PAB_ACT_GPI_OPE
  -- Objetivo: Procedimiento que actualiza con información del GPI
  -- Sistema: DBO_PAB.
  -- Base de Datos: DBO_PAB.
  -- Tablas Usadas: PABS_DT_DETLL_OPRCN
  -- Fecha: 02/06/16
  -- Autor: Santander
  -- Input:    p_NUM_FOL_NMN     ->   foliador del banco
  --           p_FEC_ISR_OPE     ->   Fecha de insercion de la operacion
  --           p_NUM_SRV_TPO_UID ->   Numero referencia Swift
  --           p_NUM_IDF_UNC_UID ->   Identificador de nuevo estado.
  -- Output:
  --           p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
  -- Input/Output: N/A
  -- Retorno: N/A.
  -- Observaciones: <Fecha y Detalle de ultimos cambios>
  --***********************************************************************************************/
  PROCEDURE Sp_PAB_ACT_GPI_OPE(p_NUM_FOL_OPE     IN NUMBER,
                               p_FEC_ISR_OPE     IN DATE,
                               p_NUM_SRV_TPO_UID IN CHAR,
                               p_NUM_IDF_UNC_UID IN VARCHAR2,
                               p_ERROR           OUT NUMBER) IS

    v_NOM_SP VARCHAR2(31) := 'Sp_PAB_ACT_GPI_OPE';

  BEGIN

    --Actualizamos la información del GPI
    UPDATE PABS_DT_DETLL_OPRCN op
       SET NUM_SRV_TPO_UID = p_NUM_SRV_TPO_UID,
           NUM_IDF_UNC_UID = p_NUM_IDF_UNC_UID
     WHERE NUM_FOL_OPE = p_NUM_FOL_OPE
       AND FEC_ISR_OPE = p_FEC_ISR_OPE;

  EXCEPTION
    WHEN DUP_VAL_ON_INDEX THEN
      err_code := SQLCODE;
      err_msg  := SUBSTR(SQLERRM, 1, 300);
      P_ERROR  := PKG_PAB_CONSTANTES.V_ERROR;
      PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD(err_code,
                                         V_NOM_PCK,
                                         err_msg,
                                         V_NOM_SP);
    WHEN OTHERS THEN
      err_code := SQLCODE;
      err_msg  := SUBSTR(SQLERRM, 1, 300);
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                         v_NOM_PCK,
                                         err_msg,
                                         v_NOM_SP);
      p_ERROR     := PKG_PAB_CONSTANTES.v_ERROR;
      p_s_mensaje := err_code || '-' || err_msg;
      RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);
  END Sp_PAB_ACT_GPI_OPE;


 --***********************************************************************************************
  -- Funcion/Procedimiento: FN_PAB_BUS_VAL_BIC
  -- Objetivo: Funcion que valida mensajeria SWF.
  -- Sistema: PAB
  -- Base de Datos: DGBMSEGDB01
  -- Tablas Usadas: PABS_DT_DETLL_NOMNA
  -- Fecha: 09/11/2016
  -- Autor: Santander
  -- Input:
  -- P_NUM_FOL_OPE -> numero de nomina
  -- P_COD_EST_AOS -> codigo estado nomina
  -- P_FEC_ISR_OPE -> fecha insercion operacion
  -- N/A
  -- Output:
  -- N/A.
  -- Input/Output:
  -- N/A.
  -- Retorno:
  -- P_FLAG_EST -> valor de retorno flag de comparacion
  -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
  --***********************************************************************************************
  FUNCTION FN_PAB_BUS_VAL_BIC(P_COD_BCO_ORG IN CHAR
                             ) RETURN NUMBER IS
    --
    V_NOM_SP           VARCHAR2(22) := 'FN_PAB_BUS_VAL_BIC';
    P_FLG_MRD_UTZ_SWF  NUMBER :=0;
    P_FLAG_EST_OPE     NUMBER;
    v_TGCDSWSA         CHAR(11);

  BEGIN
    --
    --
    BEGIN

        SELECT DISTINCT(TGCDSWSA)
        INTO v_TGCDSWSA FROM TCDT040
        WHERE TGCDSWSA = P_COD_BCO_ORG;

    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        P_FLG_MRD_UTZ_SWF := 1;
    END;

    --
    IF P_FLG_MRD_UTZ_SWF = PKG_PAB_CONSTANTES.V_FLG_MRD_MN THEN
      P_FLAG_EST_OPE := 0;
    ELSE
      P_FLAG_EST_OPE := 1;
    END IF;

    --
    RETURN P_FLAG_EST_OPE;
    --
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      err_code := SQLCODE;
      err_msg  := SUBSTR(SQLERRM, 1, 300);
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                         v_NOM_PCK,
                                         err_msg,
                                         v_NOM_SP);
    WHEN OTHERS THEN
      err_code := SQLCODE;
      err_msg  := SUBSTR(SQLERRM, 1, 300);
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                         v_NOM_PCK,
                                         err_msg,
                                         v_NOM_SP);
      p_s_mensaje := err_code || '-' || err_msg;
      RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);
      --
  END FN_PAB_BUS_VAL_BIC;

-- Funcion/Procedimiento: Sp_PAB_BUS_DET_OPE_EGR_HOY
  -- Objetivo: Procedimiento almacenado que retorna el detalle de una operacion segun el numero de referencia swift del mismo dia de la ejecucion
  -- Sistema: DBO_PAB.
  -- Base de Datos: DBO_PAB.
  -- Tablas Usadas: PABS_DT_DETLL_OPRCN, PABS_DT_ESTDO_ALMOT
  -- Fecha: 19/11/19
  -- Autor: Santander
  -- Input:   p_NUM_REF_SWF: Numero de referencia de una operacion swift del mismo dia de la ejecucion (cuando se ejecuta el PL)
  -- Output:
  --        p_CURSOR : SYS_REFCURSOR, resultados de consulta.
  --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
  -- Input/Output: N/A
  -- Retorno: N/A.
  -- Observaciones: <Fecha y Detalle de ultimos cambios>
  --***********************************************************************************************/
  PROCEDURE Sp_PAB_BUS_DET_OPE_EGR_HOY (p_NUM_REF_SWF IN CHAR,
                                        p_CURSOR      OUT SYS_REFCURSOR,
                                        p_ERROR       OUT NUMBER) IS
    v_NOM_SP VARCHAR2(30) := 'Sp_PAB_BUS_DET_OPE_EGR_HOY';
    v_FECHA_HOY DATE := TRUNC(SYSDATE);
    v_DSC_EST_AOS VARCHAR2(30);
    v_FEC_ISR_OPE DATE;
    v_NUM_FOL_OPE NUMBER(12);
    v_NUM_OPE_SIS_ENT CHAR(20);
    v_COD_SIS_ENT CHAR(10);
    v_COD_MT_SWF NUMBER(3);
    v_COD_TPO_OPE_AOS CHAR(8);
    v_COD_DVI CHAR(3);
    v_IMP_OPE NUMBER(15,2);
    v_COD_BCO_DTN CHAR(11);
    v_NUM_DOC_BFC CHAR(11);
    v_COD_TPD_BFC CHAR(1);
    v_NOM_BFC VARCHAR2(100);
    v_NUM_REF_SWF CHAR(16);
    v_NOM_ODN VARCHAR2(100);
    v_NUM_DOC_ODN CHAR(11);
    v_COD_TPD_ODN CHAR(1);
    v_NUM_CTA_ODN VARCHAR2(30);
    v_FEC_VTA DATE;
  BEGIN
    -- Se lanza excepcion de que no encontro datos
    IF (p_NUM_REF_SWF IS NOT NULL) THEN
        OPEN p_CURSOR FOR
            SELECT
                 ESTALM.DSC_EST_AOS AS ESTADO,
                 DETOPE.FEC_ISR_OPE AS FEC_INS,
                 DETOPE.NUM_FOL_OPE AS NUM_OPE,
                 DETOPE.NUM_OPE_SIS_ENT AS NUM_OPE_ENT,
                 DETOPE.COD_SIS_ENT AS COD_CANAL_ENT,
                 DETOPE.COD_MT_SWF AS COD_MT,
                 DETOPE.COD_TPO_OPE_AOS AS TIPO_OPERACION,
                 DETOPE.COD_DVI AS DIVISA,
                 DETOPE.IMp_OPE AS MONTO_TOTAL,
                 DETOPE.COD_BCO_DTN AS BIC_DESTINO,
                 DETOPE.NUM_DOC_BFC AS DOC_BENEFICIARIO,
                 DETOPE.COD_TPD_BFC AS TIPO_DOC_BENEFICIARIO,
                 DETOPE.NOM_BFC AS BENEFICIARIO,
                 DETOPE.NUM_REF_SWF AS REF_SWIFT,
                 DETOPC.NOM_ODN AS NOMBRE_ORDENANTE,
                 DETOPC.NUM_DOC_ODN AS DOC_ORDENANTE,
                 DETOPC.COD_TPD_ODN AS TIPO_DOC_ORDENANTE,
                 DETOPC.NUM_CTA_ODN AS CTA_ORDENANTE,
                 DETOPE.FEC_VTA AS FECHA_VALUTA
            FROM
                 PABS_DT_DETLL_OPRCN DETOPE
            INNER JOIN
                 PABS_DT_ESTDO_ALMOT ESTALM ON DETOPE.COD_EST_AOS = ESTALM.COD_EST_AOS
            LEFT JOIN
                 PABS_DT_OPRCN_INFCN_OPCON DETOPC ON DETOPE.NUM_FOL_OPE = DETOPC.NUM_FOL_OPE AND DETOPE.FEC_ISR_OPE = DETOPC.FEC_ISR_OPE
            WHERE
                 DETOPE.NUM_REF_SWF = p_NUM_REF_SWF AND
                 DETOPE.FLG_EGR_ING = PKG_PAB_CONSTANTES.V_FLG_EGR AND
                 DETOPE.COD_EST_AOS NOT IN (PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEELI, PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEABN, PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAB, PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAS) AND
                 DETOPE.FEC_VTA = v_FECHA_HOY AND
                 ROWNUM <= 1;

        p_ERROR := PKG_PAB_CONSTANTES.v_OK; --Procedimiento ejecutado de forma correcta
    ELSE
        OPEN p_CURSOR FOR
            SELECT
                v_DSC_EST_AOS AS ESTADO,
                v_FEC_ISR_OPE AS FEC_INS,
                v_NUM_FOL_OPE AS NUM_OPE,
                v_NUM_OPE_SIS_ENT AS NUM_OPE_ENT,
                v_COD_SIS_ENT AS COD_CANAL_ENT,
                v_COD_MT_SWF AS COD_MT,
                v_COD_TPO_OPE_AOS AS TIPO_OPERACION,
                v_COD_DVI AS DIVISA,
                v_IMP_OPE AS MONTO_TOTAL,
                v_COD_BCO_DTN AS BIC_DESTINO,
                v_NUM_DOC_BFC AS DOC_BENEFICIARIO,
                v_COD_TPD_BFC AS TIPO_DOC_BENEFICIARIO,
                v_NOM_BFC AS BENEFICIARIO,
                v_NUM_REF_SWF AS REF_SWIFT,
                v_NOM_ODN AS NOMBRE_ORDENANTE,
                v_NUM_DOC_ODN AS DOC_ORDENANTE,
                v_COD_TPD_ODN AS TIPO_DOC_ORDENANTE,
                v_NUM_CTA_ODN AS CTA_ORDENANTE,
                v_FEC_VTA AS FECHA_VALUTA
            FROM
                DUAL;

        err_code := '2100';
        err_msg  := SUBSTR('Se le paso valor NULL en parametro de entrada p_NUM_REF_SWF', 1, 300);
        PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                         v_NOM_PCK,
                                         err_msg,
                                         v_NOM_SP);
        p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
    END IF;

  EXCEPTION

    WHEN NO_DATA_FOUND THEN

      err_code := SQLCODE;
      err_msg  := SUBSTR(SQLERRM, 1, 300);
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                         v_NOM_PCK,
                                         err_msg,
                                         v_NOM_SP);
      p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;

    WHEN OTHERS THEN

      err_code    := SQLCODE;
      err_msg     := SUBSTR(SQLERRM, 1, 300);
      p_s_mensaje := err_code || '-' || err_msg;
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                         v_NOM_PCK,
                                         err_msg,
                                         v_NOM_SP);
      p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);

  END Sp_PAB_BUS_DET_OPE_EGR_HOY;

/************************************************************************************************
  -- Funcion/Procedimiento: SP_PAB_VAL_REF_SWF
  -- Objetivo: Fusion que retorne el codifo de referencia Swift
  -- Sistema: DBO_PAB.
  -- Base de Datos: DBO_PAB.
  -- Tablas Usadas: N/A
  -- Fecha: 02/06/16
  -- Autor: Santander
  -- Input:      p_NUM_FOL_OPE --> numero de operacion a validar,
  --             p_FEC_ISR_OPE --> fecha de inreso de operacion a validar,,
  --
  -- Output:
  --           p_ERROR  --> retorna codigo de validacion.

  -- Input/Output: N/A
  -- Retorno: N/A.
  -- Observaciones: <Fecha y Detalle de ultimos cambios>
  --***********************************************************************************************/
  PROCEDURE SP_PAB_VAL_REF_SWF(p_NUM_FOL_OPE     IN NUMBER,
                               p_FEC_ISR_OPE     IN DATE,
                               p_ERROR     OUT NUMBER)
                               IS

    v_REF_SWF         VARCHAR2(16);
    v_NOM_SP          VARCHAR2(30) := 'SP_PAB_VAL_REF_SWF';
    v_COD_TIPO_OPE_AS VARCHAR2(7);


 BEGIN


  BEGIN
    SELECT NUM_REF_SWF
    INTO v_REF_SWF
    FROM PABS_DT_DETLL_OPRCN
    WHERE NUM_FOL_OPE = p_NUM_FOL_OPE;
    --AND TRUNC(FEC_ISR_OPE) = TRUNC(p_FEC_ISR_OPE) ;

    EXCEPTION
    WHEN NO_DATA_FOUND THEN
      err_code    := SQLCODE;
      err_msg     := SUBSTR(SQLERRM, 1, 300);
      p_ERROR     := PKG_PAB_CONSTANTES.V_ERROR;
          p_s_mensaje := err_code || '-' || err_msg;
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                         v_NOM_PCK,
                                         err_msg,
                                         v_NOM_SP);
      RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);

    END;



   IF  TRIM(v_REF_SWF) = '' OR  NVL(v_REF_SWF,'#')  = '#' THEN
       p_ERROR     := PKG_PAB_CONSTANTES.V_OK;
        err_code    := 22000;
        err_msg     := 'Operación  : ' || p_NUM_FOL_OPE || ' es valida para ser liberada.';
        PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                         v_NOM_PCK,
                                         err_msg,
                                         v_NOM_SP);
   ELSE
     p_ERROR     := PKG_PAB_CONSTANTES.V_ERROR;
        err_code    := 22000;
        err_msg     := 'Operación '|| p_NUM_FOL_OPE ||' ya presenta referencia de envío. refernencia Actual : ' || v_REF_SWF || ' . No se procesará nuevamente.';
        PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                         v_NOM_PCK,
                                         err_msg,
                                       v_NOM_SP);
   END IF;



  END SP_PAB_VAL_REF_SWF;

  /************************************************************************************************
  -- Funcion/Procedimiento: Sp_PAB_BUS_DET_OPE_ORI
  -- Objetivo: Procedimiento almacenado que retorna el detalle de una operacion que puede estar  en la tabla detalle operacion o historico.
  -- Sistema: DBO_PAB.
  -- Base de Datos: DBO_PAB.
  -- Tablas Usadas: PABS_DT_DETLL_OPRCN, PABS_DT_OPRCN_INFCN_OPCON, PABS_DT_DETLL_OPRCN_HTRCA, PABS_DT_OPRCN_OPCON_HTRCA
  --          PABS_DT_ESTDO_ALMOT, PABS_DT_SISTM_ENTRD_SALID
  -- Fecha: 05/02/20
  -- Autor: Santander
  -- Input:   p_NUM_FOL_OPE : Numero de folio de la operación
  --          p_NUM_REF_SWF: Numero de referencia de una operacion swift
  --          p_FLG_EGR_ING: Flag de egreso o ingreso
  -- Output:
  --        p_CURSOR : SYS_REFCURSOR, resultados de consulta.
  --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
  -- Input/Output: N/A
  -- Retorno: N/A.
  -- Observaciones: 05-02-2020: (Julio Villalobos) Creacion de procedimiento almacenado
  --***********************************************************************************************/
  PROCEDURE Sp_PAB_BUS_DET_OPE_ORI(p_NUM_FOL_OPE IN NUMBER,
                                   p_NUM_REF_SWF IN CHAR,
                                   p_FLG_EGR_ING IN NUMBER,
                                   p_CURSOR      OUT SYS_REFCURSOR,
                                   p_ERROR       OUT NUMBER) IS
    v_NOM_SP VARCHAR2(30) := 'Sp_PAB_BUS_DET_OPE_ORI';
    v_cuaCer CONSTANT CHAR(4) := '0000';
    v_NUM_REF_SWF char(16);
    v_NUM_FOL_OPE NUMBER;

  BEGIN

    v_NUM_REF_SWF := p_NUM_REF_SWF;

    --Integracion no envia nulos por lo que el 0 representa un nulo
    IF (p_NUM_FOL_OPE > 0) THEN
      v_NUM_FOL_OPE := p_NUM_FOL_OPE;
    END IF;

    OPEN p_CURSOR FOR
     SELECT ESTALM.DSC_EST_AOS AS ESTADO,
             DETOPE.FEC_ISR_OPE AS FEC_INS,
             DETOPE.NUM_FOL_OPE AS NUM_OPE,
             DETOPE.NUM_FOL_NMN AS NUM_NOM,
             DETOPE.NUM_FOL_OPE_ORG AS NUM_OPE_ORG,
             DETOPE.NUM_OPE_SIS_ENT AS NUM_OPE_ENT,
             DECODE(DETOPE.FLG_EGR_ING,
                    PKG_PAB_CONSTANTES.v_FLG_EGR,
                    PKG_PAB_CONSTANTES.v_STR_FLG_EGR,
                    PKG_PAB_CONSTANTES.v_STR_FLG_ING) AS EGRESO_INGRESO,
             DETOPE.COD_SIS_ENT AS COD_CANAL_ENT,
             ENT.DSC_SIS_ENT_SAL AS CANAL_ENTRADA,
             DETOPE.COD_SIS_SAL AS COD_CANAL_SAL,
             SAL.DSC_SIS_ENT_SAL AS CANAL_SALIDA,
             DECODE(DETOPE.COD_TPO_ING,
                    PKG_PAB_CONSTANTES.v_COD_TIp_ING_MAS,
                    PKG_PAB_CONSTANTES.v_STR_TIp_ING_MAS,
                    PKG_PAB_CONSTANTES.V_COD_TIP_ING_UNI,
                    PKG_PAB_CONSTANTES.V_STR_TIP_ING_UNI,
                    PKG_PAB_CONSTANTES.V_COD_TIP_ING_DIR,
                    PKG_PAB_CONSTANTES.V_STR_TIP_ING_DIR) AS TIPO_INGRESO,
             DETOPE.FEC_ENv_RCp_SWF AS FECHA_SWIFT,
             DETOPE.HOR_ENv_RCp_SWF AS HORA_SWIFT,
             DETOPE.COD_MT_SWF AS COD_MT,
             DECODE(DETOPE.FLG_MRD_UTZ_SWF,
                    PKG_PAB_CONSTANTES.v_FLG_MRD_MN,
                    PKG_PAB_CONSTANTES.v_STR_FLG_MRD_MN,
                    PKG_PAB_CONSTANTES.v_STR_FLG_MRD_MX) AS MERCADO,
             DETOPE.COD_TPO_OPE_AOS AS TIPO_OPERACION,
             DETOPE.FEC_VTA AS FECHA_PAGO,
             DETOPE.COD_DVI AS DIVISA,
             DETOPE.IMp_OPE AS MONTO_TOTAL,
             DETOPE.COD_BCO_DTN AS BIC_DESTINO,
             DETOPE.COD_BCO_ORG AS BIC_ORIGEN,
             DETOPE.NUM_DOC_BFC AS DOC_BENEFICIARIO,
             DETOPE.COD_TPD_BFC AS TIPO_DOC_BENEFICIARIO,
             DETOPE.NOM_BFC AS BENEFICIARIO,
             DETOPE.NUM_CTA_BFC AS CTA_BENEFICIARIO,
             DETOPE.COD_SUC AS COD_SUCURSAL,
             DETOPE.NUM_REF_SWF AS REF_SWIFT,
             DETOPE.NUM_REF_EXT AS REF_EXT_SWIFT,
             DECODE(DETOPE.COD_EST_AOS,
                    PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEREC,
                    DETOPC.GLS_EST_RCH,
                    PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPVI,
                    DETOPC.GLS_MTv_VSD,
                    DETOPE.GLS_ADC_EST) AS GLOSA_ESTADO,
             PKG_PAB_CONSTANTES.v_COD_ENT_BAN_SANT || v_cuaCer ||
             DETOPE.NUM_REF_CTB AS REF_CONTABLE,
             DETOPC.COD_BCO_BFC AS BIC_BENEFICIARIO,
             DETOPC.COD_BCO_ITD AS BIC_INTERMEDIARIO,
             DETOPC.NUM_DOC_BFC AS DOC_BENEFICIARIO_OPC,
             DETOPC.COD_TPD_BFC AS TIPO_DOC_BENEFICIARIO_OPC,
             DETOPC.NUM_CTA_DCv_BFC AS CTA_DCv_BENEFICIARIO,
             DETOPC.GLS_DIR_BFC AS DIRECCION_BENEFICIARIO,
             DETOPC.NOM_CDD_BFC AS CIUDAD_BENEFICIARIO,
             DETOPC.COD_PAS_BFC AS PAIS_BENEFICIARIO,
             DETOPC.NOM_ODN AS NOMBRE_ORDENANTE,
             DETOPC.NUM_DOC_ODN AS DOC_ORDENANTE,
             DETOPC.COD_TPD_ODN AS TIPO_DOC_ORDENANTE,
             DETOPC.NUM_CTA_ODN AS CTA_ORDENANTE,
             DETOPC.NUM_CTA_DCv_ODN AS CTA_DCv_ORDENANTE,
             DETOPC.GLS_DIR_ODN AS DIRECCION_ORDENANTE,
             DETOPC.NOM_CDD_ODN AS CIUDAD_ORDENANTE,
             DETOPC.COD_PAS_ODN AS PAIS_ORDENANTE,
             DETOPC.NUM_CLv_NGC AS CLAVE_NEGOCIO,
             DETOPC.NUM_AGT AS NUM_AGENTE,
             DETOPC.COD_FND_CCLV AS FONDO_CCLV,
             DETOPC.COD_TPO_SDO AS TIPO_SALDO,
             DETOPC.COD_TPO_CMA AS TIPO_CAMARA,
             DETOPC.COD_TPO_FND AS CODIGO_FONDO,
             DETOPC.FEC_TPO_OPE_CCLV AS FECHA_CCLV,
             DETOPC.NUM_CLv_IDF AS NUMERO_IDENTIFICATORIO,
             DETOPC.GLS_EST_RCH AS GLOSA_RECHAZO,
             DETOPC.GLS_MTv_VSD AS GLOSA_VISADA,
             DETOPC.OBS_OPC_SWF AS OBSERVACION_SWIFT,
             TO_CHAR(SYSDATE, 'HH24MI') AS INDHOR,
             DETOPE.NOM_BFC AS NOMBRE_ORI,
             DETOPE.COD_BCO_ORG AS CUENTA_ORI,
             DETOPE.NUM_REF_SWF AS REF_SWF_ORI,
             DETOPE.NUM_DOC_BFC AS RUT_ORI,
             DETOPE.IMP_OPE AS MONTO_ORI,
             DETOPE.COD_DVI AS MONEDA_ORI,
             DETOPE.COD_MT_SWF AS MT_SWIFT_ORI
        FROM PABS_DT_DETLL_OPRCN       DETOPE,
             PABS_DT_OPRCN_INFCN_OPCON DETOPC,
             PABS_DT_ESTDO_ALMOT       ESTALM,
             PABS_DT_SISTM_ENTRD_SALID ENT,
             PABS_DT_SISTM_ENTRD_SALID SAL
       WHERE DETOPE.NUM_FOL_OPE = NVL(v_NUM_FOL_OPE, DETOPE.NUM_FOL_OPE)
         AND DETOPE.NUM_REF_SWF = NVL(v_NUM_REF_SWF, DETOPE.NUM_REF_SWF)
         AND DETOPE.NUM_FOL_OPE = DETOPC.NUM_FOL_OPE(+)
         AND DETOPE.FEC_ISR_OPE = DETOPC.FEC_ISR_OPE(+)
         AND DETOPE.COD_EST_AOS = ESTALM.COD_EST_AOS(+)
         AND DETOPE.COD_SIS_ENT = ENT.COD_SIS_ENT_SAL(+)
         AND DETOPE.COD_SIS_SAL = SAL.COD_SIS_ENT_SAL(+)
         AND DETOPE.FLG_EGR_ING = p_FLG_EGR_ING

       UNION ALL

     SELECT ESTALM.DSC_EST_AOS AS ESTADO,
             DETOPE.FEC_ISR_OPE AS FEC_INS,
             DETOPE.NUM_FOL_OPE AS NUM_OPE,
             DETOPE.NUM_FOL_NMN AS NUM_NOM,
             DETOPE.NUM_FOL_OPE_ORG AS NUM_OPE_ORG,
             DETOPE.NUM_OPE_SIS_ENT AS NUM_OPE_ENT,
             DECODE(DETOPE.FLG_EGR_ING,
                    PKG_PAB_CONSTANTES.v_FLG_EGR,
                    PKG_PAB_CONSTANTES.v_STR_FLG_EGR,
                    PKG_PAB_CONSTANTES.v_STR_FLG_ING) AS EGRESO_INGRESO,
             DETOPE.COD_SIS_ENT AS COD_CANAL_ENT,
             ENT.DSC_SIS_ENT_SAL AS CANAL_ENTRADA,
             DETOPE.COD_SIS_SAL AS COD_CANAL_SAL,
             SAL.DSC_SIS_ENT_SAL AS CANAL_SALIDA,
             DECODE(DETOPE.COD_TPO_ING,
                    PKG_PAB_CONSTANTES.v_COD_TIp_ING_MAS,
                    PKG_PAB_CONSTANTES.v_STR_TIp_ING_MAS,
                    PKG_PAB_CONSTANTES.V_COD_TIP_ING_UNI,
                    PKG_PAB_CONSTANTES.V_STR_TIP_ING_UNI,
                    PKG_PAB_CONSTANTES.V_COD_TIP_ING_DIR,
                    PKG_PAB_CONSTANTES.V_STR_TIP_ING_DIR) AS TIPO_INGRESO,
             DETOPE.FEC_ENv_RCp_SWF AS FECHA_SWIFT,
             DETOPE.HOR_ENv_RCp_SWF AS HORA_SWIFT,
             DETOPE.COD_MT_SWF AS COD_MT,
             DECODE(DETOPE.FLG_MRD_UTZ_SWF,
                    PKG_PAB_CONSTANTES.v_FLG_MRD_MN,
                    PKG_PAB_CONSTANTES.v_STR_FLG_MRD_MN,
                    PKG_PAB_CONSTANTES.v_STR_FLG_MRD_MX) AS MERCADO,
             DETOPE.COD_TPO_OPE_AOS AS TIPO_OPERACION,
             DETOPE.FEC_VTA AS FECHA_PAGO,
             DETOPE.COD_DVI AS DIVISA,
             DETOPE.IMp_OPE AS MONTO_TOTAL,
             DETOPE.COD_BCO_DTN AS BIC_DESTINO,
             DETOPE.COD_BCO_ORG AS BIC_ORIGEN,
             DETOPE.NUM_DOC_BFC AS DOC_BENEFICIARIO,
             DETOPE.COD_TPD_BFC AS TIPO_DOC_BENEFICIARIO,
             DETOPE.NOM_BFC AS BENEFICIARIO,
             DETOPE.NUM_CTA_BFC AS CTA_BENEFICIARIO,
             DETOPE.COD_SUC AS COD_SUCURSAL,
             DETOPE.NUM_REF_SWF AS REF_SWIFT,
             DETOPE.NUM_REF_EXT AS REF_EXT_SWIFT,
             DECODE(DETOPE.COD_EST_AOS,
                    PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEREC,
                    DETOPC.GLS_EST_RCH,
                    PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPVI,
                    DETOPC.GLS_MTv_VSD,
                    DETOPE.GLS_ADC_EST) AS GLOSA_ESTADO,
             PKG_PAB_CONSTANTES.v_COD_ENT_BAN_SANT || v_cuaCer ||
             DETOPE.NUM_REF_CTB AS REF_CONTABLE,
             DETOPC.COD_BCO_BFC AS BIC_BENEFICIARIO,
             DETOPC.COD_BCO_ITD AS BIC_INTERMEDIARIO,
             DETOPC.NUM_DOC_BFC AS DOC_BENEFICIARIO_OPC,
             DETOPC.COD_TPD_BFC AS TIPO_DOC_BENEFICIARIO_OPC,
             DETOPC.NUM_CTA_DCv_BFC AS CTA_DCv_BENEFICIARIO,
             DETOPC.GLS_DIR_BFC AS DIRECCION_BENEFICIARIO,
             DETOPC.NOM_CDD_BFC AS CIUDAD_BENEFICIARIO,
             DETOPC.COD_PAS_BFC AS PAIS_BENEFICIARIO,
             DETOPC.NOM_ODN AS NOMBRE_ORDENANTE,
             DETOPC.NUM_DOC_ODN AS DOC_ORDENANTE,
             DETOPC.COD_TPD_ODN AS TIPO_DOC_ORDENANTE,
             DETOPC.NUM_CTA_ODN AS CTA_ORDENANTE,
             DETOPC.NUM_CTA_DCv_ODN AS CTA_DCv_ORDENANTE,
             DETOPC.GLS_DIR_ODN AS DIRECCION_ORDENANTE,
             DETOPC.NOM_CDD_ODN AS CIUDAD_ORDENANTE,
             DETOPC.COD_PAS_ODN AS PAIS_ORDENANTE,
             DETOPC.NUM_CLv_NGC AS CLAVE_NEGOCIO,
             DETOPC.NUM_AGT AS NUM_AGENTE,
             DETOPC.COD_FND_CCLV AS FONDO_CCLV,
             DETOPC.COD_TPO_SDO AS TIPO_SALDO,
             DETOPC.COD_TPO_CMA AS TIPO_CAMARA,
             DETOPC.COD_TPO_FND AS CODIGO_FONDO,
             DETOPC.FEC_TPO_OPE_CCLV AS FECHA_CCLV,
             DETOPC.NUM_CLv_IDF AS NUMERO_IDENTIFICATORIO,
             DETOPC.GLS_EST_RCH AS GLOSA_RECHAZO,
             DETOPC.GLS_MTv_VSD AS GLOSA_VISADA,
             DETOPC.OBS_OPC_SWF AS OBSERVACION_SWIFT,
             TO_CHAR(SYSDATE, 'HH24MI') AS INDHOR,
             DETOPE.NOM_BFC AS NOMBRE_ORI,
             DETOPE.COD_BCO_ORG AS CUENTA_ORI,
             DETOPE.NUM_REF_SWF AS REF_SWF_ORI,
             DETOPE.NUM_DOC_BFC AS RUT_ORI,
             DETOPE.IMP_OPE AS MONTO_ORI,
             DETOPE.COD_DVI AS MONEDA_ORI,
             DETOPE.COD_MT_SWF AS MT_SWIFT_ORI
        FROM PABS_DT_DETLL_OPRCN_HTRCA DETOPE,
             PABS_DT_OPRCN_OPCON_HTRCA DETOPC,
             PABS_DT_ESTDO_ALMOT       ESTALM,
             PABS_DT_SISTM_ENTRD_SALID ENT,
             PABS_DT_SISTM_ENTRD_SALID SAL
       WHERE DETOPE.NUM_FOL_OPE = NVL(v_NUM_FOL_OPE, DETOPE.NUM_FOL_OPE)
         AND DETOPE.NUM_REF_SWF = NVL(v_NUM_REF_SWF, DETOPE.NUM_REF_SWF)
         AND DETOPE.NUM_FOL_OPE = DETOPC.NUM_FOL_OPE(+)
         AND DETOPE.FEC_ISR_OPE = DETOPC.FEC_ISR_OPE(+)
         AND DETOPE.COD_EST_AOS = ESTALM.COD_EST_AOS(+)
         AND DETOPE.COD_SIS_ENT = ENT.COD_SIS_ENT_SAL(+)
         AND DETOPE.COD_SIS_SAL = SAL.COD_SIS_ENT_SAL(+)
         AND DETOPE.FLG_EGR_ING = p_FLG_EGR_ING;

    p_ERROR := PKG_PAB_CONSTANTES.v_OK; --Procedimiento ejecutado de forma correcta

  EXCEPTION

    WHEN NO_DATA_FOUND THEN

      err_code := SQLCODE;
      err_msg  := SUBSTR(SQLERRM, 1, 300);
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                         v_NOM_PCK,
                                         err_msg,
                                         v_NOM_SP);
      p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;

    WHEN OTHERS THEN

      err_code    := SQLCODE;
      err_msg     := SUBSTR(SQLERRM, 1, 300);
      p_s_mensaje := err_code || '-' || err_msg;
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD(err_code,
                                         v_NOM_PCK,
                                         err_msg,
                                         v_NOM_SP);
      p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);

  END Sp_PAB_BUS_DET_OPE_ORI;
END PKG_PAB_OPERACION;