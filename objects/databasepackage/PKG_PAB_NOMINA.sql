CREATE OR REPLACE PACKAGE         PKG_PAB_NOMINA
IS
   -- Package el cual contrendra todas las funciones y procedimientos relacionados con las nominas
   -- //@GCP
   -- ******************* INICIO VARIABLE GLOBALES *******************************
   -- //@Santander
   v_NOM_PCK            VARCHAR2 (30) := 'PKG_PAB_NOMINA';   -- Nombre package
   ERR_CODE             NUMBER := 0;                        -- Codigo de error
   ERR_MSG              VARCHAR2 (300);                    -- Mensaje de error
   v_DES                VARCHAR2 (300);  -- Descripcion de insercion de nomina
   v_DES_BIT            VARCHAR2 (100);                  -- Descripcion nomina
   v_BANCO              VARCHAR2 (9) := '970150005'; -- Identificador banco Santander
   p_s_mensaje          VARCHAR2 (400);                       -- Mensaje Raise
   v_cont_est_vis       NUMBER := 0;      -- cantidad de operaciones por visar
   v_est_ope_vis        NUMBER := 0; -- indica si la operación actual esta por visar

   TYPE T_NUM_FOL_NMN
   IS
      TABLE OF NUMBER
         INDEX BY PLS_INTEGER;        -- Arreglo numeros de foliador del banco

   -- ********************** FIN VARIABLE GLOBALES ********************************


   -- **************Declaracion Cursor C_CONSULTA_OPE_NOM**************************
   -- //@Santander
   CURSOR C_CONSULTA_OPE_NOM (
      p_NUM_FOL_OPE   IN            NUMBER,
      p_FEC_ISR_OPE   IN            DATE
   )
   IS
      SELECT   DETNOM.FEC_ISR_OPE,
               DETNOM.NUM_FOL_OPE,
               DETNOM.NOM_BFC,
               DETNOM.NUM_FOL_NMN,
               DETNOM.NUM_OPE_SIS_ENT,
               DETNOM.FLG_EGR_ING,
               CAB.COD_SIS_ENT,
               DETNOM.COD_SIS_SAL,
               DETNOM.COD_EST_AOS,
               DETNOM.COD_TPO_ING,
               DETNOM.FEC_ENv_RCp_SWF,
               DETNOM.HOR_ENv_RCp_SWF,
               DETNOM.COD_MT_SWF,
               DETNOM.FLG_MRD_UTZ_SWF,
               DETNOM.COD_TPO_OPE_AOS,
               DETNOM.FEC_VTA,
               DETNOM.COD_DVI,
               DETNOM.IMp_OPE,
               DETNOM.COD_BCO_DTN,
               DETNOM.NUM_CTA_BFC,
               DETNOM.COD_SUC,
               DETNOM.NUM_REF_SWF,
               DETNOM.FLG_OPE_DUP,
               DETNOM.NUM_REF_EXT,
               DETNOM.GLS_ADC_EST,
               DETNOM.NUM_REF_CTB,
               DETOPC.OBS_OPC_SWF,
               DETOPC.COD_BCO_BFC,
               DETOPC.COD_BCO_ITD,
               DETOPC.NUM_DOC_BFC,
               DETOPC.COD_TPD_BFC,
               DETOPC.NUM_CTA_DCv_BFC,
               DETOPC.GLS_DIR_BFC,
               DETOPC.NOM_CDD_BFC,
               DETOPC.COD_PAS_BFC,
               DETOPC.NOM_ODN,
               DETOPC.NUM_DOC_ODN,
               DETOPC.COD_TPD_ODN,
               DETOPC.NUM_CTA_ODN,
               DETOPC.NUM_CTA_DCv_ODN,
               DETOPC.GLS_DIR_ODN,
               DETOPC.NOM_CDD_ODN,
               DETOPC.COD_PAS_ODN,
               DETOPC.NUM_CLv_NGC,
               DETOPC.NUM_AGT,
               DETOPC.COD_FND_CCLV,
               DETOPC.COD_TPO_SDO,
               DETOPC.COD_TPO_CMA,
               DETOPC.COD_TPO_FND,
               DETOPC.FEC_TPO_OPE_CCLV,
               DETOPC.NUM_CLv_IDF,
               DETOPC.GLS_EST_RCH,
               DETOPC.GLS_MTv_VSD,
               CAB.COD_USR
        FROM   PABS_DT_DETLL_NOMNA DETNOM,
               PABS_DT_NOMNA_INFCN_OPCON DETOPC,
               PABS_DT_CBCRA_NOMNA CAB
       WHERE       DETNOM.NUM_FOL_OPE = p_NUM_FOL_OPE
               AND DETNOM.FEC_ISR_OPE = p_FEC_ISR_OPE
               AND CAB.NUM_FOL_NMN = DETNOM.NUM_FOL_NMN
               AND DETNOM.NUM_FOL_OPE = DETOPC.NUM_FOL_OPE(+)
               AND DETNOM.FEC_ISR_OPE = DETOPC.FEC_ISR_OPE(+);

   R_CONSULTA_OPE_NOM   C_CONSULTA_OPE_NOM%ROWTYPE;


   --*********************Declaracion Cursor C_CONSULTA_NOMINA************************
   -- //@Santander
   CURSOR C_CONSULTA_NOMINA (
      p_NUM_FOL_NMN   IN            NUMBER
   )
   IS
      SELECT   FEC_ISR_OPE,
               NUM_FOL_OPE,
               COD_EST_AOS,
               COD_BCO_DTN,
               COD_MT_SWF,
               COD_TPO_OPE_AOS,
               COD_DVI
        FROM   PABS_DT_DETLL_NOMNA
       WHERE   NUM_FOL_NMN = p_NUM_FOL_NMN
               AND COD_EST_AOS <> PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEELI; --No obtenemos las operaciones que estan eliminadas.

   R_CONSULTA_NOMINA    C_CONSULTA_NOMINA%ROWTYPE;


   -- ********************************************************************************
   -- //@Santander
   -- Funcion/Procedimiento: Sp_PAB_INS_CAB_NOMINA
   -- Objetivo: Procedimiento que inserta la informacion de la cabecera de nomina
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_CBCRA_NOMNA
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_NMN  -> Numero foliador de la nomina
   -- p_GLS_MTv_EST  -> Informacion adicional del estado
   -- p_NOM_NMN      -> Nombre de la nomina
   -- p_COD_SIS_ENT  -> Codigo del canal de entrada
   -- p_COD_USR      -> Rut usuario
   -- p_EST_NMN      -> Indica el estado de la nomina
   -- Output:
   -- p_ERROR        -> Indicador de errores (0 Sin errores :: 1 existen errores). Se ven en la tabla Log estos errores.
   -- Input/Output:  N/A
   -- Retorno:       N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ********************************************************************************
   PROCEDURE Sp_PAB_INS_CAB_NOMINA (p_NUM_FOL_NMN   IN     NUMBER,
                                    p_GLS_MTv_EST   IN     VARCHAR2,
                                    p_NOM_NMN       IN     VARCHAR2,
                                    p_COD_SIS_ENT   IN     CHAR,
                                    p_COD_USR       IN     CHAR,
                                    p_EST_NMN       IN     NUMBER,
                                    p_ERROR            OUT NUMBER);


   -- ********************************************************************************
   -- //@Santander
   -- Funcion/Procedimiento : Sp_PAB_INS_DET_NOMINA
   -- Objetivo : Procedimiento que inserta la informacion que se encuentra en la nomina
   -- Sistema : PAB
   -- Base de Datos : DGBMSEGDB01_NGALTM
   -- Tablas Usadas : PABS_DT_DETLL_NOMNA
   -- Fecha : 21/06/16
   -- Autor : Santander
   -- Input:
   -- p_NUM_FOL_OPE        -> Numero Foliador de la operacion
   -- p_NOM_BFC            -> Nombre del Beneficiario
   -- p_NUM_FOL_NMN        -> Numero Foliador de la nomina
   -- p_NUM_OPE_SIS_ENT    -> Numero de operacion de la operacion en el sistema origen
   -- p_COD_MT_SWF         -> Numero del tipo de mensaje swift
   -- p_COD_TPO_OPE_AOS    -> Tipo de operacion.
   -- p_FEC_VTA            -> Fecha de vencimiento
   -- p_COD_DVI            -> Codigo de la divisa
   -- p_IMp_OPE            -> Importe de la operacion
   -- p_COD_BCO_DTN        -> BIC del banco destino
   -- p_NUM_DOC_BFC        -> Numero rut del beneficiario
   -- p_NUM_CTA_BFC        -> Numero de cuenta del beneficiario
   -- p_COD_SUC            -> Codigo de la sucursal
   -- p_GLS_ADC_EST        -> Informacion adicional
   -- p_NUM_REF_CTB        -> Numero de referencia contable
   -- p_OBS_OPC_SWF        -> Observacion Opcional Swift
   -- p_COD_BCO_BFC        -> BIC Banco Beneficario
   -- p_COD_BCO_ITD        -> BIC Banco Intermediario
   -- p_NUM_CTA_DCv_BFC    -> Numero cuenta DCV Beneficiario
   -- p_GLS_DIR_BFC        -> Direccion Beneficario
   -- p_NOM_CDD_BFC        -> Nombre ciudad beneficiario
   -- p_COD_PAS_BFC        -> Codigo pais beneficiario
   -- p_NOM_ODN            -> Nombre ordenante
   -- p_NUM_DOC_ODN        -> Numero rut ordenante
   -- p_NUM_CTA_ODN        -> Numero cuenta ordenante
   -- p_NUM_CTA_DCv_ODN    -> Numero cuenta DCV ordenante
   -- p_GLS_DIR_ODN        -> Direccion ordenante
   -- p_NOM_CDD_ODN        -> Nombre ciudad ordenante
   -- p_COD_PAS_ODN        -> Codigo pais ordenante
   -- p_NUM_CLv_NGC        -> Numero de clave de negocio
   -- p_NUM_AGT            -> Numero de agente
   -- p_COD_FND_CCLV       -> Codigo Fondo (Producto CCLV)
   -- p_COD_TPO_SDO        -> Tipo de saldo
   -- p_COD_TPO_CMA        -> Tipo de camara
   -- p_COD_TPO_FND        -> Tipo de fondo
   -- p_FEC_TPO_OPE_CCLV   -> Fecha operacion CCLV
   -- p_NUM_CLv_IDF        -> Numero clave identificatorio
   -- p_COD_EST_OPE        -> Indica si la operacion esta con errores
   -- p_COD_USR->          -> Rut usuario
   -- p_COD_TPO_ING        -> Tipo de ingreso de la operacion (Masiva, unitaria)
   -- Output :
   -- p_FEC_ISR_OPE        --> Fecha con la que se inserto la operacion al sistema
   -- p_ERROR              -> Indicador codigo error
   -- Input/Output : N/A
   -- Retorno : N/A.
   -- Observaciones : <Fecha y Detalle de ultimos cambios>
   -- ********************************************************************************
   PROCEDURE Sp_PAB_INS_DET_NOMINA (p_NOM_BFC            IN     VARCHAR2,
                                    p_NUM_FOL_NMN        IN     NUMBER,
                                    p_NUM_OPE_SIS_ENT    IN     CHAR,
                                    p_COD_MT_SWF         IN     NUMBER,
                                    p_COD_TPO_OPE_AOS    IN     CHAR,
                                    p_FEC_VTA            IN     DATE,
                                    p_COD_DVI            IN     CHAR,
                                    p_IMp_OPE            IN     NUMBER,
                                    p_COD_BCO_DTN        IN     CHAR,
                                    p_NUM_DOC_BFC        IN     CHAR,
                                    p_NUM_CTA_BFC        IN     VARCHAR2,
                                    p_COD_SUC            IN     VARCHAR2,
                                    p_GLS_ADC_EST        IN     VARCHAR2,
                                    p_NUM_REF_CTB        IN     CHAR,
                                    -----------------------------
                                    p_OBS_OPC_SWF        IN     VARCHAR2,
                                    p_COD_BCO_BFC        IN     CHAR,
                                    p_COD_BCO_ITD        IN     CHAR,
                                    p_NUM_CTA_DCv_BFC    IN     VARCHAR2,
                                    p_GLS_DIR_BFC        IN     VARCHAR2,
                                    p_NOM_CDD_BFC        IN     VARCHAR2,
                                    p_COD_PAS_BFC        IN     CHAR,
                                    p_NOM_ODN            IN     VARCHAR2,
                                    p_NUM_DOC_ODN        IN     CHAR,
                                    p_NUM_CTA_ODN        IN     VARCHAR2,
                                    p_NUM_CTA_DCv_ODN    IN     VARCHAR2,
                                    p_GLS_DIR_ODN        IN     VARCHAR2,
                                    p_NOM_CDD_ODN        IN     VARCHAR2,
                                    p_COD_PAS_ODN        IN     CHAR,
                                    p_NUM_CLv_NGC        IN     VARCHAR2,
                                    p_NUM_AGT            IN     CHAR,
                                    p_COD_FND_CCLV       IN     CHAR,
                                    p_COD_TPO_SDO        IN     CHAR,
                                    p_COD_TPO_CMA        IN     CHAR,
                                    p_COD_TPO_FND        IN     VARCHAR2,
                                    p_FEC_TPO_OPE_CCLV   IN     DATE,
                                    p_NUM_CLv_IDF        IN     VARCHAR2,
                                    --------------------------------
                                    p_COD_EST_OPE        IN     NUMBER,
                                    p_COD_USR            IN     CHAR,
                                    p_COD_TPO_ING        IN     NUMBER,
                                    p_FEC_ISR_OPE           OUT DATE,
                                    p_NUM_FOL_OPE           OUT NUMBER,
                                    p_ERROR                 OUT NUMBER);


   -- ********************************************************************************
   -- //@Santander
   -- IDENTIFICACION PROCEDIMIENTO-> Sp_PAB_INS_DET_OPC_NOMINA
   -- Descripcion-> Procedimiento que inserta la informacion que se encuentra en la nomina
   -- Sistema-> PAB
   -- Base de Datos-> DGBMSEGDB01_NGALTM
   -- Tablas Usadas-> PABS_DT_DETLL_NOMNA
   -- Fecha-> 21/06/16
   -- Autor-> Santander
   -- Input->
   -- p_FEC_ISR_OPE     -> Fecha de cuando fue insertada la operacion
   -- p_NUM_FOL_OPE     -> Numero Foliador de la operacion
   -- p_COD_BCO_DTN     -> Codigo banco destino
   -- p_NUM_DOC_BFC     -> Numero rut del beneficiario
   -- p_COD_TPD_BFC     -> Digito verificador beneficiario
   -- p_OBS_OPC_SWF     -> Observacion Opcional Swift
   -- p_COD_BCO_BFC     -> BIC Banco Beneficario
   -- p_COD_BCO_ITD     -> BIC Banco Intermediario
   -- p_NUM_CTA_DCv_BFC -> Numero cuenta DCV Beneficiario
   -- p_GLS_DIR_BFC     ->Direccion Beneficario
   -- p_NOM_CDD_BFC     -> Nombre ciudad beneficiario
   -- p_COD_PAS_BFC     -> Codigo pais beneficiario
   -- p_NOM_ODN         -> Nombre ordenante
   -- p_NUM_DOC_ODN     -> Numero rut ordenante
   -- p_NUM_CTA_ODN     -> Numero cuenta ordenante
   -- p_NUM_CTA_DCv_ODN -> Numero cuenta DCV ordenante
   -- p_GLS_DIR_ODN     -> Direccion ordenante
   -- p_NOM_CDD_ODN     -> Nombre ciudad ordenante
   -- p_COD_PAS_ODN     -> Codigo pais ordenante
   -- p_NUM_CLv_NGC     -> Numero de clave de negocio
   -- p_NUM_AGT         -> Numero de agente
   -- p_COD_FND_CCLV    -> Codigo Fondo (Producto CCLV)
   -- p_COD_TPO_SDO     -> Tipo de saldo
   -- p_COD_TPO_CMA     -> Tipo de camara
   -- p_COD_TPO_FND     -> Tipo de fondo
   -- p_FEC_TPO_OPE_CCLV -> Fecha operacion CCLV
   -- p_NUM_CLv_IDF      -> Numero clave identificatorio
   -- Output :
   -- p_ERROR              -> Indicador codigo error
   -- Input/Output : N/A
   -- Retorno : N/A.
   -- Observaciones : <Fecha y Detalle de ultimos cambios>
   --******************************************************************************
   PROCEDURE Sp_PAB_INS_DET_OPC_NOMINA (p_FEC_ISR_OPE        IN     DATE,
                                        p_NUM_FOL_OPE        IN     NUMBER,
                                        p_COD_BCO_DTN        IN     CHAR,
                                        ---------------------------------
                                        p_NUM_DOC_BFC        IN     CHAR,
                                        p_COD_TPD_BFC        IN     CHAR,
                                        p_OBS_OPC_SWF        IN     VARCHAR2,
                                        p_COD_BCO_BFC        IN     CHAR,
                                        p_COD_BCO_ITD        IN     CHAR,
                                        p_NUM_CTA_DCv_BFC    IN     VARCHAR2,
                                        p_GLS_DIR_BFC        IN     VARCHAR2,
                                        p_NOM_CDD_BFC        IN     VARCHAR2,
                                        p_COD_PAS_BFC        IN     CHAR,
                                        p_NOM_ODN            IN     VARCHAR2,
                                        p_NUM_DOC_ODN        IN     CHAR,
                                        p_NUM_CTA_ODN        IN     VARCHAR2,
                                        p_NUM_CTA_DCv_ODN    IN     VARCHAR2,
                                        p_GLS_DIR_ODN        IN     VARCHAR2,
                                        p_NOM_CDD_ODN        IN     VARCHAR2,
                                        p_COD_PAS_ODN        IN     CHAR,
                                        p_NUM_CLv_NGC        IN     VARCHAR2,
                                        p_NUM_AGT            IN     CHAR,
                                        p_COD_FND_CCLV       IN     CHAR,
                                        p_COD_TPO_SDO        IN     CHAR,
                                        p_COD_TPO_CMA        IN     CHAR,
                                        p_COD_TPO_FND        IN     VARCHAR2,
                                        p_FEC_TPO_OPE_CCLV   IN     DATE,
                                        p_NUM_CLv_IDF        IN     VARCHAR2,
                                        p_ERROR                 OUT NUMBER);

   -- ***********************************************************************************************
   -- //@Santander
   -- Funcion/Procedimiento: Sp_PAB_BUS_CAB_NOMINA
   -- Objetivo: Procedimiento que busca las nominas segun el estado de entrada
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_CBCRA_NOMNA, PABS_DT_ESTDO_ALMOT
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- p_COD_EST_AOS -> Estado de la nomina
   -- p_COD_SIS_ENT -> codigo del canal de entrada
   -- Output:
   -- p_CURSOR      -> Cursor con las nominas encontradas
   -- p_ERROR       -> Indicador codigo error
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_BUS_CAB_NOMINA (p_COD_EST_AOS   IN     CHAR,
                                    p_COD_SIS_ENT   IN     CHAR,
                                    p_CURSOR           OUT SYS_REFCURSOR,
                                    p_ERROR            OUT NUMBER);


   -- ***********************************************************************************************
   -- //@Santander
   -- Funcion/Procedimiento: FN_PAB_BUS_DVI_NOMINA
   -- Objetivo: Fusion que trae los el tipo de moneda de la nomina
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- p_FLG_DVI     -> flag de multimoneda
   -- p_NUM_FOL_NMN -> foliador de la nomina
   -- Output:
   -- N/A
   -- Input/Output: N/A
   -- Retorno:
   -- p_TIPO_DIV
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   FUNCTION FN_PAB_BUS_DVI_NOMINA (p_FLG_DVI       IN NUMBER,
                                   p_NUM_FOL_NMN   IN NUMBER)
      RETURN VARCHAR2;

   -- ***********************************************************************************************
   -- //@Santander
   -- Funcion/Procedimiento: FN_PAB_BUS_DVI_NOMINA_CON
   -- Objetivo: Fusion que trae los el tipo de moneda de la nomina
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- p_FLG_DVI     -> flag de multimoneda
   -- p_NUM_FOL_NMN -> foliador de la nomina
   -- Output:
   -- N/A
   -- Input/Output: N/A
   -- Retorno:
   -- p_TIPO_DIV
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   FUNCTION FN_PAB_BUS_DVI_NOMINA_CON (p_FLG_DVI       IN NUMBER,
                                       p_NUM_FOL_NMN   IN NUMBER)
      RETURN VARCHAR2;

   -- ***********************************************************************************************
   -- //@Santander
   -- Funcion/Procedimiento: FN_PAB_BUS_DVI_NOMINA_CON_HIS
   -- Objetivo: Fusion que trae los el tipo de moneda de la nomina
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- p_FLG_DVI     -> flag de multimoneda
   -- p_NUM_FOL_NMN -> foliador de la nomina
   -- P_FEC_CGA_HIS -> Fecha carga historica
   -- Output:
   -- N/A
   -- Input/Output: N/A
   -- Retorno:
   -- p_TIPO_DIV
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   FUNCTION FN_PAB_BUS_DVI_NOMINA_CON_HIS (p_FLG_DVI       IN NUMBER,
                                           p_NUM_FOL_NMN   IN NUMBER,
                                           P_FEC_CGA_HIS      DATE)
      RETURN VARCHAR2;

   --
   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUS_DET_NOMINA
   -- Objetivo: Procedimiento almacenado que retorna todos las operaciones de una nomina que no se encuentren
   -- eliminadas
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA, PABS_DT_SISTM_ENTRD_SALID, PABS_DT_CBCRA_NOMNA, TCDT040, TCDTBAI
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_NMN -> Numero de la nomina
   -- Output:
   -- p_CURSOR -> Cursor con las nominas encontradas
   -- p_ERROR-> Indicador codigo error
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_BUS_DET_NOMINA (p_NUM_FOL_NMN   IN     NUMBER,
                                    p_CURSOR           OUT SYS_REFCURSOR,
                                    p_ERROR            OUT NUMBER);


   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_TOT_DVI_NMN_MULTI
   -- Objetivo: Procedimiento que retorna el total de las operaciones agrupados por sus monedas
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_CBCRA_NOMNA, PABS_DT_DETLL_NOMNA, PABS_DT_ESTDO_ALMOT
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_NMN -> Numero de la nomina
   -- Output:
   -- p_CURSOR -> SYS_REFCURSOR, resultados de consulta.
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_TOT_DVI_NMN_MULTI (p_NUM_FOL_NMN   IN     NUMBER,
                                       p_CURSOR           OUT SYS_REFCURSOR,
                                       p_ERROR            OUT NUMBER);


   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_GES_CAM_EST_NOMINA
   -- Objetivo: Procedimiento que llama procedimiento que actualiza estado de nomina.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: N/A
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_NMN -> arreglo con los numeros de foliador del banco
   -- p_COD_EST_AOS -> codigo del estado con el cual se va a actualizar
   -- p_COD_USR -> rut usuario
   -- p_GLS_MTV_EST -> motivo o glosa de la actualizacion
   -- Output:
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_GES_CAM_EST_NOMINA (p_NUM_FOL_NMN   IN     T_NUM_FOL_NMN,
                                        p_COD_EST_AOS   IN     CHAR,
                                        p_COD_USR       IN     VARCHAR2,
                                        p_GLS_MTV_EST   IN     VARCHAR2,
                                        p_ERROR            OUT NUMBER);

   --
   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ACT_EST_CAB_NOMINA
   -- Objetivo: Procedimiento que actualiza el estado de la cabecera de una nomina y sus operaciones, registra en la bitacora los cambios
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_CBCRA_NOMNA
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_NMN -> foliador del banco
   -- p_COD_EST_AOS -> Identificador de nuevo estado.
   -- p_COD_USR -> rut usuario
   -- p_GLS_MTV_EST -> motivo o glosa de la actualizacion
   -- Output:
   -- p_ERROR -> p_ERROR-> Indicador codigo error
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_ACT_EST_CAB_NOMINA (p_NUM_FOL_NMN   IN     NUMBER,
                                        p_COD_EST_AOS   IN     CHAR,
                                        p_COD_USR       IN     VARCHAR2,
                                        p_GLS_MTV_EST   IN     VARCHAR2,
                                        p_ERROR            OUT NUMBER);

   --
   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_REC_ACT_OPE_NOMINA
   -- Objetivo: Procedimiento que recorre todas las operaciones de una nomin, para luego llamar a otros
   -- procedimientos que actualizan estado y graban la bitacora Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: N/A
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_NMN -> foliador del banco
   -- p_COD_EST_AOS -> Identificador de nuevo estado.
   -- p_COD_USR -> identificador usuario
   -- p_GLS_MTV_EST -> motivo o glosa de la actualizacion
   -- Output:
   -- p_ERROR -> Indicador codigo error
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_REC_ACT_OPE_NOMINA (p_NUM_FOL_NMN   IN     NUMBER,
                                        p_COD_EST_AOS   IN     NUMBER,
                                        p_COD_USR       IN     VARCHAR2,
                                        p_GLS_MTV_EST   IN     VARCHAR2,
                                        p_ERROR            OUT NUMBER);

   --
   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ACT_EST_OPE_NOMINA
   -- Objetivo: Procedimiento que actualiza el estado de una operacion de una nomina.
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_OPE -> foliador del banco, PK tabla operaciones nomina.
   -- p_FEC_ISR_OPE -> Fecha de insercion de operacion en el sistema, PK tabla operaciones n?mina.
   -- p_COD_EST_AOS -> Identificador de nuevo estado.
   -- p_GLS_ADC_EST -> motivo o glosa de la actualizacion
   -- Output:
   -- p_ERROR -> Indicador codigo error
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_ACT_EST_OPE_NOMINA (p_NUM_FOL_OPE   IN     NUMBER,
                                        p_FEC_ISR_OPE   IN     DATE,
                                        p_COD_EST_AOS   IN     NUMBER,
                                        p_GLS_ADC_EST   IN     VARCHAR2,
                                        p_ERROR            OUT NUMBER);

   --
   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUS_DET_OPE_NOM
   -- Objetivo: Procedimiento almacenado que retorna el detalle de una operacion seleccionada
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_OPE->   foliador del banco, PK tabla operaciones nomina.
   -- p_FEC_ISR_OPE->   Fecha de insercion de operacion en el sistema, PK tabla operaciones nomina.
   -- Output:
   -- p_CURSOR -> Informacion de la operacion
   -- p_ERROR -> Indicador codigo error
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_BUS_DET_OPE_NOM (p_NUM_FOL_OPE   IN     NUMBER,
                                     p_FEC_ISR_OPE   IN     DATE,
                                     p_CURSOR           OUT SYS_REFCURSOR,
                                     p_ERROR            OUT NUMBER);

   --
   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ACT_DATOS_OPE_NOM
   -- Objetivo:  Procedimiento que actualiza informacion de una operacion de una nomina
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_OPE -> foliador del banco
   -- p_FEC_ISR_OPE -> Fecha de insercion de la operacion
   -- p_NUM_REF_SWF -> Numero de referencia del swift
   -- p_FEC_ENv_RCp_SWF -> Fecha Swift
   -- p_HOR_ENv_RCp_SWF -> Hora Swift
   -- p_GLS_ADC_EST  ->Glosa Adicional Estado
   -- p_GLS_EST_RCH  -> Glosa Estado Rechazado
   -- p_GLS_MTv_VSD  -> Glosa Motivo Visacion
   -- Output:
   -- p_ERROR -> p_ERROR-> Indicador codigo error
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_ACT_DATOS_OPE_NOM (p_NUM_FOL_OPE       IN     NUMBER,
                                       p_FEC_ISR_OPE       IN     DATE,
                                       p_NUM_REF_SWF       IN     VARCHAR2,
                                       p_FEC_ENv_RCp_SWF   IN     DATE,
                                       p_HOR_ENv_RCp_SWF   IN     NUMBER,
                                       p_GLS_ADC_EST       IN     VARCHAR2,
                                       p_GLS_EST_RCH       IN     VARCHAR2,
                                       p_GLS_MTv_VSD       IN     VARCHAR2,
                                       p_ERROR                OUT NUMBER);


   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ACT_OPE_NOM
   -- Objetivo: Procedimiento almacenado que actualiza informacion de una operaciones de una nomina
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA, PABS_DT_NOMNA_INFCN_OPCON
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_OPE      IN NUMBER,   -> Numero de la Operacion
   -- p_FEC_ISR_OPE      IN DATE,     -> Fecha de insercion de la operacion
   -- p_COD_USR          IN CHAR,     -> Usuario que realiza la modificacion
   -- p_NUM_DOC_ODN      IN CHAR,     -> Rut del ordenante
   -- p_COD_TPD_ODN      IN CHAR,     -> Digito Verificador del ordenante
   -- p_NOM_ODN          IN VARCHAR2, -> Nombre Ordenate
   -- p_NUM_CTA_ODN      IN VARCHAR2, -> Numero cuenta ordenante
   -- p_GLS_DIR_ODN      IN VARCHAR2, -> Glosa direccion ordenante
   -- p_NOM_CDD_ODN      IN VARCHAR2, -> nombre ciudad ordenante
   -- p_COD_PAS_ODN      IN CHAR,     -> codigo pais ordenante
   -- p_NUM_CTA_DCV_ODN  IN VARCHAR2, -> numero cuenta dcv ordenante
   -- p_COD_BCO_DTN      IN CHAR,     -> codigo bic banco destino
   -- p_NUM_DOC_BFC      IN CHAR,     -> rut del beneficiario
   -- p_COD_TPD_BFC      IN CHAR,     -> Digito Verificador del beneficiario
   -- p_NOM_BFC          IN VARCHAR2, -> Nombre beneficiario
   -- p_NUM_CTA_BFC      IN VARCHAR2, -> Numero cuenta beneficiario
   -- p_GLS_DIR_BFC      IN VARCHAR2, -> Glosa direccion beneficiario
   -- p_NOM_CDD_BFC      IN VARCHAR2, -> nombre ciudad beneficiario
   -- p_COD_PAS_BFC      IN CHAR,     -> codigo pais beneficiario
   -- p_NUM_CTA_DCV_BFC  IN VARCHAR2, -> numero cuenta dcv beneficiario
   -- p_COD_BCO_ITD      IN CHAR,      -> codigo bic banco intermediario
   -- p_COD_BCO_BFC      IN CHAR,      -> codigo bic banco corresponsal
   -- p_IMP_OPE          IN NUMBER,      -> monto operacion
   -- p_FEC_VTA          IN DATE,     -> fecha de valuta
   -- p_COD_SUC          IN VARCHAR2, -> codigo sucursal
   -- p_NUM_REF_CTB      IN CHAR,      -> numero referencia contable
   -- p_FEC_TPO_OPE_CCLV IN DATE,     -> fecha cclv
   -- p_COD_TPO_SDO      IN CHAR,      -> tipo saldo
   -- p_COD_TPO_CMA      IN CHAR,      -> tipo camara
   -- p_NUM_AGT          IN CHAR,      -> numero agente
   -- p_NUM_CLV_NGC      IN VARCHAR2, -> numero clave de negocio
   -- p_COD_TPO_FND      IN CHAR,     -> tipo fondo
   -- p_NUM_CLv_IDF      IN VARCHAR2, -> numero clave identificatorio
   -- P_NUM_OPE_SIS_ENT  IN CHAR,     -> numero operacion origen
   -- p_OBS_OPC_SWF      IN VARCHAR2, -> glosa swift
   -- Output:
   -- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_ACT_OPE_NOM (p_NUM_FOL_OPE        IN     NUMBER,
                                 p_FEC_ISR_OPE        IN     DATE,
                                 p_COD_USR            IN     CHAR,
                                 ------Ordenante---------------------
                                 p_NUM_DOC_ODN        IN     CHAR,
                                 p_COD_TPD_ODN        IN     CHAR,
                                 p_NOM_ODN            IN     VARCHAR2,
                                 p_NUM_CTA_ODN        IN     VARCHAR2,
                                 p_GLS_DIR_ODN        IN     VARCHAR2,
                                 p_NOM_CDD_ODN        IN     VARCHAR2,
                                 p_COD_PAS_ODN        IN     CHAR,
                                 p_NUM_CTA_DCV_ODN    IN     VARCHAR2,
                                 --------Beneficiario-----------------
                                 p_COD_BCO_DTN        IN     CHAR,
                                 p_NUM_DOC_BFC        IN     CHAR,
                                 p_COD_TPD_BFC        IN     CHAR,
                                 p_NOM_BFC            IN     VARCHAR2,
                                 p_NUM_CTA_BFC        IN     VARCHAR2,
                                 p_GLS_DIR_BFC        IN     VARCHAR2,
                                 p_NOM_CDD_BFC        IN     VARCHAR2,
                                 p_COD_PAS_BFC        IN     CHAR,
                                 p_NUM_CTA_DCV_BFC    IN     VARCHAR2,
                                 --------Corresponsales-------------------
                                 p_COD_BCO_ITD        IN     CHAR,
                                 p_COD_BCO_BFC        IN     CHAR,
                                 --------Operacion------------------------
                                 p_IMP_OPE            IN     NUMBER,
                                 p_FEC_VTA            IN     DATE,
                                 p_COD_SUC            IN     VARCHAR2,
                                 p_NUM_REF_CTB        IN     CHAR,
                                 p_FEC_TPO_OPE_CCLV   IN     DATE,
                                 p_COD_TPO_SDO        IN     CHAR,
                                 p_COD_TPO_CMA        IN     CHAR,
                                 p_NUM_AGT            IN     CHAR,
                                 p_NUM_CLV_NGC        IN     VARCHAR2,
                                 p_COD_TPO_FND        IN     CHAR,
                                 p_NUM_CLv_IDF        IN     VARCHAR2,
                                 P_NUM_OPE_SIS_ENT    IN     CHAR,
                                 p_OBS_OPC_SWF        IN     VARCHAR2,
                                 p_ERROR                 OUT NUMBER);

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_GES_CAM_EST_DUPLI
   -- Objetivo: Procedimiento almacenado que recibe las operaciones a actualizar su estado de duplicada por no duplicada
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- p_REG_OPER -> Numero de Operaciones
   -- p_NUM_FOL_NMN -> Numero de la nomina
   -- p_COD_USR -> Numero del usuario
   -- Output:
   -- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_GES_CAM_EST_DUPLI (p_REG_OPER      IN     REG_OPER,
                                       p_NUM_FOL_NMN   IN     NUMBER,
                                       p_COD_USR       IN     CHAR,
                                       p_ERROR            OUT NUMBER);

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ACT_OPE_DUP
   -- Objetivo: Procedimiento almacenado que actualiza las operaciones duplicada por no duplicada
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- p_FEC_ISR_OPE -> Fecha Operacion
   -- p_NUM_FOL_OPE -> Numero de Operacion
   -- p_COD_USR -> Numero del usuario
   -- Output:
   -- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_ACT_OPE_DUP (p_FEC_ISR_OPE   IN     DATE,
                                 p_NUM_FOL_OPE   IN     NUMBER,
                                 p_COD_USR       IN     CHAR,
                                 p_ERROR            OUT NUMBER);

   --**********************************************************************************************
   -- Funcion/Procedimiento : Sp_PAB_ACT_EST_NMN_DUP
   -- Objetivo : Procedimiento que inserta la informacion que se encuentra en la nomina
   -- Sistema : PAB
   -- Base de Datos : DGBMSEGDB01_NGALTM
   -- Tablas Usadas : PABS_DT_DETLL_NOMNA, PABS_DT_CBCRA_NOMNA
   -- Fecha : 21/06/16
   -- Autor : Santander
   -- Input:
   -- p_NUM_FOL_NMN  ->  Numero de folio nomina
   -- Output :
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output : N/A
   -- Retorno : N/A.
   -- Observaciones : <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_ACT_EST_NMN_DUP (p_NUM_FOL_NMN   IN     NUMBER,
                                     p_ERROR            OUT NUMBER);

   --***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_ACT_SOL_EST_CAB_NOM
   -- Objetivo: Procedimiento que actualiza solo estado de la cabecera de una nomina
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_CBCRA_NOMNA
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_NMN -> foliador del banco
   -- p_COD_EST_AOS -> Identificador de nuevo estado.
   -- p_GLS_MTV_EST -> motivo o glosa de la actualizacion
   -- Output:
   -- p_ERROR -> p_ERROR-> Indicador codigo error
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_ACT_SOL_EST_CAB_NOM (p_NUM_FOL_NMN   IN     NUMBER,
                                         p_COD_EST_AOS   IN     CHAR,
                                         p_GLS_MTV_EST   IN     VARCHAR2,
                                         p_ERROR            OUT NUMBER);

   --***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_VAL_OPER_DUPLICADA
   -- Objetivo: Consulta las posibles operaciones duplicadas
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas: N/A
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- P_FEC_VTA       : Fecha de valuta
   -- P_COD_BCO_DTN   : codigo del banco destinatario
   -- P_NUM_CTA_BFC   : cuenta beneficiario
   -- P_IMP_OPE       : importe total
   -- P_COD_USR       : codigo del usuario
   -- Output:
   -- P_OPER_DUP -> Retorna 1 o 0 si esta o no duplicada.
   -- p_ERROR-> Indicador codigo error
   -- Input/Output:
   -- N/A.
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_PAB_VAL_OPER_DUPLICADA (P_FEC_VTA       IN     DATE,
                                        P_COD_BCO_DTN   IN     VARCHAR2,
                                        P_NUM_CTA_BFC   IN     VARCHAR2,
                                        P_IMP_OPE       IN     NUMBER,
                                        P_COD_USR       IN     CHAR,
                                        P_OPER_DUP         OUT NUMBER,
                                        P_ERROR            OUT NUMBER);


   --***********************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_BUS_DUPLICADO
   -- Objetivo: Consulta las posibles operaciones duplicadas
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA, PABS_DT_CBCRA_NOMNA
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- P_FEC_VTA -> fecha valuta
   -- P_COD_BCO_DTN  -> banco destino
   -- P_NUM_CTA_BFC  -> numero cuenta beneficiaria
   -- P_IMP_OPE     ->  importe de una operacion
   -- P_COD_SIS_ENT  -> codigo de sistema entrada
   -- N/A
   -- Output:
   -- N/A.
   -- Input/Output:
   -- N/A.
   -- Retorno:
   -- PKG_PAB_CONSTANTES_2.V_COD_DUP_OK -> flag duplicado ok / PKG_PAB_CONSTANTES_2.V_COD_NO_DUP -> flag no duplicado
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   FUNCTION FN_PAB_BUS_DUPLICADO (P_FEC_VTA       IN DATE,
                                  P_COD_BCO_DTN   IN CHAR,
                                  P_NUM_CTA_BFC   IN VARCHAR2,
                                  P_IMP_OPE       IN NUMBER,
                                  P_COD_SIS_ENT   IN CHAR)
      RETURN NUMBER;

   --***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_VERIF_NOM_DUPL
   -- Objetivo: Consulta la si la nomina esta duplicada
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA, PABS_DT_CBCRA_NOMNA
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- P_NUM_FOL_NMN   : numero de folio nomina
   -- Output:
   -- P_RESUL_OPE_DUP -> Retorna 1 o 0 si esta o no duplicada.
   -- P_ERROR -> Indicador codigo error
   -- Input/Output:
   -- N/A.
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_PAB_VERIF_NOM_DUPL (P_NUM_FOL_NMN     IN     NUMBER,
                                    P_RESUL_OPE_DUP      OUT NUMBER,
                                    P_ERROR              OUT NUMBER);

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_GES_ELI_OPE
   -- Objetivo: Procedimiento que actualiza el estado de una operacion
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: N/A
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- p_REG_OPER -> parametro tipo registro que contiene el numero y fecha operacion
   -- p_COD_USR -> rut usuario
   -- p_GLS_MTV_EST -> motivo o glosa de eliminacion
   -- p_SIS_NOM_OPE -> origen al que pertenece la operacion. NOM = Nomina - OPE = Operacion
   -- Output:
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_GES_ELI_OPE (p_REG_OPER      IN     REG_OPER,
                                 p_COD_USR       IN     VARCHAR2,
                                 p_GLS_MTV_EST   IN     VARCHAR2,
                                 p_SIS_NOM_OPE   IN     CHAR,
                                 p_ERROR            OUT NUMBER);

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ACT_ELI_NMN
   -- Objetivo: Procedimiento que actualiza el estado de una nomina con todas las operaciones eliminadas
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA, PABS_DT_DETLL_OPRCN, PABS_DT_CBCRA_NOMNA
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_NMN -> foliador del banco
   -- p_GLS_MTV_EST -> motivo o glosa de eliminacion
   -- Output:
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_ACT_ELI_NMN (p_NUM_FOL_NMN   IN     NUMBER,
                                 p_GLS_MTV_EST   IN     VARCHAR2,
                                 p_ERROR            OUT NUMBER);

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_CON_NOM_HIST
   -- Objetivo: Procedimiento almacenado que retorna las consultas operaciones historicas de una nomina
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN, PABS_DT_OPRCN_INFCN_OPCON, PABS_DT_DETLL_OPRCN_HTRCA, PABS_DT_OPRCN_OPCON_HTRCA
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- p_COD_EST_AOS -> codigo estado
   -- p_COD_SIS_ENT -> codigo sistema entrada
   -- p_COD_USR     -> codigo usuario
   -- p_NUM_FOL_NMN -> numero folio nomina
   -- p_FEC_DESDE   -> fecha desde
   -- p_FEC_HASTA   -> fecha hasta
   -- p_IMp_TOT_DESDE -> importe total desde
   -- p_IMp_TOT_HASTA -> importe total hasta
   -- Output:
   -- p_CURSOR -> Cursor con las nominas encontradas
   -- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_CON_NOM_HIST (p_COD_EST_AOS     IN     NUMBER,
                                  p_COD_SIS_ENT     IN     CHAR,
                                  p_COD_USR         IN     CHAR,
                                  p_NUM_FOL_NMN     IN     NUMBER,
                                  p_FEC_DESDE       IN     DATE,
                                  p_FEC_HASTA       IN     DATE,
                                  p_IMp_TOT_DESDE   IN     NUMBER,
                                  p_IMp_TOT_HASTA   IN     NUMBER,
                                  p_CURSOR             OUT SYS_REFCURSOR,
                                  p_ERROR              OUT NUMBER);


   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUS_DET_NOM_CON
   -- Objetivo: Procedimiento almacenado que retorna las operaciones de una nomina Historica o Normal
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA, PABS_DT_SISTM_ENTRD_SALID, PABS_DT_CBCRA_NOMNA, TCDT040, TCDTBAI
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_NMN -> Numero de la nomina
   -- p_TIP_BUS_HIST -> Indicador de tipo de busqueda H = Historica -  N = Normal
   -- Output:
   -- p_CURSOR -> Cursor con las nominas encontradas
   -- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_BUS_DET_NOM_CON (p_NUM_FOL_NMN    IN     NUMBER,
                                     p_TIP_BUS_HIST   IN     CHAR,
                                     p_CURSOR            OUT SYS_REFCURSOR,
                                     p_ERROR             OUT NUMBER);

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUS_DET_NOMINA_ELI
   -- Objetivo: Procedimiento almacenado que retorna las operaciones de una nomina
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA, PABS_DT_SISTM_ENTRD_SALID, PABS_DT_CBCRA_NOMNA, TCDT040, TCDTBAI
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_NMN -> Numero de la nomina
   -- Output:
   -- p_CURSOR -> Cursor con las nominas encontradas
   -- p_ERROR-> Indicador codigo error
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_BUS_DET_NOMINA_ELI (p_NUM_FOL_NMN   IN     NUMBER,
                                        p_CURSOR           OUT SYS_REFCURSOR,
                                        p_ERROR            OUT NUMBER);

   --***********************************************************************************************
   -- Funcion/Procedimiento: Fn_PAB_BUSCA_EST_OPE
   -- Objetivo: BUSCA EL ESTADO POR OPERACION Y DEVUELVE EL ESTADO
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas: PABS_DT_CBCRA_NOMNA, PABS_DT_DETLL_NOMNA
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_OPE -> NUMERO DE FOLIO OPERACION
   -- p_FEC_ISR_OPE -> Fecha de Operacion
   -- N/A
   -- Output:
   -- N/A.
   -- Input/Output:
   -- N/A.
   -- Retorno:
   -- v_COD_EST_AOS-> CODIGO DEL ESTADO
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   FUNCTION Fn_PAB_BUSCA_EST_OPE (p_NUM_FOL_OPE   IN NUMBER,
                                  p_FEC_ISR_OPE   IN DATE)
      RETURN NUMBER;

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUS_DET_OPE_NOM_CON
   -- Objetivo: Procedimiento almacenado que retorna el detalle de una operacion historica seleccionada
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_OPE->   foliador del banco, PK tabla operaciones nomina.
   -- p_FEC_ISR_OPE->   Fecha de valuta de la operacion.
   -- p_TIP_BUS_HIST-> tipo busqueda de historificacion
   -- Output:
   -- p_CURSOR -> Informacion de la operacion
   -- p_ERROR -> Indicador codigo error
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_BUS_DET_OPE_NOM_CON (
      p_NUM_FOL_OPE    IN     NUMBER,
      p_FEC_ISR_OPE    IN     DATE,
      p_TIP_BUS_HIST   IN     CHAR,
      p_CURSOR            OUT SYS_REFCURSOR,
      p_ERROR             OUT NUMBER
   );

   --
   --**********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_ELI_MOT_NO_HABLT
   -- Objetivo: Procedimiento que elimina registro de errores encontrados por las validaciones de integracion
   -- en las n?minas
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_MOTVO_NO_HABLT
   -- Fecha: 19/08/16
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_OPE -> Foliador del banco
   -- p_FEC_ISR_OPE -> Fecha de una Operacion
   -- Output:
   -- p_ERROR -> Indicador codigo error
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_PAB_ELI_MOT_NO_HABLT (p_NUM_FOL_OPE   IN     NUMBER,
                                      p_FEC_ISR_OPE   IN     DATE,
                                      p_ERROR            OUT NUMBER);

   --**********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_INS_ERR_VAL
   -- Objetivo: Procedimiento que registra los errores encontrados por las validaciones de integracion
   -- en las n?minas
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_MOTVO_NO_HABLT
   -- Fecha: 19/08/16
   -- Autor: Santander
   -- Input:
   -- p_FEC_ISR_OPE -> Fecha de una Operacion
   -- p_NUM_FOL_OPE -> Foliador del banco
   -- p_COD_ARC_ENT -> Codigo Archivo de Entrada
   -- p_COD_CAM_ARC -> Codigo camara
   -- p_COD_ERR_BIB -> Codigo error
   -- Output:
   -- p_ERROR -> Indicador codigo error
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_INS_ERR_VAL (p_FEC_ISR_OPE   IN     DATE,
                                 p_NUM_FOL_OPE   IN     NUMBER,
                                 p_COD_ARC_ENT   IN     CHAR,
                                 p_COD_CAM_ARC   IN     NUMBER,
                                 p_COD_ERR_BIB   IN     CHAR,
                                 p_ERROR            OUT NUMBER);

   --**********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_OBT_CAM_INV_OPE
   -- Objetivo: Procedimiento que devuelve los campos invalidos que tiene una operacion
   -- que existen entre los tipos de mensajes, tipo de operaciones y  sistema de salida.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_MOTVO_NO_HABLT, PABS_DT_BICLI_ERROR_ALMOT, PABS_DT_CAMPO_ARCHV_ENTRD
   -- PABS_DT_ESTRT_ARCHV_ENTRD
   -- Fecha: 23/09/2016
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_OPE -> Numero de Operacion
   -- p_FEC_ISR_OPE -> Fecha de Operacion
   -- Output:
   -- p_CURSOR  -> Cursor con todao los registros decampos invalidos de la opracion
   -- p_ERROR -> Indicador codigo error
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_PAB_OBT_CAM_INV_OPE (p_NUM_FOL_OPE   IN     NUMBER,
                                     p_FEC_ISR_OPE   IN     DATE,
                                     p_CURSOR           OUT SYS_REFCURSOR,
                                     p_ERROR            OUT NUMBER);

   --
   --***********************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_CONT_EST
   -- Objetivo: Cuenta por numero de operacion y estado de la misma
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA, PABS_DT_CBCRA_NOMNA
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_NMN -> folio de nomina
   -- p_STR_EST_AOS  -> String de estado de la operacion
   -- N/A
   -- Output:
   -- N/A.
   -- Input/Output:
   -- N/A.
   -- Retorno:
   -- PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEINV -> string de salida invalida
   -- PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEVAL -> string de salida valida
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   FUNCTION FN_PAB_CONT_EST (p_NUM_FOL_NMN IN NUMBER, p_STR_EST_AOS IN CHAR)
      RETURN VARCHAR2;


   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_TOT_DVI_NMN_MULTI_HIST
   -- Objetivo: Procedimiento que retorna el total de las operaciones Historicas agrupados por sus monedas
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_CBCRA_NOMNA_HTRCA, PABS_DT_DETLL_NOMNA_HTRCA, PABS_DT_ESTDO_ALMOT
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_NMN -> Numero de la nomina
   -- p_TIP_BUS_HIST -> Tipo de busqueda historica
   -- Output:
   -- p_CURSOR -> SYS_REFCURSOR, resultados de consulta.
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_TOT_DVI_NMN_MULTI_HIST (
      p_NUM_FOL_NMN    IN     NUMBER,
      p_TIP_BUS_HIST   IN     CHAR,
      p_CURSOR            OUT SYS_REFCURSOR,
      p_ERROR             OUT NUMBER
   );

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ACT_ELI_OPE
   -- Objetivo: Procedimiento que actualiza el estado de una operacion
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA, PABS_DT_DETLL_OPRCN, PABS_DT_CBCRA_NOMNA
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_NMN -> foliador del banco
   -- p_FEC_ISR_OPE -> Fecha de Operacion
   -- p_NUM_FOL_OPE -> Numero de operacion
   -- p_COD_USR -> rut usuario
   -- p_GLS_MTV_EST -> motivo o glosa de eliminacion
   -- p_SIS_NOM_OPE -> origen al que pertenece la operacion. NOM = Nomina - OPE = Operacion
   -- Output:
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_ACT_ELI_OPE (p_NUM_FOL_NMN   IN     NUMBER,
                                 p_FEC_ISR_OPE   IN     DATE,
                                 p_NUM_FOL_OPE   IN     NUMBER,
                                 p_COD_USR       IN     VARCHAR2,
                                 p_GLS_MTV_EST   IN     VARCHAR2,
                                 p_SIS_NOM_OPE   IN     CHAR,
                                 p_ERROR            OUT NUMBER);

   --**********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_GES_VAL_HORA_MONDA
   -- Objetivo: Procedimiento que llama a los SP que validan e informan ventana horaria de sistema
   -- y monto maximo importe segun moneda
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: N/A
   -- Fecha: 19/08/16
   -- Autor: Santander
   -- Input:
   -- p_COD_USR -> Codigo de usuario
   -- p_COD_DVI -> Codigo Divisa
   -- p_IMP_INF -> Importe informado para consultar en tabla de moneda
   -- Output:
   -- p_RESP_DET -> Detalle de respuesta para validaciones de horario y sistema
   -- p_RESP_VAL -> Flag cumple con las condiciones de hora y monto importe maximo -> 0 cumple y 1 no cumple
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_GES_VAL_HORA_MONDA (p_COD_USR    IN     CHAR,
                                        p_COD_DVI    IN     CHAR,
                                        p_IMP_INF    IN     NUMBER,
                                        p_RESP_DET      OUT VARCHAR2,
                                        p_RESP_VAL      OUT NUMBER,
                                        p_ERROR         OUT NUMBER);

   --******************************************************************************
   -- //@Santander
   -- Funcion/Procedimiento: Sp_PAB_ACT_CAB_NOMINA_ELI
   -- Objetivo: Procedimiento que actualiza la informacion de la cabecera de la nomina en estado eliminada
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_NMN -> foliador de la nomina
   -- Output:
   -- p_ERROR-> Indicador codigo error
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_ACT_CAB_NOMINA_ELI (p_NUM_FOL_NMN   IN     NUMBER,
                                        p_ERROR            OUT NUMBER);

   --**********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_DEV_NOM_COM
   -- Objetivo: Procedimiento que devuelve datos de cabecera de una nomina y todos sus detalles
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas:
   -- Fecha: 05/12/2016
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_NMN
   -- Output:
   -- p_CURSOR_CBCRA
   -- p_CURSOR_DETLL_OPE
   -- p_ERROR -> Indicador de errores (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_PAB_DEV_NOM_COM (p_NUM_FOL_NMN        IN     NUMBER,
                                 p_CURSOR_CBCRA          OUT SYS_REFCURSOR,
                                 p_CURSOR_DETLL_OPE      OUT SYS_REFCURSOR,
                                 p_ERROR                 OUT NUMBER);

   --

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ACT_CAB_NOMINA
   -- Objetivo: Procedimiento que actualiza la informacion de la cabecera de la nomina
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_NMN -> foliador de la nomina
   -- p_EST_NMN -> Indica el estado de la nomina 0)Valida 1)Invalida
   -- Output:
   -- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_ACT_CAB_NOMINA (p_NUM_FOL_NMN   IN     NUMBER,
                                    p_EST_NMN       IN     NUMBER,
                                    p_ERROR            OUT NUMBER);



   /************************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_BUS_ID_GTR
   -- Objetivo: Funcion que retorna el codigo indentificatorio del gestor documental
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: N/A
   -- Fecha: 05/06/2017
   -- Autor: Santander CAH
   -- Input:
   -- p_NUM_FOL_OPE->   foliador del banco, PK tabla operaciones nomina.
   -- p_FEC_ISR_OPE->   Fecha de insercion de la operacion.
   -- p_TIP_BUS_HIST-> Tipo de busqueda H o N
   -- Output:
   --          Retorna el codigo de gestor documental
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/

   FUNCTION FN_PAB_BUS_ID_GTR (p_NUM_FOL_OPE    IN NUMBER,
                               p_FEC_ISR_OPE    IN DATE,
                               p_TIP_BUS_HIST   IN CHAR)
      RETURN VARCHAR2;

   --***********************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_BUS_EST_NOM
   -- Objetivo: Comprara el estado de una nomina con la que figura en la tabla de nominas.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA
   -- Fecha: 09/11/2016
   -- Autor: Santander
   -- Input:
   -- P_NUM_FOL_NMN -> numero de nomina
   -- P_COD_EST_AOS -> codigo estado nomina
   -- N/A
   -- Output:
   -- N/A.
   -- Input/Output:
   -- N/A.
   -- Retorno:
   -- P_FLAG_EST -> valor de retorno flag de comparacion
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   --***********************************************************************************************
   FUNCTION FN_PAB_BUS_EST_NOM (P_NUM_FOL_NMN   IN NUMBER,
                                P_COD_EST_AOS   IN CHAR)
      RETURN NUMBER;
END PKG_PAB_NOMINA;
--