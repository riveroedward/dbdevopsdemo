CREATE OR REPLACE PACKAGE         PKG_PAB_OPERACION
IS
   /**  Package el cual contrendra todas las funciones y procedimientos relacionados con las operaciones
   -- @Santander */
   --
   v_NOM_PCK     VARCHAR2 (30) := 'PKG_PAB_OPERACION';
   ERR_CODE      NUMBER := 0;
   ERR_MSG       VARCHAR2 (300);
   v_DES         VARCHAR2 (300);
   v_DES_BIT     VARCHAR2 (100);
   p_s_mensaje   VARCHAR2 (400);
   v_BANCO       VARCHAR2 (9) := '970150005';
   v_cero        NUMBER := 0;
   v_dia_ini     DATE := TRUNC (SYSDATE) + INTERVAL '1' MINUTE;
   v_dia_fin DATE
         := TRUNC (SYSDATE) + INTERVAL '23' HOUR + INTERVAL '59' MINUTE ;

   --
   /************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUS_OPE_LIB
   -- Objetivo: PROCEDIMIENTO ALMACENADO QUE ENTREGA LOS TIPOS
   -- DE OPERACION LIBERADAS, SEGUN FILTROS INGRESADOS
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN
   --        , PABS_DT_OPRCN_INFCN_OPCON
   --        , PABS_DT_SISTM_ENTRD_SALID
   --        , PABS_DT_SISTM_ENTRD_SALID
   -- Fecha: 02/06/16
   -- Autor: Santander
   -- Input: p_TIPO_OP -> tipo operacion
   --        p_MT_SWIFT-> codigo Mt Swift
   --        p_COD_DVI-> moneda
   --        p_NOM_BFC-> nombre beneficiario
   --        p_COD_ENT-> canal origen
   --        p_COD_SAL-> canal pago
   --        p_BCO_DTN-> banco
   --        p_MONTO_DESDE-> monto desde
   --        p_MONTO_HASTA-> monto hasta
   --        p_NUM_NOM-> numero nomina
   --        p_NUM_OPE-> numero operacion
   --        p_RUT-> rut beneficiario
   --        p_COD_TIP_SEG -> Codigo de tipo de sistema
   -- Output:
   --        p_CURSOR-> SYS_REFCURSOR, RESULTADOS DE CONSULTA.
   --        p_ERROR-> Devuelve error
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_BUS_OPE_LIB (p_TIPO_OP       IN     CHAR,
                                 p_MT_SWIFT      IN     NUMBER,
                                 p_COD_DVI       IN     CHAR,
                                 p_NOM_BFC       IN     VARCHAR2,
                                 p_COD_ENT       IN     CHAR,
                                 p_COD_SAL       IN     CHAR,
                                 p_BCO_DTN       IN     CHAR,
                                 p_MONTO_DESDE   IN     NUMBER,
                                 p_MONTO_HASTA   IN     NUMBER,
                                 p_NUM_NOM       IN     NUMBER,
                                 p_NUM_OPE       IN     NUMBER,
                                 p_RUT           IN     CHAR,
                                 p_COD_TIP_SEG   IN     NUMBER,
                                 p_CURSOR           OUT SYS_REFCURSOR,
                                 p_ERROR            OUT NUMBER);

   /* ******************************************************************************
   *  IDENTIFICACION PROCEDIMIENTO: Sp_PAB_CAMBIAR_CANAL
   * -----------------------------------------------------------------------------
   * Version :    1.0
   * Descripcion: Procedimiento que cambia el canal de pago a todas las operaciones seleccionadas desde la aplicacion.
   * -----------------------------------------------------------------------------
   * Parametros Entrada:
   *       p_FEC_ISR_OPE:     Fecha de insercion de operacion en el sistema, PK tabla detalle operaciones.
   *       p_NUM_FOL_OPE:     foliador del banco, PK tabla detalle operaciones.
   *       p_COD_SIS_SAL:     Codigo del sistema de salida utilizado en la operacion.
   *       p_COD_MT_SWF:   Codigo de Mensaje Swift.
   *       p_COD_TPO_OPE_AOS:Codigo tipo de operacion utilizado en el sistema de altos montos.
   *       p_COD_SIS_ENT_SAL:Codigo del sistema de salida, correspondiente al cambio de canal.
   *       p_COD_DVI       : Codigo de la divisa.
   *       p_GLS_ERROR     : Glosa con error
   *       p_COD_USR:        rut usuario.
   * Par?metros Salida:
   *        p_GLS_ERROR: mensaje de slida que indica el motivo por el que no se ejecuto el cambio de canal.
   *       p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   * Variables globales:
   *******************************************************************************
   * Control de Modificaciones
   * -------------------------
   * Fecha         Nombre           Descripcion
   * ----------    --------------   ----------------------------------
   * 26/05/16      Santander           Primera Version
   ***************************************************************************** */
   PROCEDURE Sp_PAB_CAMBIAR_CANAL (p_FEC_ISR_OPE       IN     DATE,
                                   p_NUM_FOL_OPE       IN     NUMBER,
                                   p_COD_SIS_SAL       IN     CHAR,
                                   p_COD_MT_SWF        IN     NUMBER,
                                   p_COD_TPO_OPE_AOS   IN     CHAR,
                                   p_COD_SIS_ENT_SAL   IN     CHAR,
                                   p_COD_DVI           IN     CHAR,
                                   p_COD_USR           IN     CHAR,
                                   p_GLS_ERROR            OUT VARCHAR2,
                                   p_ERROR                OUT NUMBER);

   --
   /************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUS_DET_OPE
   -- Objetivo: Procedimiento almacenado que retorna el detalle de una operaci?n seleccionada
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN, PABS_DT_OPRCN_INFCN_OPCON
   --          PABS_DT_ESTDO_ALMOT, PABS_DT_SISTM_ENTRD_SALID
   -- Fecha: 02/06/16
   -- Autor: Santander
   -- Input: p_NUM_FOL_OPE: Foliador del banco, PK tabla detalle operaciones.
   --        p_FEC_ISR_OPE: Fecha de inserci?n de operaci?n en el sistema, PK tabla detalle operaciones.
   -- Output:
   --        p_CURSOR : SYS_REFCURSOR, resultados de consulta.
   --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_BUS_DET_OPE (p_NUM_FOL_OPE   IN     NUMBER,
                                 p_FEC_ISR_OPE   IN     DATE,
                                 p_CURSOR           OUT SYS_REFCURSOR,
                                 p_ERROR            OUT NUMBER);

   --
   /************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ACT_EST_OPERACION
   -- Objetivo: Procedimiento que actualiza el estado de una operacion
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN
   -- Fecha: 02/06/16
   -- Autor: Santander
   -- Input:    p_NUM_FOL_NMN->   foliador del banco
   --           p_FEC_ISR_OPE->   Fecha de insercion de la operacion
   --           p_NUM_REF_SWF->   Numero referencia Swift
   --           p_COD_EST_AOS->   Identificador de nuevo estado.
   --           p_COD_USR->       rut usuario
   -- Output:
   --           p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_ACT_EST_OPERACION (p_NUM_FOL_OPE   IN     NUMBER,
                                       p_FEC_ISR_OPE   IN     DATE,
                                       p_NUM_REF_SWF   IN     CHAR,
                                       p_COD_EST_AOS   IN     CHAR,
                                       p_COD_USR       IN     VARCHAR2,
                                       p_ERROR            OUT NUMBER);

   /************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ACT_EST_OPERACION_BTC
   -- Objetivo: Procedimiento que actualiza el estado de una operacion y permite recibir la bitacora que se desea agregar
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN
   -- Fecha: 02/06/16
   -- Autor: Santander
   -- Input:    p_NUM_FOL_NMN->   foliador del banco
   --           p_FEC_ISR_OPE->   Fecha de insercion de la operacion
   --           p_NUM_REF_SWF->   Numero referencia Swift
   --           p_COD_EST_AOS->   Identificador de nuevo estado.
   --           p_COD_USR->       rut usuario
   --           p_DSC_GLB_BTC->   Bitacora de la operación
   -- Output:
   --           p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_ACT_EST_OPERACION_BTC (p_NUM_FOL_OPE   IN     NUMBER,
                                           p_FEC_ISR_OPE   IN     DATE,
                                           p_NUM_REF_SWF   IN     CHAR,
                                           p_COD_EST_AOS   IN     CHAR,
                                           p_COD_USR       IN     VARCHAR2,
                                           p_DSC_GLB_BTC   IN     VARCHAR2,
                                           p_ERROR            OUT NUMBER);

   --
   /************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ACT_DATOS_OPE
   -- Objetivo: Procedimiento que actualiza informacion de una operacion
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN, PABS_DT_OPRCN_INFCN_OPCON
   -- Fecha: 02/06/16
   -- Autor: Santander
   -- Input:    p_NUM_FOL_NMN->     foliador del banco
   --           p_FEC_ISR_OPE->     Fecha de insercion de la operacion
   --           p_NUM_REF_SWF->     Numero de referencia del swift
   --           p_FEC_ENv_RCp_SWF-> Fecha Swift
   --           p_HOR_ENv_RCp_SWF-> Hora Swift
   --           p_GLS_ADC_EST->     Glosa Adicional Estado
   --           p_GLS_EST_RCH->     Glosa Estado Rechazado
   --           p_GLS_MTv_VSD->     Glosa Motivo Visacion
   --           p_COD_USR->         rut usuario
   -- Output:
   --          p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_ACT_DATOS_OPE (p_NUM_FOL_OPE       IN     NUMBER,
                                   p_FEC_ISR_OPE       IN     DATE,
                                   p_NUM_REF_SWF       IN     VARCHAR2,
                                   p_FEC_ENv_RCp_SWF   IN     DATE,
                                   p_HOR_ENv_RCp_SWF   IN     NUMBER,
                                   p_GLS_ADC_EST       IN     VARCHAR2,
                                   p_GLS_EST_RCH       IN     VARCHAR2,
                                   p_GLS_MTV_VSD       IN     VARCHAR2,
                                   p_COD_USR           IN     VARCHAR2,
                                   p_ERROR                OUT NUMBER);

   --
   /************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_TRAS_NOM_OPE
   -- Objetivo: Procedimiento Transfiere la informacion de las nominas a las operaciones
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_BANCO_ALMOT
   -- Fecha: 02/06/16
   -- Autor: Santander
   -- Input: p_NUM_FOL_OPE: Numero de Folio de una Operacion
   --        p_FEC_ISR_OPE: Fecha de una Operacion
   --        p_COD_USR: Codigo de Usuario.
   --        p_COD_BCO_DTN: Bic Banco Destino
   --        p_COD_MT_SWF : Codigo MT Swift
   --        p_COD_TPO_OPE_AOS: Codigo Tipo operación AOS
   --        p_COD_DVI: Codigo DVI
   -- Output:
   --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_TRAS_NOM_OPE (p_NUM_FOL_OPE       IN     NUMBER,
                                  p_FEC_ISR_OPE       IN     DATE,
                                  p_COD_USR           IN     VARCHAR2,
                                  p_COD_BCO_DTN       IN     CHAR,
                                  p_COD_MT_SWF        IN     NUMBER,
                                  p_COD_TPO_OPE_AOS   IN     CHAR,
                                  p_COD_DVI           IN     CHAR,
                                  p_ERROR                OUT NUMBER);

   --
   /************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_INS_OPE
   -- Objetivo: Procedimiento que inserta la informacion que se encuentra en la nomina
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN
   -- Fecha: 02/06/16
   -- Autor: Santander
   -- Input:
   --            p_FEC_ISR_OPE     -> Fecha de insercion de la operación
   --            p_NUM_FOL_OPE     -> Numero Foliador de la operacion
   --            p_NUM_FOL_NMN     -> Numero Foliador de la nomina
   --            p_NUM_FOL_OPE_ORG -> Numero que tiene el numero original de la operacion relacionada
   --            p_NUM_OPE_SIS_ENT -> Numero de operacion de la operacion en el sistema origen
   --            p_FLG_EGR_ING     -> Flag de egreso e ingreso
   --            p_COD_SIS_SAL     -> Codigo de sistema de salida
   --            p_COD_SIS_ENT     -> Codigo de sistema de entrada
   --            p_COD_EST_OPE     -> Estado de la operación
   --            p_COD_TPO_ING     -> Tipo de ingreso
   --            p_COD_MT_SWF      -> Numero del tipo de mensaje swift
   --            p_FLG_MRD_UTZ_SWF -> Flasg de mercado
   --            p_COD_TPO_OPE_AOS -> Tipo de operacion.
   --            p_FEC_VTA         -> Fecha de vencimiento
   --            p_COD_DVI         -> Codigo de la divisa
   --            p_IMp_OPE         -> Importe de la operacion
   --            p_COD_BCO_DTN     -> BIC del banco destino
   --            p_COD_BCO_ORG     -> BIC Banco Origen
   --            p_FEC_ENV_RCP_SWF -> Fecha de envio / recepcion swfit
   --            p_HOR_ENV_RCP_SWF -> Hora de envio / recepcion swfit
   --            p_NUM_REF_SWF     -> Numero de referencia Swift
   --            p_NUM_REF_EXT     -> Numero de referencia swift externa
   --            p_NUM_DOC_BFC     -> Numero rut del beneficiario
   --            p_COD_TPD_BFC     -> Rut tipo de beneficiario
   --            p_NOM_BFC         -> Nombre del Beneficiario
   --            p_NUM_CTA_BFC     -> Numero de cuenta del beneficiario
   --            p_COD_SUC         -> Codigo de la sucursal
   --            p_GLS_ADC_EST     -> Informacion adicional
   --            p_NUM_REF_CTB     -> Numero de referencia contable
   --            p_COD_BCO_BFC     -> BIB Banco Beneficario
   --            p_COD_BCO_ITD     -> BIC Banco Intermediario
   --            p_NUM_CTA_DCv_BFC -> Numero cuenta DCV Beneficiario
   --            p_GLS_DIR_BFC     -> Direccion Beneficario
   --            p_NOM_CDD_BFC     -> Nombre ciudad beneficiario
   --            p_COD_PAS_BFC     -> Codigo pais beneficiario
   --            p_NOM_ODN         -> Nombre ordenante
   --            p_NUM_DOC_ODN     -> Numero rut ordenante
   --            p_COD_TPD_ODN     -> Tipo documento de ordenante
   --            p_NUM_CTA_ODN     -> Numero cuenta ordenante
   --            p_NUM_CTA_DCv_ODN -> Numero cuenta DCV ordenante
   --            p_GLS_DIR_ODN     -> Direccion ordenante
   --            p_NOM_CDD_ODN     -> Nombre ciudad ordenante
   --            p_COD_PAS_ODN     -> Codigo pais ordenante
   --            p_NUM_CLv_NGC     -> Numero de clave de negocio
   --            p_NUM_AGT         -> Numero de agente
   --            p_COD_FND_CCLV    -> Codigo Fondo (Producto CCLV)
   --            p_COD_TPO_SDO     -> Tipo de saldo
   --            p_COD_TPO_CMA     -> Tipo de camara
   --            p_COD_TPO_FND     -> Tipo de fondo
   --            p_FEC_TPO_OPE_CCLV-> Fecha operacion CCLV
   --            p_NUM_CLv_IDF     -> Numero clave identificatorio
   --            p_OBS_OPC_SWF   -> Observacion Opcional Swift
   --            p_NUM_IDF_GTR_DOC -> Identificador gestor documental
   --            p_NUM_MVT         -> Numero de Movimiento (Vigente, Cuenta Corriente)
   --            p_COD_USR     -> rut usuario
   -- Output:
   --          p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_INS_OPE (p_FEC_ISR_OPE        IN     DATE,
                             p_NUM_FOL_OPE        IN     NUMBER,
                             p_NUM_FOL_NMN        IN     NUMBER,
                             p_NUM_FOL_OPE_ORG    IN     NUMBER,
                             p_NUM_OPE_SIS_ENT    IN     CHAR,
                             p_FLG_EGR_ING        IN     NUMBER,
                             p_COD_SIS_SAL        IN     CHAR,
                             p_COD_SIS_ENT        IN     CHAR,
                             p_COD_EST_OPE        IN     NUMBER,
                             p_COD_TPO_ING        IN     NUMBER,
                             p_COD_MT_SWF         IN     NUMBER,
                             p_FLG_MRD_UTZ_SWF    IN     NUMBER,
                             p_COD_TPO_OPE_AOS    IN     CHAR,
                             p_FEC_VTA            IN     DATE,
                             p_COD_DVI            IN     CHAR,
                             p_IMp_OPE            IN     NUMBER,
                             p_COD_BCO_DTN        IN     CHAR,
                             p_COD_BCO_ORG        IN     CHAR,
                             p_FEC_ENV_RCP_SWF    IN     DATE,
                             p_HOR_ENV_RCP_SWF    IN     NUMBER,
                             p_NUM_REF_SWF        IN     CHAR,
                             p_NUM_REF_EXT        IN     CHAR,
                             ------------Beneficiario------------
                             p_NUM_DOC_BFC        IN     CHAR,
                             p_COD_TPD_BFC        IN     CHAR,
                             p_NOM_BFC            IN     VARCHAR2,
                             p_NUM_CTA_BFC        IN     VARCHAR2,
                             -------------------------------------
                             p_COD_SUC            IN     VARCHAR2,
                             p_GLS_ADC_EST        IN     VARCHAR2,
                             p_NUM_REF_CTB        IN     CHAR,
                             ----------Opcionales--------------
                             p_COD_BCO_BFC        IN     CHAR,
                             p_COD_BCO_ITD        IN     CHAR,
                             p_NUM_CTA_DCv_BFC    IN     VARCHAR2,
                             p_GLS_DIR_BFC        IN     VARCHAR2,
                             p_NOM_CDD_BFC        IN     VARCHAR2,
                             p_COD_PAS_BFC        IN     CHAR,
                             p_NOM_ODN            IN     VARCHAR2,
                             p_NUM_DOC_ODN        IN     CHAR,
                             p_COD_TPD_ODN        IN     CHAR,
                             p_NUM_CTA_ODN        IN     VARCHAR2,
                             p_NUM_CTA_DCv_ODN    IN     VARCHAR2,
                             p_GLS_DIR_ODN        IN     VARCHAR2,
                             p_NOM_CDD_ODN        IN     VARCHAR2,
                             p_COD_PAS_ODN        IN     CHAR,
                             p_NUM_CLv_NGC        IN     VARCHAR2,
                             p_NUM_AGT            IN     CHAR,
                             p_COD_FND_CCLV       IN     CHAR,
                             p_COD_TPO_SDO        IN     CHAR,
                             p_COD_TPO_CMA        IN     CHAR,
                             p_COD_TPO_FND        IN     VARCHAR2,
                             p_FEC_TPO_OPE_CCLV   IN     DATE,
                             p_NUM_CLv_IDF        IN     VARCHAR2,
                             p_OBS_OPC_SWF        IN     VARCHAR2,
                             p_NUM_IDF_GTR_DOC    IN     VARCHAR2,
                             p_NUM_MVT            IN     CHAR,
                             --------------------------------
                             p_COD_USR            IN     CHAR,
                             p_ERROR                 OUT NUMBER);

   /************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_INS_OPE_OPC
   -- Objetivo: Procedimiento que inserta la informacion que se encuentra en la nomina
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_OPRCN_INFCN_OPCON
   -- Fecha: 02/06/16
   -- Autor: Santander
   -- Input:     p_FEC_ISR_OPE     -> Fecha Insercion Operacion
   --            p_NUM_FOL_OPE     -> Numero Foliador de la operacion
   --            p_NUM_DOC_BFC     -> Numero rut del beneficiario
   --            p_COD_TPD_BFC     -> Codigo tipo documento beneficiario.
   --            p_COD_BCO_BFC     -> BIB Banco Beneficario
   --            p_COD_BCO_ITD     -> BIC Banco Intermediario
   --            p_NUM_CTA_DCv_BFC -> Numero cuenta DCV Beneficiario
   --            p_GLS_DIR_BFC     -> Direccion Beneficario
   --            p_NOM_CDD_BFC     -> Nombre ciudad beneficiario
   --            p_COD_PAS_BFC     -> Codigo pais beneficiario
   --            p_NOM_ODN         -> Nombre ordenante
   --            p_NUM_DOC_ODN     -> Numero rut ordenante
   --            p_NUM_CTA_ODN     -> Numero cuenta ordenante
   --            p_NUM_CTA_DCv_ODN -> Numero cuenta DCV ordenante
   --            p_GLS_DIR_ODN    -> Direccion ordenante
   --            p_NOM_CDD_ODN    -> Nombre ciudad ordenante
   --            p_COD_PAS_ODN    -> Codigo pais ordenante
   --            p_NUM_CLv_NGC    -> Numero de clave de negocio
   --            p_NUM_AGT        -> Numero de agente
   --            p_COD_FND_CCLV   -> Codigo Fondo (Producto CCLV)
   --            p_COD_TPO_SDO    -> Tipo de saldo
   --            p_COD_TPO_CMA    -> Tipo de camara
   --            p_COD_TPO_FND     -> Tipo de fondo
   --            p_FEC_TPO_OPE_CCLV -> Fecha operacion CCLV
   --            p_NUM_CLv_IDF     -> Numero clave identificatorio
   --            p_OBS_OPC_SWF   -> Observacion Opcional Swift
   --            p_COD_USR     -> rut usuario
   -- Output:
   --          p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_INS_OPE_OPC (p_FEC_ISR_OPE        IN     DATE,
                                 p_NUM_FOL_OPE        IN     NUMBER,
                                 ------------Beneficiario------------
                                 p_NUM_DOC_BFC        IN     CHAR,
                                 p_COD_TPD_BFC        IN     CHAR,
                                 ------------Opcionales--------------
                                 p_COD_BCO_BFC        IN     CHAR,
                                 p_COD_BCO_ITD        IN     CHAR,
                                 p_NUM_CTA_DCv_BFC    IN     VARCHAR2,
                                 p_GLS_DIR_BFC        IN     VARCHAR2,
                                 p_NOM_CDD_BFC        IN     VARCHAR2,
                                 p_COD_PAS_BFC        IN     CHAR,
                                 p_NOM_ODN            IN     VARCHAR2,
                                 p_NUM_DOC_ODN        IN     CHAR,
                                 p_COD_TPD_ODN        IN     CHAR,
                                 p_NUM_CTA_ODN        IN     VARCHAR2,
                                 p_NUM_CTA_DCv_ODN    IN     VARCHAR2,
                                 p_GLS_DIR_ODN        IN     VARCHAR2,
                                 p_NOM_CDD_ODN        IN     VARCHAR2,
                                 p_COD_PAS_ODN        IN     CHAR,
                                 p_NUM_CLv_NGC        IN     VARCHAR2,
                                 p_NUM_AGT            IN     CHAR,
                                 p_COD_FND_CCLV       IN     CHAR,
                                 p_COD_TPO_SDO        IN     CHAR,
                                 p_COD_TPO_CMA        IN     CHAR,
                                 p_COD_TPO_FND        IN     VARCHAR2,
                                 p_FEC_TPO_OPE_CCLV   IN     DATE,
                                 p_NUM_CLv_IDF        IN     VARCHAR2,
                                 p_OBS_OPC_SWF        IN     VARCHAR2,
                                 --------------------------------
                                 p_COD_USR            IN     CHAR,
                                 p_ERROR                 OUT NUMBER);

   --
   /************************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_GEN_REF_SWF
   -- Objetivo: Fusion que retorne el codifo de referencia Swift
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: N/A
   -- Fecha: 02/06/16
   -- Autor: Santander
   -- Input:      p_COD_SIS_SAL:      Sistema de salida de pago
   --             p_COD_TPO_OPE_AOS:  Sistema de operaciones en altos montos
   --             p_NUM_FOL_OPE       Numero de Folio Operacion
   --             p_COD_MT_SWF        Codigo de MT Swift
   -- Output:
   --          Retorna el codigo de referencia Swift
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   FUNCTION FN_PAB_GEN_REF_SWF (p_COD_SIS_SAL       IN CHAR,
                                p_COD_TPO_OPE_AOS   IN CHAR,
                                p_NUM_FOL_OPE       IN NUMBER,
                                p_COD_MT_SWF        IN NUMBER)
      RETURN VARCHAR2;

   --
   /************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_OBT_EST_ACT_OPE
   -- Objetivo: Procedimiento que obtiene el estado actual de la operacion
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN
   -- Fecha: 02/06/16
   -- Autor: Santander
   -- Input:    p_NUM_FOL_OPE: Numero de Folio de una Operacion
   --           p_FEC_ISR_OPE: Fecha de una Operacion
   -- Output:
   --           p_COD_EST_AOS: Estado actual de la operacion
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_PAB_OBT_EST_ACT_OPE (p_NUM_FOL_OPE   IN     NUMBER,
                                     p_FEC_ISR_OPE   IN     DATE,
                                     p_COD_EST_AOS      OUT NUMBER);

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_BUS_TOT_DET_OPE
   -- Objetivo: Procedimiento almacenado que retorna el detalle de una operacion seleccionada
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_OPE->   foliador del banco, PK tabla operaciones operacion.
   -- p_FEC_ISR_OPE->   Fecha de insercion de operacion en el sistema, PK tabla operaciones nomina.
   -- Output:
   -- p_CURSOR -> Informacion de la operacion
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_BUS_TOT_DET_OPE (p_NUM_FOL_OPE   IN     NUMBER,
                                     p_FEC_ISR_OPE   IN     DATE,
                                     p_CURSOR           OUT SYS_REFCURSOR,
                                     p_ERROR            OUT NUMBER);

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_CON_OPE_HIST
   -- Objetivo: Procedimiento almacenado que retorna las operaciones de una nomina
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN, PABS_DT_OPRCN_INFCN_OPCON, PABS_DT_DETLL_OPRCN_HTRCA, PABS_DT_OPRCN_OPCON_HTRCA, PABS_DT_ESTDO_ALMOT
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_NMN -> Numero de la nomina
   -- p_NUM_FOL_OPE -> Numero de folio operacion
   -- p_FEC_DESDE ->  fecha desde
   -- p_FEC_HASTA ->  fecha hasta
   -- p_COD_TPO_OPE_AOS ->  codigo tipo operacion estado
   -- p_COD_EST_AOS ->  codigo estado
   -- p_COD_SIS_ENT ->  codigo sistema entrada
   -- p_COD_SIS_SAL ->  codigo de sistema salida
   -- p_COD_BCO_DTN ->  codigo banco destino
   -- p_COD_BCO_ORG -> Codigo Bic banco origen
   -- p_COD_MT_SWF ->  codigo MT swf
   -- p_FLG_EGR_ING -> Flasg egreso ingreso
   -- p_IMP_OPE_DESDE ->  importe operacion desde
   -- p_IMP_OPE_HASTA ->  importe operacion hasta
   -- p_COD_DVI -> codigo de divisa
   -- p_NUM_REF_SWF ->  numero referencia swf
   -- Output:
   -- p_CURSOR -> Cursor con las nominas encontradas
   -- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_CON_OPE_HIST (p_NUM_FOL_NMN       IN     NUMBER,
                                  p_NUM_FOL_OPE       IN     NUMBER,
                                  p_FEC_DESDE         IN     DATE,
                                  p_FEC_HASTA         IN     DATE,
                                  p_COD_TPO_OPE_AOS   IN     CHAR,
                                  p_COD_EST_AOS       IN     NUMBER,
                                  p_COD_SIS_ENT       IN     CHAR,
                                  p_COD_SIS_SAL       IN     CHAR          ---
                                                                 ,
                                  p_COD_BCO_DTN       IN     CHAR,
                                  p_COD_BCO_ORG       IN     CHAR          ---
                                                                 ,
                                  p_COD_MT_SWF        IN     NUMBER,
                                  p_FLG_EGR_ING       IN     NUMBER        ---
                                                                   ,
                                  p_IMP_OPE_DESDE     IN     NUMBER,
                                  p_IMP_OPE_HASTA     IN     NUMBER,
                                  p_COD_DVI           IN     CHAR,
                                  p_NUM_REF_SWF       IN     CHAR,
                                  p_CURSOR               OUT SYS_REFCURSOR,
                                  p_ERROR                OUT NUMBER);

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUS_DET_OPE_CON
   -- Objetivo: Procedimiento almacenado que retorna el detalle de una operacion historica seleccionada
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN, PABS_DT_OPRCN_INFCN_OPCON, PABS_DT_ESTDO_ALMOT, PABS_DT_SISTM_ENTRD_SALID, PABS_DT_DETLL_OPRCN_HTRCA, PABS_DT_OPRCN_OPCON_HTRCA
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_OPE->   foliador del banco, PK tabla operaciones nomina.
   -- p_FEC_ISR_OPE->   Fecha de insercion de la operacion.
   -- p_TIP_BUS_HIST-> tipo busqueda de historificacion
   -- Output:
   -- p_CURSOR -> Informacion de la operacion
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_BUS_DET_OPE_CON (p_NUM_FOL_OPE    IN     NUMBER,
                                     p_FEC_ISR_OPE    IN     DATE,
                                     p_TIP_BUS_HIST   IN     CHAR,
                                     p_CURSOR            OUT SYS_REFCURSOR,
                                     p_ERROR             OUT NUMBER);

   /************************************************************************************************

   -- Funcion/Procedimiento: Sp_PAB_BUS_DET_OPE_REF
   -- Objetivo: Procedimiento almacenado que retorna el detalle de una operaci?n seleccionada
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN, PABS_DT_OPRCN_INFCN_OPCON
   --          PABS_DT_ESTDO_ALMOT, PABS_DT_SISTM_ENTRD_SALID
   -- Fecha: 02/06/16
   -- Autor: Santander
   -- Input:   p_NUM_FOL_OPE : Numero de folio de la operación
   --          p_NUM_REF_SWF: Numero de referencia de una operacion swfit
   --          p_FLG_EGR_ING: Flag de egreso o ingreso
   -- Output:
   --        p_CURSOR : SYS_REFCURSOR, resultados de consulta.
   --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_BUS_DET_OPE_REF (p_NUM_FOL_OPE   IN     NUMBER,
                                     p_NUM_REF_SWF   IN     CHAR,
                                     p_FLG_EGR_ING   IN     NUMBER,
                                     p_CURSOR           OUT SYS_REFCURSOR,
                                     p_ERROR            OUT NUMBER);

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_GES_VIS_OPE
   -- Objetivo: Procedimiento que recorre listado de operaciones a visar
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: N/A
   -- Fecha: 21/09/16
   -- Autor: Santander
   -- Input:
   -- p_REG_OPER -> parametro tipo registro que contiene el numero, fecha operacion y n?mina
   -- p_COD_USR -> rut usuario
   -- p_GLS_MTV_EST -> motivo o glosa de eliminacion
   -- Output:
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_GES_VIS_OPE (p_REG_OPER      IN     REG_OPER,
                                 p_COD_USR       IN     VARCHAR2,
                                 p_GLS_MTV_EST   IN     VARCHAR2,
                                 p_ERROR            OUT NUMBER);

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ACT_VIS_OPE
   -- Objetivo: Procedimiento que actualiza el estado de una operacion desde Por Visar a Autorizada
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA, PABS_DT_DETLL_OPRCN, PABS_DT_OPRCN_INFCN_OPCON, PABS_DT_NOMNA_INFCN_OPCON
   -- Fecha: 01/10/16
   -- Autor: Santander
   -- Input:
   -- p_FEC_ISR_OPE -> Fecha de ingreso operacion
   -- p_NUM_FOL_OPE -> Numero de operacion
   -- p_NUM_FOL_NMN -> foliador del banco
   -- p_COD_USR -> rut usuario
   -- p_GLS_MTV_EST -> motivo o glosa de visacion
   -- Output:
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ************************************************************************************************
   PROCEDURE Sp_PAB_ACT_VIS_OPE (p_FEC_ISR_OPE   IN     DATE,
                                 p_NUM_FOL_OPE   IN     NUMBER,
                                 p_NUM_FOL_NMN   IN     NUMBER,
                                 p_COD_USR       IN     VARCHAR2,
                                 p_GLS_MTV_EST   IN     VARCHAR2,
                                 p_ERROR            OUT NUMBER);

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_CON_OPE
   -- Objetivo: Procedimiento que consulta y lista las operaciones que se deben visar
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA, PABS_DT_DETLL_OPRCN
   -- Fecha: 27/10/16
   -- Autor: Santander
   -- Input:
   -- p_COD_EST_AOS -> Estado de la operaci?n Por Visar
   -- p_COD_SIS_SAL -> Codigo del sistema de salida
   -- p_FECHA_DES -> Fecha ingreso operacion para filtro desde
   -- p_FECHA_HAS -> Fecha ingreso operacion para filtro hasta
   -- Output:
   -- p_CURSOR -> Cursor con las nominas encontradas
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_CON_OPE (p_COD_EST_AOS   IN     CHAR,
                             p_COD_SIS_SAL   IN     CHAR,
                             p_FECHA_DES     IN     DATE,
                             p_FECHA_HAS     IN     DATE,
                             p_CURSOR           OUT SYS_REFCURSOR,
                             p_ERROR            OUT NUMBER);

   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ACT_EST_GLS_OPE
   -- Objetivo: Procedimiento que actualiza el estado y glosa de una operacion al ser liberada por contingencia.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN
   -- Fecha: 02/06/16
   -- Autor: Santander
   -- Input:    p_NUM_FOL_NMN->   foliador del banco
   --           p_FEC_ISR_OPE->   Fecha de insercion de la operacion
   --           p_COD_EST_AOS->   Identificador de nuevo estado.
   --           p_GLS_ADC_EST->   Glosa Cambio estado
   --           p_COD_USR->       rut usuario
   -- Output:
   --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_ACT_EST_GLS_OPE (p_NUM_FOL_OPE   IN     NUMBER,
                                     p_FEC_ISR_OPE   IN     DATE,
                                     p_COD_EST_AOS   IN     CHAR,
                                     p_GLS_ADC_EST   IN     VARCHAR2,
                                     p_COD_USR       IN     VARCHAR2,
                                     p_ERROR            OUT NUMBER);

   /************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_GES_INS_OPE
   -- Objetivo: Procedimiento que inserta una operacion en tabla operacion
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN
   -- Fecha: 02/06/16
   -- Autor: Santander
   -- Input:     p_FEC_ISR_OPE     -> Fecha Insercion (se puede enviar nulo y lo genera el sistema)
   --            p_NUM_FOL_OPE     -> Numero Foliador de la operacion
   --            p_NUM_FOL_NMN     -> Numero Foliador de la nomina
   --            p_NUM_FOL_OPE_ORG -> Numero que tiene el numero original de la operacion relacionada
   --            p_NUM_OPE_SIS_ENT -> Numero de operacion de la operacion en el sistema origen
   --            p_FLG_EGR_ING     -> Flag que indica si es egreso (0) o ingreso (1)
   --            p_COD_SIS_SAL     -> Sistema de salida
   --            p_COD_SIS_ENT     -> Sistema de entrada
   --            P_COD_TPO_ING     -> Tipo Ingreso
   --            p_EST_OPE         -> Estado de la operacion (PAS (Por Asociar))
   --            p_COD_MT_SWF      -> Numero del tipo de mensaje swift
   --            p_COD_TPO_OPE_AOS -> Tipo de operacion.
   --            p_FEC_VTA         -> Fecha de vencimiento
   --            p_COD_DVI         -> Codigo de la divisa
   --            p_IMp_OPE         -> Importe de la operacion
   --            p_COD_BCO_DTN     -> BIC del banco destino
   --            p_COD_BCO_ORG     -> BIC Banco Origen
   --            p_FEC_ENV_RCP_SWF -> Fecha Envio/Recepcion Swift
   --            p_HOR_ENV_RCP_SWF -> Hora Envio/Recepcion Swift
   --            p_NUM_REF_SWF     -> Referencia Swfit
   --            p_NUM_REF_EXT     -> Referencia Swift Relacionada
   --            p_NUM_DOC_BFC     -> Numero rut del beneficiario
   --            p_COD_TPD_BFC     -> Digito verificador del Beneficiario
   --            p_NOM_BFC         -> Nombre del Beneficiario
   --            p_NUM_CTA_BFC     -> Numero de cuenta del beneficiario
   --            p_COD_SUC         -> Codigo de la sucursal
   --            p_GLS_ADC_EST     -> Informacion adicional
   --            p_NUM_REF_CTB     -> Numero de referencia contable
   --            p_COD_BCO_BFC     -> BIB Banco Beneficario
   --            p_COD_BCO_ITD     -> BIC Banco Intermediario
   --            p_NUM_CTA_DCv_BFC -> Numero cuenta DCV Beneficiario
   --            p_GLS_DIR_BFC     -> Direccion Beneficario
   --            p_NOM_CDD_BFC     -> Nombre ciudad beneficiario
   --            p_COD_PAS_BFC     -> Codigo pais beneficiario
   --            p_NOM_ODN         -> Nombre ordenante
   --            p_NUM_DOC_ODN     -> Numero rut ordenante
   --            p_COD_TPD_ODN     -> codigo tipo documento ordenante
   --            p_NUM_CTA_ODN     -> Numero cuenta ordenante
   --            p_NUM_CTA_DCv_ODN -> Numero cuenta DCV ordenante
   --            p_GLS_DIR_ODN     -> Direccion ordenante
   --            p_NOM_CDD_ODN     -> Nombre ciudad ordenante
   --            p_COD_PAS_ODN     -> Codigo pais ordenante
   --            p_NUM_CLv_NGC     -> Numero de clave de negocio
   --            p_NUM_AGT         -> Numero de agente
   --            p_COD_FND_CCLV    -> Codigo Fondo (Producto CCLV)
   --            p_COD_TPO_SDO     -> Tipo de saldo
   --            p_COD_TPO_CMA     -> Tipo de camara
   --            p_COD_TPO_FND     -> Tipo de fondo
   --            p_FEC_TPO_OPE_CCLV-> Fecha operacion CCLV
   --            p_NUM_CLv_IDF     -> Numero clave identificatorio
   --            p_OBS_OPC_SWF   -> Observacion Opcional Swift
   --            p_NUM_MVT         -> Numero de Movimiento (Vigente, Cuenta Corriente)
   --            p_COD_USR     -> rut usuario
   -- Output:
   --          p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_GES_INS_OPE (p_FEC_ISR_OPE        IN     DATE,
                                 p_NUM_FOL_OPE        IN     NUMBER,
                                 p_NUM_FOL_NMN        IN     NUMBER,
                                 p_NUM_FOL_OPE_ORG    IN     NUMBER,
                                 p_NUM_OPE_SIS_ENT    IN     CHAR,
                                 p_FLG_EGR_ING        IN     NUMBER,
                                 p_COD_SIS_SAL        IN     CHAR,
                                 p_COD_SIS_ENT        IN     CHAR,
                                 P_COD_TPO_ING        IN     NUMBER,
                                 p_EST_OPE            IN     CHAR,
                                 p_COD_MT_SWF         IN     NUMBER,
                                 p_COD_TPO_OPE_AOS    IN     CHAR,
                                 p_FEC_VTA            IN     DATE,
                                 p_COD_DVI            IN     CHAR,
                                 p_IMp_OPE            IN     NUMBER,
                                 p_COD_BCO_DTN        IN     CHAR,
                                 p_COD_BCO_ORG        IN     CHAR,
                                 p_FEC_ENV_RCP_SWF    IN     DATE,
                                 p_HOR_ENV_RCP_SWF    IN     NUMBER,
                                 p_NUM_REF_SWF        IN     CHAR,
                                 p_NUM_REF_EXT        IN     CHAR,
                                 ------------Beneficiario------------
                                 p_NUM_DOC_BFC        IN     CHAR,
                                 p_COD_TPD_BFC        IN     CHAR,
                                 p_NOM_BFC            IN     VARCHAR2,
                                 p_NUM_CTA_BFC        IN     VARCHAR2,
                                 -------------------------------------
                                 p_COD_SUC            IN     VARCHAR2,
                                 p_GLS_ADC_EST        IN     VARCHAR2,
                                 p_NUM_REF_CTB        IN     CHAR,
                                 ----------Opcionales--------------
                                 p_COD_BCO_BFC        IN     CHAR,
                                 p_COD_BCO_ITD        IN     CHAR,
                                 p_NUM_CTA_DCv_BFC    IN     VARCHAR2,
                                 p_GLS_DIR_BFC        IN     VARCHAR2,
                                 p_NOM_CDD_BFC        IN     VARCHAR2,
                                 p_COD_PAS_BFC        IN     CHAR,
                                 p_NOM_ODN            IN     VARCHAR2,
                                 p_NUM_DOC_ODN        IN     CHAR,
                                 p_COD_TPD_ODN        IN     CHAR,
                                 p_NUM_CTA_ODN        IN     VARCHAR2,
                                 p_NUM_CTA_DCv_ODN    IN     VARCHAR2,
                                 p_GLS_DIR_ODN        IN     VARCHAR2,
                                 p_NOM_CDD_ODN        IN     VARCHAR2,
                                 p_COD_PAS_ODN        IN     CHAR,
                                 p_NUM_CLv_NGC        IN     VARCHAR2,
                                 p_NUM_AGT            IN     CHAR,
                                 p_COD_FND_CCLV       IN     CHAR,
                                 p_COD_TPO_SDO        IN     CHAR,
                                 p_COD_TPO_CMA        IN     CHAR,
                                 p_COD_TPO_FND        IN     VARCHAR2,
                                 p_FEC_TPO_OPE_CCLV   IN     DATE,
                                 p_NUM_CLv_IDF        IN     VARCHAR2,
                                 p_OBS_OPC_SWF        IN     VARCHAR2,
                                 p_NUM_MVT            IN     CHAR,
                                 --------------------------------
                                 p_COD_USR            IN     CHAR,
                                 p_ERROR                 OUT NUMBER);

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUS_PAG_RECIB
   -- Objetivo: Busca las operaciones que no fue posible realizar un abono automatico.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA
   -- Fecha: 24/10/16
   -- Autor: Santander
   -- Input:
   --      p_COD_MT_SWF -> Codigo de MT Swift
   --      p_MNTO_DES -> Monto operacion desde
   --      p_MNTO_HAS -> Monto operacion hasta
   --      p_COD_DVI-> Codigo de moneda
   --      p_NUM_REF_SWF -> Numero Referencia SWIFT
   --      p_COD_BCO -> Codigo de banco
   -- Output:  p_CURSOR -> Cursor que tiene los pagos recibidos
   --          p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   --
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_BUS_PAG_RECIB (p_COD_MT_SWF    IN     NUMBER,
                                   p_MNTO_DES      IN     NUMBER,
                                   p_MNTO_HAS      IN     NUMBER,
                                   p_COD_DVI       IN     CHAR,
                                   p_NUM_REF_SWF   IN     CHAR,
                                   p_COD_BCO       IN     CHAR,
                                   p_CURSOR           OUT SYS_REFCURSOR,
                                   p_ERROR            OUT NUMBER);

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_GES_ABO_PAG_RCB
   -- Objetivo: Procedimiento actualiza estado de la operacion Por pago recibido
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: N/A
   -- Fecha: 04/05/17
   -- Autor: Santander-CAH
   -- Input:
   --      p_NUM_FOL_OPE        -> Numero de Operacion Origen
   --      p_FEC_ISR_OPE        -> Fecha de insercion de operacion origen
   --      p_SIS_ENT_ORI        -> Nombre Canal de Entrada origen
   --      p_NUM_REF_SWF        -> Numero de referencia Swift
   --      p_TIP_ABO_MAN        -> Codigo de tipo Abono
   --      p_ARE_DTN            -> Area destino del Abono, si p_TIP_ABO_MAN es Puente Contable
   --      p_NUM_CTA              -> Numero de cuenta
   --      p_NUM_DEV_TRANS      -> Numero devuelto por WS de Vigencia Salida
   --      p_COD_USR            -> Codigo de Usuario que se registrara en bitacora
   -- Output:
   --      p_ERROR              -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_GES_ABO_PAG_RCB (p_NUM_FOL_OPE     IN     NUMBER,
                                     p_FEC_ISR_OPE     IN     DATE,
                                     p_SIS_ENT_ORI     IN     CHAR,
                                     p_NUM_REF_SWF     IN     CHAR     -- Ref.
                                                                  ,
                                     p_TIP_ABO_MAN     IN     CHAR,
                                     p_ARE_DTN         IN     CHAR,
                                     p_NUM_CTA         IN     CHAR,
                                     p_NUM_DEV_TRANS   IN     VARCHAR2,
                                     p_COD_USR         IN     CHAR,
                                     p_ERROR              OUT NUMBER);

   /************************************************************************************************

   -- Funcion/Procedimiento: Sp_PAB_ACT_DATOS_OPE_REC
   -- Objetivo: Procedimiento que actualiza informacion de una operacion recibida
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN, PABS_DT_OPRCN_INFCN_OPCON
   -- Fecha: 02/06/16
   -- Autor: Santander
   -- Input:    p_NUM_FOL_NMN->     foliador del banco
   --           p_FEC_ISR_OPE->     Fecha de insercion de la operacion
   --           p_COD_EST_AOS->     Estado nuevo de la operacion
   --           p_NUM_REF_SWF->     Numero de referencia del swift
   --           p_NUM_FOL_OPE_ORG-> Numero folio operacion origen
   --           p_FEC_ISR_OPE_ORG-> Fecha de operación original
   --           p_NUM_REF_EXT ->    Numero de referencia operacion origen
   --           p_NUM_IDF_GTR_DOC-> Numero identificador gestor documental VARCHAR2(40 BYTE),
   --           p_NUM_MVT_CTB ->    Numero movimiento contable CHAR(12 BYTE)
   --           p_GLS_ADC_EST->     Glosa Adicional Estado
   --           p_GLS_EST_RCH->     Glosa Estado Rechazado
   --           p_GLS_MTv_VSD->     Glosa Motivo Visacion
   --           p_COD_EST_AOS->     Identificador de nuevo estado.
   --           p_COD_USR->         rut usuario
   -- Output:
   --          p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_ACT_DATOS_OPE_REC (p_NUM_FOL_OPE       IN     NUMBER,
                                       p_FEC_ISR_OPE       IN     DATE,
                                       p_COD_EST_AOS       IN     CHAR,
                                       p_NUM_REF_SWF       IN     VARCHAR2,
                                       p_NUM_FOL_OPE_ORG   IN     NUMBER,
                                       p_FEC_ISR_OPE_ORG   IN     DATE,
                                       p_NUM_REF_EXT       IN     VARCHAR2,
                                       p_NUM_IDF_GTR_DOC   IN     VARCHAR2,
                                       p_NUM_MVT_CTB       IN     CHAR,
                                       p_GLS_ADC_EST       IN     VARCHAR2,
                                       p_GLS_EST_RCH       IN     VARCHAR2,
                                       p_GLS_MTV_VSD       IN     VARCHAR2,
                                       p_COD_USR           IN     VARCHAR2,
                                       P_COD_SIS_ENT_ORG   IN     VARCHAR2,
                                       p_ERROR                OUT NUMBER);

   /************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUS_CANAL_SALIDA
   -- Objetivo: Procedimiento que busca el Canal de Salida segun, banco, moneda, tipo operacion y horario
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_BANCO_ALMOT
   -- Fecha: 02/06/16
   -- Autor: Santander
   -- Input:
   --                       p_COD_BCO_DTN      Banco  destino
   --                       p_COD_MT_SWF       Cod.MT
   --                       p_MONTO_OPE        Monto
   --                       p_COD_TPO_OPE_AOS  Tipo operacion
   --                       p_COD_DVI          Moneda
   -- Output:
   --          p_COD_SIS_SAL   canal de salida
   --          p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_BUS_CANAL_SALIDA (p_COD_BCO_DTN       IN     CHAR,
                                      p_COD_MT_SWF        IN     NUMBER,
                                      p_MONTO_OPE         IN     NUMBER,
                                      p_COD_TPO_OPE_AOS   IN     CHAR,
                                      p_COD_DVI           IN     CHAR,
                                      p_COD_SIS_SAL          OUT CHAR,
                                      p_ERROR                OUT NUMBER);

   --***********************************************************************************************
   -- Funcion/Procedimiento: SP_ACT_ID_OPE
   -- Objetivo: PROCEDIMIENTO QUE ACTUALIZA ID POR OPERACION
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN
   -- Fecha: 29/12/16
   -- Autor: Santander
   -- Input:
   -- p_NUM_REF_SWF : Referencia Swift
   -- p_NUM_FOL_OPE : Numero folio operacion
   -- p_FEC_ISR_OPE  : Fecha insercion operacion
   -- p_NUM_IDF_GTR_DOC : Numero Documento Gestor
   -- p_FLG_EGR_ING    : Flag Egreso o Ingreso
   -- p_COD_MT_SWF     : Codigo de mt Swift
   -- Output:
   -- p_ERROR -> Inidca si hay error
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   PROCEDURE SP_ACT_ID_OPE (p_NUM_REF_SWF       IN     VARCHAR2,
                            p_NUM_FOL_OPE       IN     NUMBER,
                            p_FEC_ISR_OPE       IN     DATE,
                            p_NUM_IDF_GTR_DOC   IN     VARCHAR2,
                            p_FLG_EGR_ING       IN     NUMBER,
                            p_COD_MT_SWF        IN     NUMBER,
                            p_ERROR                OUT NUMBER);

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_OBT_REF_MVTO
   -- Objetivo: Procedimiento almacenado que crea el campo ReferMvto para contabilidad para Abono cta. cte.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA, PABS_DT_SISTM_ENTRD_SALID, PABS_DT_CBCRA_NOMNA, TCDT040, TCDTBAI
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- p_COD_BCO_STD    -> Codigo banco Santander '0035'
   -- p_COD_SCR        -> Codigo Sucursal
   -- p_NUM_CTA_CTE    -> Numero de cuenta corriente
   -- Output:
   -- p_REFER_MVTO  -> Referencia Movimiento contable
   -- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_OBT_REF_MVTO (p_COD_BCO_STR   IN     CHAR,
                                  p_COD_SCR       IN     CHAR,
                                  p_NUM_CTA_CTE   IN     CHAR,
                                  p_REFER_MVTO       OUT CHAR,
                                  p_ERROR            OUT NUMBER);

   --***********************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_BUS_EST_OPE
   -- Objetivo: Compara el estado de una operación con la que figura en la tabla de detalle de nómina.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA
   -- Fecha: 09/11/2016
   -- Autor: Santander
   -- Input:
   -- P_NUM_FOL_OPE -> numero de nomina
   -- P_COD_EST_AOS -> codigo estado nomina
   -- P_FEC_ISR_OPE -> fecha insercion operacion
   -- N/A
   -- Output:
   -- N/A.
   -- Input/Output:
   -- N/A.
   -- Retorno:
   -- P_FLAG_EST -> valor de retorno flag de comparacion
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   --***********************************************************************************************
   FUNCTION FN_PAB_BUS_EST_OPE (P_NUM_FOL_OPE   IN NUMBER,
                                P_COD_EST_AOS   IN CHAR,
                                P_FEC_ISR_OPE   IN DATE)
      RETURN NUMBER;

   --***********************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_BUS_EST_OPE_LIB
   -- Objetivo: Comprara el estado de una nomina con la que figura en la tabla de nominas.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA
   -- Fecha: 09/11/2016
   -- Autor: Santander
   -- Input:
   -- P_NUM_FOL_OPE -> numero de nomina
   -- P_COD_EST_AOS -> codigo estado nomina
   -- P_FEC_ISR_OPE -> fecha insercion operacion
   -- N/A
   -- Output:
   -- N/A.
   -- Input/Output:
   -- N/A.
   -- Retorno:
   -- P_FLAG_EST -> valor de retorno flag de comparacion
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   --***********************************************************************************************
   FUNCTION FN_PAB_BUS_EST_OPE_LIB (P_NUM_FOL_OPE   IN NUMBER,
                                    P_COD_EST_AOS   IN CHAR,
                                    P_FEC_ISR_OPE   IN DATE)
      RETURN NUMBER;

   /************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_GES_REC_CMN
   -- Objetivo: Procedimiento que gestiona los rechazos de mensajes a combanc que pueden volver a ser gestionados
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN
   --
   -- Fecha: 30/06/17
   -- Autor: GCP
   -- Input: p_NUM_REF_SWF      Referencia de la operación
   --        p_NUM_FOL_OPE      Numero de Folio
   --        p_FEC_ISR_OPE      Fecha de inserción
   -- Output:
   --       p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_GES_REC_CMN (p_NUM_REF_SWF   IN     CHAR,
                                 p_NUM_FOL_OPE      OUT NUMBER,
                                 p_FEC_ISR_OPE      OUT DATE,
                                 p_ERROR            OUT NUMBER);

   /************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_GES_REC_DVPDCV
   -- Objetivo: Procedimiento que gestiona los rechazos de mensajes a combanc de operaciones que son DVPDCV
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN
   --
   -- Fecha: 30/06/17
   -- Autor: GCP
   -- Input: p_NUM_FOL_OPE      Numero de folio de operación
   --        p_FEC_ISR_OPE      Fecha de inserción de operación
   --        p_GLS_MTV_RSH      Glosa Motivo de Rechazo
   --        p_COD_SIS_ENT      Sistema de entrada
   --        p_NUM_MOV_CTB      Numero de movimiento Contable (Cuentas Corrientes)
   -- Output:
   --       p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_GES_REC_DVPDCV (p_NUM_FOL_OPE   IN     NUMBER,
                                    p_FEC_ISR_OPE   IN     DATE,
                                    p_GLS_MTV_RSH   IN     VARCHAR2,
                                    p_COD_SIS_ENT   IN     CHAR,
                                    p_NUM_MOV_CTB   IN     CHAR,
                                    p_ERROR            OUT NUMBER);

   /******************************************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUS_OPE_CAMBIO_CANAL
   -- Objetivo: Procedimiento almacenado que retorna operaciones abonadas para cambio de canal
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN
   --
   -- Fecha: 04-01-2018
   -- Autor: Santander CAH
   -- Input:
   --  p_COD_SIS_SAL -->Area de abono
   --  p_COD_BCO     -->Banco Origen
   --  p_COD_MT_SW   -->Código mt
   --  p_NUM_REF_SWF -->referencia Operación
   -- Output:
   --       p_CURSOR        Notificaciones del día
   --       p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --******************************************************************************************************************************/
   PROCEDURE Sp_PAB_BUS_OPE_CAMBIO_CANAL (
      p_COD_SIS_SAL   IN     CHAR,
      p_COD_BCO       IN     CHAR,
      p_COD_MT_SWF    IN     NUMBER,
      p_NUM_REF_SWF   IN     CHAR,
      p_CURSOR           OUT SYS_REFCURSOR,
      p_ERROR            OUT NUMBER
   );

   /******************************************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ACT_CANAL_ABONO
   -- Objetivo: Procedimiento almacenado que actualiza el canal de salida de la operación
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_USRIO
   --
   -- Fecha: 03-01-2018
   -- Autor: Santander CAH
   -- Input:
   --  p_NUM_FOL_OPE  --> Folio operación
   --  p_FEC_ISR_OPE  --> Fecha operación
   --  p_COD_SIS_SAL  --> Cod. sistema entrada nuevo
   --  p_COD_USR      --> Usuario sistema
   -- Output:
   --       p_CURSOR  --> Cursor de salida con operaciones
   --       p_ERROR: --> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --******************************************************************************************************************************/
   PROCEDURE Sp_PAB_ACT_CANAL_ABONO (p_NUM_FOL_OPE   IN     NUMBER,
                                     p_FEC_ISR_OPE   IN     DATE,
                                     p_COD_SIS_SAL   IN     CHAR,
                                     p_COD_USR       IN     CHAR,
                                     p_ERROR            OUT NUMBER);

   --***********************************************************************************************
   -- Funcion/Procedimiento: SP_VRFCA_NUM_SWF
   -- Objetivo: Consultar si el numero de referencia swf ya se encuentra procesado.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN, PABS_DT_DETLL_OPRCN_DCV
   -- Fecha: 07/08/2018
   -- Autor: jbarboza
   -- Input:
   -- P_NUM_REF_SWF -> NUMERO DE REFERENCIA SWIFT
   -- P_BIC_ORIGEN -> BIC BANCARIO
   -- P_CASOS_SWF -> EL CASO QUE DETERMINARA EL CRITERIO DE BUSQUEDA LOS CUALES PUEDEN SER : INGRESO,EGRESO Y DCP
   -- p_fec_vta   -> Fecha del pago
   -- Output:
   -- P_VALIDACION -> VARIABLE VALIDADOR, SI ES 1 EL NUMERO DE REFERENCIA SWIFT YA ESTA REGISTRADO DE LO CONTRARIO SERA 0
   -- P_ERROR      -> DETERMINA SI HUBO UN ERROR EN LA EJECUCION : 1 SI OCURRIO UN  ERROR DE LO CONTRARIO SERA 0
   -- P_MENSAJE    -> MENSAJE QUE SE USARA COM LOG DEL PROCESO
   -- Input/Output:
   -- N/A.
   -- Retorno:
   -- N/A.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   --***********************************************************************************************
   PROCEDURE sp_vrfca_num_swf (p_num_ref_swf   IN     CHAR,
                               p_bic_origen    IN     CHAR,
                               p_casos_swf     IN     NUMBER,
                               p_fec_vta       IN     DATE,
                               p_validacion       OUT NUMBER,
                               p_error            OUT NUMBER,
                               p_mensaje          OUT VARCHAR2);

   /********************************************************************************************************
   FUNCION/PROCEDIMIENTO: SP_PAB_COMBO_AREA_PAG_REC
   OBJETIVO             : MUESTRA FILTRO EN: INGRESOS -> RECIBO OPERACIONES -> CAMBIAR CANAL SALIDA
                            COMBO: CANAL DE SALIDA, CAMBIO CANAL.
   SISTEMA              : DBO_PAB.
   BASE DE DATOS        : DBO_PAB.
   TABLAS USADAS        : PABS_DT_SISTM_ENTRD_SALID.
   FECHA                : 03/07/2018.
   AUTOR                : SANTANDER
   INPUT                : N/A
   OUTPUT               : P_CURSOR := DATOS CURSOR.
                          P_ERROR  := DETERMINA SI HUBO UN ERROR EN LA EJECUCION.
                                      1 := CON ERROR
                                      0 := SIN ERROR
   OBSERVACIONES
   FECHA       USUARIO    DETALLE
   03/07/2018  SANTANDER  CREACIÓN.
   ********************************************************************************************************/
   PROCEDURE SP_PAB_COMBO_AREA_PAG_REC (p_CURSOR   OUT SYS_REFCURSOR,
                                        p_ERROR    OUT NUMBER);

   /************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ACT_GPI_OPE
   -- Objetivo: Procedimiento que actualiza con información del GPI
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN
   -- Fecha: 02/06/16
   -- Autor: Santander
   -- Input:    p_NUM_FOL_NMN     ->   foliador del banco
   --           p_FEC_ISR_OPE     ->   Fecha de insercion de la operacion
   --           p_NUM_SRV_TPO_UID ->   Numero referencia Swift
   --           p_NUM_IDF_UNC_UID- >   Identificador de nuevo estado.
   -- Output:
   --           p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_ACT_GPI_OPE (p_NUM_FOL_OPE       IN     NUMBER,
                                 p_FEC_ISR_OPE       IN     DATE,
                                 p_NUM_SRV_TPO_UID   IN     CHAR,
                                 p_NUM_IDF_UNC_UID   IN     VARCHAR2,
                                 p_ERROR                OUT NUMBER);


   --***********************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_BUS_VAL_BIC
   -- Objetivo: Funcion que valida mensajeria SWF.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA
   -- Fecha: 09/11/2016
   -- Autor: Santander
   -- Input:
   -- P_NUM_FOL_OPE -> numero de nomina
   -- P_COD_EST_AOS -> codigo estado nomina
   -- P_FEC_ISR_OPE -> fecha insercion operacion
   -- N/A
   -- Output:
   -- N/A.
   -- Input/Output:
   -- N/A.
   -- Retorno:
   -- P_FLAG_EST -> valor de retorno flag de comparacion
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   --***********************************************************************************************
   FUNCTION FN_PAB_BUS_VAL_BIC (P_COD_BCO_ORG IN CHAR)
      RETURN NUMBER;

   --***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUS_DET_OPE_EGR_HOY
   -- Objetivo: Procedimiento almacenado que retorna el detalle de una operacion segun el numero de referencia swift del mismo dia de la ejecucion
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN, PABS_DT_ESTDO_ALMOT
   -- Fecha: 19/11/19
   -- Autor: Santander
   -- Input:   p_NUM_REF_SWF: Numero de referencia de una operacion swift del mismo dia de la ejecucion (cuando se ejecuta el PL)
   -- Output:
   --        p_CURSOR : SYS_REFCURSOR, resultados de consulta.
   --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_BUS_DET_OPE_EGR_HOY (
      p_NUM_REF_SWF   IN     CHAR,
      p_CURSOR           OUT SYS_REFCURSOR,
      p_ERROR            OUT NUMBER
   );

   /************************************************************************************************
     -- Funcion/Procedimiento: SP_PAB_VAL_REF_SWF
     -- Objetivo: Fusion que retorne el codifo de referencia Swift
     -- Sistema: DBO_PAB.
     -- Base de Datos: DBO_PAB.
     -- Tablas Usadas: N/A
     -- Fecha: 02/06/16
     -- Autor: Santander
     -- Input:      p_NUM_FOL_OPE --> numero de operacion a validar,
     --             p_FEC_ISR_OPE --> fecha de inreso de operacion a validar,,
     --
     -- Output:
     --           p_ERROR  --> retorna codigo de validacion.
     -- Input/Output: N/A
     -- Retorno: N/A.
     -- Observaciones: <Fecha y Detalle de ultimos cambios>
     --***********************************************************************************************/
   PROCEDURE SP_PAB_VAL_REF_SWF (p_NUM_FOL_OPE   IN     NUMBER,
                                 p_FEC_ISR_OPE   IN     DATE,
                                 p_ERROR            OUT NUMBER);

   /************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUS_DET_OPE_ORI
   -- Objetivo: Procedimiento almacenado que retorna el detalle de una operacion que puede estar  en la tabla detalle operacion o historico.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN, PABS_DT_OPRCN_INFCN_OPCON, PABS_DT_DETLL_OPRCN_HTRCA, PABS_DT_OPRCN_OPCON_HTRCA
   --          PABS_DT_ESTDO_ALMOT, PABS_DT_SISTM_ENTRD_SALID
   -- Fecha: 05/02/20
   -- Autor: Santander
   -- Input:   p_NUM_FOL_OPE : Numero de folio de la operación
   --          p_NUM_REF_SWF: Numero de referencia de una operacion swift
   --          p_FLG_EGR_ING: Flag de egreso o ingreso
   -- Output:
   --        p_CURSOR : SYS_REFCURSOR, resultados de consulta.
   --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: 05-02-2020: (Julio Villalobos) Creacion de procedimiento almacenado
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_BUS_DET_OPE_ORI (p_NUM_FOL_OPE   IN     NUMBER,
                                     p_NUM_REF_SWF   IN     CHAR,
                                     p_FLG_EGR_ING   IN     NUMBER,
                                     p_CURSOR           OUT SYS_REFCURSOR,
                                     p_ERROR            OUT NUMBER);
END PKG_PAB_OPERACION;