CREATE OR REPLACE PACKAGE         PKG_PAB_MANTENEDOR
IS
   --
   /**************************VARIABLE GLOBALES************************************/
   v_NOM_PCK       VARCHAR2 (30) := 'PKG_PAB_MANTENEDOR';
   ERR_CODE        NUMBER := 0;
   ERR_MSG         VARCHAR2 (300);
   v_DES           VARCHAR2 (300);
   v_DES_BIT       VARCHAR2 (100);
   v_BANCO         VARCHAR2 (9) := '970150005';
   p_s_mensaje     VARCHAR2 (400);
   V_FLG_VGN_NO    NUMBER (1);
   V_FLG_VGN_SI    NUMBER (1);
   V_FLG_VGN_ELI   NUMBER (1);

   TYPE T_NUM_FOL_NMN
   IS
      TABLE OF NUMBER
         INDEX BY PLS_INTEGER;

   /**************************FIN VARIABLE GLOBALES********************************/

   --
   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_CON_BCO_ALMOT
   -- Objetivo: Procedimiento actualiza estado de la operaci?n abonada exitosamente.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: N/A
   -- Fecha: 18/11/16
   -- Autor: Santander
   -- Input:
   -- p_COD_PAIS -> foliador del banco, PK tabla detalle operaciones.
   -- p_COD_BIC -> Fecha de insercion de operacion en el sistema, PK tabla detalle operaciones.
   -- p_COD_RUT -> Numero de Refencia SWIFT
   -- p_NOMBRE -> Codigo de Usuario que se registrara en bitacora
   -- p_FLG_VGN -> Indica si el banco esta habilitado o no
   -- Output:
   -- p_CURSOR -> Resultado de consulta
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************

   PROCEDURE SP_PAB_CON_BCO_ALMOT (p_COD_PAIS   IN     CHAR,
                                   p_COD_BIC    IN     CHAR,
                                   p_COD_RUT    IN     CHAR,
                                   p_NOMBRE     IN     CHAR,
                                   p_FLG_VGN    IN     CHAR,
                                   p_CURSOR        OUT SYS_REFCURSOR,
                                   p_ERROR         OUT NUMBER);


   --
   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_INS_BCO_ALMOT
   -- Objetivo: Procedimiento actualiza estado de la operaci?n abonada exitosamente.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: N/A
   -- Fecha: 18/11/16
   -- Autor: Santander
   -- Input:
   -- COD_BIC -> codigo bis del banco
   -- p_IMP_LMT_CDT  -> Importe Limite Credito
   -- p_IMP_LMT_DBT  -> Importe Limite Debito
   -- p_CANALPAGO    -> Canla de pago
   -- p_USUARIO      -> Corresponde al usuario
   -- Output:
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.

   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_INS_BCO_ALMOT (p_COD_BIC       IN     CHAR,
                                   p_IMP_LMT_CDT   IN     NUMBER,
                                   p_IMP_LMT_DBT   IN     NUMBER,
                                   p_CANALPAGO     IN     CHAR,
                                   p_USUARIO       IN     CHAR,
                                   p_CURSOR           OUT SYS_REFCURSOR,
                                   p_ERROR            OUT NUMBER);

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_ACT_BCO_ALMOT
   -- Objetivo: Procedimiento actualiza estado de la operaci?n abonada exitosamente.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: N/A
   -- Fecha: 18/11/16
   -- Autor: Santander
   -- Input:
   --    p_COD_BIC -> codigo bis del banco
   --    p_IMP_LMT_CDT  -> Importe Limite Credito
   --    p_IMP_LMT_DBT  -> Importe Limite Debito
   --    p_HABILITADO -> indica si esta habilitado
   --    p_CANALPAGO -> corresponde al canal de pago
   --    p_USUARIO -> corresponde al codigo de usuario
   -- Output:
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.

   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_ACT_BCO_ALMOT (p_COD_BIC       IN     CHAR,
                                   p_IMP_LMT_CDT   IN     NUMBER,
                                   p_IMP_LMT_DBT   IN     NUMBER,
                                   p_HABILITADO    IN     CHAR,
                                   p_CANALPAGO     IN     CHAR,
                                   p_USUARIO       IN     CHAR,
                                   p_ERROR            OUT NUMBER);


   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_CON_BCO_COR_ALMOT
   -- Objetivo: Procedimiento que consultar bancos de altos montos
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas:  TCDTBAI , TCDT040, TGEN_0112
   -- Fecha: 01/12/2016
   -- Autor: Santander
   -- Input:
   --    p_COD_PAIS -> corresponde al codigo del pais
   --    p_COD_BIC -> corresponde al codigo bic del banco
   --    p_COD_RUT -> corresponde al rut del banco
   --    p_NOMBRE -> corresponde al nombre del banco
   -- Output:
   -- p_CURSOR -> Cursor con los registros de los elementos
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- **************************************************************************
   PROCEDURE SP_PAB_CON_BCO_COR_ALMOT (p_COD_PAIS   IN     CHAR,
                                       p_COD_BIC    IN     CHAR,
                                       p_COD_RUT    IN     CHAR,
                                       p_NOMBRE     IN     CHAR,
                                       p_CURSOR        OUT SYS_REFCURSOR,
                                       p_ERROR         OUT NUMBER);

   --
   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_ELI_BCO_ALMOT
   -- Objetivo: Procedimiento actualiza estado de la operaci?n abonada exitosamente.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: N/A
   -- Fecha: 18/11/16
   -- Autor: Santander
   -- Input:
   --    p_COD_BIC -> codigo bis del banco
   --    p_HABILITADO -> indica si esta habilitado
   --    p_USUARIO -> corresponde al usuario que realiza el cambio
   --    p_USUARIO -> corresponde al codigo de usuario
   -- Output:
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.

   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_ELI_BCO_ALMOT (p_COD_BIC      IN     CHAR,
                                   p_HABILITADO   IN     NUMBER,
                                   p_USUARIO      IN     CHAR,
                                   p_ERROR           OUT NUMBER);

   --
   --
   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_ACT_MON_ALMOT
   -- Objetivo: Procedimiento actualiza monto de la moneda.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: N/A
   -- Fecha: 18/11/16
   -- Autor: Santander
   -- Input:
   --    p_COD_DVI -> tipo de moneda
   --    p_IMP_MAX_SIN_ATZ -> importe maximo
   --    p_USR_MOD -> corresponde al codigo de usuario
   -- Output:
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_ACT_MON_ALMOT (p_COD_DVI           IN     CHAR,
                                   p_IMP_MAX_SIN_ATZ   IN     NUMBER,
                                   p_USR_MOD           IN     CHAR,
                                   p_ERROR                OUT NUMBER);

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_ELI_MON_ALMOT
   -- Objetivo: Procedimiento que Elimina bancos de altos montos
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas:  PABS_DT_MONDA_ALMOT
   -- Fecha: 01/12/2016
   -- Autor: Santander
   -- Input:
   -- p_COD_DVI
   -- p_USR_MOD
   -- N/A
   -- Output:
   -- p_CURSOR -> Cursor con los registros de los 15 elementos
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_ELI_MON_ALMOT (p_COD_DVI   IN     CHAR,
                                   p_USR_MOD   IN     CHAR,
                                   p_ERROR        OUT NUMBER);

   --
   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_INS_MON_ALMOT
   -- Objetivo: Procedimiento que Elimina bancos de altos montos
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas:  PABS_DT_BANCO_ALMOT
   -- Fecha: 01/12/2016
   -- Autor: Santander
   -- Input:
   --    p_COD_DVI : Codigo de moneda
   --    p_IMP_MAX_SIN_ATZ : valor de importe
   --    p_FLG_VGN : flag de vigencia
   --    p_USR_MOD . usuario de modificacion
   -- N/A
   -- Output:
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_INS_MON_ALMOT (p_COD_DVI           IN     CHAR,
                                   p_IMP_MAX_SIN_ATZ   IN     NUMBER,
                                   p_FLG_VGN           IN     CHAR,
                                   p_USR_MOD           IN     CHAR,
                                   p_ERROR                OUT NUMBER);

   --
   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_CON_MON_ALMOT
   -- Objetivo: Procedimiento que Cosulta las monedas de altos montos
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas:  PABS_DT_MONDA_ALMOT
   -- Fecha: 01/12/2016
   -- Autor: Santander
   -- Input:
   --    p_COD_DVI : Codigo de monedan
   -- N/A
   -- Output:
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_CON_MON_ALMOT (p_COD_DVI   IN     CHAR,
                                   p_CURSOR       OUT SYS_REFCURSOR,
                                   p_ERROR        OUT NUMBER);

   --
   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_CON_TPO_OPE_ALMOT
   -- Objetivo: Procedimiento que consulta tipo de operacion CONSTITUCION GARANTIA COMBANC
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas:  PABS_DT_TIPO_OPRCN_ALMOT
   -- Fecha: 17/02/2017
   -- Autor: Santander
   -- Input:
   -- Output:
   -- p_CURSOR -> Cursor con los registros de los elementos
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_CON_TPO_OPE_ALMOT (p_CURSOR   OUT SYS_REFCURSOR,
                                       p_ERROR    OUT NUMBER);

   --
   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_ACT_TPO_OPE_ALMOT
   -- Objetivo: Procedimiento que Actualiza tipo de operacion CONSTITUCION GARANTIA COMBANC
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas:  NN
   -- Fecha: 17/02/2017
   -- Autor: Santander
   -- Input:
   --    p_MONTO_OPE -> corresponde al Monto
   --  p_COD_USR -> Codigo de Usuario
   -- Output:
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_ACT_TPO_OPE_ALMOT (p_MONTO_OPE   IN     NUMBER,
                                       p_COD_USR     IN     CHAR,
                                       p_ERROR          OUT NUMBER);

   --
   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_CON_CTA_CNTBL_ALMOT
   -- Objetivo: Procedimiento que consulta los datos de las cuentas
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_CENTA_CNTBL, PABS_DT_SISTM_ENTRD_SALID
   -- Fecha: 12/DIC/2016
   -- Autor: Santander
   -- Input:
   -- p_COD_SIS_ENT_SAL -> corresponde al codigo del sistema
   -- p_FLAG_MRD_UTZ_SW -> Flag de vigencia
   -- Output:
   -- p_CURSOR
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_CON_CTA_CNTBL_ALMOT (
      p_COD_SIS_ENT_SAL   IN     CHAR,
      p_FLAG_MRD_UTZ_SW   IN     NUMBER,
      p_CURSOR               OUT SYS_REFCURSOR,
      p_ERROR                OUT NUMBER
   );

   --
   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_INS_CTA_CNTBL_ALMOT
   -- Objetivo: Procedimiento inserta los datos del corresponsal
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_CENTA_CNTBL
   -- Fecha: 12/DIC/2016
   -- Autor: Santander
   -- Input:
   --    p_COD_SIS_ENT_SAL -> corresponde al codigo del sistema
   --    p_FLAG_UTZ_SWF -> Flag de vigencia
   --    p_NUM_CTA_CTB -> corresponde al numero de cuenta
   --    p_NUM_CTA_CTB -> corresponde al numero de cuenta
   --    p_NUM_CRT_CTA_CTB -> corresponde al numero corto de cuenta
   --    p_NUM_CNP_CTA_CTB -> numero de concepto cuenta
   --    p_COD_SUC_DTN_TPN -> corresponde al codigo de la sucursal
   --    p_GLS_IFM_MVT_CTB -> corresponde a la glosa
   --    p_COD_USR_CRC -> Corresponde al numero codigo de usuario
   -- Output:
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_INS_CTA_CNTBL_ALMOT (p_COD_SIS_ENT_SAL   IN     CHAR,
                                         p_FLAG_UTZ_SWF      IN     NUMBER,
                                         p_NUM_CTA_CTB       IN     CHAR,
                                         p_NUM_CRT_CTA_CTB   IN     CHAR,
                                         p_NUM_CNP_CTA_CTB   IN     CHAR,
                                         p_COD_SUC_DTN_TPN   IN     CHAR,
                                         p_GLS_IFM_MVT_CTB   IN     CHAR,
                                         p_GLS_COM_CTA_CTB   IN     CHAR,
                                         p_COD_USR_CRC       IN     NUMBER,
                                         p_ERROR                OUT NUMBER);


   --
   --
   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_CON_CRPSL_ALMOT
   -- Objetivo: Procedimiento almacenado que muestra los bancos corresponsales de Altos Montos.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM, TCDTBAI TBAI, TGEN_0112 PAIS
   -- Tablas Usadas: PABS_DT_CENTA_BANCO_CRPSL
   -- Fecha: 12/DIC/2016
   -- Autor: Santander
   -- Input:
   --    p_COD_BIC -> Corresponde al codigo bic del banco
   --    p_COD_PAIS -> Corresponde al codigo del pais
   --    p_COD_MON -> corresponde al codigo del tipo de moneda
   --    p_COD_CTA -> Corresponde al codigo de cuenta
   -- Output:
   -- p_CURSOR
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_CON_CRPSL_ALMOT (p_COD_BIC    IN     CHAR,
                                     p_COD_PAIS   IN     CHAR,
                                     p_COD_MON    IN     CHAR,
                                     p_COD_CTA    IN     CHAR,
                                     p_CURSOR        OUT SYS_REFCURSOR,
                                     p_ERROR         OUT NUMBER);

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_INS_CRPSL_ALMOT
   -- Objetivo: Procedimiento inserta los datos del corresponsal
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_CENTA_BANCO_CRPSL
   -- Fecha: 12/DIC/2016
   -- Autor: Santander
   -- Input:
   --    p_NUM_CTA_COR -> Corresponde al numero de cuenta
   --    p_COD_BIC_BCO -> Corresponde al codigo bic del banco
   --    p_COD_DVI -> Codigo de moneda
   --    p_IMP_TOT_LCD -> monto linea de credito
   --    p_COD_USR_CRC     -> Codigo del usuario
   -- Output:
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_INS_CRPSL_ALMOT (p_NUM_CTA_COR       IN     VARCHAR2,
                                     p_COD_BIC_BCO       IN     CHAR,
                                     p_COD_DVI           IN     CHAR,
                                     p_IMP_TOT_LCD       IN     NUMBER,
                                     p_COD_USR_CRC       IN     CHAR,
                                     p_COD_MT_SWF        IN     NUMBER,
                                     p_NUM_PDI           IN     NUMBER,
                                     p_POR_INT_LNA_CDT   IN     NUMBER,
                                     p_FEC_VGN_LNA       IN     DATE,
                                     p_FLG_CHQ           IN     NUMBER,
                                     p_FLG_TRF_LNA       IN     NUMBER,
                                     p_FLG_OVR_LNA       IN     NUMBER,
                                     p_IMP_LMT_SBF_LNA   IN     NUMBER,
                                     p_HOR_CRR_COR       IN     VARCHAR2,
                                     p_IMP_MAX_SBF       IN     NUMBER,
                                     p_ERROR                OUT NUMBER);

   --
   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_ACT_CRPSL_ALMOT
   -- Objetivo: Procedimiento actualiza los datos del corresponsal
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_CENTA_BANCO_CRPSL
   -- Fecha: 12/DIC/2016
   -- Autor: Santander
   -- Input:
   -- p_NUM_CTA_COR -> Corresponde al numero de cuenta
   -- p_COD_BIC_BCO -> Corresponde al codigo bic del banco
   -- p_COD_DVI     -> Codigo de moneda
   -- p_IMP_TOT_LCD -> monto linea de credito
   -- p_IMP_SDO_CTA_COR -> monto cuenta corriente
   -- p_FLG_VGN -> Indica el estado de vigencia 1 vigente // 0 no vigente
   -- p_NUM_LIN_CRD
   -- p_COD_USR -> Codigo del usuario
   -- Output:
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_ACT_CRPSL_ALMOT (p_NUM_CTA_COR       IN     VARCHAR2,
                                     p_COD_BIC_BCO       IN     CHAR,
                                     p_COD_DVI           IN     CHAR,
                                     p_IMP_LMT_SBF_LNA   IN     NUMBER,
                                     p_IMP_MAX_SBF       IN     NUMBER,
                                     p_HOR_CRR_COR       IN     CHAR,
                                     p_COD_USR           IN     CHAR,
                                     p_IMP_TOT_LCD       IN     NUMBER,
                                     p_COD_MT_SWF        IN     NUMBER,
                                     p_NUM_PDI           IN     NUMBER,
                                     p_POR_INT_LNA_CDT   IN     NUMBER,
                                     p_FEC_VGN_LNA       IN     DATE,
                                     p_FLG_CHQ           IN     NUMBER,
                                     p_FLG_TRF_LNA       IN     NUMBER,
                                     p_FLG_OVR_LNA       IN     NUMBER,
                                     p_ERROR                OUT NUMBER);

   --
   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_ELI_CRPSL_ALMOT
   -- Objetivo: Procedimiento elimina de forma logica un corresponsal
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_CENTA_BANCO_CRPSL
   -- Fecha: 12/DIC/2016
   -- Autor: Santander
   -- Input:
   -- p_NUM_CTA_COR -> Corresponde al numero de cuenta
   -- p_COD_BIC_BCO -> Corresponde al codigo bic del banco
   -- p_COD_USR -> Codigo del usuario
   -- Output:
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_ELI_CRPSL_ALMOT (p_NUM_CTA_COR   IN     VARCHAR2,
                                     p_COD_BIC_BCO   IN     CHAR,
                                     p_COD_DVI       IN     CHAR,
                                     p_ERROR            OUT NUMBER);

   --
   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_CON_CRPSL_COR_ALMOT
   -- Objetivo: Procedimiento consulta los corresponsales que no estan en las tabls de altosmontos
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_CENTA_BANCO_CRPSL,  TCDTBAI, TGEN_0112, TCDT040
   -- Fecha: 12/DIC/2016
   -- Autor: Santander
   -- Input:
   -- p_RUT_BCO -> Corresponde al numero de rut del banco
   -- p_NOM_BCO -> Corresponde al nombre del banco
   -- p_COD_BIC -> Corresponde al codigo bic del banco
   -- p_COD_PAIS -> Codigo del pais
   -- Output:
   -- p_CURSOR
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_CON_CRPSL_COR_ALMOT (p_COD_BIC    IN     CHAR,
                                         p_COD_PAIS   IN     CHAR,
                                         p_RUT_BCO    IN     CHAR,
                                         p_NOM_BCO    IN     CHAR,
                                         p_CURSOR        OUT SYS_REFCURSOR,
                                         p_ERROR         OUT NUMBER);

   --
   --**********************************************************************************************
   -- Funcion/Procedimiento: SP_ PAB_OBT_SIS_ENTSAL
   -- Objetivo: Procedimiento que entrega lista de canales segun el tipo ingresado
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_SISTM_ENTRD_SALID
   -- Fecha: 30/11/16
   -- Autor: Santander
   -- Input:
   -- p_COD_SIS_ENT_SAL -> Codigo de canal
   -- Output:
   -- p_CURSOR ->  Lista de horarios de inicio o cierre de canales
   -- p_ERROR -> Indicador de errores (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_PAB_OBT_SIS_ENTSAL (p_COD_SIS_ENT_SAL   IN     CHAR,
                                    p_CURSOR               OUT SYS_REFCURSOR,
                                    p_ERROR                OUT NUMBER);

   --**********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_ACT_SIS_ENTSAL
   -- Objetivo: Procedimiento que entrega detale de canal seleccionado
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_SISTM_ENTRD_SALID
   -- Fecha: 30/11/16
   -- Autor: Santander
   -- Input:
   -- p_COD_SIS_ENT_SAL -> Codigo de canal
   -- p_COD_USR_ULT_MOD
   -- p_HOR_ICO_SIS
   -- p_HOR_CRR_SIS
   -- p_FLG_VGN
   -- Output:
   -- p_ERROR -> Indicador de errores (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_PAB_ACT_SIS_ENTSAL (p_COD_SIS_ENT_SAL   IN     CHAR,
                                    p_COD_USR_ULT_MOD   IN     CHAR,
                                    p_HOR_ICO_SIS       IN     NUMBER,
                                    p_HOR_CRR_SIS       IN     NUMBER,
                                    p_FLG_VGN           IN     NUMBER,
                                    p_ERROR                OUT NUMBER);

   --

   --***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_COMBO_BAN_ALMTO_MAN
   -- Objetivo: PROCEDIMIENTO ALMACENADO QUE CONSULTA Y ENTREGA TODOS LOS BANCOS DEL SISTEMA (HABILITADOS/DESHABILITADOS)
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_BANCO_ALMOT
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- N/A
   -- Output:
   -- p_CURSOR -> SYS_REFCURSOR, resultados de consulta con los bancos
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_COMBO_BAN_ALMTO_MAN (p_CURSOR OUT SYS_REFCURSOR);

   --***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_COMBO_BAN_NUE_ALMTO
   -- Objetivo: PROCEDIMIENTO ALMACENADO QUE CONSULTA Y ENTREGA TODOS LOS BANCOS DEL SISTEMA (HABILITADOS/DESHABILITADOS)
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_BANCO_ALMOT
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- N/A
   -- Output:
   -- p_CURSOR -> SYS_REFCURSOR, resultados de consulta con los bancos
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_COMBO_BAN_NUE_ALMTO (p_CURSOR OUT SYS_REFCURSOR);

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_COMBO_TPO_MT_CRTLA
   -- Objetivo: Procedimiento que retorne tipo de mt para cartola 940 ó 950
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_TIPO_MT_SWIFT
   -- Fecha: 05/09/2017
   -- Autor: Santander- CAH
   -- Input:
   -- N/A
   -- Output:
   -- p_CURSOR -> Cursor con los registros de los 2 estados
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_COMBO_TPO_MT_CRTLA (p_CURSOR   OUT SYS_REFCURSOR,
                                        p_ERROR    OUT NUMBER);
END PKG_PAB_MANTENEDOR;