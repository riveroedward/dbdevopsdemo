CREATE OR REPLACE PACKAGE         PKG_PAB_TRACKING
IS
   /**  Package el cual contrendra todas las funciones y procedimientos relacionados con la trazabilidad del sistem
   -- @everis */
   /**************************VARIABLE GLOBALES************************************/
   v_NOM_PCK     VARCHAR2 (16) := 'PKG_PAB_TRACKING';
   ERR_CODE      NUMBER := 0;
   ERR_MSG       VARCHAR2 (300);
   p_s_mensaje   VARCHAR2 (400);

   /**************************FIN VARIABLE GLOBALES********************************/

   /************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_INS_LOG_BD
   -- Objetivo: El procedimiento insertara en la tabla PABS_DT_BTCOR_BASE_DATO_ALMOT la secuencia de acciones o
   --               instrucciones que ocurran en la base de datos de altos montos.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas:   PABS_DT_BTCOR_PRCSO
   -- Fecha: 02/06/16
   -- Autor: EVERIS
   -- Input:  p_COD_ERR_BSE_DTS-> Se puede almacenar el codigo interno de la base de datos o el error de oracle.
   --         p_NOM_PCK       -> Nombre del package de base de datos.
   --         p_GLS_BTC_BSE_DTS-> Utilizado para mostrar informacion o guardar errores que suceden durante una ejecucion de un procedimiento.
   --         p_NOM_PCM-      > Nombre del prodecimiento de la base de datos.
   -- Output:
   --   N/A
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_INS_LOG_BD (p_COD_ERR_BSE_DTS   IN CHAR,
                                p_NOM_PCK           IN VARCHAR2,
                                p_GLS_BTC_BSE_DTS   IN VARCHAR2,
                                p_NOM_PCM           IN CHAR);

   --
   /************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_INS_BIT_CAB_PROCESO
   -- Objetivo: Procedimiento que incerta la informacion principal de la cabecera de los procesos.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_BTCOR_DETLL_PRCSO
   -- Fecha: 02/06/16
   -- Autor: EVERIS
   -- Input:  p_COD_TPO_PSC-> Identificador del proceso que utilizara la grabacion de traza
   -- Output:
   --    p_FEC_ICO_PCS: Fecha inicio de proceso con milisegundos
   --          p_NUM_TCK_PCS: Numero de ticket del proceso, el cual sirve como seguimiento para el detalle del tracking
   --          p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_INS_BIT_CAB_PROCESO (p_COD_TPO_PSC   IN     CHAR,
                                         p_FEC_ICO_PCS      OUT VARCHAR2,
                                         p_NUM_TCK_PCS      OUT NUMBER,
                                         p_ERROR            OUT NUMBER);

   --
   /************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_INS_BIT_DET_PROCESO
   -- Objetivo: Procedimiento que inserta el detalle de los procesos ejecutados en el sistema
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_BTCOR_OPRCN
   -- Fecha: 02/06/16
   -- Autor: EVERIS
   -- Input:  p_GLS_DET_PCS    : Glosa con informacion del detalle del proceso
   --         p_FEC_ICO_PCS    : Fecha de inicio del proceso
   --         p_COD_ERR_BIB    : Codigo de error de la Bitacora. (Es completado cuando existe error)
   --         p_COD_TPO_PSC    : Identificador del proceso
   -- Output:
   --    p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS   IN     VARCHAR2,
                                         p_FEC_ICO_PCS   IN     VARCHAR2,
                                         p_COD_ERR_BIB   IN     CHAR,
                                         p_COD_TPO_PSC   IN     CHAR,
                                         p_ERROR            OUT NUMBER);

   --
   /************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_UPD_BIT_CAB_PROCESO
   -- Objetivo: Procedimiento que actualiza informacion de la cabecera del proceso.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_BTCOR_BASE_DATO_ALMOT
   -- Fecha: 02/06/16
   -- Autor: EVERIS
   -- Input:  p_FEC_ICO_PCS    : Fecha de inicio del proceso
   --         p_COD_TPO_PSC    : Identificador del proceso
   --          p_NUM_TCK_PCS    : Numero de ticket del proceso, el cual sirve como seguimiento para el detalle del tracking
   --          p_ERROR_PROCESO  : Identifica si el proceso ejecutado termino de forma correcta o con error.
   -- Output:
   --    p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_UPD_BIT_CAB_PROCESO (p_FEC_ICO_PCS     IN     VARCHAR2,
                                         p_COD_TPO_PSC     IN     CHAR,
                                         p_NUM_TCK_PCS     IN     NUMBER,
                                         p_ERROR_PROCESO   IN     NUMBER,
                                         p_ERROR              OUT NUMBER);

   --
   /************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_INS_BIT_OPE
   -- Objetivo: Procedimiento que inserta en la bitacora de operaciones las acciones de los usuarios
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_BTCOR_PRCSO
   -- Fecha: 02/06/16
   -- Autor: EVERIS
   -- Input:  p_NUM_FOL_OPE    : Numero de folio de la operacion a la cual se esta registrando un cambio
   --         p_FEC_ISR_OPE    : Fecha de insercion de la operacion a la cual se esta registrando un cambio
   --         p_COD_USR        : Identificador del usuario que realiza la accion
   --         p_COD_EST_ICO    : Estado inicial de la operacion - Antes de la accion
   --         p_COD_EST_FIN    : Estado fin de la operacion - Despues de la accion
   --         p_DSC_GLB_BTC    : Informacion adicional de la accion realizada
   -- Output:
   --    p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_INS_BIT_OPE (p_NUM_FOL_OPE   IN     NUMBER,
                                 p_FEC_ISR_OPE   IN     TIMESTAMP,
                                 p_COD_USR       IN     CHAR,
                                 p_COD_EST_ICO   IN     NUMBER,
                                 p_COD_EST_FIN   IN     NUMBER,
                                 p_DSC_GLB_BTC   IN     VARCHAR2,
                                 p_ERROR            OUT NUMBER);
END PKG_PAB_TRACKING;
--