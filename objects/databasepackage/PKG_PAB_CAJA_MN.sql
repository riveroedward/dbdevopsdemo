CREATE OR REPLACE PACKAGE         PKG_PAB_CAJA_MN
IS
   --
   v_NOM_PCK        VARCHAR2 (30) := 'PKG_PAB_CAJA_MN';
   ERR_CODE         NUMBER := 0;
   ERR_MSG          VARCHAR2 (300);
   v_DES            VARCHAR2 (300);
   v_DES_BIT        VARCHAR2 (100);
   p_s_mensaje      VARCHAR2 (400);
   v_FECHA_AYER     DATE := TRUNC (SYSDATE - 1);
   v_FECHA_ACTUAL   DATE := TRUNC (SYSDATE);
   v_COD_PAIS       CHAR (2) := 'SN';
   v_cero           NUMBER := 0;

   --Canal de salida, para estado Pagado por contingencia
   v_EST_PAG_CNT    CHAR (10) := 'PCNT';

   v_dia_ini        DATE := TRUNC (SYSDATE) + INTERVAL '1' MINUTE;
   v_dia_fin DATE
         := TRUNC (SYSDATE) + INTERVAL '23' HOUR + INTERVAL '59' MINUTE ;


   /************************************************************************************************
   -- Funcion/Procedimiento: SP_BUS_PSCON_BCCH_MN
   -- Objetivo: Procedimiento almacenado que Muestra la posición del Banco Central, saldos
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX
   --
   -- Fecha: 22-11-2017
   -- Autor: Santander
   -- Input: p_
   --
   -- Output:
   --        p_CURSOR : SYS_REFCURSOR, resultados de consulta.
   --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_BUS_PSCON_BCCH_MN (p_CURSOR   OUT SYS_REFCURSOR,
                                   p_ERROR    OUT NUMBER);

   /************************************************************************************************
   -- Funcion/Procedimiento: SP_BUS_DETLL_BCCH_MN
   -- Objetivo: Procedimiento almacenado que Muestra el detalle de la posición del Banco Central
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX
   --
   -- Fecha: 22-11-2017
   -- Autor: Santander
   -- Input: p_
   --
   -- Output:
   --        p_CURSOR : SYS_REFCURSOR, resultados de consulta.
   --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_BUS_DETLL_BCCH_MN (p_CURSOR    OUT SYS_REFCURSOR,
                                   p_CURSOR2   OUT SYS_REFCURSOR,
                                   p_ERROR     OUT NUMBER);

   /************************************************************************************************
   -- Funcion/Procedimiento: SP_BUS_PSCON_COMBAC_MN
   -- Objetivo: Procedimiento almacenado que Muestra la posición del Comban
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX
   --
   -- Fecha: 22-11-2017
   -- Autor: Santander
   -- Input: p_
   --
   -- Output:
   --        p_CURSOR : SYS_REFCURSOR, resultados de consulta.
   --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_BUS_PSCON_COMBAC_MN (p_CURSOR   OUT SYS_REFCURSOR,
                                     p_ERROR    OUT NUMBER);


   /************************************************************************************************
   -- Funcion/Procedimiento: SP_BUS_DETLL_COMBANC_MN
   -- Objetivo: Procedimiento almacenado que Muestra el detalle de la posición del Combanc
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX
   --
   -- Fecha: 22-11-2017
   -- Autor: Santander
   -- Input: p_
   --
   -- Output:
   --        p_CURSOR : SYS_REFCURSOR, resultados de consulta.
   --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_BUS_DETLL_COMBANC_MN (p_CURSOR    OUT SYS_REFCURSOR,
                                      p_CURSOR2   OUT SYS_REFCURSOR,
                                      p_ERROR     OUT NUMBER);

   /************************************************************************************************
   -- Funcion/Procedimiento: SP_BUS_PSCON_SINACOFI_MN
   -- Objetivo: Procedimiento almacenado que Muestra la posición del Sinacofi
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX
   --
   -- Fecha: 22-11-2017
   -- Autor: Santander
   -- Input: p_
   --
   -- Output:
   --        p_CURSOR : SYS_REFCURSOR, resultados de consulta.
   --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_BUS_PSCON_SINACOFI_MN (p_CURSOR   OUT SYS_REFCURSOR,
                                       p_ERROR    OUT NUMBER);

   /************************************************************************************************
   -- Funcion/Procedimiento: SP_BUS_DETLL_SINACOFI_MN
   -- Objetivo: Procedimiento almacenado que Muestra el detalle de la posición del Combanc
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX,
   --
   -- Fecha: 22-11-2017
   -- Autor: Santander
   -- Input: p_
   --
   -- Output:
   --        p_CURSOR : SYS_REFCURSOR, resultados de consulta.
   --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_BUS_DETLL_SINACOFI_MN (p_CURSOR    OUT SYS_REFCURSOR,
                                       p_CURSOR2   OUT SYS_REFCURSOR,
                                       p_ERROR     OUT NUMBER);


   /************************************************************************************************
   -- Funcion/Procedimiento: SP_BUS_PRYCN_INTRADIA_MN
   -- Objetivo: Procedimiento almacenado que Muestra los saldos de Intradia
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX
   --
   -- Fecha: 22-11-2017
   -- Autor: Santander
   -- Input: p_CURSOR
   --
   -- Output:
   --        p_CURSOR : SYS_REFCURSOR, resultados de consulta.
   --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_BUS_PRYCN_INTRADIA_MN (p_CURSOR   OUT SYS_REFCURSOR,
                                       p_ERROR    OUT NUMBER);



   /************************************************************************************************
   -- Funcion/Procedimiento: SP_BUS_DETLL_INTRADIA_MN
   -- Objetivo: Procedimiento almacenado que Muestra el detalle de saldos Intradia
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX
   --
   -- Fecha: 22-11-2017
   -- Autor: Santander
   -- Input: p_
   --
   -- Output:
   --        p_CURSOR : SYS_REFCURSOR, resultados de consulta.
   --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_BUS_DETLL_INTRADIA_MN (p_CURSOR    OUT SYS_REFCURSOR,
                                       p_CURSOR2   OUT SYS_REFCURSOR,
                                       p_ERROR     OUT NUMBER);

   --*******************************************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_ELI_PRYCN_MN
   -- Objetivo: Procedimiento que elimina registro de proyecciones de Caja Nacional según número de operación y su respectiva nómina
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_MVNTO_FLIAL
   -- Fecha: 21-11-2017
   -- Autor: Santander- CAH
   -- Input:
   -- p_NUM_FOL_NMN  -> Núnmero Folio
   -- p_NUM_FOL_OPE  -> Número Operación
   -- p_FEC_INS_OPE  -> Fecha Ingreso Movimiento
   -- Output:
   -- p_ERROR  ->  Indicada si el procedimiento
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --********************************************************************************************************************************
   PROCEDURE SP_PAB_ELI_PRYCN_MN (p_NUM_FOL_NMN   IN     NUMBER,
                                  p_NUM_FOL_OPE   IN     NUMBER,
                                  p_FEC_INS_OPE   IN     DATE,
                                  p_ERROR            OUT NUMBER);

   /***************************************************************************************************************
   -- Funcion/Procedimiento: P_PAB_CAL_SLDO_FINAL_DIA_MN
   -- Objetivo: Procedimiento almacenado que actualiza saldo del campo Total final Día de Proyección
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX
   --
   -- Fecha:22-11-2017
   -- Autor: Santander - CAH
   -- Input:
   -- Output:
   --    p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --**************************************************************************************************************/
   PROCEDURE SP_PAB_CAL_SLDO_FINAL_DIA_MN (p_ERROR OUT NUMBER);

   /************************************************************************************************
   -- Funcion/Procedimiento: SP_BUS_GRNTA_COMDER_MN
   -- Objetivo: Procedimiento almacenado que Muestra la posición del Banco Central, saldos
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_BIBLI_CAMPO_OBLTO_MT
   --
   -- Fecha: 16/06/17
   -- Autor: Santander
   -- Input: p_
   --
   -- Output:
   --        p_CURSOR : SYS_REFCURSOR, resultados de consulta.
   --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_BUS_GRNTA_COMDER_MN (p_CURSOR   OUT SYS_REFCURSOR,
                                     p_ERROR    OUT NUMBER);


   /************************************************************************************************
  -- Funcion/Procedimiento: SP_BUS_CRTLA_BBCH_MN
  -- Objetivo: Procedimiento almacenado que Muestra la cartola del Banco Central, saldos
  -- Sistema: DBO_PAB.
  -- Base de Datos: DBO_PAB.
  -- Tablas Usadas: PABS_DT_BIBLI_CAMPO_OBLTO_MT
  --
  -- Fecha: 16/06/17
  -- Autor: Santander
  -- Input: p_
  --
  -- Output:
  --        p_CURSOR : SYS_REFCURSOR, resultados de consulta.
  --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
  -- Input/Output: N/A
  -- Retorno: N/A.
  -- Observaciones: <Fecha y Detalle de ultimos cambios>
  --***********************************************************************************************/
   PROCEDURE SP_BUS_CRTLA_BCCH_MN (p_CURSOR   OUT SYS_REFCURSOR,
                                   p_ERROR    OUT NUMBER);

   /************************************************************************************************
   -- Funcion/Procedimiento: SP_DETLL_CTCTE_BCCH_MN
   -- Objetivo: Procedimiento almacenado que Muestra detalle de Cartola Banco Central de Chile
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_BIBLI_CAMPO_OBLTO_MT
   --
   -- Fecha: 16/06/17
   -- Autor: Santander
   -- Input: p_
   --
   -- Output:
   --        p_CURSOR : SYS_REFCURSOR, resultados de consult cabecera.
   --        p_CURSOR2 : SYS_REFCURSOR, resultados de consulta detalle.
   --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_DETLL_CTCTE_BCCH_MN (p_CURSOR    OUT SYS_REFCURSOR,
                                     p_CURSOR2   OUT SYS_REFCURSOR,
                                     p_ERROR     OUT NUMBER);

   /************************************************************************************************
   -- Funcion/Procedimiento: SP_BUS_SALDO_OPRCN_MN
   -- Objetivo: Procedimiento almacenado que Muestra la posición del Banco Central, saldos
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_BIBLI_CAMPO_OBLTO_MT
   --
   -- Fecha: 16/06/17
   -- Autor: Santander
   -- Input: p_
   --
   -- Output:
   --        p_CURSOR : SYS_REFCURSOR, resultados de consulta.
   --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_BUS_SALDO_OPRCN_MN (p_CURSOR   OUT SYS_REFCURSOR,
                                    p_ERROR    OUT NUMBER);


   /************************************************************************************************
   -- Funcion/Procedimiento: SP_BUS_SALDO_AREA_MN
   -- Objetivo: Procedimiento almacenado que Muestra la posición del Banco Central, saldos
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_BIBLI_CAMPO_OBLTO_MT
   --
   -- Fecha: 16/06/17
   -- Autor: Santander
   -- Input: p_
   --
   -- Output:
   --        p_CURSOR : SYS_REFCURSOR, resultados de consulta.
   --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_BUS_SALDO_AREA_MN (p_CURSOR   OUT SYS_REFCURSOR,
                                   p_ERROR    OUT NUMBER);

   /************************************************************************************************
   -- Funcion/Procedimiento: SP_GES_CAJA_EGRESO_MN
   -- Objetivo: Procedimiento almacenado que actualiza saldos de Combanc o LBTR.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN
   --
   -- Fecha: 16/06/17
   -- Autor: Santander - CAH
   -- Input:
   --           p_NUM_REF_SWF->   Numero referencia Swift
   --           p_COD_SIS_SAL->   Sistema salida LBTR o Combanc
   -- Output:
   --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_GES_CAJA_EGRESO_MN (p_NUM_REF_SWF   IN     CHAR,
                                    p_ERROR            OUT NUMBER);

   /************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_CALCULA_SALDO_BILATERAL
   -- Objetivo: Procedimiento almacenado que actualiza saldos bilaterales para LBTR y Combanc
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_SALDO_BILAT
   --
   -- Fecha:14-11-2017
   -- Autor: Gonzalo Correa
   -- Input_
   --  p_MONTO     -->Monto de la operación
   --  p_BANCO_ORG_DET     -->Banco del movimiento
   --  p_FLAG_EGR_ING      -->Flag de ingreso o egreso
   --  p_CANAL_ENT_SAL     -->Canal del movimiento (LBTR o COMBANC)
   -- Output:
   --  p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_PAB_CALCULA_SALDO_BILATERAL (p_MONTO           IN     NUMBER,
                                             p_BANCO_ORG_DET   IN     CHAR,
                                             p_FLAG_EGR_ING    IN     NUMBER,
                                             p_CANAL_ENT_SAL   IN     CHAR,
                                             p_ERROR              OUT NUMBER);

   /************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_CALCULA_SALDO_CAJA
   -- Objetivo: Procedimiento almacenado que actualiza saldos de caja
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX
   --
   -- Fecha:14-11-2017
   -- Autor: Gonzalo Correa
   -- Input: P_IMP_ING_DIA    --> Monto Ingreso Altos Montos
   --        P_IMP_EGR_DIA    --> Montos Egreso Altos Montos
   --        P_IMP_ABN_BCT    --> Abonos Directos al central
   --        P_IMP_CGO_BCT    --> Cargos directos al central
   -- Output:
   --    p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_PAB_CAL_SLDO_POS_LBTR (P_IMP_ING_DIA   IN     NUMBER,
                                       P_IMP_EGR_DIA   IN     NUMBER,
                                       P_IMP_ABN_BCT   IN     NUMBER,
                                       P_IMP_CGO_BCT   IN     NUMBER,
                                       p_ERROR            OUT NUMBER);

   /************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_CAL_SLDO_POS_CMN
   -- Objetivo: Procedimiento almacenado que actualiza saldos de caja
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX
   --
   -- Fecha:14-11-2017
   -- Autor: Gonzalo Correa
   -- Input: P_IMP_ING_CBA    -->Monto ingreso combanc
   --        P_IMP_EGR_CBA    -->Monto egreso combanc
   -- Output:
   --    p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_PAB_CAL_SLDO_POS_CMN (P_IMP_ING_CBA   IN     NUMBER,
                                      P_IMP_EGR_CBA   IN     NUMBER,
                                      P_ERROR            OUT NUMBER);

   /************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_CAL_SLDO_MNT_SWF
   -- Objetivo: Procedimiento almacenado que actualiza saldos de caja
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX
   --
   -- Fecha:14-11-2017
   -- Autor: Gonzalo Correa
   -- Input: P_IMP_103    -->Monto 103
   --        P_IMP_202    -->Monto 202
   --        P_IMP_205    -->Monto 205
   --        P_IMP_900    -->Monto 900
   --        P_IMP_910    -->Monto 910
   -- Output:
   --    p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_PAB_CAL_SLDO_MNT_SWF (P_IMP_103        IN     NUMBER,
                                      P_IMP_202        IN     NUMBER,
                                      P_IMP_205        IN     NUMBER,
                                      P_IMP_900        IN     NUMBER,
                                      P_IMP_910        IN     NUMBER,
                                      p_FLAG_EGR_ING   IN     NUMBER,
                                      p_ERROR             OUT NUMBER);

   /************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_CAL_SLDO_POS_SINACOFI
   -- Objetivo: Procedimiento almacenado que actualiza saldos, de operaciones recibidas o enviadas
               por Sinacofi
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX
   --
   -- Fecha:17-11-2017
   -- Autor: Santander - CAH
   -- Input: P_IMP_ING_SNF    --> Monto Ingreso Sinacofi
   --        P_IMP_EGR_SNF    --> Montos Egreso Sinacofi
   -- Output:
   --    p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_PAB_CAL_SLDO_POS_SINACOFI (P_IMP_ING_SNF   IN     NUMBER,
                                           P_IMP_EGR_SNF   IN     NUMBER,
                                           p_ERROR            OUT NUMBER);

   -- ***************************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_GES_CNTGC_BCCH_IDA
   -- Objetivo: Procedimiento gestor que llama a pl actuliza operación por contingencia y actuliza saldo para caja MN
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: N/A
   -- Fecha: 21-11-2017
   -- Autor: Santander- CAH
   -- Input:
   -- p_REG_OPER -> parametro tipo registro que contiene el numero y fecha operacion
   -- p_COD_EST_AOS-> Estado de la operación
   -- p_COD_USR -> rut usuario
   -- p_DSC_GLB_BTC-> Glosa para bitacora de la operación.
   -- Output:
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ****************************************************************************************************************
   PROCEDURE Sp_PAB_GES_CNTGC_BCCH_IDA (p_REG_OPER      IN     REG_OPER,
                                        p_COD_EST_AOS   IN     CHAR,
                                        p_COD_USR       IN     VARCHAR2,
                                        p_DSC_GLB_BTC   IN     VARCHAR2,
                                        p_ERROR            OUT NUMBER);


   /************************************************************************************************
   -- Funcion/Procedimiento: SP_BUS_CALCULA_AREA_MN
   -- Objetivo: Procedimiento calcula los saldos por areas
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_SALDO_AREA
   --
   -- Fecha: 16/06/17
   -- Autor: GCORREA
   -- Input:
   --
   -- Output:
   --        p_CURSOR : SYS_REFCURSOR, resultados de consulta.
   --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_BUS_CALCULA_AREA_MN (P_COD_SIS_ENT_SAL   IN     CHAR,
                                     P_IMP_ING_ARA       IN     NUMBER,
                                     P_IMP_EGR_ARA       IN     NUMBER,
                                     p_ERROR                OUT NUMBER);

   /********************************************************************************************************
   FUNCION/PROCEDIMIENTO: SP_BUS_ACT_AREA_MN.
   OBJETIVO             : ACTUALIZA LOS SALDOS POR ÁREA CUANDO EXISTE UN CAMBIO DE CANAL (RESTA),
                          SOLO MT 900/910.
   SISTEMA              : DBO_PAB.
   BASE DE DATOS        : DBO_PAB.
   TABLAS USADAS        : PABS_DT_SALDO_AREA.
   FECHA                : 03/09/2018.
   AUTOR                : VARAYA.
   INPUT                : P_COD_SIS_ENT_SAL := CÓDIGO ENTRADA/SALIDA.
                          P_IMP_ING_ARA     := IMPUESTO INGRESO MT910.
                          P_IMP_EGR_ARA     := IMPUESTO EGRESO MT900.
   OUTPUT               : P_ERROR           := INDICADOR DE ERRORES:
                                               0 := SIN ERRORES.
                                               1 := CON ERRORES.
   OBSERVACIONES
   FECHA       USUARIO    DETALLE
   03-09-2018  VARAYA     CREACIÓN SP.
   ********************************************************************************************************/
   PROCEDURE SP_BUS_ACT_AREA_MN (P_COD_SIS_ENT_SAL   IN     CHAR,
                                 P_IMP_ING_ARA       IN     NUMBER,
                                 P_IMP_EGR_ARA       IN     NUMBER,
                                 p_ERROR                OUT NUMBER);

   /************************************************************************************************
   -- Funcion/Procedimiento: SP_REI_SALDO_MN
   -- Objetivo: Procedimiento que reinicia los saldos de mn
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX
   --
   -- Fecha: 24-11-2017
   -- Autor: Santander - CAH
   -- Input:
   --
   -- Output:
   --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_REI_SLDO_MN (p_ERROR OUT NUMBER);
END PKG_PAB_CAJA_MN;