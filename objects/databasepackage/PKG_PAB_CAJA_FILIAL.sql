CREATE OR REPLACE PACKAGE         PKG_PAB_CAJA_FILIAL
IS
   --
   v_NOM_PCK               VARCHAR2 (30) := 'PKG_PAB_CAJA_FILIAL';
   ERR_CODE                NUMBER := 0;
   ERR_MSG                 VARCHAR2 (300);
   v_DES                   VARCHAR2 (300);
   v_DES_BIT               VARCHAR2 (100);
   v_BANCO                 VARCHAR2 (9) := '970150005';
   p_s_mensaje             VARCHAR2 (400);
   TOPE_MSJ_BLQ            NUMBER := 300;
   V_FECHA_CUR             DATE := TRUNC (SYSDATE);
   v_dia_ini               DATE := TRUNC (SYSDATE) + INTERVAL '1' MINUTE;
   v_dia_fin DATE
         := TRUNC (SYSDATE) + INTERVAL '23' HOUR + INTERVAL '59' MINUTE ;

   --
   CURSOR C_FILIALES_PROYECCION (
      P_NUM_CTA_CTE                 VARCHAR2,
      P_NUM_DOC_FIL                 CHAR
   )
   IS
        SELECT   NUM_DOC_FIL, NUM_CTA_CTE
          FROM   PABS_DT_MVNTO_FLIAL
         WHERE       FEC_VTA_FIL >= V_FECHA_CUR
                 AND NUM_DOC_FIL = NVL (P_NUM_DOC_FIL, NUM_DOC_FIL)
                 AND NUM_CTA_CTE = NVL (P_NUM_CTA_CTE, NUM_CTA_CTE)
      GROUP BY   NUM_DOC_FIL, NUM_CTA_CTE;

   -------------------------------
   R_FILIALES_PROYECCION   C_FILIALES_PROYECCION%ROWTYPE;

   ----------------------------

   /************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_INS_PYN_MNT_FIL
   -- Objetivo: Procedimiento que inserta movimientos de filiales de WS TIBCO al cargar nómina
   -- de proyeccción de caja filiales
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_MVNTO_FLIAL
   -- Fecha: 20/09/17
   -- Autor: Santander
   -- Input:
   -- p_NUM_RUT_FIL  -> Rut Filial
   -- p_NUM_FOL_NMN  -> Folio Nómina
   -- p_COD_FIL_AOS  -> Código Filial
   -- p_NUM_CTA_CTE  -> Número de Cuenta Corriente
   -- p_COD_DVI      -> Moneda
   -- p_FEC_VTA_FIL  -> Fecha Valuta
   -- p_FLAG_FLJ_FIL -> Flag indicador Egreso o ingreso
   -- p_IMP_MONTO    -> Monto
   -- p_COD_CNP      -> Código Concepto
   -- p_GLS_CNP      -> Glosa  Concepto
   -- p_OBS_CNP      -> Observación Concepto
   -- p_COD_USR      -> Usuario Tibco
   -- p_COD_USR_SIS  -> Usuario Sistema
   -- p_COD_SIS_ENT  -> Canal
   -- Output:
   -- p_NUM_FOL_OPE  -> Folio Operación
   -- p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   -- GSD - OK
   PROCEDURE Sp_PAB_INS_PYN_MNT_FIL (p_NUM_FOL_NMN    IN     NUMBER,
                                     p_NUM_RUT_FIL    IN     VARCHAR,
                                     p_COD_FIL_AOS    IN     VARCHAR,
                                     p_NUM_CTA_CTE    IN     VARCHAR,
                                     p_COD_DVI        IN     VARCHAR,
                                     p_FEC_VTA_FIL    IN     DATE,
                                     p_FLAG_FLJ_FIL   IN     VARCHAR,
                                     p_IMP_MONTO      IN     NUMBER,
                                     p_COD_CNP        IN     VARCHAR,
                                     p_GLS_CNP        IN     VARCHAR,
                                     p_OBS_CNP        IN     VARCHAR,
                                     p_COD_USR        IN     VARCHAR,
                                     p_COD_USR_SIS    IN     VARCHAR,
                                     p_COD_SIS_ENT    IN     VARCHAR,
                                     p_NUM_FOL_OPE       OUT NUMBER,
                                     p_ERROR             OUT NUMBER);

   --**********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_PYN_RSM_CJA_FIL
   -- Objetivo: Retorna el resumen de proyecciones de caja filiales del día
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_ENCAB_CRTLA_FLIAL, PABS_DT_CAJA_FLIAL
   -- Fecha: 27/09/17
   -- Autor: Santander
   -- Input:
   -- N/A
   -- Output:
   -- p_CURSOR ->  Lista los filiales caja flilales
   -- p_ERROR  ->  Indicada si el procedimiento
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   --
   PROCEDURE SP_PAB_PYN_RSM_CJA_FIL (p_CURSOR   OUT SYS_REFCURSOR,
                                     p_ERROR    OUT NUMBER);



   --**********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_PYN_DELL_CJA_FIL
   -- Objetivo: Retorna el detalle de proyecciones de caja filiales
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_ENCAB_CRTLA_FLIAL, PABS_DT_CAJA_FLIAL
   -- Fecha: 27/09/17
   -- Autor: Santander
   -- Input:
   -- p_N_CTA    -> Núnmero cuenta
   -- p_NUM_DOC  -> Numero de documento de filial
   -- Output:
   -- p_CURSOR ->  Lista los filiales caja flilales
   -- p_ERROR  ->  Indicada si el procedimiento
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_PAB_PYN_DELL_CJA_FIL (p_N_CTA         IN     NUMBER,
                                      p_NUM_DOC       IN     CHAR,
                                      P_CABECERA         OUT SYS_REFCURSOR,
                                      P_MOVIMIENTOS      OUT SYS_REFCURSOR,
                                      p_ERROR            OUT NUMBER);


   --**********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_PYN_ELI_CJA_FIL
   -- Objetivo: Elimina registro de proyecciones de caja filiales según número de operación y su respectiva nómina
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_ENCAB_CRTLA_FLIAL, PABS_DT_CAJA_FLIAL
   -- Fecha: 27/09/17
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_NMN  -> Núnmero Folio
   -- p_NUM_FOL_OPE  -> Número Operación
   -- p_FEC_ING_MVT  -> Fecha Ingreso Movimiento
   -- Output:
   -- p_CURSOR ->  Lista los filiales caja flilales
   -- p_ERROR  ->  Indicada si el procedimiento
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_PAB_PYN_ELI_CJA_FIL (p_NUM_FOL_NMN   IN     NUMBER,
                                     p_NUM_FOL_OPE   IN     NUMBER,
                                     p_FEC_ING_MVT   IN     DATE,
                                     p_ERROR            OUT NUMBER);


   --**********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_PYN_CTL_CJA_FIL
   -- Objetivo: Retorna cartola de proyecciones de caja filiales del día
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_ENCAB_CRTLA_FLIAL, PABS_DT_CAJA_FLIAL
   -- Fecha: 27/09/17
   -- Autor: Santander
   -- Input:
   -- N/A
   -- Output:
   -- p_CURSOR ->  Lista los filiales caja flilales
   -- p_ERROR  ->  Indicada si el procedimiento
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_PAB_PYN_CTL_CJA_FIL (p_CURSOR   OUT SYS_REFCURSOR,
                                     p_ERROR    OUT NUMBER);


   --**********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_PYN_DELL_CTL_FIL
   -- Objetivo: Retorna el detalle de proyecciones de caja filiales
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_ENCAB_CRTLA_FLIAL, PABS_DT_CAJA_FLIAL
   -- Fecha: 27/09/17
   -- Autor: Santander
   -- Input:
   -- p_N_CTA        -> Núnmero cuenta
   -- p_NUM_EPS_FIL  -> Número empresa filial
   -- Output:
   -- p_CURSOR ->  Lista los filiales caja flilales
   -- p_ERROR  ->  Indicada si el procedimiento
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_PAB_PYN_DELL_CTL_FIL (p_N_CTA         IN     VARCHAR2,
                                      p_NUM_DOC_FIL   IN     CHAR,
                                      p_CABECERA         OUT SYS_REFCURSOR,
                                      p_DETALLE          OUT SYS_REFCURSOR,
                                      p_ERROR            OUT NUMBER);


   --**********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_CNA_CJA_FIL
   -- Objetivo: Retorna datos de las empresas filiales de Santander.
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_CAJA_FLIAL
   -- Fecha: 04/10/17
   -- Autor: Santander
   -- Input:
   -- N/A
   -- Output:
   -- p_CURSOR ->  Lista los filiales caja flilales
   -- p_ERROR  ->  Indicada si el procedimiento
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************POK
   PROCEDURE SP_PAB_CNA_CJA_FIL (p_CURSOR   OUT SYS_REFCURSOR,
                                 p_ERROR    OUT NUMBER);



   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_INS_CBCRA_CRTLA_FLIAL
   -- Objetivo: Procedimiento que inserta cabecera de Cartola Filiales desde WS Tibco desde la MW157 y 110.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_ENCAB_CRTLA_FLIAL, PABS_DT_CAJA_FLIAL
   -- Fecha: 17/10/17
   -- Autor: Santander
   -- Input:
   -- p_NUM_DOC_FIL  -> Rut
   -- p_NUM_CTA_CTE  -> Cuenta
   -- p_COD_DVI      -> Divisa
   -- p_FEC_CTL_CTA  -> Fecha Cartola
   -- p_GLS_TPO_CTL  -> Tipo Cartola
   -- p_FEC_INI_CTL  -> Fecha de inicio
   -- p_FEC_FNL_CTL  -> Fecha Final
   -- p_NUM_CTL_FIL  -> Número Cartola
   -- p_NUM_MVT_CTL  -> Número Movimiento
   -- p_IMP_INI_CTL  -> Saldo Incial
   -- p_IMP_TOT_ING  -> Saldo Total Ingreso
   -- p_IMP_TOT_EGR  -> Saldo Total Egreso
   -- p_IMP_FNL_CTL  -> Saldo Final
   -- p_IMP_RTN_CTL  -> Saldo Retención
   -- p_IMP_LCD_CTL  -> Saldo Línea
   -- p_IMP_LCD_USO  -> Saldo Línea Uso
   -- p_IMP_LCD_DIP  -> Saldo Línea Disponible
   -- p_IMP_TOT_DIP  -> Saldo Disponible
   -- p_ERROR
   -- Output:
   -- p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_INS_CBCRA_CRTLA_FLIAL (p_NUM_DOC_FIL   IN     VARCHAR2,
                                           p_NUM_CTA_CTE   IN     VARCHAR2,
                                           p_COD_DVI       IN     VARCHAR2,
                                           p_FEC_CTL_CTA   IN     DATE,
                                           p_GLS_TPO_CTL   IN     VARCHAR2,
                                           p_FEC_INI_CTL   IN     DATE,
                                           p_FEC_FNL_CTL   IN     DATE,
                                           p_NUM_CTL_FIL   IN     NUMBER,
                                           p_NUM_MVT_CTL   IN     NUMBER,
                                           p_IMP_INI_CTL   IN     NUMBER,
                                           p_IMP_TOT_ING   IN     NUMBER,
                                           p_IMP_TOT_EGR   IN     NUMBER,
                                           p_IMP_FNL_CTL   IN     NUMBER,
                                           p_IMP_RTN_CTL   IN     NUMBER,
                                           p_IMP_LCD_CTL   IN     NUMBER,
                                           p_IMP_LCD_USO   IN     NUMBER,
                                           p_IMP_LCD_DIP   IN     NUMBER,
                                           p_IMP_TOT_DIP   IN     NUMBER,
                                           p_ERROR            OUT NUMBER);


   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_INS_DETLL_CRTLA_FLIAL
   -- Objetivo: Procedimiento que inserta el detalle de cartola.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_CRTLA_FLIAL, PABS_DT_CAJA_FLIAL
   -- Fecha: 23/10/17
   -- Autor: Santander
   -- Input:
   -- P_NUM_CTL_FIL        -> Número de cartola
   -- p_FEC_CTL_CTA        -> Fecha de Cartola
   -- p_NUM_CTA_CTE        -> Número de cuenta
   -- p_NUM_MVT_FIL        -> Número de Movimiento
   -- p_NUM_DOC_FIL        -> Rut Filial
   -- p_CANT_MOV           -> Cantidad de Movimientos
   -- p_XML_MOV            -> XML con detalle de los movimientos
   -- Output:
   --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_INS_DETLL_CRTLA_FLIAL (P_NUM_CTL_FIL   IN     NUMBER,
                                           p_NUM_CTA_CTE   IN     VARCHAR2,
                                           p_FEC_CTL_CTA   IN     DATE,
                                           p_NUM_DOC_FIL   IN     CHAR,
                                           p_NUM_MVT_FIL   IN     NUMBER,
                                           p_CANT_MOV      IN     NUMBER,
                                           p_XML_MOV       IN     CLOB,
                                           p_ERROR            OUT NUMBER);


   --**********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_ELI_DETLL_CRTLA
   -- Objetivo: Elimina detalles de cartolas previo la inserción de la carga enviada por TIBCO al comienzo del día
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_CRTLA_FLIAL
   -- Fecha: 18/10/2017
   -- Autor: Santander
   -- Input:
   -- p_NUM_RUT_FIL
   -- p_NUM_CTA_CTE
   -- Output:
   -- p_ERROR  ->  Indicada si el procedimiento
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   -- GSD
   PROCEDURE SP_PAB_ELI_DETLL_CRTLA (p_NUM_RUT_FIL   IN     VARCHAR,
                                     p_NUM_CTA_CTE   IN     VARCHAR,
                                     p_ERROR            OUT NUMBER);

   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ACT_CRTLA_FLIAL
   -- Objetivo: Procedimiento que actualiza los montos de egreso e ingreso según la tabla de movimientos de proyección filiales.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_MVNTO_FLIAL, PABS_DT_ENCAB_CRTLA_FLIAL
   -- Fecha: 17/10/17
   -- Autor: Santander
   -- Input:
   -- p_NUM_CTA_CTE -> Número de cuenta corriente
   -- Output:
   -- p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   -- Pendiente de Análisis con Claudio F.
   PROCEDURE Sp_PAB_ACT_CRTLA_FLIAL (p_NUM_CTL_FIL   IN     NUMBER,
                                     p_NUM_CTA_CTE   IN     VARCHAR2,
                                     p_FEC_CTL_CTA   IN     DATE,
                                     p_NUM_DOC_FIL   IN     CHAR,
                                     p_ERROR            OUT NUMBER);



   --**********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_ACT_CAB_PYN_FIL
   -- Objetivo: Actualiza los saldos de la cabecera de filiales cuando se realiza una carga de nomina
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_ENCAB_CRTLA_FLIAL
   -- Fecha: 27/09/17
   -- Autor: Santander
   -- Input:
   -- N/A
   -- Output:
   -- p_NUM_RUT_FIL -> Número Rut Filiales
   -- p_NUM_CTA_CTE -> Número Cta Corriente
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_PAB_ACT_CAB_PYN_FIL (p_NUM_RUT_FIL   IN     VARCHAR,
                                     p_NUM_CTA_CTE   IN     VARCHAR,
                                     p_NUM_FOL_NMN   IN     NUMBER,
                                     p_ERROR            OUT NUMBER);
END PKG_PAB_CAJA_FILIAL;