CREATE OR REPLACE PACKAGE         PKG_PAB_LIQUIDEZ
IS
   /******************************************************************************
      NAME:       PKG_PAB_LIQUIDEZ
      PURPOSE:

      REVISIONS:
      Ver        Date        Author           Description
      ---------  ----------  ---------------  ------------------------------------
      1.0        15-02-2018      carcehu       1. Created this package.
   ******************************************************************************/
   v_NOM_PCK           VARCHAR2 (30) := 'PKG_PAB_LIQUIDEZ';
   ERR_CODE            NUMBER := 0;
   ERR_MSG             VARCHAR2 (300);
   v_DES               VARCHAR2 (300);
   v_DES_BIT           VARCHAR2 (100);
   v_BANCO             VARCHAR2 (9) := '970150005'; -- Identificador banco Santander
   p_s_mensaje         VARCHAR2 (400);

   TYPE T_NUM_FOL_NMN
   IS
      TABLE OF NUMBER
         INDEX BY PLS_INTEGER;        -- Arreglo numeros de foliador del banco

   v_FECHA_AYER        DATE := TRUNC (SYSDATE - 1);
   v_FECHA_HOY         DATE := TRUNC (SYSDATE);
   v_DIA_INI           DATE := TRUNC (SYSDATE) + INTERVAL '1' MINUTE;
   v_DIA_FIN DATE
         := TRUNC (SYSDATE) + INTERVAL '23' HOUR + INTERVAL '59' MINUTE ;
   v_COD_PAIS          CHAR (2) := 'SN';
   v_motivo_eli        VARCHAR2 (70);
   v_FECHA_HABIL       DATE;
   v_HORA_INI_GRF      CHAR (4) := '0800';
   v_HORA_FIN_GRF      CHAR (4) := '1900';

   ----*********************Declaracion Cursor C_CONSULTA_MONEDA************************
   --    CURSOR C_CONSULTA_MONEDA
   --    IS
   --
   --         SELECT COD_DVI
   --            FROM PABS_DT_DETLL_SALDO_CAJA_MN_MX
   --            --WHERE COD_DVI NOT IN  'CLP'
   --            GROUP BY COD_DVI
   --            ORDER BY COD_DVI;
   --
   --    R_CONSULTA_MONEDA  C_CONSULTA_MONEDA%ROWTYPE;

   -- --*********************Declaracion Cursor C_CONSULTA_BANCOS************************
   CURSOR C_CONSULTA_BANCOS
   IS
        SELECT   SD.COD_BCO_ORG AS BANCO, SD.COD_DVI AS MONEDA
          FROM   PABS_DT_DETLL_SALDO_CAJA_MN_MX SD
         WHERE   SD.COD_DVI <> 'CLP'
      GROUP BY   SD.COD_DVI, SD.COD_BCO_ORG
      ORDER BY   SD.COD_BCO_ORG ASC;

   R_CONSULTA_BANCOS   C_CONSULTA_BANCOS%ROWTYPE;


   --*********************Declaracion Cursor C_BANCOS_LBTR************************
   CURSOR C_BANCOS_LBTR
   IS
        SELECT   SB.COD_BIC_BCO AS BANCO
          FROM   PABS_DT_SALDO_BILAT SB
         WHERE   SB.COD_SIS_SAL = PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_LBTR
                 AND SB.FEC_ING_CTL =
                       (SELECT   MAX (SB.FEC_ING_CTL)
                          FROM   PABS_DT_SALDO_BILAT SB
                         WHERE   SB.COD_SIS_SAL =
                                    PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_LBTR)
      ORDER BY   SB.COD_BIC_BCO ASC;

   R_BANCOS_LBTR       C_BANCOS_LBTR%ROWTYPE;

   --*********************Declaracion Cursor C_BANCOS_CMN************************
   CURSOR C_BANCOS_CMN
   IS
        SELECT   SB.COD_BIC_BCO AS BANCO
          FROM   PABS_DT_SALDO_BILAT SB
         WHERE   SB.COD_SIS_SAL = PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_CMN
                 AND SB.FEC_ING_CTL =
                       (SELECT   MAX (SB.FEC_ING_CTL)
                          FROM   PABS_DT_SALDO_BILAT SB
                         WHERE   SB.COD_SIS_SAL =
                                    PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_CMN)
      ORDER BY   SB.COD_BIC_BCO ASC;


   R_BANCOS_CMN        C_BANCOS_CMN%ROWTYPE;

   --*******************************************************************************


   /********************************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_GES_LIQDZ_ITDIA
   -- Objetivo: Procedimiento almacenado gestor que se ejecuta cada minuto para el registro
   --            de saldos necesarios de Liquidez Intradia
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas:
   --
   -- Fecha: 03-09-2018
   -- Autor: Santander CAH
   -- Input:--
   --
   -- Output:
   --      p_ALERTA  Indicador de alerta
   --      p_ASUNTO  Asunto del mensaje
   --      p_CORREO  Correo de destino
   --      p_MENSAJE Mensaje de alerta
   --       p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de últimos cambios>
   --*******************************************************************************************************************/
   PROCEDURE SP_PAB_GES_LIQDZ_ITDIA (p_ALERTA    OUT NUMBER,
                                     p_ASUNTO    OUT VARCHAR2,
                                     p_CORREO    OUT VARCHAR2,
                                     p_MENSAJE   OUT VARCHAR2,
                                     p_ERROR     OUT NUMBER);

   /**********************************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_REC_SALDO_ITDIA
   -- Objetivo: Procedimiento almacenado  que recorre las monedas y llamando a otros SP para inserción de data
         en las tablas que contienen la data de gráficos de Liquidez Intradia
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas:
   --
   -- Fecha: 24-07-2018
   -- Autor: Santander CAH
   -- Input:
   --
   --
   -- Output:
   --
   --       p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de últimos cambios>
   --********************************************************************************************************************/
   PROCEDURE SP_PAB_REC_SALDO_ITDIA (p_ERROR OUT NUMBER);

   /*********************************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_REC_PAGOS_ACMDO
   -- Objetivo: Procedimiento almacenado recorre las monedas  para insertar los pagos acumulados
              por hora.
   -- Sistema: DBO_PAB.
   -- Base de Datos:   DBO_PAB.
   -- Tablas Usadas:   PABS_DT_RGTRO_PAGOS_ACMDO
   --                  PABS_DT_DETLL_SALDO_CAJA_MN_MX
   --
   -- Fecha: 08/08/2018
   -- Autor: Santander CAH
   -- Input:
   -- Output:
   --       p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de últimos cambios>
   --*********************************************************************************************************************/
   PROCEDURE SP_PAB_REC_PAGOS_ACMDO (p_ERROR OUT NUMBER);

   /**********************************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_INS_SALDO_POR_MONDA
   -- Objetivo: Procedimiento almacenado que registra saldos Intradia por moneda, e insertando los registros
             a medida que recibe la moneda
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX
   --                PABS_DT_RGTRO_SALDO_ITDIA
   --
   -- Fecha: 07/08/2018
   -- Autor: Santander CAH
   -- Input:
   --      p_COD_DVI      Moneda para actualizar.
   -- Output:
   --       p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de últimos cambios>
   --********************************************************************************************************************/
   PROCEDURE Sp_PAB_INS_SALDO_POR_MONDA (p_COD_DVI   IN     CHAR,
                                         p_ERROR        OUT NUMBER);

   /*********************************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_INS_PAGOS_ACMDO_MONDA
   -- Objetivo: Procedimiento almacenado que inserta los pagos acumulados por hora, en moneda nacional.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_RGTRO_PAGOS_ACMDO , PABS_DT_DETLL_SALDO_CAJA_MN_MX
   --
   -- Fecha: 08/08/2018
   -- Autor: Santander CAH
   -- Input:
   --      p_COD_DVI  Moneda a consultar e insertar movimiento
   -- Output:
   --       p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de últimos cambios>
   --*******************************************************************************************************************/
   PROCEDURE SP_PAB_INS_PAGOS_ACMDO_MONDA (p_COD_DVI   IN     CHAR,
                                           p_ERROR        OUT NUMBER);

   /********************************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_REC_SALDO_LBTR
   -- Objetivo: Procedimiento almacenado recorre las monedas  para insertar los pagos acumulados
              por hora.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_RGTRO_PAGOS_ACMDO
   --
   -- Fecha: 08/08/2018
   -- Autor: Santander CAH
   -- Input:
   -- Output:
   --       p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de últimos cambios>
   --*******************************************************************************************************************/
   PROCEDURE SP_PAB_REC_SALDO_LBTR (p_ERROR OUT NUMBER);

   /********************************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_REC_SALDO_MX
   -- Objetivo: Procedimiento almacenado recorre los banco corresponsales.
               por hora.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_RGTRO_PAGOS_ACMDO
   --
   -- Fecha: 08/08/2018
   -- Autor: Santander CAH
   -- Input:
   -- Output:
   --       p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de últimos    cambios>
   --*******************************************************************************************************************/
   PROCEDURE SP_PAB_REC_SALDO_MX (p_ERROR OUT NUMBER);

   /********************************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_REC_SALDO_CMN
   -- Objetivo: Procedimiento almacenado recorre las monedas  para insertar los pagos acumulados
              por hora.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_RGTRO_PAGOS_ACMDO
   --
   -- Fecha: 08/08/2018
   -- Autor: Santander CAH
   -- Input:
   -- Output:
   --       p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de últimos cambios>
   --*******************************************************************************************************************/
   PROCEDURE SP_PAB_REC_SALDO_CMN (p_ERROR OUT NUMBER);

   /*********************************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_INS_SALDO_LIQDZ_BCCH
   -- Objetivo: Procedimiento almacenado que inserta Registros de saldos, para la tabla de consulta Saldos
   -- Sistema: DBO_PAB.
   -- Base de Datos: PABS_DT_, PABS_DT_
   -- Tablas Usadas:
   --
   -- Fecha: 17-08-2018
   -- Autor: Santander CAH
   -- Input:
   --
   -- Output:
   --      p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de últimos cambios>
     --******************************************************************************************************************/
   PROCEDURE SP_PAB_INS_SALDO_LIQDZ_BCCH (p_ERROR OUT NUMBER);

   /*********************************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_BUS_ERRO_BTCOR
   -- Objetivo: Procedimiento almacenado que muestra si un error esta ingresado en bitacora.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_BTCOR_BASE_DATO_ALMOT
   --
   -- Fecha: 11-03-2019
   -- Autor: Santander CAH
   -- Input:
   --      p_ERR_BTCOR     Código del error en bitácora
   --      p_NOM_PCK       Nombre del package
   -- Output:
   --       P_FECHA_INS
   --       p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de últimos cambios>
   --*******************************************************************************************************************/
   PROCEDURE SP_PAB_BUS_ERRO_BTCOR (p_ERR_BTCOR   IN     CHAR,
                                    p_NOM_PCK     IN     VARCHAR2,
                                    P_FECHA_INS      OUT TIMESTAMP,
                                    p_ERROR          OUT NUMBER);

   /*******************************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_INS_SALDO_LIQDZ
   -- Objetivo: Procedimiento almacenado que inserta Registros de saldos, para la tabla de consulta Saldos
   -- Sistema: DBO_PAB.
   -- Base de Datos:   DBO_PAB
   -- Tablas Usadas:   PABS_DT_SALDO_BILAT, PABS_DT_DETLL_SALDO_CAJA_MN_MX,
   --                  PABS_DT_RGTRO_SALDO_HTRCO
   --
   -- Fecha: 06-11-2018
   -- Autor: Santander CAH
   -- Input:
   --      p_BANCO  Banco para actualizar
   --      p_CANAL Canal de salida
   -- Output:

   --      p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de últimos cambios>
      --*****************************************************************************************************************/
   PROCEDURE SP_PAB_INS_SALDO_LIQDZ (p_BANCO   IN     CHAR,
                                     p_CANAL   IN     CHAR,
                                     p_ERROR      OUT NUMBER);

   /**********************************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_INS_SALDO_MX_LIQDZ
   -- Objetivo: Procedimiento almacenado que inserta Registros de saldos  de moneda extranjera
                , para la tabla de consulta Saldos
   -- Sistema: DBO_PAB.
   -- Base de Datos: PABS_DT_DETLL_SALDO_CAJA_MN_MX
                     PABS_DT_RGTRO_SALDO_HTRCO
   -- Tablas Usadas:
   --
   -- Fecha: 17-08-2018
   -- Autor: Santander CAH
   -- Input:
   --      p:BANCO     Banco a buscar
   --      p_MONEDA    Moneda a buscar, para insertar saldo
   -- Output:

   --      p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de últimos cambios>
   --********************************************************************************************************************/
   PROCEDURE SP_PAB_INS_SALDO_MX_LIQDZ (p_BANCO     IN     CHAR,
                                        p_COD_DVI   IN     CHAR,
                                        p_ERROR        OUT NUMBER);

   /*********************************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_RGTRO_SALDOS_DARIO
   -- Objetivo: Procedimiento almacenado que Busca los top montos máximos, mínimos, luego
   --          registra en la tabla  PABS_DT_RGTRO_MAXMO_MINMO_DIA, al cierre del sistena.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_SALDO_HORA_ITDIA, PABS_DT_RGTRO_MAXMO_MINMO_DIA, PABS_DT_SALDO_MONDA_DARIO
   --
   -- Fecha: 09/08/2018
   -- Autor: Santander CAH
   -- Input:
   --      p_COD_DVI      Moneda para actualizar.
   -- Output:
   --       p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de últimos  cambios>
   --********************************************************************************************************************/
   PROCEDURE SP_PAB_RGTRO_SALDOS_DARIO (p_COD_DVI   IN     CHAR,
                                        p_ERROR        OUT NUMBER);

   /*********************************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_REC_ALERTA_MONEDA
   -- Objetivo: Procedimiento almacenado que recorre monedas y retorna si hay una alerta.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX
   --
   -- Fecha: 31-01-2019
   -- Autor: Santander CAH
   -- Input:
   --
   -- Output:
   --      p_ALERTA    --> Valor de alerta (0 no alertar, 1 si)
   --      p_ASUNTO    --> Asunto para notificar
   --      p_CORREO    --> Correo para notificar
   --      p_MENSAJE   --> Mensaje de la notificación
   --       p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de últimos  cambios>
   --********************************************************************************************************************/
   PROCEDURE SP_PAB_REC_ALERTA_MONEDA (p_ALERTA    OUT NUMBER,
                                       p_ASUNTO    OUT VARCHAR2,
                                       p_CORREO    OUT VARCHAR2,
                                       p_MENSAJE   OUT VARCHAR2,
                                       p_ERROR     OUT NUMBER);

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_ALRTA_SALDO
   -- Objetivo: Procedimiento que retorna si existe alerta por Saldo cerca de loas limites
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas:  PABS_DT_SALDO_HORA_ITDIA, PABS_DT_MONDA_ALMOT, PABS_DT_NTFCC_ALMOT
   -- Fecha: 08/10/2018
   -- Autor: Santander CAH
   -- Input:
   --      p_MONEDA    --> Moneda para buscar saldo
   --     P_TIP_ALRTA    --> Flag indicador de alerta
   -- Output:
   --      p_ALERTA    --> Valor de alerta (0 no alertar, 1 si)
   --      p_ASUNTO    --> Asunto para notificar
   --      p_CORREO    --> Correo para notificar
   --      p_MENSAJE   --> Mensaje de la notificación
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de últimos   cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_ALRTA_SALDO (p_MONEDA      IN     CHAR,
                                 P_TIP_ALRTA   IN     CHAR DEFAULT NULL ,
                                 p_ALERTA         OUT NUMBER,
                                 p_ASUNTO         OUT VARCHAR2,
                                 p_CORREO         OUT VARCHAR2,
                                 p_MENSAJE        OUT VARCHAR2,
                                 p_ERROR          OUT NUMBER);

   /************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_NTFCC_SALDO
   -- Objetivo: Procedimiento almacenado que retorna el cuerpo del correo para la notificación
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_CRREO_ALMOT
   --
   -- Fecha: 07/02/2019
   -- Autor: Santander CAH
   -- Input:
   --   p_COD_TPO_OPE     Descripción de la notificación
   --   p_SALDO
   --   p_HORA
   --   p_MONEDA
   -- Output:
   --     MENSAJE_SWF         Contenido de mensaje
   --     p_ASUNTO           Asunto correo
   --     p_CORREO          Dirección de correo
   --     p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de últimos  cambios>
   --***********************************************************************************************/
   PROCEDURE SP_PAB_NTFCC_SALDO (p_COD_TPO_OPE   IN     CHAR,
                                 p_SALDO         IN     VARCHAR2,
                                 p_HORA          IN     VARCHAR2,
                                 p_MONEDA        IN     VARCHAR2,
                                 MENSAJE_SWF        OUT VARCHAR2,
                                 p_ASUNTO           OUT VARCHAR2,
                                 p_CORREO           OUT VARCHAR2,
                                 p_ERROR            OUT NUMBER);


   /****************************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_BUS_MONIT_MONEDA
   -- Objetivo: Procedimiento almacenado para gráficos Liquidez intradia
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas:  PABS_DT_SISTM_ENTRD_SALID
   --                 PABS_DT_SALDO_HORA_ITDIA
   --                 PABS_DT_MONDA_ALMOT
   --
   -- Fecha: 06-02-2018
   -- Autor: Santander CAH
   -- Input:
   --      p_MONEDA     Moneda a buscar
   -- Output:
   --          p_HORARIO    Cursor con data parametrica para gráfico
   --          p_GRAFICO    Cursor con data para gráfico
   --          p_ERROR:      Indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --****************************************************************************************************************/
   PROCEDURE SP_PAB_BUS_MONIT_MONEDA (p_MONEDA    IN     CHAR,
                                      p_HORARIO      OUT SYS_REFCURSOR,
                                      p_GRAFICO      OUT SYS_REFCURSOR,
                                      p_ERROR        OUT NUMBER);

   /****************************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_BUS_DETLL_MONEDA
   -- Objetivo: Procedimiento almacenado para gráficos Liquidez intradia
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX, PABS_DT_CBCRA_CRTLA
   --
   -- Fecha: 06-02-2018
   -- Autor: Santander CAH
   -- Input:
   --
   -- Output:
   --          p_DETALLE    Cursor con data parametrica para gráfico
   --          p_ERROR:      Indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --****************************************************************************************************************/
   PROCEDURE SP_PAB_BUS_DETLL_MONEDA (p_DETALLE   OUT SYS_REFCURSOR,
                                      p_ERROR     OUT NUMBER);

   /*********************************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_BUS_DETLL_MONEDA_CLP
   -- Objetivo: Procedimiento almacenado que retorna los datos para gráfica CLP (Monitor moneda Nacional)
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas:  PABS_DT_DETLL_SALDO_CAJA_MN_MX
   --                    PABS_DT_SALDO_BILAT
   --                   TCDTBAI
   --
   -- Fecha: 14-02-2019
   -- Autor: Santander CAH
   -- Input:
   --
   -- Output:
   --                p_SALDO_REAL      Cursor con data saldos real CLP
   --                p_DETLL_BCCH      Cursor con data Detalle del lbtr
   --                p_DETLL_CMN       Cursor con data Detalle del combanc
   --                p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --********************************************************************************************************************/
   PROCEDURE SP_PAB_BUS_DETLL_MONEDA_CLP (p_SALDO_REAL   OUT SYS_REFCURSOR,
                                          p_ALERTA       OUT SYS_REFCURSOR,
                                          p_DETLL_BCCH   OUT SYS_REFCURSOR,
                                          p_DETLL_CMN    OUT SYS_REFCURSOR,
                                          p_ERROR        OUT NUMBER);

   --***************************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_BUS_CONVERSION
   -- Objetivo: Función que consulta el valor de conversión de una moneda en USD.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas:
   -- Fecha: 07-02-2019
   -- Autor: CAH
   -- Input:
   --      p_MONEDA    Moneda a consultar
   -- N/A
   -- Output:
   -- N/A.
   -- Input/Output:
   -- N/A.
   -- Retorno:
   --      v_VALOR -> monto conversión de la moneda consultada.
   -- Observaciones: <Fecha y Detalle de últimos cambios>
   --****************************************************************************************************
   FUNCTION FN_PAB_BUS_CONVERSION (p_FECHA        IN DATE,
                                   p_MONEDA_USD   IN VARCHAR2,
                                   p_MONEDA_VAR   IN VARCHAR2)
      RETURN NUMBER;

   /******************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_SALDO_NETO
   -- Objetivo: Procedimiento almacenado que Muestra los datos para gráfica de saldos moneda CLP
   -- Sistema: DBO_PAB.
   -- Base de Datos:  DBO_PAB.
   -- Tablas Usadas:  PABS_DT_SALDO_MONDA_DARIO,PABS_DT_SISTM_ENTRD_SALID
   --
   -- Fecha: 21-01-2019
   -- Autor: Santander CAH
   -- Input:
   --      p_MONEDA           Moneda a consultar
   --
   -- Output:
   --       p_HORARIO         Cursor con data de horario de gráfico
   --       p_SALDONET        Cursor con data gráfico
   --       p_ERROR           indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de últimos    cambios>
   --*****************************************************************************************************/
   PROCEDURE SP_PAB_SALDO_NETO (p_MONEDA     IN     CHAR,
                                p_HORARIO       OUT SYS_REFCURSOR,
                                p_SALDONET      OUT SYS_REFCURSOR,
                                p_ERROR         OUT NUMBER);

   /******************************************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_RESUMEN_SALDO_NETO
   -- Objetivo: Procedimiento almacenado que Muestra los datos para gráfica de saldos moneda CLP
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas:   PABS_DT_SALDO_MONDA_DARIO
   --
   -- Fecha: 21-01-2019
   -- Autor: Santander CAH
   -- Input:
   --      p_FLG_MES       Flag para indicar mes de búsqueda
   --      p_MONEDA        Moneda a consultar
   -- Output:
   --       p_TOP         Cursor con top de cada mes
   --      p_RESUMEN       Cursor con resumen del mes
   --       p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de últimos    cambios>
   --******************************************************************************************************************************/
   PROCEDURE SP_PAB_RESUMEN_SALDO_NETO (p_FLG_MES   IN     NUMBER,
                                        p_MONEDA    IN     CHAR,
                                        p_TOP          OUT SYS_REFCURSOR,
                                        p_RESUMEN      OUT SYS_REFCURSOR,
                                        p_ERROR        OUT NUMBER);

   /******************************************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_BUS_ING_EGR
   -- Objetivo: Procedimiento almacenado que Muestra los datos para gráfica de saldos moneda CLP
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas:  PABS_DT_SALDO_MONDA_DARIO
   --
   -- Fecha: 28-08-2018
   -- Autor: Santander CAH
   -- Input:
   --      p_MONEDA        Moneda a consultar
   --
   -- Output:
   --       p_HORARIO       Cursor con data gráfico
   --       p_ING_EGR       Cursor con data de máximo actual
   --       p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   --
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de últimos    cambios>
   --******************************************************************************************************************************/
   PROCEDURE SP_PAB_BUS_ING_EGR (p_MONEDA    IN     CHAR,
                                 p_HORARIO      OUT SYS_REFCURSOR,
                                 p_ING_EGR      OUT SYS_REFCURSOR,
                                 p_ERROR        OUT NUMBER);


   /******************************************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_RESUMEN_ING_EGR
   -- Objetivo: Procedimiento almacenado que Muestra los datos para gráfica de Ingresos y Egresos
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas:  PABS_DT_SALDO_MONDA_DARIO
   --
   -- Fecha: 28-01-2019
   -- Autor: Santander CAH
   -- Input:
   --      p_FLG_MES       Flag para indicar mes de búsqueda
   --      p_MONEDA        Moneda a consultar
   -- Output:
   --       p_TOP         Cursor con top de cada mes
   --      p_RESUMEN       Cursor con resumen del mes
   --       p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de últimos    cambios>
   --******************************************************************************************************************************/
   PROCEDURE SP_PAB_RESUMEN_ING_EGR (p_FLG_MES   IN     NUMBER,
                                     p_MONEDA    IN     CHAR,
                                     p_TOP          OUT SYS_REFCURSOR,
                                     p_RESUMEN      OUT SYS_REFCURSOR,
                                     p_ERROR        OUT NUMBER);

   --***********************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_OBT_MAX_EGR
   -- Objetivo: Funcion que devuelve MAX EGRESOS
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_SALDO_MONDA_DARIO
   -- Fecha: 30-05-2019
   -- Autor: CAH
   -- Input:
   --      p_MONEDA    -> Moneda
   --      p_FLG_MES   -> Hora a consultar
   --      p_FLUJO     -> Flujo de ingreso o egreso
   --      p_MAX_MIX   -> Flag de max o min.
   --      p_MONTO     -> MOnto anterior
   -- Output:
   -- N/A.
   -- Input/Output: N/A
   -- Retorno:
   --      v_MONTO -> monto de la moneda consultada.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   FUNCTION FN_PAB_OBT_TOP_ING_EGR (p_MONEDA    IN CHAR,
                                    p_FLG_MES   IN NUMBER,
                                    p_FLUJO     IN NUMBER,
                                    p_MAX_MIX   IN NUMBER,
                                    p_MONTO     IN NUMBER)
      RETURN NUMBER;

   --***********************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_BUS_TRAMO_PRMDO
   -- Objetivo: Consulta el monto acumulado de una moneda, según la hora
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas: PABS_DT_PAGOS_RLZAR_ACMDO
   -- Fecha: 20-09-2018
   -- Autor: CAH
   -- Input:
   --      p_FECHA         -> Fecha a consultar
   --      p_HORA          -> Hora a consultar
   --      p_MONEDA        -> Moneda
   --      p_FLG_EGR_ING   -> Flag de ingreso.
   -- Output:
   -- N/A.
   -- Input/Output: N/A
   -- Retorno:
   --      v_MONTO_HORA -> monto de la moneda consultada.
   -- Observaciones: <Fecha y Detalle de últimos    cambios>
   --***********************************************************************************************
   FUNCTION FN_PAB_BUS_TRAMO_PRMDO (p_FECHA         IN DATE,
                                    p_HORA          IN NUMBER,
                                    p_MONEDA        IN CHAR,
                                    p_FLG_EGR_ING   IN NUMBER)
      RETURN NUMBER;

   --***********************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_BUS_MONTO_MAX_DIA
   -- Objetivo: Función que consulta el monto máximo del dia de una moneda, segun fecha
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas: PABS_DT_PAGOS_RLZAR_ACMDO
   -- Fecha: 20-09-2018
   -- Autor: CAH
   -- Input:
   --      p_FECHA     -> Fecha a consultar
   --      p_MONEDA    -> Moneda
   -- N/A
   -- Output:
   -- N/A.
   -- Input/Output:
   -- N/A.
   -- Retorno:
   --      v_MONTO_HORA -> monto de la moneda consultada.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   --***********************************************************************************************
   FUNCTION FN_PAB_BUS_MONTO_MAX_DIA (p_FECHA IN DATE, p_MONEDA IN CHAR)
      RETURN NUMBER;

   /******************************************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_BUS_PRMDO
   -- Objetivo: Procedimiento almacenado que muestra los datos de grafica Promedios
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas:  PABS_DT_PAGOS_RLZAR_ACMDO
   --
   -- Fecha: 28-01-2019
   -- Autor: Santander CAH
   -- Input:
   --      p_FLG_EGR_ING   Flag para indicar mes de búsqueda
   --      p_MONEDA        Moneda a consultar
   --
   -- Output:
   --       p_PROMEDIO    Cursor con detalle de grafica promedios
   --       p_ERROR       indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de últimos    cambios>
   --******************************************************************************************************************************/
   PROCEDURE SP_PAB_BUS_PRMDO (p_FLG_EGR_ING   IN     NUMBER,
                               p_MONEDA        IN     CHAR,
                               p_PROMEDIO         OUT SYS_REFCURSOR,
                               p_ERROR            OUT NUMBER);

   /********************************************************************************************************
   --FUNCION/PROCEDIMIENTO: FN_PAB_BUS_TRAMO.
   --OBJETIVO             : retorna tramos de media hora para la gráfica de productos.
   --SISTEMA              : DBO_PAB.
   --BASE DE DATOS        : DBO_PAB.
   --TABLAS USADAS        : PABS_DT_DETLL_OPRCN.
   --FECHA                : 09/11/18
   --AUTOR                : CAH.
   --INPUT                : P_NFOLIO := número folio.
   --OUTPUT               : v_TRAMO  := valor de retorno.
   --OBSERVACIONES
   --FECHA       USUARIO    DETALLE
   --11/02/18    VAR        Se modifica función.
   ********************************************************************************************************/
   FUNCTION FN_PAB_BUS_TRAMO (P_NFOLIO NUMBER)
      RETURN VARCHAR2;

   --***********************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_BUS_TRAMO_MONTO
   -- Objetivo: Función que retorna tramos de media hora para la grafica de produtos
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas: PABS_DT_DETLL_MVNTO_CAJA
   --                 PABS_DT_TIPO_OPRCN_ALMOT
   --                 PABS_DT_TIPO_SUB_PRDTO
   -- Fecha: 09/11/2016
   -- Autor: CAH
   -- Input:
   --      p_FECHA -> Fecha y hora de consulta.
   -- N/A
   -- Output:
   -- N/A.
   -- Input/Output:
   -- N/A.
   -- Retorno:
   -- v_TRAMO -> valor de retorno
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   FUNCTION FN_PAB_BUS_TRAMO_MONTO (p_COD_OPE IN CHAR)
      RETURN NUMBER;

   --***************************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_BUS_MONTO_BANCO
   -- Objetivo: Función que retorna el monto de ingreso o egreso de  un banco, por hora.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN
   --                PABS_DT_TIPO_OPRCN_ALMOT
   --                TCDTBAI
   --                PABS_DT_TIPO_SUB_PRDTO
   -- Fecha: 22-11-2018
   -- Autor: CAH
   -- Input:
   --      p_BANCO_ING     Fecha a cosultar
   --      p_FLAG          Hora a consultar
   --      p_COD_OPE       Código de operación
   --      p_HORA          Hora a consultar
   -- N/A
   -- Output:
   -- N/A.
   -- Input/Output:
   -- N/A.
   -- Retorno:
   --      v_MONTO_HORA -> monto de la moneda consultada.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   --******************************************************************************************************
   FUNCTION FN_PAB_BUS_MONTO_BANCO (p_BANCO_ING   IN CHAR,
                                    p_MONEDA      IN CHAR,
                                    p_FLAG        IN NUMBER,
                                    p_COD_OPE     IN CHAR,
                                    p_HORA        IN NUMBER)
      RETURN NUMBER;

   /********************************************************************************************************
   --Funcion/Procedimiento: SP_PAB_BUS_MVNTO_PRDTO
   --Objetivo             : Muestra datos para gráfica de productos en moneda nacional.
   --Sistema              : DBO_PAB.
   --Base De Datos        : DBO_PAB.
   --Tablas Usadas        : PABS_DT_DETLL_OPRCN
   --                      PABS_DT_TIPO_OPRCN_ALMOT
   --                      PABS_DT_TIPO_SUB_PRDTO
   --                      TCDTBAI

   --Fecha                : 29/08/18
   --Autor                : CAH.
   --Input                : P_COD_DVI   := MONEDA.
   --Output               : P_NETO_PROD := NETO PRODUCTOS.
   --                     P_ERROR     := INDICADOR DE ERRORES.
   --OBSERVACIONES
   --Fecha       Usuario    Detalle
   --11/02/18    VAR        Se Modifica Cursor.
   ********************************************************************************************************/
   PROCEDURE SP_PAB_BUS_MVNTO_PRDTO (p_COD_DVI     IN     CHAR,
                                     p_NETO_PROD      OUT SYS_REFCURSOR,
                                     p_ERROR          OUT NUMBER);

   /**********************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_BUS_DETLL_MVNTO_PRDTO
   -- Objetivo: Procedimiento almacenado que muestra el detalle de productos
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN, PABS_DT_TIPO_OPRCN_ALMOT
   --
   -- Fecha: 22/10/2018
   -- Autor: Santander CAH
   -- Input:
   --      p_COD_PROD      -> Código de producto a mostrar.
   -- Output:
   --      p_DETLL_PRDTO   -> Cursor con detalle de productos
   --      p_ERROR         -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de últimos    cambios>
   --********************************************************************************************************/
   PROCEDURE SP_PAB_BUS_DETLL_MVNTO_PRDTO (
      p_COD_PROD      IN     CHAR,
      p_DETLL_PRDTO      OUT SYS_REFCURSOR,
      p_ERROR            OUT NUMBER
   );

   --**************************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_PAGO_REALIZADO
   -- Objetivo: Función que consulta el monto de pagos realizados acumulados por producto
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN
   --                  PABS_DT_TIPO_SUB_PRDTO
   --                  PABS_DT_TIPO_OPRCN_ALMOT
   -- Fecha: 28-01-2019
   -- Autor: Santander - CAH
   -- Input:
   --      p_COD_OPE     -> Fecha a cosultar
   --      p_MONEDA      -> Hora a consultar
   -- N/A
   -- Output:
   --      v_PAGO      -> Monto acumulado del prodcuto
   -- Input/Output:
   -- N/A.
   -- Retorno:
   --      v_MONTO_HORA -> monto de la moneda consultada.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   --**************************************************************************************************
   FUNCTION FN_PAB_PAGO_REALIZADO (p_COD_OPE IN CHAR, p_MONEDA IN CHAR)
      RETURN NUMBER;

   --**************************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_PAGO_PENDIENTE
   -- Objetivo: Función que consulta el monto de pagos pendientes(proyección) acumulados por producto
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas: PABS_DT_DETLL_MVNTO_CAJA
   --                  PABS_DT_TIPO_SUB_PRDTO
   --                  PABS_DT_TIPO_OPRCN_ALMOT
   -- Fecha: 28-01-2019
   -- Autor: Santander - CAH
   -- Input:
   --      p_COD_OPE     -> Fecha a cosultar
   --      p_MONEDA      -> Hora a consultar
   -- N/A
   -- Output:
   --      v_PAGO      -> Monto acumulado del prodcuto
   -- Input/Output:
   -- N/A.
   -- Retorno:
   --      v_MONTO_HORA -> monto de la moneda consultada.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   --**************************************************************************************************
   FUNCTION FN_PAB_PAGO_PENDIENTE (p_COD_OPE IN CHAR, p_MONEDA IN CHAR)
      RETURN NUMBER;

   /********************************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_BUS_CNTRL_PAGO
   -- Objetivo: Procedimiento almacenado que muestra datos para gráfica control productos en moneda nacional
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_TIPO_SUB_PRDTO
   --
   -- Fecha: 29-01-2019
   -- Autor: Santander CAH
   -- Input:
   --       p_MONEDA   MOneda a buscar
   -- Output:
   --       p_PAGOS
   --       p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --********************************************************************************************************************/
   PROCEDURE SP_PAB_BUS_CNTRL_PAGO (p_MONEDA   IN     CHAR,
                                    p_PAGOS       OUT SYS_REFCURSOR,
                                    p_ERROR       OUT NUMBER);

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_CON_OPE_SWF_ALCO
   -- Objetivo: Procedimiento almacenado que retorna los datos para operaciones,( Especial ALCO)
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN, PABS_DT_OPRCN_INFCN_OPCON,
   --                PABS_DT_DETLL_OPRCN_HTRCA,  PABS_DT_OPRCN_OPCON_HTRCA,
   --                    PABS_DT_ESTDO_ALMOT,  PABS_DT_TIPO_OPRCN_ALMOT
   --
   -- Fecha: 29-08-2018
   -- Autor: Santander CAH
   -- Input:
   --      p_NUM_FOL_OPE -> Numero de folio operacion
   --      p_FEC_DESDE ->  fecha desde
   --      p_FEC_HASTA ->  fecha hasta
   --      p_COD_TPO_OPE_AOS ->  codigo tipo operacion estado
   --      p_COD_EST_AOS ->  codigo estado
   --      p_COD_SIS_ENT ->  codigo sistema entrada
   --      p_COD_SIS_SAL ->  codigo de sistema salida
   --      p_COD_BCO_DTN ->  codigo banco destino
   --      p_COD_BCO_ORG -> Codigo Bic banco origen
   --      p_COD_MT_SWF ->  codigo MT swf
   --      p_FLG_EGR_ING -> Flasg egreso ingreso
   --      p_IMP_OPE_DESDE ->  importe operacion desde
   --      p_IMP_OPE_HASTA ->  importe operacion hasta
   --      p_COD_DVI -> codigo de divisa
   -- Output:
   -- p_CURSOR -> Cursor con las nominas encontradas
   -- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   --             (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_CON_OPE_SWF_ALCO (
      p_COD_DVI           IN     CHAR,
      p_COD_BCO_ORG       IN     CHAR,
      p_COD_BCO_DTN       IN     CHAR,
      p_COD_TPO_OPE_AOS   IN     CHAR,
      p_NUM_FOL_OPE       IN     NUMBER,
      p_IMP_OPE_DESDE     IN     NUMBER,
      p_IMP_OPE_HASTA     IN     NUMBER,
      p_COD_SIS_ENT       IN     CHAR,
      p_COD_SIS_SAL       IN     CHAR,
      p_FEC_DESDE         IN     DATE,
      p_FEC_HASTA         IN     DATE,
      p_COD_EST_AOS       IN     NUMBER,
      p_COD_MT_SWF        IN     NUMBER,
      p_HOR_DESDE         IN     NUMBER,
      p_HOR_HASTA         IN     NUMBER,
      p_FLG_EGR_ING       IN     NUMBER,
      p_OPE_VISADA        IN     NUMBER,
      p_CURSOR               OUT SYS_REFCURSOR,
      p_ERROR                OUT NUMBER
   );

   --***********************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_BUS_OPRCN_VSADA
   -- Objetivo: Función que retorna si la operacion fue Visada
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN
   --                  PABS_DT_NOMNA_INFCN_OPCON
   --                  PABS_DT_BTCOR_OPRCN
   -- Fecha: 21-11-2018
   -- Autor: CAH
   -- Input:
   --      p_FOLIO_OPE     -> Fecha a cosultar
   --      p_FECHA_OPE      -> Hora a consultar
   -- N/A
   -- Output:
   -- N/A.
   -- Input/Output:
   -- N/A.
   -- Retorno:
   --      v_MONTO_HORA -> monto de la moneda consultada.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   --***********************************************************************************************
   FUNCTION FN_PAB_BUS_OPRCN_VSADA (p_FOLIO_OPE   IN NUMBER,
                                    p_FECHA_OPE   IN DATE)
      RETURN NUMBER;

   /******************************************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_CON_SALDO_LIQDZ
   -- Objetivo: Procedimiento almacenado que retorna los saldos de moneda nacional
   -- Sistema: DBO_PAB.
   -- Base de Datos: PABS_DT_DETLL_OPRCN_HTRCA
   -- Tablas Usadas:
   --
   -- Fecha: 08-11-2018
   -- Autor: Santander CAH
   -- Input:
   --      p_FECHA_INI      Fecha inicio de consulta
   --      p_FECHA_FIN      Fecha fin de consulta
   --      p_HORA_INI       Hora incioo consulta
   --      p_HORA_FIN       Hora fin consulta
   --      p_BANCO          Banco a consultar
   --      p_MONEDA         Moneda a consultar
   --
   -- Output:
   --      p_SALDO_LBTR       Cursor con acumulado LBTR
   --      p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de últimos    cambios>
   --********************************************************************************************/
   PROCEDURE SP_PAB_CON_SALDO_LIQDZ (p_FECHA_INI   IN     DATE,
                                     p_FECHA_FIN   IN     DATE,
                                     p_HORA_INI    IN     NUMBER,
                                     p_HORA_FIN    IN     NUMBER,
                                     p_BANCO       IN     CHAR,
                                     p_MONEDA      IN     CHAR,
                                     p_SALDO          OUT SYS_REFCURSOR,
                                     p_ERROR          OUT NUMBER);

   /*****************************************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_CON_DETLL_LIQDZ
   -- Objetivo: Procedimiento almacenado que retorna los saldos de moneda nacional
   -- Sistema: DBO_PAB.
   -- Base de Datos: PABS_DT_DETLL_OPRCN_HTRCA
   -- Tablas Usadas:
   --
   -- Fecha: 08-11-2018
   -- Autor: Santander CAH
   -- Input:
   --      p_FECHA_INI      Fecha inicio de consulta
   --      p_FECHA_FIN      Fecha fin de consulta
   --      p_HORA_INI       Hora incioo consulta
   --      p_HORA_FIN       Hora fin consulta
   --      p_BANCO          Banco a consultar
   --      p_MONEDA         Moneda a consultar
   --      p_CANAL          Canal a consultar
   --
   -- Output:
   --      p_DETLL       Cursor con acumulado por banco
   --      p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de últimos    cambios>
   --********************************************************************************************/
   PROCEDURE SP_PAB_CON_DETLL_LIQDZ (p_FECHA_INI   IN     DATE,
                                     p_FECHA_FIN   IN     DATE,
                                     p_HORA_INI    IN     NUMBER,
                                     p_HORA_FIN    IN     NUMBER,
                                     p_BANCO       IN     CHAR,
                                     p_MONEDA      IN     CHAR,
                                     p_CANAL       IN     CHAR,
                                     p_DETLL          OUT SYS_REFCURSOR,
                                     p_ERROR          OUT NUMBER);

   --***********************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_BUS_SALDO
   -- Objetivo: Función que consulta el Saldo inicial o final segun moneda y banco
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas: PABS_DT_RGTRO_SALDO_HTRCO
   -- Fecha: 12-02-2019
   -- Autor: CAH
   -- Input:
   --      p_FLG_SLD   -> Flag de saldo
   --      p_MONEDA    -> Moneda
   --      p_BANCO     -> Banco a consultar
   --      p_FEC_INI   -> Fecha a cosultar
   --      p_FEC_FIN   -> Fecha a cosultar
   --      p_HORA_INI  -> Hora a consultar
   --      p_HORA_FIN  I-> Hora a consultar
   -- N/A
   -- Output:
   -- N/A.
   -- Input/Output:
   -- N/A.
   -- Retorno:
   --      v_SALDO -> Saldo de la moneda consultada.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   --***********************************************************************************************
   FUNCTION FN_PAB_BUS_SALDO (p_FLG_SLD    IN NUMBER,
                              p_MONEDA     IN CHAR,
                              p_BANCO      IN CHAR,
                              p_FEC_INI    IN NUMBER,
                              p_FEC_FIN    IN NUMBER,
                              p_HORA_INI   IN NUMBER,
                              p_HORA_FIN   IN NUMBER)
      RETURN NUMBER;

   --***********************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_BUS_ING_MIN
   -- Objetivo: Función que consulta el minimo ingreso  de una moneda
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas: PABS_DT_RGTRO_SALDO_HTRCO
   -- Fecha: 12-02-2019
   -- Autor: CAH
   -- Input:
   --      p_FLG_SLD   -> Flag de saldo
   --      p_MONEDA    -> Moneda
   --      p_BANCO     -> Banco a consultar
   --      p_FEC_INI   -> Fecha a cosultar
   --      p_FEC_FIN   -> Fecha a cosultar
   --      p_HORA_INI  -> Hora a consultar
   --      p_HORA_FIN  I-> Hora a consultar
   -- N/A
   -- Output:
   -- N/A.
   -- Input/Output:
   -- N/A.
   -- Retorno:
   --      v_SALDO -> Saldo de la moneda consultada.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   --***********************************************************************************************
   FUNCTION FN_PAB_BUS_ING_MIN (p_MONEDA     IN CHAR,
                                p_CANAL      IN CHAR,
                                p_FEC_INI    IN NUMBER,
                                p_FEC_FIN    IN NUMBER,
                                p_HORA_INI   IN NUMBER,
                                p_HORA_FIN   IN NUMBER)
      RETURN NUMBER;

   --***********************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_BUS_EGR_MIN
   -- Objetivo: Función que consulta el minimo EGRESO  de una moneda
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas: PABS_DT_RGTRO_SALDO_HTRCO
   -- Fecha: 12-02-2019
   -- Autor: CAH
   -- Input:
   --      p_FLG_SLD   -> Flag de saldo
   --      p_MONEDA    -> Moneda
   --      p_BANCO     -> Banco a consultar
   --      p_FEC_INI   -> Fecha a cosultar
   --      p_FEC_FIN   -> Fecha a cosultar
   --      p_HORA_INI  -> Hora a consultar
   --      p_HORA_FIN  I-> Hora a consultar
   -- N/A
   -- Output:
   -- N/A.
   -- Input/Output:
   -- N/A.
   -- Retorno:
   --      v_SALDO -> Saldo de la moneda consultada.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   --***********************************************************************************************
   FUNCTION FN_PAB_BUS_EGR_MIN (p_MONEDA     IN CHAR,
                                p_CANAL      IN CHAR,
                                p_FEC_INI    IN NUMBER,
                                p_FEC_FIN    IN NUMBER,
                                p_HORA_INI   IN NUMBER,
                                p_HORA_FIN   IN NUMBER)
      RETURN NUMBER;

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_CON_LIMTE_MONDA_ITDIA
   -- Objetivo: Procedimiento que consulta limites intradia de cada moneda
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas:  PABS_DT_MONDA_ALMOT
   -- Fecha: 07/08/2018
   -- Autor: Santander CAH
   -- Input:
   -- Output:
   -- p_CURSOR -> Cursor con los registros de los elementos
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de últimos    cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_CON_LIMTE_MONDA_ITDIA (p_CURSOR   OUT SYS_REFCURSOR,
                                           p_ERROR    OUT NUMBER);

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_ACT_LIMTE_MONDA_ITDIA
   -- Objetivo: Procedimiento que actualiza limites de una moneda
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas:  PABS_DT_MONDA_ALMOT
   -- Fecha: 07/08/2018
   -- Autor: Santander CAH
   -- Input:
   --        p_MONEDA      Moneda a actualizar
   --        p_LIMITE_MAX  Monto máximo
   --        p_LIMITE_MIN  Monto mínimo
   --        p_PORCEN_MAX  Porcentaje máximo a actualizar
   --        p_PORCEN_MIN  Porcentaje mínimo a actualizar
   --        p_USUARIO     Usuario que modifica.
   -- Output:
   -- p_CURSOR -> Cursor con los registros de los elementos
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de últimos    cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_ACT_LIMTE_MONDA_ITDIA (p_MONEDA       IN     CHAR,
                                           p_LIMITE_MAX   IN     NUMBER,
                                           p_LIMITE_MIN   IN     NUMBER,
                                           p_PORCEN_MAX   IN     NUMBER,
                                           p_PORCEN_MIN   IN     NUMBER,
                                           p_USUARIO      IN     CHAR,
                                           p_ERROR           OUT NUMBER);

   --***********************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_BUS_PAIS
   -- Objetivo: Consulta el pais de una operación, segun flag de ingreso
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN
   --                TGEN_0112 , TCDTBAI
   -- Fecha: 20-09-2018
   -- Autor: CAH
   -- Input:
   --      p_FECHA     -> Fecha a cosultar
   --      p_HORA      -> Hora a consultar
   --      p_MONEDA    -> MOneda
   -- N/A
   -- Output:
   -- N/A.
   -- Input/Output:
   -- N/A.
   -- Retorno:
   --      v_MONTO_HORA -> monto de la moneda consultada.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   --***********************************************************************************************
   FUNCTION FN_PAB_BUS_PAIS (p_FOLIO   IN NUMBER,
                             p_FECHA   IN DATE,
                             p_FLAG    IN NUMBER)
      RETURN VARCHAR2;

   --************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_INTFZ_RISGO
   -- Objetivo: Procedimiento que al cierre del sistema se crea la interfaz de Riesgo con Transacciones
   --            Swift del día.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN OPE,
   --        PABS_DT_OPRCN_INFCN_OPCON ODC
   --        ,TCDTBAI TBAI
   --        ,TGEN_0112 PAIS
   --
   -- Fecha: 21-08-2018 CAH
   -- Autor: Santander
   -- Input: p_FEC_PCS -> Fecha Proceso.
   -- Output: p_CURSOR -> Con datos para la creación de la interfaz
   --         p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: 21-08-2018, Creacion Procedimiento.
   --***********************************************************************************************
   PROCEDURE SP_PAB_INTFZ_RISGO (p_FEC_PCS   IN     VARCHAR2,
                                 p_CURSOR       OUT SYS_REFCURSOR,
                                 p_ERROR        OUT NUMBER);

   --************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_INFRM_CIRRE_MONDA_PRDTO
   -- Objetivo: Procedimiento que crea Informe de cierre del día por moneda y producto
   --
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN OPE,
   --                    PABS_DT_TIPO_OPRCN_ALMOT
   --                    PABS_DT_TIPO_SUB_PRDTO
   --
   -- Fecha: 24-08-2018 CAH
   -- Autor: Santander
   -- Input: p_FEC_PCS -> Fecha Proceso.
   -- Output: p_CURSOR -> Con datos para la creación de la interfaz
   --         p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: 21-08-2018, Creacion Procedimiento.
   --***********************************************************************************************
   PROCEDURE SP_PAB_INFRM_CIRRE_MONDA_PRDTO (
      p_FEC_PCS   IN     VARCHAR2,
      p_CURSOR       OUT SYS_REFCURSOR,
      p_ERROR        OUT NUMBER
   );

   --************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_INFRM_CIRRE_BANCO_CRPSL
   -- Objetivo: Procedimiento que crea Informe Cierre de Bancos Corresponsales MX
   --            Swift del día.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX
   --
   -- Fecha: 24-08-2018 CAH
   -- Autor: Santander
   -- Input: p_FEC_PCS -> Fecha Proceso.
   -- Output: p_CURSOR -> Con datos para la creación de la interfaz
   --         p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: 21-08-2018, Creacion Procedimiento.
   --***********************************************************************************************
   PROCEDURE SP_PAB_INFRM_CIRRE_BANCO_CRPSL (
      p_FEC_PCS   IN     VARCHAR2,
      p_CURSOR       OUT SYS_REFCURSOR,
      p_ERROR        OUT NUMBER
   );

   --************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_INFRM_TRNSC_MN
   -- Objetivo: Procedimiento que crea el informe Resumen de Transferencia MN
   --            Swift del día.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_BANCO_ALMOT
   --
   -- Fecha: 24-08-2018 CAH
   -- Autor: Santander
   -- Input: p_FEC_PCS -> Fecha Proceso.
   -- Output: p_CURSOR -> Con datos para la creación de la interfaz
   --         p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: 24-08-2018, Creacion Procedimiento.
   --***********************************************************************************************
   PROCEDURE SP_PAB_INFRM_TRNSC_MN (p_FEC_PCS   IN     VARCHAR2,
                                    p_CURSOR       OUT SYS_REFCURSOR,
                                    p_ERROR        OUT NUMBER);


   --***********************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_BUS_CANT_CANAL
   -- Objetivo: Funcion que retorna la cantidad  de operaciones por canal y flujo(egreso o  ingreso)
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN
   -- Fecha: 05-10-2018
   -- Autor: CAH
   -- Input:
   --      p_CANAL     -> Canal a consultar.
   --      p_BANCO     -> Banco a consultar
   --      p_FLAG      -> Flag de ingreso o egreso

   -- N/A
   -- Output:
   -- N/A.
   -- Input/Output:
   -- N/A.
   -- Retorno:
   --      v_MONTO_HORA -> monto de la moneda consultada.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   --***********************************************************************************************
   FUNCTION FN_PAB_BUS_CANT_CANAL (p_CANAL    IN CHAR,
                                   p_MONEDA   IN CHAR,
                                   p_BANCO    IN CHAR,
                                   p_FLAG     IN NUMBER)
      RETURN NUMBER;

   --***********************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_BUS_SUMA_CANAL
   -- Objetivo: Funcion que retorna la suma por canal y flujo(egreso-ingreso)
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN
   -- Fecha: 05-10-2018
   -- Autor: CAH
   -- Input:
   --      p_CANAL     -> Canal a consultar.
   --      p_FLAG      -> Flag de ingreso o egreso
   -- N/A
   -- Output:
   -- N/A.
   -- Input/Output:
   -- N/A.
   -- Retorno:
   --      v_MONTO_HORA -> monto de la moneda consultada.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   --***********************************************************************************************
   FUNCTION FN_PAB_BUS_SUMA_CANAL (p_CANAL    IN CHAR,
                                   p_MONEDA   IN CHAR,
                                   p_BANCO    IN CHAR,
                                   p_FLAG     IN NUMBER)
      RETURN NUMBER;

   --************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_INFRM_TRNSC_MX
   -- Objetivo: Procedimiento que crea informe Resumen de Transferencias MX
   --            Swift del día.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX
   --                  PABS_DT_CBCRA_CRTLA
   --
   -- Fecha: 24-08-2018 CAH
   -- Autor: Santander
   -- Input: p_FEC_PCS -> Fecha Proceso.
   -- Output: p_CURSOR -> Con datos para la creación de la interfaz
   --         p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: 24-08-2018, Creacion Procedimiento.
   --***********************************************************************************************
   PROCEDURE SP_PAB_INFRM_TRNSC_MX (p_FEC_PCS   IN     VARCHAR2,
                                    p_CURSOR       OUT SYS_REFCURSOR,
                                    p_ERROR        OUT NUMBER);

   --************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_INFRM_OPRCN_FUERA_HORA
   -- Objetivo: Procedimiento que crea Informe de operaciones cursadas fuera de horario
   --            Swift del día.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN
   --
   -- Fecha: 24-08-2018 CAH
   -- Autor: Santander
   -- Input: p_FEC_PCS -> Fecha Proceso.
   -- Output: p_CURSOR -> Con datos para la creación de la interfaz
   --         p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: 24-08-2018, Creacion Procedimiento.
   --***********************************************************************************************
   PROCEDURE SP_PAB_INFRM_OPRCN_FUERA_HORA (p_FEC_PCS   IN     VARCHAR2,
                                            p_CURSOR       OUT SYS_REFCURSOR,
                                            p_ERROR        OUT NUMBER);

   --************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_INFRM_SBRGR_BANCO_CRPSL
   -- Objetivo: Procedimiento que crea el Informe de sobregiros Bancos Corresponsales MX
   --            Swift del día.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_RGTRO_SALDO_HTRCO
   --
   -- Fecha: 24-08-2018 CAH
   -- Autor: Santander
   -- Input: p_FEC_PCS -> Fecha Proceso.
   -- Output: p_CURSOR -> Con datos para la creación de la interfaz
   --         p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: 24-08-2018, Creacion Procedimiento.
   --***********************************************************************************************
   PROCEDURE SP_PAB_INFRM_SBRGR_BANCO_CRPSL (
      p_FEC_PCS   IN     VARCHAR2,
      p_CURSOR       OUT SYS_REFCURSOR,
      p_ERROR        OUT NUMBER
   );

   --************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_INFRM_USO_SISTM_PAGO
   -- Objetivo: Procedimiento que crea el Informe Uso Sistema de Pago (Lbtr y Combanc)
   --            Swift del día.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN
   --
   -- Fecha: 30-10-2018
   -- Autor: CAH
   -- Input: p_FEC_PCS -> Fecha Proceso.
   -- Output:
   --          p_TOTAL     -> Cursor con totales segun canal de salida (CLP)
   --          p_DETALLE   -> Cursor con detalle segun canal de salida (CLP)
   --         p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: 24-08-2018, Creacion Procedimiento.
   --***********************************************************************************************
   PROCEDURE SP_PAB_INFRM_USO_SISTM_PAGO (p_FEC_PCS   IN     VARCHAR2,
                                          p_TOTAL        OUT SYS_REFCURSOR,
                                          p_DETALLE      OUT SYS_REFCURSOR,
                                          p_ERROR        OUT NUMBER);

   /******************************************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_COMBO_SUB_PRDTO
   -- Objetivo: Procedimiento almacenado que retorna los sub productos
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_TIPO_SUB_PRDTO
   --
   -- Fecha: 15-01-2018
   -- Autor: Santander CAH
   -- Input:
   --
   -- Output:
   --       p_TIPO_PROD : datos de productos
   --       p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de últimos    cambios>
   --******************************************************************************************************************************/
   PROCEDURE SP_PAB_COMBO_SUB_PRDTO (p_TIPO_PROD   OUT SYS_REFCURSOR,
                                     p_ERROR       OUT NUMBER);


   /************************************************************************************************
   --  Procedimiento        :  SP_PAB_CREA_PART_SALDO_HTRCO
   --  Objetivo/Descripción :  Procedimiento que utiliza pkg_dba para crear particiones de
   --                          tablas.
   --  Sistema: PAB
   --  Base de Datos: DGBMSEGDB01_NGALTM
   --  Tablas Usadas: PABS_DT_RGTRO_SALDO_HTRCO
   --  Fecha: 11-12-2018
   --  Autor: Santander CAH
   --  Input:
   --          NOM_TABLA   --> Nombre de tabla para crear particion
   --          NOM_PART    --> Nombre de particion
   --          FEC_DATA    --> Fecha de particion
   --  N/A
   --  Output:
   --       p_ERROR:    Indicador de errores, maneja dos estados, 0 sin errores 1 existen errores..
   --  Input/Output: N/A
   --  Retorno: N/A.
   --  Observaciones: <Fecha y Detalle de últimos    cambios>
   *************************************************************************************************/
   PROCEDURE SP_PAB_CREA_PART_SALDO_HTRCO (NOM_TABLA   IN     VARCHAR2,
                                           NOM_PART    IN     VARCHAR2,
                                           FEC_DATA    IN     VARCHAR2,
                                           p_ERROR        OUT NUMBER);


   --***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_ELI_PART_SALDO_HTRCO
   -- Objetivo: Procedimiento que elimina partición de la tabla PABS_DT_RGTRO_SALDO_HTRCO
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_RGTRO_SALDO_HTRCO
   -- Fecha: 11-12-2018
   -- Autor: Santander CAH
   -- Input:
   --          NOM_TABLA   --> Nombre de tabla para crear partición
   --          NOM_PART    --> Nombre de particion a eliminar
   -- Output:
   --       p_ERROR:    Indicador de errores, maneja dos estados, 0 sin errores 1 existen errores..
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de últimos    cambios>
   --***********************************************************************************************
   PROCEDURE SP_PAB_ELI_PART_SALDO_HTRCO (NOM_TABLA   IN     VARCHAR2,
                                          NOM_PART    IN     VARCHAR2,
                                          p_ERROR        OUT NUMBER);
END PKG_PAB_LIQUIDEZ;