CREATE OR REPLACE PACKAGE         PKG_PAB_CAJA
IS
   --
   v_NOM_PCK     VARCHAR2 (30) := 'PKG_PAB_CAJA';
   ERR_CODE      NUMBER := 0;
   ERR_MSG       VARCHAR2 (300);
   v_DES         VARCHAR2 (300);
   v_DES_BIT     VARCHAR2 (100);
   p_s_mensaje   VARCHAR2 (400);
   v_cero        NUMBER := 0;

   v_dia_ini     DATE := TRUNC (SYSDATE) + INTERVAL '1' MINUTE;
   v_dia_fin DATE
         := TRUNC (SYSDATE) + INTERVAL '23' HOUR + INTERVAL '59' MINUTE ;

   /***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_INS_PYN_MNT_CJA
   -- Objetivo: Procedimiento que inserta movimientos de multinomeda para flujo de egreso e ingreso
   -- desde WS TIBCO al cargar nómina
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_MVNTO_CAJA
   -- Fecha: 22/08/2018
   -- Autor: JBARBOZA
   -- Input:
   -- p_NUM_FOL_OPE        -> Folio Operación de ingreso
   -- p_FEC_INS_OPE        -> Fecha de inserción
   -- p_TPO_OPE            -> Tipo Operación (Cod de concepto del movimiento)
   -- p_COD_BCO_DTN        -> BIC Banco destino
   -- p_COD_BCO_ORG        -> BIC Banco Origen
   -- p_NUM_CTA_CTE        -> Número Cuenta Corriente
   -- p_COD_DVI            -> Moneda
   -- p_FLG_ING_EGR        -> Flag indicador de flujo de ingreso o egreso
   -- p_IMP_OPE            -> Monto
   -- p_FEC_VTA            -> Fecha Valuta
   -- p_COD_SIS_ENT        -> Canal
   -- p_NUM_DOC_EPS        -> Rut Empresa
   -- p_COD_TPO_DOC_EPS    -> Digito Rut Empresa
   -- p_DSC_SIS_ENT        -> Descripción de sistema
   -- p_COD_USR            -> Usuario Tibco
   -- p_COD_USR_SIS        -> Usuario Sistema
   -- Output:
   -- p_ERROR              DETERMINA SI HUBO UN ERROR EN LA EJECUCION : 1 SI OCURRIO UN  ERROR DE LO CONTRARIO SERA 0
   -- p_NUM_FOL_OPE        VARIABLE QUE DEVUELVE EL NUMERO DE FOLIO DE LA OPERACIÓN
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: 22/08/2018  : SE REALIZO UNA MODIFICACIÓN ,EL CUAL SE DESPLAZO TODAS LA ACCIONES
                                   RELACIONADA A LA ACTUALIZACIÓN AL PROCEDMIENTO SP_PAB_ACT_SALDO_MN_MX.
                                   (POR LO TANTO SE CAMBIO LA LOÓGICA DEL MANEJO DE LOS DATOS )
   --***********************************************************************************************/
   PROCEDURE SP_PAB_INS_PYN_MNT_CJA (p_num_fol_nmn       IN     NUMBER,
                                     p_flg_ing_egr       IN     NUMBER,
                                     p_cod_bco           IN     CHAR,
                                     p_num_cta_cte       IN     VARCHAR2,
                                     p_tpo_ope           IN     CHAR,
                                     p_cod_dvi           IN     CHAR,
                                     p_imp_ope           IN     NUMBER,
                                     p_fec_vta           IN     DATE,
                                     p_gls_sis_org       IN     VARCHAR2,
                                     p_gls_mvt           IN     VARCHAR2,
                                     p_cod_sis_ent       IN     CHAR,
                                     p_num_doc_eps       IN     CHAR,
                                     p_cod_tpo_doc_eps   IN     CHAR,
                                     p_cod_usr           IN     VARCHAR,
                                     p_num_fol_ope          OUT NUMBER,
                                     p_error                OUT NUMBER);

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_INS_CRTLA_CARGO_ABONO
   -- Objetivo: Procedimiento almacenado que inserta Movimientos de cargo o abono.(MT 900 y 910)
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_CBCRA_CRTLA
   -- Fecha: 12/10/17
   -- Autor: Santander
   -- Input:
   -- p_FEC_ING_CTL    -> Fecha de ingreso cartola
   -- p_HOR_ING_CTL    -> Hora de ingreso
   -- p_NUM_FOL_CTL    -> Numero de folio cartola
   -- p_NUM_REF_SWF_CTL-> Referencia cartola
   -- p_COD_MT_SWF     -> Código mt de cartola
   -- p_COD_DVI        -> Moneda de movimiento
   -- p_NUM_CTA_CTE    -> Numero de cuenta corriente
   -- p_COD_BCO_ORG    -> Banco origen cartola
   -- p_FLG_DEB_CRE_INI-> Flag debito o crédito inicial
   -- p_IMP_ING_CTL    -> Monto ingreso cartola
   -- p_IMP_EGR_CTL    -> Monto egreso cartola
   -- p_IMP_INI_CTL    -> Monto incio cartola
   -- p_IMP_FNL_CTL    -> Monto final cartola
   -- p_FLG_DEB_CRE_FNL-> Flag debito o crédito final
   -- p_FLG_ING_EGR    -> Flag de ingreso o egreso
   -- Output:
   -- v_NUM_FOL_OPE    ->Folio de MT ingresado
   -- v_FEC_ING_OPE    ->Fecha de mt ingresado
   -- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_INS_CRTLA_CARGO_ABONO (p_COD_MT_SWF    IN     NUMBER,
                                           p_COD_DVI       IN     CHAR,
                                           p_COD_BCO_DTN   IN     VARCHAR2,
                                           p_COD_BCO_ORG   IN     VARCHAR2,
                                           p_COD_BCO_ITD   IN     CHAR,
                                           p_NUM_SEQ_IFD   IN     NUMBER,
                                           p_FEC_ING_IFD   IN     DATE,
                                           p_HOR_ING_IFD   IN     NUMBER,
                                           p_NUM_REF_SWF   IN     CHAR,
                                           p_NUM_CTA_CTE   IN     VARCHAR2,
                                           p_FEC_VTA       IN     DATE,
                                           p_IMP_OPE       IN     NUMBER,
                                           p_GLS_TRN       IN     VARCHAR2,
                                           p_NUM_REF_EXT   IN     CHAR,
                                           --------------------------------
                                           p_NUM_DOC_BFC   IN     VARCHAR2,
                                           p_NOM_BFC       IN     VARCHAR2,
                                           ---------------------------------
                                           p_NUM_FOL_OPE      OUT NUMBER,
                                           p_FEC_ING_OPE      OUT TIMESTAMP,
                                           p_ERROR            OUT NUMBER);

   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_INS_CBCRA_CRTLA_MT
   -- Objetivo: Procedimiento que inserta cartola - Movimiento de entrada y salida de cuentas corrientes de bancos corresponsales
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_CBCRA_CRTLA
   -- Fecha: 03/11/17
   -- Autor: Santander
   -- Input:
   -- p_COD_BCO_ORG
   -- p_NUM_CTA_CTE
   -- p_NUM_FOL_CTL
   -- p_COD_DVI
   -- p_NUM_REF_SWF_CTL
   -- p_FEC_ING_CTL
   -- p_HOR_ING_CTL
   -- p_COD_MT_SWF
   -- p_IM-- p_INI_CTL
   -- p_IM-- p_ING_CTL
   -- p_IM-- p_EGR_CTL
   -- p_IM-- p_FNL_CTL
   -- p_FLG_DEB_CRE_INI
   -- p_FLG_DEB_CRE_FNL
   -- p_FLG_INI_FNL
   -- Output:
   -- p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_INS_CBCRA_CRTLA_MT (p_COD_BCO_ORG       IN     VARCHAR2,
                                        p_NUM_CTA_CTE       IN     VARCHAR2,
                                        p_NUM_FOL_CTL       IN     NUMBER,
                                        p_COD_DVI           IN     CHAR,
                                        p_NUM_REF_SWF_CTL   IN     VARCHAR2,
                                        p_FEC_ING_CTL       IN     DATE,
                                        p_HOR_ING_CTL       IN     NUMBER,
                                        p_COD_MT_SWF        IN     VARCHAR2,
                                        p_IMP_INI_CTL       IN     NUMBER,
                                        p_IMP_ING_CTL       IN     NUMBER,
                                        p_IMP_EGR_CTL       IN     NUMBER,
                                        p_IMP_FNL_CTL       IN     NUMBER,
                                        p_FLG_DEB_CRE_INI   IN     CHAR,
                                        p_FLG_DEB_CRE_FNL   IN     CHAR,
                                        p_FLG_INI_FNL       IN     NUMBER,
                                        p_ERROR                OUT NUMBER);



   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_INS_DETLL_CRTLA_MT
   -- Objetivo: Procedimiento que inserta cartola - Movimiento de entrada y salida de cuentas corrientes de bancos corresponsales
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_CRTLA
   -- Fecha: 03/11/17
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_CTL
   -- p_FEC_TRN_OPE
   -- p_NUM_REF_SWF_OPE
   -- p_COD_TRN_OPE
   -- p_NUM_REF_ITN_CTA
   -- p_FLG_DEB_CRE
   -- p_IM-- p_TRN
   -- p_GLS_TRN
   -- p_NUM_CTA_CTE
   -- p_FEC_ING_CTL
   -- Output:
   -- p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_INS_DETLL_CRTLA_MT (p_NUM_FOL_CTL       IN     NUMBER,
                                        p_FEC_TRN_OPE       IN     TIMESTAMP,
                                        p_NUM_REF_SWF_OPE   IN     VARCHAR2,
                                        p_COD_TRN_OPE       IN     VARCHAR2,
                                        p_NUM_REF_ITN_CTA   IN     VARCHAR2,
                                        p_FLG_DEB_CRE       IN     VARCHAR2,
                                        p_IMP_TRN           IN     NUMBER,
                                        p_GLS_TRN           IN     VARCHAR2,
                                        p_NUM_CTA_CTE       IN     VARCHAR2,
                                        p_COD_BCO_ORG       IN     VARCHAR2,
                                        p_COD_DVI           IN     VARCHAR2,
                                        p_FEC_ING_CTL       IN     DATE,
                                        p_ERROR                OUT NUMBER);


   --**********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_CNA_CJA_FIL
   -- Objetivo: Retorna totales de saldos intradía, 24 y 48
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_CAJA_FLIAL
   -- Fecha: 04/10/17
   -- Autor: Santander
   -- Input:
   -- N/A
   -- Output:

   -- p_ERROR  ->  Indicada si el procedimiento
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_PAB_CNA_SLD_CJA (p_FEC_VAL       IN     DATE,
                                 p_NUM_CTA_CTE   IN     VARCHAR2,
                                 p_COD_DIV       IN     CHAR,
                                 p_COD_BCO       IN     VARCHAR2,
                                 p_SDO_ING          OUT NUMBER,
                                 p_SDO_EGRE         OUT NUMBER,
                                 p_SDO_TOT          OUT NUMBER,
                                 p_ERROR            OUT NUMBER);

   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ACT_CRTLA_MX
   -- Objetivo: Procedimiento que actualiza saldo de cartola MT940 MT950.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_CBCRA_CRTLA, PABS_DT_DETLL_CRTLA, PABS_DT_DETLL_SALDO_CAJA_MN_MX
   -- Fecha: 17/10/17
   -- Autor: Santander
   -- Input:
   -- p_NUM_CTA_CTE -> Número de cuenta corriente
   -- p_COD_BCO_ORG -> Código banco
   -- p_FEC_ING_CTL -> Fecha de ingreso cartola
   -- Output:
   -- p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_ACT_CRTLA_MX (p_NUM_CTA_CTE   IN     VARCHAR2,
                                  p_COD_BCO_ORG   IN     VARCHAR2,
                                  p_FEC_ING_CTL   IN     DATE,
                                  p_COD_DVI       IN     VARCHAR2,
                                  p_ERROR            OUT NUMBER);

   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_INS_SLD_CRPSL
   -- Objetivo: Procedimiento que INSERTA registro saldo corresponsales en tabla saldo
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas:
   -- Fecha: 17/10/17
   -- Autor: Santander
   -- Input:
   -- p_FEC_ING_CTL  FECHA INGRESO CARTOLA
   -- p_COD_BCO_ORG  BANCO CORRESPONSAL
   -- p_NUM_CTA_CTE  NUMERO DE CUENTA CORRESPONSAL
   -- p_COD_DVI      MONEDA ASOCIADA A CUENTA CORRESPONSAL
   -- Output:
   -- p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_INS_SLD_CRPSL (p_COD_BCO_ORG   IN     CHAR,
                                   p_NUM_CTA_CTE   IN     VARCHAR2,
                                   p_COD_DVI       IN     CHAR,
                                   p_ERROR            OUT NUMBER);

   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ELI_SLD_CRPSL
   -- Objetivo: Procedimiento que ELIMINA registro saldo corresponsales en tabla saldo
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas:
   -- Fecha: 17/10/17
   -- Autor: Santander
   -- Input:
   -- p_FEC_ING_CTL  FECHA INGRESO CARTOLA
   -- p_COD_BCO_ORG  BANCO CORRESPONSAL
   -- p_NUM_CTA_CTE  NUMERO DE CUENTA CORRESPONSAL
   -- p_COD_DVI      MONEDA ASOCIADA A CUENTA CORRESPONSAL
   -- Output:
   -- p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_ELI_SLD_CRPSL (p_COD_BCO_ORG   IN     CHAR,
                                   p_NUM_CTA_CTE   IN     VARCHAR2,
                                   p_COD_DVI       IN     CHAR,
                                   p_ERROR            OUT NUMBER);

   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_RNO_FEC_PYN
   -- Objetivo: Procedimiento que retorna las fechas de proyeccion para MX
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas:
   -- Fecha: 19/03/18
   -- Autor: SANTANDER
   -- Input:
   -- p_FEC_ING_CTL  FECHA INGRESO CARTOLA
   -- p_COD_BCO_ORG  BANCO CORRESPONSAL
   -- p_NUM_CTA_CTE  NUMERO DE CUENTA CORRESPONSAL
   -- p_COD_DVI      MONEDA ASOCIADA A CUENTA CORRESPONSAL
   -- Output:
   -- p_FEC_DIA      RETORNO DE FECHA ACTUAL
   -- p_FEC_24H      RETORNO DE FECHA 24h
   -- p_FEC_48H      RETORNO DE FECHA 48h
   -- p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_RNO_FEC_PYN (p_COD_BCO_ORG   IN     CHAR,
                                 p_NUM_CTA_CTE   IN     VARCHAR2,
                                 p_COD_DVI       IN     CHAR,
                                 p_FEC_DIA          OUT DATE,
                                 p_FEC_24H          OUT DATE,
                                 p_FEC_48H          OUT DATE,
                                 p_ERROR            OUT NUMBER);

   --************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_ACT_SALDO_MN_MX
   -- Objetivo: PROCEDIMIENTO QUE REALIZA LAS ACTUALIZACIONES DE LOS SALDOS PARA LA CARGA DE PROYECCIONES  DE INGRESO Y  EL EGRESO.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas:PABS_DT_DETLL_MVNTO_CAJA ,PABS_DT_DETLL_SALDO_CAJA_MN_MX
   -- Fecha: 22/08/2018
   -- Autor: JBARBOZA
   -- Input:  N/A.
   -- Output:
   -- p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE sp_pab_act_saldo_mn_mx (p_error OUT NUMBER);

   /********************************************************************************************************
   FUNCION/PROCEDIMIENTO: SP_BUS_PRYCN_MNMX.
   OBJETIVO             : MUESTRA EL DETALLE DE LA PROYECCIÓN CARGADA PARA MONEDA NACIONAL Y MONEDA
                          EXTRANJERA SEGÚN ÁREA.
   SISTEMA              : DBO_PAB.
   BASE DE DATOS        : DBO_PAB.
   TABLAS USADAS        : PABS_DT_DETLL_MVNTO_CAJA.
                          PABS_DT_SISTM_ENTRD_SALID.
                          TCDTBAI.
   FECHA                : 13/02/19.
   AUTOR                : VAR.
   INPUT                : P_AREA    := ÁREA.
                          P_FLG_MRD := MONEDA.
                            0 := MN - 1 := MX.
   OUTPUT               : P_CURSOR  := DATA.
                          P_ERROR   := INDICADOR DE ERRORES.
   OBSERVACIONES
   FECHA       USUARIO    DETALLE
   13/02/19    VAR        CREACIÓN.
   ********************************************************************************************************/
   PROCEDURE SP_BUS_PRYCN_MNMX (P_AREA      IN     CHAR,
                                P_FLG_MRD   IN     CHAR,
                                P_CURSOR       OUT SYS_REFCURSOR,
                                P_ERROR        OUT NUMBER);
END PKG_PAB_CAJA;