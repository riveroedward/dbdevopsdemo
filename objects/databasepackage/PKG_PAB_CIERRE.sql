CREATE OR REPLACE PACKAGE         PKG_PAB_CIERRE
IS
   /**************************VARIABLE GLOBALES************************************/
   v_NOM_PCK CONSTANT       VARCHAR2 (30) := 'PKG_PAB_CIERRE';
   ERR_CODE                 NUMBER := 0;
   ERR_MSG                  VARCHAR2 (300);
   v_DES                    VARCHAR2 (300);
   p_s_mensaje              VARCHAR2 (400);
   v_CERO                   NUMBER := 0;
   p_FEC_ICO_PCS            VARCHAR2 (30);
   p_COD_TPO_PSC CONSTANT   CHAR (3) := 'PCS';
   p_COD_UNI_PSC            VARCHAR2 (30);
   p_COD_ERR_BIB            CHAR (6) := 0;
   p_NUM_TCK_PCS            NUMBER;
   p_GLS_DET_PCS            VARCHAR2 (300);

   /**************************FIN VARIABLE GLOBALES********************************/

   /**************************INICIO CURSOR CARTOLAS********************************/

   CURSOR C_CONSULTA_CTAS_MX
   IS
      SELECT   SALDO.COD_BCO_ORG, SALDO.NUM_CTA_CTE, SALDO.COD_DVI
        FROM   PABS_DT_DETLL_SALDO_CAJA_MN_MX SALDO;

   R_CONSULTA_CTAS_MX       C_CONSULTA_CTAS_MX%ROWTYPE;

   /**************************FIN CURSOR CARTOLAS********************************/

   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_GESTION_CIERRE
   -- Objetivo: Gestiona el cierre del sistema orquestando la llamada a los procedimientos especificos
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas:
   -- Fecha: 14/09/2016
   -- Autor: Santander
   -- Input:
   -- p_FEC_PCS -> Fecha Proceso.
   -- Output: p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   --         P_ERR_MSG -> Glosa que indica el error
   -- Retorno: N/A.
   -- Observaciones: 14/09/2016, Creacion Procedimiento.
   --***********************************************************************************************
   PROCEDURE Sp_PAB_GESTION_CIERRE (p_FEC_PCS   IN     VARCHAR2,
                                    p_ERROR        OUT NUMBER,
                                    P_ERR_MSG      OUT VARCHAR2);

   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ACTUALIZA_NOMINAS_CIERRE
   -- Objetivo: Al cierre del sistema busca nominas que no fueron autorizadas por el usuario y cambia su estado a eliminadas
   --          Estado Nominas buscadas: Valida, Invalida, Cursada, Rechazada, Duplicada
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_CBCRA_NOMNA
   -- Fecha: 14/09/2016
   -- Autor: Santander
   -- Input:
   -- p_FEC_PCS -> Fecha Proceso.
   -- Output: p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: 14/09/2016, Creacion Procedimiento.
   --***********************************************************************************************
   PROCEDURE Sp_PAB_ACT_NOMINAS_CIERRE (p_FEC_PCS   IN     VARCHAR2,
                                        p_ERROR        OUT NUMBER);

   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ACT_OPERACIONES_CIERRE
   -- Objetivo: Al cierre del sistema busca operaciones que no fueron pagadas por el usuario y cambia su estado a eliminadas
   --          Estado Operaciones buscadas: Valida, Invalida, Cursada, Rechazada, Duplicada
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN
   -- Fecha: 26/09/2016
   -- Autor: Santander
   -- Input:
   -- p_FEC_PCS -> Fecha Proceso.
   -- Output: p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: 14/09/2016, Creacion Procedimiento.
   --***********************************************************************************************
   PROCEDURE Sp_PAB_ACT_OPERACIONES_CIERRE (p_FEC_PCS   IN     VARCHAR2,
                                            p_ERROR        OUT NUMBER);

   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_HIS_INS_NOMINAS_CIERRE
   -- Objetivo: Al cierre del sistema se historifican las operaciones para la fecha ingresada
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_CBCRA_NOMNA_HTRCA, PABS_DT_CBCRA_NOMNA, PABS_DT_DETLL_NOMNA_HTRCA,
   --                PABS_DT_DETLL_NOMNA, PABS_DT_NOMNA_OPCON_HTRCA, PABS_DT_NOMNA_INFCN_OPCON
   -- Fecha: 26/09/2016
   -- Autor: Santander
   -- Input:
   -- p_FEC_PCS -> Fecha Proceso.
   -- Output: p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: 27/09/2016, Creacion Procedimiento.
   --***********************************************************************************************
   PROCEDURE Sp_PAB_HIS_NOMINAS_CIERRE (p_FEC_PCS   IN     VARCHAR2,
                                        p_ERROR        OUT NUMBER);

   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_HIS_INS_OPERACIONES_CIERRE
   -- Objetivo: Al cierre del sistema se historifican las operaciones para la fecha ingresada
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN
   -- Fecha: 26/09/2016
   -- Autor: Santander
   -- Input:
   -- p_FEC_PCS -> Fecha Proceso.
   -- Output: p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: 27/09/2016, Creacion Procedimiento.
   --***********************************************************************************************
   PROCEDURE Sp_PAB_HIS_OPERACIONES_CIERRE (p_FEC_PCS   IN     VARCHAR2,
                                            p_ERROR        OUT NUMBER);

   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_HIS_BITACORA_CIERRE
   -- Objetivo: Al cierre del sistema se historifican las operaciones para la fecha ingresada
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN_HTRCA, PABS_DT_DETLL_OPRCN, PABS_DT_OPRCN_OPCON_HTRCA, PABS_DT_OPRCN_INFCN_OPCON
   -- Fecha: 26/09/2016
   -- Autor: Santander
   -- Input: p_FEC_PCS -> Fecha Proceso.
   -- Output: p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: 14/09/2016, Creacion Procedimiento.
   --***********************************************************************************************
   PROCEDURE Sp_PAB_HIS_BITACORA_CIERRE (p_FEC_PCS   IN     VARCHAR2,
                                         p_ERROR        OUT NUMBER);

   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ELI_BITACORA_PROCESO
   -- Objetivo: Al cierre del sistema se eliminan los registros de la tabla bitacora proceso para evitar acumulaci?n de registros de  traza
   --           el prerior eliminado es segun lo indicado por la constante PKG_PAB_CONSTANTES.V_DIA_ELI_LOG.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_BTCOR_PRCSO, PABS_DT_BTCOR_DETLL_PRCSO
   -- Fecha: 14/11/2016
   -- Autor: Santander
   -- Input: p_FEC_PCS -> Fecha Proceso.
   -- Output: p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: 14/11/2016, Creacion Procedimiento.
   --***********************************************************************************************
   PROCEDURE Sp_PAB_ELI_BITACORA_PROCESO (p_FEC_PCS   IN     VARCHAR2,
                                          p_ERROR        OUT NUMBER);

   --**************************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ELI_DET_OPERACION_DCV
   -- Objetivo: Al cierre del sistema se eliminan los registros de la tabla detalle operaciones dcv para evitar acumulación
   --          de registros, esto se hará eliminando registros que cumplan más de 1 día.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN_DCV
   -- Fecha: 06/08/2018
   -- Autor: Santander-CAH
   -- Input: p_FEC_PCS -> Fecha Proceso.
   -- Output: p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: 06/08/2018, Creacion Procedimiento.
   --***************************************************************************************************************
   PROCEDURE Sp_PAB_ELI_DET_OPERACION_DCV (p_FEC_PCS   IN     VARCHAR2,
                                           p_ERROR        OUT NUMBER);

   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ELI_LOG_CIERRE
   -- Objetivo: Al cierre del sistema se eliminan los registros del log de sistema, se conservan X dias segun
   --           lo indicado por la constante PKG_PAB_CONSTANTES.V_DIA_ELI_LOG.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_BTCOR_BASE_DATO_ALMOT
   -- Fecha: 26/09/2016
   -- Autor: Santander
   -- Input: p_FEC_PCS -> Fecha Proceso.
   -- Output: p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: 14/09/2016, Creacion Procedimiento.
   --***********************************************************************************************
   PROCEDURE Sp_PAB_ELI_LOG_CIERRE (p_FEC_PCS   IN     VARCHAR2,
                                    p_ERROR        OUT NUMBER);

   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ELI_NOM_EXT
   -- Objetivo: Al cierre del sistema se eliminan los registros de validacion nomina externa
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_MOTVO_NO_HABLT
   -- Fecha: 30/01/2019
   -- Autor: Santander
   -- Input: p_FEC_PCS -> Fecha Proceso.
   -- Output: p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: 30/01/2019, Creacion Procedimiento.
   --***********************************************************************************************
   PROCEDURE Sp_PAB_ELI_VAL_NOM_EXT (p_FEC_PCS   IN     VARCHAR2,
                                     p_ERROR        OUT NUMBER);

   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ELI_MOV_PRO_FIL
   -- Objetivo: Al cierre del sistema se eliminan los registros de proyeccion de filiales con fechas antiguas
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_MVNTO_FLIAL
   -- Fecha: 26/09/2016
   -- Autor: Santander
   -- Input: p_FEC_PCS -> Fecha Proceso.
   -- Output: p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: 14/09/2016, Creacion Procedimiento.
   --***********************************************************************************************
   PROCEDURE Sp_PAB_ELI_MOV_PRO_FIL (p_FEC_PCS   IN     VARCHAR2,
                                     p_ERROR        OUT NUMBER);

   --**************************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ELI_NOTIFICACION
   -- Objetivo: Al cierre del sistema se eliminan los registros de la tabla Notificaciones para evitar acumulación
   --          de registros, esto se hará eliminando registros que cumplan más de 7 días, para eliminar se crea la
   --          constante PKG_PAB_CONSTANTES.V_DIA_ELI_NTF que inicialmente tendra valor 7.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_BTCOR_PRCSO, PABS_DT_BTCOR_DETLL_PRCSO
   -- Fecha: 03/11/2017
   -- Autor: Santander-CAH
   -- Input: p_FEC_PCS -> Fecha Proceso.
   -- Output: p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: 14/11/2016, Creacion Procedimiento.
   --***************************************************************************************************************
   PROCEDURE Sp_PAB_ELI_NOTIFICACION (p_FEC_PCS   IN     VARCHAR2,
                                      p_ERROR        OUT NUMBER);

   --**************************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ELI_NOTIFICACION
   -- Objetivo: Al cierre del sistema se eliminan los registros de la tabla Saldos bilaterales para evitar acumulación
   --          de registros, esto se hará eliminando registros que cumplan más de 1 día.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_SALDO_BILAT
   -- Fecha: 16/11/2017
   -- Autor: Santander-CAH
   -- Input: p_FEC_PCS -> Fecha Proceso.
   -- Output: p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: 16/11/2016, Creacion Procedimiento.
   --***************************************************************************************************************
   PROCEDURE Sp_PAB_ELI_SALDO_BILAT (p_FEC_PCS   IN     VARCHAR2,
                                     p_ERROR        OUT NUMBER);

   --**************************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ELI_SALDO_AREA
   -- Objetivo: Al cierre del sistema se eliminan los registros de la tabla Saldos áreas para evitar acumulación
   --          de registros, esto se hará eliminando todos los registros
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_SALDO_BILAT
   -- Fecha: 22-11-2017
   -- Autor: Santander-CAH
   -- Input: p_FEC_PCS -> Fecha Proceso.
   -- Output: p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: 16/11/2016, Creacion Procedimiento.
   --***************************************************************************************************************
   PROCEDURE Sp_PAB_ELI_SALDO_AREA (p_FEC_PCS   IN     VARCHAR2,
                                    p_ERROR        OUT NUMBER);

   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_HIS_MENSAJERIA_CIERRE
   -- Objetivo: Al cierre del sistema se historifican las operaciones para la fecha ingresada
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas:PABS_DT_MNSJE_PRTCZ_HTRCA, PABS_DT_MNSJE_PRTCZ
   -- Fecha: 26/09/2016
   -- Autor: Santander
   -- Input: p_FEC_PCS -> Fecha Proceso.
   -- Output: p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: 27/04/2017, Creacion Procedimiento.
   --***********************************************************************************************
   PROCEDURE Sp_PAB_HIS_MENSAJERIA_CIERRE (p_FEC_PCS   IN     VARCHAR2,
                                           p_ERROR        OUT NUMBER);

   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_GESTION_CIERRE_MANUAL
   -- Objetivo: Gestiona el proceso de cierre manual
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas:
   -- Fecha: 29/05/2017
   -- Autor: Santander
   -- Input: p_FEC_PCS -> Fecha Proceso.
   -- p_COD_USR      IN  VARCHAR2,
   -- p_COD_SIS_ENT  IN  CHAR,
   -- p_CURSOR       OUT SYS_REFCURSOR,
   -- p_DIA          OUT VARCHAR2,
   -- p_HORA         OUT VARCHAR2,
   -- p_ERROR        OUT NUMBER  )
   -- Output: p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Retorno: N/A.
   -- Observaciones: 14/09/2016, Creacion Procedimiento.
   --***********************************************************************************************
   PROCEDURE Sp_PAB_GESTION_CIERRE_MANUAL (
      p_COD_USR       IN     VARCHAR2,
      p_COD_SIS_ENT   IN     CHAR,
      p_CURSOR           OUT SYS_REFCURSOR,
      p_CURSOR2          OUT SYS_REFCURSOR,
      p_DIA              OUT VARCHAR2,
      p_HORA             OUT VARCHAR2,
      p_ERROR            OUT NUMBER
   );

   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_HIS_DET_MNSJE_CIERRE
   -- Objetivo: Al cierre del sistema se historifican las operaciones para la fecha ingresada
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas:PABS_DT_DETLL_MNSJE_PRTCZ_HTRCA, PABS_DT_DETLL_MNSJE_PRTCZ
   -- Fecha: 26/09/2016
   -- Autor: Santander
   -- Input: p_FEC_PCS -> Fecha Proceso.
   -- Output: p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: 27/04/2017, Creacion Procedimiento.
   --***********************************************************************************************
   PROCEDURE Sp_PAB_HIS_DET_MNSJE_CIERRE (p_FEC_PCS   IN     VARCHAR2,
                                          p_ERROR        OUT NUMBER);

   --**************************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ELI_900_910
   -- Objetivo: Al cierre del sistema se eliminan los registros de los protocolos 900/910
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_CARGO_ABONO_CAJA
   -- Fecha: 16/11/2017
   -- Autor: Gonzalo Correa
   -- Input: p_FEC_PCS -> Fecha Proceso.
   -- Output: p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: 28/11/2017, Creacion Procedimiento.
   --***************************************************************************************************************
   PROCEDURE Sp_PAB_ELI_900_910 (p_FEC_PCS IN VARCHAR2, p_ERROR OUT NUMBER);

   --**************************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ELI_MOV_CAJA
   -- Objetivo: Al cierre del sistema se eliminan los registros de la tabla Movimientos de Caja para evitar acumulación
   --          de registros, esto se hará eliminando registros que sean menores a la fecha de proceso en su fecha
   --           valuta
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_BTCOR_PRCSO, PABS_DT_BTCOR_DETLL_PRCSO, PABS_DT_DETLL_MVNTO_CAJA
   -- Fecha: 03/11/2017
   -- Autor: Santander-CAH
   -- Input: p_FEC_PCS -> Fecha Proceso.
   -- Output: p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: 14/11/2016, Creacion Procedimiento.
   --***************************************************************************************************************
   PROCEDURE Sp_PAB_ELI_MOV_CAJA (p_FEC_PCS IN VARCHAR2, p_ERROR OUT NUMBER);

   --**************************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ELI_CART
   -- Objetivo: Al cierre del sistema se eliminan los registros de la tabla CARTOLAS para evitar acumulación
   --          de registros, esto se hará eliminando registros que cumplan más de x días, para eliminar se crea la
   --          constante PKG_PAB_CONSTANTES.V_DIA_ELI_CAR que inicialmente tendra valor 7.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_BTCOR_PRCSO, PABS_DT_BTCOR_DETLL_PRCSO, PABS_DT_CBCRA_CRTLA, PABS_DT_DETLL_CRTLA
   -- Fecha: 03/11/2017
   -- Autor: Santander-CAH
   -- Input: p_FEC_PCS -> Fecha Proceso.
   -- Output: p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: 14/11/2016, Creacion Procedimiento.
   --***************************************************************************************************************
   PROCEDURE Sp_PAB_ELI_CART (p_FEC_PCS IN VARCHAR2, p_ERROR OUT NUMBER);

   --********************************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ELI_RGTRO_CARGO_ABONO
   -- Objetivo: Al cierre del sistema se eliminan los registros de la tabla de registro de cargo y abono del aplicativo
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_RGTRO_OPRCN_CARGO_ABON
   -- Fecha: 06-12-2017
   -- Autor: Santander - CAH
   -- Input: p_FEC_PCS -> Fecha Proceso.
   -- Output: p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: 06-12-2017, Creacion Procedimiento.
   --********************************************************************************************************************
   PROCEDURE Sp_PAB_ELI_RGTRO_CARGO_ABONO (p_FEC_PCS   IN     VARCHAR2,
                                           p_ERROR        OUT NUMBER);

   --**************************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ACT_SLDO_MX
   -- Objetivo: PROCEDIMIENTO QUE ACTUALIZA LOS SALDOS AL CIERRE DEL DIA, CAMBIANDO LOS SALDOS PROYECTADOS DIA, 24H, 48H.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_BTCOR_PRCSO, PABS_DT_BTCOR_DETLL_PRCSO, PABS_DT_CBCRA_CRTLA, PABS_DT_DETLL_CRTLA
   -- Fecha: 03/11/2017
   -- Autor: Santander-CAH
   -- Input: N/A
   -- Output: p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: 14/11/2016, Creacion Procedimiento.
   --***************************************************************************************************************
   PROCEDURE Sp_PAB_ACT_SLDO_MX (p_ERROR OUT NUMBER);

   /**********************************************************************************************
    FUNCION/PROCEDIMIENTO: SP_PAB_ACT_900_910.
    OBJETIVO             : ACTUALIZA EL CAMPO COD_TPO_OPE_AOS A 'OOFF' CUANDO EL MT := 900/910
                           Y EL CANAL DE ENTRADA O SALIDA SEA NULL.
    SISTEMA              : DBO_PAB.
    BASE DE DATOS        : DBO_PAB.
    TABLAS USADAS        : PABS_DT_DETLL_CARGO_ABONO_CAJA.
    FECHA                : 17/08/2018.
    AUTOR                : VARAYA.
    INPUT                : P_FEC_PCS := FECHA PROCESO.
    OUTPUT               : P_ERROR   := INDICADOR DE ERRORES, MANEJA DOS ESTADOS, 0 SIN ERRORES 1
                           EXISTEN ERRORES.
                           P_ERR_MSG := GLOSA QUE INDICA EL ERROR.
    OBSERVACIONES        : 17-08-2018, CREACIÓN SP.
   ***********************************************************************************************/
   PROCEDURE SP_PAB_ACT_900_910 (P_FEC_PCS IN VARCHAR2, P_ERROR OUT NUMBER);

   --************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_ELI_PAGOS_ACMDO
   -- Objetivo: Al cierre del sistema se eliminan los registros del los pagos realizados
   --        que se conservan por mas 30 días
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_PAGOS_RLZAR_ACMDO
   -- Fecha: 02-10-2018
   -- Autor: Santander CAH
   -- Input: p_FEC_PCS -> Fecha Proceso.
   -- Output: p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: 02-10-2018, Creacion Procedimiento.
   --***********************************************************************************************
   PROCEDURE SP_PAB_ELI_PAGOS_ACMDO (p_FEC_PCS   IN     VARCHAR2,
                                     p_ERROR        OUT NUMBER);

   /*******************************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_ACT_SALDOS_DARIO
   -- Objetivo: Procedimiento almacenado que recorre los saldos diarios e inserta maximos y minimos.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_MONDA_ALMOT
   --
   -- Fecha: 08/08/2018
   -- Autor: Santander CAH
   -- Input:
   --      p_FEC_PCS -> Fecha Proceso.
   -- Output:
   --      p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --*****************************************************************************************************************/
   PROCEDURE SP_PAB_ACT_SALDOS_DARIO (p_FEC_PCS   IN     VARCHAR2,
                                      p_ERROR        OUT NUMBER);

   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ELI_CONTA
   -- Objetivo: Al cierre del sistema se eliminan los registros de la contabilidad, se conservan X dias segun
   --           lo indicado por la constante PKG_PAB_CONSTANTES.V_DIA_CONT_HIS.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_CNTBL_ALMOT
   -- Fecha: 27/12/2018
   -- Autor: Santander
   -- Input: p_FEC_PCS -> Fecha Proceso.
   -- Output: p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones:
   --***********************************************************************************************
   PROCEDURE Sp_PAB_ELI_CONTA (p_FEC_PCS IN VARCHAR2, p_ERROR OUT NUMBER);

   /******************************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_GESTN_PART_SALDO_HTRCO
   -- Objetivo: Procedimiento almacenado gestor que se ejecuta para crear o eliminar particiones de la tabla
   --           PABS_DT_RGTRO_SALDO_HTRCO , Se ejecutrá al cierre del sistema.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_RGTRO_SALDO_HTRCO
   --
   -- Fecha: 11-02-2019
   -- Autor: Santander CAH
   -- Input:
   --      p_FEC_PCS   Fecha de Proceso
   --
   -- Output:
   --
   --       p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --*****************************************************************************************************************/
   PROCEDURE SP_PAB_GESTN_PART_SALDO_HTRCO (p_FEC_PCS   IN     VARCHAR2,
                                            p_ERROR        OUT NUMBER);
END PKG_PAB_CIERRE;