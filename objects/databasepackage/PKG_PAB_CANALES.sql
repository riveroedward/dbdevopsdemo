CREATE OR REPLACE PACKAGE         PKG_PAB_CANALES
AS
   /**  Package el cual contrendra todas las funciones y procedimientos relacionados con los perfiles
   -- @Santander */

   /***************************VARIABLE GLOBALES************************************/
   v_NOM_PCK       VARCHAR2 (30) := 'PKG_PAB_CANALES';
   v_NOM_SP        VARCHAR2 (30);
   ERR_CODE        NUMBER := 0;
   ERR_MSG         VARCHAR2 (300);
   v_DES           VARCHAR2 (300);
   v_DES_BIT       VARCHAR2 (100);
   v_COD_USU       CHAR (11);
   p_s_mensaje     VARCHAR2 (400);
   v_BANCO         VARCHAR2 (9) := '970150005';
   v_largoCuenta   NUMBER := 14;
   v_RESULT        NUMBER;
   v_dia_ini       DATE := TRUNC (SYSDATE) + INTERVAL '1' MINUTE;
   v_dia_fin DATE
         := TRUNC (SYSDATE) + INTERVAL '23' HOUR + INTERVAL '59' MINUTE ;

   /**************************FIN VARIABLE GLOBALES********************************/

   --***********************************************************************************************
   -- Funcion/Procedimiento: SP_EST_OPE_AOS
   -- Objetivo: Procedimiento que retorna informacion de una operacion DCV en Opreaciones
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: SP_EST_OPE_AOS
   -- Fecha: 25/07/17
   -- Autor: Santander
   -- Input:
   -- p_CVL_NEG -> Clave de negocios informada para DCV
   -- p_MOV_CAN -> Movimiento de operación
   -- p_IMP_MTO -> Monto de la operación
   -- p_NUM_OPE -> Numero de Operacion
   -- Output:
   -- p_EST_PROC      -> Estado de proceso
   -- p_NUM_RESP_PROC -> Codigo Respuesta si existe del proceso
   -- p_NUM_DOC_BEN   -> Mensaje de error / Confirmacion de datos
   -- p_NUM_RESP_DOC  -> Codigo Respuesta si existe el Numero de Operacion
   -- p_CTA_BEN       -> Mensaje de error / Confirmacion de datos
   -- p_NUM_RESP_CTA  -> Codigo Respuesta si existe el Numero de Operacion
   -- p_ERR_CODE      -> Codigo de error de ejecucion (retorno para TIBCO)
   -- p_ERR_MSG       -> mensaje de error de ejecucion (retorno para TIBCO)
   -- p_ERROR         -> codigo de error de ejecucion de PL (para log)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_EST_OPE_AOS (p_CVL_NEG         IN     VARCHAR2,
                             p_MOV_CAN         IN     CHAR,
                             p_IMP_MTO         IN     NUMBER,
                             p_NUM_OPE         IN     VARCHAR2,
                             p_EST_PROC           OUT VARCHAR2,
                             p_NUM_RESP_PROC      OUT NUMBER,
                             p_NUM_DOC_BEN        OUT VARCHAR2,
                             p_NUM_RESP_DOC       OUT NUMBER,
                             p_CTA_BEN            OUT VARCHAR2,
                             p_NUM_RESP_CTA       OUT NUMBER,
                             p_ERR_CODE           OUT VARCHAR2,
                             p_ERR_MSG            OUT VARCHAR2,
                             p_ERROR              OUT NUMBER);

   --***********************************************************************************************
   -- Funcion/Procedimiento: SP_ING_DVP_AEN
   -- Objetivo: Procedimiento que ingresa operaciones provenientes de CUSTODIA (CARMEN)
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: SP_ING_DVP_AEN
   -- Fecha: 25/07/17
   -- Autor: Santander
   -- Input:
   -- p_NUM_OPE        -> numero de Operacion
   -- p_NUM_DOC_BNF    -> numero de rut beneficiario
   -- p_NOM_BNF        -> nombre beneficiario
   -- p_CTA_BNF        -> cuenta beneficiario
   -- p_NUM_DOC_ORD    -> numero de rut ordenante
   -- p_NOM_ORD        -> nombre ordenante
   -- p_CTA_ORD        -> cuenta ordenante
   -- p_FEC_PAG        -> fecha de pago
   -- p_FEC_VAL        -> fecha de valuta
   -- p_MTO_EGR        -> monto egreso
   -- p_MTO_ING        -> monto ingreso
   -- p_COD_DVI        -> codigo divisa
   -- p_COD_SIS_ORI    -> codifo sistema origen
   -- p_DOC_EMP        -> rut empresa
   -- p_REF_EXT        -> referencia externa
   -- p_COD_REF        -> codigo referencia
   -- p_RUT_BCO_DTN    -> rut banco destino
   -- p_COD_SWF_DTN    -> codigo swift destino
   -- p_NUM_SUC        -> numero de sucursar
   -- p_GLOSA          -> glosa operacion
   -- p_CANAL_ENT      -> canal de entrada
   -- p_FORMA_PAGO     -> forma de pago operacion
   -- p_ClaveNeg01     -> clave de negocio (1)
   -- p_ClaveNeg02     -> clave de negocio (2)
   -- p_ClaveNeg03     -> clave de negocio (3)
   -- p_ClaveNeg04     -> clave de negocio (4)
   -- p_ClaveNeg05     -> clave de negocio (5)
   -- p_ClaveNeg06     -> clave de negocio (6)
   -- p_ClaveNeg07     -> clave de negocio (7)
   -- p_ClaveNeg08     -> clave de negocio (8)
   -- p_ClaveNeg09     -> clave de negocio (9)
   -- p_ClaveNeg10     -> clave de negocio (10)
   -- p_CtaDcv_Ben     -> cuenta dvp beneficiario
   -- p_CtaDcv_Ord     -> cuenta dvp ordenante
   -- p_COD_USR        -> codigo usuario
   -- p_Num_SUC_Dest   -> sucursal destino
   -- p_Cod_TransPago  -> codigo transferencia de pago
   -- p_Msg_Rel        -> mensaje relacionado
   -- Output:
   -- p_ERR_CODE      -> codigo de error de ejecucion (retorno para TIBCO)
   -- p_ERR_MSG       -> mensaje de error de ejecucion (retorno para TIBCO)
   -- p_ERROR         -> codigo de error de ejecucion de PL (para log)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_ING_DVP_AEN (p_NUM_OPE         IN     VARCHAR2,
                             p_NUM_DOC_BNF     IN     VARCHAR2,
                             p_NOM_BNF         IN     VARCHAR2,
                             p_CTA_BNF         IN     VARCHAR2,
                             p_NUM_DOC_ORD     IN     VARCHAR2,
                             p_NOM_ORD         IN     VARCHAR2,
                             p_CTA_ORD         IN     VARCHAR2,
                             p_FEC_PAG         IN     VARCHAR2,
                             p_FEC_VAL         IN     VARCHAR2,
                             p_MTO_EGR         IN     CHAR,
                             p_MTO_ING         IN     CHAR,
                             p_COD_DVI         IN     CHAR,
                             p_COD_SIS_ORI     IN     CHAR,
                             p_DOC_EMP         IN     VARCHAR2,
                             p_REF_EXT         IN     VARCHAR2,
                             p_COD_REF         IN     CHAR,
                             p_RUT_BCO_DTN     IN     VARCHAR2,
                             p_COD_SWF_DTN     IN     VARCHAR2,
                             p_NUM_SUC         IN     VARCHAR2,
                             p_GLOSA           IN     VARCHAR2,
                             p_CANAL_ENT       IN     VARCHAR2,
                             p_FORMA_PAGO      IN     VARCHAR2,
                             p_ClaveNeg01      IN     CHAR,
                             p_ClaveNeg02      IN     CHAR,
                             p_ClaveNeg03      IN     CHAR,
                             p_ClaveNeg04      IN     CHAR,
                             p_ClaveNeg05      IN     CHAR,
                             p_ClaveNeg06      IN     CHAR,
                             p_ClaveNeg07      IN     CHAR,
                             p_ClaveNeg08      IN     CHAR,
                             p_ClaveNeg09      IN     CHAR,
                             p_ClaveNeg10      IN     CHAR,
                             p_CtaDcv_Ben      IN     VARCHAR2,
                             p_CtaDcv_Ord      IN     VARCHAR2,
                             p_COD_USR         IN     VARCHAR2,
                             p_Num_SUC_Dest    IN     VARCHAR2,
                             p_Cod_TransPago   IN     CHAR,
                             p_Msg_Rel         IN     CHAR,
                             p_ERR_CODE           OUT VARCHAR2,
                             p_ERR_MSG            OUT VARCHAR2,
                             p_ERROR              OUT NUMBER);

   --***********************************************************************************************
   -- Funcion/Procedimiento: SP_ACT_EST_OPE_DVP
   -- Objetivo: Procedimiento que actualiza en Altos Montos los estados de las operaciones DVP
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: SP_ACT_EST_OPE_DVP
   -- Fecha: 25/07/17
   -- Autor: Santander
   -- Input:
   -- p_NUM_OPE    --> Numero de operacion Origen
   -- p_FEC_ING    --> Fecha de ingreso operacion
   -- p_IMP_MTO    --> Monto de la operacion
   -- p_CLV_NGC    --> Clave de negocio
   -- p_REF_EXT    --> Referencia Externa
   -- p_COD_EST    --> Estado a actualizar
   -- Output:
   -- p_ERR_CODE      -> codigo de error de ejecucion (retorno para TIBCO)
   -- p_ERR_MSG       -> mensaje de error de ejecucion (retorno para TIBCO)
   -- p_ERROR         -> codigo de error de ejecucion de PL (para log)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   PROCEDURE SP_ACT_EST_OPE_DVP (p_NUM_OPE    IN     VARCHAR2,
                                 p_FEC_ING    IN     VARCHAR2,
                                 p_IMP_MTO    IN     VARCHAR2,
                                 p_CLV_NGC    IN     VARCHAR2,
                                 p_REF_EXT    IN     VARCHAR2,
                                 p_COD_EST    IN     VARCHAR2,
                                 p_ERR_CODE      OUT VARCHAR2,
                                 p_ERR_MSG       OUT VARCHAR2,
                                 p_ERROR         OUT NUMBER);

   --***********************************************************************************************
   -- Funcion/Procedimiento: SP_BUS_OPE_DVP
   -- Objetivo: Procedimiento que busca en Altos Montos las operaciones DVP
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: SP_ACT_EST_OPE_DVP
   -- Fecha: 25/07/17
   -- Autor: Santander
   -- Input:
   -- p_FEC_ING        -> Fecha de ingreso de la operacion
   -- p_NUM_DOC_CLI    -> numero de documento del cliente DVP
   -- p_CANT_REG       -> Cantidad de registros por pagina
   -- p_IND_RECALL     -> indicador si es rellamada.
   -- Output:
   -- p_OPERACIONES    -> Cursor con las operaciones encontradas
   -- p_id_recall      -> indicar si existen mas operaciones
   -- p_num_paginas    -> indicador de paginas
   -- p_ERR_CODE       -> codigo de error de ejecucion (retorno para TIBCO)
   -- p_ERR_MSG        -> mensaje de error de ejecucion (retorno para TIBCO)
   -- p_ERROR          -> codigo de error de ejecucion de PL (para log)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_BUS_OPE_DVP (p_FEC_ING       IN     VARCHAR2,
                             p_NUM_DOC_CLI   IN     VARCHAR2,
                             p_CANT_REG      IN     NUMBER,
                             p_IND_RECALL    IN     NUMBER,
                             p_OPERACIONES      OUT SYS_REFCURSOR,
                             p_id_recall        OUT NUMBER,
                             p_num_paginas      OUT NUMBER,
                             p_ERR_CODE         OUT VARCHAR2,
                             p_ERR_MSG          OUT VARCHAR2,
                             p_ERROR            OUT NUMBER);

   --***********************************************************************************************
   -- Funcion/Procedimiento: SP_VAL_DUP_DVP
   -- Objetivo: Procedimiento que busca operaciones duplicadas en Altos Montos
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: SP_ACT_EST_OPE_DVP
   -- Fecha: 25/07/17
   -- Autor: Santander
   -- Input:
   -- p_FEC_ING        -> Fecha de ingreso de la operacion
   -- p_IMP_MTO        -> Monto de la operacion
   -- p_CLV_NEG        -> Clave de negocio DVP
   -- Output:
   -- p_ERR_CODE       -> codigo de error de ejecucion (retorno para TIBCO)
   -- p_ERR_MSG        -> mensaje de error de ejecucion (retorno para TIBCO)
   -- p_ERROR          -> codigo de error de ejecucion de PL (para log)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_VAL_DUP_DVP (p_FEC_ING    IN     VARCHAR2,
                             p_IMP_MTO    IN     NUMBER,
                             p_CLV_NEG    IN     VARCHAR2,
                             p_ERR_CODE      OUT VARCHAR2,
                             p_ERR_MSG       OUT VARCHAR2,
                             p_ERROR         OUT NUMBER);

   --***********************************************************************************************
   -- Funcion/Procedimiento: SP_CON_OFF_BKN
   -- Objetivo: Procedimiento que consulta operaciones provenientes de OfficeBanking
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: SP_ACT_EST_OPE_DVP
   -- Fecha: 25/07/17
   -- Autor: Santander
   -- Input:
   -- p_COD_USR        -> codigo usuario
   -- p_NUM_DOC_USU    -> rut empresa
   -- p_NUM_DOC_CLI    -> rut cliente
   -- p_FEC_DSD        -> fecha desde
   -- p_FEC_HST        -> fecha hasta
   -- p_COD_SIS_ENT    -> codigo sistema de entrada
   -- p_NUM_CTA_CTE    -> numero de cuenta
   -- p_EST_AOS        -> estado operacion
   -- p_TIP_TRS        -> tipo de transaccion
   -- p_TIP_FLJ        -> tipo de flujo
   -- p_REG_PAG_SAL    -> numero de paginas de salida
   -- p_NUM_REG_PAG_SAL-> registros por pagina
   -- p_NUM_PAG_SAL    -> numero de paginas de salida
   -- Output:
   -- p_DET_OPE        -> Cursor con las operaciones encontradas
   -- p_REG_PAG        -> indicar si existen mas operaciones
   -- p_NUM_PAG        -> indicador de paginas
   -- p_ERR_CODE       -> codigo de error de ejecucion (retorno para TIBCO)
   -- p_ERR_MSG        -> mensaje de error de ejecucion (retorno para TIBCO)
   -- p_ERROR          -> codigo de error de ejecucion de PL (para log)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_CON_OFF_BKN (p_COD_USR           IN     VARCHAR2,
                             p_NUM_DOC_USU       IN     VARCHAR2,
                             p_NUM_DOC_CLI       IN     VARCHAR2,
                             p_FEC_DSD           IN     VARCHAR2,
                             p_FEC_HST           IN     VARCHAR2,
                             p_COD_SIS_ENT       IN     VARCHAR2,
                             p_NUM_CTA_CTE       IN     VARCHAR2,
                             p_EST_AOS           IN     NUMERIC,
                             p_TIP_TRS           IN     VARCHAR2,
                             p_TIP_FLJ           IN     VARCHAR2,
                             p_REG_PAG_SAL       IN     NUMERIC,
                             p_NUM_REG_PAG_SAL   IN     NUMERIC,
                             p_NUM_PAG_SAL       IN     NUMERIC,
                             p_DET_OPE              OUT SYS_REFCURSOR,
                             p_REG_PAG              OUT NUMERIC,
                             p_NUM_PAG              OUT NUMERIC,
                             p_ERR_CODE             OUT VARCHAR2,
                             p_ERR_MSG              OUT VARCHAR2,
                             p_ERROR                OUT NUMERIC);

   --***********************************************************************************************
   -- Funcion/Procedimiento: SP_ING_PAG_DVP
   -- Objetivo: Procedimiento que ingresa operaciones tipo DVP
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: SP_ACT_EST_OPE_DVP
   -- Fecha: 25/07/17
   -- Autor: Santander
   -- Input:
   -- p_NUM_DOC_BCO    -> Numero documento empresa
   -- p_COD_DVI        -> Codigo divisa
   -- p_IMP_MTO        -> Monto operacion
   -- p_FEC_ING        -> fecha de ingreso
   -- p_FEC_VTA        -> fecha de valuta
   -- p_NUM_DOC_BFN    -> numero documento beneficiario
   -- p_NOM_BFN        -> nombre beneficiario
   -- p_CTA_BFC        -> cuenta beneficiario
   -- p_NUM_DOC_ORD    -> numero documento ordenante
   -- p_NOM_ORD        -> nombre ordenante
   -- p_CTA_ORD        -> cuenta ordenante
   -- p_ID_DCVORO      -> Id DCV
   -- p_COD_SEMAT      -> codigo SEMA
   -- p_SUC_ORI        -> Sucursal origen
   -- p_COD_TSN        -> Codigo transaccion
   -- p_NUM_OPE        -> numero operacion
   -- p_REF_EXT        -> referencia externa
   -- p_GLS_ADC        -> glosa adicional
   -- p_CLV_NEG_01     -> Clave de negicio 1
   -- p_CLV_NEG_02     -> Clave de negicio 2
   -- p_CLV_NEG_03     -> Clave de negicio 3
   -- p_CLV_NEG_04     -> Clave de negicio 4
   -- p_CLV_NEG_05     -> Clave de negicio 5
   -- p_CLV_NEG_06     -> Clave de negicio 6
   -- p_CLV_NEG_07     -> Clave de negicio 7
   -- p_CLV_NEG_08     -> Clave de negicio 8
   -- p_CLV_NEG_09     -> Clave de negicio 9
   -- p_CLV_NEG_10     -> Clave de negicio 10
   -- Output:
   -- p_ERR_CODE       -> codigo de error de ejecucion (retorno para TIBCO)
   -- p_ERR_MSG        -> mensaje de error de ejecucion (retorno para TIBCO)
   -- p_ERROR          -> codigo de error de ejecucion de PL (para log)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_ING_PAG_DVP (p_NUM_DOC_BCO   IN     VARCHAR2,
                             p_COD_DVI       IN     VARCHAR2,
                             p_IMP_MTO       IN     NUMERIC,
                             p_FEC_ING       IN     VARCHAR2,
                             p_FEC_VTA       IN     VARCHAR2,
                             p_NUM_DOC_BFN   IN     VARCHAR2,
                             p_NOM_BFN       IN     VARCHAR2,
                             p_CTA_BFC       IN     VARCHAR2,
                             p_NUM_DOC_ORD   IN     VARCHAR2,
                             p_NOM_ORD       IN     VARCHAR2,
                             p_CTA_ORD       IN     VARCHAR2,
                             p_ID_DCVORO     IN     VARCHAR2,
                             p_COD_SEMAT     IN     VARCHAR2,
                             p_SUC_ORI       IN     VARCHAR2,
                             p_COD_TSN       IN     VARCHAR2,
                             p_NUM_OPE       IN     VARCHAR2,
                             p_REF_EXT       IN     VARCHAR2,
                             p_GLS_ADC       IN     VARCHAR2,
                             p_CLV_NEG_01    IN     VARCHAR2,
                             p_CLV_NEG_02    IN     VARCHAR2,
                             p_CLV_NEG_03    IN     VARCHAR2,
                             p_CLV_NEG_04    IN     VARCHAR2,
                             p_CLV_NEG_05    IN     VARCHAR2,
                             p_CLV_NEG_06    IN     VARCHAR2,
                             p_CLV_NEG_07    IN     VARCHAR2,
                             p_CLV_NEG_08    IN     VARCHAR2,
                             p_CLV_NEG_09    IN     VARCHAR2,
                             p_CLV_NEG_10    IN     VARCHAR2,
                             p_ERR_CODE         OUT VARCHAR2,
                             p_ERR_MSG          OUT VARCHAR2,
                             p_ERROR            OUT NUMBER);

   --***********************************************************************************************
   -- Funcion/Procedimiento: SP_ING_PAG
   -- Objetivo: Procedimiento que ingresa operaciones tipo LBTR
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: SP_ACT_EST_OPE_DVP
   -- Fecha: 25/07/17
   -- Autor: Santander
   -- Input:
   -- p_NUM_DOC_BCO    -> Numero documento empresa
   -- p_COD_DVI        -> Codigo divisa
   -- p_IMP_MTO        -> Monto operacion
   -- p_FEC_ING        -> fecha de ingreso
   -- p_FEC_VTA        -> fecha de valuta
   -- p_NUM_DOC_BFN    -> numero documento beneficiario
   -- p_NOM_BFN        -> nombre beneficiario
   -- p_CTA_BFC        -> cuenta beneficiario
   -- p_NUM_DOC_ORD    -> numero documento ordenante
   -- p_NOM_ORD        -> nombre ordenante
   -- p_CTA_ORD        -> cuenta ordenante
   -- p_COD_SIS_ORI    -> Codigo sistema origen
   -- p_SUC_ORI        -> Sucursal origen
   -- p_COD_TR_PAGO    -> Codigo transaccion
   -- p_NUM_OPE        -> numero operacion
   -- p_COD_RFR        -> refenrencia externa
   -- p_REF_EXT        -> Codifo referencia
   -- p_CAN_PAG        -> Cantidad de paginas
   -- p_CTA_CTB        -> referencia externa
   -- p_GLS_ADC        -> glosa adicional
   -- Output:
   -- p_ERR_CODE       -> codigo de error de ejecucion (retorno para TIBCO)
   -- p_ERR_MSG        -> mensaje de error de ejecucion (retorno para TIBCO)
   -- p_ERROR          -> codigo de error de ejecucion de PL (para log)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_ING_PAG (p_NUM_DCO_BCO   IN     VARCHAR2,
                         p_COD_DVI       IN     VARCHAR2,
                         p_IMP_MTO       IN     NUMERIC,
                         p_FEC_ING       IN     VARCHAR2,
                         p_FEC_VTA       IN     VARCHAR2,
                         p_NUM_DCO_BNF   IN     VARCHAR2,
                         p_NOM_BNF       IN     VARCHAR2,
                         p_CTA_BNF       IN     VARCHAR2,
                         p_NUM_DCO_ODN   IN     VARCHAR2,
                         p_NOM_ODN       IN     VARCHAR2,
                         p_CTA_ODN       IN     VARCHAR2,
                         p_COD_SIS_ORI   IN     VARCHAR2,
                         p_SUC_ORI       IN     NUMERIC,
                         p_COD_TR_PAGO   IN     VARCHAR2,
                         p_NUM_OPE       IN     VARCHAR2,
                         p_COD_RFR       IN     VARCHAR2,
                         p_REF_EXT       IN     VARCHAR2,
                         p_CAN_PAG       IN     NUMERIC,
                         p_CTA_CTB       IN     VARCHAR2,
                         p_GLS_ADIC      IN     VARCHAR2,
                         p_ERR_CODE         OUT VARCHAR2,
                         p_ERR_MSG          OUT VARCHAR2,
                         p_ERROR            OUT NUMBER);

   --***********************************************************************************************
   -- Funcion/Procedimiento: SP_ING_OPE_PLZ
   -- Objetivo: Procedimiento que ingresa numero de vale vista relacionado a operacion de officebanking
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: SP_ACT_EST_OPE_DVP
   -- Fecha: 25/07/17
   -- Autor: Santander
   -- Input:
   -- p_NUM_OPE        -> numero de OfficeBankin
   -- p_NUM_OPE_ORI    -> numero VV plazo
   -- p_COD_DVI        -> codigo divisa
   -- p_IMP_MTO        -> monto operacion
   -- p_FEC_ING        -> fecha de ingreso
   -- p_CLV_NEG        -> clave de negocio
   -- Output:
   -- p_ERR_CODE       -> codigo de error de ejecucion (retorno para TIBCO)
   -- p_ERR_MSG        -> mensaje de error de ejecucion (retorno para TIBCO)
   -- p_ERROR          -> codigo de error de ejecucion de PL (para log)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_ING_OPE_PLZ (p_NUM_OPE       IN     NUMBER,
                             p_NUM_OPE_ORI   IN     NUMBER,
                             p_COD_DVI       IN     CHAR,
                             p_IMP_MTO       IN     NUMBER,
                             p_FEC_ING       IN     VARCHAR2,
                             p_CLV_NEG       IN     VARCHAR2,
                             p_ERR_CODE         OUT VARCHAR2,
                             p_ERR_MSG          OUT VARCHAR2,
                             p_ERROR            OUT NUMBER);

   --***********************************************************************************************
   -- Funcion/Procedimiento: SP_INS_LBTR
   -- Objetivo: Procedimiento que ingresa operacione desde sistema Custodia
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: SP_ACT_EST_OPE_DVP
   -- Fecha: 25/07/17
   -- Autor: Santander
   -- Input:
   -- p_NUM_DOC_BCO_DTN    -> Numero de documento destino
   -- p_COD_DVI            -> codigo divisa
   -- p_IMP_MTO            -> monto
   -- p_FEC_ING_PAG        -> fecha ingreso operacion
   -- p_FEC_VAL            -> fecha de valuta
   -- p_NUM_DOC_BNF        -> numero de documento beneficiario
   -- p_NOM_BNF            -> nombre beneficiario
   -- p_CTA_BNF            -> cuenta beneficiario
   -- p_NUM_DOC_ORD        -> numero documento ordenante
   -- p_NOM_ORD            -> nombre ordenante
   -- p_CTA_ORD            -> cuenta ordenante
   -- p_COD_ORI            -> codigo origen
   -- p_SUC_ORI            -> sucursal origen
   -- p_COD_TR_PA          -> codigo transaccion
   -- p_NUM_OPE            -> numero operacion origen
   -- p_COD_REF            -> codigo referencia
   -- p_REF_EXT            -> referencia externa
   -- p_CAN_CAN            -> cantidad de operaciones
   -- p_CTA_CNT            -> cuena contable
   -- p_COD_PAR            -> Codigo de pago
   -- p_GLO_GLO            -> Glosa operacion
   -- p_soma_01            -> clave negocio 1
   -- p_soma_02            -> clave negocio 2
   -- p_soma_03            -> clave negocio 3
   -- p_soma_04            -> clave negocio 4
   -- p_soma_05            -> clave negocio 5
   -- p_soma_06            -> clave negocio 6
   -- p_soma_07            -> clave negocio 7
   -- p_soma_08            -> clave negocio 8
   -- p_soma_09            -> clave negocio 9
   -- p_soma_10            -> clave negocio 10
   -- Output:
   -- p_ERR_CODE       -> codigo de error de ejecucion (retorno para TIBCO)
   -- p_ERR_MSG        -> mensaje de error de ejecucion (retorno para TIBCO)
   -- p_ERROR          -> codigo de error de ejecucion de PL (para log)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_INS_LBTR (p_NUM_DOC_BCO_DTN   IN     VARCHAR2,
                          p_COD_DVI           IN     CHAR,
                          p_IMP_MTO           IN     NUMERIC,
                          p_FEC_ING_PAG       IN     NUMERIC,
                          p_FEC_VAL           IN     VARCHAR2,
                          p_NUM_DOC_BNF       IN     VARCHAR2,
                          p_NOM_BNF           IN     VARCHAR2,
                          p_CTA_BNF           IN     VARCHAR2,
                          p_NUM_DOC_ORD       IN     VARCHAR2,
                          p_NOM_ORD           IN     VARCHAR2,
                          p_CTA_ORD           IN     VARCHAR2,
                          p_COD_ORI           IN     VARCHAR2,
                          p_SUC_ORI           IN     VARCHAR2,
                          p_COD_TR_PA         IN     VARCHAR2,
                          p_NUM_OPE           IN     CHAR,
                          p_COD_REF           IN     VARCHAR2,
                          p_REF_EXT           IN     VARCHAR2,
                          p_CAN_CAN           IN     VARCHAR2,
                          p_CTA_CNT           IN     CHAR,
                          p_COD_PAR           IN     VARCHAR2,
                          p_GLO_GLO           IN     VARCHAR2,
                          p_soma_01           IN     VARCHAR2,
                          p_soma_02           IN     VARCHAR2,
                          p_soma_03           IN     VARCHAR2,
                          p_soma_04           IN     VARCHAR2,
                          p_soma_05           IN     VARCHAR2,
                          p_soma_06           IN     VARCHAR2,
                          p_soma_07           IN     VARCHAR2,
                          p_soma_08           IN     VARCHAR2,
                          p_soma_09           IN     VARCHAR2,
                          p_soma_10           IN     VARCHAR2,
                          p_ERR_CODE             OUT VARCHAR2,
                          p_ERR_MSG              OUT VARCHAR2,
                          p_ERROR                OUT NUMBER);

   --***********************************************************************************************
   -- Funcion/Procedimiento: SP_OBT_MSJ_SWF_HST
   -- Objetivo: Procedimiento que ingresa operacione desde sistema Custodia
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: SP_ACT_EST_OPE_DVP
   -- Fecha: 25/07/17
   -- Autor: Santander
   -- Input:
   --.p_NUM_OPE        -> Numero de operacion
   -- Output:
   -- p_NUM_IDF_GTR_DOC-> numero identificatorio gestor documental
   -- p_ERR_CODE       -> codigo de error de ejecucion (retorno para TIBCO)
   -- p_ERR_MSG        -> mensaje de error de ejecucion (retorno para TIBCO)
   -- p_ERROR          -> codigo de error de ejecucion de PL (para log)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_OBT_MSJ_SWF_HST (p_NUM_OPE           IN     VARCHAR2,
                                 p_NUM_IDF_GTR_DOC      OUT VARCHAR2,
                                 p_ERR_CODE             OUT VARCHAR2,
                                 p_ERR_MSG              OUT VARCHAR2,
                                 p_ERROR                OUT NUMBER);


   --
   /************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUS_FLG_MT_TPO
   -- Objetivo: Procedimiento almacenado que Flag segun MT y tipo de OperaCion
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_BIBLI_CAMPO_OBLTO_MT
   --
   -- Fecha: 16/06/17
   -- Autor: Santander
   -- Input: p_COD_MT_SWF      Tipo MT Swift
   --        p_COD_TPO_OPE_AOS Tipo de Operacion
   --         p_FLG_MRD_UTZ_SWF     Tipo Mercado
   -- Output:
   --        p_CURSOR : SYS_REFCURSOR, resultados de consulta.
   --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_BUS_FLG_MT_TPO (p_COD_MT_SWF        IN     NUMBER,
                                    p_COD_TPO_OPE_AOS   IN     CHAR,
                                    p_FLG_MRD_UTZ_SWF   IN     NUMBER,
                                    p_CURSOR               OUT SYS_REFCURSOR,
                                    p_ERROR                OUT NUMBER);

   --

   /************************************************************************************************
   -- Funcion/Procedimiento: Sp_INS_OPE_DCV_AUT
   -- Objetivo: Procedimiento que ingresa operaciones del DCV automaticos
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_BIBLI_CAMPO_OBLTO_MT
   --
   -- Fecha: 16/06/17
   -- Autor: SII GROUP
   -- Input:
   -- p_NUM_IDF_SWF        -> Numero identificador de swift
   -- p_NUM_SSN_SWF        -> Numero de sesion de swift
   -- p_FEC_ISR_MSJ_SWF    -> numero identificador del mensaje
   -- p_NUM_REF_IDE_DCV    -> numero referencia DCV
   -- p_NUM_MSJ_SWF_RLD    -> numero referencia relacionada
   -- p_FEC_RCP_MSJ        -> numero recepcion mensaje
   -- p_COD_DVI            -> codigo divisa
   -- p_IMP_MTO            -> monto operacion
   -- p_NUM_CTA_ODN        -> numero cuenta ordenante
   -- p_NUM_DOC_ODN        -> numero documento ordenante
   -- p_NUM_CTA_DCV_ODN    -> numero de cuenta dcv ordenante
   -- p_NOM_ODN            -> nombre ordenante
   -- p_COD_BCO_DTN        -> codigo banco destino (BIC)
   -- p_NUM_CTA_BFC        -> numero cuenta beneficiario
   -- p_NUM_DOC_BFC        -> numero documento beneficiario
   -- p_NUM_CTA_DCV_BFC    -> numero cuenta DCV beneficiario
   -- p_NOM_BFC            -> nombre beneficiario
   -- p_NUM_CLV_NGC        -> clave de negocio
   -- p_COD_EST_OFB        -> codifo estado operacion ofice
   -- p_COD_OPE_RSP_MPT    -> codigo de respuesta proceso
   -- p_GLS_OPE_RSP_MPT    -> glosa respuesta proceso
   -- Output:
   -- p_ERROR              -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_INS_OPE_DCV_AUT (p_NUM_IDF_SWF       IN     NUMBER,
                                 p_NUM_SSN_SWF       IN     NUMBER,
                                 p_FEC_ISR_MSJ_SWF   IN     DATE,
                                 p_NUM_REF_IDE_DCV   IN     VARCHAR2,
                                 p_NUM_MSJ_SWF_RLD   IN     VARCHAR2,
                                 p_FEC_RCP_MSJ       IN     DATE,
                                 p_COD_DVI           IN     CHAR,
                                 p_IMP_MTO           IN     NUMBER,
                                 p_NUM_CTA_ODN       IN     VARCHAR2,
                                 p_NUM_DOC_ODN       IN     CHAR,
                                 p_NUM_CTA_DCV_ODN   IN     VARCHAR2,
                                 p_NOM_ODN           IN     VARCHAR2,
                                 p_COD_BCO_DTN       IN     VARCHAR2,
                                 p_NUM_CTA_BFC       IN     VARCHAR2,
                                 p_NUM_DOC_BFC       IN     CHAR,
                                 p_NUM_CTA_DCV_BFC   IN     VARCHAR2,
                                 p_NOM_BFC           IN     VARCHAR2,
                                 p_NUM_CLV_NGC       IN     VARCHAR2,
                                 p_COD_EST_OFB       IN     CHAR,
                                 p_COD_OPE_RSP_MPT   IN     CHAR,
                                 p_GLS_OPE_RSP_MPT   IN     VARCHAR2,
                                 p_ERROR                OUT NUMBER);

   /************************************************************************************************
   -- Funcion/Procedimiento: Sp_BUS_CAM_OBT_SWF
   -- Objetivo: Procedimiento que ingresa operaciones del DCV automaticos
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_BIBLI_CAMPO_OBLTO_MT
   --
   -- Fecha: 16/06/17
   -- Autor: SII GROUP
   -- Input:
   -- p_COD_MT_SWF         -> Codigo MT a validar
   -- p_FLG_MRD_UTZ_SWF    -> flag de mercado del mensaje a validar
   -- p_COD_TPO_OPE_AOS    -> codigo operacion a validar
   -- Output:
   -- p_CAM_OBT            -> Retorna campos a validar.
   -- p_ERROR              -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_BUS_CAM_OBT_SWF (p_COD_MT_SWF        IN     NUMBER,
                                 p_FLG_MRD_UTZ_SWF   IN     NUMBER,
                                 p_COD_TPO_OPE_AOS   IN     VARCHAR2,
                                 p_CAM_OBT              OUT SYS_REFCURSOR,
                                 p_ERROR                OUT NUMBER);

   /************************************************************************************************
   -- Funcion/Procedimiento: Sp_BUS_MSJ_OBT_SWF
   -- Objetivo: Procedimiento que busca los mensajes a validar
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_BIBLI_CAMPO_OBLTO_MT
   --
   -- Fecha: 16/06/17
   -- Autor: SII GROUP
   -- Input: N/A
   -- Output:
   -- p_MSJ_OBT            -> Cursor de retorno de mensajes a validar.
   -- p_ERROR              -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_BUS_MSJ_OBT_SWF (p_MSJ_OBT   OUT SYS_REFCURSOR,
                                 p_ERROR     OUT NUMBER);

   /************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_GES_OPE_AGP
   -- Objetivo: Procedimiento que inserta la informacion que se encuentra en la nomina
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: N/A
   -- Fecha: 02/06/16
   -- Autor: Santander
   -- Input:     p_FEC_ISR_OPE     -> Fecha de insercion de la operación
   --            p_NUM_FOL_OPE     -> Numero Foliador de la operacion
   --            p_NUM_FOL_NMN     -> Numero Foliador de la nomina
   --            p_NUM_FOL_OPE_ORG -> Numero que tiene el numero original de la operacion relacionada
   --            p_NUM_OPE_SIS_ENT -> Numero de operacion de la operacion en el sistema origen
   --            p_FLG_EGR_ING     -> Flag de egreso e ingreso
   --            p_COD_SIS_SAL     -> Codigo de sistema de salida
   --            p_COD_SIS_ENT     -> Codigo de sistema de entrada
   --            p_COD_EST_OPE     -> Estado de la operación
   --            p_COD_TPO_ING     -> Tipo de ingreso
   --            p_COD_MT_SWF      -> Numero del tipo de mensaje swift
   --            p_FLG_MRD_UTZ_SWF -> Flasg de mercado
   --            p_COD_TPO_OPE_AOS -> Tipo de operacion.
   --            p_FEC_VTA         -> Fecha de vencimiento
   --            p_COD_DVI         -> Codigo de la divisa
   --            p_IMp_OPE         -> Importe de la operacion
   --            p_COD_BCO_DTN     -> BIC del banco destino
   --            p_COD_BCO_ORG     -> BIC Banco Origen
   --            p_FEC_ENV_RCP_SWF -> Fecha de envio / recepcion swfit
   --            p_HOR_ENV_RCP_SWF -> Hora de envio / recepcion swfit
   --            p_NUM_REF_SWF     -> Numero de referencia Swift
   --            p_NUM_REF_EXT     -> Numero de referencia swift externa
   --            p_NUM_DOC_BFC     -> Numero rut del beneficiario
   --            p_COD_TPD_BFC     -> Rut tipo de beneficiario
   --            p_NOM_BFC         -> Nombre del Beneficiario
   --            p_NUM_CTA_BFC     -> Numero de cuenta del beneficiario
   --            p_COD_SUC         -> Codigo de la sucursal
   --            p_GLS_ADC_EST     -> Informacion adicional
   --            p_NUM_REF_CTB     -> Numero de referencia contable
   --            p_COD_BCO_BFC     -> BIB Banco Beneficario
   --            p_COD_BCO_ITD     -> BIC Banco Intermediario
   --            p_NUM_CTA_DCv_BFC -> Numero cuenta DCV Beneficiario
   --            p_GLS_DIR_BFC     -> Direccion Beneficario
   --            p_NOM_CDD_BFC     -> Nombre ciudad beneficiario
   --            p_COD_PAS_BFC     -> Codigo pais beneficiario
   --            p_NOM_ODN         -> Nombre ordenante
   --            p_NUM_DOC_ODN     -> Numero rut ordenante
   --            p_COD_TPD_ODN     -> Tipo documento de ordenante
   --            p_NUM_CTA_ODN     -> Numero cuenta ordenante
   --            p_NUM_CTA_DCv_ODN -> Numero cuenta DCV ordenante
   --            p_GLS_DIR_ODN     -> Direccion ordenante
   --            p_NOM_CDD_ODN     -> Nombre ciudad ordenante
   --            p_COD_PAS_ODN     -> Codigo pais ordenante
   --            p_NUM_CLv_NGC     -> Numero de clave de negocio
   --            p_NUM_AGT         -> Numero de agente
   --            p_COD_FND_CCLV    -> Codigo Fondo (Producto CCLV)
   --            p_COD_TPO_SDO     -> Tipo de saldo
   --            p_COD_TPO_CMA     -> Tipo de camara
   --            p_COD_TPO_FND     -> Tipo de fondo
   --            p_FEC_TPO_OPE_CCLV-> Fecha operacion CCLV
   --            p_NUM_CLv_IDF     -> Numero clave identificatorio
   --            p_OBS_OPC_SWF   -> Observacion Opcional Swift
   --            p_NUM_IDF_GTR_DOC -> Identificador gestor documental
   --            p_NUM_MVT         -> Numero de Movimiento (Vigente, Cuenta Corriente)
   --            p_COD_USR     -> rut usuario
   -- Output:
   --          p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_GES_OPE_AGP (p_FEC_ISR_OPE        IN     DATE,
                                 p_NUM_FOL_OPE        IN     NUMBER,
                                 p_NUM_FOL_NMN        IN     NUMBER,
                                 p_NUM_FOL_OPE_ORG    IN     NUMBER,
                                 p_NUM_OPE_SIS_ENT    IN     CHAR,
                                 p_FLG_EGR_ING        IN     NUMBER,
                                 p_COD_SIS_SAL        IN     CHAR,
                                 p_COD_SIS_ENT        IN     CHAR,
                                 p_COD_EST_OPE        IN     NUMBER,
                                 p_COD_TPO_ING        IN     NUMBER,
                                 p_COD_MT_SWF         IN     NUMBER,
                                 p_FLG_MRD_UTZ_SWF    IN     NUMBER,
                                 p_COD_TPO_OPE_AOS    IN     CHAR,
                                 p_FEC_VTA            IN     DATE,
                                 p_COD_DVI            IN     CHAR,
                                 p_IMp_OPE            IN     NUMBER,
                                 p_COD_BCO_DTN        IN     CHAR,
                                 p_COD_BCO_ORG        IN     CHAR,
                                 p_FEC_ENV_RCP_SWF    IN     DATE,
                                 p_HOR_ENV_RCP_SWF    IN     NUMBER,
                                 p_NUM_REF_SWF        IN     CHAR,
                                 p_NUM_REF_EXT        IN     CHAR,
                                 ------------Beneficiario------------
                                 p_NUM_DOC_BFC        IN     CHAR,
                                 p_COD_TPD_BFC        IN     CHAR,
                                 p_NOM_BFC            IN     VARCHAR2,
                                 p_NUM_CTA_BFC        IN     VARCHAR2,
                                 -------------------------------------
                                 p_COD_SUC            IN     VARCHAR2,
                                 p_GLS_ADC_EST        IN     VARCHAR2,
                                 p_NUM_REF_CTB        IN     CHAR,
                                 ----------Opcionales--------------
                                 p_COD_BCO_BFC        IN     CHAR,
                                 p_COD_BCO_ITD        IN     CHAR,
                                 p_NUM_CTA_DCv_BFC    IN     VARCHAR2,
                                 p_GLS_DIR_BFC        IN     VARCHAR2,
                                 p_NOM_CDD_BFC        IN     VARCHAR2,
                                 p_COD_PAS_BFC        IN     CHAR,
                                 p_NOM_ODN            IN     VARCHAR2,
                                 p_NUM_DOC_ODN        IN     CHAR,
                                 p_COD_TPD_ODN        IN     CHAR,
                                 p_NUM_CTA_ODN        IN     VARCHAR2,
                                 p_NUM_CTA_DCv_ODN    IN     VARCHAR2,
                                 p_GLS_DIR_ODN        IN     VARCHAR2,
                                 p_NOM_CDD_ODN        IN     VARCHAR2,
                                 p_COD_PAS_ODN        IN     CHAR,
                                 p_NUM_CLv_NGC        IN     VARCHAR2,
                                 p_NUM_AGT            IN     CHAR,
                                 p_COD_FND_CCLV       IN     CHAR,
                                 p_COD_TPO_SDO        IN     CHAR,
                                 p_COD_TPO_CMA        IN     CHAR,
                                 p_COD_TPO_FND        IN     VARCHAR2,
                                 p_FEC_TPO_OPE_CCLV   IN     DATE,
                                 p_NUM_CLv_IDF        IN     VARCHAR2,
                                 p_OBS_OPC_SWF        IN     VARCHAR2,
                                 p_NUM_IDF_GTR_DOC    IN     VARCHAR2,
                                 p_NUM_MVT            IN     CHAR,
                                 --------------------------------
                                 p_COD_USR            IN     CHAR,
                                 p_ERROR                 OUT NUMBER);

   --***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_BUS_OPE_PLZ
   -- Objetivo: Procedimiento que BUSCA numero de plazo para actualizar estado
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN_PLAZO
   -- Fecha: 25/07/17
   -- Autor: Santander
   -- Input:
   -- p_NUM_OPE_OFB : numero de operacion OfficeBanking
   -- Output:
   -- p_NUM_OPE_PZO : numero de vale vista Plazo
   -- p_ERR_CODE       -> codigo de error de ejecucion (retorno para TIBCO)
   -- p_ERR_MSG        -> mensaje de error de ejecucion (retorno para TIBCO)
   -- p_ERROR          -> codigo de error de ejecucion de PL (para log)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_PAB_BUS_OPE_PLZ (p_NUM_OPE_OFB   IN     VARCHAR2,
                                 p_NUM_OPE_PZO      OUT VARCHAR2,
                                 p_ERR_CODE         OUT VARCHAR2,
                                 p_ERR_MSG          OUT VARCHAR2,
                                 p_ERROR            OUT NUMBER);

   /************************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_COR_CTA
   -- Objetivo: Corta la cuenta corriente
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: N/A
   -- Fecha: 02/06/16
   -- Autor: Santander
   -- Input:      p_NUM_CTA: Numero Cuenta Corriente
   --             p_LARGO:  Largo necesario de la cuenta
   -- Output:
   --
   -- Input/Output: N/A
   -- Retorno: Retorna la cuenta cortada segun lo solicitado
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   FUNCTION FN_PAB_COR_CTA (p_NUM_CTA IN VARCHAR2, p_LARGO IN NUMBER)
      RETURN VARCHAR2;

   --***********************************************************************************************
   -- Funcion/Procedimiento: SP_ING_PAG
   -- Objetivo: Procedimiento que ingresa operaciones tipo LBTR
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: SP_ACT_EST_OPE_DVP
   -- Fecha: 25/07/17
   -- Autor: Santander
   -- Input:
   -- p_NUM_DOC_BCO    -> Numero documento empresa
   -- p_COD_DVI        -> Codigo divisa
   -- p_IMP_MTO        -> Monto operacion
   -- p_FEC_ING        -> fecha de ingreso
   -- p_FEC_VTA        -> fecha de valuta
   -- p_NUM_DOC_BFN    -> numero documento beneficiario
   -- p_NOM_BFN        -> nombre beneficiario
   -- p_CTA_BFC        -> cuenta beneficiario
   -- p_NUM_DOC_ORD    -> numero documento ordenante
   -- p_NOM_ORD        -> nombre ordenante
   -- p_CTA_ORD        -> cuenta ordenante
   -- p_COD_SIS_ORI    -> Codigo sistema origen
   -- p_SUC_ORI        -> Sucursal origen
   -- p_COD_TR_PAGO    -> Codigo transaccion
   -- p_NUM_OPE        -> numero operacion
   -- p_COD_RFR        -> refenrencia externa
   -- p_REF_EXT        -> Codifo referencia
   -- p_CAN_PAG        -> Cantidad de paginas
   -- p_CTA_CTB        -> referencia externa
   -- p_GLS_ADC        -> glosa adicional
   -- Output:
   -- p_ERR_CODE       -> codigo de error de ejecucion (retorno para TIBCO)
   -- p_ERR_MSG        -> mensaje de error de ejecucion (retorno para TIBCO)
   -- p_ERROR          -> codigo de error de ejecucion de PL (para log)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************

   PROCEDURE SP_ING_PAG_OB (p_NUM_DCO_BCO   IN     VARCHAR2,
                            p_COD_DVI       IN     VARCHAR2,
                            p_IMP_MTO       IN     NUMERIC,
                            p_FEC_ING       IN     VARCHAR2,
                            p_FEC_VTA       IN     VARCHAR2,
                            p_NUM_DCO_BNF   IN     VARCHAR2,
                            p_NOM_BNF       IN     VARCHAR2,
                            p_CTA_BNF       IN     VARCHAR2,
                            p_NUM_DCO_ODN   IN     VARCHAR2,
                            p_NOM_ODN       IN     VARCHAR2,
                            p_CTA_ODN       IN     VARCHAR2,
                            p_COD_SIS_ORI   IN     VARCHAR2,
                            p_SUC_ORI       IN     NUMERIC,
                            p_COD_TR_PAGO   IN     VARCHAR2,  -- VVT - OVT ETC
                            p_NUM_OPE       IN     VARCHAR2,
                            p_COD_RFR       IN     VARCHAR2,
                            p_REF_EXT       IN     VARCHAR2,
                            p_CAN_PAG       IN     NUMERIC,
                            p_CTA_CTB       IN     VARCHAR2,
                            p_GLS_ADIC      IN     VARCHAR2,
                            p_NUM_FOL_OPE      OUT NUMBER,
                            p_FEC_ISR_OPE      OUT DATE,
                            p_FOR_PAG          OUT VARCHAR2,
                            p_ERR_CODE         OUT VARCHAR2,
                            p_ERR_MSG          OUT VARCHAR2,
                            p_ERROR            OUT NUMBER);
END PKG_PAB_CANALES;