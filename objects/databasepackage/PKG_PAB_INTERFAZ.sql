CREATE OR REPLACE PACKAGE         PKG_PAB_INTERFAZ
AS
   V_NOM_PCK     VARCHAR2 (30) := 'PKG_PAB_INTERFAZ';
   ERR_CODE      NUMBER := 0;
   ERR_MSG       VARCHAR2 (300);
   V_DES         VARCHAR2 (300);
   V_DES_BIT     VARCHAR2 (100);
   V_BANCO       VARCHAR2 (9) := '970150005';

   TYPE T_NUM_FOL_NMN
   IS
      TABLE OF NUMBER
         INDEX BY PLS_INTEGER;

   p_s_mensaje   VARCHAR2 (400);        --BORRA CUANDO SE PASE A PCKS ORIGINAL

   v_dia_ini     DATE := TRUNC (SYSDATE) + INTERVAL '1' MINUTE;
   v_dia_fin DATE
         := TRUNC (SYSDATE) + INTERVAL '23' HOUR + INTERVAL '59' MINUTE ;


   --**********************************************************************************************
   -- Funcion/Procedimiento: SP_GEN_ID_TRANSF
   -- Objetivo: Genera interfaz ID de transferencia.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA
   -- Fecha: 24/05/2017
   -- Autor: Santander
   -- Input:
   -- p_FEC_PROC : Fecha de proceso de generacion ID
   -- Output:
   -- p_CURSOR : Cursor de respuesta de datos
   -- p_ERROR : indicador de ejecutor
   -- Input/Output:
   -- N/A.
   -- Retorno: N/A
   -- Observaciones: Fecha del dia
   --***********************************************************************************************
   PROCEDURE SP_GEN_ID_TRANSF (p_FEC_PROC   IN     VARCHAR2,
                               p_CURSOR        OUT SYS_REFCURSOR,
                               p_ERROR         OUT NUMBER);

   --***********************************************************************************************
   -- Funcion/Procedimiento: SP_GEN_VVV
   -- Objetivo: Genera interfaz de plazo.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA
   -- Fecha: 06/06/2017
   -- Autor: Santander
   -- Input:
   -- p_FEC_ING : Fecha de proceso de generacion ID
   -- Output:
   -- p_OPERACIONES : Cursor de respuesta de datos
   -- p_CANTIDAD_OPE: cantidad de operaciones procesadas
   -- p_ERROR : indicador de ejecutor
   -- Input/Output:
   -- N/A.
   -- Retorno: N/A
   -- Observaciones: Fecha del dia
   --***********************************************************************************************
   PROCEDURE SP_GEN_VVV (p_FEC_ING        IN     VARCHAR2,
                         p_OPERACIONES       OUT SYS_REFCURSOR,
                         p_CANTIDAD_OPE      OUT NUMBER,
                         p_ERROR             OUT NUMBER);

   --***********************************************************************************************
   -- Funcion/Procedimiento: SP_GEN_INT_CTB
   -- Objetivo: Procedimiento que retorna cursor con operaciones de contabilidad
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_TIPO_FRMPG
   -- Fecha: 02-08-2019
   -- Autor: Santander CAFF
   -- Input:
   -- N/A
   -- Output:
   -- p_CURSOR -> RESULTADOS DE CONSULTA.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_GEN_INT_CTB (p_FEC DATE, p_CURSOR OUT SYS_REFCURSOR);
END PKG_PAB_INTERFAZ;