CREATE OR REPLACE PACKAGE         PKG_PAB_CONSTANTES
IS
   /******Variables relacionadas con Estados********************/

   /*Procesos*/
   V_COD_EST_AOS_EJECU         NUMBER := 40;            -- Estado En Ejecucion
   V_COD_EST_AOS_FINOK         NUMBER := 41;              -- Estado Finalizado
   V_COD_EST_AOS_FINER         NUMBER := 42;               -- Estado CON ERROR

   /*Nominas*/
   V_COD_EST_AOS_NMNVAL        NUMBER := 62;                   --Nomina Valida
   V_STR_EST_AOS_NMNVAL        CHAR (3) := 'VAL';              --Nomina Valida

   V_COD_EST_AOS_NMNCUR        NUMBER := 64;    --Nomina Cursada(Para Aprobar)
   V_STR_EST_AOS_NMNCUR        CHAR (3) := 'CUR';                    --Cursada

   V_COD_EST_AOS_NMNINV        NUMBER := 61;                 --Nomina Invalida
   V_STR_EST_AOS_NMNINV        CHAR (3) := 'INV';                   --Invalida

   V_COD_EST_AOS_NMNAUT        NUMBER := 66;               --Nomina Autorizada
   V_STR_EST_AOS_NMNAUT        CHAR (3) := 'AUT';                 --Autorizada

   V_COD_EST_AOS_NMNREC        NUMBER := 65;
   V_STR_EST_AOS_NMNREC        CHAR (3) := 'REC';                  --Rechazada

   V_COD_EST_AOS_NMNELI        NUMBER := 63;
   V_STR_EST_AOS_NMNELI        CHAR (3) := 'ELI';                  --Eliminada

   V_COD_EST_AOS_NMNDUP        NUMBER := 67;
   V_STR_EST_AOS_NMNDUP        CHAR (3) := 'DUP';                  --Duplicada

   V_COD_EST_AOS_NMNPRC        NUMBER := 68;
   V_STR_EST_AOS_NMNPRC        CHAR (3) := 'PRC';                 --En Proceso

   /*Operaciones*/
   V_COD_EST_AOS_OPEVAL        NUMBER := 3;                          -- Valida
   V_STR_EST_AOS_OPEVAL        CHAR (3) := 'VAL';                    -- Valida

   V_COD_EST_AOS_OPEINV        NUMBER := 2;                        -- Invalida
   V_STR_EST_AOS_OPEINV        CHAR (3) := 'INV';                  -- Invalida

   V_COD_EST_AOS_OPECUR        NUMBER := 5;                         -- Cursada
   V_STR_EST_AOS_OPECUR        CHAR (3) := 'CUR';                   -- Cursada

   V_COD_EST_AOS_OPEREC        NUMBER := 6;                       -- Rechazada
   V_STR_EST_AOS_OPEREC        CHAR (3) := 'REC';                 -- Rechazada

   V_COD_EST_AOS_OPEAUT        NUMBER := 7;                      -- Autorizada
   V_STR_EST_AOS_OPEAUT        CHAR (3) := 'AUT';                -- Autorizada

   V_COD_EST_AOS_OPEPAT        NUMBER := 9;                   -- Por Autorizar
   V_STR_EST_AOS_OPEPAT        CHAR (3) := 'PAT';             -- Por Autorizar

   V_COD_EST_AOS_OPELIB        NUMBER := 10;                       -- Liberada
   V_STR_EST_AOS_OPELIB        CHAR (3) := 'LIB';                  -- Liberada

   V_COD_EST_AOS_OPEENV        NUMBER := 13;                        -- Enviada
   V_STR_EST_AOS_OPEENV        CHAR (3) := 'ENV';                   -- Enviada

   V_COD_EST_AOS_OPEPAG        NUMBER := 12;                         -- Pagada
   V_STR_EST_AOS_OPEPAG        CHAR (3) := 'PAG';                    -- Pagada

   V_COD_EST_AOS_OPEDUP        NUMBER := 17;                      -- Duplicada
   V_STR_EST_AOS_OPEDUP        CHAR (3) := 'DUP';                 -- Duplicada

   V_COD_EST_AOS_OPEELI        NUMBER := 4;                       -- Eliminada
   V_STR_EST_AOS_OPEELI        CHAR (3) := 'ELI';                 -- Eliminada

   V_COD_EST_AOS_OPEPVI        NUMBER := 8;                       -- Por Visar
   V_STR_EST_AOS_OPEPVI        CHAR (3) := 'PVI';                 -- Por Visar

   V_COD_EST_AOS_OPEPAB        NUMBER := 16;                     -- Por Abonar
   V_STR_EST_AOS_OPEPAB        CHAR (3) := 'PAB';                -- Por Abonar

   V_COD_EST_AOS_OPEPAS        NUMBER := 19;                    -- Por Asociar
   V_STR_EST_AOS_OPEPAS        CHAR (3) := 'PAS';               -- Por Asociar

   V_COD_EST_AOS_OPEABN        NUMBER := 15;                        -- Abonada
   V_STR_EST_AOS_OPEABN        CHAR (3) := 'ABN';                   -- Abonada

   V_COD_EST_AOS_OPEDEV        NUMBER := 18;                       -- Devuelta
   V_STR_EST_AOS_OPEDEV        CHAR (3) := 'DEV';                  -- Devuelta

   V_COD_EST_AOS_OPEINGAR      NUMBER := 20;                --Ingreso garantia
   V_STR_EST_AOS_OPEINGAR      CHAR (3) := 'GAR';           --Ingreso garantia

   V_COD_EST_AOS_OPEPAGCFI     NUMBER := 21;             --Pagada Contingencia
   V_STR_EST_AOS_OPEPAGCFI     CHAR (3) := 'CFI';        --Pagada Contingencia

   V_COD_EST_AOS_OPEASO        NUMBER := 22; --Asociada Mjs de Prococolo relacionado con Ope Origen
   V_STR_EST_AOS_OPEASO        CHAR (3) := 'ASO'; --Asociada Mjs de Prococolo relacionado con Ope Origen

   V_COD_EST_AOS_OPEPDV        NUMBER := 23;                   -- Por Devolver
   V_STR_EST_AOS_OPEPDV        CHAR (3) := 'PDV';              -- Por Devolver

   V_COD_EST_AOS_OPEING        NUMBER := 1;                        --Ingresada
   V_STR_EST_AOS_OPEING        CHAR (3) := 'ING';                  --Ingresada

   V_COD_EST_AOS_OPEPDEB       NUMBER := 11;          -- Pendiente por debitar
   V_STR_EST_AOS_OPEPDEB       CHAR (3) := 'PDE';     -- Pendiente por debitar

   V_COD_EST_AOS_OPEREG        NUMBER := 14;                     -- Registrada
   V_STR_EST_AOS_OPEREG        CHAR (3) := 'REG';                -- Registrada

   V_COD_EST_AOS_OPEPAGCON     NUMBER := 21;            -- Pagada Contingenica
   V_STR_EST_AOS_OPEPAGCON     CHAR (3) := 'PAC';                -- Registrada

   V_COD_EST_AOS_OPEPROBLM     NUMBER := 99;        -- Operacion con problemas
   V_STR_EST_AOS_OPEPROBLM     CHAR (3) := 'OCP';   -- Operacion con problemas

   V_COD_EST_AOS_OPECONABN     NUMBER := 98;                -- Confirmar Abono
   V_STR_EST_AOS_OPECONABN     CHAR (3) := 'CFO';           -- Confirmar Abono

   /*Tesorería*/

   V_COD_EST_AOS_OPECOM        NUMBER := 27;
   V_STR_EST_AOS_OPECOM        CHAR (3) := 'COM';        --Operación Compuesta

   /*Protocolo*/
   V_COD_EST_AOS_OPENDE        NUMBER := 80; --Mensajes 298 que no son para devolucion
   V_STR_EST_AOS_OPENDE        CHAR (3) := 'NDE'; --Mensajes 298 que no son para devolucion
   V_HORA_LIMITE               NUMBER (4) := 1945; --HORA MÁXIMA PARA REALIZAR ACTUALIZACIONES
   V_CANT_0                    NUMBER (1) := 0;
   ----

   -- Variable para indicar Nro de Operaci?n que se crea sin estar asociada a una nomina
   V_NUM_FOL_NMN_BSE           NUMBER (1) := 0;
   ----

   --Variables para identificar si es Nomina u Operacion
   V_IND_NMN                   NUMBER := 1;
   V_IND_OPE                   NUMBER := 0;

   ----
   /***********************************************************/

   --Variable direccion de mensajeria
   V_FLG_REC                   NUMBER := 1;                         --Recibido
   V_STR_FLG_REC               CHAR (8) := 'Recibido';
   V_FLG_ENV                   NUMBER := 0;                          --Enviado
   V_STR_FLG_ENV               CHAR (7) := 'Enviado';

   --Variables relaciones con salidas de procedimientos
   V_ERROR_KEY_DUPLICATE       NUMBER := 2;
   V_ERROR                     NUMBER := 1;
   V_OK                        NUMBER := 0;
   V_NO_ACTUALIZA              NUMBER := 3;

   --Variables relacionadas con registrso
   V_REG_VIG                   NUMBER := 1;                          --Vigente
   V_REG_NOVIG                 NUMBER := 0;                       --No Vigente

   --Variables de tipo de ingreso------------------------------------------------
   V_COD_TIP_ING_MAS           NUMBER := 0;
   V_STR_TIP_ING_MAS           CHAR (14) := 'Ingreso Masivo'; -- Ingreso Masivo

   V_COD_TIP_ING_UNI           NUMBER := 1;
   V_STR_TIP_ING_UNI           CHAR (16) := 'Ingreso Unitario'; -- Ingreso Unitario

   V_COD_TIP_ING_DIR           NUMBER := 2;
   V_STR_TIP_ING_DIR           CHAR (15) := 'Ingreso Directo'; -- Ingreso Directo
   ------------------------------------------------------------------------------

   --Variable direccion operacion
   V_FLG_EGR                   NUMBER := 0;                           --Egreso
   V_STR_FLG_EGR               CHAR (6) := 'Egreso';
   V_FLG_ING                   NUMBER := 1;                          --Ingreso
   V_STR_FLG_ING               CHAR (7) := 'Ingreso';

   --Variable de Nominas
   V_FLG_DVI_MULTI             NUMBER := 1;                      --Multimoneda
   V_FLG_DVI_UNA               NUMBER := 0;                  --Una sola moneda
   V_STR_DVI_MULTI             CHAR (3) := 'MMX';

   --Variable Tipos de Segmento
   V_COD_TPO_SEG_AOS_CLI       NUMBER := 1;                          --Cliente

   --Variables relacionadas con cta
   V_FLG_DCV_SI                NUMBER := 1;
   V_FLG_DCV_NO                NUMBER := 0;

   --Variable Mercados
   V_FLG_MRD_MN                NUMBER := 0;                 --Mercado Nacional
   V_STR_FLG_MRD_MN            CHAR (2) := 'MN';
   V_FLG_MRD_MX                NUMBER := 1;               --Mercado Extranjero
   V_STR_FLG_MRD_MX            CHAR (2) := 'MX';

   --Variables Mensajes
   V_COD_MT103                 NUMBER := 103;
   V_COD_MT200                 NUMBER := 200;
   V_COD_MT202                 NUMBER := 202;
   V_COD_MT205                 NUMBER := 205;
   V_COD_MT298                 NUMBER := 298;
   V_COD_MT299                 NUMBER := 299;
   V_COD_MT505                 NUMBER := 505;
   V_COD_MT557                 NUMBER := 557;

   V_COD_MT900                 NUMBER := 900;
   V_COD_MT910                 NUMBER := 910;
   V_COD_MT940                 NUMBER := 940;
   V_COD_MT950                 NUMBER := 950;

   /*Tipo MT*/
   V_NUM_MT_PAGO               NUMBER := 0;
   V_NUM_MT_PROTOCOLO          NUMBER := 1;
   V_NUM_MT_GARANTIA           NUMBER := 2;
   V_NUM_MT_AVISOS_PRO         NUMBER := 3;
   V_NUM_MT_CARTOLA            NUMBER := 4;

   --Variables de Monedas
   V_STR_DVI_CLP               CHAR (3) := 'CLP';
   V_STR_DVI_EUR               CHAR (3) := 'EUR';
   V_STR_DVI_USD               CHAR (3) := 'USD';
   V_STR_DVI_AUD               CHAR (3) := 'AUD';
   V_STR_DVI_CAD               CHAR (3) := 'CAD';
   V_STR_DVI_GBP               CHAR (3) := 'GBP';
   V_STR_DVI_JPY               CHAR (3) := 'JPY';
   V_STR_DVI_CHF               CHAR (3) := 'CHF';
   V_STR_DVI_CNY               CHAR (3) := 'CNY';
   V_STR_DVI_DKK               CHAR (3) := 'DKK';
   V_STR_DVI_MXN               CHAR (3) := 'MXN';
   V_STR_DVI_SEK               CHAR (3) := 'SEK';
   V_STR_DVI_NOK               CHAR (3) := 'NOK';

   --Variables Datos Banco Santander
   V_BIC_BANCO_SANTANDER       CHAR (11) := 'BSCHCLRM   ';
   V_BIC_BANCO_SANTANDER_HOM   CHAR (11) := 'BSCHCLR0   ';
   V_NUM_DOC_BANCO_SANTANDER   CHAR (11) := '97036000';
   V_COD_TPD_BANCO_SANTANDER   CHAR (1) := 'K';
   V_CTA_BANCO_SANTANDER       NUMBER (1) := 0;

   --BIC
   V_BIC_COMBANC               CHAR (11) := 'PCCACLRM';
   V_BIC_BCCH                  CHAR (11) := 'BCECCLRR';


   --Variable tipo de operacion
   V_STR_TPO_OPE_CCLV          CHAR (8) := 'CCLV';
   V_STR_TPO_OPE_DVP           CHAR (8) := 'DVPDCV  ';
   V_STR_TPO_OPE_DEVFDOS       CHAR (8) := 'DEVFDOS ';
   V_STR_TPO_OPE_ABN           CHAR (8) := 'ABN ';
   V_STR_TPO_OPE_OPB           CHAR (8) := 'OPB ';
   V_STR_TPO_OPE_CDLE          CHAR (8) := 'CDLE    ';
   V_STR_TPO_OPE_SIDEN         CHAR (8) := 'SIN-IDEN';

   --Variable para pais
   V_STR_COD_PAS_CHILE         CHAR (2) := 'CL';

   --Canales
   V_SRT_COD_SIS_SAL_SWF       CHAR (10) := 'SWF';
   V_SRT_COD_SIS_SAL_CMN       CHAR (10) := 'CMN';
   V_SRT_COD_SIS_SAL_CTACTE    CHAR (10) := 'CTACTE';
   V_SRT_COD_SIS_SAL_LBTR      CHAR (10) := 'LBTR';
   V_SRT_COD_SIS_SE_OOFF       CHAR (10) := 'OOFF';
   V_SRT_COD_SIS_SE_TECNO      CHAR (10) := 'TECNO';
   V_SRT_COD_SIS_SE_MESA       CHAR (10) := 'MESA';

   /*Canales*/
   V_FLG_SIS_ENT               NUMBER (1) := 0;              -- Canal internos
   V_FLG_SIS_SAL               NUMBER (1) := 1; -- Canal externos (CTACTE en liquidaro pasa a ser interno)
   V_FLG_SIS_ABN               NUMBER (1) := 2;    -- Canal vigentes y puentes
   V_FLG_SIS_ENT_SAL_OTRO      NUMBER (1) := 4; -- Canales Internos que no operan
   V_FLG_SIS_900_910           NUMBER (1) := 6; -- Canales de mensajes mt900/910

   --Constante de Contabilidad
   V_FLG_GNR_OPE_CTB           NUMBER := 1;              --Genera Contabilidad
   V_COD_ENT_BAN_SANT          CHAR (4) := '0035';
   V_COD_SIS_PAB               CHAR (3) := 'PAB';
   V_COD_SUC_OPERANTE          CHAR (4) := '1111';
   V_COD_SUC_ORG               CHAR (4) := '0174';            --Mesa de dinero
   V_COD_SUC_DTN               CHAR (4) := '0174';            --Mesa de dinero
   V_FLG_OPE_DEB               NUMBER := 0;
   V_FLG_OPE_HAB               NUMBER := 1;
   V_COD_PUE_GAR_OOFF          CHAR (5) := '04016';
   V_STR_PUE_GAR_OOFF          CHAR (30) := 'Cuadre MN Garantia Combanc';
   V_STR_COR_PUE_GAR_OOFF      CHAR (3) := 'GCM';
   V_COD_PUE_CMN               CHAR (5) := '04014';
   V_STR_PUE_CMN               CHAR (30) := 'Cuadre MN COMBANC';
   V_STR_COR_CMN               CHAR (3) := 'CMN';

   --Constante de Akon
   V_COD_OPE_DEB               CHAR (1) := 'D';
   V_COD_OPE_HAB               CHAR (1) := 'H';
   V_COD_PROD                  CHAR (2) := '30';
   V_COD_SUB_PROD              CHAR (4) := '3031';
   V_COD_CTA_ALT               CHAR (11) := '21051210240';

   --Constante Extension archivos
   V_FLG_EXT_XLS               NUMBER := 0;              --Formato archivo XLS
   V_STR_EXT_XLS               CHAR (3) := 'XLS';
   V_FLG_EXT_TXT               NUMBER := 1;              --Formato archivo TXT
   V_STR_EXT_TXT               CHAR (3) := 'TXT';

   --Variables duplicadas
   V_COD_DUP_OK                NUMBER (1) := 1;
   V_COD_NO_DUP                NUMBER (1) := 0;

   --Constante Cierre
   V_DIA_RES_HIS               NUMBER (2) := 10;                          --2;
   V_DIA_CONT_HIS              NUMBER (2) := 30; --Día que se almacena la Contabilidad;
   V_DIA_ELI_LOG               NUMBER (2) := 5; --Día que se almacenan los log;
   V_DIA_ELI_NTF               NUMBER (2) := 7; --Días que permanecen las notificaciones;
   V_DIA_ELI_CAR               NUMBER (2) := 45; -- DIAS QUE SE ALMACENAN CARTOLAS
   V_DIA_ELI_900_910           NUMBER (2) := 30;             -- cambio mensual
   V_DIA_ELI_PAGOS             NUMBER (2) := 30; --Dias de eliminación pagos realizados(Liquidez-Egresos)

   --Formas de pago
   V_COD_TIP_FPA_CTA           CHAR (10) := 'CTACTE';
   V_STR_TIP_FPA_CTA           VARCHAR2 (50) := 'Cuentas Corrientes';

   V_COD_TIP_FPA_CVG           CHAR (10) := 'CONVIG';
   V_STR_TIP_FPA_CVG           VARCHAR2 (50) := 'Contable Vigente';

   V_COD_TIP_FPA_CPU           CHAR (10) := 'CONPUE';
   V_STR_TIP_FPA_CPU           VARCHAR2 (50) := 'Contable Puente';

   V_COD_TIP_FPA_VLV           CHAR (10) := 'VALEVI';
   V_STR_TIP_FPA_VLV           VARCHAR2 (50) := 'Vale Vista';


   --Variables de Salida------------------------------------------------
   V_COD_TIP_ABO_CTA           CHAR (10) := 'CTACTE';
   V_STR_TIP_ABO_CTA           VARCHAR2 (50) := 'Cuentas Corrientes';

   V_COD_TIP_ABO_VLV           CHAR (10) := 'VALEVIS';
   V_STR_TIP_ABO_VLV           VARCHAR2 (50) := 'Vale Vista';

   V_COD_TIP_ABO_VXP           CHAR (10) := 'VIGPENPPAG';
   V_STR_TIP_ABO_VXP           VARCHAR2 (50) := 'Pendientes Por Pagar';

   V_COD_TIP_ABO_VAC           CHAR (10) := 'VIGVARIOS';
   V_STR_TIP_ABO_VAC           VARCHAR2 (50) := 'Varios Acreedor';

   V_COD_TIP_ABO_PUE           CHAR (10) := 'PUENTECONT';
   V_STR_TIP_ABO_PUE           VARCHAR2 (50) := 'Puente Contable';

   V_COD_TIP_ABO_XDIS          CHAR (10) := 'ABONXDISTR';
   V_STR_TIP_ABO_XDIS          VARCHAR2 (50) := 'Abonos por Distribuir';

   V_COD_TIP_ALC_LIQ           CHAR (10) := 'MESA';
   V_STR_TIP_ALC_LIQ           VARCHAR2 (50) := 'ALCO Liquidez';

   V_COD_TIP_TECNO             CHAR (10) := 'TECNO';
   V_STR_TIP_TECNO             VARCHAR2 (50) := 'Tecnologia';

   V_COD_TIP_CAJA              CHAR (10) := 'CAJA';
   V_STR_TIP_CAJA              VARCHAR2 (50) := 'Terminal Financiero';

   V_COD_TIP_OB                CHAR (10) := 'OFFICEBANK';
   V_STR_TIP_OB                VARCHAR2 (50) := 'OfficeBanking';

   V_COD_TIP_EXTRA             CHAR (10) := 'EXTRA';
   V_STR_TIP_EXTRA             VARCHAR2 (50) := 'Extra';

   V_COD_TIP_LBTRCOMB          CHAR (10) := 'LBTRCOMB';
   V_STR_TIP_LBTRCOMB          VARCHAR2 (50) := 'Garantía COMBANC';

   V_COD_TIP_CTACTELBTR        CHAR (10) := 'CTACTELBTR';
   V_STR_TIP_CTACTELBTR        VARCHAR2 (50) := 'Abonos Office Banking';

   V_COD_TIP_CONTA             CHAR (10) := 'CONTA';
   V_STR_TIP_CONTA             VARCHAR2 (50) := 'Movimientos LBTR';

   V_COD_TIP_SWF               CHAR (10) := 'SWF';
   V_STR_TIP_SWF               VARCHAR2 (50) := 'Bancos Extranjeros';


   V_COD_TIP_BOLSA             CHAR (10) := 'BOLSA';
   V_STR_TIP_BOLSA VARCHAR2 (50)
         := 'Movimientos LBTR no reconocidos' ;

   V_COD_TIP_OPHIPO            CHAR (10) := 'OPHIPO';
   V_STR_TIP_OPHIPO VARCHAR2 (50)
         := 'Línea de financiemiento BCOMA' ;

   V_COD_TIP_CARGXDISTR        CHAR (10) := 'CARGXDISTR';
   V_STR_TIP_CARGXDISTR        VARCHAR2 (50) := 'Cargos por Distribuir';

   V_COD_TIP_BANPRI            CHAR (10) := 'BANPRI';
   V_STR_TIP_BANPRI            VARCHAR2 (50) := 'Banca Privada';

   V_COD_TIP_CORRBOL           CHAR (10) := 'CORRBOL';
   V_STR_TIP_CORRBOL           VARCHAR2 (50) := 'Corredora de Bolsa';

   V_COD_TIP_CORRSEG           CHAR (10) := 'CORRSEG';
   V_STR_TIP_CORRSEG           VARCHAR2 (50) := 'Corredora de Seguro';

   V_COD_TIP_GTB               CHAR (10) := 'GTB';
   V_STR_TIP_GTB VARCHAR2 (50)
         := 'Gerencia Banca Transaccional' ;

   V_COD_TIP_LEAFAC            CHAR (10) := 'LEAFAC';
   V_STR_TIP_LEAFAC VARCHAR2 (50)
         := 'Operaciones Leasing / Factoring' ;

   V_COD_TIP_TESCEN            CHAR (10) := 'TESCEN';
   V_STR_TIP_TESCEN            VARCHAR2 (50) := 'Tesoreria Central';

   V_COD_TIP_HIPOTE            CHAR (10) := 'HIPOTE';
   V_STR_TIP_HIPOTE            VARCHAR2 (50) := 'Hipotecarios';

   V_COD_TIP_FONMUT            CHAR (10) := 'FONMUT';
   V_STR_TIP_FONMUT            VARCHAR2 (50) := 'Fondos Mutuos';

   V_COD_TIP_CUSTODIA          CHAR (10) := 'CUSTODIA';
   V_STR_TIP_CUSTODIA          VARCHAR2 (50) := 'Custodia';

   V_COD_TIP_CANJE             CHAR (10) := 'CANJE';
   V_STR_TIP_CANJE             VARCHAR2 (50) := 'Canje';

   V_COD_TIP_ABOMAS            CHAR (10) := 'ABOMAS';
   V_STR_TIP_ABOMAS            VARCHAR2 (50) := 'Abonos Masivos';

   V_COD_TIP_CREDIT            CHAR (10) := 'CREDIT';
   V_STR_TIP_CREDIT            VARCHAR2 (50) := 'Procesos Activos';
   ------------------------------------------------------------------------------

   --Constantes que indican vigencia -----------------------------------------
   -- 0 No Vigente
   -- 1 Si Vigente
   -- 2 Eliminacion Permanenete
   V_FLG_VGN_NO                NUMBER (1) := 0;
   V_FLG_VGN_SI                NUMBER (1) := 1;
   V_FLG_VGN_ELI               NUMBER (1) := 2;
   ---------------------------------------------------------------------------

   --Glosa por cambio estado contingencia y eliminacion cierre
   V_GLS_EST_CON VARCHAR2 (210)
         := 'Operacion Pagada por contingencia' ;
   V_GLS_EST_CIE_OPE VARCHAR2 (210)
         := 'Cierre de Sistema, Eliminacion Operaciones sin Pagar' ;
   V_GLS_EST_CIE_NMN VARCHAR2 (210)
         := 'Cierre de Sistema, Eliminacion Nominas sin Autorizar' ;

   --Respuestas
   V_STR_SI_CORTO              CHAR (1) := 'S';
   V_STR_NO_CORTO              CHAR (1) := 'N';

   --Cambio Canal
   V_COD_CMB_CNL_OK            NUMBER (1) := 0;
   V_COD_CMB_CNL_NOK           NUMBER (1) := 1;
   V_COD_CMB_CNL_NOHOR         NUMBER (1) := 2;
   V_COD_CMB_CNL_NOCOMB        NUMBER (1) := 3;
   V_COD_CMB_CNL_BLQUE         NUMBER (1) := 4;

   --Variables Garantias
   V_NUM_DOC_COMDER            CHAR (8) := '76317889';
   V_NUM_VER_COMDER            CHAR (1) := '7';
   V_DSC_CTA                   CHAR (6) := 'COMDER';
   V_NOM_BCO_CTR               VARCHAR2 (15) := 'BANCO CENTRAL';
   V_NOM_CMD                   VARCHAR2 (15) := 'COMDER S A';
   V_COMDERM                   CHAR (7) := 'COMDERM';
   V_COMDERG                   CHAR (7) := 'COMDERG';


   --Variables de Movimiento a Cuenta corriente
   V_FLG_MOV_CGO               NUMBER := 0;                          --0 Cargo
   V_FLG_MOV_ABN               NUMBER := 1;                          --1 Abono

   --Calendarios GBO
   v_STR_CAL_CL                CHAR (2) := 'SN';

   --Tipo de rol
   V_COD_TIP_ROL_SUP           NUMBER (1) := 1;                   --Supervisor
   V_COD_TIP_ROL_ANA           NUMBER (1) := 2;                     --Analista
   V_COD_TIP_ROL_ADM           NUMBER (1) := 4;                --Administrador
   V_COD_TIP_ROL_PER           NUMBER (1) := 5;                   --Perfilador
   V_COD_TIP_ROL_VIS           NUMBER (1) := 6;                      --Visador

   -- Notificaciones
   V_COD_TIP_CRO_ERACT         CHAR (5) := 'ERACT'; -- Error actualizar referencia
   V_COD_TIP_CRO_MHOR          CHAR (5) := 'MHOR'; -- Cambio de horario canal de salida
   V_COD_TIP_CRO_CCABN         CHAR (5) := 'CCABN';      -- Cambio canal Abono
   V_COD_TIP_CRO_PVIS          CHAR (5) := 'PVIS';                -- Por Visar
   V_COD_TIP_CRO_ELIMI         CHAR (5) := 'ELIM';   -- Operaciones Eliminadas
   V_COD_TIP_CRO_ABCT          CHAR (5) := 'ABCT'; -- Abono cuenta corriente 910
   V_COD_TIP_CRO_EBCT          CHAR (5) := 'EBCT'; -- Error Abono cuenta corriente 910
   V_COD_TIP_CRO_ITCCC         CHAR (5) := 'ITCCC'; -- Interfaz Cuentas corrientes correcto
   V_COD_TIP_CRO_ITAKN         CHAR (5) := 'ITAKN';          -- Interfaz Akons
   V_COD_TIP_CRO_ITCTB         CHAR (5) := 'ITCTB';    -- Interaz Contabilidad
   V_COD_TIP_CRO_ITVVV         CHAR (5) := 'ITVVV';     -- Interfaz Vale Vista
   V_COD_TIP_CRO_VPCJM         CHAR (5) := 'VPCJM'; -- Validacion Valuta Carga Proyeccion MN/MX
   V_COD_TIP_CRO_CNTOK         CHAR (5) := 'CNTOK'; -- Variable Contingencia Combanc
   V_COD_TIP_CRO_CNTER         CHAR (5) := 'CNTER'; -- Variable Contingencia Combanc
   V_COD_TIP_CRO_ALMAX         CHAR (5) := 'ALMAX';     -- Alerta Saldo Máximo
   V_COD_TIP_CRO_ALMIN         CHAR (5) := 'ALMIN';     -- Alerta Saldo Mínimo
END PKG_PAB_CONSTANTES;