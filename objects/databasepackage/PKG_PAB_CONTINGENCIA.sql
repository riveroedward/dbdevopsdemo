CREATE OR REPLACE PACKAGE         PKG_PAB_CONTINGENCIA
IS
   /**************************VARIABLE GLOBALES************************************/
   v_NOM_PCK     VARCHAR2 (30) := 'PKG_PAB_CONTINGENCIA';
   ERR_CODE      NUMBER := 0;
   ERR_MSG       VARCHAR2 (300);
   v_DES         VARCHAR2 (300);
   p_s_mensaje   VARCHAR2 (400);

   /**************************FIN VARIABLE GLOBALES********************************/
   --***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ACT_EST_OPE_CON
   -- Objetivo: Procedimiento que actualiza operaciones por contingencia
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: SP_EST_OPE_AOS
   -- Fecha: 25/07/17
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_OPE    -> Numero folio operacion
   -- p_FEC_ISR_OPE    -> fecha de insercion de operacion
   -- p_COD_EST_AOS    -> Codigo estado operacion
   -- p_GLS_ADC_EST    -> glosa adicional estado
   -- p_COD_USR        -> usuario de accion
   -- Output:
   -- p_ERROR         -> codigo de error de ejecucion de PL (para log)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_ACT_EST_OPE_CON (p_NUM_FOL_OPE   IN     NUMBER,
                                     p_FEC_ISR_OPE   IN     DATE,
                                     p_COD_EST_AOS   IN     CHAR,
                                     p_GLS_ADC_EST   IN     VARCHAR2,
                                     p_COD_USR       IN     VARCHAR2,
                                     p_ERROR            OUT NUMBER);

   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUS_OPE_ING
   -- Objetivo: Procedimiento que busca una operaci?n de ingreso por n?mero de referencia swift, para comprobar si existe antes de insertar una nueva operaci?n.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN
   -- Fecha: 02/06/16
   -- Autor: Santander
   -- Input:    p_NUM_REF_SWF->   foliador del banco
   -- Output:
   --        p_FLG_EXT : flag indicador de salida de programa.
   --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/

   PROCEDURE Sp_PAB_BUS_OPE_ING (p_NUM_REF_SWF   IN     CHAR,
                                 p_FLG_EXT          OUT NUMBER,
                                 p_ERROR            OUT NUMBER);

   --***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUS_OPE_CONT_COMBANC
   -- Objetivo: Procedimiento que busca operaciones para enviar por contingencia combanc
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN
   -- Fecha: 25/07/17
   -- Autor: Santander
   -- Input:
   -- p_TIPO_OP        -> tipo de operacion
   -- p_MT_SWIFT       -> numero de mensaje
   -- p_NOM_BFC        -> nombre beneficiario
   -- p_BCO_DTN        -> banco destino
   -- p_MONTO_DESDE    -> monto desde
   -- p_MONTO_HASTA    -> monto hasta
   -- p_NUM_NOM        -> Numero de nomina
   -- Output:
   -- p_CURSOR        -> cursor que retorna operaciones disponibles.
   -- p_ERROR         -> codigo de error de ejecucion de PL (para log)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_BUS_OPE_CONT_COMBANC (
      p_TIPO_OP       IN     CHAR,
      p_MT_SWIFT      IN     NUMBER,
      p_NOM_BFC       IN     VARCHAR2,
      p_BCO_DTN       IN     CHAR,
      p_MONTO_DESDE   IN     NUMBER,
      p_MONTO_HASTA   IN     NUMBER,
      p_NUM_NOM       IN     NUMBER,
      p_CURSOR           OUT SYS_REFCURSOR,
      p_ERROR            OUT NUMBER
   );

   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUS_ARCH_GEN
   -- Objetivo: Retorna los archivos generados y por descargar de las contingencias combanc
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas:
   -- Fecha: 13/12/2016
   -- Autor: Santander
   -- Input:
   -- p_FEC_ARC_CGE    -> Fecha Proceso.
   -- p_FLG_ARC_CGE    -> Flag de descarga/descarga de archivo
   -- Output:
   -- p_CURSOR         -> Cursor con los archivos generados.
   -- p_ERROR          -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Retorno: N/A.
   -- Observaciones: 14/09/2016, Creacion Procedimiento.
   --***********************************************************************************************
   PROCEDURE Sp_PAB_BUS_ARCH_GEN (p_FEC_ARC_CGE   IN     DATE,
                                  p_FLG_ARC_CGE   IN     NUMBER,
                                  p_CURSOR           OUT SYS_REFCURSOR,
                                  p_ERROR            OUT NUMBER);

   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ACT_ARCH_GEN
   -- Objetivo: actualiza los archivos generados y por descargar de las contingencias combanc
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas:
   -- Fecha: 13/12/2016
   -- Autor: Santander
   -- Input:
   -- p_NUM_CRV_ARC_CGE     -> numero identificador de archivo
   -- p_COD_USR_DGA_CGE     -> codigo usuario que descarga
   -- p_FEC_DGA_ARC_CGE     -> fecha de ultima descarga
   -- Output:
   -- p_ERROR          -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Retorno: N/A.
   -- Observaciones: 14/09/2016, Creacion Procedimiento.
   --***********************************************************************************************
   PROCEDURE Sp_PAB_ACT_ARCH_GEN (p_NUM_CRV_ARC_CGE   IN     NUMBER,
                                  p_COD_USR_DGA_CGE   IN     VARCHAR2,
                                  p_FEC_DGA_ARC_CGE   IN     DATE,
                                  p_ERROR                OUT NUMBER);

   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_RES_CONT_SINACOFI
   -- Objetivo: procedimiento que genera el resumen de operacione generadas por sinacofi.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas:
   -- Fecha: 13/12/2016
   -- Autor: Santander
   -- Input:
   -- p_FEC_VTA     -> fecha valuta
   -- Output:
   -- p_CURSOR         -> cursor con resumen de operaciones
   -- p_ERROR          -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Retorno: N/A.
   -- Observaciones: 14/09/2016, Creacion Procedimiento.
   --***********************************************************************************************
   PROCEDURE Sp_PAB_RES_CONT_SINACOFI (p_FEC_VTA   IN     DATE,
                                       p_CURSOR       OUT SYS_REFCURSOR,
                                       p_ERROR        OUT NUMBER);

   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_DET_CONT_SINACOFI
   -- Objetivo: procedimiento que genera el detalle de operacione generadas por sinacofi.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas:
   -- Fecha: 13/12/2016
   -- Autor: Santander
   -- Input:
   -- p_COD_BCO_DTN  -> BIC BANCO DESTINO
   -- p_FEC_VTA     -> fecha valuta
   -- Output:
   -- p_CURSOR         -> cursor con resumen de operaciones
   -- p_ERROR          -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Retorno: N/A.
   -- Observaciones: 14/09/2016, Creacion Procedimiento.
   --***********************************************************************************************
   PROCEDURE Sp_PAB_DET_CONT_SINACOFI (p_COD_BCO_DTN   IN     CHAR,
                                       p_FEC_VTA       IN     DATE,
                                       p_CURSOR           OUT SYS_REFCURSOR,
                                       p_ERROR            OUT NUMBER);

   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUS_OPE_CONT_LBTR
   -- Objetivo: Busca operaciones para generar contingencia SINACOF.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas:
   -- Fecha: 13/12/2016
   -- Autor: Santander
   -- Input:
   -- p_TIPO_OP        -> tipo de operacion
   -- p_MT_SWIFT       -> numero de mensaje
   -- p_NOM_BFC        -> nombre beneficiario
   -- p_BCO_DTN        -> banco destino
   -- p_MONTO_DESDE    -> monto desde
   -- p_MONTO_HASTA    -> monto hasta
   -- p_NUM_FOL_MNA    -> Numero de nomina
   -- p_COD_SIS_ENT    -> codigo sistema de entrada
   -- p_COD_SIS_SAL    -> codigo sistema salida
   -- p_NUM_FOL_OPR    -> numero de folio
   -- p_NOM_BFC        -> nombre beneficiario
   -- p_NUM_DCO_ORD    -> numero de documento ordenante
   -- Output:
   -- p_CURSOR         -> cursor con resumen de operaciones
   -- p_ERROR          -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Retorno: N/A.
   -- Observaciones: 14/09/2016, Creacion Procedimiento.
   --***********************************************************************************************
   PROCEDURE Sp_PAB_BUS_OPE_CONT_LBTR (p_TIPO_OP       IN     CHAR,
                                       p_MT_SWIFT      IN     NUMBER,
                                       p_BCO_DTN       IN     CHAR,
                                       p_MONTO_DESDE   IN     NUMBER,
                                       p_MONTO_HASTA   IN     NUMBER,
                                       p_NUM_FOL_MNA   IN     NUMBER,
                                       p_COD_SIS_ENT   IN     VARCHAR2,
                                       p_COD_SIS_SAL   IN     VARCHAR2,
                                       p_NUM_FOL_OPR   IN     NUMBER,
                                       p_NOM_BFC       IN     VARCHAR2,
                                       p_NUM_DCO_ORD   IN     VARCHAR2,
                                       p_CURSOR           OUT SYS_REFCURSOR,
                                       p_ERROR            OUT NUMBER);


   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ING_ARCH_CGE
   -- Objetivo: Inserta acrchivo generados y por descargar de las contingencias combanc
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas:
   -- Fecha: 13/11/2017
   -- Autor: Santander
   -- Input:
   -- p_ARC_CGE_ECD -> nombre beneficiario
   -- Output:
   -- p_ERROR       -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Retorno: N/A.
   -- Observaciones:
   --***********************************************************************************************
   PROCEDURE Sp_PAB_ING_ARCH_CGE (p_ARC_CGE_ECD IN CLOB, p_ERROR OUT NUMBER);

   --************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_BUS_VVV_CTG_PLZ
   -- Objetivo: Procedimiento que busca las operaciones con fecha valuta hoy, de los canales de entrada de OFFICE BANKING y CAJA
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN
   -- Fecha: 02/06/16
   -- Autor: Santander
   -- Input:    p_FECHA_DESDE->   Fecha desde para fecha valuta
   --           p_FECHA_HASTA->   Fecha hasta para fecha valuta
   -- Output:
   --           p_CURSOR: Cursor con tabla de resultado de operaciones
   --           p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_PAB_BUS_VVV_CTG_PLZ (p_FECHA_DESDE       DATE,
                                     p_FECHA_HASTA       DATE,
                                     p_CURSOR        OUT SYS_REFCURSOR,
                                     p_ERROR         OUT NUMBER);
END PKG_PAB_CONTINGENCIA;