CREATE OR REPLACE PACKAGE         "PKG_PAB_UTILITY"
IS
   -- Package el cual contrendra todas las funciones y procedimientos comunes que pueden ser utilizados en mas de una funcionalidad
   -- @Santander
   -- **************************VARIABLE GLOBALES************************************
   v_NOM_PCK     VARCHAR2 (30) := 'PKG_PAB_UTILITY';
   ERR_CODE      NUMBER := 0;
   ERR_MSG       VARCHAR2 (300);
   v_DES         VARCHAR2 (300);
   p_s_mensaje   VARCHAR2 (400);

   -- **************************FIN VARIABLE GLOBALES************************************************
   --
   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_OBT_CAN_ENT_USU
   -- Objetivo: Procedimiento que obtiene el canal de entrada segun el usuario
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_USRIO
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- p_COD_USR -> rut usuario. (Obligatorio).
   -- Output:
   -- p_COD_SIS_ENT -> codigo del canal de entrada.
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_OBT_CAN_ENT_USU (p_COD_USR       IN     CHAR,
                                     p_COD_SIS_ENT      OUT CHAR,
                                     p_ERROR            OUT NUMBER);

   --
   --***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_CAL_MER
   -- Objetivo: Procedimiento que obtiene el mercado segun la moneda
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: N/A
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- p_COD_DVI -> Codigo de la moneda.
   -- Output:
   -- p_FLG_MRD_UTZ_SWF -> Identificador del mercado 0)MN 1)MX
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_CAL_MER (p_COD_DVI IN CHAR, p_FLG_MRD_UTZ_SWF OUT NUMBER);

   --
   --***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUSCA_ESTRUC_ARCH
   -- Objetivo: Procedimiento que recibe como parametro ID_ARCHIVO, con el valor asociado a la nomina o planilla excel para la carga masiva,
   --           el procedimiento devuelve un cursor con los nombres de los campos que deberia componer la nomina, los tipos de datos de estos y su ID.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_ARCHv_ENTRD_SALID, PABS_DT_ESTRT_ARCHv_ENTRD, PABS_DT_CAMPO_ARCHv_ENTRD
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- p_IDF_ARC -> Identificador unico de la tabla ARCHIVO.
   -- p_IDF_SIS -> Nombre del sistema que llama al procedimiento.
   -- Output:
   -- p_CURSOR -> SYS_REFCURSOR, resultados de consulta.
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_BUSCA_ESTRUC_ARCH (p_IDF_ARC   IN     CHAR,
                                       p_IDF_SIS   IN     VARCHAR2,
                                       p_CURSOR       OUT SYS_REFCURSOR,
                                       p_ERROR        OUT NUMBER);

   --
   --***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_DESCOMp_RUT
   -- Objetivo: Procedimiento que descompone el Rut, separandolo del digito verificador.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: N/A
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- p_RUT_PSN -> Rut de personaa.
   -- Output:
   -- p_NUM_DOC_PSN -> Numero documento persona
   -- p_COD_TPD_PSN -> Codigo tipo documento persona
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_DESCOMp_RUT (p_RUT_PSN       IN     VARCHAR2,
                                 p_NUM_DOC_PSN      OUT VARCHAR2,
                                 p_COD_TPD_PSN      OUT CHAR);

   --
   --***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_MAN_BFC_ODN
   -- Objetivo:
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: N/A
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- p_NUM_DOC_PSN -> Numero documento del beneficiario/ordenante
   -- p_COD_TPD_PSN -> Digito Verificador del beneficiario/ordenante
   -- Output:
   -- Retorno: N/A.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_MAN_BFC_ODN (p_NUM_DOC_PSN   IN VARCHAR2,
                                 p_COD_TPD_PSN   IN CHAR);

   --
   --***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_INS_BNF_ODN
   -- Objetivo:
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_BNFCR_ORDNT
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- p_NUM_DOC_PSN -> Numero documento del beneficiario/ordenante
   -- p_COD_TPD_PSN -> Digito Verificador del beneficiario/ordenante
   -- Output:
   -- Retorno: N/A.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_INS_BNF_ODN (p_NUM_DOC_PSN   IN VARCHAR2,
                                 p_COD_TPD_PSN   IN CHAR);

   --
   --***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_MAN_CTA_BFC_ODN
   -- Objetivo: procediemitno que verifica si existe la cuenta del ordenasnte/beneficiario en caso de no existir la inserta
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_BNFCR_ORDNT
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- p_RUT_PSN ->Rut de persona
   -- p_COD_TPD_PSN ->Codigo tipo documento persona:
   -- p_NUM_CTA_TRF ->Cuenta de trasnferencia
   -- p_FLG_DCV ->Indica si la cuenat es DCV : 1)DCV 0)Cuenta Normal
   -- p_COD_BIC_BCO -> Codigo bitacora banco
   -- Output:
   -- Retorno: N/A.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_MAN_CTA_BFC_ODN (p_NUM_DOC_PSN   IN VARCHAR2,
                                     p_COD_TPD_PSN   IN CHAR,
                                     p_NUM_CTA_TRF   IN VARCHAR2,
                                     p_FLG_DCV       IN NUMBER,
                                     p_COD_BIC_BCO   IN VARCHAR2);

   --
   --***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_INS_CTA_TRANS
   -- Objetivo: Procedimiento que inserta la cuenta asociada al rut del beneficiario
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_CENTA_TRNSC
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- p_NUM_DOC_PSN -> Rut dueno de la cuenta.
   -- p_COD_TPD_PSN -> Digito Verificado dueuo de la cuenta
   -- p_COD_BIC_BCO -> BIC del Banco dueuo de la cuenta.
   -- p_NUM_CTA_TRF -> Numero de cuenta.
   -- p_FLG_DCV -> Indica si la cuenat es DCV : 1)DCV 0)Cuenta Normal
   -- Output:
   -- Retorno: N/A.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_INS_CTA_TRANS (p_NUM_DOC_PSN   IN CHAR,
                                   p_COD_TPD_PSN   IN CHAR,
                                   p_COD_BIC_BCO   IN VARCHAR2,
                                   p_NUM_CTA_TRF   IN VARCHAR2,
                                   p_FLG_DCV       IN NUMBER);

   --
   --***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_DEv_COD_TPO_FND
   -- Objetivo: Devuelve el codigo del tipo de Fondo
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_TIPO_FONDO_ALMOT
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   --
   -- Output:
   -- p_COD_TPO_FND -> Codigo del tipo de fondo
   -- Input/Output:
   -- p_GLS_TPO_FND -> Glosa del tipo de fondo a buscar.
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_DEv_COD_TPO_FND (p_GLS_TPO_FND   IN     VARCHAR2,
                                     p_COD_TPO_FND   IN OUT CHAR);

   --
   --***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_COMBO_MONEDA
   -- Objetivo: Devuelve el codigo del tipo de Fondo
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_MONEDA_ALMOT
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- N/A
   -- Output:
   -- p_CURSOR -> SYS_REFCURSOR, RESULTADOS DE CONSULTA.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_COMBO_MONEDA (p_CURSOR OUT SYS_REFCURSOR);

   --
   /********************************************************************************************************
   FUNCION/PROCEDIMIENTO: SP_PAB_COMBO_CANAL
   OBJETIVO             : MUESTRA FILTRO EN COMBO:
                           EGRESOS -> ENVIADOS - OPERACIONES -> LIBERAR
                                   COMBO: CANAL PAGO, CAMBIO CANAL, CANAL DE ORIGEN.
                           CONTINGENCIA -> ENVIADO -> BCCH UPLOAD - DESCARGAR ARCHIVO
                                   COMBO: CAMBIO CANAL, CANAL DE ORIGEN.
   SISTEMA              : DBO_PAB.
   BASE DE DATOS        : DBO_PAB.
   TABLAS USADAS        : PABS_DT_SISTM_ENTRD_SALID.
   FECHA                : 21/06/16
   AUTOR                : SANTANDER
   INPUT                : P_CANAL  := CANAL.
   OUTPUT               : P_CURSOR := DATOS CURSOR.
   OBSERVACIONES
   FECHA       USUARIO    DETALLE
   21/06/16    SANTANDER  CREACIÓN
   ********************************************************************************************************/
   PROCEDURE SP_PAB_COMBO_CANAL (P_CANAL    IN     NUMBER,
                                 P_CURSOR      OUT SYS_REFCURSOR);

   --
   /********************************************************************************************************
     FUNCION/PROCEDIMIENTO: SP_PAB_COMBO_MT_SWIFT.
     OBJETIVO             : MUESTRA FILTRO EN
                             NOMINAS -> SWIFT -> CREAR UNITARIA
                                     COMBO: TIPO MENSAJE
                             ENVIADOS - OPERACIONES -> LIBERAR
                                     COMBO: MT SWIFT
                             MENSAJES -> ENVIADO - PROTOCOLO -> CREAR
                                     COMBO: SUB - MENSAJE
                             CONSULTAS -> SWIFT -> OPERACIONES
                                     COMBO: MT SWIFT
     SISTEMA              : DBO_PAB.
     BASE DE DATOS        : DBO_PAB.
     TABLAS USADAS        : PABS_DT_TIPO_MT_SWIFT.
     FECHA                : 21/06/2016.
     AUTOR                : SANTANDER.
     INPUT                : NA.
     OUTPUT               : P_CURSOR := DATOS CURSOR.
     OBSERVACIONES
     FECHA       USUARIO    DETALLE
     21/06/2016  SANTANDER  CREACIÓN.
     ********************************************************************************************************/
   PROCEDURE SP_PAB_COMBO_MT_SWIFT (p_CURSOR OUT SYS_REFCURSOR);

   /********************************************************************************************************
   FUNCION/PROCEDIMIENTO: SP_PAB_COMBO_MT_SWIFT_CLP.
   OBJETIVO             : MUESTRA FILTRO EN
                           INGRESOS -> RECIBO OPERACIONES -> PROCESAR PAGOS RECIBIDOS MANUAL
                                    COMBO: MT SWIFT
                           INGRESOS -> RECIBO OPERACIONES -> CAMBIAR CANAL SALIDA
                                    COMBO: MT SWIFT
                           CONTINGENCIA -> ENVIADO -> BCCH UPLOAD - DESCARGAR ARCHIVO
                                     COMBO: MT SWIFT
   SISTEMA              : DBO_PAB.
   BASE DE DATOS        : DBO_PAB.
   TABLAS USADAS        : PABS_DT_TIPO_MT_SWIFT.
   FECHA                : 21/06/2016.
   AUTOR                : SANTANDER.
   INPUT                : NA
   OUTPUT               : P_CURSOR := DATOS CURSOR.
                          P_ERROR  := MANEJA DOS ESTADOS, 0 SIN ERRORES 1 EXISTEN ERRORES.
   OBSERVACIONES
   FECHA       USUARIO    DETALLE
   21/06/2016  SANTANDER  CREACIÓN.
   ********************************************************************************************************/
   PROCEDURE SP_PAB_COMBO_MT_SWIFT_CLP (p_CURSOR   OUT SYS_REFCURSOR,
                                        p_ERROR    OUT NUMBER);

   --
   --***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_COMBO_TIPO_OPERACION
   -- Objetivo: PROCEDIMIENTO ALMACENADO QUE CONSULTA Y ENTREGA TODOS LOS TIPOS DE OPERACION QUE SON
   -- UTILIZADOS EN LA APLICACION DE ALTOS MONTOS. LOS QUE SON LLAMADOS EN LOS COMBO BOX.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_TIPO_OPRCN_ALMOT
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- N/A
   -- Output:
   -- p_CURSOR -> SYS_REFCURSOR, RESULTADOS DE CONSULTA.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_COMBO_TIPO_OPERACION (p_CURSOR OUT SYS_REFCURSOR);

   --
   --***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUSCA_INFO_ARCH
   -- Objetivo: Procedimiento que recibe como parametro ID_ARCHIVO, y devuelve la estructura de este
   -- en la tabla PABS_DT_ARCHv_ENTRD_SALID.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_ARCHv_ENTRD_SALID
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- p_IDF_ARC -> identificador unico de la tabla ARCHIVO.
   -- Output:
   -- p_CURSOR -> SYS_REFCURSOR, resultados de consulta.
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_BUSCA_INFO_ARCH (p_IDF_ARC   IN     CHAR,
                                     p_CURSOR       OUT SYS_REFCURSOR,
                                     p_ERROR        OUT NUMBER);

   --
   --***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_COMBO_BAN_ORD
   -- Objetivo: PROCEDIMIENTO ALMACENADO QUE CONSULTA Y ENTREGA TODOS LOS BANCOS DEL SISTEMA
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_ARCHv_ENTRD_SALID
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- N/A
   -- Output:
   -- p_CURSOR -> SYS_REFCURSOR, resultados de consulta.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_COMBO_BAN_ORD (p_CURSOR OUT SYS_REFCURSOR);

   --
   --***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_DEv_FECHA_HORA_SERV
   -- Objetivo: PROCEDIMIENTO QUE DEVUELVE LA FECHA Y HORA DEL SERVIDOR
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: DUAL
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- N/A
   -- Output:
   -- p_FECHA_SERV -> Fecha del servidor
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_DEv_FECHA_HORA_SERV (p_FECHA_SERV OUT DATE);

   --

   --**********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_VAL_HOR_SIS
   -- Objetivo: Procedimiento que informa si el sistema esta fuera de horario.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_SISTM_ENTRD_SALID
   -- Fecha: 19/08/16
   -- Autor: Santander
   -- Input:
   -- p_COD_SIS_ENT -> Codigo de sistema
   -- Output:
   -- p_FLG_SIS_HAB -> Indicador para informar si esta en horario valido -> 0 cumple y 1 no cumple
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_VAL_HOR_SIS (p_COD_SIS_ENT   IN     CHAR,
                                 p_FLG_SIS_HAB      OUT NUMBER,
                                 p_ERROR            OUT NUMBER);

   --**********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUSCA_INFO_SIS
   -- Objetivo: Procedimiento que despliega datos del sistema o producto consultado.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_SISTM_ENTRD_SALID
   -- Fecha: 19/08/16
   -- Autor: Santander
   -- Input:
   -- p_COD_SIS_ENT_SAL -> Codigo de sistema
   -- Output:
   -- p_DSC_SIS -> Descripcion del sistema de entrada / salida
   -- p_HOR_ICO -> Hora de inicio ventana de tiempo sistema
   -- p_HOR_CRR -> Hora de Cierre ventana de tiempo sistema
   -- p_FLG_ENT_SAL -> Flag de sistema entrada salida
   -- p_FLG_BSR_IFM -> Flag de busqueda informacion
   -- p_COD_SNO_CTB -> Codigo sinonimp contabilidad
   -- Output:
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_BUSCA_INFO_SIS (p_COD_SIS_ENT_SAL   IN     CHAR,
                                    p_DSC_SIS              OUT VARCHAR2,
                                    p_HOR_ICO              OUT NUMBER,
                                    p_HOR_CRR              OUT NUMBER,
                                    p_FLG_ENT_SAL          OUT NUMBER,
                                    p_FLG_BSR_IFM          OUT NUMBER,
                                    p_COD_SNO_CTB          OUT CHAR,
                                    P_ERROR                OUT NUMBER);

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_TRAN_COD_EST_GLOSA
   -- Objetivo: Funcion que transforma segun el codigo del estado la glosa que le corresponde a este estado
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PKG_PAB_CONSTANTES
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- p_COD_EST->     Identificador de nuevo estado.
   -- p_TIP_ORI ->    Identifica si la consulta es por estado de nomina o operacion
   -- Output: N/A.
   -- Input/Output: N/A.
   -- Retorno: p_DSC_ESTADO -> sigla del estado
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   FUNCTION FN_PAB_TRAN_COD_EST_GLOSA (p_COD_EST   IN NUMBER,
                                       p_TIP_ORI   IN NUMBER)
      RETURN VARCHAR2;

   --***********************************************************************************************
   -- Funcion/Procedimiento: Fn_PAB_COMBO_PAIS
   -- Objetivo: Devuelve la glosa del PAIS
   -- Sistema: PAB
   -- Base de Datos: TABLAS CORPORATIVAS
   -- Tablas Usadas: TGEN_0112
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- p_COD_PAIS -> CODIGO DEL PAIS
   -- Output:
   -- N/A
   -- Input/Output: N/A
   -- Retorno:
   -- v_GLOSA_PAIS -> Devuelve el nombre del pais
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   FUNCTION Fn_PAB_COMBO_PAIS (p_COD_PAIS IN VARCHAR2)
      RETURN VARCHAR2;

   --
   --***********************************************************************************************
   -- Funcion/Procedimiento: Fn_PAB_COMBO_CAMARA
   -- Objetivo: Consulta los tipos de camara para altos montos
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas: PABS_DT_TIPO_CAMAR_ALMOT
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input: p_COD_TPO_CMA -> codigo del tipo camara
   -- N/A
   -- Output:
   -- N/A.
   -- Input/Output:
   -- N/A.
   -- Retorno:
   -- v_GLS_TPO_CMA-> GLOSA TIPO CAMARA
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   FUNCTION Fn_PAB_COMBO_CAMARA (p_COD_TPO_CMA IN CHAR)
      RETURN VARCHAR2;

   --
   --***********************************************************************************************
   -- Funcion/Procedimiento: Fn_PAB_COMBO_SUCURSAL
   -- Objetivo: Consulta los tipos de sucursal para altos montos
   -- Sistema: PAB
   -- Base de Datos: TCDT050
   -- Tablas Usadas:
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input: p_tccofici -> codigo de la sucursal
   -- Output:
   -- N/A.
   -- Input/Output:
   -- Retorno:
   -- v_tcmofcur -> glosa de tipo de sucursal.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   FUNCTION Fn_PAB_COMBO_SUCURSAL (p_tccofici IN VARCHAR2)
      RETURN VARCHAR2;

   --
   --***********************************************************************************************
   -- Funcion/Procedimiento: Fn_PAB_COMBO_TSALDO
   -- Objetivo: Consulta los tipos de SALDO para altos montos
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas: PABS_DT_TIPO_SALDO_ALMOT
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input: p_COD_TPO_SDO -> codigo de tipo saldo
   -- Output:
   -- N/A.
   -- Input/Output:
   -- N/A.
   -- Retorno: v_GLS_TPO_SDO->GLOSA TIPO SALDO
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   FUNCTION Fn_PAB_COMBO_TSALDO (p_COD_TPO_SDO IN CHAR)
      RETURN VARCHAR2;

   --
   --***********************************************************************************************
   -- Funcion/Procedimiento: Fn_PAB_COMBO_TIPO_OPERACION
   -- Objetivo: PROCEDIMIENTO ALMACENADO QUE CONSULTA Y ENTREGA TODOS LOS TIPOS DE OPERACION
   -- UTILIZADOS EN LA APLICACION DE ALTOS MONTOS. LOS QUE SON LLAMADOS EN LOS COMBO BOX.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_TIPO_OPRCN_ALMOT
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- P_COD_TPO_OPE_AOS -> TIPO DE OPERACIONES
   -- Output:
   -- N/A.
   -- Input/Output:
   -- N/A.
   -- Retorno:
   -- v_DSC_TPO_OPE_AOS -> TIPO OPERACION
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   FUNCTION Fn_PAB_COMBO_TIPO_OPERACION (P_COD_TPO_OPE_AOS IN CHAR)
      RETURN VARCHAR2;

   --***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_COMBO_PAIS
   -- Objetivo: Devuelve los paises que soporta el sistema
   -- Sistema: PAB
   -- Base de Datos: TABLAS CORPORATIVAS
   -- Tablas Usadas: TGEN_0112
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- N/A
   -- Output:
   -- p_CURSOR -> SYS_REFCURSOR, RESULTADOS DE CONSULTA.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_COMBO_PAIS (p_CURSOR OUT SYS_REFCURSOR);

   --***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_COMBO_TSALDO
   -- Objetivo: Consulta los tipos de saldos para altos montos
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas: PABS_DT_TIPO_SALDO_ALMOT
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- N/A
   -- Output:
   -- p_CURSOR -> SYS_REFCURSOR, RESULTADOS DE CONSULTA.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_COMBO_TSALDO (p_CURSOR OUT SYS_REFCURSOR);

   --**********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_VAL_RUT
   -- Objetivo: Procedimiento que valida RUT de acuerdo a su digito verificador
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: N/A
   -- Fecha: 19/08/16
   -- Autor: Santander
   -- Input:
   -- p_NUM_RUT -> Numero de RUT
   -- p_DIG_VERIF -> Digito Verificador
   -- Output:
   -- p_FLAG_VAL -> Flag cumple con las condiciones de hora y monto importe maximo -> 0 cumple y 1 no cumple
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_VAL_RUT (p_NUM_RUT     IN     NUMBER,
                             p_DIG_VERIF   IN     CHAR,
                             p_FLAG_VAL       OUT NUMBER,
                             p_ERROR          OUT NUMBER);

   --***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_COMBO_CAMARA
   -- Objetivo: Consulta los tipos de camara para altos montos
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas:
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- N/A
   -- Output:
   -- p_CURSOR -> SYS_REFCURSOR, RESULTADOS DE CONSULTA.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_COMBO_CAMARA (p_CURSOR OUT SYS_REFCURSOR);

   --***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_COMBO_SUCURSAL
   -- Objetivo: Consulta los tipos de sucursal para altos montos
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas:
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- N/A
   -- Output:
   -- p_CURSOR -> SYS_REFCURSOR, RESULTADOS DE CONSULTA.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_COMBO_SUCURSAL (p_CURSOR OUT SYS_REFCURSOR);

   --***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_COMBO_SEGMENTO
   -- Objetivo: Consulta los tipos de segmento de los clientes
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas:
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- N/A
   -- Output:
   -- p_CURSOR -> SYS_REFCURSOR, RESULTADOS DE CONSULTA.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_COMBO_SEGMENTO (p_CURSOR OUT SYS_REFCURSOR);

   --***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_COMBO_ESTADO_OPE
   -- Objetivo: Consulta los tipos de estados de operacion
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas:
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- N/A
   -- Output:
   -- p_CURSOR -> SYS_REFCURSOR, RESULTADOS DE CONSULTA.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones:
   --***********************************************************************************************
   PROCEDURE SP_PAB_COMBO_ESTADO_OPE (p_CURSOR OUT SYS_REFCURSOR);

   --***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_COMBO_ESTADO_NOM
   -- Objetivo: Consulta los tipos de estados de una nomina
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas:
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- N/A
   -- Output:
   -- p_CURSOR -> SYS_REFCURSOR, RESULTADOS DE CONSULTA.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_PAB_COMBO_ESTADO_NOM (p_CURSOR OUT SYS_REFCURSOR);

   --***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_COMBO_TFONDO
   -- Objetivo: Consulta los tipos de fondos
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas: PABS_DT_TIPO_FONDO_ALMOT
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- N/A
   -- Output:
   -- p_CURSOR -> SYS_REFCURSOR, RESULTADOS DE CONSULTA.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_COMBO_TFONDO (p_CURSOR OUT SYS_REFCURSOR);

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_TRAN_COD_FONDO
   -- Objetivo: Funcion que transforma segun el codigo de fondo
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_TIPO_FONDO_ALMOT
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- p_COD_FND -> Codigo identificador del Fondo.
   -- Output: N/A.
   -- Input/Output: N/A.
   -- Retorno:
   -- v_GLOSA_FONDO -> Retorno Descripcion del Fondo
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   FUNCTION FN_PAB_TRAN_COD_FONDO (p_COD_FND IN CHAR)
      RETURN VARCHAR2;

   --**********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUS_VAL_ARC
   -- Objetivo: Procedimiento que consultara la tabla donde se almacenaran las distintas relaciones
   -- que existen entre los tipos de mensajes, tipo de operaciones y  sistema de salida.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: N/A
   -- Fecha: 14/09/2016
   -- Autor: Santander
   -- Input:
   -- p_COD_ARC_ENT -> Codigo de archivo de Entrada
   -- Output:
   -- p_CURSOR  -> Cursor con todas las operaciones
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_BUS_VAL_ARC (p_COD_ARC_ENT   IN     CHAR,
                                 p_CURSOR           OUT SYS_REFCURSOR,
                                 p_ERROR            OUT NUMBER);

   --***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_COMBO_TIPO_OPE_FILMT
   -- Objetivo: PROCEDIMIENTO ALMACENADO QUE CONSULTA Y ENTREGA TODOS LOS TIPOS DE OPERACION QUE SON
   -- UTILIZADOS EN LA APLICACION DE ALTOS MONTOS. LOS QUE SON LLAMADOS EN LOS COMBO BOX .
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_TIPO_OPRCN_ALMOT
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- p_COD_MT_SWF -> Codigo tipo de mensaje
   -- P_COD_DVI -> Codigo de moneda
   -- p_COD_SIS_ENT_SAL -> Codigo de sistema
   -- Output:
   -- p_CURSOR -> SYS_REFCURSOR, RESULTADOS DE CONSULTA.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_COMBO_TIPO_OPE_FILMT (
      p_COD_MT_SWF        IN     CHAR,
      P_COD_DVI           IN     CHAR,
      p_COD_SIS_ENT_SAL   IN     CHAR,
      p_CURSOR               OUT SYS_REFCURSOR
   );

   --***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_COMBO_MONEDA_FILMT
   -- Objetivo: Devuelve el codigo del tipo de Fondo
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_MONEDA_ALMOT
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- p_COD_MT_SWF:
   -- Output:
   -- p_CURSOR -> SYS_REFCURSOR, RESULTADOS DE CONSULTA.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_COMBO_MONEDA_FILMT (p_COD_MT_SWF   IN     CHAR,
                                        p_CURSOR          OUT SYS_REFCURSOR);

   --**********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_BUS_DTO_BFC_ING_UNI
   -- Objetivo: Busca los datos del beneficiario
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas:
   -- Fecha: 26/09/16
   -- Autor: Santander
   -- Input:
   -- p_COD_USR -> rut usuario
   -- p_COD_BIC -> BIC
   -- Output:
   -- P_DATOS_BFC --> Datos del beneficiario
   -- P_NUM_CTA_BFC --> Cuentas del beneficiario
   -- P_NUM_CTA_DCV_BFC --> Cuenta DCV Beneficiario
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_PAB_BUS_DTO_BFC_ING_UNI (
      -- ENTRADAS
      P_COD_USR           IN     CHAR,
      P_COD_BIC           IN     CHAR,
      -- SALIDAS
      P_DATOS_BFC            OUT SYS_REFCURSOR,
      P_NUM_CTA_BFC          OUT SYS_REFCURSOR,
      P_NUM_CTA_DCV_BFC      OUT SYS_REFCURSOR,
      P_ERROR                OUT NUMBER
   );

   --**********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUS_DTO_ODN_ING_UNI
   -- Objetivo: Busca los datos del Ordenante
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas:
   -- Fecha: 26/09/16
   -- Autor: Santander
   -- Input:
   -- p_COD_USR -> rut usuario
   -- Output:
   -- P_DATOS_ODN --> Datos del ordenante
   -- P_NUM_CTA_ODN --> Cuentas del ordenante
   -- P_NUM_CTA_DCV_ODN --> Cuenta DCV Ordenante
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_PAB_BUS_DTO_ODN_ING_UNI (
      -- ENTRADAS
      P_COD_USR           IN     CHAR,
      -- SALIDAS
      P_DATOS_ODN            OUT SYS_REFCURSOR,
      P_NUM_CTA_ODN          OUT SYS_REFCURSOR,
      P_NUM_CTA_DCV_ODN      OUT SYS_REFCURSOR,
      P_ERROR                OUT NUMBER
   );

   -- **********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_VAL_MONDA_ALMOT
   -- Objetivo: Procedimiento que informa si el importe ingresado excede o no el definido en la moneda.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_MONDA_ALMOT
   -- Fecha: 19/08/16
   -- Autor: Santander
   -- Input:
   -- p_COD_DVI -> Codigo Divisa
   -- p_IMP_INF -> Importe informado para consultar en tabla de moneda
   -- Output:
   -- p_FLG_IMP_VAL -> Flag monto importe valido = 1 o invalido = 0
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_VAL_MONDA_ALMOT (p_COD_DVI       IN     CHAR,
                                     p_IMP_INF       IN     NUMBER,
                                     p_FLG_IMP_VAL      OUT NUMBER,
                                     p_ERROR            OUT NUMBER);

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_OBT_DSC_SIS_ENT_SAL
   -- Objetivo: Funcion que devuelve la descripcion del codigo del sistema de entrada o salida
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_SISTM_ENTRD_SALID
   -- Fecha: 18/10/2016
   -- Autor: Santander
   -- Input:
   -- p_COD_SIS_ENT_SAL -> Codigo de sistema salida
   -- Output:
   -- N/A.
   -- Input/Output:
   -- N/A.
   -- Retorno:
   -- v_DSC_SIS_ENT_SAL -> Descripcion de codigo de sistema salida consultado
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   FUNCTION FN_PAB_OBT_DSC_SIS_ENT_SAL (p_COD_SIS_ENT_SAL IN CHAR)
      RETURN VARCHAR2;

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_OBT_COD_DSC_EST
   -- Objetivo: Funcion que devuelve codigo del estado de nomina u operacion consultando por la
   -- descripcion del estado
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PKG_PAB_CONSTANTES
   -- Fecha: 14/10/2016
   -- Autor: Santander
   -- Input:
   -- p_GLS_EST -> Descripcion del eatado de la operacion informada
   -- p_TIP_ORI -> Tipo origen que consulta estado
   -- Output:
   -- N/A.
   -- Input/Output:
   -- N/A.
   -- Retorno:
   -- v_COD_EST -> Codigo del estado de la operacion informada
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   FUNCTION FN_PAB_OBT_COD_DSC_EST (p_DSC_EST VARCHAR2, p_TIP_ORI IN NUMBER)
      RETURN NUMBER;

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_OBT_DSC_COD_EST
   -- Objetivo: Funcion que devuelve descripcion del estado de nomina u operacion consultando por el
   -- codigo del estado
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PKG_PAB_CONSTANTES
   -- Fecha: 14/10/2016
   -- Autor: Santander
   -- Input:
   -- p_COD_EST -> Codigo del eatado de la operacion informada
   -- Output:
   -- N/A.
   -- Input/Output:
   -- N/A.
   -- Retorno:
   -- v_GLS_EST -> Descripcion del estado de la operacion informada
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   FUNCTION FN_PAB_OBT_DSC_COD_EST (p_COD_EST IN VARCHAR2)
      RETURN VARCHAR2;

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_OBT_TIP_ING
   -- Objetivo: Funcion que devuelve la descripcion del codigo del tipo de ingreso
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_TIPO_INGRO_ALMOT
   -- Fecha: 18/10/2016
   -- Autor: Santander
   -- Input:
   -- p_COD_TPO_ING -> Codigo de tipo de ingreso
   -- Output:
   -- N/A.
   -- Input/Output:
   -- N/A.
   -- Retorno:
   -- v_DSC_TPO_ING -> Descripcion de codigo de tipo de ingreso
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   FUNCTION FN_PAB_OBT_TIP_ING (p_COD_TPO_ING IN NUMBER)
      RETURN VARCHAR2;

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_OBT_DSC_TIP_OPE
   -- Objetivo: Funcion que devuelve la descripcion del codigo del tipo de operacion
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_TIPO_OPRCN_ALMOT
   -- Fecha: 18/10/2016
   -- Autor: Santander
   -- Input:
   -- p_COD_TPO_OPE_AOS -> Codigo de tipo operacion
   -- Output:
   -- N/A.
   -- Input/Output:
   -- N/A.
   -- Retorno:
   -- v_DSC_TPO_OPE_AOS -> Retorna Descripcion de codigo de tipo operacion
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   FUNCTION FN_PAB_OBT_DSC_TIP_OPE (p_COD_TPO_OPE_AOS IN CHAR)
      RETURN VARCHAR2;

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_OBT_DSC_BCO
   -- Objetivo: Funcion que devuelve la descripcion del codigo del banco
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: TCDTBAI
   -- Fecha: 18/10/2016
   -- Autor: Santander
   -- Input:
   -- p_TGCDSWSA -> Codigo de banco
   -- Output:
   -- N/A.
   -- Input/Output:
   -- N/A.
   -- Retorno:
   -- v_DES_BCO -> Descripcion de codigo de tipo de ingreso
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   FUNCTION FN_PAB_OBT_DSC_BCO (p_TGCDSWSA IN CHAR)
      RETURN CHAR;

   --***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_COMBO_CRPSL
   -- Objetivo: PROCEDIMIENTO ALMACENADO QUE CONSULTA Y ENTREGA TODOS LOS BANCOS CORRESPONSALES DEL SISTEMA
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_ARCHv_ENTRD_SALID
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- N/A
   -- Output:
   -- p_CURSOR -> SYS_REFCURSOR, resultados de consulta.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_PAB_COMBO_CRPSL (p_CURSOR OUT SYS_REFCURSOR);

   --
   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_COMBO_EST_DEV_EGR
   -- Objetivo: Procedimiento que consulta estados Por Abonar o Por Asociar
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_ESTDO_ALMOT
   -- Fecha: 27/10/16
   -- Autor: Santander
   -- Input:
   -- N/A
   -- Output:
   -- p_CURSOR -> Cursor con los registros de los 2 estados
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_COMBO_EST_DEV_EGR (p_CURSOR   OUT SYS_REFCURSOR,
                                       p_ERROR    OUT NUMBER);

   --

   --**********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_BTCOR_OPRCN
   -- Objetivo: Procedimiento que devuelve la bitacora de una operacion
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_BTCOR_OPRCN, PABS_DT_ESTDO_ALMOT
   -- Fecha: 07/12/2016
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_OPE
   -- p_FEC_ISR_OPE
   -- Output:
   -- p_CURSOR_BTCOR
   -- p_ERROR -> Indicador de errores (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_PAB_BTCOR_OPRCN (p_NUM_FOL_OPE    IN     NUMBER,
                                 p_FEC_ISR_OPE    IN     DATE,
                                 p_CURSOR_BTCOR      OUT SYS_REFCURSOR,
                                 p_ERROR             OUT NUMBER);

   --**********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_ELI_CTA_TRF
   -- Objetivo: Procedimiento que elimina cuenta corriente de las habituales de usuario en el ingreso de operacion
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_CENTA_TRNSC
   -- Fecha: 15/11/16
   -- Autor: Santander
   -- Input:
   -- p_NUM_CTA_TRF -> Numero de cuenta
   -- p_COD_BIC --> Codigo BIC de la cuenta
   -- p_FLG_DCV -> Indicado de cuenta DCV cuando es 1
   -- p_NUM_DOC_PSN -> Rut de usuario
   -- p_COD_TPD_PSN -> Dv usuario
   -- Output:
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_PAB_ELI_CTA_TRF (p_NUM_CTA_TRF   IN     CHAR,
                                 p_COD_BIC       IN     CHAR,
                                 p_FLG_DCV       IN     NUMBER,
                                 p_NUM_DOC_PSN   IN     CHAR,
                                 p_COD_TPD_PSN   IN     CHAR,
                                 p_ERROR            OUT NUMBER);

   --***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_COMBO_MERCADO
   -- Objetivo: PROCEDIMIENTO ALMACENADO QUE DEVUELVE MERCADO.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: DUAL
   -- Fecha: 06/01/17
   -- Autor: Santander
   -- Input:
   -- N/A
   -- Output:
   -- p_CURSOR -> SYS_REFCURSOR, RESULTADOS DE CONSULTA.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_COMBO_MERCADO (p_CURSOR OUT SYS_REFCURSOR);

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_CMB_TIP_ABO
   -- Objetivo: Procedimiento que consulta y lista las tipos de abonos.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: N/A
   -- Fecha: 07/11/16
   -- Autor: Santander
   -- Input:
   -- p_COD_MT_SWF -> Codigo motivo SWIFT
   -- Output:
   -- p_CURSOR -> Cursor con loa tipos de abonos
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_CMB_TIP_ABO (p_COD_MT_SWF   IN     NUMBER,
                                 p_CURSOR          OUT SYS_REFCURSOR,
                                 p_ERROR           OUT NUMBER);

   --
   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_BUS_CTA_ABO
   -- Objetivo: Procedimiento almacenado que busca numero relacionado a la cuenta del tipo abono
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_CENTA_CNTBL
   -- Fecha: 07/11/16
   -- Autor: Santander
   -- Input:
   -- p_COD_SIS_ENT_SAL ->  codigo sistema entrada salida
   -- p_COD_DVI ->  Tipo de operaci?n Ingreso o Egreso
   -- Output:
   -- p_CURSOR -> Cursor con registro encontrado
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_BUS_CTA_ABO (p_COD_SIS_ENT_SAL   IN     CHAR,
                                 p_COD_DVI           IN     CHAR,
                                 p_CURSOR               OUT SYS_REFCURSOR,
                                 p_ERROR                OUT NUMBER);

   --
   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_COMBO_AREA
   -- Objetivo: Procedimiento que busca Area
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_SISTM_ENTRD_SALID
   -- Fecha: 28/04/2017
   -- Autor: Santander- CAH
   -- Input:
   -- N/A
   -- Output:
   -- p_CURSOR -> Cursor con los registros de los 2 estados
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************                                    );
   PROCEDURE SP_PAB_COMBO_AREA (p_CURSOR   OUT SYS_REFCURSOR,
                                p_ERROR    OUT NUMBER);

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_COMBO_CRPSL_BAI
   -- Objetivo: Procedimiento que trae los corresponsales que no estan indicados en el altos montos
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_SISTM_ENTRD_SALID
   -- Fecha: 28/04/2017
   -- Autor: Santander- CAH
   -- Input:
   -- N/A
   -- Output:
   -- p_CURSOR -> Cursor con los registros de los 2 estados
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************                                    );
   --PROCEDURE Sp_PAB_COMBO_CRPSL_BAI(p_CURSOR OUT SYS_REFCURSOR,
   --                                 p_ERROR     OUT NUMBER);
   --
   -- ***********************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_VLD_TXT
   -- Objetivo: Procedimiento que RETORNA EL TEXTO sin caracteres especiales
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_VLDCN_CRCTR_SWIFT
   -- Fecha: 28/04/2017
   -- Autor: SII Group
   -- Input: p_VLD_TXT_ENT = Texto a validar
   -- Output:
   -- N/A
   -- Input/Output: N/A
   -- Retorno: texto validado (V_VLD_TXT_ENT)
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   FUNCTION FN_PAB_VLD_TXT (p_VLD_TXT_ENT IN VARCHAR2)
      RETURN VARCHAR2;

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_COMBO_EST_DEB_PAT
   -- Objetivo: Procedimiento que consulta estados Por Abonar o Por Asociar
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_ESTDO_ALMOT
   -- Fecha: 27/10/16
   -- Autor: Santander
   -- Input:
   -- N/A
   -- Output:
   -- p_CURSOR -> Cursor con los registros de los 2 estados
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_COMBO_EST_DEB_PAT (p_CURSOR   OUT SYS_REFCURSOR,
                                       p_ERROR    OUT NUMBER);

   /*******************************************************************************************************
    FUNCION/PROCEDIMIENTO: SP_PAB_COMBO_CANAL_ENT_TODO.
    OBJETIVO             : MUESTRA FILTRO EN
                            CONSULTAS -> SWIFT -> OPERACIONES
                                      COMBO: CANAL DE ENTRADA.
    SISTEMA              : DBO_PAB.
    BASE DE DATOS        : DBO_PAB.
    TABLAS USADAS        : PABS_DT_SISTM_ENTRD_SALID.
    FECHA                : 20/09/2018.
    AUTOR                : VARAYA.
    INPUT                : NA.
    OUTPUT               : P_CURSOR := DATOS CURSOR.
    OBSERVACIONES
    FECHA       USUARIO    DETALLE
    20/09/2018  VARAYA     CREACIÓN.
    *******************************************************************************************************/
   PROCEDURE Sp_PAB_COMBO_CANAL_ENT_TODO (P_CURSOR OUT SYS_REFCURSOR);

   /********************************************************************************************************
   FUNCION/PROCEDIMIENTO: PKG_PAB_UTILITY.SP_PAB_COMBO_CANAL_SAL_TODO.
   OBJETIVO             : MUESTRA FILTRO EN
                           CONSULTAS -> SWIFT ->OPERACIONES
   SISTEMA              : DBO_PAB.
   BASE DE DATOS        : DBO_PAB.
   TABLAS USADAS        : PABS_DT_SISTM_ENTRD_SALID.
   FECHA                : 20/09/2018.
   AUTOR                : VARAYA.
   OUTPUT               : P_CURSOR := DATOS CURSOR.
   OBSERVACIONES
   FECHA       USUARIO    DETALLE
   20-09-2018  VARAYA     CREACIÓN.
   ********************************************************************************************************/
   PROCEDURE SP_PAB_COMBO_CANAL_SAL_TODO (P_CURSOR OUT SYS_REFCURSOR);

   /********************************************************************************************************
   FUNCION/PROCEDIMIENTO: PKG_PAB_UTILITY.SP_PAB_COMBO_CANAL_ENT_INT.
   OBJETIVO             : MUESTRA FILTRO EN COMBO:
                           CONSULTAS -> INTERNAS -> OPERACIONES.
   SISTEMA              : DBO_PAB.
   BASE DE DATOS        : DBO_PAB.
   TABLAS USADAS        : PABS_DT_SISTM_ENTRD_SALID.
   FECHA                : 20/09/2018.
   AUTOR                : VARAYA.
   OUTPUT               : P_CURSOR := DATOS CURSOR.
   OBSERVACIONES
   FECHA       USUARIO    DETALLE
   20-09-2018  VARAYA     CREACIÓN.
   ********************************************************************************************************/
   PROCEDURE SP_PAB_COMBO_CANAL_ENT_INT (P_CURSOR OUT SYS_REFCURSOR);

   /********************************************************************************************************
   FUNCION/PROCEDIMIENTO: PKG_PAB_UTILITY.SP_PAB_COMBO_CANAL_SAL_INT.
   OBJETIVO             : MUESTRA FILTRO EN COMBO:
                           CONSULTAS -> INTERNAS -> OPERACIONES.
   SISTEMA              : DBO_PAB.
   BASE DE DATOS        : DBO_PAB.
   TABLAS USADAS        : PABS_DT_SISTM_ENTRD_SALID.
   FECHA                : 20/09/2018.
   AUTOR                : VARAYA.
   OUTPUT               : P_CURSOR := DATOS CURSOR.
   OBSERVACIONES
   FECHA       USUARIO    DETALLE
   20-09-2018  VARAYA     CREACIÓN.
   ********************************************************************************************************/
   PROCEDURE SP_PAB_COMBO_CANAL_SAL_INT (P_CURSOR OUT SYS_REFCURSOR);

   /********************************************************************************************************
   FUNCION/PROCEDIMIENTO: SP_PAB_COMBO_MT_INT.
   OBJETIVO             : MUESTRA FILTRO EN
                           CONTINGENCIA -> ENVIADO -> BCCH UPLOAD - DESCARGAR ARCHIVO.
                           CONTINGENCIA -> ENVIADO -> COMBANC UPLOAD - DESCARGAR ARCHIVO.
   SISTEMA              : DBO_PAB.
   BASE DE DATOS        : DBO_PAB.
   TABLAS USADAS        : PABS_DT_TIPO_MT_SWIFT.
   FECHA                : 20/09/2018.
   AUTOR                : VARAYA.
   INPUT                : P_CANAL  := CANAL.
   OUTPUT               : P_CURSOR := DATOS CURSOR.
   OBSERVACIONES
   FECHA       USUARIO    DETALLE
   20-09-2018  VARAYA     CREACIÓN.
   ********************************************************************************************************/
   PROCEDURE SP_PAB_COMBO_MT_INT (p_CURSOR OUT SYS_REFCURSOR);

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_COMBO_EGR_ING
   -- Objetivo: Procedimiento que devuelve tipo de Egreso
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_SISTM_ENTRD_SALID
   -- Fecha: 05/06/17
   -- Autor: Santander
   -- Input:
   -- Output:
   -- p_CURSOR   -> cursor
   -- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_COMBO_EGR_ING (p_CURSOR   OUT SYS_REFCURSOR,
                                   p_ERROR    OUT NUMBER);

   --***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_COMBO_ESTADO_MSJ
   -- Objetivo: Consulta los tipos de estados de mensajes de protocolo
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_ESTDO_ALMOT
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- N/A
   -- Output:
   -- p_CURSOR -> Cursor que devuelve los estados de los mensajes de protocolo
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones:
   --***********************************************************************************************
   PROCEDURE SP_PAB_COMBO_ESTADO_MSJ (p_CURSOR OUT SYS_REFCURSOR);

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_COMBO_REC_ENV
   -- Objetivo: Procedimiento que devuelve tipo de Egreso
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: DUAL
   -- Fecha: 05/06/17
   -- Autor: Santander
   -- Input: N/A
   -- Output:
   -- p_CURSOR  -> Cursor que contiene los valores utilizados para el combo de flujo operacion (Enviado / Recibido)
   -- p_ERROR-> Indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones:
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_COMBO_REC_ENV (p_CURSOR   OUT SYS_REFCURSOR,
                                   p_ERROR    OUT NUMBER);

   /************************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_GEN_FOL_AM
   -- Objetivo: Funcion que retorna el folio solicitado segun la variable de entrada
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: N/A
   -- Fecha: 05/06/2017
   -- Autor: GCP
   -- Input:
   -- P_STR_FOL: Valor de la secuencia solicitada ej (PEAM: Egreso Altos Montos)
   -- Output:
   -- N/A
   -- Input/Output: N/A
   -- Retorno: Retorna el folio solicitado
   -- Observaciones:
   --***********************************************************************************************/
   FUNCTION FN_PAB_GEN_FOL_AM (P_STR_FOL CHAR)
      RETURN NUMBER;

   /**********************************************************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_INS_MVNTO_CARGO_ABONO
   -- Objetivo: Procedimiento almacenado que inserta los movimientos de abono y cargo de Cuentas corrientes en la tabla de resgistro Abo/car
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_RGTRO_OPRCN_CARGO_ABON
   --
   -- Fecha: 01/09/2017
   -- Autor: Santander CAH
   -- Input:
   --  p_NUM_MOV_CTB   Movimiento contable
   --  p_NUM_FOL_OPE   Numero de operación
   --  p_FEC_ISR_OPE   Fecha de inserción de la operación
   --  p_FLG_CGO_ABN   Flag indicador de Abono o Cargo
   -- Output:
   --  p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --************************************************************************************************************************************************/
   PROCEDURE Sp_PAB_INS_MVNTO_CARGO_ABONO (p_NUM_MOV_CTB   IN     CHAR,
                                           p_NUM_FOL_OPE   IN     NUMBER,
                                           p_FEC_ISR_OPE   IN     DATE,
                                           p_FLG_CGO_ABN   IN     NUMBER,
                                           p_ERROR            OUT NUMBER);

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_COMBO_TPO_MT_CRTLA
   -- Objetivo: Procedimiento que consulta Por MT940 y MT950
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_TIPO_MT_SWIFT
   -- Fecha: 06-09-2017
   -- Autor: Santander- CAH
   -- Input:
   -- N/A
   -- Output:
   -- p_CURSOR -> Cursor con los registros de los 2 estados
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_COMBO_TPO_MT_CRTLA (p_CURSOR   OUT SYS_REFCURSOR,
                                        p_ERROR    OUT NUMBER);

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_DEV_DIA_HABIL_SIG
   -- Objetivo: Procedimiento que devuelve el día habil siguiente desde la fecha de entrada
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: N/A
   -- Fecha: 06-09-2017
   -- Autor: Santander- GCP
   -- Input:
   -- p_calendario -> Calendario a validar
   -- Output:
   -- Input/Output: N/A
   -- p_fecha -> Fecha que se valida
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_DEV_DIA_HABIL_SIG (p_calendario   IN     CHAR,
                                       p_fecha        IN OUT DATE);

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_DEV_DIA_HABIL_ANT
   -- Objetivo: Procedimiento que devuelve el día habil anterior desde la fecha de entrada
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: N/A
   -- Fecha: 06-09-2017
   -- Autor: Santander- GCP
   -- Input:
   -- p_calendario -> Calendario a validar
   -- Output:
   -- Input/Output: N/A
   -- p_fecha -> Fecha que se valida
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_DEV_DIA_HABIL_ANT (p_calendario   IN     CHAR,
                                       p_fecha        IN OUT DATE);

   --***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_COMBO_PERFIL
   -- Objetivo: Procedimiento que devuelve los perfiles por Áarea ingresada
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_TIPO_ROL_ALMOT
   --              PABS_DT_RELCN_ROL_MENU

   -- Fecha:03-01-2018
   -- Autor: Santander CAH
   -- Input:
   --
   -- Output:
   -- p_CURSOR -> SYS_REFCURSOR, RESULTADOS DE CONSULTA.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_COMBO_PERFIL (p_COD_SIS_ENT   IN     CHAR,
                                  p_CURSOR           OUT SYS_REFCURSOR);

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_COMBO_MT_SWF_ABONO
   -- Objetivo: Procedimiento que consulta Por MT103 O MT202
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_TIPO_MT_SWIFT
   -- Fecha:05-01-2018
   -- Autor: Santander- CAH
   -- Input:
   -- N/A
   -- Output:
   -- p_CURSOR -> Cursor con los registros de los 2 estados
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_COMBO_MT_SWF_ABONO (p_CURSOR   OUT SYS_REFCURSOR,
                                        p_ERROR    OUT NUMBER);

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_COMBO_BCO_PAIS
   -- Objetivo: Procedimiento que retorna los banco por pais
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_TIPO_MT_SWIFT
   -- Fecha:05-01-2018
   -- Autor: Santander- CAH
   -- Input:
   --  p_PAIS --> Pais para buscar, código
   -- Output:
   -- p_CURSOR -> Cursor con los registros de los 2 estados
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_COMBO_BCO_PAIS (p_PAIS     IN     VARCHAR2,
                                    p_CURSOR      OUT SYS_REFCURSOR,
                                    p_ERROR       OUT NUMBER);

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_COMBO_BCO_MERCADO
   -- Objetivo: Procedimiento que retorna los banco segun mercado
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_TIPO_MT_SWIFT
   -- Fecha:08-01-2018
   -- Autor: Santander- CAH
   -- Input:
   --  p_MRCDO -->  código mercado
   -- Output:
   -- p_CURSOR -> Cursor con los registros de los 2 estados
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_COMBO_BCO_MERCADO (p_MRCDO    IN     NUMBER,
                                       p_CURSOR      OUT SYS_REFCURSOR,
                                       p_ERROR       OUT NUMBER);

   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_OBT_CNL_CON_TPO_OPE
   -- Objetivo: Obtiene el canal segun el tipo
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_TIPO_OPRCN_ALMOT
   -- Fecha: 16/08/2018
   -- Autor: Santander
   -- Input:
   -- P_COD_TPO_OPE_AOS -> Tipo de operación
   -- Output:
   -- P_COD_SIS_ENT_SAL -> Codigo sistema entrada/salida
   -- P_DSC_SIS_ENT_SAL -> Descripcion del canal entrada/salida
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: 27/09/2016, Creacion Procedimiento.
   --***********************************************************************************************
   PROCEDURE Sp_PAB_OBT_CNL_CON_TPO_OPE (P_COD_TPO_OPE_AOS   IN     CHAR,
                                         P_COD_SIS_ENT_SAL      OUT CHAR,
                                         P_DSC_SIS_ENT_SAL      OUT VARCHAR2,
                                         p_ERROR                OUT NUMBER);

   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_VAL_FEC_PROC_ABN
   -- Objetivo: Obtiene el canal segun el tipo
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_TIPO_OPRCN_ALMOT
   -- Fecha: 16/08/2018
   -- Autor: Santander
   -- Input:
   -- P_COD_TPO_OPE_AOS -> Tipo de operación
   -- Output:
   -- P_COD_SIS_ENT_SAL -> Codigo sistema entrada/salida
   -- P_DSC_SIS_ENT_SAL -> Descripcion del canal entrada/salida
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: 27/09/2016, Creacion Procedimiento.
   --***********************************************************************************************
   PROCEDURE Sp_PAB_VAL_FEC_PROC_ABN (P_FEC_ISR_OPE   IN     DATE,
                                      P_FEC_VTA       IN     DATE,
                                      P_COD_DVI       IN     VARCHAR2,
                                      p_ERROR            OUT NUMBER);


   /************************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_BUS_VVV
   -- Objetivo: Buscar si existe el numero de cuenta en AAMM
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: N/A
   -- Fecha: 02/06/16
   -- Autor: Santander
   -- Input:      p_NUM_VVV: Numero Cuenta Corriente
   -- Output:
   --
   -- Input/Output: N/A
   -- Retorno: Retorna si un numero de VVV esta ya registrado en AAMM
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   FUNCTION FN_PAB_BUS_VVV (p_NUM_VVV IN VARCHAR2)
      RETURN INTEGER;

   /************************************************************************************************
   -- Funcion/Procedimiento: FN_VAL_HOR_CMN
   -- Objetivo: Valida horario de COMBANC.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: N/A
   -- Fecha: 02/06/16
   -- Autor: Santander
   -- Input:      p_NUM_VVV: Numero Cuenta Corriente
   -- Output:
   --
   -- Input/Output: N/A
   -- Retorno: Retorna si un numero de VVV esta ya registrado en AAMM
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   FUNCTION FN_VAL_HOR_CMN
      RETURN VARCHAR2;

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_OBT_URL_EXTERNA
   -- Objetivo: Procedimiento que obtiene la ruta del una pagina externa al front .NET de AAMM en base al usuario y un ID
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: ??
   -- Fecha: 23/04/2020
   -- Autor: Santander
   -- Input:
   -- p_COD_USR -> rut usuario. (Obligatorio).
   -- p_COD_MENU_EXT -> Codigo de URL externa solicitada
   -- Output:
   -- p_URL:MENU_EXT -> URL asociada al usuario y código respectivo.
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_OBT_URL_EXTERNA (p_COD_USR        IN     VARCHAR2,
                                     p_COD_MENU_EXT   IN     NUMBER,
                                     p_URL_MENU_EXT      OUT VARCHAR2,
                                     p_ERROR             OUT NUMBER);


   -- ***********************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_DEV_DIA_HABIL_ANT
   -- Objetivo: Procedimiento que devuelve el día habil anterior desde la fecha de entrada
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: N/A
   -- Fecha: 06-09-2017
   -- Autor: Santander- GCP
   -- Input:
   -- p_calendario -> Calendario a validar
   -- Output:
   -- Input/Output: N/A
   -- p_fecha -> Fecha que se valida
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   FUNCTION FN_PAB_DEV_DIA_HABIL_ANT (p_calendario IN CHAR, p_fecha IN DATE)
      RETURN NUMBER;

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_OBT_TPO_PROC_VIG
   -- Objetivo: Procedimiento que obtiene la vigencia de un tipo de proceso
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_TIPO_PRCSO
   -- Fecha: 21/07/2020
   -- Autor: Santander
   -- Input:
   -- p_TPO_COD -> codigo tipo de proceso. (Obligatorio).
   -- Output:
   -- p_TPO_COD_VIG -> codigo del canal de entrada.
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: 22/07/2020: Creacion Julio Villalobos (julio.villalobos@servexternos.santander.cl)
   --***********************************************************************************************
   PROCEDURE Sp_PAB_OBT_TPO_PROC_VIG (p_TPO_COD       IN     CHAR,
                                      p_TPO_COD_VIG      OUT NUMBER,
                                      p_ERROR            OUT NUMBER);
END PKG_PAB_UTILITY;