CREATE OR REPLACE PACKAGE BODY         PKG_PAB_GARANTIA
AS
   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_INS_GRNTIA_CMDR_CMBNC
   -- Objetivo: Gestiona la insercion las operaciones que llegan por garantia tanto para combanc y comder.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLLU_NOMNA
   -- Fecha: 24/10/16
   -- Autor: Santander
   -- Input:
   --p_COD_TPO_GRNTIA -> Tipo de garantia
   --p_FEC_ISR_OPE -> Fecha de insercion de operacion origen
   --p_NUM_FOL_OPE  -> numero de folio operacion origen
   --p_FEC_VTA -> fecha de valuta
   --p_COD_DVI -> codigo divisa
   --p_IMp_OPE -> monto garantia
   --p_COD_BCO_DTN -> banco destino
   --p_COD_BCO_BFC -> banco beneficiario
   --p_NUM_FOL_MSJ -> numero de folio mensaje nuevo
   --p_FEC_ISR_MSJ -> fecha de insercion mensaje nuevo
   --p_NUM_REF_SWF -> referencia swift
   --p_NUM_CTA_CTE -> numero de cuenta corriente COMDER
   --p_GLS_TPO_LLM_MRG -> glosa de llamado a margen
   --P_FEC_LLM_MRG -> Fecha llamado a margen
   --P_FEC_RCP_SWF -> fecha de recepcion del swift
   --p_HOR_RCP_SWF -> hora de recepcion de swift
   --P_COD_BCO_ORD  -> banco ordenante
   -- Output:
   -- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- p_o_NUM_FOL_OPE --> Retorno de folio de operacion insertada
   -- p_o_FEC_ISR_OPE --> fecha insercion de la operacion
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_INS_GRNTIA_CMDR_CMBNC (
      p_COD_TPO_GRNTIA    IN     NUMBER,
      p_FEC_ISR_OPE       IN     VARCHAR2,
      p_NUM_FOL_OPE       IN     NUMBER,
      p_FEC_VTA           IN     VARCHAR2,
      p_COD_DVI           IN     CHAR,
      p_IMp_OPE           IN     NUMBER,
      p_COD_BCO_DTN       IN     CHAR,
      p_COD_BCO_BFC       IN     CHAR,
      --503--
      p_NUM_FOL_MSJ       IN     NUMBER,
      p_FEC_ISR_MSJ       IN     VARCHAR2,
      p_NUM_REF_SWF       IN     VARCHAR2,
      p_NUM_CTA_CTE       IN     VARCHAR2,
      p_GLS_TPO_LLM_MRG   IN     CHAR,
      P_FEC_LLM_MRG       IN     VARCHAR2,
      P_FEC_RCP_SWF       IN     VARCHAR2,
      p_HOR_RCP_SWF       IN     NUMBER,
      P_COD_BCO_ORD       IN     CHAR,
      p_ERROR                OUT NUMBER,
      ------------RETORNO DE NUMERO DE OPERACION
      p_o_NUM_FOL_OPE        OUT NUMBER,
      p_o_FEC_ISR_OPE        OUT DATE
   )
   IS
      --
      v_NOM_SP              VARCHAR2 (30) := 'Sp_PAB_INS_GRNTIA_CMDR_CMBNC';
      v_NUM_FOL_NMN         NUMBER (12) := 0;
      v_FLG_EGR_ING         NUMBER := 0;
      v_COD_EST_OPE_CMBNC   NUMBER := 20;
      v_COD_EST_OPE_CMDER   NUMBER := 20;  -- PARA PRUEBA. SE DEBE DEJAR EN 21
      v_COD_TPO_OPE_AOS     CHAR (8) := PKG_PAB_CONSTANTES.V_STR_TPO_OPE_CDLE;
      v_COD_TPO_OPE_CMD     CHAR (7);
      v_COD_MT_SWF_CMBNC    NUMBER := 202;
      v_COD_MT_SWF_CMDER    NUMBER := 103;
      v_COD_SIS_ENT CHAR (10)
            := PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SE_OOFF ;
      v_COD_SIS_SAL CHAR (10)
            := PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_LBTR ;
      v_COD_SUC             VARCHAR2 (4) := '0174';
      v_COD_USR             VARCHAR2 (11) := 'GRT_CMD';
      v_INS_OPE             NUMBER;
      v_HORA_FEC            VARCHAR2 (50);
      v_p_FEC_ISR_OPE       DATE;
      v_FEC_VTA             VARCHAR2 (15);
      v_p_FEC_ISR_MSJ       VARCHAR2 (15);
      v_P_FEC_LLM_MRG       VARCHAR2 (15);
      v_Glosa_COMBANC       VARCHAR2 (35) := 'GARANTIA COMBANC ALTOS MONTOS';
      v_Glosa_COMDER        VARCHAR2 (35) := 'GARANTIA COMDER ALTOS MONTOS';
      v_Nom_Banco           VARCHAR2 (15) := 'BANCO SANTANDER';
      v_p_FEC_ISR_MSJ_D     DATE;

      -- FOLIO NUEVO OPERACION
      v_NUM_FOL_OPE         NUMBER;
      v_NUM_FOL_MSJ         NUMBER;

      --VALIDACION BANCO DESTINO
      v_rut_bco_dtn         CHAR (11) := '97029001K';
      v_COD_BCO_DTN         CHAR (11);

      --VARIABLE PARA ACTUALIZAR SALDOS
      v_IMP_OER_503         NUMBER (15, 2);

      --VARIABLE DE EXISTENCIA DE REGISTRO
      v_EXISTE_GARANTIA     NUMBER (12) := 0;
   --
   BEGIN
      --Mientras no se ocupe la variable de entrada
      p_ERROR := p_NUM_FOL_MSJ;


      v_FEC_VTA := TO_DATE (p_FEC_VTA, 'DD-MM-RR');
      p_ERROR := PKG_PAB_CONSTANTES.v_OK;

      BEGIN
         SELECT   TGCDSWSA
           INTO   v_COD_BCO_DTN
           FROM   TCDT040
          WHERE   TCNIFENT = v_rut_bco_dtn;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            err_code := SQLCODE;
            err_msg :=
               'No se han encontrado registos asociados a la busqueda por rut :'
               || v_rut_bco_dtn;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_s_mensaje := err_code || '-' || err_msg;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;

      IF p_COD_TPO_GRNTIA = 1
      THEN                                    -- 1 = 'COMBANC'  / 2 = 'COMDER'
         -- se agrega logica de validacion para saber si el registro ya existe.

         BEGIN
            SELECT   NUM_FOL_OPE
              INTO   v_EXISTE_GARANTIA
              FROM   PABS_DT_DETLL_OPRCN
             WHERE   FEC_VTA = v_FEC_VTA
                     AND COD_TPO_OPE_AOS =
                           PKG_PAB_CONSTANTES.V_STR_TPO_OPE_CDLE
                     AND IMP_OPE = p_IMp_OPE
                     AND COD_BCO_DTN = v_COD_BCO_DTN;

            err_code := 00;
            err_msg :=
                  'Garantia para Dia: '
               || v_FEC_VTA
               || ' ya se encuentra registrada. no se almacenará';
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               v_NUM_FOL_OPE := SPK_PAB_PAG_EAM.NEXTVAL;   -- OBTIENE EL FOLIO
               p_o_NUM_FOL_OPE := v_NUM_FOL_OPE;        -- RETORNO EL VALOR...

               PKG_PAB_OPERACION.Sp_PAB_INS_OPE (
                  SYSDATE,                                          --NOT NULL
                  v_NUM_FOL_OPE,                                    --NOT NULL
                  v_NUM_FOL_NMN,           -- NUM_FOL_NMN        ,  --NOT NULL
                  0,                                         --NUM_FOL_OPE_ORG
                  0,                                         --NUM_OPE_SIS_ENT
                  v_FLG_EGR_ING, -- p_FLG_EGR_ING ,  --SI ES UN 103 MARCAR COMO EGRESO  1 VARIABLE GLOBAL
                  PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_LBTR, -- p_COD_SIS_SAL      ,
                  PKG_PAB_CONSTANTES.V_COD_TIP_LBTRCOMB,                -- GCP
                  v_COD_EST_OPE_CMBNC, -- p_COD_EST_OPE       ,  -- VARIABLE GLOBAL
                  PKG_PAB_CONSTANTES.V_COD_TIP_ING_DIR, --v_COD_TPO_ING ,     -- p_COD_TPO_ING      ,  --
                  PKG_PAB_CONSTANTES.V_COD_MT202, -- p_COD_MT_SWF ,  --VARIABLE GLOBAL 202
                  PKG_PAB_CONSTANTES.V_FLG_MRD_MN,     -- p_FLG_MRD_UTZ_SWF  ,
                  v_COD_TPO_OPE_AOS, -- p_COD_TPO_OPE_AOS  ,  -- CDLE VARIABLE GLOBAL PARA EL TIPO DE OPERACION
                  v_FEC_VTA,                        --v_FEC_VTA ,  -- NOT NULL
                  p_COD_DVI,                                       -- NOT NULL
                  p_IMp_OPE,                                       -- NOT NULL
                  v_COD_BCO_DTN,                                   -- NOT NULL
                  p_COD_BCO_BFC,                                   -- NOT NULL
                  NULL,
                  NULL,
                  NULL,
                  NULL,
                  ----------Beneficiario------------
                  NULL,
                  NULL,
                  PKG_PAB_CONSTANTES.V_NOM_BCO_CTR, --p_NOM_BFC          ,     --LLENAR BUSCANDO EN LA TABLA CORPORATIVA EL NOMBRE
                  NULL,                                -- p_NUM_CTA_BFC      ,
                  -------------------------------------
                  v_COD_SUC,                  --NOT NULL  variable global 0174
                  NULL,                                -- p_GLS_ADC_EST      ,
                  NULL,                                -- p_NUM_REF_CTB      ,
                  ----------Opcionales--------------
                  NULL,
                  NULL,                                -- p_COD_BCO_ITD      ,
                  NULL,                                -- p_NUM_CTA_DCv_BFC  ,
                  NULL,                                -- p_GLS_DIR_BFC      ,
                  NULL,                                -- p_NOM_CDD_BFC      ,
                  NULL,                                -- p_COD_PAS_BFC      ,
                  NULL,                                -- p_NOM_ODN          ,
                  NULL,                                -- p_NUM_DOC_ODN      ,
                  NULL,                                -- p_COD_TPD_ODN      ,
                  NULL,                                -- p_NUM_CTA_ODN      ,
                  NULL,                                -- p_NUM_CTA_DCv_ODN  ,
                  NULL,                                -- p_GLS_DIR_ODN      ,
                  NULL,                                -- p_NOM_CDD_ODN      ,
                  NULL,                                -- p_COD_PAS_ODN      ,
                  NULL,                                -- p_NUM_CLv_NGC      ,
                  NULL,                                -- p_NUM_AGT          ,
                  NULL,                                -- p_COD_FND_CCLV     ,
                  NULL,                                -- p_COD_TPO_SDO      ,
                  NULL,                                -- p_COD_TPO_CMA      ,
                  NULL,                                -- p_COD_TPO_FND      ,
                  NULL,                                -- p_FEC_TPO_OPE_CCL  ,
                  NULL,                                -- p_NUM_CLv_IDF      ,
                  v_Glosa_COMBANC,                     -- p_OBS_OPC_SWF      ,
                  NULL,
                  NULL,
                  --------------------------------
                  v_COD_USR,
                  p_ERROR
               );
            WHEN OTHERS
            THEN
               err_code := SQLCODE;
               err_msg := SUBSTR (SQLERRM, 1, 300);
               p_s_mensaje := err_code || '-' || err_msg;
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
               RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
         END;
      END IF;

      IF p_COD_TPO_GRNTIA = 2
      THEN
         IF p_GLS_TPO_LLM_MRG = 'G'
         THEN
            v_COD_TPO_OPE_CMD := PKG_PAB_CONSTANTES.V_COMDERG;
         ELSE
            v_COD_TPO_OPE_CMD := PKG_PAB_CONSTANTES.V_COMDERM;
         END IF;

         v_p_FEC_ISR_MSJ := TO_DATE (p_FEC_ISR_MSJ, 'DD-MM-RRRR HH24:MI:SS');
         v_p_FEC_ISR_OPE := SYSDATE;
         v_P_FEC_LLM_MRG := TO_DATE (P_FEC_LLM_MRG, 'DD-MM-RRRR');

         v_NUM_FOL_OPE := SPK_PAB_PAG_EAM.NEXTVAL;         -- OBTIENE EL FOLIO

         p_o_NUM_FOL_OPE := v_NUM_FOL_OPE;              -- RETORNO EL VALOR...
         --p_o_FEC_ISR_OPE := to_date(v_p_FEC_ISR_OPE, 'DD-MM-YYYY');-- Retorna fecha
         p_o_FEC_ISR_OPE := v_p_FEC_ISR_OPE;                  -- Retorna fecha

         PKG_PAB_OPERACION.Sp_PAB_INS_OPE (
            v_p_FEC_ISR_OPE,
            v_NUM_FOL_OPE,
            v_NUM_FOL_NMN,
            0,
            0,
            v_FLG_EGR_ING,
            PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_LBTR,
            PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SE_OOFF,
            v_COD_EST_OPE_CMDER,
            PKG_PAB_CONSTANTES.V_COD_TIP_ING_DIR,
            PKG_PAB_CONSTANTES.V_COD_MT103,
            PKG_PAB_CONSTANTES.V_FLG_MRD_MN,
            v_COD_TPO_OPE_CMD,
            v_FEC_VTA,
            p_COD_DVI,
            p_IMp_OPE,
            p_COD_BCO_DTN,
            p_COD_BCO_BFC,
            NULL,
            NULL,
            NULL,
            NULL,
            -- ----------Beneficiario------------
            PKG_PAB_CONSTANTES.V_NUM_DOC_COMDER,
            PKG_PAB_CONSTANTES.V_NUM_VER_COMDER,
            PKG_PAB_CONSTANTES.V_NOM_CMD,
            p_NUM_CTA_CTE,
            -- -------------------------------------
            v_COD_SUC,
            NULL,
            NULL,
            -- ----------Opcionales--------------
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            v_Nom_Banco,
            PKG_PAB_CONSTANTES.V_NUM_DOC_BANCO_SANTANDER,
            PKG_PAB_CONSTANTES.V_COD_TPD_BANCO_SANTANDER,
            PKG_PAB_CONSTANTES.V_CTA_BANCO_SANTANDER,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            v_Glosa_COMDER,
            NULL,
            NULL,
            -- --------------------------------
            v_COD_USR,
            p_ERROR
         );
      END IF;

      IF p_COD_TPO_GRNTIA = 0
      THEN
         v_p_FEC_ISR_MSJ_D := TO_DATE (p_FEC_ISR_OPE, 'DD-MM-RRRR HH24:MI:SS');
         v_p_FEC_ISR_OPE := TO_DATE (v_p_FEC_ISR_MSJ, 'DD-MM-RRRR HH24:MI:SS');
         v_P_FEC_LLM_MRG := TO_DATE (P_FEC_LLM_MRG, 'DD-MM-RRRR HH24:MI:SS');

         v_NUM_FOL_MSJ := SPK_PAB_MSJ_EAM.NEXTVAL;         -- OBTIENE EL FOLIO

         p_o_NUM_FOL_OPE := v_NUM_FOL_MSJ;              -- RETORNO EL VALOR...

         PKG_PAB_GARANTIA.SP_PAB_INS_MT503_COMDER (v_NUM_FOL_MSJ,
                                                   SYSDATE --v_p_FEC_ISR_MSJ_D
                                                          ,
                                                   P_NUM_REF_SWF,
                                                   P_COD_DVI,
                                                   P_IMP_OPE,
                                                   v_FEC_VTA,
                                                   P_NUM_CTA_CTE,
                                                   p_GLS_TPO_LLM_MRG,
                                                   v_P_FEC_LLM_MRG,
                                                   P_FEC_RCP_SWF,
                                                   p_HOR_RCP_SWF,
                                                   P_COD_BCO_BFC,
                                                   P_COD_BCO_ORD,
                                                   p_NUM_FOL_OPE,
                                                   v_p_FEC_ISR_MSJ_D,
                                                   P_ERROR);


         --Actualización de tabla de saldos MN con monto de la garantia Comder MT503
         BEGIN
            --Buscamos saldo actual de un 503 comder
            SELECT   SN.IMP_OER_503
              INTO   v_IMP_OER_503
              FROM   PABS_DT_DETLL_SALDO_CAJA_MN_MX SN
             WHERE   SN.COD_BCO_ORG = PKG_PAB_CONSTANTES.V_BIC_BCCH
                     AND COD_DVI = PKG_PAB_CONSTANTES.V_STR_DVI_CLP;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               err_code := SQLCODE;
               err_msg :=
                     'No existe registro para el banco: '
                  || PKG_PAB_CONSTANTES.V_BIC_BCCH
                  || ' del día';
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
               RAISE;
            WHEN OTHERS
            THEN
               err_code := SQLCODE;
               err_msg := SUBSTR (SQLERRM, 1, 300);
               p_s_mensaje := err_code || '-' || err_msg;
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
               RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
         END;

         --Actualizamos la tabla de saldos
         UPDATE   PABS_DT_DETLL_SALDO_CAJA_MN_MX SC
            SET   SC.IMP_OER_503 = P_IMP_OPE + v_IMP_OER_503
          WHERE   COD_BCO_ORG = PKG_PAB_CONSTANTES.V_BIC_BCCH
                  AND COD_DVI = PKG_PAB_CONSTANTES.V_STR_DVI_CLP;
      END IF;


      COMMIT;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         ROLLBACK;
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         ROLLBACK;
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   --
   END Sp_PAB_INS_GRNTIA_CMDR_CMBNC;


   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUS_COMBANC
   -- Objetivo: Busca las operaciones disponibles para pago por garantia combanc.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA
   -- Fecha: 24/10/16
   -- Autor: Santander
   -- Input:
   -- p_FEC_ISR_OPE -> Fecha de busqueda de operaciones (fecha de insercion al sistema)
   -- Output:
   -- p_Cursor -> Cursor con datos de operaciones
   -- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_BUS_COMBANC (p_FEC_ISR_OPE   IN     DATE,
                                 p_CURSOR           OUT SYS_REFCURSOR,
                                 P_ERROR            OUT NUMBER)
   IS
      v_NOM_SP   VARCHAR2 (30) := 'Sp_PAB_BUS_COMBANC';
      v_dia      DATE := TRUNC (SYSDATE);
   BEGIN
      --



      OPEN p_CURSOR FOR
           SELECT   OPE.COD_MT_SWF AS COD_MT_SWF,
                    OPE.NUM_REF_SWF AS NUM_REF_SWF,                         --
                    OPE.NUM_FOL_OPE AS NUM_FOL_OPE,                         --
                    OPE.COD_TPO_OPE_AOS AS COD_TPO_OPE_AOS, -- CDLE     .tipo operacion -- depende de la tabla TIPO OPERACION
                    EST.DSC_EST_AOS AS DSC_EST_AOS,
                    OPE.IMP_OPE AS IMP_OPE,                                 --
                    OPE.COD_DVI AS COD_DVI,                                 --
                    OPE.FEC_VTA AS FEC_VTA,                                 --
                    OPE.COD_BCO_DTN AS COD_BCO_DTN,                         --
                    TI.DES_BCO AS NOM_BCO_DTN,
                    OPEOP.COD_BCO_BFC AS COD_BCO_BFC, -- tabla operaciones opcional
                    OPE.HOR_ENV_RCP_SWF AS HOR_ENV_RCP_SWF,
                    OPE.COD_SIS_ENT AS SISTEMA_ENTRADA,
                    OPE.FEC_ISR_OPE AS FECH_ISR_OPE,
                    EST.COD_EST_AOS AS COD_ESTADO
             FROM               PABS_DT_DETLL_OPRCN OPE
                             LEFT JOIN
                                PABS_DT_OPRCN_INFCN_OPCON OPEOP
                             ON OPE.NUM_FOL_OPE = OPEOP.NUM_FOL_OPE
                                AND OPE.FEC_ISR_OPE = OPEOP.FEC_ISR_OPE
                          INNER JOIN
                             PABS_DT_TIPO_OPRCN_ALMOT TOPE
                          ON TOPE.COD_TPO_OPE_AOS = OPE.COD_TPO_OPE_AOS
                       INNER JOIN
                          PABS_DT_ESTDO_ALMOT EST
                       ON OPE.COD_EST_AOS = EST.COD_EST_AOS
                    INNER JOIN
                       TCDTBAI TI
                    ON OPE.COD_BCO_DTN = TI.TGCDSWSA
            WHERE   TOPE.COD_TPO_OPE_AOS =
                       PKG_PAB_CONSTANTES.V_STR_TPO_OPE_CDLE         -- 'CDLE'
                    AND OPE.COD_EST_AOS =
                          PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEINGAR
                    AND OPE.COD_MT_SWF = PKG_PAB_CONSTANTES.V_COD_MT202
                    AND OPE.FEC_VTA >= v_dia
         ORDER BY   OPE.FEC_VTA;

      --
      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   --
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg :=
            'No se han encontrado registos asociados a la busqueda por fecha de valuta :'
            || p_FEC_ISR_OPE;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   --
   END Sp_PAB_BUS_COMBANC;

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUS_DETLL_COMBANC
   -- Objetivo: Busca el detalle de las operaciones disponibles para pago por garantia COMBANC.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA
   -- Fecha: 24/10/16
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_OPE -> Numero de folio de operacion a buscar
   -- p_FEC_ISR_OPE -> fecha insercion de operacion a buscar.
   -- Output:
   -- p_CURSOR-> Cursor de operaciones encontradas
   -- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_BUS_DETLL_COMBANC (p_NUM_FOL_OPE   IN     NUMBER,
                                       p_FEC_ISR_OPE   IN     DATE,
                                       p_CURSOR           OUT SYS_REFCURSOR,
                                       P_ERROR            OUT NUMBER)
   IS
      --
      v_NOM_SP    VARCHAR2 (30) := 'Sp_PAB_BUS_DETLL_COMBANC';
      v_NOM_ORG   VARCHAR2 (21) := 'BANCO SANTANDER CHILE';
   BEGIN
      OPEN p_CURSOR FOR
         SELECT   OPE.NUM_FOL_OPE AS NUM_FOL_OPE,                           --
                  OPE.COD_TPO_OPE_AOS AS COD_TPO_OPE_AOS, -- CDLE     .tipo operacion -- depende de la tabla TIPO OPERACION
                  OPE.COD_EST_AOS AS COD_EST_AOS,
                  OPE.IMP_OPE AS IMP_OPE,                                   --
                  OPE.COD_DVI AS COD_DVI,                                   --
                  OPE.FEC_VTA AS FEC_VTA,                                   --
                  OPE.COD_BCO_DTN AS COD_BCO_DTN,
                  OPE.NOM_BFC AS NOM_DTN,                                   --
                  OPE.COD_BCO_ORG AS COD_BCO_ORG,
                  v_NOM_ORG AS NOM_ORG,          -- tabla operaciones opcional
                  OPE.FEC_ISR_OPE AS FEC_ISR_OPE
           FROM   PABS_DT_DETLL_OPRCN OPE, PABS_DT_OPRCN_INFCN_OPCON OPEOP
          WHERE       OPE.NUM_FOL_OPE = p_NUM_FOL_OPE
                  AND OPE.FEC_ISR_OPE = p_FEC_ISR_OPE
                  AND OPE.NUM_FOL_OPE = OPEOP.NUM_FOL_OPE
                  AND OPE.FEC_ISR_OPE = OPEOP.FEC_ISR_OPE;

      --
      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   --
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg :=
               'No existe registro para el folio: '
            || p_NUM_FOL_OPE
            || ' y fecha '
            || p_FEC_ISR_OPE;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   --
   END Sp_PAB_BUS_DETLL_COMBANC;

   --
   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUS_COMDER
   -- Objetivo: Busca las operaciones disponibles para pago por garantia.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA
   -- Fecha: 24/10/16
   -- Autor: Santander
   -- input : n/a
   -- Output:
   -- p_CURSOR-> Cursor de operaciones encontradas
   -- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_BUS_COMDER (p_CURSOR   OUT SYS_REFCURSOR,
                                P_ERROR    OUT NUMBER)
   IS
      --
      v_NOM_SP   VARCHAR2 (30) := 'Sp_PAB_BUS_COMDER';
      v_dia      DATE := TRUNC (SYSDATE);
   --
   BEGIN
      OPEN p_CURSOR FOR
         SELECT   MT.NUM_FOL_MSJ_503 AS NUM_FOL_MSJ_503,
                  MT.FEC_ISR_MSJ_503 AS FEC_ISR_MSJ_503,
                  MT.NUM_FOL_OPE AS NUM_FOL_OPE,
                  MT.FEC_ISR_OPE AS FEC_ISR_OPE,
                  MT.NUM_REF_SWF AS NUM_REF_SWF,
                  EST.DSC_EST_AOS AS COD_EST_AOS,
                  MT.COD_DVI AS COD_DVI,
                  MT.IMP_OPE_MSJ_503 AS IMP_OPE_MSJ_503,
                  MT.FEC_VTA AS FEC_VTA,
                  MT.NUM_CTA_CTE AS NUM_CTA_CTE,
                  MT.GLS_TPO_LLM_MRG AS GLS_TPO_LLM_MRG,
                  MT.FEC_LLM_MRG AS FEC_LLM_MRG,
                  MT.FEC_RCP_SWF AS FEC_RCP_SWF,
                  MT.HOR_RCP_SWF AS HOR_RCP_SWF,
                  MT.COD_BCO_BFC AS COD_BCO_BFC,
                  MT.COD_BCO_ORD AS COD_BCO_ORD,
                  OPE.NUM_FOL_NMN AS NUM_FOL_NMN,
                  OPE.NOM_BFC AS NOM_BFC,
                  OPE.NUM_CTA_BFC AS NUM_CTA_BFC,
                  OPE.NUM_DOC_BFC AS NUM_DOC_BFC,
                  OPE.COD_MT_SWF AS COD_MT_SWF,
                  OPEOP.NOM_ODN AS NOM_ODN,
                  OPEOP.NUM_CTA_ODN AS NUM_CTA_ODN,
                  OPEOP.NUM_DOC_ODN AS NUM_DOC_ODN,
                  EST.COD_EST_AOS AS COD_ESTADO
           FROM   PABS_DT_MNSJE_503 MT,
                  PABS_DT_DETLL_OPRCN OPE,
                  PABS_DT_OPRCN_INFCN_OPCON OPEOP,
                  PABS_DT_ESTDO_ALMOT EST
          WHERE       OPE.NUM_FOL_OPE = MT.NUM_FOL_OPE
                  AND OPE.FEC_ISR_OPE = MT.FEC_ISR_OPE
                  AND OPEOP.NUM_FOL_OPE = MT.NUM_FOL_OPE
                  AND OPEOP.FEC_ISR_OPE = MT.FEC_ISR_OPE
                  AND OPE.FEC_VTA >= v_dia
                  AND OPE.COD_EST_AOS =
                        PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEINGAR
                  AND OPE.COD_EST_AOS = EST.COD_EST_AOS;

      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   --
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := 'No existe registro para el dia: ' || v_dia;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   --
   END Sp_PAB_BUS_COMDER;

   --
   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_INS_MT503_COMDER
   -- Objetivo: Recibe informacion desde tibco para el ingreso de una garantia a pagar
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA
   -- Fecha: 24/10/16
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_OPE -> Numero de folio operacion origen
   -- p_FEC_ISR_OPE -> fecha insercion operacion origen
   -- p_NUM_REF_SWF -> numero de refencia swift
   -- p_COD_DVI    ->  codigo divisa
   -- p_IMP_OPE_MSJ_503  ->  monto informado mensaje 503
   -- p_FEC_VTA        -> fecha de valuta
   -- p_NUM_CTA_CTE    ->  cuenta corriente
   -- p_GLS_TPO_LLM_MRG ->  glosa tipo de llamado a margen
   -- p_FEC_LLM_MRG    ->  fecha llamado a mergen
   -- p_FEC_RCP_SWF    ->  fecha recepcion swift
   -- p_HOR_RCP_SWF    ->  hora recepcion swift
   -- p_COD_BCO_BFC    -> banco beneficiario
   -- p_COD_BCO_ORD    -> banco ordenante
   -- p_NUM_FOL_MT503  -> numero de folio mensaje 503
   -- p_FEC_ISR_MSJ_503 ->  fecha insercion mensaje 503
   -- Output:
   -- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_INS_MT503_COMDER (p_NUM_FOL_OPE       IN     NUMBER,
                                      p_FEC_ISR_OPE       IN     DATE,
                                      p_NUM_REF_SWF       IN     VARCHAR2,
                                      p_COD_DVI           IN     CHAR,
                                      p_IMP_OPE_MSJ_503   IN     NUMBER,
                                      p_FEC_VTA           IN     DATE,
                                      p_NUM_CTA_CTE       IN     VARCHAR2,
                                      p_GLS_TPO_LLM_MRG   IN     CHAR,
                                      p_FEC_LLM_MRG       IN     DATE,
                                      p_FEC_RCP_SWF       IN     DATE,
                                      p_HOR_RCP_SWF       IN     NUMBER,
                                      p_COD_BCO_BFC       IN     CHAR,
                                      p_COD_BCO_ORD       IN     CHAR,
                                      p_NUM_FOL_MT503     IN     NUMBER,
                                      p_FEC_ISR_MSJ_503   IN     DATE,
                                      P_ERROR                OUT NUMBER)
   IS
      v_NOM_SP            VARCHAR2 (30) := 'Sp_PAB_INS_MT503_COMDER';

      v_FEC_ISR_OPE       DATE := p_FEC_ISR_OPE;
      v_FEC_ISR_MSJ_503   DATE := p_FEC_ISR_MSJ_503;
   BEGIN
      --

      --
      INSERT INTO PABS_DT_MNSJE_503 (NUM_FOL_MSJ_503,
                                     FEC_ISR_MSJ_503,
                                     NUM_FOL_OPE,
                                     FEC_ISR_OPE,
                                     NUM_REF_SWF,
                                     COD_DVI,
                                     IMP_OPE_MSJ_503,
                                     FEC_VTA,
                                     NUM_CTA_CTE,
                                     GLS_TPO_LLM_MRG,
                                     FEC_LLM_MRG,
                                     FEC_RCP_SWF,
                                     HOR_RCP_SWF,
                                     COD_BCO_BFC,
                                     COD_BCO_ORD)
        VALUES   (p_NUM_FOL_OPE,
                  v_FEC_ISR_OPE,
                  p_NUM_FOL_MT503,
                  v_FEC_ISR_MSJ_503,
                  p_NUM_REF_SWF,
                  p_COD_DVI,
                  p_IMP_OPE_MSJ_503,
                  p_FEC_VTA,
                  p_NUM_CTA_CTE,
                  p_GLS_TPO_LLM_MRG,
                  p_FEC_LLM_MRG,
                  p_FEC_RCP_SWF,
                  p_HOR_RCP_SWF,
                  p_COD_BCO_BFC,
                  p_COD_BCO_ORD);

      --
      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   --
   EXCEPTION
      WHEN DUP_VAL_ON_INDEX
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   --
   END Sp_PAB_INS_MT503_COMDER;

   --
   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUS_MT503
   -- Objetivo: Busca los MT503 relacionadas a operaciones pagadas por garantia.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA
   -- Fecha: 24/10/16
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_OPE -> Numero de folio de operacion a buscar
   -- p_FEC_ISR_OPE -> fecha insercion de operacion a buscar.
   -- Output:
   -- p_CURSOR-> Cursor de operaciones encontradas
   -- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_BUS_MT503 (p_NUM_FOL_OPE   IN     NUMBER,
                               p_FEC_ISR_OPE   IN     DATE,
                               p_CURSOR           OUT SYS_REFCURSOR,
                               P_ERROR            OUT NUMBER)
   IS
      --
      v_NOM_SP   VARCHAR2 (30) := 'Sp_PAB_BUS_MT503';
   --
   BEGIN
      --
      OPEN p_CURSOR FOR
         --
         SELECT   NUM_FOL_MSJ_503 AS NUM_FOL_MSJ_503,
                  FEC_ISR_MSJ_503 AS FEC_ISR_MSJ_503,
                  NUM_FOL_OPE AS NUM_FOL_OPE,
                  FEC_ISR_OPE AS FEC_ISR_OPE,
                  NUM_REF_SWF AS NUM_REF_SWF,
                  COD_DVI AS COD_DVI,
                  IMP_OPE_MSJ_503 AS IMP_OPE_MSJ_503,
                  FEC_VTA AS FEC_VTA,
                  NUM_CTA_CTE AS NUM_CTA_CTE,
                  GLS_TPO_LLM_MRG AS GLS_TPO_LLM_MRG,
                  FEC_LLM_MRG AS FEC_LLM_MRG,
                  FEC_RCP_SWF AS FEC_RCP_SWF,
                  HOR_RCP_SWF AS HOR_RCP_SWF,
                  COD_BCO_BFC AS COD_BCO_BFC,
                  COD_BCO_ORD AS COD_BCO_ORD
           FROM   PABS_DT_MNSJE_503
          WHERE   NUM_FOL_OPE = p_NUM_FOL_OPE AND FEC_ISR_OPE = p_FEC_ISR_OPE;

      --
      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   --
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg :=
               'No existe registro para el numero de operacion: '
            || p_NUM_FOL_OPE
            || ' y fecha : '
            || p_FEC_ISR_OPE;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   --
   END Sp_PAB_BUS_MT503;

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUS_DETLL_COMDER
   -- Objetivo: Busca el detalle de las operaciones disponibles para pago por garantia.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA
   -- Fecha: 24/10/16
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_OPE -> Numero de folio de operacion a buscar
   -- p_FEC_ISR_OPE -> fecha insercion de operacion a buscar.
   -- Output:
   -- p_CURSOR-> Cursor de operaciones encontradas
   -- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_BUS_DETLL_COMDER (p_NUM_FOL_OPE   IN     NUMBER,
                                      p_FEC_ISR_OPE   IN     DATE,
                                      p_CURSOR           OUT SYS_REFCURSOR,
                                      P_ERROR            OUT NUMBER)
   IS
      --
      v_NOM_SP   VARCHAR2 (30) := 'Sp_PAB_BUS_DETLL_COMDER';
   --
   BEGIN
      --
      OPEN p_CURSOR FOR
         SELECT   OPE.NOM_BFC AS NOM_BFC,
                  OPE.NUM_CTA_BFC AS NUM_CTA_BFC,
                  OPE.NUM_DOC_BFC AS NUM_DOC_BFC,
                  OPE.COD_MT_SWF AS COD_MT_SWF,
                  OPE.NUM_FOL_OPE AS NUM_FOL_OPE,
                  OPE.FEC_ISR_OPE AS FEC_ISR_OPE,
                  OPEOP.NOM_ODN AS NOM_ODN,
                  OPEOP.NUM_CTA_ODN AS NUM_CTA_ODN,
                  OPEOP.NUM_DOC_ODN AS NUM_DOC_ODN,
                  OPE.COD_BCO_DTN AS COD_BCO_BFC,
                  MT.COD_DVI AS COD_DVI,
                  MT.FEC_LLM_MRG AS FEC_LLM_MRG,
                  MT.FEC_VTA AS FEC_VTA,
                  MT.IMP_OPE_MSJ_503 AS IMP_OPE_MSJ_503
           FROM         PABS_DT_DETLL_OPRCN OPE
                     LEFT JOIN
                        PABS_DT_OPRCN_INFCN_OPCON OPEOP
                     ON OPEOP.NUM_FOL_OPE = OPE.NUM_FOL_OPE
                        AND OPEOP.FEC_ISR_OPE = OPE.FEC_ISR_OPE
                  LEFT JOIN
                     PABS_DT_MNSJE_503 MT
                  ON OPE.NUM_FOL_OPE = MT.NUM_FOL_OPE
                     AND OPE.FEC_ISR_OPE = MT.FEC_ISR_OPE
          WHERE   OPE.NUM_FOL_OPE = p_NUM_FOL_OPE
                  AND OPE.FEC_ISR_OPE = p_FEC_ISR_OPE;

      --
      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   --
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg :=
               'No existe registro para el numero de operacion: '
            || p_NUM_FOL_OPE
            || ' y fecha : '
            || p_FEC_ISR_OPE;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   --
   END Sp_PAB_BUS_DETLL_COMDER;

   --

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUS_DIA_HAB
   -- Objetivo: Procedimiento que realiza la busqueda de los dias habilies
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas:
   -- Fecha: 24/10/16
   -- Autor: Santander
   -- Input
   -- p_DIA_VAL -> Fecha a validar
   -- P_COD_PAIS -> Codigo pais del calendaro
   -- Output:
   -- P_IND_HABIL -> Indicaro de dia habil
   -- P_ERRORCODE -> codigo error de PL CHICON
   -- P_ERRORMSG  -> mensaje de error CHICON
   -- P_FEC_VAL   ->  fecha de valura
   -- P_VAL_FEC  ->  valor de la fecha
   -- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_BUS_DIA_HAB (p_DIA_VAL     IN     NUMBER,
                                 P_COD_PAIS    IN     CHAR,
                                 P_IND_HABIL      OUT NUMBER,
                                 P_ERRORCODE      OUT NUMBER,
                                 P_ERRORMSG       OUT VARCHAR2,
                                 P_FEC_VAL        OUT VARCHAR2,
                                 P_VAL_FEC        OUT NUMBER,
                                 P_ERROR          OUT NUMBER)
   IS
      --

      v_NOM_SP        VARCHAR2 (30) := 'Sp_PAB_BUS_DIA_HAB';
      v_FEC_ISR_OPE   DATE;
   --

   BEGIN
      v_FEC_ISR_OPE := TO_DATE (SYSDATE + p_DIA_VAL, 'DD-MM-RRRR');
      P_VAL_FEC := TO_CHAR (v_FEC_ISR_OPE, 'D', 'NLS_DATE_LANGUAGE=SPANISH');
      PKG_CHI_CON_AOS.SP_CHI_CON_DHB_AOS (v_FEC_ISR_OPE,
                                          P_COD_PAIS,
                                          P_IND_HABIL,
                                          P_ERRORCODE,
                                          P_ERRORMSG);
      P_FEC_VAL := TO_CHAR (TRUNC (SYSDATE + p_DIA_VAL), 'DD-MM-RRRR');

      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
      COMMIT;
   --
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg :=
               'No existe registro para el dia: '
            || p_DIA_VAL
            || ', codigo pais : '
            || P_COD_PAIS;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   --
   END Sp_PAB_BUS_DIA_HAB;

   --
   -- ***********************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_BUS_IMP_GAR
   -- Objetivo: Funcion que retorna valores de divisas sus montos maximos por moneda.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_MONDA_ALMOT
   -- Fecha: 24/10/16
   -- Autor: Santander
   -- Input:
   -- P_COD_DVI : Codigo de divisa
   -- Output:
   -- Input/Output: N/A
   -- Retorno: MONTO de garantoa
   -- Observaciones: N/A.
   -- ***********************************************************************************************
   FUNCTION FN_PAB_BUS_IMP_GAR (P_COD_DVI IN CHAR)
      RETURN NUMBER
   IS
      V_NOM_SP            VARCHAR2 (20) := 'FN_PAB_BUS_IMP_GAR';
      V_IMP_MAX_SIN_ATZ   NUMBER (18, 2);
   BEGIN
      BEGIN
         SELECT   IMP_GRT_BCS
           INTO   V_IMP_MAX_SIN_ATZ
           FROM   PABS_DT_TIPO_OPRCN_ALMOT
          WHERE   COD_TPO_OPE_AOS = P_COD_DVI;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            V_IMP_MAX_SIN_ATZ := 0;
      END;

      RETURN V_IMP_MAX_SIN_ATZ;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := 'No existe registro para la divisa ' || P_COD_DVI;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         RETURN V_IMP_MAX_SIN_ATZ;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END FN_PAB_BUS_IMP_GAR;
--

END PKG_PAB_GARANTIA;