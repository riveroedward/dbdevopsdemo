CREATE OR REPLACE PACKAGE BODY         PKG_PAB_NOTIFICACION
AS
   /******************************************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUS_NTFCC_ALMOT
   -- Objetivo: Procedimiento almacenado que retorna lista de notificaciones del dashboard y la cantidad de notificiones por leer
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_NTFCC_ALMOT
   --
   -- Fecha: 04/07/17
   -- Autor: Santander CAH
   -- Input:
   --   p_COD_SIS_ENT       Cod. sistema entrada eje:OOFF
   --   p_COD_TPO_ROL_AOS   Rol usuario ingresado
   -- Output:
   --       p_CURSOR        Notificaciones del día
   --       p_CANT_NTF     Cantidad de operaciones sin leer
   --       p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --******************************************************************************************************************************/
   PROCEDURE Sp_PAB_BUS_NTFCC_ALMOT (
      p_COD_SIS_ENT       IN     CHAR,
      p_COD_TPO_ROL_AOS   IN     NUMBER,
      p_CURSOR               OUT SYS_REFCURSOR,
      p_CANT_NTF             OUT NUMBER,
      p_ERROR                OUT NUMBER
   )
   IS
      v_NOM_SP            VARCHAR2 (30) := 'Sp_PAB_BUS_NTFCC_ALMOT';
      v_FECHA_ACTUAL      DATE := TRUNC (SYSDATE);
      v_COD_TPO_ROL_AOS   NUMBER;
      v_CANT_PVI          NUMBER;
      V_COD_SIS_ENT       CHAR (10);
      V_COD_SIS_ENT_ESP   CHAR (10);
   BEGIN
      --Canal de Entrada
      V_COD_SIS_ENT := p_COD_SIS_ENT;

      --Tecnologia (Administrador) debe ver todas las notificaciones
      IF (V_COD_SIS_ENT <> PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SE_TECNO
          AND p_COD_TPO_ROL_AOS <> PKG_PAB_CONSTANTES.V_COD_TIP_ROL_ADM)
      THEN
         V_COD_SIS_ENT_ESP := p_COD_SIS_ENT;
      END IF;

      BEGIN
         OPEN p_CURSOR FOR
              SELECT   TO_CHAR (HORA, 'HH24:MI:SS') AS HORA,
                       HORA AS HORA_ORDEN,
                       DESCRIPCION,
                       ESTADO,
                       FECHA_NOT,
                       TIPO
                FROM   (--Todos
                        SELECT   FEC_ING_NTF AS HORA,
                                 SUBSTR (' -  ' || GLS_MTV_NTF, 1, 118)
                                    AS DESCRIPCION,
                                 DECODE (NT.FLG_VGN_NTF,
                                         0, 'NO LEIDA',
                                         'LEIDA')
                                    AS ESTADO,
                                 NT.FEC_ING_NTF AS FECHA_NOT, --Fecha ingreso notificación
                                 NT.COD_TIP_CRO AS TIPO
                          FROM   PABS_DT_NTFCC_ALMOT NT,
                                 PABS_DT_RELCN_CRREO_ROL RELCRREO,
                                 PABS_DT_DETLL_CRREO_ALMOT DETCRREO
                         WHERE   NT.COD_TIP_CRO = RELCRREO.COD_TIP_CRO
                                 AND RELCRREO.COD_SIS_ENT = V_COD_SIS_ENT
                                 AND RELCRREO.COD_TPO_ROL_AOS =
                                       p_COD_TPO_ROL_AOS
                                 AND DETCRREO.COD_TPO_CRO = NT.COD_TIP_CRO
                                 AND NT.COD_SIS_ENT IS NULL
                                 AND FEC_ING_NTF BETWEEN v_dia_ini
                                                     AND  v_dia_fin --Solo las del día
                                 AND DETCRREO.FLG_VGN_NTF =
                                       PKG_PAB_CONSTANTES.V_REG_VIG --Registro vigente
                                 AND RELCRREO.FLG_VGN =
                                       PKG_PAB_CONSTANTES.V_REG_VIG
                        UNION
                        --Filtrados
                        SELECT   FEC_ING_NTF AS HORA,
                                 SUBSTR (' -  ' || GLS_MTV_NTF, 1, 118)
                                    AS DESCRIPCION,
                                 DECODE (NT.FLG_VGN_NTF,
                                         0, 'NO LEIDA',
                                         'LEIDA')
                                    AS ESTADO,
                                 NT.FEC_ING_NTF AS FECHA_NOT, --Fecha ingreso notificación
                                 NT.COD_TIP_CRO AS TIPO
                          FROM   PABS_DT_NTFCC_ALMOT NT,
                                 PABS_DT_RELCN_CRREO_ROL RELCRREO,
                                 PABS_DT_DETLL_CRREO_ALMOT DETCRREO
                         WHERE   NT.COD_TIP_CRO = RELCRREO.COD_TIP_CRO
                                 AND RELCRREO.COD_SIS_ENT = V_COD_SIS_ENT
                                 AND RELCRREO.COD_TPO_ROL_AOS =
                                       p_COD_TPO_ROL_AOS
                                 AND DETCRREO.COD_TPO_CRO = NT.COD_TIP_CRO
                                 AND NT.COD_SIS_ENT IS NOT NULL
                                 AND NT.COD_SIS_ENT =
                                       NVL (V_COD_SIS_ENT_ESP, NT.COD_SIS_ENT)
                                 AND FEC_ING_NTF BETWEEN v_dia_ini
                                                     AND  v_dia_fin --Solo las del día
                                 AND DETCRREO.FLG_VGN_NTF =
                                       PKG_PAB_CONSTANTES.V_REG_VIG --Registro vigente
                                 AND RELCRREO.FLG_VGN =
                                       PKG_PAB_CONSTANTES.V_REG_VIG)
            ORDER BY   HORA_ORDEN DESC;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            err_code := SQLCODE;
            err_msg :=
                  'Problema al obtener las notificaciones Sistema'
               || p_COD_SIS_ENT
               || ' Rol:'
               || p_COD_TPO_ROL_AOS;
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_s_mensaje := err_code || '-' || err_msg;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;


      BEGIN
         --Todos
         SELECT   SUM (CANTIDAD)
           INTO   p_CANT_NTF
           FROM   (SELECT   COUNT (1) AS CANTIDAD
                     FROM   PABS_DT_NTFCC_ALMOT NT,
                            PABS_DT_RELCN_CRREO_ROL RELCRREO,
                            PABS_DT_DETLL_CRREO_ALMOT DETCRREO
                    WHERE       NT.COD_TIP_CRO = RELCRREO.COD_TIP_CRO
                            AND NT.FLG_VGN_NTF = v_FLG_VGN_NTF_NO  --No leidas
                            AND RELCRREO.COD_SIS_ENT = V_COD_SIS_ENT
                            AND RELCRREO.COD_TPO_ROL_AOS = p_COD_TPO_ROL_AOS
                            AND DETCRREO.COD_TPO_CRO = NT.COD_TIP_CRO
                            AND NT.COD_SIS_ENT IS NULL
                            AND FEC_ING_NTF BETWEEN v_dia_ini AND v_dia_fin --Solo las del día
                            AND DETCRREO.FLG_VGN_NTF =
                                  PKG_PAB_CONSTANTES.V_REG_VIG --Registro vigente
                            AND RELCRREO.FLG_VGN =
                                  PKG_PAB_CONSTANTES.V_REG_VIG
                   UNION
                   SELECT   COUNT (1) AS CANTIDAD
                     FROM   PABS_DT_NTFCC_ALMOT NT,
                            PABS_DT_RELCN_CRREO_ROL RELCRREO,
                            PABS_DT_DETLL_CRREO_ALMOT DETCRREO
                    WHERE       NT.COD_TIP_CRO = RELCRREO.COD_TIP_CRO
                            AND NT.FLG_VGN_NTF = v_FLG_VGN_NTF_NO  --No leidas
                            AND RELCRREO.COD_SIS_ENT = V_COD_SIS_ENT
                            AND RELCRREO.COD_TPO_ROL_AOS = p_COD_TPO_ROL_AOS
                            AND DETCRREO.COD_TPO_CRO = NT.COD_TIP_CRO
                            AND NT.COD_SIS_ENT IS NOT NULL
                            AND NT.COD_SIS_ENT =
                                  NVL (V_COD_SIS_ENT_ESP, NT.COD_SIS_ENT)
                            AND FEC_ING_NTF BETWEEN v_dia_ini AND v_dia_fin --Solo las del día
                            AND DETCRREO.FLG_VGN_NTF =
                                  PKG_PAB_CONSTANTES.V_REG_VIG --Registro vigente
                            AND RELCRREO.FLG_VGN =
                                  PKG_PAB_CONSTANTES.V_REG_VIG);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            err_code := SQLCODE;
            err_msg :=
               'Problema al obtener la cantidad de notificaciones no leidas de Sistema'
               || p_COD_SIS_ENT
               || ' Rol:'
               || p_COD_TPO_ROL_AOS;
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_s_mensaje := err_code || '-' || err_msg;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;

      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_BUS_NTFCC_ALMOT;

   /****************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUS_NTFCC_POR_REVIS
   -- Objetivo: Procedimiento almacenado que retorna lista de notificaciones del dashboard por revisar
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_NTFCC_ALMOT
   --
   -- Fecha: 04/07/17
   -- Autor: Santander CAH
   -- Input:
   --   p_FLG_NTF          Flag de notificaión revisada
   --   p_COD_SIS_ENT       Cod. sistema entrada eje:OOFF
   --   p_COD_TPO_ROL_AOS   Rol usuario ingresado
   -- Output:
   --       p_CURSOR        Notificaciones del día
   --       p_CANT_NTF     Cantidad de operaciones sin leer
   --       p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --************************************************************************************************/
   PROCEDURE Sp_PAB_BUS_NTFCC_POR_REVIS (
      p_FLG_NTF           IN     NUMBER,
      p_COD_SIS_ENT       IN     CHAR,
      p_COD_TPO_ROL_AOS   IN     NUMBER,
      p_CURSOR               OUT SYS_REFCURSOR,
      p_CANT_NTF             OUT NUMBER,
      p_ERROR                OUT NUMBER
   )
   IS
      v_NOM_SP            VARCHAR2 (30) := 'Sp_PAB_BUS_NTFCC_POR_REVIS';
      v_CANT_PVI          NUMBER;
      V_COD_SIS_ENT       CHAR (10);
      V_COD_SIS_ENT_ESP   CHAR (10);
   BEGIN
      --Canal de Entrada
      V_COD_SIS_ENT := p_COD_SIS_ENT;

      --Tecnologia (Administrador) debe ver todas las notificaciones
      IF (V_COD_SIS_ENT <> PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SE_TECNO
          AND p_COD_TPO_ROL_AOS <> PKG_PAB_CONSTANTES.V_COD_TIP_ROL_ADM)
      THEN
         V_COD_SIS_ENT_ESP := p_COD_SIS_ENT;
      END IF;

      BEGIN
         OPEN p_CURSOR FOR
              SELECT   TO_CHAR (HORA, 'HH24:MI:SS') AS HORA,
                       HORA AS HORA_ORDEN,
                       DESCRIPCION,
                       ESTADO,
                       FECHA_NOT,
                       TIPO
                FROM   (--Todos
                        SELECT   FEC_ING_NTF AS HORA,
                                 GLS_MTV_NTF AS DESCRIPCION,
                                 DECODE (NT.FLG_VGN_NTF,
                                         0, 'NO LEIDA',
                                         'LEIDA')
                                    AS ESTADO,
                                 NT.FEC_ING_NTF AS FECHA_NOT, --Fecha ingreso notificación
                                 NT.COD_TIP_CRO AS TIPO
                          FROM   PABS_DT_NTFCC_ALMOT NT,
                                 PABS_DT_RELCN_CRREO_ROL RELCRREO,
                                 PABS_DT_DETLL_CRREO_ALMOT DETCRREO
                         WHERE   NT.COD_TIP_CRO = RELCRREO.COD_TIP_CRO
                                 AND RELCRREO.COD_SIS_ENT = V_COD_SIS_ENT
                                 AND RELCRREO.COD_TPO_ROL_AOS =
                                       p_COD_TPO_ROL_AOS
                                 AND DETCRREO.COD_TPO_CRO = NT.COD_TIP_CRO
                                 AND NT.COD_SIS_ENT IS NULL
                                 AND FEC_ING_NTF BETWEEN v_dia_ini
                                                     AND  v_dia_fin --Solo las del día
                                 AND DETCRREO.FLG_VGN_NTF =
                                       PKG_PAB_CONSTANTES.V_REG_VIG --Registro vigente
                                 AND RELCRREO.FLG_VGN =
                                       PKG_PAB_CONSTANTES.V_REG_VIG
                                 AND NT.FLG_VGN_NTF = p_FLG_NTF
                        UNION
                        --Filtrados
                        SELECT   FEC_ING_NTF AS HORA,
                                 GLS_MTV_NTF AS DESCRIPCION,
                                 DECODE (NT.FLG_VGN_NTF,
                                         0, 'NO LEIDA',
                                         'LEIDA')
                                    AS ESTADO,
                                 NT.FEC_ING_NTF AS FECHA_NOT, --Fecha ingreso notificación
                                 NT.COD_TIP_CRO AS TIPO
                          FROM   PABS_DT_NTFCC_ALMOT NT,
                                 PABS_DT_RELCN_CRREO_ROL RELCRREO,
                                 PABS_DT_DETLL_CRREO_ALMOT DETCRREO
                         WHERE   NT.COD_TIP_CRO = RELCRREO.COD_TIP_CRO
                                 AND RELCRREO.COD_SIS_ENT = V_COD_SIS_ENT
                                 AND RELCRREO.COD_TPO_ROL_AOS =
                                       p_COD_TPO_ROL_AOS
                                 AND DETCRREO.COD_TPO_CRO = NT.COD_TIP_CRO
                                 AND NT.COD_SIS_ENT IS NOT NULL
                                 AND NT.COD_SIS_ENT =
                                       NVL (V_COD_SIS_ENT_ESP, NT.COD_SIS_ENT)
                                 AND FEC_ING_NTF BETWEEN v_dia_ini
                                                     AND  v_dia_fin --Solo las del día
                                 AND DETCRREO.FLG_VGN_NTF =
                                       PKG_PAB_CONSTANTES.V_REG_VIG --Registro vigente
                                 AND RELCRREO.FLG_VGN =
                                       PKG_PAB_CONSTANTES.V_REG_VIG
                                 AND NT.FLG_VGN_NTF = p_FLG_NTF)
            ORDER BY   HORA_ORDEN DESC;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            err_code := SQLCODE;
            err_msg :=
                  'Problema al obtener las notificaciones Sistema'
               || p_COD_SIS_ENT
               || ' Rol:'
               || p_COD_TPO_ROL_AOS;
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_s_mensaje := err_code || '-' || err_msg;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;


      BEGIN
         --Todos
         SELECT   SUM (CANTIDAD)
           INTO   p_CANT_NTF
           FROM   (SELECT   COUNT (1) AS CANTIDAD
                     FROM   PABS_DT_NTFCC_ALMOT NT,
                            PABS_DT_RELCN_CRREO_ROL RELCRREO,
                            PABS_DT_DETLL_CRREO_ALMOT DETCRREO
                    WHERE       NT.COD_TIP_CRO = RELCRREO.COD_TIP_CRO
                            AND NT.FLG_VGN_NTF = p_FLG_NTF
                            AND RELCRREO.COD_SIS_ENT = V_COD_SIS_ENT
                            AND RELCRREO.COD_TPO_ROL_AOS = p_COD_TPO_ROL_AOS
                            AND DETCRREO.COD_TPO_CRO = NT.COD_TIP_CRO
                            AND NT.COD_SIS_ENT IS NULL
                            AND FEC_ING_NTF BETWEEN v_dia_ini AND v_dia_fin --Solo las del día
                            AND DETCRREO.FLG_VGN_NTF =
                                  PKG_PAB_CONSTANTES.V_REG_VIG --Registro vigente
                            AND RELCRREO.FLG_VGN =
                                  PKG_PAB_CONSTANTES.V_REG_VIG
                   UNION
                   SELECT   COUNT (1) AS CANTIDAD
                     FROM   PABS_DT_NTFCC_ALMOT NT,
                            PABS_DT_RELCN_CRREO_ROL RELCRREO,
                            PABS_DT_DETLL_CRREO_ALMOT DETCRREO
                    WHERE       NT.COD_TIP_CRO = RELCRREO.COD_TIP_CRO
                            AND NT.FLG_VGN_NTF = p_FLG_NTF
                            AND RELCRREO.COD_SIS_ENT = V_COD_SIS_ENT
                            AND RELCRREO.COD_TPO_ROL_AOS = p_COD_TPO_ROL_AOS
                            AND DETCRREO.COD_TPO_CRO = NT.COD_TIP_CRO
                            AND NT.COD_SIS_ENT IS NOT NULL
                            AND NT.COD_SIS_ENT =
                                  NVL (V_COD_SIS_ENT_ESP, NT.COD_SIS_ENT)
                            AND FEC_ING_NTF BETWEEN v_dia_ini AND v_dia_fin --Solo las del día
                            AND DETCRREO.FLG_VGN_NTF =
                                  PKG_PAB_CONSTANTES.V_REG_VIG --Registro vigente
                            AND RELCRREO.FLG_VGN =
                                  PKG_PAB_CONSTANTES.V_REG_VIG);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            err_code := SQLCODE;
            err_msg :=
               'Problema al obtener la cantidad de notificaciones no leidas de Sistema'
               || p_COD_SIS_ENT
               || ' Rol:'
               || p_COD_TPO_ROL_AOS;
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_s_mensaje := err_code || '-' || err_msg;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;

      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   --        IF p_COD_SIS_ENT = PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SE_OOFF  THEN
   --
   --            OPEN p_CURSOR FOR
   --                    SELECT TO_CHAR(FEC_ING_NTF, 'HH24:MI:SS')  AS HORA,
   --                    GLS_MTV_NTF                                AS DESCRIPCION,
   --                    DECODE (FLG_VGN_NTF,0,'NO LEIDA','LEIDA')  AS ESTADO,
   --                    FEC_ING_NTF                                AS FECHA_NOT,--Fecha ingreso notificación
   --                    COD_TIP_CRO                                AS TIPO
   --                    FROM PABS_DT_NTFCC_ALMOT NT
   --                    WHERE FLG_VGN_NTF  = p_FLG_NTF  -- No leida
   --                    AND NT.COD_SIS_ENT IN (PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SE_OOFF)
   --                    AND NT.COD_TPO_ROL_AOS = p_COD_TPO_ROL_AOS
   --                    AND FEC_ING_NTF BETWEEN v_dia_ini and v_dia_fin
   --                    UNION
   --                    SELECT TO_CHAR(FEC_ING_NTF, 'HH24:MI:SS')  AS HORA,
   --                    GLS_MTV_NTF                                AS DESCRIPCION,
   --                    DECODE (FLG_VGN_NTF,0,'NO LEIDA','LEIDA')  AS ESTADO,
   --                    FEC_ING_NTF                                AS FECHA_NOT,--Fecha ingreso notificación
   --                    COD_TIP_CRO                                AS TIPO
   --                    FROM PABS_DT_NTFCC_ALMOT NT
   --                    WHERE FLG_VGN_NTF  = p_FLG_NTF  -- No leida
   --                    AND FEC_ING_NTF BETWEEN v_dia_ini and v_dia_fin
   --                    AND NT.COD_TIP_CRO IN('NAUT','CCABN','MHOR')--'PVIS ',
   --                    order by 4 DESC;
   --
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg :=
               'No se han encontrado registos asociados a la busqueda '
            || p_FLG_NTF
            || '- canal '
            || p_COD_SIS_ENT
            || '- rol '
            || p_COD_TPO_ROL_AOS;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_BUS_NTFCC_POR_REVIS;


   /************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ACT_NTFCC_ALMOT
   -- Objetivo: Procedimiento almacenado que Actualiza las notificaciones leidas
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_SISTM_ENTRD_SALID
   --
   -- Fecha: 04/07/17
   -- Autor: Santander CAH
   -- Input:
   --   p_FEC_ING_NTF      Fecha de ingreso de la notificación
   --  p_COD_TIP_CRO      Tipo de notificación
   -- Output:
   --       p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_ACT_NTFCC_ALMOT (p_FEC_ING_NTF   IN     TIMESTAMP,
                                     p_COD_TIP_CRO   IN     CHAR,
                                     p_ERROR            OUT NUMBER)
   IS
      v_NOM_SP   VARCHAR2 (30) := 'Sp_PAB_ACT_NTFCC_ALMOT';
   BEGIN
      --        IF p_REG_OPER.COUNT > 0 THEN
      --
      --            FOR i IN p_REG_OPER.FIRST .. p_REG_OPER.LAST
      --            LOOP
      --
      --                BEGIN
      --
      --                    --Actualizamos estado nuevo de la operacion.enviada
      --                    UPDATE PABS_DT_NTFCC_ALMOT
      --                    SET FLG_VGN_NTF = v_FLG_VGN_NTF_SI -- VALOR 1, notif leida
      --                    WHERE FEC_ING_NTF = p_FEC_ING_NTF
      --                    AND   COD_TIP_CRO = p_COD_TIP_CRO;
      --
      --                EXCEPTION
      --                    WHEN NO_DATA_FOUND THEN
      --                    err_msg := 'No se pudo obtener informacion de la operacion:' || p_REG_OPER(i).NUM_FOL_OPE;
      --                    PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( NULL, v_NOM_PCK ,err_msg , v_NOM_SP);
      --                    RAISE;
      --
      --                WHEN OTHERS THEN
      --                    err_code := SQLCODE;
      --                    err_msg := SUBSTR(SQLERRM, 1, 300);
      --                    p_s_mensaje := err_code || '-' || err_msg;
      --                    PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
      --                    p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
      --                    RAISE_APPLICATION_ERROR(-20000,p_s_mensaje);
      --
      --                END;
      --
      --            END LOOP;
      --
      --            p_ERROR:= PKG_PAB_CONSTANTES.v_OK;
      --
      --        END IF;


      --Actualizamos estado nuevo de la operacion.enviada
      UPDATE   PABS_DT_NTFCC_ALMOT
         SET   FLG_VGN_NTF = v_FLG_VGN_NTF_SI          -- VALOR 1, notif leida
       WHERE   FEC_ING_NTF = p_FEC_ING_NTF AND COD_TIP_CRO = p_COD_TIP_CRO;

      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg :=
               'No se han encontrado registos asociados a la busqueda fecha '
            || p_FEC_ING_NTF
            || '- vigencia '
            || v_FLG_VGN_NTF_SI
            || '- tipo '
            || p_COD_TIP_CRO;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_ACT_NTFCC_ALMOT;



   /************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_INS_NTFCC_ALMOT
   -- Objetivo: Procedimiento almacenado que inserta las notificaciones en la tabla
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_NTFCC_ALMOT
   --
   -- Fecha: 10/07/17
   -- Autor: Santander CAH
   -- Input:
   --   p_COD_TIP_CRO       Tipo de notificación
   --   p_COD_SIS_ENT       Cod. sistema entrada
   --   p_COD_TPO_ROL_AOS   Rol usuario
   --   p_IMP_OPE            Monto Operación asociada
   --   p_NUM_FOL_OPE      Numero de operación
   --   p_NUM_FOL_NOM      Numero de nómina
   --   p_GLS_MTV_NTF     Descripción de la notificación
   --
   -- Output:
   --       p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_INS_NTFCC_ALMOT (p_COD_TIP_CRO       IN     CHAR,
                                     p_COD_SIS_ENT       IN     CHAR,
                                     p_COD_TPO_ROL_AOS   IN     NUMBER,
                                     p_IMP_OPE           IN     NUMBER,
                                     p_NUM_FOL_OPE       IN     NUMBER,
                                     p_NUM_FOL_NOM       IN     NUMBER,
                                     p_GLS_MTV_NTF       IN     VARCHAR2,
                                     p_ERROR                OUT NUMBER)
   IS
      v_NOM_SP        VARCHAR2 (30) := 'Sp_PAB_INS_NTFCC_ALMOT';
      v_FLG_LEIDA     NUMBER (1) := PKG_PAB_CONSTANTES.V_FLG_VGN_NO; -- VALOR 0, notif No leida
      v_FLG_VGN_NTF   NUMBER (1);
      v_FEC_ING_NTF   TIMESTAMP;
      v_COD_SIS_ENT   CHAR (10) := p_COD_SIS_ENT;
   BEGIN
      --Buscamos la vigencia de la notificación a guardar
      BEGIN
         SELECT   DC.FLG_VGN_NTF
           INTO   v_FLG_VGN_NTF
           FROM   PABS_DT_DETLL_CRREO_ALMOT DC
          WHERE   DC.COD_TPO_CRO = p_COD_TIP_CRO;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            err_code := SQLCODE;
            err_msg :=
               'No existe la  notificación, con código: ' || p_COD_TIP_CRO;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            RAISE;
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_s_mensaje := err_code || '-' || err_msg;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;


      v_FEC_ING_NTF := SYSTIMESTAMP;

      IF v_FLG_VGN_NTF = PKG_PAB_CONSTANTES.V_FLG_VGN_SI
      THEN
         BEGIN
            INSERT INTO PABS_DT_NTFCC_ALMOT (FEC_ING_NTF,
                                             COD_TIP_CRO,
                                             FLG_VGN_NTF,
                                             COD_SIS_ENT,
                                             COD_TPO_ROL_AOS,
                                             IMP_OPE,
                                             NUM_FOL_OPE,
                                             NUM_FOL_NOM,
                                             GLS_MTV_NTF)
              VALUES   (v_FEC_ING_NTF,
                        p_COD_TIP_CRO,
                        v_FLG_LEIDA,
                        v_COD_SIS_ENT,
                        p_COD_TPO_ROL_AOS,
                        p_IMP_OPE,
                        p_NUM_FOL_OPE,
                        p_NUM_FOL_NOM,
                        p_GLS_MTV_NTF);

            p_ERROR := PKG_PAB_CONSTANTES.v_OK;
         EXCEPTION
            WHEN DUP_VAL_ON_INDEX
            THEN
               err_code := SQLCODE;
               err_msg :=
                  'No se pudo insertar la notificación: ' || p_COD_TIP_CRO;
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            WHEN OTHERS
            THEN
               err_code := SQLCODE;
               err_msg := SUBSTR (SQLERRM, 1, 300);
               p_s_mensaje := err_code || '-' || err_msg;
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);

               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
               RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
         END;
      END IF;

      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   END Sp_PAB_INS_NTFCC_ALMOT;

   /************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_DET_CRRO_TIBCO
   -- Objetivo: Procedimiento almacenado que retorna el cuerpo del correo para la notificación
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_CRREO_ALMOT
   --
   -- Fecha: 10/07/17
   -- Autor: Santander CAH
   -- Input:
   --   p_REF_SWF          Referencia Swift de la operación
   --   p_NUM_FOL_OPE      Folio de operación
   --   p_FEC_ISR_OPE      fecha de inserción operación
   --   p_COD_TPO_OPE      Numero de operación
   --   p_ERROR_SWF        Numero de nómina
   --   p_COD_SIS_ENT_SAL     Descripción de la notificación
   --   p_AUX_1
   --   p_AUX_2
   --   p_AUX_3
   -- Output:
   --     p_ADJUNTO           Si tiene adjunto
   --     p_ID                Id de gestor
   --     MENSAJE_SWF         Contenido de mensaje
   --     p_ASUNTO           Asunto correo
   --     p_CORREO          Dirección de correo
   --     p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_PAB_DET_CRRO_TIBCO (p_REF_SWF           IN     VARCHAR2,
                                    p_NUM_FOL_OPE       IN     NUMBER,
                                    p_FEC_ISR_OPE       IN     DATE,
                                    p_COD_TPO_OPE       IN     CHAR,
                                    p_ERROR_SWF         IN     VARCHAR2,
                                    p_COD_SIS_ENT_SAL   IN     CHAR, --Para la notif de cambio horario canal.
                                    p_AUX_1             IN     VARCHAR2,
                                    p_AUX_2             IN     VARCHAR2,
                                    p_AUX_3             IN     VARCHAR2,
                                    p_ADJUNTO              OUT NUMBER,
                                    p_ID                   OUT VARCHAR2,
                                    MENSAJE_SWF            OUT VARCHAR2,
                                    p_ASUNTO               OUT VARCHAR2,
                                    p_CORREO               OUT VARCHAR2,
                                    p_ERROR                OUT NUMBER)
   IS
      --
      v_NOM_SP            VARCHAR2 (30) := 'SP_PAB_DET_CRRO_TIBCO';
      v_REF_SWIFT_ORI     VARCHAR2 (30);
      v_REF_SWIFT         VARCHAR2 (100);
      v_COD_MT            VARCHAR2 (30);
      v_MONTO_TOTAL       NUMBER (15, 2);
      v_MONTO_TOTAL_STR   VARCHAR2 (25);
      v_BIC_DESTINO       VARCHAR2 (11);
      v_BIC_ORIGEN        VARCHAR2 (11);
      v_FECHA_PAGO        VARCHAR2 (30);
      v_BENEFICIARIO      VARCHAR2 (50);
      v_NUM_OPE           VARCHAR2 (30);
      v_NUM_NOM           VARCHAR2 (50);
      v_MERCADO           VARCHAR2 (30);
      v_MENSAJE           VARCHAR2 (500);
      v_MONEDA            VARCHAR2 (10);
      v_ID                VARCHAR2 (100);
      v_NUM_CTA_ODN       VARCHAR2 (30);
      v_NUM_MOV_CTB       VARCHAR2 (30);
      V_COD_TPO_OPE       CHAR (5) := p_COD_TPO_OPE;
      ---Modificar variables para recibir rol y área.
      v_COD_TPO_ROL_AOS   NUMBER (1) := 2;                              -- Rol
      v_COD_SIS_ENT_SAL   CHAR (10) := p_COD_SIS_ENT_SAL;
      v_MENSAJE_NTF       VARCHAR2 (500);
      v_ASUNTO            VARCHAR2 (300) := 'Mensajería de Altos Montos - ';
      v_ASUNTO2           VARCHAR2 (300);
      v_NUM_OPE2          VARCHAR2 (12);
      v_NUM_FOL_OPE       NUMBER;
      v_FEC_ISR_OPE       DATE;
      v_NUM_REF_SWF       CHAR (16);
      v_GLS_ADC_EST       VARCHAR2 (500);
      v_FLG_VGN_NTF       NUMBER (1);
      v_DESC_AREA         VARCHAR2 (50);
      v_OBS_OPC_SWF       VARCHAR2 (200);
      v_GLS_CRO_NTF       VARCHAR2 (100);
      ERROR_REFSWF EXCEPTION;
   BEGIN
      --Si solo viene la referencia Swift, buscamos el numero de operacion y la fecha.
      -- Busqueda por referencia
      IF (    NVL (p_REF_SWF, '#') <> '#'
          AND NVL (p_NUM_FOL_OPE, 0) = 0
          AND p_COD_TPO_OPE <> PKG_PAB_CONSTANTES.V_COD_TIP_CRO_ERACT)
      THEN
         v_NUM_REF_SWF := p_REF_SWF;

         BEGIN
            --Obtenemos la informacion de la operacion origen
            SELECT   NUM_FOL_OPE, FEC_ISR_OPE
              INTO   v_NUM_FOL_OPE, v_FEC_ISR_OPE
              FROM   PABS_DT_DETLL_OPRCN
             WHERE   NUM_REF_SWF = v_NUM_REF_SWF;

            IF (v_NUM_FOL_OPE IS NULL OR v_FEC_ISR_OPE IS NULL)
            THEN
               RAISE ERROR_REFSWF;
            END IF;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               err_code := SQLCODE;
               err_msg :=
                  'No se pudo obtener la información de la operación origen: '
                  || v_NUM_REF_SWF;
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
               RAISE;
            WHEN OTHERS
            THEN
               err_code := SQLCODE;
               err_msg := SUBSTR (SQLERRM, 1, 300);
               p_s_mensaje := err_code || '-' || err_msg;
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
               RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
         END;
      ELSE
         v_NUM_FOL_OPE := p_NUM_FOL_OPE;
         v_FEC_ISR_OPE := p_FEC_ISR_OPE;
      END IF;

      --Busqueda por numero de operación
      IF v_NUM_FOL_OPE > 0
      THEN
         BEGIN
            --Buscamos datos de la operación.
            SELECT   DETOPE.NUM_REF_SWF,
                     DETOPE.NUM_REF_EXT,
                     DETOPE.COD_MT_SWF,
                     DETOPE.IMp_OPE,
                     DETOPE.COD_BCO_DTN,
                     DETOPE.FEC_VTA,
                     DETOPE.NOM_BFC,
                     DETOPE.NUM_FOL_OPE,
                     DETOPE.NUM_FOL_NMN,
                     DETOPE.FLG_MRD_UTZ_SWF,
                     DETOPE.COD_DVI,
                     DETOPE.NUM_IDF_GTR_DOC,
                     DETOPE.GLS_ADC_EST,
                     DETOPE.NUM_MVT_CTB,
                     DETPINF.NUM_CTA_ODN,
                     DETPINF.OBS_OPC_SWF,
                     DETOPE.COD_BCO_ORG
              INTO   v_REF_SWIFT_ORI,
                     v_REF_SWIFT,
                     v_COD_MT,
                     v_MONTO_TOTAL,
                     v_BIC_DESTINO,
                     v_FECHA_PAGO,
                     v_BENEFICIARIO,
                     v_NUM_OPE,
                     v_NUM_NOM,
                     v_MERCADO,
                     v_MONEDA,
                     v_ID,
                     v_GLS_ADC_EST,
                     v_NUM_MOV_CTB,
                     v_NUM_CTA_ODN,
                     v_OBS_OPC_SWF,
                     v_BIC_ORIGEN
              FROM   PABS_DT_DETLL_OPRCN DETOPE,
                     PABS_DT_OPRCN_INFCN_OPCON DETPINF
             WHERE       DETOPE.NUM_FOL_OPE = DETPINF.NUM_FOL_OPE
                     AND DETOPE.FEC_ISR_OPE = DETPINF.FEC_ISR_OPE
                     AND DETOPE.NUM_FOL_OPE = v_NUM_FOL_OPE
                     AND DETOPE.FEC_ISR_OPE = v_FEC_ISR_OPE;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               err_code := SQLCODE;
               err_msg :=
                     'No se encuentra información para la operación: '
                  || v_NUM_FOL_OPE
                  || ' Fecha:'
                  || TO_CHAR (v_FEC_ISR_OPE, 'DD-MM-YYYY');
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               RAISE;
            WHEN OTHERS
            THEN
               err_code := SQLCODE;
               err_msg := SUBSTR (SQLERRM, 1, 300);
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_s_mensaje := err_code || '-' || err_msg;
               RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         END;
      END IF;

      BEGIN
         --Obtenemos informacion para generar correo

         --Busca glosa para correo y adjunto si tiene, también glosa notificación pantalla
         SELECT   REPLACE (GLS_MSJ_CRO, '%Ope', v_NUM_OPE),
                  FLG_ADJ_CRO,
                  REPLACE (GLS_NOT_USR, '%Ope', v_NUM_OPE),
                  v_ASUNTO || COR.GLS_ENC_CRO,
                  COR.FLG_VGN_NTF,
                  COR.GLS_CRO_NTF,
                  COR.COD_TPO_ROL_AOS
           INTO   v_MENSAJE,
                  p_ADJUNTO,
                  v_MENSAJE_NTF,
                  p_ASUNTO,
                  v_FLG_VGN_NTF,
                  p_CORREO,
                  v_COD_TPO_ROL_AOS
           FROM   PABS_DT_DETLL_CRREO_ALMOT COR
          WHERE   COD_TPO_CRO = V_COD_TPO_OPE;

         --Formateo de monto
         IF v_MONEDA = PKG_PAB_CONSTANTES.V_STR_DVI_CLP
         THEN
            v_MONTO_TOTAL_STR :=
               '$ ' || TO_CHAR (v_MONTO_TOTAL, 'FM999,999,999,999,999');
         ELSE
            v_MONTO_TOTAL_STR :=
               '$ ' || TO_CHAR (v_MONTO_TOTAL, 'FM999,999,999,999.00');
         END IF;

         --
         v_MONTO_TOTAL_STR := REPLACE (v_MONTO_TOTAL_STR, '.', '#');
         v_MONTO_TOTAL_STR := REPLACE (v_MONTO_TOTAL_STR, ',', '.');
         v_MONTO_TOTAL_STR := REPLACE (v_MONTO_TOTAL_STR, '#', ',');

         --   Si es cambio de Horario o un cambio ...
         IF V_COD_TPO_OPE = PKG_PAB_CONSTANTES.V_COD_TIP_CRO_MHOR
            OR V_COD_TPO_OPE = PKG_PAB_CONSTANTES.V_COD_TIP_CRO_CCABN
         THEN
            BEGIN                            -- Busca la descripción del Area.
               SELECT   SE.DSC_SIS_ENT_SAL
                 INTO   v_DESC_AREA
                 FROM   PABS_DT_SISTM_ENTRD_SALID SE
                WHERE   SE.COD_SIS_ENT_SAL = p_COD_SIS_ENT_SAL;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  err_code := SQLCODE;
                  err_msg := 'No hay data para el área ' || p_COD_SIS_ENT_SAL;
                  PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                      v_NOM_PCK,
                                                      err_msg,
                                                      v_NOM_SP);
                  p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
                  RAISE;
               WHEN OTHERS
               THEN
                  err_code := SQLCODE;
                  err_msg := SUBSTR (SQLERRM, 1, 300);
                  PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                      v_NOM_PCK,
                                                      err_msg,
                                                      v_NOM_SP);
                  p_s_mensaje := err_code || '-' || err_msg;
                  RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
                  p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            END;

            v_COD_SIS_ENT_SAL := p_COD_SIS_ENT_SAL;
         END IF;

         /*Correo*/
         v_MENSAJE :=
            REPLACE (v_MENSAJE, '%Ref', NVL (v_REF_SWIFT_ORI, ' --- '));
         v_MENSAJE := REPLACE (v_MENSAJE, '%MtRef', NVL (p_REF_SWF, ' --- '));
         v_MENSAJE :=
            REPLACE (v_MENSAJE, '%Monto', NVL (v_MONTO_TOTAL_STR, ' --- '));
         v_MENSAJE := REPLACE (v_MENSAJE, '%Moneda', NVL (v_MONEDA, ' ---'));
         v_MENSAJE := REPLACE (v_MENSAJE, '%Error', p_ERROR_SWF);
         v_MENSAJE := REPLACE (v_MENSAJE, '%BcoDtn', v_BIC_DESTINO);
         v_MENSAJE := REPLACE (v_MENSAJE, '%BcoOrg', v_BIC_ORIGEN);
         v_MENSAJE := REPLACE (v_MENSAJE, '%Motivo', v_GLS_ADC_EST);
         v_MENSAJE := REPLACE (v_MENSAJE, '%Canal', v_DESC_AREA);
         v_MENSAJE :=
            REPLACE (v_MENSAJE, '%CtaOdn', NVL (v_NUM_CTA_ODN, ' --- '));
         v_MENSAJE :=
            REPLACE (v_MENSAJE, '%NumCtb', NVL (v_NUM_MOV_CTB, ' --- '));

         /*Notificación*/
         v_MENSAJE_NTF :=
            REPLACE (v_MENSAJE_NTF, '%Ref', NVL (v_REF_SWIFT_ORI, ' _ '));
         v_MENSAJE_NTF :=
            REPLACE (v_MENSAJE_NTF, '%MtRef', NVL (p_REF_SWF, ' _ '));
         v_MENSAJE_NTF :=
            REPLACE (v_MENSAJE_NTF, '%Monto', NVL (v_MONTO_TOTAL_STR, ' - '));
         v_MENSAJE_NTF :=
            REPLACE (v_MENSAJE_NTF, '%Moneda', NVL (v_MONEDA, ' -'));
         v_MENSAJE_NTF := REPLACE (v_MENSAJE_NTF, '%Error', p_ERROR_SWF);
         v_MENSAJE_NTF := REPLACE (v_MENSAJE_NTF, '%BcoDtn', v_BIC_DESTINO);
         v_MENSAJE_NTF := REPLACE (v_MENSAJE_NTF, '%BcoOrg', v_BIC_ORIGEN);
         v_MENSAJE_NTF := REPLACE (v_MENSAJE_NTF, '%Canal', v_DESC_AREA);
         v_MENSAJE_NTF :=
            REPLACE (v_MENSAJE_NTF, '%CtaOdn', NVL (v_NUM_CTA_ODN, ' --- '));
         v_MENSAJE_NTF :=
            REPLACE (v_MENSAJE_NTF, '%NumCtb', NVL (v_NUM_MOV_CTB, ' --- '));

         p_ERROR := PKG_PAB_CONSTANTES.v_OK;

         CASE
            WHEN V_COD_TPO_OPE = PKG_PAB_CONSTANTES.V_COD_TIP_CRO_PVIS
            THEN
               v_COD_SIS_ENT_SAL := PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SE_MESA;
            WHEN V_COD_TPO_OPE = PKG_PAB_CONSTANTES.V_COD_TIP_CRO_ELIMI
            THEN
               --Operaciones Eliminadas
               v_COD_SIS_ENT_SAL := NULL;

               v_MENSAJE_NTF :=
                  REPLACE (v_MENSAJE_NTF, '%CantEliOpe', p_COD_SIS_ENT_SAL);
               v_MENSAJE_NTF :=
                  REPLACE (v_MENSAJE_NTF, '%MovEli', v_motivo_eli);

               v_MENSAJE :=
                  REPLACE (v_MENSAJE, '%CantEliOpe', p_COD_SIS_ENT_SAL);
               v_MENSAJE := REPLACE (v_MENSAJE, '%MovEli', v_motivo_eli);
            WHEN V_COD_TPO_OPE = PKG_PAB_CONSTANTES.V_COD_TIP_CRO_ABCT OR V_COD_TPO_OPE = PKG_PAB_CONSTANTES.V_COD_TIP_CRO_EBCT
            THEN
               --Abono / Error cuenta corriente
               v_COD_SIS_ENT_SAL := NULL;
               v_MONTO_TOTAL_STR :=
                  '$ ' || TO_CHAR (p_AUX_2, 'FM999,999,999,999,999');

               v_MONTO_TOTAL_STR := REPLACE (v_MONTO_TOTAL_STR, ',', '.');

               v_MENSAJE_NTF := REPLACE (v_MENSAJE_NTF, '%Cue910', p_AUX_1);
               v_MENSAJE_NTF :=
                  REPLACE (v_MENSAJE_NTF, '%Mon910', v_MONTO_TOTAL_STR);
               v_MENSAJE_NTF :=
                  REPLACE (v_MENSAJE_NTF, '%Mot910', p_ERROR_SWF);
               v_MENSAJE_NTF := REPLACE (v_MENSAJE_NTF, '%ben910', p_AUX_3);

               v_MENSAJE := REPLACE (v_MENSAJE, '%Cue910', p_AUX_1);
               v_MENSAJE := REPLACE (v_MENSAJE, '%Mon910', v_MONTO_TOTAL_STR);
               v_MENSAJE := REPLACE (v_MENSAJE, '%Mot910', p_ERROR_SWF);
               v_MENSAJE := REPLACE (v_MENSAJE, '%ben910', p_AUX_3);
            WHEN V_COD_TPO_OPE = PKG_PAB_CONSTANTES.V_COD_TIP_CRO_ERACT
            THEN
               v_MENSAJE_NTF := REPLACE (v_MENSAJE_NTF, '%CanOrig', p_AUX_1);
               v_MENSAJE := REPLACE (v_MENSAJE, '%CanOrig', p_AUX_1);
            WHEN V_COD_TPO_OPE = PKG_PAB_CONSTANTES.V_COD_TIP_CRO_ITCCC OR V_COD_TPO_OPE = PKG_PAB_CONSTANTES.V_COD_TIP_CRO_ITAKN OR V_COD_TPO_OPE = PKG_PAB_CONSTANTES.V_COD_TIP_CRO_ITCTB OR V_COD_TPO_OPE = PKG_PAB_CONSTANTES.V_COD_TIP_CRO_ITVVV
            THEN
               --Interfaz
               v_MENSAJE_NTF := REPLACE (v_MENSAJE_NTF, '%NomArch', p_AUX_1);
               v_MENSAJE := REPLACE (v_MENSAJE, '%NomArch', p_AUX_1);
            ELSE
               NULL;
         END CASE;

         MENSAJE_SWF := v_MENSAJE;
         p_ID := v_ID;

         PKG_PAB_NOTIFICACION.Sp_PAB_INS_NTFCC_ALMOT (V_COD_TPO_OPE --   (p_COD_TIP_CRO,
                                                                   ,
                                                      v_COD_SIS_ENT_SAL --   p_COD_SIS_ENT_SAL,
                                                                       ,
                                                      v_COD_TPO_ROL_AOS --   p_COD_TPO_ROL_AOS,
                                                                       ,
                                                      v_MONTO_TOTAL --   p_IMP_OPE,
                                                                   ,
                                                      v_NUM_OPE --   p_NUM_FOL_OPE,
                                                               ,
                                                      v_NUM_NOM --   p_NUM_FOL_NOM,
                                                               ,
                                                      v_MENSAJE_NTF --   p_GLS_MTV_NTF,
                                                                   ,
                                                      p_ERROR);

         p_ERROR := PKG_PAB_CONSTANTES.v_OK;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            err_code := SQLCODE;
            err_msg := 'No hay data para la operacion ' || p_COD_TPO_OPE;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_s_mensaje := err_code || '-' || err_msg;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      END;
   END SP_PAB_DET_CRRO_TIBCO;

   /************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_BUS_OPE_SIN_MVT
   -- Objetivo: Procedimiento almacenado que retorna la operación sin cambio de estado.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_CRREO_ALMOT
   --
   -- Fecha: 16/08/17
   -- Autor: Santander CAH
   -- Input:
   --   p_NUM_FOL_OPE       Numero de operación
   --   p_FEC_ISR_OPE        fecha de operación
   --   p_COD_TPO_OPE     Tipo notificación
   -- Output:
   --      p_ID_NTF        Id de notificación
   --      p_CANT_PAB      Cantidad de operaciones
   --       p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_PAB_BUS_OPE_SIN_MVT (p_NUM_FOL_OPE   IN     NUMBER,
                                     p_FEC_ISR_OPE   IN     DATE,
                                     p_COD_TPO_OPE   IN     VARCHAR2,
                                     p_ID_NTF           OUT NUMBER,
                                     p_CANT_PAB         OUT NUMBER,
                                     p_ERROR            OUT NUMBER)
   IS
      v_NOM_SP            VARCHAR2 (30) := 'SP_PAB_BUS_OPE_SIN_MVT';
      v_COD_EST_AOS_ACT   NUMBER;
      v_COD_EST_AOS       NUMBER;
      v_COD_TPO_OPE_LIB   VARCHAR2 (10) := 'NLIB';
      v_COD_TPO_OPE_ABO   VARCHAR2 (10) := 'NCAM';
   BEGIN
      -- Obtiene el estado actual de la operación
      PKG_PAB_OPERACION.SP_PAB_OBT_EST_ACT_OPE (p_NUM_FOL_OPE,
                                                p_FEC_ISR_OPE,
                                                v_COD_EST_AOS);


      IF p_COD_TPO_OPE = v_COD_TPO_OPE_LIB
      THEN                                         --Notificación No liberadas
         IF v_COD_EST_AOS = PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPELIB
         THEN
            p_ID_NTF := 1;                                  --Sin Notificación
            p_CANT_PAB := 0;
         ELSE
            p_ID_NTF := 0;
            p_CANT_PAB := 0;
         END IF;
      ELSIF p_COD_TPO_OPE = v_COD_TPO_OPE_ABO
      THEN                                          --Notificación No abonadas
         IF v_COD_EST_AOS = PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAB
         THEN
            p_ID_NTF := 1;                                   --Sin Notifiación

            SELECT   COUNT (DETOPE.COD_EST_AOS)
              INTO   p_CANT_PAB
              FROM   PABS_DT_DETLL_OPRCN DETOPE
             WHERE   DETOPE.COD_EST_AOS =
                        PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAB
                     AND DETOPE.FEC_ISR_OPE BETWEEN v_dia_ini AND v_dia_fin;
         ELSE
            p_ID_NTF := 0;
            p_CANT_PAB := 0;
         END IF;
      END IF;


      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg :=
               'No se han encontrado registos asociados a la busqueda  :'
            || p_NUM_FOL_OPE
            || '- fecha '
            || p_FEC_ISR_OPE
            || ' - codigo'
            || p_COD_TPO_OPE;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_BUS_OPE_SIN_MVT;

   /************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUS_OPE_SIN_CBO
   -- Objetivo: Procedimiento almacenado que retorna tiempo de operaciones sin cambio de estado,
   --          para operaciones en estado "Liberada" y "Por Abonar"
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN
   --
   -- Fecha: 30/06/17
   -- Autor: Santander CAH
   -- Input:
   --       p_COD_EST_AOS      Estado Operación(liberada-Por abonar)
   -- Output:
   --       p_FLG_NTF       Indica Si hay que notificar
   --       p_CAN_OPE_NOT   Cantidad de operaciones sin cambios
   --       p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_BUS_OPE_SIN_CBO (p_COD_EST_AOS   IN     VARCHAR2,
                                     p_FLG_NTF          OUT NUMBER,
                                     p_CAN_OPE_NOT      OUT NUMBER,
                                     p_ERROR            OUT NUMBER)
   IS
      v_NOM_SP        VARCHAR2 (30) := 'Sp_PAB_BUS_OPE_SIN_CBO';
      v_COD_EST_AOS   CHAR (3) := p_COD_EST_AOS;
      v_CAN_OPE_NOT   NUMBER;
      v_FEC_AZA_SWF   CHAR (20);
      v_HOR_ACT       CHAR (20);
      v_HOR_SIN_CMB   NUMBER (10);
      v_CAN           NUMBER;
   BEGIN
      IF p_COD_EST_AOS = PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEPAB
      THEN
         SELECT   COUNT (1)
           INTO   v_CAN
           FROM   PABS_DT_DETLL_OPRCN OP
          WHERE   OP.COD_EST_AOS = PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAB
                  AND OP.FEC_ACT_OPE BETWEEN v_dia_ini AND v_dia_fin;

         IF v_CAN >= 1
         THEN
            p_FLG_NTF := 1;
         ELSE
            p_FLG_NTF := 0;
         END IF;
      ELSIF p_COD_EST_AOS = PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEPVI
      THEN
         --Revisar Notificación.
         SELECT   COUNT (1)
           INTO   v_CAN
           FROM   PABS_DT_DETLL_OPRCN OP
          WHERE   OP.COD_EST_AOS = PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPVI
                  AND OP.FEC_ACT_OPE BETWEEN v_dia_ini AND v_dia_fin;


         IF v_CAN >= 1
         THEN
            p_FLG_NTF := 1;
         ELSE
            p_FLG_NTF := 0;
         END IF;
      END IF;

      p_CAN_OPE_NOT := v_CAN;
      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := 'No hay operaciones para el estado  ' || v_COD_EST_AOS;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_BUS_OPE_SIN_CBO;

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_GES_NTF_OPE
   -- Objetivo: Procedimiento que gestiona el envio de notificación para cada operacion eliminada.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: N/A
   -- Fecha: 18/08/17
   -- Autor: Santander
   -- Input:
   --      p_REG_OPER       -> parametro tipo registro que contiene el numero y fecha operacion
   --      p_GLS_MTV_EST    -> motivo o glosa de eliminacion
   -- Output:

   --     p_ASUNTO           Asunto correo
   --     p_CORREO          Dirección de correo
   --     p_MENSAJE_SWF      Contenido de mensaje
   --     p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_GES_NTF_OPE (p_REG_OPER      IN     REG_OPER,
                                 p_GLS_MTV_EST   IN     VARCHAR2,
                                 p_ASUNTO           OUT VARCHAR2,
                                 p_CORREO           OUT VARCHAR2,
                                 p_MENSAJE_SWF      OUT VARCHAR2,
                                 p_ERROR            OUT NUMBER)
   IS
      --
      v_NOM_SP            VARCHAR2 (31) := 'Sp_PAB_GES_NTF_OPE';

      p_REF_SWF           VARCHAR2 (100) := NULL;
      p_COD_TPO_OPE       CHAR (5) := 'ELIM';
      p_ERROR_SWF         VARCHAR2 (300) := p_GLS_MTV_EST;
      p_COD_SIS_ENT_SAL   CHAR (5) := 'OOFF';
      p_ADJUNTO           NUMBER := NULL;
      p_ID                VARCHAR2 (100) := NULL;
      p_ACUM_CORREO       VARCHAR2 (800) := NULL;
      v_TOTAL_CORREO      VARCHAR2 (800) := NULL;
      v_FEC_ISR_OPE       DATE;
      v_NUM_FOL_OPE       NUMBER (12);
      v_NUM_FOL_NMN       NUMBER (12);
      p_SALIDA            VARCHAR2 (800);
      p_CANT              NUMBER (2) := 0;
   BEGIN
      p_ERROR := PKG_PAB_CONSTANTES.v_OK;

      IF p_REG_OPER.COUNT > 0
      THEN
         FOR i IN p_REG_OPER.FIRST .. p_REG_OPER.LAST
         LOOP
            IF p_CANT = 0
            THEN
               p_SALIDA := p_REG_OPER (i).NUM_FOL_OPE;
            ELSE
               p_SALIDA := p_SALIDA || ' - ' || p_REG_OPER (i).NUM_FOL_OPE;
            END IF;

            p_CANT := p_CANT + 1;
         END LOOP;
      END IF;

      --Guardamos el motivo
      v_motivo_eli := p_GLS_MTV_EST;

      --
      PKG_PAB_NOTIFICACION.SP_PAB_DET_CRRO_TIBCO (NULL,
                                                  NULL,
                                                  NULL,
                                                  p_COD_TPO_OPE,
                                                  p_SALIDA, -- Texto con operaciones
                                                  p_CANT,            --Sistema
                                                  NULL,
                                                  NULL,
                                                  NULL,
                                                  p_ADJUNTO,
                                                  p_ID,
                                                  p_MENSAJE_SWF,
                                                  p_ASUNTO,
                                                  p_CORREO,
                                                  p_ERROR);
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := 'No se han encontrado registos asociados a la busqueda';
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_GES_NTF_OPE;

   /************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_GES_NTFCC_ALMOT
   -- Objetivo: Procedimiento almacenado que crea notificaciones solo por pantalla (Dashborad)
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_NTFCC_ALMOT
   --
   -- Fecha: 21/08/17
   -- Autor: Santander CAH
   -- Input:
   --   p_NUM_REF_SWF       Numero de referencia
   --   p_NUM_FOL_OPE       Numero de Operación
   --   p_FEC_ISR_OPE       Fecha de Operación
   --   p_NUM_FOL_NMN       Numero de Nómina
   --  p_COD_TPO_OPE       Tipo de notifiación
   -- Output:
   --   p_ERROR:            indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_GES_NTFCC_ALMOT (p_NUM_REF_SWF   IN     CHAR,
                                     p_NUM_FOL_OPE   IN     NUMBER,
                                     p_FEC_ISR_OPE   IN     DATE,
                                     p_NUM_FOL_NMN   IN     NUMBER,
                                     p_COD_TPO_OPE   IN     CHAR,
                                     p_COD_SIS_ENT   IN     CHAR,
                                     p_ERROR            OUT NUMBER)
   IS
      --
      v_NOM_SP            VARCHAR2 (30) := 'Sp_PAB_GES_NTFCC_ALMOT';
      v_REF_SWIFT_ORI     VARCHAR2 (30);
      v_REF_SWIFT         VARCHAR2 (30);
      v_COD_MT            VARCHAR2 (30);
      v_MONTO_TOTAL       VARCHAR2 (30);
      v_BIC_DESTINO       VARCHAR2 (30);
      v_FECHA_PAGO        VARCHAR2 (30);
      v_BENEFICIARIO      VARCHAR2 (30);
      v_NUM_OPE           VARCHAR2 (30);
      v_NUM_NOM           VARCHAR2 (30) := p_NUM_FOL_NMN;
      v_MONEDA            VARCHAR2 (10);
      v_ID                VARCHAR2 (50);
      v_COD_TPO_ROL_AOS   NUMBER (1) := 2;                              -- Rol
      v_COD_SIS_ENT_SAL   CHAR (10);
      v_MENSAJE_NTF       VARCHAR2 (500);
      v_COD_TPO_OPE       CHAR (5) := p_COD_TPO_OPE;
      v_NUM_FOL_OPE       NUMBER;
      v_FEC_ISR_OPE       DATE;
      v_NUM_FOL_NMN       NUMBER;
      v_IMP_TOT_NMN       VARCHAR2 (30);
      v_COD_EST_NOM       NUMBER;
      v_COD_EST_OPE       NUMBER;
      v_ESTADO            VARCHAR2 (100);
      v_ESTADO_AUT        VARCHAR2 (150);
      v_NUM_REF_SWF       CHAR (16) := p_NUM_REF_SWF;
      v_CANT_AUT          NUMBER;
      v_CANT_PVI          NUMBER;
      v_GLS_MTV_EST       VARCHAR (350);
      ERROR_REFSWF EXCEPTION;
      ERR_COD_INEX EXCEPTION;
   BEGIN
      IF    v_COD_TPO_OPE = 'NERR'
         OR v_COD_TPO_OPE = 'NPRO'
         OR v_COD_TPO_OPE = 'NERV'
         OR v_COD_TPO_OPE = 'CPYN'
      THEN
         BEGIN
            --Busca glosa para correo y adjunto si tiene, también glosa notificación pantalla
            SELECT   REPLACE (GLS_NOT_USR, '%Nom', v_NUM_NOM), 'OOFF'
              INTO   v_MENSAJE_NTF, v_COD_SIS_ENT_SAL
              FROM   PABS_DT_DETLL_CRREO_ALMOT COR
             WHERE   COD_TPO_CRO = v_COD_TPO_OPE;

            v_MONTO_TOTAL := 0;
            v_NUM_OPE := 0;
            v_MENSAJE_NTF := REPLACE (v_MENSAJE_NTF, '%Ope', p_NUM_FOL_OPE);
            v_MENSAJE_NTF := REPLACE (v_MENSAJE_NTF, '%Monto', p_NUM_FOL_NMN);
            v_MENSAJE_NTF := REPLACE (v_MENSAJE_NTF, '%Dia', p_NUM_FOL_NMN);

            PKG_PAB_NOTIFICACION.Sp_PAB_INS_NTFCC_ALMOT (v_COD_TPO_OPE,
                                                         p_COD_SIS_ENT,
                                                         v_COD_TPO_ROL_AOS,
                                                         v_MONTO_TOTAL,
                                                         v_NUM_OPE,
                                                         v_NUM_NOM,
                                                         v_MENSAJE_NTF,
                                                         p_ERROR);

            p_ERROR := PKG_PAB_CONSTANTES.v_OK;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               err_code := SQLCODE;
               err_msg :=
                  'No se pudo obtener la información de la operación origen: '
                  || v_NUM_REF_SWF;
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
               RAISE;
            WHEN OTHERS
            THEN
               err_code := SQLCODE;
               err_msg := SUBSTR (SQLERRM, 1, 300);
               p_s_mensaje := err_code || '-' || err_msg;
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
               RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
         END;
      ELSIF v_COD_TPO_OPE = 'MNCUR' OR v_COD_TPO_OPE = 'MNREC'
      THEN                            --Notificaciones de Mensajeria Protocolo
         BEGIN
            --Rescatamos estado actual
            SELECT   PT.COD_BCO_BFC, PT.GLS_EST_RCH, PT.NUM_REF_SWF
              INTO   v_BIC_DESTINO, v_GLS_MTV_EST, v_NUM_REF_SWF
              FROM   PABS_DT_MNSJE_PRTCZ PT
             WHERE   NUM_FOL_MSJ_PRZ = p_NUM_FOL_OPE
                     AND FEC_ISR_MSJ_PRZ = p_FEC_ISR_OPE;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               err_code := SQLCODE;
               err_msg :=
                  'No se pudo obtener la información de la operación origen:'
                  || p_NUM_FOL_OPE;
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
               RAISE;
            WHEN OTHERS
            THEN
               err_code := SQLCODE;
               err_msg := SUBSTR (SQLERRM, 1, 300);
               p_s_mensaje := err_code || '-' || err_msg;
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
               RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
         END;

         BEGIN
            --Busca glosa para correo y adjunto si tiene, también glosa notificación pantalla
            SELECT   REPLACE (GLS_NOT_USR, '%Nom', v_NUM_NOM),
                     'OOFF',
                     COR.COD_TPO_ROL_AOS
              INTO   v_MENSAJE_NTF, v_COD_SIS_ENT_SAL, v_COD_TPO_ROL_AOS
              FROM   PABS_DT_DETLL_CRREO_ALMOT COR
             WHERE   COD_TPO_CRO = v_COD_TPO_OPE;

            v_MONTO_TOTAL := 0;
            v_NUM_OPE := 0;
            v_NUM_NOM := 0;
            v_MENSAJE_NTF := REPLACE (v_MENSAJE_NTF, '%Ope', p_NUM_FOL_OPE);
            v_MENSAJE_NTF := REPLACE (v_MENSAJE_NTF, '%Banco', v_BIC_DESTINO);
            v_MENSAJE_NTF := REPLACE (v_MENSAJE_NTF, '%Motivo', v_GLS_MTV_EST);

            PKG_PAB_NOTIFICACION.Sp_PAB_INS_NTFCC_ALMOT (v_COD_TPO_OPE,
                                                         v_COD_SIS_ENT_SAL,
                                                         v_COD_TPO_ROL_AOS,
                                                         v_MONTO_TOTAL,
                                                         p_NUM_FOL_OPE,
                                                         v_NUM_NOM,
                                                         v_MENSAJE_NTF,
                                                         p_ERROR);

            p_ERROR := PKG_PAB_CONSTANTES.v_OK;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               err_code := SQLCODE;
               err_msg :=
                  'No se pudo obtener la información de la notificación: '
                  || v_COD_TPO_OPE;
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
               RAISE;
            WHEN OTHERS
            THEN
               err_code := SQLCODE;
               err_msg := SUBSTR (SQLERRM, 1, 300);
               p_s_mensaje := err_code || '-' || err_msg;
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
               RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
         END;
      ELSE
         IF (NVL (p_NUM_REF_SWF, '#') <> '#')
         THEN                          -- AND NVL (p_NUM_FOL_NMN, 0) = 0) THEN
            BEGIN
               --Obtenemos la informacion de la operacion origen
               SELECT   NUM_FOL_OPE, FEC_ISR_OPE, NUM_FOL_NMN
                 INTO   v_NUM_FOL_OPE, v_FEC_ISR_OPE, v_NUM_FOL_NMN
                 FROM   PABS_DT_DETLL_OPRCN DP
                WHERE   NUM_REF_SWF = v_NUM_REF_SWF;

               IF (   v_NUM_FOL_OPE IS NULL
                   OR v_FEC_ISR_OPE IS NULL
                   OR v_NUM_FOL_NMN IS NULL)
               THEN
                  RAISE ERROR_REFSWF;
               END IF;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  err_code := SQLCODE;
                  err_msg :=
                     'No se pudo obtener la información de la operación origen: '
                     || v_NUM_REF_SWF;
                  PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                      v_NOM_PCK,
                                                      err_msg,
                                                      v_NOM_SP);
                  p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
                  RAISE;
               WHEN OTHERS
               THEN
                  err_code := SQLCODE;
                  err_msg := SUBSTR (SQLERRM, 1, 300);
                  p_s_mensaje := err_code || '-' || err_msg;
                  PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                      v_NOM_PCK,
                                                      err_msg,
                                                      v_NOM_SP);
                  p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
                  RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
            END;
         ELSE
            --
            v_NUM_FOL_OPE := p_NUM_FOL_OPE;
            v_FEC_ISR_OPE := p_FEC_ISR_OPE;
            v_NUM_FOL_NMN := p_NUM_FOL_NMN;
         END IF;


         IF v_NUM_FOL_NMN = 0
         THEN
            --Buscamos datos de la operación.
            SELECT   DETOPE.NUM_REF_SWF,
                     DETOPE.NUM_REF_EXT,
                     DETOPE.COD_MT_SWF,
                     DETOPE.IMp_OPE,
                     DETOPE.COD_BCO_DTN,
                     DETOPE.FEC_VTA,
                     DETOPE.NOM_BFC,
                     DETOPE.NUM_FOL_OPE,
                     DETOPE.NUM_FOL_NMN,
                     DETOPE.COD_DVI,
                     DETOPE.NUM_IDF_GTR_DOC,
                     CN.COD_EST_AOS,
                     DETOPE.COD_SIS_ENT,
                     DETOPE.COD_EST_AOS
              INTO   v_REF_SWIFT_ORI,
                     v_REF_SWIFT,
                     v_COD_MT,
                     v_MONTO_TOTAL,
                     v_BIC_DESTINO,
                     v_FECHA_PAGO,
                     v_BENEFICIARIO,
                     v_NUM_OPE,
                     v_NUM_NOM,
                     v_MONEDA,
                     v_ID,
                     v_COD_EST_NOM,
                     v_COD_SIS_ENT_SAL,
                     v_COD_EST_OPE
              FROM   PABS_DT_DETLL_OPRCN DETOPE, PABS_DT_CBCRA_NOMNA CN
             WHERE       DETOPE.NUM_FOL_NMN = CN.NUM_FOL_NMN
                     AND DETOPE.NUM_FOL_OPE = v_NUM_FOL_OPE
                     AND DETOPE.FEC_ISR_OPE = v_FEC_ISR_OPE;
         ELSE
            BEGIN
               SELECT   CN.NUM_FOL_NMN,
                        CN.IMP_TOT_NMN,
                        CN.COD_EST_AOS,
                        CN.GLS_MTV_EST,
                        CN.COD_SIS_ENT
                 INTO   v_NUM_NOM,
                        v_IMP_TOT_NMN,
                        v_COD_EST_NOM,
                        v_GLS_MTV_EST,
                        v_COD_SIS_ENT_SAL
                 FROM   PABS_DT_CBCRA_NOMNA CN, PABS_DT_ESTDO_ALMOT ES
                WHERE   CN.COD_EST_AOS = ES.COD_EST_AOS
                        AND CN.NUM_FOL_NMN = v_NUM_FOL_NMN;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  err_code := SQLCODE;
                  err_msg :=
                     'No se pudo obtener la información de la operación origen: '
                     || v_NUM_FOL_NMN;
                  PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                      v_NOM_PCK,
                                                      err_msg,
                                                      v_NOM_SP);
                  p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
                  RAISE;
               WHEN OTHERS
               THEN
                  err_code := SQLCODE;
                  err_msg := SUBSTR (SQLERRM, 1, 300);
                  p_s_mensaje := err_code || '-' || err_msg;
                  PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                      v_NOM_PCK,
                                                      err_msg,
                                                      v_NOM_SP);
                  p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
                  RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
            END;
         END IF;


         CASE
            WHEN v_COD_EST_NOM = PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNVAL
            THEN                                              -- Nomina Valida
               v_ESTADO := 'válida y lista para cursar';
            WHEN v_COD_EST_NOM = PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNINV
            THEN                                             --Nomina Invalida
               v_ESTADO := 'inválida';
            WHEN V_COD_EST_NOM = PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNCUR
            THEN                                              --Nomina Cursada
               v_ESTADO := 'Nómina cursada';
               v_COD_TPO_ROL_AOS := 1;
            WHEN v_COD_EST_NOM = PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNAUT
            THEN                                          -- Nomina Autorizada
               IF v_COD_TPO_OPE <> 'OPAG'
               THEN
                  IF v_NUM_FOL_OPE >= 1
                  THEN
                     v_COD_TPO_OPE := 'NAUT';
                     v_ESTADO_AUT :=
                        ':' || v_NUM_FOL_OPE || ' Operaciones Por Visar';
                  END IF;
               END IF;
            WHEN V_COD_EST_NOM = PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNREC
            THEN                                           -- Nomina Rechazada
               v_ESTADO := 'rechazada';
            WHEN V_COD_EST_NOM = PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNELI
            THEN                                           -- Nomina Eliminada
               v_ESTADO := 'eliminada';
            WHEN V_COD_EST_NOM = PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNDUP
            THEN                                           -- Nomina Duplicada
               v_ESTADO := 'duplicada';
            WHEN V_COD_EST_NOM = PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAG
            THEN                                           -- Operación pagada
               v_ESTADO := 'Operación Pagada';
            ELSE
               RAISE ERR_COD_INEX;
         END CASE;

         BEGIN
            --Por el momento se seguira ocupando la tabla de correos.
            --Busca glosa para correo y adjunto si tiene, también glosa notificación pantalla
            SELECT   REPLACE (GLS_NOT_USR, '%Nom', v_NUM_NOM)
              INTO   v_MENSAJE_NTF
              FROM   PABS_DT_DETLL_CRREO_ALMOT COR
             WHERE   COD_TPO_CRO = v_COD_TPO_OPE;

            v_MENSAJE_NTF :=
               REPLACE (v_MENSAJE_NTF, '%Ope', NVL (v_NUM_OPE, ' _ '));
            v_MENSAJE_NTF :=
               REPLACE (v_MENSAJE_NTF, '%Ref', NVL (v_REF_SWIFT_ORI, ' _ '));
            v_MENSAJE_NTF :=
               REPLACE (v_MENSAJE_NTF, '%Monto', NVL (v_MONTO_TOTAL, ' - '));
            v_MENSAJE_NTF :=
               REPLACE (v_MENSAJE_NTF,
                        '%MontoNom',
                        NVL (v_IMP_TOT_NMN, ' - '));
            v_MENSAJE_NTF :=
               REPLACE (v_MENSAJE_NTF, '%Moneda', NVL (v_MONEDA, ' -'));
            v_MENSAJE_NTF := REPLACE (v_MENSAJE_NTF, '%BcoDtn', v_BIC_DESTINO);
            v_MENSAJE_NTF := REPLACE (v_MENSAJE_NTF, '%CantOpe', v_CANT_AUT);
            v_MENSAJE_NTF := REPLACE (v_MENSAJE_NTF, '%Motivo', v_GLS_MTV_EST);
            v_MENSAJE_NTF := REPLACE (v_MENSAJE_NTF, '%Estado', v_ESTADO);
            v_MENSAJE_NTF := REPLACE (v_MENSAJE_NTF, '%LibVis', v_ESTADO_AUT);
            v_MENSAJE_NTF := REPLACE (v_MENSAJE_NTF, '%Error', v_NUM_FOL_OPE);

            --Llama a procedimietno para generar notificación
            PKG_PAB_NOTIFICACION.Sp_PAB_INS_NTFCC_ALMOT (v_COD_TPO_OPE --   (p_COD_TIP_CRO,
                                                                      ,
                                                         v_COD_SIS_ENT_SAL --   p_COD_SIS_ENT_SAL,
                                                                          ,
                                                         v_COD_TPO_ROL_AOS --   p_COD_TPO_ROL_AOS,
                                                                          ,
                                                         v_MONTO_TOTAL --   p_IMP_OPE,
                                                                      ,
                                                         v_NUM_OPE --   p_NUM_FOL_OPE,
                                                                  ,
                                                         v_NUM_NOM --   p_NUM_FOL_NOM,
                                                                  ,
                                                         v_MENSAJE_NTF --   p_GLS_MTV_NTF,
                                                                      ,
                                                         p_ERROR);

            p_ERROR := PKG_PAB_CONSTANTES.v_OK;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               err_code := SQLCODE;
               err_msg :=
                     SUBSTR (SQLERRM, 1, 300)
                  || 'No hay data para la operacion '
                  || p_COD_TPO_OPE;
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            WHEN OTHERS
            THEN
               err_code := SQLCODE;
               err_msg := SUBSTR (SQLERRM, 1, 300);
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_s_mensaje := err_code || '-' || err_msg;
               RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         END;
      END IF;
   END Sp_PAB_GES_NTFCC_ALMOT;

   --***********************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_BUS_CAN_OPE_EST_AOS
   -- Objetivo: Retorna cantidad de operaciones por estado
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA
   -- Fecha:21/08/2017
   -- Autor: Santander-CAH
   -- Input:
   -- P_COD_SIS_ENT_SAL -> CODIGO SISTEMA ENTRADA SALIDA (CANAL)
   -- N/A
   -- Output:
   -- N/A.
   -- Input/Output:
   -- N/A.
   -- Retorno:
   -- V_CANT_LIB -> valor de retorno que muestra el horario de inicio segun el area
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   --***********************************************************************************************
   FUNCTION FN_PAB_BUS_CAN_OPE_EST_AOS (P_NUM_FOL_NMN      NUMBER,
                                        P_COD_EST_AOS   IN NUMBER)
      RETURN NUMBER
   IS
      V_NOM_SP     VARCHAR2 (30) := 'FN_PAB_BUS_CAN_OPE_EST_AOS';
      V_CANT_OPE   NUMBER (10);
   BEGIN
      SELECT   COUNT (1)
        INTO   v_CANT_OPE
        FROM   PABS_DT_DETLL_NOMNA DN
       WHERE   DN.NUM_FOL_NMN = P_NUM_FOL_NMN                               --
               AND DN.COD_EST_AOS = P_COD_EST_AOS;                          --

      --
      RETURN V_CANT_OPE;
   --
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg :=
               SUBSTR (SQLERRM, 1, 300)
            || 'No hay data para la operacion '
            || P_NUM_FOL_NMN
            || '- estado '
            || P_COD_EST_AOS;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END FN_PAB_BUS_CAN_OPE_EST_AOS;


   /******************************************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_GEN_NTFCC
   -- Objetivo: Procedimiento que gestiona Notificaciones Generales
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN, PABS_DT_ESTDO_ALMOT
   --
   -- Fecha: 04/07/17
   -- Autor: Santander CAH
   -- Input:
   -- NO APLICA
   -- Output:
   --       p_CURSOR  Operacion con notificaciones.
   --       p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --******************************************************************************************************************************/
   PROCEDURE Sp_PAB_GEN_NTFCC (p_CURSOR   OUT SYS_REFCURSOR,
                               p_ERROR    OUT NUMBER)
   IS
      v_NOM_SP            VARCHAR2 (30) := 'Sp_PAB_BUS_OPE_SIN_CBO';
      v_COD_TPO_CRO_ANS   CHAR (5) := 'NANS';
      v_COD_TPO_CRO_SUP   CHAR (5) := 'NSUP';
   BEGIN
      OPEN p_CURSOR FOR
           SELECT      'EXISTEN '
                    || COUNT (A.NUM_FOL_OPE)
                    || ' OPERACION(ES) EN ESTADO '
                    || TRIM (UPPER (B.DSC_EST_AOS))
                    || '. FAVOR ATENDER'
                       AS "MENSAJE",
                    PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SE_OOFF AS "AREA",
                    CASE
                       WHEN A.COD_EST_AOS IN (8, 9, 11) THEN v_COD_TPO_CRO_SUP
                       ELSE v_COD_TPO_CRO_ANS
                    END
                       AS "TIPO_NOTIFICACION",
                    CASE WHEN A.COD_EST_AOS IN (8, 9, 11) THEN '1' ELSE '2' END
                       AS "PERFIL",
                    CASE
                       WHEN A.COD_EST_AOS IN (8, 9, 11)
                       THEN
                             'ESTIMADO SUPERVISOR: EXISTEN '
                          || COUNT (A.NUM_FOL_OPE)
                          || ' OPERACION(ES) EN ESTADO '
                          || TRIM (UPPER (B.DSC_EST_AOS))
                          || '. FAVOR ATENDER. %0aMensajeria Altos Montos'
                       ELSE
                             'ESTIMADO ANALISTA: EXISTEN '
                          || COUNT (A.NUM_FOL_OPE)
                          || ' OPERACION(ES) EN ESTADO '
                          || TRIM (UPPER (B.DSC_EST_AOS))
                          || '. FAVOR ATENDER. %0aMensajeria Altos Montos'
                    END
                       AS "SMS",
                    CASE
                       WHEN A.COD_EST_AOS IN (8, 9, 11)
                       THEN
                          (SELECT   LISTAGG (TRIM (COD_USR_ULT_MOD), ';')
                                       WITHIN GROUP (ORDER BY COD_USR_ULT_MOD)
                             FROM   DBO_PAB.PABS_DT_USRIO
                            WHERE   COD_SIS_ENT =
                                       PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SE_OOFF
                                    AND COD_TPO_ROL_AOS = 1)
                       ELSE
                          (SELECT   LISTAGG (TRIM (COD_USR_ULT_MOD), ';')
                                       WITHIN GROUP (ORDER BY COD_USR_ULT_MOD)
                             FROM   DBO_PAB.PABS_DT_USRIO
                            WHERE   COD_SIS_ENT =
                                       PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SE_OOFF
                                    AND COD_TPO_ROL_AOS = 2)
                    END
                       AS "TELEFONO"
             FROM   PABS_DT_DETLL_OPRCN A, PABS_DT_ESTDO_ALMOT B
            WHERE       A.COD_EST_AOS IN (10, 13, 8, 9, 11)
                    AND A.COD_EST_AOS = B.COD_EST_AOS
                    AND FEC_VTA = SYSDATE              -- Cambiar a definitivo
                    AND (SYSDATE - FEC_ACT_OPE) >= 0.00710648148148148 -- 1O MINUTOS
         GROUP BY   A.COD_EST_AOS, B.DSC_EST_AOS;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg :=
            SUBSTR (SQLERRM, 1, 300)
            || 'NO EXISTEN NOTIFICACIONES PARA ENVIAR';
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         p_ERROR := PKG_PAB_CONSTANTES.V_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_GEN_NTFCC;
END PKG_PAB_NOTIFICACION;