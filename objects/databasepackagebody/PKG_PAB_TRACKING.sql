CREATE OR REPLACE PACKAGE BODY         PKG_PAB_TRACKING
IS
   /**  Package el cual contrendra todas las funciones y procedimientos relacionados con la trazabilidad del sistem
   -- @everis */


   /************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_INS_LOG_BD
   -- Objetivo: El procedimiento insertara en la tabla PABS_DT_BTCOR_BASE_DATO_ALMOT la secuencia de acciones o
   --               instrucciones que ocurran en la base de datos de altos montos.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas:   PABS_DT_BTCOR_PRCSO
   -- Fecha: 02/06/16
   -- Autor: EVERIS
   -- Input:  p_COD_ERR_BSE_DTS-> Se puede almacenar el codigo interno de la base de datos o el error de oracle.
   --         p_NOM_PCK-> Nombre del package de base de datos.
   --         p_GLS_BTC_BSE_DTS-> Utilizado para mostrar informacion o guardar errores que suceden durante una ejecucion de un procedimiento.
   --         p_NOM_PCM-> Nombre del prodecimiento de la base de datos.
   -- Output:
   --   N/A
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_INS_LOG_BD (p_COD_ERR_BSE_DTS   IN CHAR,
                                p_NOM_PCK           IN VARCHAR2,
                                p_GLS_BTC_BSE_DTS   IN VARCHAR2,
                                p_NOM_PCM           IN CHAR)
   IS
      PRAGMA AUTONOMOUS_TRANSACTION; -- En caso de roolback no afectara a los insert realizados en este procedimiento.
      v_NOM_SP                VARCHAR2 (30) := 'Sp_PAB_INS_LOG_BD';
      v_FEC_ISR_BTC_BSE_DTS   TIMESTAMP;
   BEGIN
      --Obtenemos el tiempo para pa PK
      V_FEC_ISR_BTC_BSE_DTS := SYSTIMESTAMP;

      INSERT INTO PABS_DT_BTCOR_BASE_DATO_ALMOT (COD_ERR_BSE_DTS,
                                                 NOM_PCK,
                                                 FEC_ISR_BTC_BSE_DTS,
                                                 GLS_BTC_BSE_DTS,
                                                 NOM_PCM)
        VALUES   (p_COD_ERR_BSE_DTS,
                  p_NOM_PCK,
                  v_FEC_ISR_BTC_BSE_DTS,
                  p_GLS_BTC_BSE_DTS,
                  p_NOM_PCM);

      COMMIT; -- Este commit solo afecta a lo que sucede en este procedimiento.
   EXCEPTION
      WHEN DUP_VAL_ON_INDEX
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);

         INSERT INTO PABS_DT_BTCOR_BASE_DATO_ALMOT (COD_ERR_BSE_DTS,
                                                    NOM_PCK,
                                                    FEC_ISR_BTC_BSE_DTS,
                                                    GLS_BTC_BSE_DTS,
                                                    NOM_PCM)
           VALUES   (err_code,
                     v_NOM_PCK,
                     SYSTIMESTAMP,
                     err_msg,
                     v_NOM_SP);

         COMMIT; -- Este commit solo afecta a lo que sucede en este procedimiento.
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_INS_LOG_BD;


   /************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_INS_BIT_CAB_PROCESO
   -- Objetivo: Procedimiento que incerta la informacion principal de la cabecera de los procesos.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_BTCOR_DETLL_PRCSO
   -- Fecha: 02/06/16
   -- Autor: EVERIS
   -- Input:  p_COD_TPO_PSC-> Identificador del proceso que utilizara la grabacion de traza
   -- Output:
   --    p_FEC_ICO_PCS: Fecha inicio de proceso con milisegundos
   --          p_NUM_TCK_PCS: Numero de ticket del proceso, el cual sirve como seguimiento para el detalle del tracking
   --          p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_INS_BIT_CAB_PROCESO (p_COD_TPO_PSC   IN     CHAR,
                                         p_FEC_ICO_PCS      OUT VARCHAR2,
                                         p_NUM_TCK_PCS      OUT NUMBER,
                                         p_ERROR            OUT NUMBER)
   IS
      PRAGMA AUTONOMOUS_TRANSACTION; -- En caso de roolback no afectara a los insert realizados en este procedimiento.
      v_NOM_SP        VARCHAR2 (30) := 'Sp_PAB_INS_BIT_CAB_PROCESO';
      v_FEC_ICO_PCS   TIMESTAMP (6) := SYSTIMESTAMP;
      v_NUM_TCK_PCS   NUMBER (12);
   BEGIN
      --Obtenemos el numero de ticket que representara el proceso
      v_NUM_TCK_PCS := SPK_PABS_DT_BTCOR_PRCSO.NEXTVAL;

      /**INSERTA BITACORA CABECERA DE PROCESO*/
      INSERT INTO PABS_DT_BTCOR_PRCSO (FEC_ICO_PCS,
                                       COD_TPO_PSC,
                                       COD_EST_AOS,
                                       NUM_TCK_PCS)
        VALUES   (v_FEC_ICO_PCS,
                  p_COD_TPO_PSC,
                  PKG_PAB_CONSTANTES.v_COD_EST_AOS_EJECU,
                  v_NUM_TCK_PCS);

      --Almacenamos la fecha la cual fue insertado el registro para devolverlo como parametro salida
      p_FEC_ICO_PCS := TO_CHAR (v_FEC_ICO_PCS, 'dd-mm-yyyy hh24:mi:ss.FF6');

      /*Almacenamos el numero de ticket el cual fue insertado el registro para
      devolverlo como parametro salida*/
      p_NUM_TCK_PCS := v_NUM_TCK_PCS;

      --Procedimiento ejecutado de forma correcta
      p_ERROR := PKG_PAB_CONSTANTES.v_OK;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;

         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_INS_BIT_CAB_PROCESO;

   /************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_INS_BIT_DET_PROCESO
   -- Objetivo: Procedimiento que inserta el detalle de los procesos ejecutados en el sistema
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_BTCOR_OPRCN
   -- Fecha: 02/06/16
   -- Autor: EVERIS
   -- Input:  p_GLS_DET_PCS    : Glosa con informacion del detalle del proceso
   --         p_FEC_ICO_PCS    : Fecha de inicio del proceso
   --         p_COD_ERR_BIB    : Codigo de error de la Bitacora. (Es completado cuando existe error)
   --         p_COD_TPO_PSC    : Identificador del proceso
   -- Output:
   --    p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS   IN     VARCHAR2,
                                         p_FEC_ICO_PCS   IN     VARCHAR2,
                                         p_COD_ERR_BIB   IN     CHAR,
                                         p_COD_TPO_PSC   IN     CHAR,
                                         p_ERROR            OUT NUMBER)
   IS
      v_NOM_SP   VARCHAR2 (30) := 'Sp_PAB_INS_BIT_DET_PROCESO';
      PRAGMA AUTONOMOUS_TRANSACTION; -- En caso de roolback no afectara a los insert realizados en este procedimiento.
   BEGIN
      /*INSERTA DETALLE PROCESO*/
      INSERT INTO PABS_DT_BTCOR_DETLL_PRCSO (FEC_ISR_DET_BTC,
                                             GLS_DET_PCS,
                                             FEC_ICO_PCS,
                                             COD_ERR_BIB,
                                             COD_TPO_PSC)
        VALUES   (SYSTIMESTAMP,
                  p_GLS_DET_PCS,
                  TO_TIMESTAMP (p_FEC_ICO_PCS, 'DD-MM-YYYY HH24:MI:SS.FF6'),
                  p_COD_ERR_BIB,
                  p_COD_TPO_PSC);

      --Variable de salida--
      p_ERROR := PKG_PAB_CONSTANTES.v_OK; --Procedimiento ejecutado de forma correcta

      COMMIT; -- Este commit solo afecta a lo que sucede en este procedimiento.
   EXCEPTION
      WHEN DUP_VAL_ON_INDEX
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg :=
               SUBSTR (SQLERRM, 1, 300)
            || 'p_GLS_DET_PCS: '
            || p_GLS_DET_PCS
            || ' p_FEC_ICO_PCS:'
            || p_FEC_ICO_PCS
            || ' p_COD_ERR_BIB:'
            || p_COD_ERR_BIB
            || ' p_COD_TPO_PSC:'
            || p_COD_TPO_PSC;

         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_INS_BIT_DET_PROCESO;

   /************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_UPD_BIT_CAB_PROCESO
   -- Objetivo: Procedimiento que actualiza informacion de la cabecera del proceso.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_BTCOR_BASE_DATO_ALMOT
   -- Fecha: 02/06/16
   -- Autor: EVERIS
   -- Input:  p_FEC_ICO_PCS    : Fecha de inicio del proceso
   --         p_COD_TPO_PSC    : Identificador del proceso
   --          p_NUM_TCK_PCS    : Numero de ticket del proceso, el cual sirve como seguimiento para el detalle del tracking
   --          p_ERROR_PROCESO  : Identifica si el proceso ejecutado termino de forma correcta o con error.
   -- Output:
   --    p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_UPD_BIT_CAB_PROCESO (p_FEC_ICO_PCS     IN     VARCHAR2,
                                         p_COD_TPO_PSC     IN     CHAR,
                                         p_NUM_TCK_PCS     IN     NUMBER,
                                         p_ERROR_PROCESO   IN     NUMBER,
                                         p_ERROR              OUT NUMBER)
   IS
      --Seteo de Variables
      v_NOM_SP        VARCHAR2 (30) := 'Sp_PAB_UPD_BIT_CAB_PROCESO';
      v_COD_EST_AOS   NUMBER (5);
      v_NUM_TCK_PCS   NUMBER (12);
      PRAGMA AUTONOMOUS_TRANSACTION; -- En caso de roolback no afectara a los insert realizados en este procedimiento.
   BEGIN
      --Verificamos como termino el proceso.
      IF p_ERROR_PROCESO = PKG_PAB_CONSTANTES.v_OK
      THEN
         v_COD_EST_AOS := PKG_PAB_CONSTANTES.v_COD_EST_AOS_FINOK;
      ELSE
         v_COD_EST_AOS := PKG_PAB_CONSTANTES.v_COD_EST_AOS_FINER;
      END IF;

      /*ACTUALIZA BITACORA DE PROCESOS*/
      UPDATE   PABS_DT_BTCOR_PRCSO
         SET   FEC_FIN_PCS = SYSDATE, COD_EST_AOS = v_COD_EST_AOS
       WHERE   FEC_ICO_PCS =
                  TO_TIMESTAMP (p_FEC_ICO_PCS, 'DD-MM-YYYY HH24:MI:SS.FF6')
               AND COD_TPO_PSC = p_COD_TPO_PSC
               AND NUM_TCK_PCS = p_NUM_TCK_PCS;

      p_ERROR := PKG_PAB_CONSTANTES.v_OK; --Procedimiento ejecutado de forma correcta

      COMMIT; -- Este commit solo afecta a lo que sucede en este procedimiento.
   EXCEPTION
      WHEN DUP_VAL_ON_INDEX
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_UPD_BIT_CAB_PROCESO;

   /************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_INS_BIT_OPE
   -- Objetivo: Procedimiento que inserta en la bitacora de operaciones las acciones de los usuarios
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_BTCOR_PRCSO
   -- Fecha: 02/06/16
   -- Autor: EVERIS
   -- Input:  p_NUM_FOL_OPE    : Numero de folio de la operacion a la cual se esta registrando un cambio
   --         p_FEC_ISR_OPE    : Fecha de insercion de la operacion a la cual se esta registrando un cambio
   --         p_COD_USR        : Identificador del usuario que realiza la accion
   --         p_COD_EST_ICO    : Estado inicial de la operacion - Antes de la accion
   --         p_COD_EST_FIN    : Estado fin de la operacion - Despues de la accion
   --         p_DSC_GLB_BTC    : Informacion adicional de la accion realizada
   -- Output:
   --    p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_INS_BIT_OPE (p_NUM_FOL_OPE   IN     NUMBER,
                                 p_FEC_ISR_OPE   IN     TIMESTAMP,
                                 p_COD_USR       IN     CHAR,
                                 p_COD_EST_ICO   IN     NUMBER,
                                 p_COD_EST_FIN   IN     NUMBER,
                                 p_DSC_GLB_BTC   IN     VARCHAR2,
                                 p_ERROR            OUT NUMBER)
   IS
      --Seteo de Variables
      v_NOM_SP        VARCHAR2 (30) := 'Sp_PAB_INS_BIT_OPE';
      v_FEC_ISR_BTC   TIMESTAMP (6) := SYSTIMESTAMP;
      ERR_ESTADO EXCEPTION;
   BEGIN
      --Verificamos que no vengan vacios los estados
      IF (p_COD_EST_ICO IS NULL OR p_COD_EST_FIN IS NULL)
      THEN
         RAISE ERR_ESTADO;
      END IF;

      --Insertamos en la bitacora de operacion
      INSERT INTO PABS_DT_BTCOR_OPRCN (FEC_ISR_BTC,
                                       NUM_FOL_OPE,
                                       FEC_ISR_OPE,
                                       COD_USR,
                                       COD_EST_ICO,
                                       COD_EST_FIN,
                                       DSC_GLB_BTC)
        VALUES   (v_FEC_ISR_BTC,
                  p_NUM_FOL_OPE,
                  p_FEC_ISR_OPE,
                  p_COD_USR,
                  p_COD_EST_ICO,
                  p_COD_EST_FIN,
                  p_DSC_GLB_BTC);

      p_ERROR := PKG_PAB_CONSTANTES.v_OK; --Procedimiento ejecutado de forma correcta
   EXCEPTION
      WHEN ERR_ESTADO
      THEN
         err_code := SQLCODE;
         err_msg :=
               'Vienen vacio algun estado -> Inicio:'
            || p_COD_EST_ICO
            || ' Fin:'
            || p_COD_EST_FIN;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN DUP_VAL_ON_INDEX
      THEN
         err_msg :=
               'Insertar:'
            || p_NUM_FOL_OPE
            || ' - '
            || p_FEC_ISR_OPE
            || ' - '
            || p_COD_EST_ICO
            || ' - '
            || p_COD_EST_FIN
            || ' - '
            || p_DSC_GLB_BTC;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);

         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_msg :=
               'Insertar:'
            || p_NUM_FOL_OPE
            || ' - '
            || p_FEC_ISR_OPE
            || ' - '
            || p_COD_EST_ICO
            || ' - '
            || p_COD_EST_ICO
            || ' - '
            || p_COD_EST_FIN
            || ' - '
            || p_DSC_GLB_BTC;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);

         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_INS_BIT_OPE;
END PKG_PAB_TRACKING;