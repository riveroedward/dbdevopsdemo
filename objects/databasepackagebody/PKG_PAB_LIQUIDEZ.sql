CREATE OR REPLACE PACKAGE BODY         PKG_PAB_LIQUIDEZ
IS
   /**********************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_GES_LIQDZ_ITDIA
   -- Objetivo: Procedimiento almacenado gestor que se ejecuta cada minuto para el resgitro
   --            de saldos necesarios de Liquidez Intradia
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas:
   --
   -- Fecha: 03-09-2018
   -- Autor: Santander CAH
   -- Input:
   --
   --
   -- Output:
   --      p_ALERTA  Indicador de alerta
   --      p_ASUNTO  Asunto del mensaje
   --      p_CORREO  Correo de destino
   --      p_MENSAJE Mensaje de alerta
   --       p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --*********************************************************************************************************/
   PROCEDURE SP_PAB_GES_LIQDZ_ITDIA (p_ALERTA    OUT NUMBER,
                                     p_ASUNTO    OUT VARCHAR2,
                                     p_CORREO    OUT VARCHAR2,
                                     p_MENSAJE   OUT VARCHAR2,
                                     p_ERROR     OUT NUMBER)
   IS
      v_NOM_SP        VARCHAR2 (30) := 'SP_PAB_GES_LIQDZ_ITDIA';
      v_NOM_PROCESO   VARCHAR2 (22);
      v_HORA_INICIO   NUMBER := '800';
      v_HORA_ACTUAL   NUMBER := TO_NUMBER (TO_CHAR (SYSDATE, 'HH24MI'));
      err_perror exception;
   BEGIN
      IF v_HORA_ACTUAL >= v_HORA_INICIO
      THEN
         -- Sp inserta - Por Minuto
         PKG_PAB_LIQUIDEZ.SP_PAB_REC_SALDO_ITDIA (p_ERROR);

         --En caso de error, no continuar
         IF (p_ERROR = PKG_PAB_CONSTANTES.v_ERROR)
         THEN
            v_NOM_PROCESO := 'Registro por minuto';
            RAISE err_perror;
         END IF;

         -- Sp inserta - Por Hora
         PKG_PAB_LIQUIDEZ.SP_PAB_REC_PAGOS_ACMDO (p_ERROR);

         --En caso de error, no continuar
         IF (p_ERROR = PKG_PAB_CONSTANTES.v_ERROR)
         THEN
            v_NOM_PROCESO := 'Registro por Hora';
            RAISE err_perror;
         END IF;

         --Agregar validacion de existencia de cartola nacional !!!
         -- Para que no se genere traza y se muestre una notifcacion.

         --Registros de Saldos Historicos--
         PKG_PAB_LIQUIDEZ.SP_PAB_REC_SALDO_LBTR (p_ERROR);
         PKG_PAB_LIQUIDEZ.SP_PAB_REC_SALDO_CMN (p_ERROR);
         PKG_PAB_LIQUIDEZ.SP_PAB_INS_SALDO_LIQDZ_BCCH (p_ERROR);
         PKG_PAB_LIQUIDEZ.SP_PAB_REC_SALDO_MX (p_ERROR);


         --Alerta de notidficacion saldos POR CORREO ....
         PKG_PAB_LIQUIDEZ.SP_PAB_REC_ALERTA_MONEDA (p_ALERTA,
                                                    p_ASUNTO,
                                                    p_CORREO,
                                                    p_MENSAJE,
                                                    p_ERROR);

         IF p_ERROR = 1
         THEN
            p_ALERTA := 0;
            p_ASUNTO := '-';
            p_CORREO := '-';
            p_MENSAJE := '#';



            COMMIT;
         ELSE
            --No enviar alerta po horario
            p_ALERTA := 0;
            p_ASUNTO := '-';
            p_CORREO := '-';
            p_MENSAJE := '#';
         END IF;



         p_ERROR := PKG_PAB_CONSTANTES.v_OK;
      END IF;
   EXCEPTION
      WHEN err_perror
      THEN
         err_code := SQLCODE;
         err_msg :=
            'Se detiene proceso de insercion por error interno :'
            || v_NOM_PROCESO;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_ALERTA := 0;
         p_ASUNTO := '-';
         p_CORREO := '-';
         p_MENSAJE := '#';
      WHEN OTHERS
      THEN
         ROLLBACK;
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_ALERTA := 0;
         p_ASUNTO := '-';
         p_CORREO := '-';
         p_MENSAJE := '#';
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_GES_LIQDZ_ITDIA;

   /*******************************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_REC_SALDO_ITDIA
   -- Objetivo: Procedimiento almacenado  que recorre las monedas y llamando a otros SP para insercion de data
         en las tablas que contienen la data de graficos de Liquidez Intradia
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas:
   --
   -- Fecha: 24-07-2018
   -- Autor: Santander CAH
   -- Input:
   --
   --
   -- Output:
   --
   --       p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --*****************************************************************************************************************/
   PROCEDURE SP_PAB_REC_SALDO_ITDIA (p_ERROR OUT NUMBER)
   IS
      v_NOM_SP            VARCHAR2 (30) := 'SP_PAB_REC_SALDO_ITDIA';
      v_ING_BCCH          NUMBER;
      v_EGR_BCCH          NUMBER;
      v_SALDO_BBCH        NUMBER;
      v_INGRESO_PROY      NUMBER;
      v_EGRESO_PROY       NUMBER;
      v_NETO_REAL         NUMBER;
      v_NETO_PROY         NUMBER;

      --*********************Declaracion Cursor C_CONSULTA_MONEDA************************
      CURSOR C_CONSULTA_MONEDA
      IS
           SELECT   COD_DVI
             FROM   PABS_DT_DETLL_SALDO_CAJA_MN_MX DS
            WHERE   DS.FEC_ING_CTL < v_FECHA_HOY
         GROUP BY   COD_DVI
         ORDER BY   COD_DVI;

      R_CONSULTA_MONEDA   C_CONSULTA_MONEDA%ROWTYPE;
   --*********************Declaracion Cursor C_CONSULTA_BANCOS************************

   BEGIN
      BEGIN
         OPEN C_CONSULTA_MONEDA;
      EXCEPTION
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            RAISE_APPLICATION_ERROR (-20000, err_msg);
      END;

      BEGIN
         FETCH C_CONSULTA_MONEDA INTO   R_CONSULTA_MONEDA;
      EXCEPTION
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);

            --Verificamos si el cursor se encuentra abierto en caso de error.
            IF (C_CONSULTA_MONEDA%ISOPEN)
            THEN
               CLOSE C_CONSULTA_MONEDA;
            END IF;

            RAISE_APPLICATION_ERROR (-20000, err_msg);
      END;

      WHILE C_CONSULTA_MONEDA%FOUND
      LOOP
         --Se registra un nuevo saldo por moneda
         PKG_PAB_LIQUIDEZ.Sp_PAB_INS_SALDO_POR_MONDA (
            R_CONSULTA_MONEDA.COD_DVI,
            p_ERROR
         );

         FETCH C_CONSULTA_MONEDA INTO   R_CONSULTA_MONEDA;
      END LOOP;

      CLOSE C_CONSULTA_MONEDA;
   EXCEPTION
      WHEN OTHERS
      THEN
         --Verificamos si el cursor se encuentra abierto en caso de error.
         IF (C_CONSULTA_MONEDA%ISOPEN)
         THEN
            CLOSE C_CONSULTA_MONEDA;
         END IF;

         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_REC_SALDO_ITDIA;

   /***************************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_REC_PAGOS_ACMDO
   -- Objetivo: Procedimiento almacenado recorre las moneda  para insertar los pagos acumulados
              por hora.
   -- Sistema: DBO_PAB.
   -- Base de Datos:   DBO_PAB.
   -- Tablas Usadas:   PABS_DT_RGTRO_PAGOS_ACMDO
   --                  PABS_DT_DETLL_SALDO_CAJA_MN_MX
   --
   -- Fecha: 08/08/2018
   -- Autor: Santander CAH
   -- Input:
   -- Output:
   --       p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --**************************************************************************************************************/
   PROCEDURE SP_PAB_REC_PAGOS_ACMDO (p_ERROR OUT NUMBER)
   IS
      v_NOM_SP            VARCHAR2 (30) := 'SP_PAB_REC_PAGOS_ACMDO';
      v_ING_BCCH          NUMBER;
      v_EGR_BCCH          NUMBER;
      v_SALDO_BBCH        NUMBER;
      v_INGRESO_PROY      NUMBER;
      v_EGRESO_PROY       NUMBER;
      v_NETO_REAL         NUMBER;
      v_NETO_PROY         NUMBER;

      --*********************Declaracion Cursor C_CONSULTA_MONEDA************************
      CURSOR C_CONSULTA_MONEDA
      IS
           SELECT   COD_DVI
             FROM   PABS_DT_DETLL_SALDO_CAJA_MN_MX DS
            WHERE   DS.FEC_ING_CTL <= v_FECHA_HOY
         GROUP BY   COD_DVI
         ORDER BY   COD_DVI;

      R_CONSULTA_MONEDA   C_CONSULTA_MONEDA%ROWTYPE;
   --*********************Declaracion Cursor C_CONSULTA_BANCOS************************

   BEGIN
      BEGIN
         OPEN C_CONSULTA_MONEDA;
      EXCEPTION
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            RAISE_APPLICATION_ERROR (-20000, err_msg);
      END;

      BEGIN
         FETCH C_CONSULTA_MONEDA INTO   R_CONSULTA_MONEDA;
      EXCEPTION
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);

            --Verificamos si el cursor se encuentra abierto en caso de error.
            IF (C_CONSULTA_MONEDA%ISOPEN)
            THEN
               CLOSE C_CONSULTA_MONEDA;
            END IF;

            RAISE_APPLICATION_ERROR (-20000, err_msg);
      END;

      WHILE C_CONSULTA_MONEDA%FOUND
      LOOP
         --Se registra un nuevo saldo por moneda
         PKG_PAB_LIQUIDEZ.SP_PAB_INS_PAGOS_ACMDO_MONDA (
            R_CONSULTA_MONEDA.COD_DVI,
            p_ERROR
         );

         FETCH C_CONSULTA_MONEDA INTO   R_CONSULTA_MONEDA;
      END LOOP;

      CLOSE C_CONSULTA_MONEDA;
   EXCEPTION
      WHEN OTHERS
      THEN
         --Verificamos si el cursor se encuentra abierto en caso de error.
         IF (C_CONSULTA_MONEDA%ISOPEN)
         THEN
            CLOSE C_CONSULTA_MONEDA;
         END IF;

         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_REC_PAGOS_ACMDO;

   /*****************************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_INS_SALDO_POR_MONDA
   -- Objetivo: Procedimiento almacenado que registra saldos intradia por moneda, e insertando los registros
             a medida que recibe la moneda
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX
   --                PABS_DT_RGTRO_SALDO_ITDIA
   --
   -- Fecha: 07/08/2018
   -- Autor: Santander CAH
   -- Input:
   --      p_COD_DVI      Moneda para actualizar.
   -- Output:
   --       p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***************************************************************************************************************/
   PROCEDURE Sp_PAB_INS_SALDO_POR_MONDA (p_COD_DVI   IN     CHAR,
                                         p_ERROR        OUT NUMBER)
   IS
      v_NOM_SP         VARCHAR2 (30) := 'Sp_PAB_INS_SALDO_POR_MONDA';
      v_ING_BCCH       NUMBER;
      v_EGR_BCCH       NUMBER;
      v_SALDO_BBCH     NUMBER;
      v_INGRESO_PROY   NUMBER;
      v_EGRESO_PROY    NUMBER;
      v_NETO_REAL      NUMBER;
      v_NETO_PROY      NUMBER;
      v_PROY_TOT_24    NUMBER;
      v_PROY_TOT_48    NUMBER;
      v_HORA_ACTUAL    NUMBER := TO_CHAR (SYSDATE, 'HH24MI');
      v_MONEDA         PABS_DT_MONDA_ALMOT.COD_DVI%TYPE;
      v_CANTIDAD       NUMBER;
   BEGIN
      BEGIN                                  -- Data  para insertar los saldos
           SELECT   SL.COD_DVI,
                    (  SUM (SL.IMP_ING_DIA)
                     + SUM (SL.IMP_ABN_BCT)
                     + SUM (SL.IMP_ING_CBA)),                       --Ingresos
                    (  SUM (SL.IMP_EGR_DIA)
                     + SUM (SL.IMP_CGO_BCT)
                     + SUM (SL.IMP_EGR_CBA)),                        --Egresos
                      SUM (SL.IMP_RAL_AOS)
                    + SUM (SL.IMP_ING_CBA)
                    - SUM (SL.IMP_EGR_CBA),                      -- Saldo Real
                      (SUM (SL.IMP_ING_DIA) + SUM (SL.IMP_ABN_BCT))
                    - (SUM (SL.IMP_EGR_DIA) + SUM (SL.IMP_CGO_BCT))
                    + SUM (SL.IMP_PSC_CBA),                             --Neto
                    SUM (SL.IMP_ING_CPR),
                    SUM (SL.IMP_EGR_CPR),
                    (SUM (SL.IMP_ING_CPR) - SUM (SL.IMP_EGR_CPR)),
                    SUM (SL.IMP_TOT_24),
                    SUM (SL.IMP_TOT_48)
             INTO   v_MONEDA,
                    v_ING_BCCH,
                    v_EGR_BCCH,
                    v_SALDO_BBCH,
                    v_NETO_REAL,
                    v_INGRESO_PROY,
                    v_EGRESO_PROY,
                    v_NETO_PROY,
                    v_PROY_TOT_24,
                    v_PROY_TOT_48
             FROM   PABS_DT_DETLL_SALDO_CAJA_MN_MX SL
            WHERE   SL.COD_DVI = p_COD_DVI AND SL.FEC_ING_CTL <= v_FECHA_HOY
         GROUP BY   SL.COD_DVI;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_s_mensaje := err_code || '-' || err_msg;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;


      --Buscamos si hay data.
      SELECT   COUNT ( * )
        INTO   v_CANTIDAD
        FROM   PABS_DT_SALDO_HORA_ITDIA SD
       WHERE   SD.FEC_REG_SDO = v_FECHA_HOY AND SD.COD_DVI = p_COD_DVI;


      IF v_CANTIDAD = 0
      THEN
         v_NETO_PROY := v_SALDO_BBCH;
      END IF;

      p_ERROR := PKG_PAB_CONSTANTES.v_OK;

      --TABLA  PABS_DT_SALDO_HORA_ITDIA en creacion
      INSERT INTO PABS_DT_SALDO_HORA_ITDIA (FEC_REG_SDO,
                                            HOR_REG_SDO,
                                            COD_DVI,
                                            IMP_NET_ING,
                                            IMP_NET_EGR,
                                            IMP_SDO_RAL_AOS,
                                            IMP_NET_RAL,
                                            IMP_NET_ING_CPR,
                                            IMP_NET_EGR_CPR,
                                            IMP_NET_PYN,
                                            IMP_TOT_PYN_24_HOR,
                                            IMP_TOT_PYN_48_HOR)
        VALUES   (v_FECHA_HOY,
                  v_HORA_ACTUAL,
                  p_COD_DVI,
                  v_ING_BCCH,
                  v_EGR_BCCH,
                  v_SALDO_BBCH,
                  v_NETO_REAL,
                  v_INGRESO_PROY,
                  v_EGRESO_PROY,
                  v_NETO_PROY,
                  v_PROY_TOT_24,
                  v_PROY_TOT_48);
   EXCEPTION                                          -- si existe el registro
      WHEN DUP_VAL_ON_INDEX
      THEN
         err_code := SQLCODE;
         err_msg :=
               'Ya existe registro, moneda'
            || p_COD_DVI
            || ', con hora: '
            || TO_CHAR (SYSDATE, 'HH24MI');
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_INS_SALDO_POR_MONDA;

   /****************************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_INS_PAGOS_ACMDO_MONDA
   -- Objetivo: Procedimiento almacenado que inserta los pagos acumulados por hora, en moneda nacional.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_RGTRO_PAGOS_ACMDO , PABS_DT_DETLL_SALDO_CAJA_MN_MX
   --
   -- Fecha: 08/08/2018
   -- Autor: Santander CAH
   -- Input:
   --      p_COD_DVI  Moneda a consultar e insertar movimiento
   -- Output:
   --       p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***************************************************************************************************************/
   PROCEDURE SP_PAB_INS_PAGOS_ACMDO_MONDA (p_COD_DVI   IN     CHAR,
                                           p_ERROR        OUT NUMBER)
   IS
      v_NOM_SP                   VARCHAR2 (30) := 'SP_PAB_INS_PAGOS_ACMDO_MONDA';
      v_EGR_BCCH                 PABS_DT_DETLL_SALDO_CAJA_MN_MX.IMP_EGR_CBA%TYPE;
      v_ING_BCCH                 PABS_DT_DETLL_SALDO_CAJA_MN_MX.IMP_ING_CBA%TYPE;
      v_SALDO_BBCH               NUMBER;
      v_INGRESO_PROY             NUMBER;
      v_EGRESO_PROY              NUMBER;
      v_NETO_REAL                NUMBER;
      v_NETO_PROY                NUMBER;
      v_PROY_TOT_24              NUMBER;
      v_PROY_TOT_48              NUMBER;
      v_MONEDA                   PABS_DT_MONDA_ALMOT.COD_DVI%TYPE;
      v_HORA                     NUMBER := '0800';
      v_HORA_MAS                 NUMBER := '0100';
      v_HORA_ACTUAL              NUMBER := TO_CHAR (SYSDATE, 'HH24MI');
      v_HORA_REGISTRO_EGR        NUMBER;
      v_MONTO_REGISTRO_EGR       NUMBER;
      v_MONTO_REGISTRO_DIA_EGR   NUMBER;

      v_HORA_REGISTRO_ING        NUMBER;
      v_MONTO_REGISTRO_ING       NUMBER;
      v_MONTO_REGISTRO_DIA_ING   NUMBER;
   BEGIN
      BEGIN -- Tabla registros acumulados por hora. ultimo registro. PABS_DT_PAGOS_RLZAR_ACMDO
         SELECT   OP.IMP_PAG_RLZ_ACM_HOR,
                  OP.HOR_PAG_RLZ,
                  OP.IMP_PAG_RLZ_ACM_DIA
           INTO   v_MONTO_REGISTRO_EGR,
                  v_HORA_REGISTRO_EGR,
                  v_MONTO_REGISTRO_DIA_EGR
           FROM   PABS_DT_PAGOS_RLZAR_ACMDO OP
          WHERE   OP.HOR_PAG_RLZ IN
                        (SELECT   MAX (OP.HOR_PAG_RLZ)
                           FROM   PABS_DT_PAGOS_RLZAR_ACMDO OP
                          WHERE   OP.COD_DVI = p_COD_DVI
                                  AND OP.FLG_EGR_ING =
                                        PKG_PAB_CONSTANTES.V_FLG_EGR -- Campo en creacion
                                  --AND OP.IMP_PAG_RLZ_ACM_HOR > 0
                                  AND FEC_PAG_RLZ = v_FECHA_HOY)
                  AND OP.COD_DVI = p_COD_DVI
                  AND OP.FLG_EGR_ING = PKG_PAB_CONSTANTES.V_FLG_EGR -- Campo en creacion
                  AND FEC_PAG_RLZ = v_FECHA_HOY;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            v_MONTO_REGISTRO_EGR := NULL;
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_s_mensaje := err_code || '-' || err_msg;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;

      BEGIN
         -- INGRESOS
         SELECT   OP.IMP_PAG_RLZ_ACM_HOR,
                  OP.HOR_PAG_RLZ,
                  OP.IMP_PAG_RLZ_ACM_DIA
           INTO   v_MONTO_REGISTRO_ING,
                  v_HORA_REGISTRO_ING,
                  v_MONTO_REGISTRO_DIA_ING
           FROM   PABS_DT_PAGOS_RLZAR_ACMDO OP
          WHERE   OP.HOR_PAG_RLZ IN
                        (SELECT   MAX (OP.HOR_PAG_RLZ)
                           FROM   PABS_DT_PAGOS_RLZAR_ACMDO OP
                          WHERE   OP.COD_DVI = p_COD_DVI
                                  AND OP.FLG_EGR_ING =
                                        PKG_PAB_CONSTANTES.V_FLG_ING -- Campo en creacion
                                  AND FEC_PAG_RLZ = v_FECHA_HOY)
                  AND OP.COD_DVI = p_COD_DVI
                  AND OP.FLG_EGR_ING = PKG_PAB_CONSTANTES.V_FLG_ING
                  AND FEC_PAG_RLZ = v_FECHA_HOY;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            v_MONTO_REGISTRO_ING := NULL;
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_s_mensaje := err_code || '-' || err_msg;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;

      BEGIN
           --A modificar tabla para agregar flag de ingreso o egreso.
           SELECT   SL.COD_DVI,
                    (SUM (SL.IMP_EGR_DIA) + SUM (SL.IMP_CGO_BCT)),
                    (SUM (SL.IMP_ING_DIA) + SUM (SL.IMP_ABN_BCT))
             INTO   v_MONEDA, v_EGR_BCCH, v_ING_BCCH
             FROM   PABS_DT_DETLL_SALDO_CAJA_MN_MX SL
            WHERE   SL.COD_DVI = p_COD_DVI AND SL.FEC_ING_CTL <= v_FECHA_HOY
         GROUP BY   SL.COD_DVI;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            err_code := SQLCODE;
            err_msg := 'No se han encontrado registos';
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_s_mensaje := err_code || '-' || err_msg;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;


      --Si la hora es nulo, sera el primer registro a las 9:00, sino se sumara una hora al ultimo registro
      IF v_HORA_REGISTRO_EGR IS NULL
      THEN
         --v_HORA := TO_CHAR(SYSDATE,'HH24')||'00';--'0800';

         --INSERTA REGISTRO EGRESOS
         INSERT INTO PABS_DT_PAGOS_RLZAR_ACMDO (FEC_PAG_RLZ,
                                                HOR_PAG_RLZ,
                                                COD_DVI,
                                                IMP_PAG_RLZ_ACM_HOR,
                                                IMP_PAG_RLZ_ACM_DIA,
                                                FLG_EGR_ING)
           VALUES   (v_FECHA_HOY,
                     v_HORA,
                     p_COD_DVI,
                     v_EGR_BCCH,
                     v_EGR_BCCH,
                     PKG_PAB_CONSTANTES.V_FLG_EGR);

         --INSERTA REGISTRO INGRESOS
         INSERT INTO PABS_DT_PAGOS_RLZAR_ACMDO (FEC_PAG_RLZ,
                                                HOR_PAG_RLZ,
                                                COD_DVI,
                                                IMP_PAG_RLZ_ACM_HOR,
                                                IMP_PAG_RLZ_ACM_DIA,
                                                FLG_EGR_ING)
           VALUES   (v_FECHA_HOY,
                     v_HORA,
                     p_COD_DVI,
                     v_ING_BCCH,
                     v_ING_BCCH,
                     PKG_PAB_CONSTANTES.V_FLG_ING);
      ELSE
         --Agregamos una hora.
         v_HORA := v_HORA_REGISTRO_EGR + v_HORA_MAS;                -- '0100';


         IF v_HORA_ACTUAL >= v_HORA
         THEN
            v_MONTO_REGISTRO_EGR :=
               ABS (v_EGR_BCCH - v_MONTO_REGISTRO_DIA_EGR);
            v_MONTO_REGISTRO_ING :=
               ABS (v_ING_BCCH - v_MONTO_REGISTRO_DIA_ING);


            --            --Si el saldo queda neagtivo. En caso de cambio de cartola
            --            IF v_MONTO_REGISTRO_EGR <0 THEN
            --               v_MONTO_REGISTRO_EGR :=v_MONTO_REGISTRO_EGR * -1;
            --
            --            ELSIF   v_MONTO_REGISTRO_EGR =0 THEN
            --                v_MONTO_REGISTRO_EGR := v_MONTO_REGISTRO_DIA_EGR;
            --
            --            END IF;
            --
            --            IF v_MONTO_REGISTRO_ING <0 THEN
            --               v_MONTO_REGISTRO_ING := v_MONTO_REGISTRO_ING * -1;
            --
            --            ELSIF v_MONTO_REGISTRO_ING =0 THEN
            --                v_MONTO_REGISTRO_ING := v_MONTO_REGISTRO_DIA_ING;
            --
            --            END IF;

            --INSERTA REGISTRO EGRESO
            INSERT INTO PABS_DT_PAGOS_RLZAR_ACMDO (FEC_PAG_RLZ,
                                                   HOR_PAG_RLZ,
                                                   COD_DVI,
                                                   IMP_PAG_RLZ_ACM_HOR,
                                                   IMP_PAG_RLZ_ACM_DIA,
                                                   FLG_EGR_ING)
              VALUES   (v_FECHA_HOY,
                        v_HORA,
                        p_COD_DVI,
                        v_MONTO_REGISTRO_EGR,
                        v_EGR_BCCH,
                        PKG_PAB_CONSTANTES.V_FLG_EGR);

            COMMIT;

            --INSERTA REGISTRO INGRESO
            INSERT INTO PABS_DT_PAGOS_RLZAR_ACMDO (FEC_PAG_RLZ,
                                                   HOR_PAG_RLZ,
                                                   COD_DVI,
                                                   IMP_PAG_RLZ_ACM_HOR,
                                                   IMP_PAG_RLZ_ACM_DIA,
                                                   FLG_EGR_ING)
              VALUES   (v_FECHA_HOY,
                        v_HORA,
                        p_COD_DVI,
                        v_MONTO_REGISTRO_ING,
                        v_ING_BCCH,
                        PKG_PAB_CONSTANTES.V_FLG_ING);

            COMMIT;
         END IF;
      END IF;

      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   EXCEPTION
      WHEN DUP_VAL_ON_INDEX
      THEN
         err_code := SQLCODE;
         err_msg :=
               'Ya existe registro, moneda'
            || p_COD_DVI
            || ', con hora: '
            || v_HORA;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_INS_PAGOS_ACMDO_MONDA;

   /******************************************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_REC_SALDO_LBTR
   -- Objetivo: Procedimiento almacenado recorre las moneda  para insertar los pagos acumulados
              por hora.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_RGTRO_PAGOS_ACMDO
   --
   -- Fecha: 08/08/2018
   -- Autor: Santander CAH
   -- Input:
   -- Output:
   --       p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --******************************************************************************************************************************/
   PROCEDURE SP_PAB_REC_SALDO_LBTR (p_ERROR OUT NUMBER)
   IS
      v_NOM_SP         VARCHAR2 (30) := 'SP_PAB_REC_SALDO_LBTR';
      v_ING_BCCH       NUMBER;
      v_EGR_BCCH       NUMBER;
      v_SALDO_BBCH     NUMBER;
      v_INGRESO_PROY   NUMBER;
      v_EGRESO_PROY    NUMBER;
      v_NETO_REAL      NUMBER;
      v_NETO_PROY      NUMBER;
      v_MONEDA         CHAR;
      v_CANT           NUMBER;
   BEGIN
      --Obtenemos la  ultima fecha habil
      PKG_PAB_UTILITY.SP_PAB_DEV_DIA_HABIL_ANT (v_COD_PAIS, v_FECHA_AYER);

      ---------------------------------------------

      BEGIN
         SELECT   COUNT (SB.COD_BIC_BCO)
           INTO   v_CANT
           FROM   PABS_DT_SALDO_BILAT SB
          WHERE   SB.COD_SIS_SAL = PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_LBTR
                  AND SB.FEC_ING_CTL = v_FECHA_AYER;
      EXCEPTION
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            p_s_mensaje := err_code || '-' || err_msg;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;

      --------------------------------------------

      IF v_CANT > 0
      THEN
         BEGIN
            OPEN C_BANCOS_LBTR;
         EXCEPTION
            WHEN OTHERS
            THEN
               err_code := SQLCODE;
               err_msg := SUBSTR (SQLERRM, 1, 300);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               RAISE_APPLICATION_ERROR (-20000, err_msg);
         END;

         BEGIN
            FETCH C_BANCOS_LBTR INTO   R_BANCOS_LBTR;
         EXCEPTION
            WHEN OTHERS
            THEN
               err_code := SQLCODE;
               err_msg := SUBSTR (SQLERRM, 1, 300);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);

               --Verificamos si el cursor se encuentra abierto en caso de error.
               IF (C_BANCOS_LBTR%ISOPEN)
               THEN
                  CLOSE C_BANCOS_LBTR;
               END IF;

               RAISE_APPLICATION_ERROR (-20000, err_msg);
         END;

         WHILE C_BANCOS_LBTR%FOUND
         LOOP
            --Se registra un nuevo saldo por moneda
            PKG_PAB_LIQUIDEZ.SP_PAB_INS_SALDO_LIQDZ (
               R_BANCOS_LBTR.BANCO,
               PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_LBTR,
               p_ERROR
            );

            FETCH C_BANCOS_LBTR INTO   R_BANCOS_LBTR;
         END LOOP;

         CLOSE C_BANCOS_LBTR;

         p_ERROR := PKG_PAB_CONSTANTES.v_OK;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         --Verificamos si el cursor se encuentra abierto en caso de error.
         IF (C_BANCOS_LBTR%ISOPEN)
         THEN
            CLOSE C_BANCOS_LBTR;
         END IF;

         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_REC_SALDO_LBTR;

   /******************************************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_REC_SALDO_MX
   -- Objetivo: Procedimiento almacenado recorre los banco corresponsales.
               por hora.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_RGTRO_PAGOS_ACMDO
   --
   -- Fecha: 08/08/2018
   -- Autor: Santander CAH
   -- Input:
   -- Output:
   --       p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --******************************************************************************************************************************/
   PROCEDURE SP_PAB_REC_SALDO_MX (p_ERROR OUT NUMBER)
   IS
      v_NOM_SP         VARCHAR2 (30) := 'SP_PAB_REC_SALDO_MX';
      v_ING_BCCH       NUMBER;
      v_EGR_BCCH       NUMBER;
      v_SALDO_BBCH     NUMBER;
      v_INGRESO_PROY   NUMBER;
      v_EGRESO_PROY    NUMBER;
      v_NETO_REAL      NUMBER;
      v_NETO_PROY      NUMBER;
   BEGIN
      BEGIN
         OPEN C_CONSULTA_BANCOS;
      EXCEPTION
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            RAISE_APPLICATION_ERROR (-20000, err_msg);
            err_msg := 'Aqui open cursor';
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
      END;

      BEGIN
         FETCH C_CONSULTA_BANCOS INTO   R_CONSULTA_BANCOS;
      EXCEPTION
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);

            --Verificamos si el cursor se encuentra abierto en caso de error.
            IF (C_CONSULTA_BANCOS%ISOPEN)
            THEN
               CLOSE C_CONSULTA_BANCOS;
            END IF;

            RAISE_APPLICATION_ERROR (-20000, err_msg);
      END;

      WHILE C_CONSULTA_BANCOS%FOUND
      LOOP
         --Se registra un nuevo saldo por moneda
         PKG_PAB_LIQUIDEZ.SP_PAB_INS_SALDO_MX_LIQDZ (
            R_CONSULTA_BANCOS.BANCO,
            R_CONSULTA_BANCOS.MONEDA,
            p_ERROR
         );

         FETCH C_CONSULTA_BANCOS INTO   R_CONSULTA_BANCOS;
      END LOOP;

      CLOSE C_CONSULTA_BANCOS;
   EXCEPTION
      WHEN OTHERS
      THEN
         --Verificamos si el cursor se encuentra abierto en caso de error.
         IF (C_CONSULTA_BANCOS%ISOPEN)
         THEN
            CLOSE C_CONSULTA_BANCOS;
         END IF;

         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_REC_SALDO_MX;

   /******************************************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_REC_SALDO_CMN
   -- Objetivo: Procedimiento almacenado recorre las moneda  para insertar los pagos acumulados
              por hora.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_RGTRO_PAGOS_ACMDO
   --
   -- Fecha: 08/08/2018
   -- Autor: Santander CAH
   -- Input:
   -- Output:
   --       p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --******************************************************************************************************************************/
   PROCEDURE SP_PAB_REC_SALDO_CMN (p_ERROR OUT NUMBER)
   IS
      v_NOM_SP         VARCHAR2 (30) := 'SP_PAB_REC_SALDO_CMN';
      v_ING_BCCH       NUMBER;
      v_EGR_BCCH       NUMBER;
      v_SALDO_BBCH     NUMBER;
      v_INGRESO_PROY   NUMBER;
      v_EGRESO_PROY    NUMBER;
      v_NETO_REAL      NUMBER;
      v_NETO_PROY      NUMBER;
      v_MONEDA         CHAR;
      v_CANT           NUMBER;
   BEGIN
      --Obtenemos la  ultima fecha habil
      PKG_PAB_UTILITY.SP_PAB_DEV_DIA_HABIL_ANT (v_COD_PAIS, v_FECHA_AYER);

      BEGIN                                     --Buscamos si hay data del dia
         SELECT   COUNT (SB.COD_BIC_BCO)
           INTO   v_CANT
           FROM   PABS_DT_SALDO_BILAT SB
          WHERE   SB.COD_SIS_SAL = PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_CMN
                  AND SB.FEC_ING_CTL = v_FECHA_AYER;
      EXCEPTION
         --            WHEN NO_DATA_FOUND  THEN
         --                 err_code := SQLCODE;
         --                 err_msg := SUBSTR (SQLERRM, 1, 300) || 'No hay datos para: '||PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_LBTR ;
         --                 RAISE;


         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            p_s_mensaje := err_code || '-' || err_msg;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;

      IF v_CANT > 0
      THEN
         BEGIN
            OPEN C_BANCOS_CMN;
         EXCEPTION
            WHEN OTHERS
            THEN
               err_code := SQLCODE;
               err_msg := SUBSTR (SQLERRM, 1, 300);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               RAISE_APPLICATION_ERROR (-20000, err_msg);
         END;

         BEGIN
            FETCH C_BANCOS_CMN INTO   R_BANCOS_CMN;
         EXCEPTION
            WHEN OTHERS
            THEN
               err_code := SQLCODE;
               err_msg := SUBSTR (SQLERRM, 1, 300);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);

               --Verificamos si el cursor se encuentra abierto en caso de error.
               IF (C_BANCOS_CMN%ISOPEN)
               THEN
                  CLOSE C_BANCOS_CMN;
               END IF;

               RAISE_APPLICATION_ERROR (-20000, err_msg);
         END;

         WHILE C_BANCOS_CMN%FOUND
         LOOP
            --Se registra un nuevo saldo por moneda
            PKG_PAB_LIQUIDEZ.SP_PAB_INS_SALDO_LIQDZ (
               R_BANCOS_CMN.BANCO,
               PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_CMN,
               p_ERROR
            );

            FETCH C_BANCOS_CMN INTO   R_BANCOS_CMN;
         END LOOP;

         CLOSE C_BANCOS_CMN;

         p_ERROR := PKG_PAB_CONSTANTES.v_OK;
      END IF;

      p_ERROR := PKG_PAB_CONSTANTES.V_ERROR;
   EXCEPTION
      WHEN OTHERS
      THEN
         --Verificamos si el cursor se encuentra abierto en caso de error.
         IF (C_BANCOS_CMN%ISOPEN)
         THEN
            CLOSE C_BANCOS_CMN;
         END IF;

         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_REC_SALDO_CMN;

   /******************************************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_INS_SALDO_LIQDZ_BCCH
   -- Objetivo: Procedimiento almacenado que inserta Registros de saldos, para la tabla de consulta Saldos
   -- Sistema: DBO_PAB.
   -- Base de Datos: PABS_DT_, PABS_DT_
   -- Tablas Usadas:
   --
   -- Fecha: 17-08-2018
   -- Autor: Santander CAH
   -- Input:
   --
   --
   -- Output:

   --      p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
     --******************************************************************************************************************************/
   PROCEDURE SP_PAB_INS_SALDO_LIQDZ_BCCH (p_ERROR OUT NUMBER)
   IS
      v_NOM_SP         VARCHAR2 (30) := 'SP_PAB_INS_SALDO_LIQDZ_BCCH';

      v_BANCO          PABS_DT_DETLL_SALDO_CAJA_MN_MX.COD_BCO_ORG%TYPE;
      v_SDO_APERTURA   PABS_DT_DETLL_SALDO_CAJA_MN_MX.IMP_APR_AOS%TYPE;
      v_INGRESOS       PABS_DT_DETLL_SALDO_CAJA_MN_MX.IMP_ING_DIA%TYPE;
      v_EGRESOS        PABS_DT_DETLL_SALDO_CAJA_MN_MX.IMP_EGR_DIA%TYPE;
      v_SDO_FINAL      PABS_DT_DETLL_SALDO_CAJA_MN_MX.IMP_RAL_AOS%TYPE;

      v_FEC_REG        PABS_DT_RGTRO_SALDO_HTRCO.FEC_REG_SDO%TYPE;
      v_HOR_REG        PABS_DT_RGTRO_SALDO_HTRCO.HOR_REG_SDO%TYPE;
      v_HOR_REG2       CHAR (4);
      v_COD_DVI        PABS_DT_RGTRO_SALDO_HTRCO.COD_DVI%TYPE;
      v_COD_BIC_BCO    PABS_DT_RGTRO_SALDO_HTRCO.COD_BIC_BCO%TYPE;
      v_COD_CNL_SAL    PABS_DT_RGTRO_SALDO_HTRCO.COD_CNL_SAL%TYPE;
      v_IMP_SDO_APR    PABS_DT_RGTRO_SALDO_HTRCO.IMP_SDO_APR%TYPE;
      v_IMP_NET_ING    PABS_DT_RGTRO_SALDO_HTRCO.IMP_NET_ING%TYPE;
      v_IMP_NET_EGR    PABS_DT_RGTRO_SALDO_HTRCO.IMP_NET_EGR%TYPE;
      v_IMP_SDO_RAL    PABS_DT_RGTRO_SALDO_HTRCO.IMP_SDO_RAL%TYPE;
      v_IMP_SDO_ITE    PABS_DT_RGTRO_SALDO_HTRCO.IMP_SDO_ITE%TYPE;
      v_IMP_NET_RAL    PABS_DT_RGTRO_SALDO_HTRCO.IMP_NET_RAL%TYPE;
      v_IMP_NET_MIN    PABS_DT_RGTRO_SALDO_HTRCO.IMP_NET_MIN%TYPE;
      v_IMP_NET_MAX    PABS_DT_RGTRO_SALDO_HTRCO.IMP_NET_MAX%TYPE;
      v_IMP_MIN_ING    PABS_DT_RGTRO_SALDO_HTRCO.IMP_MIN_ING%TYPE;
      v_IMP_MIN_EGR    PABS_DT_RGTRO_SALDO_HTRCO.IMP_MIN_EGR%TYPE;
      v_IMP_MAX_ING    PABS_DT_RGTRO_SALDO_HTRCO.IMP_MAX_ING%TYPE;
      v_IMP_MAX_EGR    PABS_DT_RGTRO_SALDO_HTRCO.IMP_MAX_EGR%TYPE;

      v_HORA_ACTUAL    NUMBER := TO_CHAR (SYSDATE, 'HH24MI');
      v_HORA           NUMBER;
      v_FECHA_NUM      NUMBER := TO_NUMBER (TO_CHAR (SYSDATE, 'YYYYMMDD'));

      --Variables para bitacora.
      --    p_ERR_BTCOR   PABS_DT_BTCOR_BASE_DATO_ALMOT.COD_ERR_BSE_DTS%TYPE;
      --    p_NOM_PCK     PABS_DT_BTCOR_BASE_DATO_ALMOT.NOM_PCK%TYPE;
      P_FECHA_INS      DATE;
      v_COD_ERR_ITRA   NUMBER := 500;
   BEGIN
      --Obtenemos la  ultima fecha habil
      PKG_PAB_UTILITY.SP_PAB_DEV_DIA_HABIL_ANT (v_COD_PAIS, v_FECHA_AYER);

      BEGIN                              --Buscamos el saldo del banco central
         SELECT   SL.COD_BCO_ORG,
                  SL.IMP_APR_AOS,
                  SL.IMP_ING_DIA + SL.IMP_ABN_BCT,
                  SL.IMP_EGR_DIA + SL.IMP_CGO_BCT,
                  SL.IMP_RAL_AOS
           INTO   v_BANCO,
                  v_SDO_APERTURA,
                  v_INGRESOS,
                  v_EGRESOS,
                  v_SDO_FINAL
           FROM   PABS_DT_DETLL_SALDO_CAJA_MN_MX SL
          WHERE       SL.COD_DVI = PKG_PAB_CONSTANTES.V_STR_DVI_CLP
                  AND SL.FEC_ING_CTL = v_FECHA_AYER        -- Ultimo dia habil
                  AND SL.COD_BCO_ORG = PKG_PAB_CONSTANTES.V_BIC_BCCH;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            err_code := v_COD_ERR_ITRA;                             --SQLCODE;
            err_msg :=
               'No hay data de Cartola Nacional del dia: ' || v_FECHA_AYER;
            RAISE;
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            p_s_mensaje := err_code || '-' || err_msg;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;


      BEGIN             --Buscamos saldos de nuestra tabla (saldos historicos)
         SELECT   FEC_REG_SDO,
                  HOR_REG_SDO,
                  COD_DVI,
                  COD_BIC_BCO,
                  COD_CNL_SAL,
                  IMP_SDO_APR,
                  IMP_NET_ING,
                  IMP_NET_EGR,
                  IMP_SDO_RAL,
                  IMP_SDO_ITE,
                  IMP_NET_RAL,
                  IMP_NET_MIN,
                  IMP_NET_MAX,
                  IMP_MIN_ING,
                  IMP_MIN_EGR,
                  IMP_MAX_ING,
                  IMP_MAX_EGR
           INTO   v_FEC_REG,
                  v_HOR_REG,
                  v_COD_DVI,
                  v_COD_BIC_BCO,
                  v_COD_CNL_SAL,
                  v_IMP_SDO_APR,
                  v_IMP_NET_ING,
                  v_IMP_NET_EGR,
                  v_IMP_SDO_RAL,
                  v_IMP_SDO_ITE,
                  v_IMP_NET_RAL,
                  v_IMP_NET_MIN,
                  v_IMP_NET_MAX,
                  v_IMP_MIN_ING,
                  v_IMP_MIN_EGR,
                  v_IMP_MAX_ING,
                  v_IMP_MAX_EGR
           FROM   PABS_DT_RGTRO_SALDO_HTRCO
          WHERE       COD_BIC_BCO = PKG_PAB_CONSTANTES.V_BIC_BCCH
                  AND COD_DVI = PKG_PAB_CONSTANTES.V_STR_DVI_CLP
                  AND COD_CNL_SAL = PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_LBTR
                  AND FEC_REG_SDO = v_FECHA_NUM             -- TRUNC(SYSDATE);
                  AND HOR_REG_SDO IN
                           (SELECT   MAX (HOR_REG_SDO)
                              FROM   PABS_DT_RGTRO_SALDO_HTRCO
                             WHERE   COD_BIC_BCO =
                                        PKG_PAB_CONSTANTES.V_BIC_BCCH
                                     AND COD_DVI =
                                           PKG_PAB_CONSTANTES.V_STR_DVI_CLP
                                     AND COD_CNL_SAL =
                                           PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_LBTR
                                     AND FEC_REG_SDO = v_FECHA_NUM);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            v_HOR_REG := 0;
         --err_code := SQLCODE;
         --            err_msg := 'No existen Datos para el dia: ' || v_FECHA_AYER;
         --            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,  v_NOM_PCK, err_msg, v_NOM_SP);

         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            p_s_mensaje := err_code || '-' || err_msg;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;

      IF v_HOR_REG = 0
      THEN                                       -- Se ingresa primer registro
         v_HOR_REG := TO_NUMBER (TO_CHAR (SYSDATE, 'HH24MI'));


         v_IMP_SDO_APR := v_SDO_APERTURA;
         v_IMP_NET_ING := v_INGRESOS;
         v_IMP_NET_EGR := v_EGRESOS;
         v_IMP_SDO_RAL := v_SDO_FINAL;
         v_IMP_SDO_ITE := v_INGRESOS - v_EGRESOS;
         v_IMP_NET_RAL := v_INGRESOS - v_EGRESOS;
         v_IMP_NET_MIN := v_INGRESOS - v_EGRESOS;
         v_IMP_NET_MAX := v_INGRESOS - v_EGRESOS;
         v_IMP_MIN_ING := v_INGRESOS;
         v_IMP_MIN_EGR := v_EGRESOS;
         v_IMP_MAX_ING := v_INGRESOS;
         v_IMP_MAX_EGR := v_EGRESOS;


         BEGIN
            INSERT INTO PABS_DT_RGTRO_SALDO_HTRCO (FEC_REG_SDO,
                                                   HOR_REG_SDO,
                                                   COD_DVI,
                                                   COD_BIC_BCO,
                                                   COD_CNL_SAL,
                                                   IMP_SDO_APR,
                                                   IMP_NET_ING,
                                                   IMP_NET_EGR,
                                                   IMP_SDO_RAL,
                                                   IMP_SDO_ITE,
                                                   IMP_NET_RAL,
                                                   IMP_NET_MIN,
                                                   IMP_NET_MAX,
                                                   IMP_MIN_ING,
                                                   IMP_MIN_EGR,
                                                   IMP_MAX_ING,
                                                   IMP_MAX_EGR)
              VALUES   (v_FECHA_NUM,
                        v_HOR_REG,
                        PKG_PAB_CONSTANTES.V_STR_DVI_CLP,
                        PKG_PAB_CONSTANTES.V_BIC_BCCH,
                        PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_LBTR,
                        v_IMP_SDO_APR,
                        v_IMP_NET_ING,
                        v_IMP_NET_EGR,
                        v_IMP_SDO_RAL,
                        v_IMP_SDO_ITE,
                        v_IMP_NET_RAL,
                        v_IMP_NET_MIN,
                        v_IMP_NET_MAX,
                        v_IMP_MIN_ING,
                        v_IMP_MIN_EGR,
                        v_IMP_MAX_ING,
                        v_IMP_MAX_EGR);
         EXCEPTION                                    -- si existe el registro
            WHEN DUP_VAL_ON_INDEX
            THEN
               err_code := SQLCODE;
               err_msg :=
                     'Ya existe registro, moneda'
                  || v_COD_DVI
                  || ', con hora: '
                  || v_HOR_REG;
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         END;
      ELSE                                        -- Se ingresa nuevo registro
         BEGIN
            SELECT   SUM (IMP_NET_ING), SUM (IMP_NET_EGR)
              INTO   v_IMP_NET_ING, v_IMP_NET_EGR
              FROM   PABS_DT_RGTRO_SALDO_HTRCO SH
             WHERE   COD_BIC_BCO = PKG_PAB_CONSTANTES.V_BIC_BCCH
                     AND COD_DVI = PKG_PAB_CONSTANTES.V_STR_DVI_CLP
                     AND COD_CNL_SAL =
                           PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_LBTR
                     AND FEC_REG_SDO = v_FECHA_NUM;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               v_HOR_REG := 0;
            WHEN OTHERS
            THEN
               err_code := SQLCODE;
               err_msg := SUBSTR (SQLERRM, 1, 300);
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
               p_s_mensaje := err_code || '-' || err_msg;
               RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
         END;


         IF v_SDO_FINAL = v_IMP_SDO_RAL
         THEN                                               -- Sin movimientos
            v_IMP_NET_ING := 0;
            v_IMP_NET_EGR := 0;
         ELSE
            v_IMP_NET_ING := v_INGRESOS - v_IMP_NET_ING;
            v_IMP_NET_EGR := v_EGRESOS - v_IMP_NET_EGR;
         END IF;

         IF v_HOR_REG < 1000
         THEN
            v_HOR_REG2 := TO_CHAR ('0' || v_HOR_REG);
         ELSE
            v_HOR_REG2 := v_HOR_REG;
         END IF;


         v_HORA :=
            TO_CHAR (
               TO_DATE (TO_CHAR (SYSDATE, 'YYYY-MM-DD') || ' ' || v_HOR_REG2,
                        'YYYY-MM-DD HH24:MI')
               + 30 / 1440,
               'HH24MI'
            );

         IF v_HORA_ACTUAL >= v_HORA
         THEN                        --Si inserta, solo si ya paso 30 minutos.
            --  v_IMP_SDO_APR := v_IMP_SDO_RAL;
            v_IMP_SDO_APR := 0;
            v_IMP_SDO_RAL := v_IMP_SDO_APR + v_IMP_NET_ING - v_IMP_NET_EGR;
            v_IMP_SDO_ITE := v_IMP_NET_ING - v_IMP_NET_EGR;
            v_IMP_NET_RAL := v_IMP_NET_ING - v_IMP_NET_EGR;
            v_IMP_NET_MIN := v_IMP_NET_ING - v_IMP_NET_EGR;
            v_IMP_NET_MAX := v_IMP_NET_ING - v_IMP_NET_EGR;
            v_IMP_MIN_ING := v_IMP_NET_ING;
            v_IMP_MIN_EGR := v_IMP_NET_EGR;
            v_IMP_MAX_ING := v_IMP_NET_ING;
            v_IMP_MAX_EGR := v_IMP_NET_EGR;

            BEGIN
               INSERT INTO PABS_DT_RGTRO_SALDO_HTRCO (FEC_REG_SDO,
                                                      HOR_REG_SDO,
                                                      COD_DVI,
                                                      COD_BIC_BCO,
                                                      COD_CNL_SAL,
                                                      IMP_SDO_APR,
                                                      IMP_NET_ING,
                                                      IMP_NET_EGR,
                                                      IMP_SDO_RAL,
                                                      IMP_SDO_ITE,
                                                      IMP_NET_RAL,
                                                      IMP_NET_MIN,
                                                      IMP_NET_MAX,
                                                      IMP_MIN_ING,
                                                      IMP_MIN_EGR,
                                                      IMP_MAX_ING,
                                                      IMP_MAX_EGR)
                 VALUES   (v_FECHA_NUM,
                           v_HORA,
                           v_COD_DVI,
                           PKG_PAB_CONSTANTES.V_BIC_BCCH,
                           PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_LBTR,
                           v_IMP_SDO_APR,
                           v_IMP_NET_ING,
                           v_IMP_NET_EGR,
                           v_IMP_SDO_RAL,
                           v_IMP_SDO_ITE,
                           v_IMP_NET_RAL,
                           v_IMP_NET_MIN,
                           v_IMP_NET_MAX,
                           v_IMP_MIN_ING,
                           v_IMP_MIN_EGR,
                           v_IMP_MAX_ING,
                           v_IMP_MAX_EGR);
            EXCEPTION
               WHEN DUP_VAL_ON_INDEX
               THEN                                   -- si existe el registro
                  err_code := SQLCODE;
                  err_msg :=
                        'Ya existe registro, moneda'
                     || v_COD_DVI
                     || ', con hora:.. '
                     || v_HORA;
                  PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                      v_NOM_PCK,
                                                      err_msg,
                                                      v_NOM_SP);
                  p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            END;
         END IF;
      END IF;

      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         --Validamos si hay traza hoy
         PKG_PAB_LIQUIDEZ.SP_PAB_BUS_ERRO_BTCOR (err_code,
                                                 v_NOM_PCK,
                                                 P_FECHA_INS,
                                                 P_ERROR);


         IF TRUNC (P_FECHA_INS) IS NULL
         THEN                                        -- <> trunc(SYSDATE) THEN
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         END IF;


         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_INS_SALDO_LIQDZ_BCCH;

   /******************************************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_BUS_ERRO_BTCOR
   -- Objetivo: Procedimiento almacenado que muestra si un error esta ingresado en bitacora.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_BTCOR_BASE_DATO_ALMOT
   --
   -- Fecha: 11-03-2019
   -- Autor: Santander CAH
   -- Input:
   --      p_ERR_BTCOR     Codigo del error en bitacora
   --      p_NOM_PCK       Nombre del package
   -- Output:
   --       P_FECHA_INS
   --       p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --******************************************************************************************************************************/
   PROCEDURE SP_PAB_BUS_ERRO_BTCOR (p_ERR_BTCOR   IN     CHAR,
                                    p_NOM_PCK     IN     VARCHAR2,
                                    P_FECHA_INS      OUT TIMESTAMP,
                                    p_ERROR          OUT NUMBER)
   IS
      v_NOM_SP      VARCHAR2 (30) := 'SP_PAB_BUS_ERRO_BTCOR';
      v_FECHA_HOY   DATE := TRUNC (SYSDATE);
      v_DIA_INI     DATE := TRUNC (SYSDATE) + INTERVAL '1' MINUTE;
      v_DIA_FIN DATE
            := TRUNC (SYSDATE) + INTERVAL '23' HOUR + INTERVAL '59' MINUTE ;
   BEGIN
      SELECT   MAX (BT.FEC_ISR_BTC_BSE_DTS)
        INTO   P_FECHA_INS
        FROM   PABS_DT_BTCOR_BASE_DATO_ALMOT BT
       WHERE       BT.FEC_ISR_BTC_BSE_DTS BETWEEN v_DIA_INI AND v_DIA_FIN
               AND BT.COD_ERR_BSE_DTS = p_ERR_BTCOR
               AND BT.NOM_PCK = P_NOM_PCK;

      --AND BT.NOM_PCM = P_NOM_SP;

      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         P_FECHA_INS := NULL;
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_BUS_ERRO_BTCOR;

   /******************************************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_INS_SALDO_LIQDZ
   -- Objetivo: Procedimiento almacenado que inserta Registros de saldos, para la tabla de consulta Saldos
   -- Sistema: DBO_PAB.
   -- Base de Datos:   DBO_PAB
   -- Tablas Usadas:   PABS_DT_SALDO_BILAT, PABS_DT_DETLL_SALDO_CAJA_MN_MX,
   --                  PABS_DT_RGTRO_SALDO_HTRCO
   --
   -- Fecha: 06-11-2018
   -- Autor: Santander CAH
   -- Input:
   --      p_BANCO  Banco para actualizar
   --      p_CANAL Canal de salida
   -- Output:

   --      p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
      --******************************************************************************************************************************/
   PROCEDURE SP_PAB_INS_SALDO_LIQDZ (p_BANCO   IN     CHAR,
                                     p_CANAL   IN     CHAR,
                                     p_ERROR      OUT NUMBER)
   IS
      v_NOM_SP         VARCHAR2 (30) := 'SP_PAB_INS_SALDO_LIQDZ';
      v_BANCO          PABS_DT_DETLL_SALDO_CAJA_MN_MX.COD_BCO_ORG%TYPE;
      v_SDO_APERTURA   PABS_DT_DETLL_SALDO_CAJA_MN_MX.IMP_APR_AOS%TYPE;
      v_INGRESOS       PABS_DT_DETLL_SALDO_CAJA_MN_MX.IMP_ING_DIA%TYPE;
      v_EGRESOS        PABS_DT_DETLL_SALDO_CAJA_MN_MX.IMP_EGR_DIA%TYPE;
      v_SDO_FINAL      PABS_DT_DETLL_SALDO_CAJA_MN_MX.IMP_RAL_AOS%TYPE;
      v_HORA           NUMBER;
      v_COD_SIS_SAL    PABS_DT_SALDO_BILAT.COD_SIS_SAL%TYPE;
      v_IMP_PAG_REC    PABS_DT_SALDO_BILAT.IMP_PAG_REC%TYPE;
      v_IMP_PAG_ENV    PABS_DT_SALDO_BILAT.IMP_PAG_ENV%TYPE;
      v_IMP_POS_BIL    PABS_DT_SALDO_BILAT.IMP_POS_BIL%TYPE;
      v_FEC_REG        PABS_DT_RGTRO_SALDO_HTRCO.FEC_REG_SDO%TYPE;
      v_HOR_REG        PABS_DT_RGTRO_SALDO_HTRCO.HOR_REG_SDO%TYPE;
      v_HOR_REG2       CHAR (4);
      v_COD_DVI        PABS_DT_RGTRO_SALDO_HTRCO.COD_DVI%TYPE;
      v_COD_BIC_BCO    PABS_DT_RGTRO_SALDO_HTRCO.COD_BIC_BCO%TYPE;
      v_COD_CNL_SAL    PABS_DT_RGTRO_SALDO_HTRCO.COD_CNL_SAL%TYPE;
      v_IMP_SDO_APR    PABS_DT_RGTRO_SALDO_HTRCO.IMP_SDO_APR%TYPE;
      v_IMP_NET_ING    PABS_DT_RGTRO_SALDO_HTRCO.IMP_NET_ING%TYPE;
      v_IMP_NET_EGR    PABS_DT_RGTRO_SALDO_HTRCO.IMP_NET_EGR%TYPE;
      v_IMP_SDO_RAL    PABS_DT_RGTRO_SALDO_HTRCO.IMP_SDO_RAL%TYPE;
      v_IMP_SDO_ITE    PABS_DT_RGTRO_SALDO_HTRCO.IMP_SDO_ITE%TYPE;
      v_IMP_NET_RAL    PABS_DT_RGTRO_SALDO_HTRCO.IMP_NET_RAL%TYPE;
      v_IMP_NET_MIN    PABS_DT_RGTRO_SALDO_HTRCO.IMP_NET_MIN%TYPE;
      v_IMP_NET_MAX    PABS_DT_RGTRO_SALDO_HTRCO.IMP_NET_MAX%TYPE;
      v_IMP_MIN_ING    PABS_DT_RGTRO_SALDO_HTRCO.IMP_MIN_ING%TYPE;
      v_IMP_MIN_EGR    PABS_DT_RGTRO_SALDO_HTRCO.IMP_MIN_EGR%TYPE;
      v_IMP_MAX_ING    PABS_DT_RGTRO_SALDO_HTRCO.IMP_MAX_ING%TYPE;
      v_IMP_MAX_EGR    PABS_DT_RGTRO_SALDO_HTRCO.IMP_MAX_EGR%TYPE;

      v_NET_ING        NUMBER;
      v_NET_EGR        NUMBER;
      v_HORA_ACTUAL    NUMBER := TO_CHAR (SYSDATE, 'HH24MI');
      v_FECHA_NUM      NUMBER := TO_NUMBER (TO_CHAR (SYSDATE, 'YYYYMMDD'));
   BEGIN
      --Obtenemos la  ultima fecha habil
      PKG_PAB_UTILITY.SP_PAB_DEV_DIA_HABIL_ANT (v_COD_PAIS, v_FECHA_AYER);

      BEGIN
           SELECT   SB.COD_BIC_BCO,
                    SB.COD_SIS_SAL,
                    SB.IMP_PAG_REC,
                    SB.IMP_PAG_ENV,
                    SB.IMP_POS_BIL
             INTO   v_COD_BIC_BCO,
                    v_COD_SIS_SAL,
                    v_IMP_PAG_REC,
                    v_IMP_PAG_ENV,
                    v_IMP_POS_BIL
             FROM   PABS_DT_SALDO_BILAT SB
            WHERE       SB.COD_SIS_SAL = p_CANAL
                    AND SB.COD_BIC_BCO = p_BANCO
                    AND SB.FEC_ING_CTL = v_FECHA_AYER
         ORDER BY   SB.COD_BIC_BCO ASC;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            err_code := SQLCODE;
            err_msg :=
                  SUBSTR (SQLERRM, 1, 300)
               || 'No hay Datos para el Banco: '
               || p_BANCO;
            RAISE;
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            p_s_mensaje := err_code || '-' || err_msg;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;


      BEGIN
         SELECT   FEC_REG_SDO,
                  HOR_REG_SDO,
                  COD_DVI,
                  COD_BIC_BCO,
                  COD_CNL_SAL,
                  IMP_SDO_APR,
                  IMP_NET_ING,
                  IMP_NET_EGR,
                  IMP_SDO_RAL,
                  IMP_SDO_ITE,
                  IMP_NET_RAL,
                  IMP_NET_MIN,
                  IMP_NET_MAX,
                  IMP_MIN_ING,
                  IMP_MIN_EGR,
                  IMP_MAX_ING,
                  IMP_MAX_EGR
           INTO   v_FEC_REG,
                  v_HOR_REG,
                  v_COD_DVI,
                  v_COD_BIC_BCO,
                  v_COD_CNL_SAL,
                  v_IMP_SDO_APR,
                  v_IMP_NET_ING,
                  v_IMP_NET_EGR,
                  v_IMP_SDO_RAL,
                  v_IMP_SDO_ITE,
                  v_IMP_NET_RAL,
                  v_IMP_NET_MIN,
                  v_IMP_NET_MAX,
                  v_IMP_MIN_ING,
                  v_IMP_MIN_EGR,
                  v_IMP_MAX_ING,
                  v_IMP_MAX_EGR
           FROM   PABS_DT_RGTRO_SALDO_HTRCO
          WHERE       COD_BIC_BCO = p_BANCO
                  AND COD_CNL_SAL = p_CANAL
                  AND FEC_REG_SDO = v_FECHA_NUM
                  AND HOR_REG_SDO IN
                           (SELECT   MAX (HOR_REG_SDO)
                              FROM   PABS_DT_RGTRO_SALDO_HTRCO
                             WHERE       COD_BIC_BCO = p_BANCO
                                     AND COD_CNL_SAL = p_CANAL
                                     AND FEC_REG_SDO = v_FECHA_NUM);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            v_IMP_SDO_RAL := 0;
            err_code := SQLCODE;
            err_msg :=
                  SUBSTR (SQLERRM, 1, 300)
               || 'No hay Datos para el Banco: '
               || p_BANCO;
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            p_s_mensaje := err_code || '-' || err_msg;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;


      IF v_HOR_REG IS NULL
      THEN                                -- Si es el primer registro del dia.
         v_HOR_REG := TO_NUMBER (TO_CHAR (SYSDATE, 'HH24MI'));
         v_IMP_SDO_APR := v_IMP_SDO_APR;
         v_IMP_NET_ING := v_IMP_PAG_REC;
         v_IMP_NET_EGR := v_IMP_PAG_ENV;
         v_IMP_SDO_RAL := v_IMP_PAG_REC - v_IMP_PAG_ENV;
         v_IMP_SDO_ITE := v_IMP_PAG_REC - v_IMP_PAG_ENV;
         v_IMP_NET_RAL := v_IMP_PAG_REC - v_IMP_PAG_ENV;
         v_IMP_NET_MIN := v_IMP_PAG_REC - v_IMP_PAG_ENV;
         v_IMP_NET_MAX := v_IMP_PAG_REC - v_IMP_PAG_ENV;
         v_IMP_MIN_ING := v_IMP_PAG_REC;
         v_IMP_MIN_EGR := v_IMP_PAG_ENV;
         v_IMP_MAX_ING := v_IMP_PAG_REC;
         v_IMP_MAX_EGR := v_IMP_PAG_ENV;

         BEGIN
            INSERT INTO PABS_DT_RGTRO_SALDO_HTRCO (FEC_REG_SDO,
                                                   HOR_REG_SDO,
                                                   COD_DVI,
                                                   COD_BIC_BCO,
                                                   COD_CNL_SAL,
                                                   IMP_SDO_APR,
                                                   IMP_NET_ING,
                                                   IMP_NET_EGR,
                                                   IMP_SDO_RAL,
                                                   IMP_SDO_ITE,
                                                   IMP_NET_RAL,
                                                   IMP_NET_MIN,
                                                   IMP_NET_MAX,
                                                   IMP_MIN_ING,
                                                   IMP_MIN_EGR,
                                                   IMP_MAX_ING,
                                                   IMP_MAX_EGR)
              VALUES   (v_FECHA_NUM,
                        v_HOR_REG,
                        PKG_PAB_CONSTANTES.V_STR_DVI_CLP,
                        p_BANCO,
                        p_CANAL,
                        v_IMP_SDO_APR,
                        v_IMP_NET_ING,
                        v_IMP_NET_EGR,
                        v_IMP_SDO_RAL,
                        v_IMP_SDO_ITE,
                        v_IMP_NET_RAL,
                        v_IMP_NET_MIN,
                        v_IMP_NET_MAX,
                        v_IMP_MIN_ING,
                        v_IMP_MIN_EGR,
                        v_IMP_MAX_ING,
                        v_IMP_MAX_EGR);
         EXCEPTION
            WHEN DUP_VAL_ON_INDEX
            THEN                                      -- si existe el registro
               err_code := SQLCODE;
               err_msg :=
                     'Ya existe registro, moneda '
                  || v_COD_DVI
                  || ', con hora: '
                  || v_HOR_REG;
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         END;

         p_ERROR := PKG_PAB_CONSTANTES.v_OK;
      ELSE                                                   --Nuevo registro.
         BEGIN
            SELECT   SUM (IMP_NET_ING), SUM (IMP_NET_EGR)
              INTO   v_IMP_NET_ING, v_IMP_NET_EGR
              FROM   PABS_DT_RGTRO_SALDO_HTRCO SH
             WHERE       COD_BIC_BCO = p_BANCO
                     AND COD_DVI = v_COD_DVI
                     AND COD_CNL_SAL = p_CANAL
                     AND FEC_REG_SDO = v_FECHA_NUM;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               v_HOR_REG := 0;
               err_code := SQLCODE;
               err_msg :=
                     SUBSTR (SQLERRM, 1, 300)
                  || 'No hay datos para el banco: '
                  || p_BANCO;
            WHEN OTHERS
            THEN
               err_code := SQLCODE;
               err_msg := SUBSTR (SQLERRM, 1, 300);
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
               p_s_mensaje := err_code || '-' || err_msg;
               RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
         END;

         IF v_IMP_POS_BIL = v_IMP_SDO_RAL
         THEN                                               -- Sin movimientos
            v_NET_ING := 0;
            v_NET_EGR := 0;
         ELSE
            v_NET_ING := v_IMP_PAG_REC - v_IMP_NET_ING;
            v_NET_EGR := v_IMP_PAG_ENV - v_IMP_NET_EGR;
         END IF;

         IF v_HOR_REG < 1000
         THEN
            v_HOR_REG2 := TO_CHAR ('0' || v_HOR_REG); -- agrega en caso: 800 --> 0800
         ELSE
            v_HOR_REG2 := v_HOR_REG;
         END IF;

         --Adiciona 30 minutos a la hora
         v_HORA :=
            TO_CHAR (
               TO_DATE (TO_CHAR (SYSDATE, 'YYYY-MM-DD') || '' || v_HOR_REG2,
                        'YYYY-MM-DD HH24:MI')
               + 30 / 1440,
               'HH24MI'
            );

         IF v_HORA_ACTUAL >= v_HORA
         THEN                     --Se ingresa, solo si ya pasaron 30 minutos.
            v_COD_CNL_SAL := p_CANAL;
            v_IMP_SDO_APR := 0;
            v_IMP_SDO_RAL := v_IMP_SDO_RAL + v_NET_ING - v_NET_EGR;
            v_IMP_NET_ING := v_NET_ING;
            v_IMP_NET_EGR := v_NET_EGR;

            v_IMP_SDO_ITE := v_NET_ING - v_NET_EGR;
            v_IMP_NET_RAL := v_NET_ING - v_NET_EGR;
            v_IMP_NET_MIN := v_NET_ING - v_NET_EGR;
            v_IMP_NET_MAX := v_NET_ING - v_NET_EGR;
            v_IMP_MIN_ING := v_NET_ING;
            v_IMP_MIN_EGR := v_NET_EGR;
            v_IMP_MAX_ING := v_NET_ING;
            v_IMP_MAX_EGR := v_NET_EGR;

            BEGIN
               INSERT INTO PABS_DT_RGTRO_SALDO_HTRCO (FEC_REG_SDO,
                                                      HOR_REG_SDO,
                                                      COD_DVI,
                                                      COD_BIC_BCO,
                                                      COD_CNL_SAL,
                                                      IMP_SDO_APR,
                                                      IMP_NET_ING,
                                                      IMP_NET_EGR,
                                                      IMP_SDO_RAL,
                                                      IMP_SDO_ITE,
                                                      IMP_NET_RAL,
                                                      IMP_NET_MIN,
                                                      IMP_NET_MAX,
                                                      IMP_MIN_ING,
                                                      IMP_MIN_EGR,
                                                      IMP_MAX_ING,
                                                      IMP_MAX_EGR)
                 VALUES   (v_FECHA_NUM,
                           TO_NUMBER (v_HORA_ACTUAL),
                           v_COD_DVI,
                           p_BANCO,
                           p_CANAL,
                           v_IMP_SDO_APR,
                           v_IMP_NET_ING,
                           v_IMP_NET_EGR,
                           v_IMP_SDO_RAL,
                           v_IMP_SDO_ITE,
                           v_IMP_NET_RAL,
                           v_IMP_NET_MIN,
                           v_IMP_NET_MAX,
                           v_IMP_MIN_ING,
                           v_IMP_MIN_EGR,
                           v_IMP_MAX_ING,
                           v_IMP_MAX_EGR);
            EXCEPTION                                 -- si existe el registro
               WHEN DUP_VAL_ON_INDEX
               THEN
                  err_code := SQLCODE;
                  err_msg :=
                        'Ya existe registro, moneda'
                     || v_COD_DVI
                     || ', con hora: '
                     || v_HOR_REG;
                  PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                      v_NOM_PCK,
                                                      err_msg,
                                                      v_NOM_SP);
                  p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            END;
         END IF;

         p_ERROR := PKG_PAB_CONSTANTES.v_OK;
      END IF;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_INS_SALDO_LIQDZ;

   /******************************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_INS_SALDO_MX_LIQDZ
   -- Objetivo: Procedimiento almacenado que inserta Registros de saldos  de moneda extranjnera
                ,para la tabla de consulta Saldos
   -- Sistema: DBO_PAB.
   -- Base de Datos: PABS_DT_DETLL_SALDO_CAJA_MN_MX
                     PABS_DT_RGTRO_SALDO_HTRCO
   -- Tablas Usadas:
   --
   -- Fecha: 17-08-2018
   -- Autor: Santander CAH
   -- Input:
   --      p:BANCO     Banco a buscar
   --      p_MONEDA    Moneda a buscar, para insertar saldo
   -- Output:

   --      p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --*******************************************************************************************************************/
   PROCEDURE SP_PAB_INS_SALDO_MX_LIQDZ (p_BANCO     IN     CHAR,
                                        p_COD_DVI   IN     CHAR,
                                        p_ERROR        OUT NUMBER)
   IS
      v_NOM_SP         VARCHAR2 (30) := 'SP_PAB_INS_SALDO_MX_LIQDZ';
      --Variables tabla SALDO_CAJA
      v_BANCO          PABS_DT_DETLL_SALDO_CAJA_MN_MX.COD_BCO_ORG%TYPE;
      v_SDO_APERTURA   PABS_DT_DETLL_SALDO_CAJA_MN_MX.IMP_APR_AOS%TYPE;
      v_INGRESOS       PABS_DT_DETLL_SALDO_CAJA_MN_MX.IMP_ING_DIA%TYPE;
      v_EGRESOS        PABS_DT_DETLL_SALDO_CAJA_MN_MX.IMP_EGR_DIA%TYPE;
      v_SDO_FINAL      PABS_DT_DETLL_SALDO_CAJA_MN_MX.IMP_RAL_AOS%TYPE;
      v_FEC_REG        PABS_DT_RGTRO_SALDO_HTRCO.FEC_REG_SDO%TYPE;
      v_HOR_REG        PABS_DT_RGTRO_SALDO_HTRCO.HOR_REG_SDO%TYPE;
      v_HOR_REG2       CHAR (4);
      v_COD_DVI        PABS_DT_RGTRO_SALDO_HTRCO.COD_DVI%TYPE;
      v_COD_BIC_BCO    PABS_DT_RGTRO_SALDO_HTRCO.COD_BIC_BCO%TYPE;
      v_COD_CNL_SAL    PABS_DT_RGTRO_SALDO_HTRCO.COD_CNL_SAL%TYPE;
      v_IMP_SDO_APR    PABS_DT_RGTRO_SALDO_HTRCO.IMP_SDO_APR%TYPE;
      v_IMP_NET_ING    PABS_DT_RGTRO_SALDO_HTRCO.IMP_NET_ING%TYPE;
      v_IMP_NET_EGR    PABS_DT_RGTRO_SALDO_HTRCO.IMP_NET_EGR%TYPE;
      v_IMP_SDO_RAL    PABS_DT_RGTRO_SALDO_HTRCO.IMP_SDO_RAL%TYPE;
      v_IMP_SDO_ITE    PABS_DT_RGTRO_SALDO_HTRCO.IMP_SDO_ITE%TYPE;
      v_IMP_NET_RAL    PABS_DT_RGTRO_SALDO_HTRCO.IMP_NET_RAL%TYPE;
      v_IMP_NET_MIN    PABS_DT_RGTRO_SALDO_HTRCO.IMP_NET_MIN%TYPE;
      v_IMP_NET_MAX    PABS_DT_RGTRO_SALDO_HTRCO.IMP_NET_MAX%TYPE;
      v_IMP_MIN_ING    PABS_DT_RGTRO_SALDO_HTRCO.IMP_MIN_ING%TYPE;
      v_IMP_MIN_EGR    PABS_DT_RGTRO_SALDO_HTRCO.IMP_MIN_EGR%TYPE;
      v_IMP_MAX_ING    PABS_DT_RGTRO_SALDO_HTRCO.IMP_MAX_ING%TYPE;
      v_IMP_MAX_EGR    PABS_DT_RGTRO_SALDO_HTRCO.IMP_MAX_EGR%TYPE;

      --variables para insert
      v_HORA_ACTUAL    NUMBER := TO_NUMBER (TO_CHAR (SYSDATE, 'HH24MI'));
      v_HORA           PABS_DT_RGTRO_SALDO_HTRCO.HOR_REG_SDO%TYPE;
      v_FECHA_NUM      NUMBER := TO_NUMBER (TO_CHAR (SYSDATE, 'YYYYMMDD'));
      v_ACUM_ING       NUMBER;
      v_ACUM_EGR       NUMBER;
   BEGIN
      BEGIN
         SELECT   SL.COD_BCO_ORG,
                  SL.IMP_APR_AOS,
                  SL.IMP_ING_DIA,
                  SL.IMP_EGR_DIA,
                  --SL.IMP_TOT_FNL_DIA
                  SL.IMP_RAL_AOS
           INTO   v_BANCO,
                  v_SDO_APERTURA,
                  v_INGRESOS,
                  v_EGRESOS,
                  v_SDO_FINAL
           FROM   PABS_DT_DETLL_SALDO_CAJA_MN_MX SL
          WHERE   SL.COD_DVI = p_COD_DVI --AND SL.FEC_ING_CTL < v_FECHA_AYER
                  AND SL.COD_BCO_ORG = p_BANCO;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            err_code := SQLCODE;
            err_msg :=
                  SUBSTR (SQLERRM, 1, 300)
               || 'No se puedo encontrar datos'
               || p_BANCO;
            RAISE;
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            p_s_mensaje := err_code || '-' || err_msg;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;


      BEGIN
         SELECT   FEC_REG_SDO,
                  HOR_REG_SDO,
                  COD_DVI,
                  COD_BIC_BCO,
                  COD_CNL_SAL,
                  IMP_SDO_APR,
                  IMP_NET_ING,
                  IMP_NET_EGR,
                  IMP_SDO_RAL,
                  IMP_SDO_ITE,
                  IMP_NET_RAL,
                  IMP_NET_MIN,
                  IMP_NET_MAX,
                  IMP_MIN_ING,
                  IMP_MIN_EGR,
                  IMP_MAX_ING,
                  IMP_MAX_EGR
           INTO   v_FEC_REG,
                  v_HOR_REG,
                  v_COD_DVI,
                  v_COD_BIC_BCO,
                  v_COD_CNL_SAL,
                  v_IMP_SDO_APR,
                  v_IMP_NET_ING,
                  v_IMP_NET_EGR,
                  v_IMP_SDO_RAL,
                  v_IMP_SDO_ITE,
                  v_IMP_NET_RAL,
                  v_IMP_NET_MIN,
                  v_IMP_NET_MAX,
                  v_IMP_MIN_ING,
                  v_IMP_MIN_EGR,
                  v_IMP_MAX_ING,
                  v_IMP_MAX_EGR
           FROM   PABS_DT_RGTRO_SALDO_HTRCO
          WHERE       COD_BIC_BCO = p_BANCO
                  AND COD_DVI = p_COD_DVI
                  AND COD_CNL_SAL = PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_SWF
                  AND FEC_REG_SDO = v_FECHA_NUM
                  AND HOR_REG_SDO IN
                           (SELECT   MAX (HOR_REG_SDO)
                              FROM   PABS_DT_RGTRO_SALDO_HTRCO
                             WHERE   COD_BIC_BCO = p_BANCO
                                     AND COD_DVI = p_COD_DVI
                                     AND COD_CNL_SAL =
                                           PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_SWF
                                     AND FEC_REG_SDO = v_FECHA_NUM);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            v_HOR_REG := 0;
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            p_s_mensaje := err_code || '-' || err_msg;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;


      IF v_HOR_REG = 0
      THEN                                        --Se ingresa primer registro
         v_HOR_REG := '0800';
         v_IMP_SDO_APR := v_SDO_APERTURA;
         v_IMP_NET_ING := v_INGRESOS;
         v_IMP_NET_EGR := v_EGRESOS;
         v_IMP_SDO_RAL := v_SDO_FINAL;
         v_IMP_SDO_ITE := v_INGRESOS - v_EGRESOS;
         v_IMP_NET_RAL := v_INGRESOS - v_EGRESOS;
         v_IMP_NET_MIN := v_INGRESOS - v_EGRESOS;
         v_IMP_NET_MAX := v_INGRESOS - v_EGRESOS;
         v_IMP_MIN_ING := v_INGRESOS;
         v_IMP_MIN_EGR := v_EGRESOS;
         v_IMP_MAX_ING := v_INGRESOS;
         v_IMP_MAX_EGR := v_EGRESOS;

         BEGIN
            --TABLA   en creacion
            INSERT INTO PABS_DT_RGTRO_SALDO_HTRCO (FEC_REG_SDO,
                                                   HOR_REG_SDO,
                                                   COD_DVI,
                                                   COD_BIC_BCO,
                                                   COD_CNL_SAL,
                                                   IMP_SDO_APR,
                                                   IMP_NET_ING,
                                                   IMP_NET_EGR,
                                                   IMP_SDO_RAL,
                                                   IMP_SDO_ITE,
                                                   IMP_NET_RAL,
                                                   IMP_NET_MIN,
                                                   IMP_NET_MAX,
                                                   IMP_MIN_ING,
                                                   IMP_MIN_EGR,
                                                   IMP_MAX_ING,
                                                   IMP_MAX_EGR)
              VALUES   (v_FECHA_NUM,
                        v_HOR_REG,
                        p_COD_DVI,
                        p_BANCO,
                        PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_SWF,
                        v_IMP_SDO_APR,
                        v_IMP_NET_ING,
                        v_IMP_NET_EGR,
                        v_IMP_SDO_RAL,
                        v_IMP_SDO_ITE,
                        v_IMP_NET_RAL,
                        v_IMP_NET_MIN,
                        v_IMP_NET_MAX,
                        v_IMP_MIN_ING,
                        v_IMP_MIN_EGR,
                        v_IMP_MAX_ING,
                        v_IMP_MAX_EGR);
         EXCEPTION
            WHEN DUP_VAL_ON_INDEX
            THEN                                      -- si existe el registro
               err_code := SQLCODE;
               err_msg :=
                     'Ya existe registro, moneda'
                  || v_COD_DVI
                  || ', con hora: '
                  || v_HOR_REG;
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         END;
      ELSE                                        --Se ingresa nuevo registro.
         BEGIN
            SELECT   SUM (IMP_NET_ING), SUM (IMP_NET_EGR)
              INTO   v_IMP_NET_ING, v_IMP_NET_EGR
              FROM   PABS_DT_RGTRO_SALDO_HTRCO SH
             WHERE   COD_BIC_BCO = p_BANCO AND COD_DVI = p_COD_DVI
                     AND COD_CNL_SAL =
                           PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_SWF
                     AND FEC_REG_SDO = v_FECHA_NUM;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               v_HOR_REG := 0;
            WHEN OTHERS
            THEN
               err_code := SQLCODE;
               err_msg := SUBSTR (SQLERRM, 1, 300);
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
               p_s_mensaje := err_code || '-' || err_msg;
               RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
         END;


         IF v_SDO_FINAL = v_IMP_SDO_RAL
         THEN                                               -- Sin movimientos
            v_IMP_NET_ING := 0;
            v_IMP_NET_EGR := 0;
         ELSE
            v_IMP_NET_ING := v_INGRESOS - v_IMP_NET_ING;
            v_IMP_NET_EGR := v_EGRESOS - v_IMP_NET_EGR;
         END IF;

         IF v_HOR_REG < 1000
         THEN
            v_HOR_REG2 := TO_CHAR ('0' || v_HOR_REG);
         ELSE
            v_HOR_REG2 := v_HOR_REG;
         END IF;

         -- v_HORA  :=  v_HOR_REG + '0030';
         v_HORA :=
            TO_CHAR (
               TO_DATE (TO_CHAR (SYSDATE, 'YYYY-MM-DD') || '' || v_HOR_REG2,
                        'YYYY-MM-DD HH24:MI')
               + 30 / 1440,
               'HH24MI'
            );

         IF v_HORA_ACTUAL >= v_HORA
         THEN
            v_IMP_SDO_APR := v_IMP_SDO_RAL;
            v_IMP_SDO_RAL := v_IMP_SDO_APR + v_IMP_NET_ING - v_IMP_NET_EGR;
            v_IMP_SDO_ITE := v_IMP_NET_ING - v_IMP_NET_EGR;
            v_IMP_NET_RAL := v_IMP_NET_ING - v_IMP_NET_EGR;
            v_IMP_NET_MIN := v_IMP_NET_ING - v_IMP_NET_EGR;
            v_IMP_NET_MAX := v_IMP_NET_ING - v_IMP_NET_EGR;
            v_IMP_MIN_ING := v_IMP_NET_ING;
            v_IMP_MIN_EGR := v_IMP_NET_EGR;
            v_IMP_MAX_ING := v_IMP_NET_ING;
            v_IMP_MAX_EGR := v_IMP_NET_EGR;

            BEGIN
               --TABLA   en creacion
               INSERT INTO PABS_DT_RGTRO_SALDO_HTRCO (FEC_REG_SDO,
                                                      HOR_REG_SDO,
                                                      COD_DVI,
                                                      COD_BIC_BCO,
                                                      COD_CNL_SAL,
                                                      IMP_SDO_APR,
                                                      IMP_NET_ING,
                                                      IMP_NET_EGR,
                                                      IMP_SDO_RAL,
                                                      IMP_SDO_ITE,
                                                      IMP_NET_RAL,
                                                      IMP_NET_MIN,
                                                      IMP_NET_MAX,
                                                      IMP_MIN_ING,
                                                      IMP_MIN_EGR,
                                                      IMP_MAX_ING,
                                                      IMP_MAX_EGR)
                 VALUES   (v_FECHA_NUM,
                           v_HORA,
                           v_COD_DVI,
                           p_BANCO,
                           PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_SWF,
                           v_IMP_SDO_APR,
                           v_IMP_NET_ING,
                           v_IMP_NET_EGR,
                           v_IMP_SDO_RAL,
                           v_IMP_SDO_ITE,
                           v_IMP_NET_RAL,
                           v_IMP_NET_MIN,
                           v_IMP_NET_MAX,
                           v_IMP_MIN_ING,
                           v_IMP_MIN_EGR,
                           v_IMP_MAX_ING,
                           v_IMP_MAX_EGR);
            EXCEPTION
               WHEN DUP_VAL_ON_INDEX
               THEN                                   -- si existe el registro
                  err_code := SQLCODE;
                  err_msg :=
                        'Ya existe registro, moneda'
                     || v_COD_DVI
                     || ', con hora:.. '
                     || v_HORA;
                  PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                      v_NOM_PCK,
                                                      err_msg,
                                                      v_NOM_SP);
                  p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            END;

            p_ERROR := PKG_PAB_CONSTANTES.v_OK;
         END IF;

         p_ERROR := PKG_PAB_CONSTANTES.v_OK;
      END IF;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_INS_SALDO_MX_LIQDZ;

   /******************************************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_RGTRO_SALDOS_DARIO
   -- Objetivo: Procedimiento almacenado que Busca los top montos maximos, minimos, luego
   --          registra en la tabla  PABS_DT_RGTRO_MAXMO_MINMO_DIA, al cierre del sistena.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_SALDO_HORA_ITDIA, PABS_DT_RGTRO_MAXMO_MINMO_DIA, PABS_DT_SALDO_MONDA_DARIO
   --
   -- Fecha: 09/08/2018
   -- Autor: Santander CAH
   -- Input:
   --      p_COD_DVI      Moneda para actualizar.
   -- Output:
   --       p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --******************************************************************************************************************************/
   PROCEDURE SP_PAB_RGTRO_SALDOS_DARIO (p_COD_DVI   IN     CHAR,
                                        p_ERROR        OUT NUMBER)
   IS
      v_NOM_SP        VARCHAR2 (30) := 'SP_PAB_RGTRO_SALDOS_DARIO';
      v_IMP_NET_MIN   NUMBER;
      v_IMP_NET_MAX   NUMBER;
      v_IMP_MIN_ING   NUMBER;
      v_IMP_MAX_ING   NUMBER;
      v_IMP_MIN_EGR   NUMBER;
      v_IMP_MAX_EGR   NUMBER;
   BEGIN
      BEGIN -- Buscamos los top de cada campo, de la tabla registros de saldos diarios.
         SELECT   MIN (IMP_NET_RAL),
                  MAX (IMP_NET_RAL),
                  MIN (IMP_NET_ING),
                  MAX (IMP_NET_ING),
                  MIN (IMP_NET_EGR),
                  MAX (IMP_NET_EGR)
           INTO   v_IMP_NET_MIN,
                  v_IMP_NET_MAX,
                  v_IMP_MIN_ING,
                  v_IMP_MAX_ING,
                  v_IMP_MIN_EGR,
                  v_IMP_MAX_EGR
           FROM   PABS_DT_SALDO_HORA_ITDIA
          WHERE   FEC_REG_SDO = v_FECHA_HOY -- TRUNC(SYSDATE - 1) Se actualiza para que tome en consideración la fecha actual del día al momento del cierre
                                           AND COD_DVI = p_COD_DVI;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            err_code := SQLCODE;
            err_msg :=
               'No se han encontrado registos de la moneda ' || p_COD_DVI;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_s_mensaje := err_code || '-' || err_msg;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;


      IF ( (v_IMP_NET_MIN = 0) OR (v_IMP_NET_MIN IS NULL))
      THEN
         SELECT   MIN (IMP_NET_RAL)
           INTO   v_IMP_NET_MIN
           FROM   PABS_DT_SALDO_HORA_ITDIA
          WHERE       FEC_REG_SDO = TRUNC (SYSDATE - 1)
                  AND COD_DVI = p_COD_DVI
                  AND IMP_NET_RAL > 0;

         IF v_IMP_NET_MIN IS NULL
         THEN
            v_IMP_NET_MIN := 0;
         END IF;
      END IF;


      IF ( (v_IMP_MIN_ING = 0) OR (v_IMP_MIN_ING IS NULL))
      THEN
         SELECT   MIN (IMP_NET_ING)
           INTO   v_IMP_MIN_ING
           FROM   PABS_DT_SALDO_HORA_ITDIA
          WHERE       FEC_REG_SDO = TRUNC (SYSDATE - 1)
                  AND COD_DVI = p_COD_DVI
                  AND IMP_NET_ING > 0;

         IF v_IMP_MIN_ING IS NULL
         THEN
            v_IMP_MIN_ING := 0;
         END IF;
      END IF;

      IF ( (v_IMP_MIN_EGR = 0) OR (v_IMP_MIN_EGR IS NULL))
      THEN
         SELECT   MIN (IMP_NET_EGR)
           INTO   v_IMP_MIN_EGR
           FROM   PABS_DT_SALDO_HORA_ITDIA
          WHERE       FEC_REG_SDO = TRUNC (SYSDATE - 1)
                  AND COD_DVI = p_COD_DVI
                  AND IMP_NET_EGR > 0;

         IF v_IMP_MIN_EGR IS NULL
         THEN
            v_IMP_MIN_EGR := 0;
         END IF;
      END IF;

      IF v_IMP_NET_MAX IS NULL
      THEN
         v_IMP_NET_MAX := 0;
      END IF;

      IF v_IMP_MAX_ING IS NULL
      THEN
         v_IMP_MAX_ING := 0;
      END IF;


      IF v_IMP_MAX_EGR IS NULL
      THEN
         v_IMP_MAX_EGR := 0;
      END IF;


      IF v_IMP_NET_MIN IS NOT NULL
      THEN
         BEGIN -- Insertamos en la tabla para registrar los min y max del dia/ al cierre.
            INSERT INTO PABS_DT_SALDO_MONDA_DARIO (FEC_REG,
                                                   COD_DVI,
                                                   IMP_NET_MIN,
                                                   IMP_NET_MAX,
                                                   IMP_MIN_ING,
                                                   IMP_MAX_ING,
                                                   IMP_MIN_EGR,
                                                   IMP_MAX_EGR)
              VALUES   (v_FECHA_HOY, -- TRUNC(SYSDATE-1) Se modifica para que almacene la fecha del día actual al cierre
                        p_COD_DVI,
                        v_IMP_NET_MIN,
                        v_IMP_NET_MAX,
                        v_IMP_MIN_ING,
                        v_IMP_MAX_ING,
                        v_IMP_MIN_EGR,
                        v_IMP_MAX_EGR);
         EXCEPTION
            WHEN DUP_VAL_ON_INDEX
            THEN
               err_code := SQLCODE;
               err_msg := 'Registro ya existe';
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            WHEN OTHERS
            THEN
               err_code := SQLCODE;
               err_msg := SUBSTR (SQLERRM, 1, 300);
               p_s_mensaje := err_code || '-' || err_msg;
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
               RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
         END;
      END IF;

      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_RGTRO_SALDOS_DARIO;

   /******************************************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_REC_ALERTA_MONEDA
   -- Objetivo: Procedimiento almacenado que recorre monedas y retorna si hay una alerta.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX
   --
   -- Fecha: 31-01-2019
   -- Autor: Santander CAH
   -- Input:
   --
   -- Output:
   --      p_ALERTA    --> Valor de alerta (0 no alertar, 1 si)
   --      p_ASUNTO    --> Asunto para notificar
   --      p_CORREO    --> Correo para notificar
   --      p_MENSAJE   --> Mensaje de la notificacion
   --       p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --******************************************************************************************************************************/
   PROCEDURE SP_PAB_REC_ALERTA_MONEDA (p_ALERTA    OUT NUMBER,
                                       p_ASUNTO    OUT VARCHAR2,
                                       p_CORREO    OUT VARCHAR2,
                                       p_MENSAJE   OUT VARCHAR2,
                                       p_ERROR     OUT NUMBER)
   IS
      v_NOM_SP            VARCHAR2 (30) := 'SP_PAB_REC_ALERTA_MONEDA';

      --NOTIFICACION
      v_ALERTA            NUMBER (1) := 0;
      v_ASUNTO            VARCHAR2 (3000) := '-';
      v_CORREO            VARCHAR2 (500);
      v_MENSAJE           VARCHAR2 (3000) := NULL;

      --*********************Declaracion Cursor C_CONSULTA_MONEDA************************
      CURSOR C_CONSULTA_MONEDA
      IS
           SELECT   COD_DVI
             FROM   PABS_DT_DETLL_SALDO_CAJA_MN_MX DS
            WHERE   DS.FEC_ING_CTL <= v_FECHA_HOY
         GROUP BY   COD_DVI
         ORDER BY   COD_DVI DESC;

      R_CONSULTA_MONEDA   C_CONSULTA_MONEDA%ROWTYPE;
   --*********************Declaracion Cursor C_CONSULTA_BANCOS************************

   BEGIN
      BEGIN
         OPEN C_CONSULTA_MONEDA;
      EXCEPTION
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            RAISE_APPLICATION_ERROR (-20000, err_msg);
      END;

      BEGIN
         FETCH C_CONSULTA_MONEDA INTO   R_CONSULTA_MONEDA;
      EXCEPTION
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);

            --Verificamos si el cursor se encuentra abierto en caso de error.
            IF (C_CONSULTA_MONEDA%ISOPEN)
            THEN
               CLOSE C_CONSULTA_MONEDA;
            END IF;

            RAISE_APPLICATION_ERROR (-20000, err_msg);
      END;

      WHILE C_CONSULTA_MONEDA%FOUND
      LOOP
         PKG_PAB_LIQUIDEZ.SP_PAB_ALRTA_SALDO (R_CONSULTA_MONEDA.COD_DVI,
                                              NULL,            --NULO MIENTRAS
                                              p_ALERTA,
                                              p_ASUNTO,
                                              p_CORREO,
                                              p_MENSAJE,
                                              p_ERROR);

         v_MENSAJE := v_MENSAJE || '#' || p_MENSAJE;

         IF p_ALERTA = 1
         THEN
            v_ALERTA := p_ALERTA;
         END IF;

         IF p_ASUNTO IS NOT NULL
         THEN
            v_ASUNTO := p_ASUNTO;
         END IF;

         IF p_CORREO IS NOT NULL
         THEN
            v_CORREO := p_CORREO;
         END IF;

         FETCH C_CONSULTA_MONEDA INTO   R_CONSULTA_MONEDA;
      END LOOP;

      v_MENSAJE := REPLACE (v_MENSAJE, '#####', '#');
      v_MENSAJE := REPLACE (v_MENSAJE, '####', '#');
      v_MENSAJE := REPLACE (v_MENSAJE, '###', '#');
      v_MENSAJE := REPLACE (v_MENSAJE, '##', '#');
      p_MENSAJE := v_MENSAJE;
      p_ASUNTO := v_ASUNTO;
      p_ALERTA := v_ALERTA;
      p_CORREO := v_CORREO;

      CLOSE C_CONSULTA_MONEDA;
   EXCEPTION
      WHEN OTHERS
      THEN
         --Verificamos si el cursor se encuentra abierto en caso de error.
         IF (C_CONSULTA_MONEDA%ISOPEN)
         THEN
            CLOSE C_CONSULTA_MONEDA;
         END IF;

         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_REC_ALERTA_MONEDA;

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_ALRTA_SALDO
   -- Objetivo: Procedimiento que retorna si existe alerta por Saldo cerca de loas limites
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas:  PABS_DT_SALDO_HORA_ITDIA, PABS_DT_MONDA_ALMOT, PABS_DT_NTFCC_ALMOT
   -- Fecha: 08/10/2018
   -- Autor: Santander CAH
   -- Input:
   --      p_MONEDA    --> Moneda para buscar saldo
   --     P_TIP_ALRTA    --> Flag indicador de alerta
   -- Output:
   --      p_ALERTA    --> Valor de alerta (0 no alertar, 1 si)
   --      p_ASUNTO    --> Asunto para notificar
   --      p_CORREO    --> Correo para notificar
   --      p_MENSAJE   --> Mensaje de la notificacion
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_ALRTA_SALDO (p_MONEDA      IN     CHAR,
                                 P_TIP_ALRTA   IN     CHAR DEFAULT NULL ,
                                 p_ALERTA         OUT NUMBER,
                                 p_ASUNTO         OUT VARCHAR2,
                                 p_CORREO         OUT VARCHAR2,
                                 p_MENSAJE        OUT VARCHAR2,
                                 p_ERROR          OUT NUMBER)
   IS
      V_NOM_SP            VARCHAR2 (30) := 'SP_PAB_ALRTA_SALDO';
      v_COD_NOT_MAX       CHAR (5) := 'ALMAX';
      v_COD_NOT_MIN       CHAR (5) := 'ALMIN';
      v_COD_TPO_OPE       CHAR (5);

      v_IMP_LMT_MAX       NUMBER;
      v_IMP_LMT_MIN       NUMBER;
      v_DIF_SDO_POR       NUMBER;

      v_HORA              PABS_DT_SALDO_HORA_ITDIA.HOR_REG_SDO%TYPE;
      v_MONEDA            PABS_DT_SALDO_HORA_ITDIA.COD_DVI%TYPE;
      v_SALDO_ACTUAL      PABS_DT_SALDO_HORA_ITDIA.IMP_SDO_RAL_AOS%TYPE;
      v_SALDO             VARCHAR2 (25);
      v_MON_ALERTA_MIN    NUMBER;
      v_MON_ALERTA_MAX    NUMBER;

      v_DIF_SDO_POR_MAX   NUMBER;
      v_DIF_SDO_POR_MIN   NUMBER;

      --Horas de notificar por Correo....
      v_HORA_ACTUAL       CHAR (4) := TO_CHAR (SYSDATE, 'HH24MI');
      v_NOTIFICAR_30_MI   NUMBER := 30;
      v_NOTIFICAR_15_MI   NUMBER := 15;
      v_HORA_NOTIFICAR    NUMBER;
      v_HORA_CAJA_INI     CHAR (4) := '1600';
      v_HORA_CAJA_FIN     CHAR (4) := '1730';             --1730 HORA OFICIAAL
      v_ULTIMA_HORA       CHAR (4);
      v_NUM_GLOSA         NUMBER;
      v_MON_GLS           CHAR (3);
      v_STR_MONDA         CHAR (6) := 'moneda';
   BEGIN
      --CONSULTAMOS MEDIO DE ALERTA (CORREO O PANTALLA)
      IF P_TIP_ALRTA IS NULL
      THEN
         --ALERTA POR CORREO

         IF v_HORA_ACTUAL <= v_HORA_CAJA_FIN
         THEN
            --Despues de las 17:30 no se debe notificar.

            BEGIN
               --Buscamos la ultima hora de la notificacion.
               SELECT   TO_CHAR (NT.FEC_ING_NTF, 'HH24MI')
                 INTO   v_ULTIMA_HORA
                 FROM   PABS_DT_NTFCC_ALMOT NT
                WHERE   NT.FEC_ING_NTF =
                           (SELECT   MAX (NT.FEC_ING_NTF)
                              FROM   PABS_DT_NTFCC_ALMOT NT
                             WHERE   NT.COD_TIP_CRO IN
                                           (PKG_PAB_CONSTANTES.V_COD_TIP_CRO_ALMAX,
                                            PKG_PAB_CONSTANTES.V_COD_TIP_CRO_ALMIN)
                                     AND SUBSTR (
                                           NT.GLS_MTV_NTF,
                                           INSTR (NT.GLS_MTV_NTF,
                                                  v_STR_MONDA)
                                           + 7,
                                           3
                                        ) = p_MONEDA
                                     AND TRUNC (NT.FEC_ING_NTF) =
                                           TRUNC (SYSDATE))
                        AND TRUNC (NT.FEC_ING_NTF) = TRUNC (SYSDATE);
            --AND SUBSTR(NT.GLS_MTV_NTF,INSTR(NT.GLS_MTV_NTF, v_STR_MONDA)+7,3)=p_MONEDA;


            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  v_ULTIMA_HORA := 0;
               WHEN OTHERS
               THEN
                  err_code := SQLCODE;
                  err_msg := SUBSTR (SQLERRM, 1, 300);
                  PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                      v_NOM_PCK,
                                                      err_msg,
                                                      v_NOM_SP);
                  p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;

                  p_s_mensaje := err_code || '-' || err_msg;
                  RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
            END;


            -- Se verifica periodo de Notificacion de 16 a 17:30 , notificar cad 15 minutos..resto del dia cada 30 minutos
            IF v_HORA_ACTUAL >= v_HORA_CAJA_INI
            THEN
               --<= v_HORA_CAJA_FIN THEN

               --Notificar cada 15 min.
               v_HORA_NOTIFICAR := v_NOTIFICAR_15_MI;
            ELSE
               --Notificar cada 30 min.

               v_HORA_NOTIFICAR := v_NOTIFICAR_30_MI;
            END IF;


            --Se suma X minutos mas a la hora
            v_ULTIMA_HORA :=
               TO_CHAR (
                  TO_DATE (
                     TO_CHAR (SYSDATE, 'YYYY-MM-DD') || ' ' || v_ULTIMA_HORA,
                     'YYYY-MM-DD HH24:MI'
                  )
                  + v_HORA_NOTIFICAR / 1440,
                  'HH24MI'
               );


            IF v_HORA_ACTUAL >= v_ULTIMA_HORA
            THEN
               --Notificar.

               BEGIN
                  --SALDO ACTUAL X MONEDA
                  SELECT   SH.COD_DVI, SH.HOR_REG_SDO, SH.IMP_SDO_RAL_AOS
                    INTO   v_MONEDA, v_HORA, v_SALDO_ACTUAL
                    FROM   PABS_DT_SALDO_HORA_ITDIA SH
                   WHERE   SH.COD_DVI = p_MONEDA
                           AND FEC_REG_SDO = v_FECHA_HOY
                           AND SH.HOR_REG_SDO IN
                                    (SELECT   MAX (SH.HOR_REG_SDO)
                                       FROM   PABS_DT_SALDO_HORA_ITDIA SH
                                      WHERE   SH.COD_DVI = p_MONEDA
                                              AND FEC_REG_SDO = v_FECHA_HOY);
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     err_code := SQLCODE;
                     err_msg :=
                        'No se han encontrado registos de moneda: '
                        || p_MONEDA;
                     PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                         v_NOM_PCK,
                                                         err_msg,
                                                         v_NOM_SP);
                     p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
                  WHEN OTHERS
                  THEN
                     err_code := SQLCODE;
                     err_msg := SUBSTR (SQLERRM, 1, 300);
                     PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                         v_NOM_PCK,
                                                         err_msg,
                                                         v_NOM_SP);
                     p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;

                     p_s_mensaje := err_code || '-' || err_msg;
                     RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
               END;

               BEGIN
                  -- Buscamos Limites y porcentajes de cada moneda
                  SELECT   IMP_LMT_MAX,
                           IMP_LMT_MIN,
                           (IMP_LMT_MAX * POR_ALR_MAX) / 100,
                           (IMP_LMT_MIN * POR_ALR_MIN) / 100
                    INTO   v_IMP_LMT_MAX,
                           v_IMP_LMT_MIN,
                           v_MON_ALERTA_MAX,
                           v_MON_ALERTA_MIN
                    FROM   PABS_DT_MONDA_ALMOT MO
                   WHERE   MO.COD_DVI = p_MONEDA;
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     err_code := SQLCODE;
                     err_msg :=
                        'No se han encontrado registos de la moneda:'
                        || p_MONEDA;
                     PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                         v_NOM_PCK,
                                                         err_msg,
                                                         v_NOM_SP);
                     p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
                  WHEN OTHERS
                  THEN
                     err_code := SQLCODE;
                     err_msg := SUBSTR (SQLERRM, 1, 300);
                     PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                         v_NOM_PCK,
                                                         err_msg,
                                                         v_NOM_SP);
                     p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
                     p_s_mensaje := err_code || '-' || err_msg;
                     RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
               END;


               --NUEVO
               v_DIF_SDO_POR_MAX := v_IMP_LMT_MAX - v_MON_ALERTA_MAX;

               IF v_IMP_LMT_MIN < 0
               THEN
                  v_DIF_SDO_POR_MIN := v_IMP_LMT_MIN - v_MON_ALERTA_MIN;
               ELSE
                  v_DIF_SDO_POR_MIN := v_IMP_LMT_MIN + v_MON_ALERTA_MIN;
               END IF;


               --NUEVO
               IF v_SALDO_ACTUAL >= v_DIF_SDO_POR_MAX
               THEN
                  IF v_MONEDA = PKG_PAB_CONSTANTES.V_STR_DVI_CLP
                  THEN
                     v_SALDO :=
                        '$ '
                        || TO_CHAR (v_SALDO_ACTUAL, 'FM999,999,999,999,999');
                  ELSE
                     v_SALDO :=
                        '$ '
                        || TO_CHAR (v_SALDO_ACTUAL, 'FM999,999,999,999.00');
                  END IF;

                  v_SALDO := REPLACE (v_SALDO, '.', '#');
                  v_SALDO := REPLACE (v_SALDO, ',', '.');
                  v_SALDO := REPLACE (v_SALDO, '#', ',');



                  --Insertamos lo notificacion.
                  PKG_PAB_LIQUIDEZ.SP_PAB_NTFCC_SALDO (
                     v_COD_NOT_MAX,
                     v_SALDO,
                     TO_CHAR (SYSDATE, 'HH24:MI'),
                     v_MONEDA,
                     p_MENSAJE,
                     P_ASUNTO,
                     P_CORREO,
                     P_ERROR
                  );
                  p_ALERTA := 1;
               ELSIF v_SALDO_ACTUAL <= v_DIF_SDO_POR_MIN
               THEN
                  IF v_MONEDA = PKG_PAB_CONSTANTES.V_STR_DVI_CLP
                  THEN
                     v_SALDO :=
                        '$ '
                        || TO_CHAR (v_SALDO_ACTUAL, 'FM999,999,999,999,999');
                  ELSE
                     v_SALDO :=
                        '$ '
                        || TO_CHAR (v_SALDO_ACTUAL, 'FM999,999,999,999.00');
                  END IF;

                  v_SALDO := REPLACE (v_SALDO, '.', '#');
                  v_SALDO := REPLACE (v_SALDO, ',', '.');
                  v_SALDO := REPLACE (v_SALDO, '#', ',');

                  --Insertamos lo notificacion.
                  PKG_PAB_LIQUIDEZ.SP_PAB_NTFCC_SALDO (
                     v_COD_NOT_MIN,
                     v_SALDO,
                     TO_CHAR (SYSDATE, 'HH24:MI'),
                     p_MONEDA,
                     p_MENSAJE,
                     P_ASUNTO,
                     P_CORREO,
                     P_ERROR
                  );
                  p_ALERTA := 1;
               ELSE
                  p_ALERTA := 0;
                  p_ASUNTO := NULL;
                  p_CORREO := NULL;
                  p_MENSAJE := NULL;
               END IF;
            END IF;
         ELSE
            -- Fuera de horario

            p_ALERTA := 0;
         END IF;
      ELSE                                               --ALERTA POR PANTALLA
         BEGIN
            --SALDO ACTUAL X MONEDA
            SELECT   SH.COD_DVI, SH.HOR_REG_SDO, SH.IMP_SDO_RAL_AOS
              INTO   v_MONEDA, v_HORA, v_SALDO_ACTUAL
              FROM   PABS_DT_SALDO_HORA_ITDIA SH
             WHERE   SH.COD_DVI = p_MONEDA AND FEC_REG_SDO = TRUNC (SYSDATE)
                     AND SH.HOR_REG_SDO IN
                              (SELECT   MAX (SH.HOR_REG_SDO)
                                 FROM   PABS_DT_SALDO_HORA_ITDIA SH
                                WHERE   SH.COD_DVI = p_MONEDA
                                        AND FEC_REG_SDO = TRUNC (SYSDATE));
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               err_code := SQLCODE;
               err_msg :=
                  'No se han encontrado registos de moneda:' || p_MONEDA;
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            WHEN OTHERS
            THEN
               err_code := SQLCODE;
               err_msg := SUBSTR (SQLERRM, 1, 300);
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;

               p_s_mensaje := err_code || '-' || err_msg;
               RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
         END;

         BEGIN
            --BUSCAMOS LIMITES Y PORCENTAJES DE CADA MONEDA
            SELECT   IMP_LMT_MAX,
                     IMP_LMT_MIN,
                     (IMP_LMT_MAX * POR_ALR_MAX) / 100,
                     (IMP_LMT_MIN * POR_ALR_MIN) / 100
              INTO   v_IMP_LMT_MAX,
                     v_IMP_LMT_MIN,
                     v_MON_ALERTA_MAX,
                     v_MON_ALERTA_MIN
              FROM   PABS_DT_MONDA_ALMOT MO
             WHERE   MO.COD_DVI = p_MONEDA;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               err_code := SQLCODE;
               err_msg :=
                  'No se han encontrado registos de la moneda:' || p_MONEDA;
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            WHEN OTHERS
            THEN
               err_code := SQLCODE;
               err_msg := SUBSTR (SQLERRM, 1, 300);
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
               p_s_mensaje := err_code || '-' || err_msg;
               RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
         END;



         --NUEVO
         -- v_DIF_SDO_POR_MAX := v_IMP_LMT_MAX - v_MON_ALERTA_MAX;

         IF v_IMP_LMT_MIN < 0
         THEN
            v_DIF_SDO_POR_MIN := v_IMP_LMT_MIN - v_MON_ALERTA_MIN;
         ELSE
            v_DIF_SDO_POR_MIN := v_IMP_LMT_MIN + v_MON_ALERTA_MIN;
         END IF;


         IF v_IMP_LMT_MAX < 0
         THEN
            v_DIF_SDO_POR_MAX := v_IMP_LMT_MAX + v_MON_ALERTA_MAX;
         ELSE
            v_DIF_SDO_POR_MAX := v_IMP_LMT_MAX - v_MON_ALERTA_MAX;
         END IF;

         -- IF  v_DIF_SDO_POR_MAX >= v_MON_ALERTA_MAX THEN
         IF v_SALDO_ACTUAL >= v_DIF_SDO_POR_MAX
         THEN
            IF v_MONEDA = PKG_PAB_CONSTANTES.V_STR_DVI_CLP
            THEN
               v_SALDO :=
                  '$ '
                  || TO_CHAR (v_SALDO_ACTUAL, 'FM999,999,999,999,999,999');
            ELSE
               v_SALDO :=
                  '$ '
                  || TO_CHAR (v_SALDO_ACTUAL, 'FM999,999,999,999,999.00');
            END IF;


            v_SALDO := REPLACE (v_SALDO, '.', '#');
            v_SALDO := REPLACE (v_SALDO, ',', '.');
            v_SALDO := REPLACE (v_SALDO, '#', ',');


            SELECT   COR.GLS_NOT_USR                            -- GLS_MSJ_CRO
              INTO   p_MENSAJE
              FROM   PABS_DT_DETLL_CRREO_ALMOT COR
             WHERE   COD_TPO_CRO = v_COD_NOT_MAX;

            p_MENSAJE := REPLACE (p_MENSAJE, '%Saldo', v_SALDO);
            p_MENSAJE :=
               REPLACE (p_MENSAJE, '%Hora', TO_CHAR (SYSDATE, 'HH24:MI'));

            IF v_SALDO_ACTUAL < v_IMP_LMT_MAX
            THEN
               p_MENSAJE := REPLACE (p_MENSAJE, 'excede', 'Se acerca');
            END IF;


            p_ALERTA := 1;
         --ELSIF v_DIF_SDO_POR_MIN <= v_MON_ALERTA_MIN THEN
         ELSIF v_SALDO_ACTUAL <= v_DIF_SDO_POR_MIN
         THEN
            IF v_MONEDA = PKG_PAB_CONSTANTES.V_STR_DVI_CLP
            THEN
               v_SALDO :=
                  '$ ' || TO_CHAR (v_SALDO_ACTUAL, 'FM999,999,999,999,999');
            ELSE
               v_SALDO :=
                  '$ ' || TO_CHAR (v_SALDO_ACTUAL, 'FM999,999,999,999.00');
            END IF;

            v_SALDO := REPLACE (v_SALDO, '.', '#');
            v_SALDO := REPLACE (v_SALDO, ',', '.');
            v_SALDO := REPLACE (v_SALDO, '#', ',');

            --Busca glosa para mensaje de grafico
            SELECT   GLS_NOT_USR                                 --GLS_MSJ_CRO
              INTO   p_MENSAJE
              FROM   PABS_DT_DETLL_CRREO_ALMOT COR
             WHERE   COD_TPO_CRO = v_COD_NOT_MIN;

            p_MENSAJE := REPLACE (p_MENSAJE, '%Saldo', v_SALDO);
            p_MENSAJE :=
               REPLACE (p_MENSAJE, '%Hora', TO_CHAR (SYSDATE, 'HH24:MI'));

            IF v_SALDO_ACTUAL > v_IMP_LMT_MIN
            THEN
               p_MENSAJE := REPLACE (p_MENSAJE, 'es Inferior', 'Se acerca');
            END IF;

            p_ALERTA := 1;
         ELSE
            p_ALERTA := 0;
            p_ASUNTO := NULL;
            p_CORREO := NULL;
            p_MENSAJE := NULL;
         END IF;
      END IF;

      IF p_ALERTA IS NULL
      THEN
         p_ALERTA := 0;
         p_ASUNTO := NULL;
         p_CORREO := NULL;
         p_MENSAJE := NULL;
      END IF;


      P_ERROR := PKG_PAB_CONSTANTES.V_OK;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_ALERTA := 0;
         p_ASUNTO := NULL;
         p_CORREO := NULL;
         p_MENSAJE := NULL;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_ALERTA := 0;
         p_ASUNTO := NULL;
         p_CORREO := NULL;
         p_MENSAJE := NULL;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_ALRTA_SALDO;

   /************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_NTFCC_SALDO
   -- Objetivo: Procedimiento almacenado que retorna el cuerpo del correo para la notificacion
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_CRREO_ALMOT
   --
   -- Fecha: 07/02/2019
   -- Autor: Santander CAH
   -- Input:
   --   p_COD_TPO_OPE     Descripcion de la notificacion
   --   p_SALDO
   --   p_HORA
   --   p_MONEDA
   -- Output:
   --     MENSAJE_SWF         Contenido de mensaje
   --     p_ASUNTO           Asunto correo
   --     p_CORREO          Direccion de correo
   --     p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_PAB_NTFCC_SALDO (p_COD_TPO_OPE   IN     CHAR,
                                 p_SALDO         IN     VARCHAR2,
                                 p_HORA          IN     VARCHAR2,
                                 p_MONEDA        IN     VARCHAR2,
                                 MENSAJE_SWF        OUT VARCHAR2,
                                 p_ASUNTO           OUT VARCHAR2,
                                 p_CORREO           OUT VARCHAR2,
                                 p_ERROR            OUT NUMBER)
   IS
      --
      v_NOM_SP            VARCHAR2 (30) := 'SP_PAB_NTFCC_SALDO';
      v_COD_MT            VARCHAR2 (30);
      v_MONTO_TOTAL       NUMBER (18, 2);
      v_MONTO_TOTAL_STR   VARCHAR2 (25);
      v_BENEFICIARIO      VARCHAR2 (50);
      v_NUM_OPE           VARCHAR2 (30);
      v_NUM_NOM           VARCHAR2 (50);
      v_MERCADO           VARCHAR2 (30);
      v_MENSAJE           VARCHAR2 (500);
      v_MONEDA            VARCHAR2 (10);
      v_ID                VARCHAR2 (100);
      V_COD_TPO_OPE       CHAR (5) := p_COD_TPO_OPE;
      ---Modificar variables para recibir rol y area.
      v_COD_SIS_ENT_SAL   CHAR (10);
      v_COD_TPO_ROL_AOS   NUMBER (2);
      v_MENSAJE_NTF       VARCHAR2 (500);
      v_ASUNTO            VARCHAR2 (300) := 'Mensajeria de Altos Montos - ';
      v_ASUNTO2           VARCHAR2 (300);
      v_NUM_OPE2          VARCHAR2 (12);
      v_NUM_FOL_OPE       NUMBER;
      v_FEC_ISR_OPE       DATE;
      v_NUM_REF_SWF       CHAR (16);
      v_GLS_ADC_EST       VARCHAR2 (500);
      v_FLG_VGN_NTF       NUMBER (1);
      v_DESC_AREA         VARCHAR2 (50);
      v_OBS_OPC_SWF       VARCHAR2 (200);
      v_GLS_CRO_NTF       VARCHAR2 (100);
      ERROR_REFSWF EXCEPTION;
   BEGIN
      BEGIN
         --Obtenemos informacion para generar correo
         --Busca glosa para correo y adjunto si tiene, tambien glosa notificacion pantalla
         SELECT   REPLACE (GLS_MSJ_CRO, '%Ope', v_NUM_OPE),
                  REPLACE (GLS_NOT_USR, '%Ope', v_NUM_OPE),
                  v_ASUNTO || COR.GLS_ENC_CRO,
                  COR.FLG_VGN_NTF,
                  COR.GLS_CRO_NTF,
                  COR.COD_TPO_ROL_AOS
           INTO   v_MENSAJE,
                  v_MENSAJE_NTF,
                  p_ASUNTO,
                  v_FLG_VGN_NTF,
                  p_CORREO,
                  v_COD_TPO_ROL_AOS
           FROM   PABS_DT_DETLL_CRREO_ALMOT COR
          WHERE   COD_TPO_CRO = V_COD_TPO_OPE;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            err_code := SQLCODE;
            err_msg := 'No se hay data para el codigo: ' || V_COD_TPO_OPE;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;

            p_s_mensaje := err_code || '-' || err_msg;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;

      p_ASUNTO :=
         'Las siguiente(s) moneda(s) se ecuentran en los limites establecidos';
      --Formateo de monto
      v_MONTO_TOTAL_STR := REPLACE (p_SALDO, '.', '#');
      v_MONTO_TOTAL_STR := REPLACE (p_SALDO, ',', '.');
      v_MONTO_TOTAL_STR := REPLACE (p_SALDO, '#', ',');

      CASE
         WHEN V_COD_TPO_OPE = PKG_PAB_CONSTANTES.V_COD_TIP_CRO_ALMIN
         THEN
            v_COD_SIS_ENT_SAL := PKG_PAB_CONSTANTES.V_COD_TIP_ALC_LIQ;
            v_MENSAJE_NTF :=
               REPLACE (v_MENSAJE_NTF, '%Saldo', v_MONTO_TOTAL_STR);
            v_MENSAJE_NTF := REPLACE (v_MENSAJE_NTF, '%Hora', p_HORA);
            v_MENSAJE_NTF := REPLACE (v_MENSAJE_NTF, '%Divisa', p_MONEDA);
            v_MENSAJE := REPLACE (v_MENSAJE, '%Saldo', v_MONTO_TOTAL_STR);
            v_MENSAJE := REPLACE (v_MENSAJE, '%Hora', p_HORA);
            v_MENSAJE := REPLACE (v_MENSAJE, '%Divisa', p_MONEDA);
         WHEN V_COD_TPO_OPE = PKG_PAB_CONSTANTES.V_COD_TIP_CRO_ALMAX
         THEN
            v_COD_SIS_ENT_SAL := PKG_PAB_CONSTANTES.V_COD_TIP_ALC_LIQ;
            v_MENSAJE_NTF :=
               REPLACE (v_MENSAJE_NTF, '%Saldo', v_MONTO_TOTAL_STR);
            v_MENSAJE_NTF := REPLACE (v_MENSAJE_NTF, '%Hora', p_HORA);
            v_MENSAJE_NTF := REPLACE (v_MENSAJE_NTF, '%Divisa', p_MONEDA);
            v_MENSAJE := REPLACE (v_MENSAJE, '%Saldo', v_MONTO_TOTAL_STR);
            v_MENSAJE := REPLACE (v_MENSAJE, '%Hora', p_HORA);
            v_MENSAJE := REPLACE (v_MENSAJE, '%Divisa', p_MONEDA);
         ELSE
            NULL;
      END CASE;

      MENSAJE_SWF := v_MENSAJE;

      PKG_PAB_NOTIFICACION.Sp_PAB_INS_NTFCC_ALMOT (V_COD_TPO_OPE,
                                                   v_COD_SIS_ENT_SAL,
                                                   v_COD_TPO_ROL_AOS,
                                                   v_MONTO_TOTAL,
                                                   v_NUM_OPE,
                                                   v_NUM_NOM,
                                                   v_MENSAJE,
                                                   p_ERROR);

      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := 'No hay data para la operacion ' || p_COD_TPO_OPE;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
   END SP_PAB_NTFCC_SALDO;


   /****************************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_BUS_MONIT_MONEDA
   -- Objetivo: Procedimiento almacenado para graficos Liquidez intradia
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas:  PABS_DT_SISTM_ENTRD_SALID
   --                 PABS_DT_SALDO_HORA_ITDIA
   --                  PABS_DT_MONDA_ALMOT
   --
   -- Fecha: 06-02-2018
   -- Autor: Santander CAH
   -- Input:
   --      p_MONEDA     Moneda a buscar
   -- Output:
   --          p_HORARIO    Cursor con data parametrica para grafico
   --          p_GRAFICO    Cursor con data para grafico
   --          p_ERROR:      Indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --****************************************************************************************************************/
   PROCEDURE SP_PAB_BUS_MONIT_MONEDA (p_MONEDA    IN     CHAR,
                                      p_HORARIO      OUT SYS_REFCURSOR,
                                      p_GRAFICO      OUT SYS_REFCURSOR,
                                      p_ERROR        OUT NUMBER)
   IS
      v_NOM_SP      VARCHAR2 (30) := 'SP_PAB_BUS_MONIT_MONEDA';
      v_MONEDA      CHAR (3);

      p_ALERTA      NUMBER (1) := 0;
      p_ASUNTO      VARCHAR2 (3000) := 'Inicio';
      p_CORREO      VARCHAR2 (500);
      p_MENSAJE     VARCHAR2 (3000) := NULL;
      v_MENSAJE_1 VARCHAR2 (100)
            := 'Saldo actual sobrepasa los limites establecidos' ;
      v_MENSAJE_2   VARCHAR2 (100) := 'Sin alerta';
      V_TIP_ALRTA   CHAR (1) := '1';
   BEGIN
      --MONEDA
      IF p_MONEDA IS NULL
      THEN
         v_MONEDA := PKG_PAB_CONSTANTES.V_STR_DVI_USD;
      ELSE
         v_MONEDA := p_MONEDA;
      END IF;

      BEGIN                                                          --Grafico
         OPEN p_GRAFICO FOR
              SELECT   DECODE (RPAD (HOR_REG_SDO, 1),
                               8, 0 || TO_CHAR (HOR_REG_SDO),
                               9, 0 || TO_CHAR (HOR_REG_SDO),
                               HOR_REG_SDO)
                          AS HORA,
                       IMP_SDO_RAL_AOS AS SALDO,
                       IMP_NET_PYN AS PROYECCION
                FROM   PABS_DT_SALDO_HORA_ITDIA SH
               WHERE   FEC_REG_SDO = v_FECHA_HOY            -- TRUNC (SYSDATE)
                                                AND COD_DVI = v_MONEDA
            ORDER BY   HOR_REG_SDO ASC;


         p_ERROR := PKG_PAB_CONSTANTES.v_OK;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            err_code := SQLCODE;
            err_msg := 'No hay datos para la moneda: ' || v_MONEDA;
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            p_s_mensaje := err_code || '-' || err_msg;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;

      PKG_PAB_LIQUIDEZ.SP_PAB_ALRTA_SALDO (v_MONEDA,
                                           V_TIP_ALRTA, --1 PARA ALERTA PANTALLA.
                                           p_ALERTA,
                                           p_ASUNTO, --generico -> Las siguinets monedas se encunatra cerca de los limites max o min
                                           p_CORREO,
                                           p_MENSAJE,
                                           p_ERROR);

      IF p_ALERTA IS NULL
      THEN
         p_ALERTA := 0;
      END IF;


      BEGIN
         OPEN p_HORARIO FOR
            SELECT   'HORARIO' AS DESCRIPCION,
                     DECODE (RPAD (SI.HOR_ICO_SIS, 1),
                             8, 0 || TO_CHAR (SI.HOR_ICO_SIS),
                             9, 0 || TO_CHAR (SI.HOR_ICO_SIS),
                             SI.HOR_ICO_SIS)
                        AS INICIO,
                     TO_CHAR (SI.HOR_CRR_SIS) AS FIN
              FROM   PABS_DT_SISTM_ENTRD_SALID SI
             WHERE   SI.COD_SIS_ENT_SAL =
                        PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SE_MESA
            UNION
            SELECT   'LIMITES' AS DESCRIPCION,
                     TO_CHAR (IMP_LMT_MIN) AS INICIO,
                     TO_CHAR (MON.IMP_LMT_MAX) AS FIN
              FROM   PABS_DT_MONDA_ALMOT MON
             WHERE   MON.COD_DVI = v_MONEDA
            UNION
            SELECT   'HORA' AS DESCRIPCION,
                     TO_CHAR (SYSDATE, 'HH24MI') AS INICIO,
                     '' AS FIN
              FROM   PABS_DT_SISTM_ENTRD_SALID SI
             WHERE   SI.COD_SIS_ENT_SAL =
                        PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SE_MESA
            UNION
            SELECT   'ALERTA' AS DESCRIPCION,
                     TO_CHAR (p_ALERTA) AS INICIO,
                     p_MENSAJE AS FIN
              --DECODE(p_ALERTA,1,v_MENSAJE_1, v_MENSAJE_2) AS FIN
              FROM   PABS_DT_SISTM_ENTRD_SALID SI
             WHERE   SI.COD_SIS_ENT_SAL =
                        PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SE_MESA;

         p_ERROR := PKG_PAB_CONSTANTES.v_OK;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            err_code := SQLCODE;
            err_msg :=
               'No hay datos para el area: '
               || PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SE_MESA;
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            p_s_mensaje := err_code || '-' || err_msg;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_BUS_MONIT_MONEDA;

   /****************************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_BUS_DETLL_MONEDA
   -- Objetivo: Procedimiento almacenado para graficos Liquidez intradia
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX, PABS_DT_CBCRA_CRTLA
   --
   -- Fecha: 06-02-2018
   -- Autor: Santander CAH
   -- Input:
   --
   -- Output:
   --          p_DETALLE    Cursor con data parametrica para grafico
   --          p_ERROR:      Indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --****************************************************************************************************************/
   PROCEDURE SP_PAB_BUS_DETLL_MONEDA (p_DETALLE   OUT SYS_REFCURSOR,
                                      p_ERROR     OUT NUMBER)
   IS
      v_NOM_SP             VARCHAR2 (30) := 'SP_PAB_BUS_DETLL_MONEDA';
      v_MONEDA             CHAR (3);
      v_FECHA              DATE;
      v_FECHA_ACTUAL       DATE := TRUNC (SYSDATE);
      v_CONVESION_MONEDA   NUMBER;
      v_ERRORCODE          NUMBER;
   --v_FECHA DATE := '12-05-2020';
   BEGIN
      PKG_PAB_UTILITY.SP_PAB_DEV_DIA_HABIL_ANT ('SN', v_FECHA_ACTUAL);

      --  v_FECHA := v_FECHA_ACTUAL ;
      BEGIN                                                          --Grafico
         OPEN p_DETALLE FOR
              SELECT   SL.COD_DVI AS MONEDA,
                       SL.COD_BCO_ORG AS BIC,
                       PKG_PAB_UTILITY.FN_PAB_OBT_DSC_BCO (SL.COD_BCO_ORG)
                          AS NOMBRE_BANCO,
                       SL.IMP_APR_AOS AS SALDO_INICIAL,
                       --   SL.IMP_ING_CPR  AS INGRESO,
                       SL.IMP_ING_DIA AS INGRESO,
                       -- SL.IMP_EGR_CPR AS EGRESOS,
                       SL.IMP_EGR_DIA AS EGRESOS,
                       -- SL.IMP_TOT_FNL_DIA AS SALDO_FINAL_INTRADIA,
                       SL.IMP_RAL_AOS AS SALDO_FINAL_INTRADIA,
                       IMP_TOT_FNL_DIA AS SALDO_PROYECTADO,
                       PKG_PAB_LIQUIDEZ.FN_PAB_BUS_CONVERSION (v_FECHA,
                                                               'USD',
                                                               SL.COD_DVI)
                       * SL.IMP_RAL_AOS
                          AS EQUIVALENTE
                FROM   PABS_DT_DETLL_SALDO_CAJA_MN_MX SL,
                       PABS_DT_CBCRA_CRTLA CC
               WHERE   SL.FEC_ING_CTL = CC.FEC_ING_CTL
                       AND SL.COD_DVI NOT IN PKG_PAB_CONSTANTES.V_STR_DVI_CLP -- AND SL.COD_DVI <> 'USD' --AND SL.COD_DVI <> 'EUR' AND SL.COD_DVI  <> 'SEK' AND SL.COD_DVI  <> 'CNY' AND SL.COD_DVI  <> 'GBP' AND SL.COD_DVI  <> 'JPY' AND SL.COD_DVI  <> 'NOK' AND SL.COD_DVI <> 'CLP' AND SL.COD_DVI  <> 'CHF' AND SL.COD_DVI  <> 'DKK' AND SL.COD_DVI  <> 'USD' AND SL.COD_DVI  <> 'CAD' AND  SL.COD_DVI  <> 'AUD' --AND SL.COD_DVI  <> 'MXN'
            --AND SL.IMP_TOT_FNL_DIA > 0
            GROUP BY   SL.COD_DVI,
                       SL.COD_BCO_ORG,
                       PKG_PAB_UTILITY.FN_PAB_OBT_DSC_BCO (SL.COD_BCO_ORG),
                       SL.IMP_APR_AOS,
                       SL.IMP_ING_CPR,
                       SL.IMP_ING_DIA,
                       SL.IMP_EGR_CPR,
                       SL.IMP_EGR_DIA,
                       SL.IMP_TOT_FNL_DIA,
                       SL.IMP_RAL_AOS,
                       IMP_TOT_FNL_DIA,
                       PKG_PAB_LIQUIDEZ.FN_PAB_BUS_CONVERSION (v_FECHA,
                                                               'USD',
                                                               SL.COD_DVI)
                       * IMP_TOT_FNL_DIA
            ORDER BY   SL.COD_DVI ASC;

         --La data debe estar en caebcera cartola, descomentar..

         p_ERROR := PKG_PAB_CONSTANTES.v_OK;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            err_code := SQLCODE;
            err_msg := 'No hay datos de Cartolas MX';
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            RAISE;
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            p_s_mensaje := err_code || '-' || err_msg;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;
   EXCEPTION
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_BUS_DETLL_MONEDA;

   /*********************************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_BUS_DETLL_MONEDA_CLP
   -- Objetivo: Procedimiento almacenado que retorna los datos para grafica CLP (Monitor moneda Nacional)
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas:  PABS_DT_DETLL_SALDO_CAJA_MN_MX
   --                    PABS_DT_SALDO_BILAT
   --                   TCDTBAI
   --
   -- Fecha: 14-02-2019
   -- Autor: Santander CAH
   -- Input:
   --
   -- Output:
   --                p_SALDO_REAL      Cursor con data saldos real CLP
   --                p_DETLL_BCCH      Cursor con data Detalle del lbtr
   --                p_DETLL_CMN       Cursor con data Detalle del combanc
   --                p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --********************************************************************************************************************/
   PROCEDURE SP_PAB_BUS_DETLL_MONEDA_CLP (p_SALDO_REAL   OUT SYS_REFCURSOR,
                                          p_ALERTA       OUT SYS_REFCURSOR,
                                          p_DETLL_BCCH   OUT SYS_REFCURSOR,
                                          p_DETLL_CMN    OUT SYS_REFCURSOR,
                                          p_ERROR        OUT NUMBER)
   IS
      v_NOM_SP      VARCHAR2 (30) := 'SP_PAB_BUS_DETLL_MONEDA_CLP';
      v_COD_PAIS    CHAR (2) := 'SN';
      v_FECHA_UTU   VARCHAR2 (2);
   BEGIN
      BEGIN                                          --Saldo Real Intradia CLP
         --Dia Habil
         PKG_PAB_UTILITY.SP_PAB_DEV_DIA_HABIL_ANT (v_COD_PAIS, v_FECHA_AYER);

         OPEN p_SALDO_REAL FOR
            -- OPEN p_SALDO_REAL FOR
            SELECT   SL.IMP_FNL_CTL AS SALDO_INICIAL_BCCH,
                     (SL.IMP_ING_DIA + SL.IMP_ABN_BCT)
                     - (SL.IMP_EGR_DIA + SL.IMP_CGO_BCT)
                        AS SALDO_BCCH,
                     SL.IMP_PSC_CBA AS SALDO_COMBANC,
                     (SL.IMP_PSC_CBA + SL.IMP_RAL_AOS) AS SALDO_REAL_INTRADIA
              FROM   PABS_DT_DETLL_SALDO_CAJA_MN_MX SL
             WHERE       SL.COD_BCO_ORG = PKG_PAB_CONSTANTES.V_BIC_BCCH --'BCECCLRR'
                     AND SL.FEC_ING_CTL = v_FECHA_AYER -- Crear variable confecha de ultima cartola (ayer)
                     AND SL.COD_DVI = PKG_PAB_CONSTANTES.V_STR_DVI_CLP;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            err_code := SQLCODE;
            err_msg :=
               SUBSTR (SQLERRM, 1, 300) || 'No se puedo encontrar datos';
            RAISE;
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            p_s_mensaje := err_code || '-' || err_msg;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;

      BEGIN                                                          -- Alerta
         OPEN p_ALERTA FOR
            SELECT   TI.DES_BCO AS NOMBRE_BANCO,
                     COD_BIC_BCO AS BIC,
                     IMP_PAG_REC AS INGRESO,
                     IMP_PAG_ENV AS EGRESO,
                     IMP_POS_BIL AS POSICION_BILAT,
                     IMP_CRE_ENV AS LIMITE_CRE,
                     IMP_CRE_REC AS LIMITE_DEB
              FROM   PABS_DT_SALDO_BILAT SB, TCDTBAI TI
             WHERE   TI.TGCDSWSA = SB.COD_BIC_BCO
                     AND SB.COD_SIS_SAL =
                           PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_CMN
                     AND SB.FEC_ING_CTL <= v_FECHA_AYER
                     AND (ABS (IMP_POS_BIL) <= IMP_CRE_ENV
                          OR ABS (IMP_POS_BIL) >= IMP_CRE_REC);
      --   AND (IMP_POS_BIL >= IMP_CRE_ENV OR IMP_POS_BIL >= IMP_CRE_REC);

      --AND ABS(IMP_POS_BIL) in(IMP_CRE_ENV, IMP_CRE_REC);
      --AND IMP_POS_BIL <= 0;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            err_code := SQLCODE;
            err_msg :=
               SUBSTR (SQLERRM, 1, 300) || 'No se puedo encontrar Datos';
            RAISE;
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            p_s_mensaje := err_code || '-' || err_msg;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;


      BEGIN                                                     --DETALLE BCCH
         OPEN P_DETLL_BCCH FOR
              SELECT   COD_BIC_BCO AS BIC,
                       TI.DES_BCO AS NOMBRE_BANCO,
                       IMP_PAG_REC AS INGRESO,
                       IMP_PAG_ENV AS EGRESO,
                       IMP_POS_BIL AS NETO
                FROM   PABS_DT_SALDO_BILAT SB, TCDTBAI TI
               WHERE   TI.TGCDSWSA(+) = SB.COD_BIC_BCO
                       AND SB.FEC_ING_CTL = v_FECHA_AYER
                       AND COD_SIS_SAL =
                             PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_LBTR
            ORDER BY   BIC DESC;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            err_code := SQLCODE;
            err_msg :=
               SUBSTR (SQLERRM, 1, 300) || 'No se puedo encontrar Datos';
            RAISE;
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            p_s_mensaje := err_code || '-' || err_msg;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;

      BEGIN                                                  --DETALLE COMBANC
         OPEN P_DETLL_CMN FOR
            SELECT   COD_BIC_BCO AS BIC,
                     TI.DES_BCO AS NOMBRE_BANCO,
                     IMP_PAG_REC AS INGRESO,
                     IMP_PAG_ENV AS EGRESO,
                     IMP_POS_BIL AS POSICION_BILAT,
                     IMP_CRE_ENV AS LIMITE_CRE,
                     IMP_CRE_REC AS LIMITE_DEB,
                     CASE
                        WHEN (SB.IMP_PAG_REC >= SB.IMP_CRE_REC
                              OR SB.IMP_PAG_ENV >= SB.IMP_CRE_ENV)
                        THEN
                           1
                        ELSE
                           0
                     END
                        AS FLAG_LIMITE
              FROM   PABS_DT_SALDO_BILAT SB, TCDTBAI TI
             WHERE   TI.TGCDSWSA = SB.COD_BIC_BCO
                     AND SB.COD_SIS_SAL =
                           PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_CMN
                     AND SB.FEC_ING_CTL = v_FECHA_AYER;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            err_code := SQLCODE;
            err_msg :=
               SUBSTR (SQLERRM, 1, 300) || 'No se puedo encontrar Datos';
            RAISE;
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            p_s_mensaje := err_code || '-' || err_msg;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_BUS_DETLL_MONEDA_CLP;

   --***************************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_BUS_CONVERSION
   -- Objetivo: Funcion que consulta el valor de conversion de una moneda en USD.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas:
   -- Fecha: 07-02-2019
   -- Autor: CAH
   -- Input:
   --      p_MONEDA    Moneda a consultar
   -- N/A
   -- Output:
   -- N/A.
   -- Input/Output:
   -- N/A.
   -- Retorno:
   --      v_VALOR -> monto conversion de la moneda consultada.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   --***************************************************************************************************
   FUNCTION FN_PAB_BUS_CONVERSION (p_FECHA        IN DATE,
                                   p_MONEDA_USD   IN VARCHAR2,
                                   p_MONEDA_VAR   IN VARCHAR2)
      RETURN NUMBER
   IS
      V_NOM_SP             VARCHAR2 (30) := 'FN_PAB_BUS_CONVERSION';

      v_VALOR              NUMBER;
      v_CONVESION_MONEDA   NUMBER := 0;
      v_ERRORCODE          NUMBER := 0;
   BEGIN
      v_VALOR := 0;

      IF p_MONEDA_VAR = 'USD'
      THEN
         v_VALOR := 1;
      ELSE
         --
         --Agregar funcion de chi_con que retorna el valor de conversion.
         PKG_CHI_CON_AOS.SP_CHI_CON_CNVSN_MONDA (p_FECHA,
                                                 p_MONEDA_USD,
                                                 p_MONEDA_VAR,
                                                 v_CONVESION_MONEDA,
                                                 v_ERRORCODE);

         IF v_ERRORCODE = 4
         THEN
            v_VALOR := 1;
         ELSE
            v_VALOR := v_CONVESION_MONEDA;
         END IF;
      END IF;

      --Por problemas en CHI_CON se crea valores aproximados por defecto
        /*
       CASE
            WHEN p_MONEDA_VAR = 'USD'  THEN
            v_VALOR := 1;
            WHEN p_MONEDA_VAR = 'AUD'  THEN
            v_VALOR := 0.34;
            WHEN p_MONEDA_VAR = 'CAD'  THEN
            v_VALOR := 0.12;
            WHEN p_MONEDA_VAR = 'GBP'  THEN
            v_VALOR := 0.13;
            WHEN p_MONEDA_VAR = 'JPY'  THEN
            v_VALOR := 0.14;
            WHEN p_MONEDA_VAR = 'EUR'  THEN
            v_VALOR := 0.12;
            WHEN p_MONEDA_VAR = 'CHF'  THEN
            v_VALOR := 0.16;
            WHEN p_MONEDA_VAR = 'DKK'  THEN
            v_VALOR := 0.17;
            WHEN p_MONEDA_VAR = 'MXN'  THEN
            v_VALOR := 0.12;
            WHEN p_MONEDA_VAR = 'NOK'  THEN
            v_VALOR := 0.23;
            WHEN p_MONEDA_VAR = 'CNY'  THEN
            v_VALOR := 0.23;
          WHEN p_MONEDA_VAR = 'SEK'  THEN
            v_VALOR := 0.21;

        ELSE
                    NULL;
        END CASE;
*/


      RETURN v_VALOR;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         v_VALOR := 0;
         RETURN v_VALOR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END FN_PAB_BUS_CONVERSION;

   /****************************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_SALDO_NETO
   -- Objetivo: Procedimiento almacenado que Muestra los datos para grafica de saldos moneda CLP
   -- Sistema: DBO_PAB.
   -- Base de Datos:  DBO_PAB.
   -- Tablas Usadas:  PABS_DT_SALDO_MONDA_DARIO,PABS_DT_SISTM_ENTRD_SALID
   --
   -- Fecha: 21-01-2019
   -- Autor: Santander CAH
   -- Input:
   --      p_MONEDA           Moneda a consultar
   --
   -- Output:
   --       p_HORARIO         Cursor con data de horario de grafico
   --       p_SALDONET        Cursor con data grafico
   --       p_ERROR           indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***************************************************************************************************************/
   PROCEDURE SP_PAB_SALDO_NETO (p_MONEDA     IN     CHAR,
                                p_HORARIO       OUT SYS_REFCURSOR,
                                p_SALDONET      OUT SYS_REFCURSOR,
                                p_ERROR         OUT NUMBER)
   IS
      v_NOM_SP        VARCHAR2 (30) := 'SP_PAB_SALDO_NETO';
      v_INI_MES_ACT   DATE;
      v_FIN_MES_ACT   DATE;
      v_INI_MES_ANT   DATE;
      v_FIN_MES_ANT   DATE;
      v_MES           NUMBER (1);
      v_INI_MES       DATE;
      v_FIN_MES       DATE;
      v_HORA_INI      CHAR (5) := '08:00';
      v_HORA_FIN      CHAR (5) := '19:00';
      v_MONEDA        CHAR (4);
   BEGIN
      --Mes actual
      v_INI_MES_ACT := TRUNC (SYSDATE, 'MM');
      v_FIN_MES_ACT := TRUNC (SYSDATE);

      --Mes anterior
      v_INI_MES_ANT := ADD_MONTHS (TRUNC (SYSDATE, 'MM'), -1);
      v_FIN_MES_ANT :=
         LAST_DAY (TO_DATE (ADD_MONTHS (SYSDATE, -1), 'DD-MM-YYY'));


      --MONEDA
      IF p_MONEDA IS NULL
      THEN
         v_MONEDA := PKG_PAB_CONSTANTES.V_STR_DVI_CLP;
      ELSE
         v_MONEDA := p_MONEDA;
      END IF;


      OPEN p_HORARIO FOR
         SELECT   'MES ACTUAL' AS DESCRIPCION,
                  NVL (TO_CHAR (MIN (IMP_NET_MIN)), 0) AS INICIO,
                  NVL (TO_CHAR (MAX (IMP_NET_MAX)), 0) AS FIN
           FROM   PABS_DT_SALDO_MONDA_DARIO SD
          WHERE       SD.FEC_REG BETWEEN v_INI_MES_ACT AND v_FIN_MES_ACT
                  AND SD.COD_DVI = v_MONEDA
                  AND IMP_NET_MIN <> 0
         UNION
         SELECT   'MES ANTERIOR' AS DESCRIPCION,
                  TO_CHAR (NVL (MIN (IMP_NET_MIN), 0)) AS INCIO,
                  TO_CHAR (NVL (MAX (IMP_NET_MAX), 0)) AS FIN
           FROM   PABS_DT_SALDO_MONDA_DARIO SD
          WHERE       SD.FEC_REG BETWEEN v_INI_MES_ANT AND v_FIN_MES_ANT
                  AND SD.COD_DVI = v_MONEDA
                  AND IMP_NET_MIN <> 0
         UNION
         SELECT   'HORARIO' AS DESCRIPCION,
                  TO_CHAR (0 || SI.HOR_ICO_SIS) AS INICIO,
                  TO_CHAR (SI.HOR_CRR_SIS) AS FIN
           FROM   PABS_DT_SISTM_ENTRD_SALID SI
          WHERE   SI.COD_SIS_ENT_SAL =
                     PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SE_MESA
         UNION
         SELECT   'HORA' AS DESCRIPCION,
                  TO_CHAR (SYSDATE, 'HH24:MI') AS INICIO,
                  NULL AS FIN
           FROM   DUAL;


      OPEN p_SALDONET FOR
         SELECT   DECODE (RPAD (HOR_REG_SDO, 1),
                          8, 0 || TO_CHAR (HOR_REG_SDO),
                          9, 0 || TO_CHAR (HOR_REG_SDO),
                          HOR_REG_SDO)
                     AS HORA,
                  IMP_NET_RAL AS MONTO
           FROM   PABS_DT_SALDO_HORA_ITDIA SH
          WHERE   (FEC_REG_SDO) = v_FECHA_HOY AND SH.COD_DVI = v_MONEDA;

      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_SALDO_NETO;

   /******************************************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_RESUMEN_SALDO_NETO
   -- Objetivo: Procedimiento almacenado que Muestra los datos para grafica de saldos moneda CLP
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas:   PABS_DT_SALDO_MONDA_DARIO
   --
   -- Fecha: 21-01-2019
   -- Autor: Santander CAH
   -- Input:
   --      p_FLG_MES       Flag para indicar mes de busqueda
   --      p_MONEDA        Moneda a consultar
   -- Output:
   --       p_TOP         Cursor con top de cada mes
   --      p_RESUMEN       CUrsor con resumen del mes
   --       p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --******************************************************************************************************************************/
   PROCEDURE SP_PAB_RESUMEN_SALDO_NETO (p_FLG_MES   IN     NUMBER,
                                        p_MONEDA    IN     CHAR,
                                        p_TOP          OUT SYS_REFCURSOR,
                                        p_RESUMEN      OUT SYS_REFCURSOR,
                                        p_ERROR        OUT NUMBER)
   IS
      v_NOM_SP      VARCHAR2 (30) := 'SP_PAB_RESUMEN_SALDO_NETO';
      v_INI_MES     DATE;
      v_FIN_MES     DATE;
      v_MONEDA      CHAR (3);
      v_CANT        NUMBER;
      v_NETO_MAX1   NUMBER;
      v_NETO_MIN1   NUMBER;
      v_NETO_MAX2   NUMBER;
      v_NETO_MIN2   NUMBER;
      v_NETO_MAX3   NUMBER;
      v_NETO_MIN3   NUMBER;
   BEGIN
      IF p_FLG_MES = 0
      THEN                                                     -- MES ANTERIOR
         v_INI_MES := ADD_MONTHS (TRUNC (SYSDATE, 'MM'), -1);
         v_FIN_MES :=
            LAST_DAY (TO_DATE (ADD_MONTHS (SYSDATE, -1), 'DD-MM-YYY'));
      ELSE                                                        --MES ACTUAL
         v_INI_MES := TRUNC (SYSDATE, 'MM');
         v_FIN_MES := TRUNC (SYSDATE);
      END IF;

      --MONEDA
      IF p_MONEDA IS NULL
      THEN
         v_MONEDA := 'CLP';
      ELSE
         v_MONEDA := p_MONEDA;
      END IF;

        --Se valida si existe data
        SELECT   COUNT (1)
          INTO   v_CANT
          FROM   PABS_DT_SALDO_MONDA_DARIO SD
         WHERE   SD.FEC_REG BETWEEN v_INI_MES AND v_FIN_MES
                 AND SD.COD_DVI = v_MONEDA
      ORDER BY   SD.FEC_REG DESC;


      IF v_CANT > 0
      THEN
         BEGIN                                      --Tabla resumen mes actual
            OPEN p_RESUMEN FOR
                 SELECT   TO_CHAR (FEC_REG, 'DD-MM') AS FECHA,
                          NVL (IMP_NET_MIN, 0) AS SALDO_MIN,
                          NVL (IMP_NET_MAX, 0) AS SALDO_MAX
                   FROM   PABS_DT_SALDO_MONDA_DARIO SD
                  WHERE   SD.FEC_REG BETWEEN v_INI_MES AND v_FIN_MES
                          AND SD.COD_DVI = v_MONEDA
               ORDER BY   SD.FEC_REG DESC;
         EXCEPTION
            WHEN OTHERS
            THEN
               err_code := SQLCODE;
               err_msg := SUBSTR (SQLERRM, 1, 300);
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
               p_s_mensaje := err_code || '-' || err_msg;
               RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
         END;

         BEGIN
            --BUSCAMOS LOS MAX Y MIN
            OPEN p_TOP FOR
               SELECT   IMP_MIN.INDICE AS ORDEN,
                        IMP_MAX.IMP_NET_MAX AS NETO_MAX,
                        IMP_MIN.IMP_NET_MIN AS NETO_MIN
                 FROM      (SELECT   ROWNUM AS INDICE, IMP_NET_MAX
                              FROM   (  SELECT   DISTINCT (IMP_NET_MAX)
                                          FROM   PABS_DT_SALDO_MONDA_DARIO
                                         WHERE   FEC_REG BETWEEN v_INI_MES
                                                             AND  v_FIN_MES
                                                 AND COD_DVI = v_MONEDA
                                      ORDER BY   IMP_NET_MAX DESC)) IMP_MAX
                        INNER JOIN
                           (SELECT   ROWNUM AS INDICE, IMP_NET_MIN
                              FROM   (  SELECT   DISTINCT (IMP_NET_MIN)
                                          FROM   PABS_DT_SALDO_MONDA_DARIO
                                         WHERE   FEC_REG BETWEEN v_INI_MES
                                                             AND  v_FIN_MES
                                                 AND COD_DVI = v_MONEDA
                                      ORDER BY   IMP_NET_MIN)) IMP_MIN
                        ON IMP_MIN.INDICE = IMP_MAX.INDICE
                WHERE   IMP_MIN.INDICE <= 3;     -- Se retornan los 3 primeros
         EXCEPTION
            WHEN OTHERS
            THEN
               err_code := SQLCODE;
               err_msg := SUBSTR (SQLERRM, 1, 300);
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
               p_s_mensaje := err_code || '-' || err_msg;
               RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
         END;
      ELSE
         BEGIN                                     -- Tabla resumen mes actual
            OPEN p_RESUMEN FOR
               SELECT   NVL (MAX (TO_CHAR (SYSDATE, 'DD-MM')),
                             TO_CHAR (SYSDATE, 'DD-MM-YYYY'))
                           AS FECHA,
                        0 AS SALDO_MIN,
                        0 AS SALDO_MAX
                 FROM   PABS_DT_SALDO_MONDA_DARIO SD
                WHERE   SD.FEC_REG BETWEEN v_INI_MES AND v_FIN_MES
                        AND SD.COD_DVI = v_MONEDA;
         EXCEPTION
            WHEN OTHERS
            THEN
               err_code := SQLCODE;
               err_msg := SUBSTR (SQLERRM, 1, 300);
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
               p_s_mensaje := err_code || '-' || err_msg;
               RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
         END;

         BEGIN                                    --Tabla resumen mes anterior
            OPEN p_TOP FOR
                 SELECT   0 AS NETO_MAX,
                          0 AS NETO_MIN,
                          NVL (MAX (TO_CHAR (SYSDATE, 'DD-MM')), 1) AS ORDEN
                   FROM   PABS_DT_SALDO_MONDA_DARIO
                  WHERE   FEC_REG BETWEEN v_INI_MES AND v_FIN_MES
                          AND COD_DVI = v_MONEDA
               ORDER BY   IMP_NET_MAX DESC, IMP_NET_MIN ASC;
         EXCEPTION
            WHEN OTHERS
            THEN
               err_code := SQLCODE;
               err_msg := SUBSTR (SQLERRM, 1, 300);
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
               p_s_mensaje := err_code || '-' || err_msg;
               RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
         END;
      END IF;

      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_RESUMEN_SALDO_NETO;

   /******************************************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_BUS_ING_EGR
   -- Objetivo: Procedimiento almacenado que Muestra los datos para grafica de saldos moneda CLP
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas:  PABS_DT_SALDO_MONDA_DARIO
   --
   -- Fecha: 28-08-2018
   -- Autor: Santander CAH
   -- Input:
   --      p_MONEDA        Moneda a consultar
   --
   -- Output:
   --       p_HORARIO       Cursor con data grafico
   --       p_ING_EGR       Cursor con data de maximo actual
   --       p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   --
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --******************************************************************************************************************************/
   PROCEDURE SP_PAB_BUS_ING_EGR (p_MONEDA    IN     CHAR,
                                 p_HORARIO      OUT SYS_REFCURSOR,
                                 p_ING_EGR      OUT SYS_REFCURSOR,
                                 p_ERROR        OUT NUMBER)
   IS
      v_NOM_SP        VARCHAR2 (30) := 'SP_PAB_BUS_ING_EGR';
      v_INI_MES_ACT   DATE;
      v_FIN_MES_ACT   DATE;
      v_INI_MES_ANT   DATE;
      v_FIN_MES_ANT   DATE;
      v_MES           NUMBER (1);
      v_INI_MES       DATE;
      v_FIN_MES       DATE;
      v_HORA_INI      CHAR (5) := '08:00';
      v_HORA_FIN      CHAR (5) := '19:00';
      v_MONEDA        CHAR (4);
   BEGIN
      --Mes actual
      v_INI_MES_ACT := TRUNC (SYSDATE, 'MM');
      v_FIN_MES_ACT := TRUNC (SYSDATE);

      --Mes anterior
      v_INI_MES_ANT := ADD_MONTHS (TRUNC (SYSDATE, 'MM'), -1);
      v_FIN_MES_ANT :=
         LAST_DAY (TO_DATE (ADD_MONTHS (SYSDATE, -1), 'DD-MM-YYY'));


      --MONEDA
      IF p_MONEDA IS NULL
      THEN
         v_MONEDA := PKG_PAB_CONSTANTES.V_STR_DVI_CLP;
      ELSE
         v_MONEDA := p_MONEDA;
      END IF;


      OPEN p_HORARIO FOR
         SELECT   'MES ACTUAL INGRESO' AS DESCRIPCION,
                  NVL (TO_CHAR (MIN (SD.IMP_MIN_ING)), 0) AS INICIO,
                  NVL (TO_CHAR (MAX (SD.IMP_MAX_ING)), 0) AS FIN
           FROM   PABS_DT_SALDO_MONDA_DARIO SD
          WHERE       SD.FEC_REG BETWEEN v_INI_MES_ACT AND v_FIN_MES_ACT
                  AND SD.COD_DVI = v_MONEDA
                  AND SD.IMP_MIN_ING <> 0
         UNION
         SELECT   'MES ANTERIOR INGRESO' AS DESCRIPCION,
                  NVL (TO_CHAR (MIN (SD.IMP_MIN_ING)), 0) AS INICIO,
                  NVL (TO_CHAR (MAX (SD.IMP_MAX_ING)), 0) AS FIN
           FROM   PABS_DT_SALDO_MONDA_DARIO SD
          WHERE       SD.FEC_REG BETWEEN v_INI_MES_ANT AND v_FIN_MES_ANT
                  AND SD.COD_DVI = v_MONEDA
                  AND SD.IMP_MIN_ING <> 0
         UNION
         SELECT   'MES ACTUAL EGRESO' AS DESCRIPCION,
                  NVL (TO_CHAR (MIN (SD.IMP_MIN_EGR)), 0) AS INICIO,
                  NVL (TO_CHAR (MAX (SD.IMP_MAX_EGR)), 0) AS FIN
           FROM   PABS_DT_SALDO_MONDA_DARIO SD
          WHERE       SD.FEC_REG BETWEEN v_INI_MES_ACT AND v_FIN_MES_ACT
                  AND SD.COD_DVI = v_MONEDA
                  AND SD.IMP_MIN_EGR <> 0
         UNION
         SELECT   'MES ANTERIOR EGRESO' AS DESCRIPCION,
                  NVL (TO_CHAR (MIN (SD.IMP_MIN_EGR)), 0) AS INICIO,
                  NVL (TO_CHAR (MAX (SD.IMP_MAX_EGR)), 0) AS FIN
           FROM   PABS_DT_SALDO_MONDA_DARIO SD
          WHERE       SD.FEC_REG BETWEEN v_INI_MES_ANT AND v_FIN_MES_ANT
                  AND SD.COD_DVI = v_MONEDA
                  AND SD.IMP_MIN_EGR <> 0
         UNION
         SELECT   'HORARIO' AS DESCRIPCION,
                  TO_CHAR (0 || SI.HOR_ICO_SIS) AS INICIO,
                  TO_CHAR (SI.HOR_CRR_SIS) AS FIN
           FROM   PABS_DT_SISTM_ENTRD_SALID SI
          WHERE   SI.COD_SIS_ENT_SAL =
                     PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SE_MESA
         UNION
         SELECT   'HORA' AS DESCRIPCION,
                  TO_CHAR (SYSDATE, 'HH24:MI') AS INICIO,
                  '' AS FIN
           FROM   PABS_DT_SISTM_ENTRD_SALID SI
          WHERE   SI.COD_SIS_ENT_SAL =
                     PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SE_MESA;


      OPEN p_ING_EGR FOR
         SELECT   DECODE (RPAD (HOR_REG_SDO, 1),
                          8, 0 || TO_CHAR (HOR_REG_SDO),
                          9, 0 || TO_CHAR (HOR_REG_SDO),
                          HOR_REG_SDO)
                     AS HORA,
                  IMP_NET_ING AS MONTO_ING,
                  IMP_NET_EGR * (-1) AS MONTO_EGR
           FROM   PABS_DT_SALDO_HORA_ITDIA SH
          WHERE   (FEC_REG_SDO) = v_FECHA_HOY --BETWEEN v_DIA_INI AND  v_DIA_FIN
                                             AND SH.COD_DVI = v_MONEDA; --PKG_PAB_CONSTANTES.V_STR_DVI_CLP;



      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_BUS_ING_EGR;


   /******************************************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_RESUMEN_ING_EGR
   -- Objetivo: Procedimiento almacenado que Muestra los datos para grafica de Ingresos y Egresos
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas:  PABS_DT_SALDO_MONDA_DARIO
   --
   -- Fecha: 28-01-2019
   -- Autor: Santander CAH
   -- Input:
   --      p_FLG_MES       Flag para indicar mes de busqueda
   --      p_MONEDA        Moneda a consultar
   -- Output:
   --       p_TOP         Cursor con top de cada mes
   --      p_RESUMEN       CUrsor con resumen del mes
   --       p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --******************************************************************************************************************************/
   PROCEDURE SP_PAB_RESUMEN_ING_EGR (p_FLG_MES   IN     NUMBER,
                                     p_MONEDA    IN     CHAR,
                                     p_TOP          OUT SYS_REFCURSOR,
                                     p_RESUMEN      OUT SYS_REFCURSOR,
                                     p_ERROR        OUT NUMBER)
   IS
      v_NOM_SP    VARCHAR2 (30) := 'SP_PAB_RESUMEN_ING_EGR';
      v_INI_MES   DATE;
      v_FIN_MES   DATE;
      v_MONEDA    CHAR (3);
      EGR_MAX1    NUMBER;
      ING_MAX1    NUMBER;
      EGR_MIN1    NUMBER;
      ING_MIN1    NUMBER;
      EGR_MAX2    NUMBER;
      ING_MAX2    NUMBER;
      EGR_MIN2    NUMBER;
      ING_MIN2    NUMBER;
      EGR_MAX3    NUMBER;
      ING_MAX3    NUMBER;
      EGR_MIN3    NUMBER;
      ING_MIN3    NUMBER;
   BEGIN
      IF p_FLG_MES = 0
      THEN                                                     -- MES ANTERIOR
         v_INI_MES := ADD_MONTHS (TRUNC (SYSDATE, 'MM'), -1);
         v_FIN_MES :=
            LAST_DAY (TO_DATE (ADD_MONTHS (SYSDATE, -1), 'DD-MM-YYY'));
      ELSE                                                        --MES ACTUAL
         v_INI_MES := TRUNC (SYSDATE, 'MM');
         v_FIN_MES := TRUNC (SYSDATE);
      END IF;

      --MONEDA
      IF p_MONEDA IS NULL
      THEN
         v_MONEDA := 'CLP';
      ELSE
         v_MONEDA := p_MONEDA;
      END IF;


      BEGIN                                         --Tabla resumen mes actual
         OPEN p_RESUMEN FOR
              SELECT   FEC_REG AS FECHA,
                       IMP_MAX_ING AS ING_MAX,
                       IMP_MIN_ING AS ING_MIN,
                       IMP_MAX_EGR AS EGR_MAX,
                       IMP_MIN_EGR AS EGR_MIN
                FROM   PABS_DT_SALDO_MONDA_DARIO SM
               WHERE   FEC_REG BETWEEN v_INI_MES AND v_FIN_MES
                       AND COD_DVI = v_MONEDA
            ORDER BY   IMP_MAX_ING DESC, IMP_MIN_ING ASC;
      --ORDER BY   FEC_REG DESC; --IMP_MAX_ING DESC, IMP_MIN_ING ASC;

      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            err_code := SQLCODE;
            err_msg :=
               SUBSTR (SQLERRM, 1, 300) || 'No se puedo encontrar datos';
            RAISE;
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            p_s_mensaje := err_code || '-' || err_msg;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;


      BEGIN                                       --Tabla resumen mes anterior
         --Maximos y minimos TOP 1
         SELECT   MAX (SD.IMP_MAX_EGR),
                  MAX (SD.IMP_MAX_ING),
                  MAX (SD.IMP_MIN_EGR),
                  MAX (SD.IMP_MIN_ING)
           INTO   EGR_MAX1,
                  ING_MAX1,
                  EGR_MIN1,
                  ING_MIN1
           FROM   PABS_DT_SALDO_MONDA_DARIO SD
          WHERE   FEC_REG BETWEEN v_INI_MES AND v_FIN_MES
                  AND SD.COD_DVI = v_MONEDA;


         SELECT   PKG_PAB_LIQUIDEZ.FN_PAB_OBT_TOP_ING_EGR (
                     v_MONEDA,
                     p_FLG_MES,
                     0,
                     0,
                     MAX (SD.IMP_MAX_EGR)
                  ),
                  PKG_PAB_LIQUIDEZ.FN_PAB_OBT_TOP_ING_EGR (
                     v_MONEDA,
                     p_FLG_MES,
                     1,
                     0,
                     MAX (SD.IMP_MAX_ING)
                  ),
                  PKG_PAB_LIQUIDEZ.FN_PAB_OBT_TOP_ING_EGR (
                     v_MONEDA,
                     p_FLG_MES,
                     0,
                     1,
                     MAX (SD.IMP_MIN_EGR)
                  ),
                  PKG_PAB_LIQUIDEZ.FN_PAB_OBT_TOP_ING_EGR (
                     v_MONEDA,
                     p_FLG_MES,
                     1,
                     1,
                     MAX (SD.IMP_MIN_ING)
                  )
           INTO   EGR_MAX2,
                  ING_MAX2,
                  EGR_MIN2,
                  ING_MIN2
           FROM   PABS_DT_SALDO_MONDA_DARIO SD
          WHERE   FEC_REG BETWEEN v_INI_MES AND v_FIN_MES
                  AND SD.COD_DVI = v_MONEDA;

         --Maximos y minimos TOP 2
         SELECT   NVL (MAX (SD.IMP_MAX_EGR), 0)
           INTO   EGR_MAX3
           FROM   PABS_DT_SALDO_MONDA_DARIO SD
          WHERE       FEC_REG BETWEEN v_INI_MES AND v_FIN_MES
                  AND SD.COD_DVI = v_MONEDA
                  --AND SD.IMP_MAX_EGR <> EGR_MAX2;
                  AND SD.IMP_MAX_EGR NOT IN (EGR_MAX1, EGR_MAX2);


         SELECT   NVL (MAX (SD.IMP_MAX_ING), 0)
           INTO   ING_MAX3
           FROM   PABS_DT_SALDO_MONDA_DARIO SD
          WHERE       FEC_REG BETWEEN v_INI_MES AND v_FIN_MES
                  AND SD.COD_DVI = v_MONEDA
                  --AND SD.IMP_MAX_ING <> ING_MAX2;
                  AND SD.IMP_MAX_ING NOT IN (ING_MAX1, ING_MAX2);

         SELECT   NVL (MAX (SD.IMP_MIN_EGR), 0)
           INTO   EGR_MIN3
           FROM   PABS_DT_SALDO_MONDA_DARIO SD
          WHERE       FEC_REG BETWEEN v_INI_MES AND v_FIN_MES
                  AND SD.COD_DVI = v_MONEDA
                  AND SD.IMP_MIN_EGR NOT IN (EGR_MIN1, EGR_MIN2);

         --AND SD.IMP_MIN_EGR <> EGR_MIN2;

         SELECT   NVL (MAX (SD.IMP_MIN_ING), 0)
           INTO   ING_MIN3
           FROM   PABS_DT_SALDO_MONDA_DARIO SD
          WHERE       FEC_REG BETWEEN v_INI_MES AND v_FIN_MES
                  AND SD.COD_DVI = v_MONEDA
                  --AND SD.IMP_MIN_ING <> ING_MIN2;
                  AND SD.IMP_MIN_ING NOT IN (ING_MIN1, ING_MIN2);

         OPEN p_TOP FOR
              --SELECT   DECODE(ROWNUM, 3,1,2,2,1,3) AS ORDEN,
              SELECT   ROWNUM AS ORDEN,
                       ING_MAX,
                       ING_MIN,
                       EGR_MAX,
                       EGR_MIN
                FROM   (SELECT   1 AS ORDEN,
                                 ING_MAX1 AS ING_MAX,
                                 ING_MIN1 AS ING_MIN,
                                 EGR_MAX1 AS EGR_MAX,
                                 EGR_MIN1 AS EGR_MIN
                          FROM   PABS_DT_SALDO_MONDA_DARIO SD
                         WHERE       FEC_REG BETWEEN v_INI_MES AND v_FIN_MES
                                 AND SD.COD_DVI = v_MONEDA
                                 AND ROWNUM BETWEEN 0 AND 1
                        UNION
                        SELECT   2 AS ORDEN,
                                 ING_MAX2 AS ING_MAX,
                                 ING_MIN2 AS ING_MIN,
                                 EGR_MAX2 AS EGR_MAX,
                                 EGR_MIN2 AS EGR_MIN
                          FROM   PABS_DT_SALDO_MONDA_DARIO SD
                         WHERE       FEC_REG BETWEEN v_INI_MES AND v_FIN_MES
                                 AND SD.COD_DVI = v_MONEDA
                                 AND ROWNUM BETWEEN 0 AND 1
                        UNION
                        SELECT   3 AS ORDEN,
                                 ING_MAX3 AS ING_MAX,
                                 ING_MIN3 AS ING_MIN,
                                 EGR_MAX3 AS EGR_MAX,
                                 EGR_MIN3 AS EGR_MIN
                          FROM   PABS_DT_SALDO_MONDA_DARIO SD
                         WHERE       FEC_REG BETWEEN v_INI_MES AND v_FIN_MES
                                 AND SD.COD_DVI = v_MONEDA
                                 AND ROWNUM BETWEEN 0 AND 1)
               WHERE   ROWNUM BETWEEN 0 AND 3
            ORDER BY   ORDEN;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            err_code := SQLCODE;
            err_msg :=
               SUBSTR (SQLERRM, 1, 300) || 'No se puedo encontrar datos';
            RAISE;
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            p_s_mensaje := err_code || '-' || err_msg;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;



      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_RESUMEN_ING_EGR;


   --***********************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_OBT_MAX_EGR
   -- Objetivo: Funcion que devuelve MAX EGRESOS
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_SALDO_MONDA_DARIO
   -- Fecha: 30-05-2019
   -- Autor: CAH
   -- Input:
   --      p_MONEDA    -> Moneda
   --      p_FLG_MES   -> Hora a consultar
   --      p_FLUJO     -> Flujo de ingreso o egreso
   --      p_MAX_MIX   -> Flag de max o min.
   --      p_MONTO     -> MOnto anterior
   -- Output:
   -- N/A.
   -- Input/Output: N/A
   -- Retorno:
   --      v_MONTO -> monto de la moneda consultada.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   FUNCTION FN_PAB_OBT_TOP_ING_EGR (p_MONEDA    IN CHAR,
                                    p_FLG_MES   IN NUMBER,
                                    p_FLUJO     IN NUMBER,
                                    p_MAX_MIX   IN NUMBER,
                                    p_MONTO     IN NUMBER)
      RETURN NUMBER
   IS
      v_NOM_SP    VARCHAR2 (30) := 'FN_PAB_OBT_TOP_ING_EGR';
      v_MONTO     NUMBER;
      v_INI_MES   DATE;
      v_FIN_MES   DATE;
   BEGIN
      IF p_FLG_MES = 0
      THEN                                                     -- MES ANTERIOR
         v_INI_MES := ADD_MONTHS (TRUNC (SYSDATE, 'MM'), -1);
         v_FIN_MES :=
            LAST_DAY (TO_DATE (ADD_MONTHS (SYSDATE, -1), 'DD-MM-YYY'));
      ELSE                                                        --MES ACTUAL
         v_INI_MES := TRUNC (SYSDATE, 'MM');
         v_FIN_MES := TRUNC (SYSDATE);
      END IF;

      IF p_FLUJO = 0
      THEN                                                           -- EGRESO
         IF p_MAX_MIX = 0
         THEN                                                        -- MAXIMO
            SELECT   MAX (SD.IMP_MAX_EGR)
              INTO   v_MONTO
              FROM   PABS_DT_SALDO_MONDA_DARIO SD
             WHERE       FEC_REG BETWEEN v_INI_MES AND v_FIN_MES
                     AND SD.COD_DVI = p_MONEDA
                     AND SD.IMP_MAX_EGR <> p_MONTO;
         ELSE                                                        -- MINIMO
            SELECT   MAX (SD.IMP_MIN_EGR)
              INTO   v_MONTO
              FROM   PABS_DT_SALDO_MONDA_DARIO SD
             WHERE       FEC_REG BETWEEN v_INI_MES AND v_FIN_MES
                     AND SD.COD_DVI = p_MONEDA
                     AND SD.IMP_MIN_EGR <> p_MONTO;
         END IF;
      ELSE                                                          -- INGRESO
         IF p_MAX_MIX = 0
         THEN                                                        -- MAXIMO
            SELECT   MAX (SD.IMP_MAX_ING)
              INTO   v_MONTO
              FROM   PABS_DT_SALDO_MONDA_DARIO SD
             WHERE       FEC_REG BETWEEN v_INI_MES AND v_FIN_MES
                     AND SD.COD_DVI = p_MONEDA
                     AND SD.IMP_MAX_ING <> p_MONTO;
         ELSE                                                        -- MINIMO
            SELECT   MAX (SD.IMP_MIN_ING)
              INTO   v_MONTO
              FROM   PABS_DT_SALDO_MONDA_DARIO SD
             WHERE       FEC_REG BETWEEN v_INI_MES AND v_FIN_MES
                     AND SD.COD_DVI = p_MONEDA
                     AND SD.IMP_MIN_ING <> p_MONTO;
         END IF;
      END IF;

      IF v_MONTO IS NULL
      THEN
         v_MONTO := 0;
      END IF;


      RETURN v_MONTO;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := 'No existe informacion para la moneda: ' || p_MONEDA;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END FN_PAB_OBT_TOP_ING_EGR;

   --***********************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_BUS_TRAMO_PRMDO
   -- Objetivo: Consulta el monto acumulado de una moneda, segun la hora
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas: PABS_DT_PAGOS_RLZAR_ACMDO
   -- Fecha: 20-09-2018
   -- Autor: CAH
   -- Input:
   --      p_FECHA         -> Fecha a cosultar
   --      p_HORA          -> Hora a consultar
   --      p_MONEDA        -> Moneda
   --      p_FLG_EGR_ING   -> Flag de ingreso.
   -- Output:
   -- N/A.
   -- Input/Output: N/A
   -- Retorno:
   --      v_MONTO_HORA -> monto de la moneda consultada.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   FUNCTION FN_PAB_BUS_TRAMO_PRMDO (p_FECHA         IN DATE,
                                    p_HORA          IN NUMBER,
                                    p_MONEDA        IN CHAR,
                                    p_FLG_EGR_ING   IN NUMBER)
      RETURN NUMBER
   IS
      V_NOM_SP        VARCHAR2 (30) := 'FN_PAB_BUS_TRAMO_PRMDO';
      V_HORA_INICIO   VARCHAR2 (5);
      v_FECHA         NUMBER := TO_CHAR (p_FECHA, 'HH24MI');
      v_TRAMO         NUMBER;
      v_MONTO_HORA    NUMBER;
   BEGIN
      SELECT   PAC.IMP_PAG_RLZ_ACM_HOR
        INTO   v_MONTO_HORA
        FROM   PABS_DT_PAGOS_RLZAR_ACMDO PAC
       WHERE       PAC.COD_DVI = p_MONEDA
               AND PAC.FEC_PAG_RLZ = TRUNC (p_FECHA)
               AND PAC.HOR_PAG_RLZ = p_HORA
               AND PAC.FLG_EGR_ING = p_FLG_EGR_ING;

      IF v_MONTO_HORA IS NULL
      THEN
         v_MONTO_HORA := 0;
      END IF;


      RETURN v_MONTO_HORA;
   --
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         v_MONTO_HORA := 0;
         RETURN v_MONTO_HORA;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END FN_PAB_BUS_TRAMO_PRMDO;

   --***********************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_BUS_MONTO_MAX_DIA
   -- Objetivo: Funcion que consulta el monto maximo del dia de una moneda, segun fecha
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas: PABS_DT_PAGOS_RLZAR_ACMDO
   -- Fecha: 20-09-2018
   -- Autor: CAH
   -- Input:
   --      p_FECHA     -> Fecha a consultar
   --      p_MONEDA    -> Moneda
   -- N/A
   -- Output:
   -- N/A.
   -- Input/Output:
   -- N/A.
   -- Retorno:
   --      v_MONTO_HORA -> monto de la moneda consultada.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   --***********************************************************************************************
   FUNCTION FN_PAB_BUS_MONTO_MAX_DIA (p_FECHA IN DATE, p_MONEDA IN CHAR)
      RETURN NUMBER
   IS
      V_NOM_SP          VARCHAR2 (30) := 'FN_PAB_BUS_MONTO_MAX_DIA';
      V_HORA_INICIO     VARCHAR2 (5);
      v_FECHA           NUMBER := TO_CHAR (p_FECHA, 'HH24MI');
      v_TRAMO           NUMBER;
      v_MONTO_MAX_DIA   NUMBER;
   BEGIN
      SELECT   MAX (PAC.IMP_PAG_RLZ_ACM_DIA)
        INTO   v_MONTO_MAX_DIA
        FROM   PABS_DT_PAGOS_RLZAR_ACMDO PAC
       WHERE   PAC.COD_DVI = p_MONEDA AND PAC.FEC_PAG_RLZ = TRUNC (p_FECHA);

      IF v_MONTO_MAX_DIA IS NULL
      THEN
         v_MONTO_MAX_DIA := 1;
      ELSIF v_MONTO_MAX_DIA = 0
      THEN
         v_MONTO_MAX_DIA := 1;
      END IF;


      RETURN v_MONTO_MAX_DIA;
   --
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         v_MONTO_MAX_DIA := 1;
         RETURN v_MONTO_MAX_DIA;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END FN_PAB_BUS_MONTO_MAX_DIA;

   /******************************************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_BUS_PRMDO
   -- Objetivo: Procedimiento almacenado que muestra los datos de grafica Promedios
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas:  PABS_DT_PAGOS_RLZAR_ACMDO
   --
   -- Fecha: 28-01-2019
   -- Autor: Santander CAH
   -- Input:
   --      p_FLG_EGR_ING   Flag para indicar mes de busqueda
   --      p_MONEDA        Moneda a consultar
   --
   -- Output:
   --       p_PROMEDIO    Cursor con detalle de grafica promedios
   --       p_ERROR       indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --******************************************************************************************************************************/
   PROCEDURE SP_PAB_BUS_PRMDO (p_FLG_EGR_ING   IN     NUMBER,
                               p_MONEDA        IN     CHAR,
                               p_PROMEDIO         OUT SYS_REFCURSOR,
                               p_ERROR            OUT NUMBER)
   IS
      v_NOM_SP        VARCHAR2 (30) := 'SP_PAB_BUS_PRMDO';
      v_INI_MES       DATE;
      v_FIN_MES       DATE;
      v_MONEDA        CHAR (3);
      v_FLG_EGR_ING   NUMBER;

      v_PAG_INI       DATE := ADD_MONTHS (TRUNC (SYSDATE - 1), -1);
      v_PAG_FIN       DATE := TRUNC (SYSDATE);
      v_MONTO_8       CHAR (4) := '800';
   BEGIN
      --Flag de ingreso o egreso
      IF p_FLG_EGR_ING IS NULL
      THEN
         v_FLG_EGR_ING := 0;
      ELSE
         v_FLG_EGR_ING := p_FLG_EGR_ING;
      END IF;

      --MONEDA
      IF p_MONEDA IS NULL
      THEN
         v_MONEDA := 'CLP';
      ELSE
         v_MONEDA := p_MONEDA;
      END IF;


      BEGIN                                         --Tabla resumen mes actual
         OPEN p_PROMEDIO FOR
              SELECT   TO_CHAR (PAC.FEC_PAG_RLZ, 'DD-MM') AS FECHA,
                       PKG_PAB_LIQUIDEZ.FN_PAB_BUS_TRAMO_PRMDO (
                          PAC.FEC_PAG_RLZ,
                          0800,
                          v_MONEDA,
                          v_FLG_EGR_ING
                       )
                          AS MONTO_8,
                       PKG_PAB_LIQUIDEZ.FN_PAB_BUS_TRAMO_PRMDO (
                          PAC.FEC_PAG_RLZ,
                          0900,
                          v_MONEDA,
                          v_FLG_EGR_ING
                       )
                          AS MONTO_9,
                       PKG_PAB_LIQUIDEZ.FN_PAB_BUS_TRAMO_PRMDO (
                          PAC.FEC_PAG_RLZ,
                          1000,
                          v_MONEDA,
                          v_FLG_EGR_ING
                       )
                          AS MONTO_10,
                       PKG_PAB_LIQUIDEZ.FN_PAB_BUS_TRAMO_PRMDO (
                          PAC.FEC_PAG_RLZ,
                          1100,
                          v_MONEDA,
                          v_FLG_EGR_ING
                       )
                          AS MONTO_11,
                       PKG_PAB_LIQUIDEZ.FN_PAB_BUS_TRAMO_PRMDO (
                          PAC.FEC_PAG_RLZ,
                          1200,
                          v_MONEDA,
                          v_FLG_EGR_ING
                       )
                          AS MONTO_12,
                       PKG_PAB_LIQUIDEZ.FN_PAB_BUS_TRAMO_PRMDO (
                          PAC.FEC_PAG_RLZ,
                          1300,
                          v_MONEDA,
                          v_FLG_EGR_ING
                       )
                          AS MONTO_13,
                       PKG_PAB_LIQUIDEZ.FN_PAB_BUS_TRAMO_PRMDO (
                          PAC.FEC_PAG_RLZ,
                          1400,
                          v_MONEDA,
                          v_FLG_EGR_ING
                       )
                          AS MONTO_14,
                       PKG_PAB_LIQUIDEZ.FN_PAB_BUS_TRAMO_PRMDO (
                          PAC.FEC_PAG_RLZ,
                          1500,
                          v_MONEDA,
                          v_FLG_EGR_ING
                       )
                          AS MONTO_15,
                       PKG_PAB_LIQUIDEZ.FN_PAB_BUS_TRAMO_PRMDO (
                          PAC.FEC_PAG_RLZ,
                          1600,
                          v_MONEDA,
                          v_FLG_EGR_ING
                       )
                          AS MONTO_16,
                       PKG_PAB_LIQUIDEZ.FN_PAB_BUS_TRAMO_PRMDO (
                          PAC.FEC_PAG_RLZ,
                          1700,
                          v_MONEDA,
                          v_FLG_EGR_ING
                       )
                          AS MONTO_17,
                       PKG_PAB_LIQUIDEZ.FN_PAB_BUS_TRAMO_PRMDO (
                          PAC.FEC_PAG_RLZ,
                          1800,
                          v_MONEDA,
                          v_FLG_EGR_ING
                       )
                       + PKG_PAB_LIQUIDEZ.FN_PAB_BUS_TRAMO_PRMDO (
                            PAC.FEC_PAG_RLZ,
                            1900,
                            v_MONEDA,
                            v_FLG_EGR_ING
                         )
                       + PKG_PAB_LIQUIDEZ.FN_PAB_BUS_TRAMO_PRMDO (
                            PAC.FEC_PAG_RLZ,
                            2000,
                            v_MONEDA,
                            v_FLG_EGR_ING
                         )
                       + PKG_PAB_LIQUIDEZ.FN_PAB_BUS_TRAMO_PRMDO (
                            PAC.FEC_PAG_RLZ,
                            2100,
                            v_MONEDA,
                            v_FLG_EGR_ING
                         )
                       + PKG_PAB_LIQUIDEZ.FN_PAB_BUS_TRAMO_PRMDO (
                            PAC.FEC_PAG_RLZ,
                            2200,
                            v_MONEDA,
                            v_FLG_EGR_ING
                         )
                       + PKG_PAB_LIQUIDEZ.FN_PAB_BUS_TRAMO_PRMDO (
                            PAC.FEC_PAG_RLZ,
                            2300,
                            v_MONEDA,
                            v_FLG_EGR_ING
                         )
                          AS FUERA_HORARIO
                FROM   PABS_DT_PAGOS_RLZAR_ACMDO PAC
               WHERE       PAC.COD_DVI = v_MONEDA
                       AND PAC.FEC_PAG_RLZ BETWEEN v_PAG_INI AND v_PAG_FIN
                       AND FLG_EGR_ING = v_FLG_EGR_ING   -- Campo en creacion.
            GROUP BY   TRUNC (PAC.FEC_PAG_RLZ), PAC.FEC_PAG_RLZ
            ORDER BY   PAC.FEC_PAG_RLZ DESC;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            err_code := SQLCODE;
            err_msg :=
               SUBSTR (SQLERRM, 1, 300) || 'No se puedo encontrar datos';
            RAISE;
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            p_s_mensaje := err_code || '-' || err_msg;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;

      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_BUS_PRMDO;

   /********************************************************************************************************
   --FUNCION/PROCEDIMIENTO: FN_PAB_BUS_TRAMO.
   --OBJETIVO             : retorna tramos de media hora para la grafica de productos.
   --SISTEMA              : DBO_PAB.
   --BASE DE DATOS        : DBO_PAB.
   --TABLAS USADAS        : PABS_DT_DETLL_OPRCN.
   --FECHA                : 09/11/18
   --AUTOR                : CAH.
   --INPUT                : P_NFOLIO := numero folio.
   --OUTPUT               : v_TRAMO  := valor de retorno.
   --OBSERVACIONES
   --FECHA       USUARIO    DETALLE
   --11/02/18    VAR        Se modifica funcion.
   ********************************************************************************************************/
   FUNCTION FN_PAB_BUS_TRAMO (P_NFOLIO NUMBER)
      RETURN VARCHAR2
   IS
      -------------------------------------DECLARACION DE VARIABLES Y CONSTANTES------------------------------
      V_NOM_SP   VARCHAR2 (30) := 'FN_PAB_BUS_TRAMO';
      v_TRAMO    VARCHAR2 (4);
   --------------------------------------------------------------------------------------------------------

   BEGIN
      --CONSULTO HORA Y REDONDEO
      BEGIN
           SELECT   TO_CHAR (
                       OPE.FEC_ACT_OPE + 1 / 48
                       - MOD (OPE.FEC_ACT_OPE - TRUNC (OPE.FEC_ACT_OPE),
                              1 / 48),
                       'HH24MI'
                    )
                       DT_ROUNDED_TO_HALF_HOUR
             INTO   V_TRAMO
             FROM   PABS_DT_DETLL_OPRCN OPE
            WHERE   OPE.NUM_FOL_OPE = P_NFOLIO
                    AND OPE.FEC_ISR_OPE BETWEEN v_DIA_INI AND v_DIA_FIN
         GROUP BY   TO_CHAR (
                       OPE.FEC_ACT_OPE + 1 / 48
                       - MOD (OPE.FEC_ACT_OPE - TRUNC (OPE.FEC_ACT_OPE),
                              1 / 48),
                       'HH24MI'
                    );
      END;

      --SI LA HORA ES ENTRE LAS 00:01 Y 06:59 AM, REDONDEO A 07:00 AM.
      BEGIN
         IF V_TRAMO BETWEEN '0001' AND '0759'
         THEN
            V_TRAMO := '0800';
         ELSE
            V_TRAMO := V_TRAMO;
         END IF;
      END;

      RETURN V_TRAMO;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         V_TRAMO := '0000';
         RETURN V_TRAMO;
      --ERR_CODE := SQLCODE;
      --ERR_MSG  := SUBSTR(SQLERRM, 1, 300);
      --PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD(ERR_CODE,
      --                                   V_NOM_PCK,
      --                                  ERR_MSG,
      --                                   V_NOM_SP);
      WHEN OTHERS
      THEN
         ERR_CODE := SQLCODE;
         ERR_MSG := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (ERR_CODE,
                                             V_NOM_PCK,
                                             ERR_MSG,
                                             V_NOM_SP);
         P_S_MENSAJE := ERR_CODE || '-' || ERR_MSG;
         RAISE_APPLICATION_ERROR (-20000, P_S_MENSAJE);
   END FN_PAB_BUS_TRAMO;

   --***********************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_BUS_TRAMO_MONTO
   -- Objetivo: Funcion que retorna tramos de media hora para la grafica de produtos
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas: PABS_DT_DETLL_MVNTO_CAJA
   --                 PABS_DT_TIPO_OPRCN_ALMOT
   --                 PABS_DT_TIPO_SUB_PRDTO
   -- Fecha: 09/11/2016
   -- Autor: CAH
   -- Input:
   --      p_FECHA -> Fecha y hora de consulta.
   -- N/A
   -- Output:
   -- N/A.
   -- Input/Output:
   -- N/A.
   -- Retorno:
   -- v_TRAMO -> valor de retorno
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   FUNCTION FN_PAB_BUS_TRAMO_MONTO (p_COD_OPE IN CHAR)
      RETURN NUMBER
   IS
      V_NOM_SP        VARCHAR2 (30) := 'FN_PAB_BUS_TRAMO_MONTO';
      V_HORA_INICIO   VARCHAR2 (5);
      v_MONTO_PROY    NUMBER;
   BEGIN
        SELECT   SUM (DET.IMP_OPE)
          INTO   v_MONTO_PROY
          FROM   PABS_DT_DETLL_MVNTO_CAJA DET,
                 PABS_DT_TIPO_OPRCN_ALMOT TOP,
                 PABS_DT_TIPO_SUB_PRDTO SUB
         WHERE       DET.COD_TPO_OPE_AOS = TOP.COD_TPO_OPE_AOS
                 AND TOP.COD_TPO_SPO = SUB.COD_TPO_SPO
                 AND DET.FLG_ING_EGR = 0
                 AND DET.FEC_VTA = v_FECHA_HOY
                 AND DET.COD_DVI = PKG_PAB_CONSTANTES.V_STR_DVI_CLP
                 AND SUB.COD_TPO_SPO = p_COD_OPE
      GROUP BY   SUB.DSC_TPO_SPO, SUB.COD_TPO_SPO;

      RETURN v_MONTO_PROY;
   --
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         v_MONTO_PROY := 0;
         RETURN v_MONTO_PROY;
      --         err_code := SQLCODE;
      --         err_msg := SUBSTR (SQLERRM, 1, 300);
      --         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
      --                                             v_NOM_PCK,
      --                                             err_msg,
      --                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END FN_PAB_BUS_TRAMO_MONTO;

   --**************************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_BUS_MONTO_BANCO
   -- Objetivo: Funcion que retorna el monto de ingreso o egreso de  un banco, por hora.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN
   --                PABS_DT_TIPO_OPRCN_ALMOT
   --                TCDTBAI
   --                PABS_DT_TIPO_SUB_PRDTO
   -- Fecha: 22-11-2018
   -- Autor: CAH
   -- Input:
   --      p_BANCO_ING     Fecha a cosultar
   --      p_FLAG          Hora a consultar
   --      p_COD_OPE       Codigo de operacion
   --      p_HORA          Hora a consultar
   -- N/A
   -- Output:
   -- N/A.
   -- Input/Output:
   -- N/A.
   -- Retorno:
   --      v_MONTO_HORA -> monto de la moneda consultada.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   --**************************************************************************************************
   FUNCTION FN_PAB_BUS_MONTO_BANCO (p_BANCO_ING   IN CHAR,
                                    p_MONEDA      IN CHAR,
                                    p_FLAG        IN NUMBER,
                                    p_COD_OPE     IN CHAR,
                                    p_HORA        IN NUMBER)
      RETURN NUMBER
   IS
      V_NOM_SP   VARCHAR2 (30) := 'FN_PAB_BUS_MONTO_BANCO';
      v_MONTO    NUMBER;
      v_VISADA   NUMBER;
   BEGIN
      IF p_FLAG = 0
      THEN
         SELECT   SUM (OPE.IMP_OPE)
           INTO   v_MONTO
           FROM   PABS_DT_DETLL_OPRCN OPE,
                  PABS_DT_TIPO_OPRCN_ALMOT TOP,
                  TCDTBAI TI,
                  PABS_DT_TIPO_SUB_PRDTO SUB
          WHERE       OPE.COD_TPO_OPE_AOS = TOP.COD_TPO_OPE_AOS
                  AND OPE.COD_BCO_DTN = TI.TGCDSWSA
                  AND TOP.COD_TPO_SPO = SUB.COD_TPO_SPO
                  AND TRUNC (OPE.FEC_ISR_OPE) = TRUNC (SYSDATE)
                  AND OPE.FEC_ISR_OPE BETWEEN v_DIA_INI AND v_DIA_FIN
                  AND OPE.COD_BCO_DTN = p_BANCO_ING
                  AND OPE.COD_DVI = p_MONEDA
                  AND SUB.COD_TPO_SPO = p_COD_OPE
                  AND OPE.FLG_EGR_ING = p_FLAG
                  AND PKG_PAB_LIQUIDEZ.FN_PAB_BUS_TRAMO (OPE.NUM_FOL_OPE) =
                        p_HORA;


         IF v_MONTO IS NULL
         THEN
            v_MONTO := 0;
         END IF;

         RETURN v_MONTO;
      ELSIF p_FLAG = 1
      THEN
         SELECT   SUM (OPE.IMP_OPE)
           INTO   v_MONTO
           FROM   PABS_DT_DETLL_OPRCN OPE,
                  PABS_DT_TIPO_OPRCN_ALMOT TOP,
                  TCDTBAI TI,
                  PABS_DT_TIPO_SUB_PRDTO SUB
          WHERE       OPE.COD_TPO_OPE_AOS = TOP.COD_TPO_OPE_AOS
                  AND OPE.COD_BCO_ORG = TI.TGCDSWSA
                  AND TOP.COD_TPO_SPO = SUB.COD_TPO_SPO
                  AND OPE.FEC_ISR_OPE BETWEEN v_DIA_INI AND v_DIA_FIN
                  AND OPE.COD_BCO_ORG = p_BANCO_ING
                  AND OPE.COD_DVI = p_MONEDA
                  AND SUB.COD_TPO_SPO = p_COD_OPE
                  AND OPE.FLG_EGR_ING = p_FLAG
                  AND PKG_PAB_LIQUIDEZ.FN_PAB_BUS_TRAMO (OPE.NUM_FOL_OPE) =
                        p_HORA;

         IF v_MONTO IS NULL
         THEN
            v_MONTO := 0;
         END IF;

         RETURN v_MONTO;
      END IF;
   --
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         v_MONTO := 0;
         RETURN v_MONTO;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END FN_PAB_BUS_MONTO_BANCO;

   /********************************************************************************************************
   --Funcion/Procedimiento: SP_PAB_BUS_MVNTO_PRDTO
   --Objetivo             : Muestra datos para grafica de productos en moneda nacional.
   --Sistema              : DBO_PAB.
   --Base De Datos        : DBO_PAB.
   --Tablas Usadas        : PABS_DT_DETLL_OPRCN
   --                      PABS_DT_TIPO_OPRCN_ALMOT
   --                      PABS_DT_TIPO_SUB_PRDTO
   --                      TCDTBAI

   --Fecha                : 29/08/18
   --Autor                : CAH.
   --Input                : P_COD_DVI   := MONEDA.
   --Output               : P_NETO_PROD := NETO PRODUCTOS.
   --                     P_ERROR     := INDICADOR DE ERRORES.
   --OBSERVACIONES
   --Fecha       Usuario    Detalle
   --11/02/18    VAR        Se Modifica Cursor.
   ********************************************************************************************************/
   PROCEDURE SP_PAB_BUS_MVNTO_PRDTO (P_COD_DVI     IN     CHAR,
                                     P_NETO_PROD      OUT SYS_REFCURSOR,
                                     P_ERROR          OUT NUMBER)
   IS
      -------------------------------------DECLARACION DE VARIABLES Y CONSTANTES------------------------------
      V_NOM_SP   VARCHAR2 (30) := 'SP_PAB_BUS_MVNTO_PRDTO';
   --------------------------------------------------------------------------------------------------------


   BEGIN
      BEGIN
         --BUSCA EL DETALLE DE PRODUCTOS DE OPERACIONES DEL DIA.
         OPEN P_NETO_PROD FOR
              SELECT   DISTINCT
                       (SUB.COD_TPO_SPO) COD_OPE,
                       PKG_PAB_LIQUIDEZ.FN_PAB_BUS_TRAMO (OPE.NUM_FOL_OPE) HORA,
                       OPE.COD_DVI MONEDA,
                       SUB.DSC_TPO_SPO DESCRIPCION_COD_OPE,
                       CASE
                          WHEN OPE.FLG_EGR_ING = PKG_PAB_CONSTANTES.V_FLG_EGR
                          THEN
                             OPE.COD_BCO_DTN
                          ELSE
                             OPE.COD_BCO_ORG
                       END
                          BANCO,
                       CASE
                          WHEN OPE.FLG_EGR_ING = PKG_PAB_CONSTANTES.V_FLG_EGR
                          THEN
                             BAI_DES.DES_BCO
                          ELSE
                             BAI_ORG.DES_BCO
                       END
                          DES_BANCO,
                       PKG_PAB_LIQUIDEZ.FN_PAB_BUS_MONTO_BANCO (
                          DECODE (OPE.FLG_EGR_ING,
                                  PKG_PAB_CONSTANTES.V_FLG_EGR,
                                  OPE.COD_BCO_DTN,
                                  OPE.COD_BCO_ORG),
                          P_COD_DVI,
                          PKG_PAB_CONSTANTES.V_FLG_ING,
                          SUB.COD_TPO_SPO,
                          PKG_PAB_LIQUIDEZ.FN_PAB_BUS_TRAMO (OPE.NUM_FOL_OPE)
                       )
                          INGRESO,
                       PKG_PAB_LIQUIDEZ.FN_PAB_BUS_MONTO_BANCO (
                          DECODE (OPE.FLG_EGR_ING,
                                  PKG_PAB_CONSTANTES.V_FLG_EGR,
                                  OPE.COD_BCO_DTN,
                                  OPE.COD_BCO_ORG),
                          P_COD_DVI,
                          PKG_PAB_CONSTANTES.V_FLG_EGR,
                          SUB.COD_TPO_SPO,
                          PKG_PAB_LIQUIDEZ.FN_PAB_BUS_TRAMO (OPE.NUM_FOL_OPE)
                       )
                          EGRESO,
                       PKG_PAB_LIQUIDEZ.FN_PAB_BUS_MONTO_BANCO (
                          DECODE (OPE.FLG_EGR_ING,
                                  PKG_PAB_CONSTANTES.V_FLG_EGR,
                                  OPE.COD_BCO_DTN,
                                  OPE.COD_BCO_ORG),
                          P_COD_DVI,
                          PKG_PAB_CONSTANTES.V_FLG_ING,
                          SUB.COD_TPO_SPO,
                          PKG_PAB_LIQUIDEZ.FN_PAB_BUS_TRAMO (OPE.NUM_FOL_OPE)
                       )
                       - PKG_PAB_LIQUIDEZ.FN_PAB_BUS_MONTO_BANCO (
                            DECODE (OPE.FLG_EGR_ING,
                                    PKG_PAB_CONSTANTES.V_FLG_EGR,
                                    OPE.COD_BCO_DTN,
                                    OPE.COD_BCO_ORG),
                            P_COD_DVI,
                            PKG_PAB_CONSTANTES.V_FLG_EGR,
                            SUB.COD_TPO_SPO,
                            PKG_PAB_LIQUIDEZ.FN_PAB_BUS_TRAMO (OPE.NUM_FOL_OPE)
                         )
                          NETO
                FROM   PABS_DT_DETLL_OPRCN OPE,
                       PABS_DT_TIPO_OPRCN_ALMOT TOP,
                       PABS_DT_TIPO_SUB_PRDTO SUB,
                       TCDTBAI BAI_ORG,
                       TCDTBAI BAI_DES
               WHERE       OPE.COD_TPO_OPE_AOS = TOP.COD_TPO_OPE_AOS
                       AND TOP.COD_TPO_SPO = SUB.COD_TPO_SPO
                       AND OPE.COD_BCO_ORG = BAI_ORG.TGCDSWSA(+)
                       AND OPE.COD_BCO_DTN = BAI_DES.TGCDSWSA(+)
                       AND OPE.FEC_ISR_OPE BETWEEN v_DIA_INI AND v_DIA_FIN
                       AND OPE.COD_DVI = NVL (P_COD_DVI, OPE.COD_DVI)
                       AND OPE.COD_EST_AOS IN
                                (PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAG, --12.PAGADA
                                 PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAGCFI, --21.PAGADA CONTINGENCIA
                                 PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEABN, --15.ABONADA
                                 PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDEV, --18.DEVUELTA
                                 PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAB) -- 16 POR ABONAR
            GROUP BY   SUB.DSC_TPO_SPO,
                       OPE.FLG_EGR_ING,
                       OPE.COD_TPO_OPE_AOS,
                       SUB.COD_TPO_SPO,
                       OPE.COD_BCO_ORG,
                       OPE.COD_BCO_DTN,
                       OPE.COD_DVI,
                       OPE.IMP_OPE,
                       OPE.FEC_ACT_OPE,
                       BAI_DES.DES_BCO,
                       BAI_ORG.DES_BCO,
                       OPE.NUM_FOL_OPE;
      --ORDER BY HORA, COD_OPE ASC;

      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            ERR_CODE := SQLCODE;
            ERR_MSG := SUBSTR (SQLERRM, 1, 300) || 'No se encontraron datos';
            RAISE;
         WHEN OTHERS
         THEN
            ERR_CODE := SQLCODE;
            ERR_MSG := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (ERR_CODE,
                                                V_NOM_PCK,
                                                ERR_MSG,
                                                V_NOM_SP);
            P_ERROR := PKG_PAB_CONSTANTES.V_ERROR;
            P_S_MENSAJE := ERR_CODE || '-' || ERR_MSG;
            RAISE_APPLICATION_ERROR (-20000, P_S_MENSAJE);
      END;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         ERR_CODE := SQLCODE;
         ERR_MSG := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (ERR_CODE,
                                             V_NOM_PCK,
                                             ERR_MSG,
                                             V_NOM_SP);
         P_ERROR := PKG_PAB_CONSTANTES.V_ERROR;
      WHEN OTHERS
      THEN
         ERR_CODE := SQLCODE;
         ERR_MSG := SUBSTR (SQLERRM, 1, 300);
         P_S_MENSAJE := ERR_CODE || '-' || ERR_MSG;
         PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (ERR_CODE,
                                             V_NOM_PCK,
                                             ERR_MSG,
                                             V_NOM_SP);
         P_ERROR := PKG_PAB_CONSTANTES.V_ERROR;
         RAISE_APPLICATION_ERROR (-20000, P_S_MENSAJE);
   END SP_PAB_BUS_MVNTO_PRDTO;

   /************************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_BUS_DETLL_MVNTO_PRDTO
   -- Objetivo: Procedimiento almacenado que muestra el detalle de productos
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN, PABS_DT_TIPO_OPRCN_ALMOT
   --
   -- Fecha: 22/10/2018
   -- Autor: Santander CAH
   -- Input:
   --      p_COD_PROD      -> Codigo de prodcuto a mostrar.
   -- Output:
   --      p_DETLL_PRDTO   -> Cursor con detalle de productos
   --      p_ERROR         -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --************************************************************************************************************/
   PROCEDURE SP_PAB_BUS_DETLL_MVNTO_PRDTO (
      p_COD_PROD      IN     CHAR,
      p_DETLL_PRDTO      OUT SYS_REFCURSOR,
      p_ERROR            OUT NUMBER
   )
   IS
      v_NOM_SP   VARCHAR2 (30) := 'SP_PAB_BUS_DETLL_MVNTO_PRDTO';
   BEGIN
      BEGIN --Productos de ingreso -egresso, Operaciones del dia con estado pagadas
         OPEN p_DETLL_PRDTO FOR
              SELECT   OPE.COD_TPO_OPE_AOS AS COD_OPE,
                       PKG_PAB_LIQUIDEZ.FN_PAB_BUS_TRAMO (OPE.NUM_FOL_OPE)
                          AS TRAMO,
                       SUM(DECODE (OPE.FLG_EGR_ING,
                                   PKG_PAB_CONSTANTES.V_FLG_EGR, OPE.IMP_OPE,
                                   0))
                          AS MONTO_EGR,
                       SUM(DECODE (OPE.FLG_EGR_ING,
                                   PKG_PAB_CONSTANTES.V_FLG_ING, OPE.IMP_OPE,
                                   0))
                          AS MONTO_ING,
                       SUM(DECODE (OPE.FLG_EGR_ING,
                                   PKG_PAB_CONSTANTES.V_FLG_ING, OPE.IMP_OPE,
                                   0))
                       - SUM(DECODE (OPE.FLG_EGR_ING,
                                     PKG_PAB_CONSTANTES.V_FLG_EGR, OPE.IMP_OPE,
                                     0))
                          AS NETO
                FROM   PABS_DT_DETLL_OPRCN OPE, PABS_DT_TIPO_OPRCN_ALMOT TOP
               WHERE   OPE.COD_TPO_OPE_AOS = TOP.COD_TPO_OPE_AOS
                       AND OPE.FEC_ISR_OPE BETWEEN v_DIA_INI AND v_DIA_FIN --  Operaciones del dia
                       AND OPE.COD_EST_AOS IN
                                (PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAG,
                                 PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAGCFI,
                                 PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEABN,
                                 PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDEV) --(12,21,15,18)
                       AND OPE.COD_DVI = PKG_PAB_CONSTANTES.V_STR_DVI_CLP
                       AND OPE.COD_TPO_OPE_AOS = p_COD_PROD       --'BCTES   '
            GROUP BY   OPE.COD_TPO_OPE_AOS,
                       PKG_PAB_LIQUIDEZ.FN_PAB_BUS_TRAMO (OPE.NUM_FOL_OPE)
            ORDER BY   TRAMO;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            err_code := SQLCODE;
            err_msg :=
               SUBSTR (SQLERRM, 1, 300) || 'No se puedo encontrar datos';
            RAISE;
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            p_s_mensaje := err_code || '-' || err_msg;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;

      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_BUS_DETLL_MVNTO_PRDTO;

   --**************************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_PAGO_REALIZADO
   -- Objetivo: Funcion que consulta el monto de pagos realizados acumulados por producto
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN
   --                  PABS_DT_TIPO_SUB_PRDTO
   --                  PABS_DT_TIPO_OPRCN_ALMOT
   -- Fecha: 28-01-2019
   -- Autor: Santander - CAH
   -- Input:
   --      p_COD_OPE     -> Fecha a cosultar
   --      p_MONEDA      -> Hora a consultar
   -- N/A
   -- Output:
   --      v_PAGO      -> Monto acumulado del prodcuto
   -- Input/Output:
   -- N/A.
   -- Retorno:
   --      v_MONTO_HORA -> monto de la moneda consultada.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   --**************************************************************************************************
   FUNCTION FN_PAB_PAGO_REALIZADO (p_COD_OPE IN CHAR, p_MONEDA IN CHAR)
      RETURN NUMBER
   IS
      V_NOM_SP   VARCHAR2 (30) := 'FN_PAB_PAGO_REALIZADO';
      v_PAGO     NUMBER;
   BEGIN
      SELECT   SUM (OPE.IMP_OPE)
        INTO   v_PAGO
        FROM   PABS_DT_TIPO_SUB_PRDTO SUB,
               PABS_DT_TIPO_OPRCN_ALMOT TOP,
               PABS_DT_DETLL_OPRCN OPE
       WHERE       SUB.COD_TPO_SPO = TOP.COD_TPO_SPO
               AND TOP.COD_TPO_OPE_AOS = OPE.COD_TPO_OPE_AOS
               AND SUB.COD_TPO_SPO = p_COD_OPE
               AND OPE.COD_DVI = p_MONEDA
               --AND OPE.FEC_ACT_OPE BETWEEN v_DIA_INI AND v_DIA_FIN
               AND OPE.FEC_VTA = v_FECHA_HOY
               AND OPE.COD_EST_AOS = PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAG;

      --Agregar estado de operaciones a Pagadas !!!

      IF v_PAGO IS NULL
      THEN
         v_PAGO := 0;
      END IF;


      RETURN v_PAGO;
   --
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         v_PAGO := 0;
         RETURN v_PAGO;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END FN_PAB_PAGO_REALIZADO;

   --**************************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_PAGO_PENDIENTE
   -- Objetivo: Funcion que consulta el monto de pagos pendientes(proyeccion) acumulados por producto
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas: PABS_DT_DETLL_MVNTO_CAJA
   --                  PABS_DT_TIPO_SUB_PRDTO
   --                  PABS_DT_TIPO_OPRCN_ALMOT
   -- Fecha: 28-01-2019
   -- Autor: Santander - CAH
   -- Input:
   --      p_COD_OPE     -> Fecha a cosultar
   --      p_MONEDA      -> Hora a consultar
   -- N/A
   -- Output:
   --      v_PAGO      -> Monto acumulado del prodcuto
   -- Input/Output:
   -- N/A.
   -- Retorno:
   --      v_MONTO_HORA -> monto de la moneda consultada.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   --**************************************************************************************************
   FUNCTION FN_PAB_PAGO_PENDIENTE (p_COD_OPE IN CHAR, p_MONEDA IN CHAR)
      RETURN NUMBER
   IS
      V_NOM_SP      VARCHAR2 (30) := 'FN_PAB_PAGO_PENDIENTE';
      v_PAGO_CAJA   NUMBER;
      v_PAGO_OPE    NUMBER;
      v_PAGO        NUMBER;
   BEGIN
      BEGIN                            --Operaciones cargadas como proyeccion.
         SELECT   SUM (DCJ.IMP_OPE)
           INTO   v_PAGO_CAJA
           FROM   PABS_DT_TIPO_SUB_PRDTO SUB,
                  PABS_DT_TIPO_OPRCN_ALMOT TOP,
                  PABS_DT_DETLL_MVNTO_CAJA DCJ
          WHERE       SUB.COD_TPO_SPO = TOP.COD_TPO_SPO
                  AND TOP.COD_TPO_OPE_AOS = DCJ.COD_TPO_OPE_AOS --OPE.COD_CNP_FIL
                  AND SUB.COD_TPO_SPO = p_COD_OPE
                  AND DCJ.COD_DVI = p_MONEDA
                  AND DCJ.FEC_VTA = v_FECHA_HOY;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            v_PAGO_CAJA := 0;
      END;


      IF v_PAGO_CAJA IS NULL
      THEN
         v_PAGO_CAJA := 0;
      END IF;

      BEGIN                                  -- Operaciones pendientes de pago
         SELECT   SUM (OPE.IMP_OPE)
           INTO   v_PAGO_OPE
           FROM   PABS_DT_TIPO_SUB_PRDTO SUB,
                  PABS_DT_TIPO_OPRCN_ALMOT TOP,
                  PABS_DT_DETLL_OPRCN OPE
          WHERE       SUB.COD_TPO_SPO = TOP.COD_TPO_SPO
                  AND TOP.COD_TPO_OPE_AOS = OPE.COD_TPO_OPE_AOS
                  AND SUB.COD_TPO_SPO = p_COD_OPE
                  AND OPE.COD_DVI = p_MONEDA
                  AND OPE.FEC_VTA = v_FECHA_HOY
                  --AND OPE.FEC_ACT_OPE BETWEEN v_DIA_INI AND v_DIA_FIN
                  AND OPE.COD_EST_AOS IN
                           (PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPELIB,
                            PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT,
                            PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPVI,
                            PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEENV);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            v_PAGO_OPE := 0;
      END;

      IF v_PAGO_OPE IS NULL
      THEN
         v_PAGO_OPE := 0;
      END IF;

      v_PAGO := v_PAGO_CAJA + v_PAGO_OPE;
      RETURN v_PAGO;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         v_PAGO := 0;
         RETURN v_PAGO;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END FN_PAB_PAGO_PENDIENTE;

   /********************************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_BUS_CNTRL_PAGO
   -- Objetivo: Procedimiento almacenado que muestra datos para grafica control productos en moneda nacional
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_TIPO_SUB_PRDTO
   --
   -- Fecha: 29-01-2019
   -- Autor: Santander CAH
   -- Input:
   --       p_MONEDA   MOneda a buscar
   -- Output:
   --       p_PAGOS
   --       p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --********************************************************************************************************************/
   PROCEDURE SP_PAB_BUS_CNTRL_PAGO (p_MONEDA   IN     CHAR,
                                    p_PAGOS       OUT SYS_REFCURSOR,
                                    p_ERROR       OUT NUMBER)
   IS
      v_NOM_SP   VARCHAR2 (30) := 'SP_PAB_BUS_CNTRL_PAGO';
      v_MONEDA   CHAR (3);
   BEGIN
      --MONEDA
      IF p_MONEDA IS NULL
      THEN
         v_MONEDA := PKG_PAB_CONSTANTES.V_STR_DVI_CLP;
      ELSE
         v_MONEDA := p_MONEDA;
      END IF;

      OPEN p_PAGOS FOR
           SELECT   SUB.DSC_TPO_SPO,
                    PKG_PAB_LIQUIDEZ.FN_PAB_PAGO_REALIZADO (SUB.COD_TPO_SPO,
                                                            v_MONEDA)
                       AS PAGO_REALIZADO,
                    PKG_PAB_LIQUIDEZ.FN_PAB_PAGO_PENDIENTE (SUB.COD_TPO_SPO,
                                                            v_MONEDA)
                       AS PAGO_PENDIENTE
             FROM   PABS_DT_TIPO_SUB_PRDTO SUB
         ORDER BY   SUB.DSC_TPO_SPO;

      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_BUS_CNTRL_PAGO;

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_CON_OPE_SWF_ALCO
   -- Objetivo: Procedimiento almacenado que retorna los datos para operaciones,( Especial ALCO)
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN, PABS_DT_OPRCN_INFCN_OPCON,
   --                PABS_DT_DETLL_OPRCN_HTRCA,  PABS_DT_OPRCN_OPCON_HTRCA,
   --                    PABS_DT_ESTDO_ALMOT,  PABS_DT_TIPO_OPRCN_ALMOT
   --
   -- Fecha: 29-08-2018
   -- Autor: Santander CAH
   -- Input:
   --      p_NUM_FOL_OPE -> Numero de folio operacion
   --      p_FEC_DESDE ->  fecha desde
   --      p_FEC_HASTA ->  fecha hasta
   --      p_COD_TPO_OPE_AOS ->  codigo tipo operacion estado
   --      p_COD_EST_AOS ->  codigo estado
   --      p_COD_SIS_ENT ->  codigo sistema entrada
   --      p_COD_SIS_SAL ->  codigo de sistema salida
   --      p_COD_BCO_DTN ->  codigo banco destino
   --      p_COD_BCO_ORG -> Codigo Bic banco origen
   --      p_COD_MT_SWF ->  codigo MT swf
   --      p_FLG_EGR_ING -> Flasg egreso ingreso
   --      p_IMP_OPE_DESDE ->  importe operacion desde
   --      p_IMP_OPE_HASTA ->  importe operacion hasta
   --      p_COD_DVI -> codigo de divisa
   -- Output:
   -- p_CURSOR -> Cursor con las nominas encontradas
   -- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   --             (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_CON_OPE_SWF_ALCO (
      p_COD_DVI           IN     CHAR,
      p_COD_BCO_ORG       IN     CHAR,
      p_COD_BCO_DTN       IN     CHAR,
      p_COD_TPO_OPE_AOS   IN     CHAR,
      p_NUM_FOL_OPE       IN     NUMBER,
      p_IMP_OPE_DESDE     IN     NUMBER,
      p_IMP_OPE_HASTA     IN     NUMBER,
      p_COD_SIS_ENT       IN     CHAR,
      p_COD_SIS_SAL       IN     CHAR,
      p_FEC_DESDE         IN     DATE,
      p_FEC_HASTA         IN     DATE,
      p_COD_EST_AOS       IN     NUMBER,
      p_COD_MT_SWF        IN     NUMBER,
      p_HOR_DESDE         IN     NUMBER,
      p_HOR_HASTA         IN     NUMBER,
      p_FLG_EGR_ING       IN     NUMBER,
      p_OPE_VISADA        IN     NUMBER,
      p_CURSOR               OUT SYS_REFCURSOR,
      p_ERROR                OUT NUMBER
   )
   IS
      v_NOM_SP            VARCHAR2 (30) := 'SP_PAB_CON_OPE_SWF_ALCO';
      v_IMp_DSD           NUMBER (15, 2);
      v_IMp_HST           NUMBER (15, 2);
      v_COD_DVI           CHAR (3);
      v_COD_EST_AOS       NUMBER;
      v_COD_MT            NUMBER;
      v_COD_BCO_DTN       CHAR (11);
      v_COD_BCO_ORG       CHAR (11);
      v_NUM_FOL_OPE       NUMBER (12);
      v_COD_TPO_OPE_AOS   CHAR (8);
      v_COD_SIS_ENT       CHAR (10);
      v_COD_SIS_SAL       CHAR (10);
      v_FLG_EGR_ING       NUMBER (1);
      v_FEC_DESDE         DATE;
      v_FEC_HASTA         DATE;
      v_HOR_DESDE         NUMBER;
      v_HOR_HASTA         NUMBER;
      v_VISADA_SI         NUMBER;
      v_VISADA_NO         NUMBER;
      v_VIS_CAJ           NUMBER;
      v_COD_EST_PAG       NUMBER;
      v_COD_EST_ABN       NUMBER;
      v_DET_HAB           NUMBER := 1;
      v_DET_BLO           NUMBER := 0;
   BEGIN
      --FOLIO DE OPERACION
      IF p_NUM_FOL_OPE = -1
      THEN
         v_NUM_FOL_OPE := NULL;
      ELSE
         v_NUM_FOL_OPE := p_NUM_FOL_OPE;
      END IF;

      --TIPO DE OPERACION
      IF p_COD_TPO_OPE_AOS = '-1'
      THEN
         v_COD_TPO_OPE_AOS := NULL;
      ELSE
         v_COD_TPO_OPE_AOS := p_COD_TPO_OPE_AOS;
      END IF;

      --CODIGO BANCO ORIGEN
      IF p_COD_BCO_ORG = '-1'
      THEN
         v_COD_BCO_ORG := NULL;
      ELSE
         v_COD_BCO_ORG := p_COD_BCO_ORG;
      END IF;

      --CODIGO BANCO DESTINO
      IF p_COD_BCO_DTN = '-1'
      THEN
         v_COD_BCO_DTN := NULL;
      ELSE
         v_COD_BCO_DTN := p_COD_BCO_DTN;
      END IF;

      --CODIGO MT
      IF p_COD_MT_SWF = -1
      THEN
         v_COD_MT := NULL;
      ELSE
         v_COD_MT := p_COD_MT_SWF;
      END IF;

      --FLAG INGRESO
      IF p_FLG_EGR_ING = -1
      THEN
         v_FLG_EGR_ING := NULL;
      ELSE
         v_FLG_EGR_ING := p_FLG_EGR_ING;
      END IF;

      --VALIDACION MONTO DESDE
      IF p_IMP_OPE_DESDE = -1
      THEN
         v_IMp_DSD := 0;
      ELSE
         v_IMp_DSD := p_IMP_OPE_DESDE;
      END IF;

      --VALIDACION MONTO HASTA
      IF p_IMP_OPE_HASTA = -1
      THEN
         v_IMp_HST := 9999999999999;
      ELSE
         v_IMp_HST := p_IMP_OPE_HASTA;
      END IF;

      --TIPO MONEDA
      IF p_COD_DVI = '-1'
      THEN
         v_COD_DVI := NULL;
      ELSE
         v_COD_DVI := p_COD_DVI;
      END IF;

      --ESTADO
      IF p_COD_EST_AOS = -1
      THEN
         -- IF p_COD_EST_AOS IS NULL  THEN
         --v_COD_EST_AOS := NULL;
         v_COD_EST_PAG := 0;
         v_COD_EST_ABN := 1;
      ELSE
         v_COD_EST_AOS := p_COD_EST_AOS;

         IF v_COD_EST_AOS = PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAG
         THEN                                  --0 CARGO 1 ABONO, 0 PAG 1 ABON
            v_COD_EST_PAG := 0;
            v_COD_EST_ABN := 2;
         END IF;

         IF v_COD_EST_AOS = PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEABN
         THEN
            v_COD_EST_PAG := 2;
            v_COD_EST_ABN := 1;
         END IF;
      END IF;

      --p_COD_SIS_ENT
      IF p_COD_SIS_ENT = '-1'
      THEN
         v_COD_SIS_ENT := NULL;
      ELSE
         v_COD_SIS_ENT := p_COD_SIS_ENT;
      END IF;

      --p_COD_SIS_SAL
      IF p_COD_SIS_SAL = '-1'
      THEN
         v_COD_SIS_SAL := NULL;
      ELSE
         v_COD_SIS_SAL := p_COD_SIS_SAL;
      END IF;

      IF p_FEC_DESDE IS NULL
      THEN
         v_FEC_DESDE := TRUNC (SYSDATE);
      ELSE
         v_FEC_DESDE := p_FEC_DESDE;
      END IF;

      IF p_FEC_HASTA IS NULL
      THEN
         v_FEC_HASTA := TRUNC (SYSDATE);
      ELSE
         v_FEC_HASTA := p_FEC_HASTA;
      END IF;


      IF p_HOR_DESDE = -1
      THEN
         v_HOR_DESDE := 0000;
      ELSE
         v_HOR_DESDE := p_HOR_DESDE;
      END IF;

      IF p_HOR_HASTA = -1
      THEN
         v_HOR_HASTA := 2359;
      ELSE
         v_HOR_HASTA := p_HOR_HASTA;
      END IF;

      IF p_OPE_VISADA = -1
      THEN
         v_VISADA_SI := 1;
         v_VISADA_NO := 0;
         v_VIS_CAJ := 0;
      ELSE
         v_VISADA_SI := p_OPE_VISADA;
         v_VISADA_NO := p_OPE_VISADA;
         v_VIS_CAJ := 2;
      END IF;

      OPEN p_CURSOR FOR
         SELECT   DETOP.NUM_FOL_OPE AS NUM_OPERACION,
                  DETOP.FEC_ISR_OPE AS FECHA_ISR_OPERACION,
                  DETOP.NUM_REF_SWF AS NUM_REF_SWIFT,
                  PKG_PAB_UTILITY.FN_PAB_OBT_DSC_SIS_ENT_SAL (
                     DETOP.COD_SIS_ENT
                  )
                     AS CANAL_ENTRADA,                            -- EX ORIGEN
                  PKG_PAB_UTILITY.FN_PAB_OBT_DSC_SIS_ENT_SAL (
                     DETOP.COD_SIS_SAL
                  )
                     AS CANAL_SALIDA,
                  DETOP.COD_MT_SWF AS MT_SWIFT,
                  DETOP.COD_TPO_OPE_AOS AS TIPO_OPERACION,
                  SP.DSC_TPO_SPO AS PRODUCTO,
                  EST.DSC_EST_AOS AS ESTADO,
                  DETOP.FEC_VTA AS FECHA_VALUTA,
                  TO_CHAR (DETOP.FEC_ACT_OPE, 'HH24:MM') AS HORA_PAGO,
                  DETOP.COD_BCO_DTN AS BANCO_DESTINO,
                  PKG_PAB_UTILITY.FN_PAB_OBT_DSC_BCO (DETOP.COD_BCO_DTN)
                     AS DSC_BCO_DTN,
                  DETOP.COD_BCO_ORG AS BANCO_ORIGEN,
                  PKG_PAB_UTILITY.FN_PAB_OBT_DSC_BCO (DETOP.COD_BCO_ORG)
                     AS DSC_BCO_ORG,
                  DETOP.COD_DVI AS MONEDA,
                  DETOP.IMP_OPE AS MONTO,
                  DECODE (DETOP.FLG_EGR_ING,
                          0, PKG_PAB_CONSTANTES.V_STR_FLG_EGR,
                          PKG_PAB_CONSTANTES.V_STR_FLG_ING)
                     AS TIPO_MOVIMIENTO,
                  DETOP.NOM_BFC AS NOMBRE_BFC,
                  NVL (
                     TRIM (DETOP.NUM_DOC_BFC),
                     NVL (DETOP.COD_TPD_BFC,
                          DETOP.NUM_DOC_BFC + '' + DETOP.COD_TPD_BFC)
                  )
                     AS RUT_BFC,
                  DETOP.COD_TPD_BFC AS DIGITO_RUT_BFC,
                  DETOP.NUM_CTA_BFC AS CUENTA_BFC,
                  DETPINF.NOM_ODN AS NOMBRE_ODN,
                  TRIM (DETPINF.NUM_DOC_ODN) AS RUT_ODN,
                  DETPINF.COD_TPD_BFC AS DIGITO_RUT_ODN,
                  DETPINF.NUM_CTA_ODN CUENTA_ODN,
                  DECODE (
                     PKG_PAB_LIQUIDEZ.FN_PAB_BUS_OPRCN_VSADA (
                        DETOP.NUM_FOL_OPE,
                        DETOP.FEC_ISR_OPE
                     ),
                     0,
                     'NO',
                     'SI'
                  )
                     AS VISADA,
                  v_DET_HAB AS DETALLE
           FROM               PABS_DT_DETLL_OPRCN DETOP
                           LEFT JOIN
                              PABS_DT_OPRCN_INFCN_OPCON DETPINF
                           ON DETOP.NUM_FOL_OPE = DETPINF.NUM_FOL_OPE
                              AND DETOP.FEC_ISR_OPE = DETPINF.FEC_ISR_OPE
                        LEFT JOIN
                           PABS_DT_ESTDO_ALMOT EST
                        ON EST.COD_EST_AOS = DETOP.COD_EST_AOS
                     LEFT JOIN
                        PABS_DT_TIPO_OPRCN_ALMOT TP
                     ON TP.COD_TPO_OPE_AOS = DETOP.COD_TPO_OPE_AOS
                  LEFT JOIN
                     PABS_DT_TIPO_SUB_PRDTO SP
                  ON SP.COD_TPO_SPO = TP.COD_TPO_SPO
          WHERE   DETOP.FEC_VTA BETWEEN v_FEC_DESDE AND v_FEC_HASTA
                  AND TO_CHAR (DETOP.FEC_ACT_OPE, 'HH24MM') BETWEEN v_HOR_DESDE
                                                                AND  v_HOR_HASTA
                  --AND DETOP.FEC_ACT_OPE BETWEEN TO_DATE(p_HOR_DESDE)  AND TO_DATE(p_HOR_HASTA)
                  AND DETOP.NUM_FOL_OPE LIKE
                        NVL (v_NUM_FOL_OPE, DETOP.NUM_FOL_OPE) || '%'
                  AND DETOP.COD_TPO_OPE_AOS =
                        NVL (v_COD_TPO_OPE_AOS, DETOP.COD_TPO_OPE_AOS)
                  AND DETOP.COD_EST_AOS =
                        NVL (v_COD_EST_AOS, DETOP.COD_EST_AOS)
                  AND NVL (DETOP.COD_SIS_ENT, '#') LIKE
                        NVL (v_COD_SIS_ENT, NVL (DETOP.COD_SIS_ENT, '#'))
                        || '%'
                  AND NVL (DETOP.COD_SIS_SAL, '#') LIKE
                        NVL (v_COD_SIS_SAL, NVL (DETOP.COD_SIS_SAL, '#'))
                        || '%'
                  AND DETOP.COD_BCO_DTN =
                        NVL (v_COD_BCO_DTN, DETOP.COD_BCO_DTN)
                  AND DETOP.COD_BCO_ORG =
                        NVL (v_COD_BCO_ORG, DETOP.COD_BCO_ORG) --Nuevo campo Bco Origen
                  AND DETOP.COD_MT_SWF = NVL (v_COD_MT, DETOP.COD_MT_SWF)
                  AND DETOP.FLG_EGR_ING =
                        NVL (v_FLG_EGR_ING, DETOP.FLG_EGR_ING) --Nuevo campo Flag ingreso-egreso
                  AND DETOP.IMP_OPE BETWEEN NVL (v_IMp_DSD, DETOP.IMP_OPE)
                                        AND  NVL (v_IMp_HST, DETOP.IMP_OPE)
                  AND DETOP.COD_DVI LIKE
                        NVL (v_COD_DVI, DETOP.COD_DVI) || '%'
                  AND PKG_PAB_LIQUIDEZ.FN_PAB_BUS_OPRCN_VSADA (
                        DETOP.NUM_FOL_OPE,
                        DETOP.FEC_ISR_OPE
                     ) IN
                           (v_VISADA_SI, v_VISADA_NO)
         UNION
         SELECT   DETOPHT.NUM_FOL_OPE AS NUM_OPERACION,
                  DETOPHT.FEC_ISR_OPE AS FECHA_ISR_OPERACION,
                  DETOPHT.NUM_REF_SWF AS NUM_REF_SWIFT,
                  PKG_PAB_UTILITY.FN_PAB_OBT_DSC_SIS_ENT_SAL (
                     DETOPHT.COD_SIS_ENT
                  )
                     AS CANAL_ENTRADA,
                  PKG_PAB_UTILITY.FN_PAB_OBT_DSC_SIS_ENT_SAL (
                     DETOPHT.COD_SIS_SAL
                  )
                     AS CANAL_SALIDA,
                  DETOPHT.COD_MT_SWF AS MT_SWIFT,
                  DETOPHT.COD_TPO_OPE_AOS AS TIPO_OPERACION,
                  SP.DSC_TPO_SPO AS PRODUCTO,
                  EST.DSC_EST_AOS AS ESTADO,
                  DETOPHT.FEC_VTA AS FECHA_VALUTA,
                  TO_CHAR (DETOPHT.FEC_ACT_OPE, 'HH24:MM') AS HORA_PAGO,
                  DETOPHT.COD_BCO_DTN AS BANCO_DESTINO,
                  PKG_PAB_UTILITY.FN_PAB_OBT_DSC_BCO (DETOPHT.COD_BCO_DTN)
                     AS DSC_BCO_DTN,
                  DETOPHT.COD_BCO_ORG AS BANCO_ORIGEN,
                  PKG_PAB_UTILITY.FN_PAB_OBT_DSC_BCO (DETOPHT.COD_BCO_ORG)
                     AS DSC_BCO_ORG,
                  DETOPHT.COD_DVI AS MONEDA,
                  DETOPHT.IMP_OPE AS MONTO,
                  DECODE (DETOPHT.FLG_EGR_ING,
                          0, PKG_PAB_CONSTANTES.V_STR_FLG_EGR,
                          PKG_PAB_CONSTANTES.V_STR_FLG_ING)
                     AS TIPO_MOVIMIENTO,
                  DETOPHT.NOM_BFC AS NOMBRE_BFC,
                  TRIM (DETOPHT.NUM_DOC_BFC) AS RUT_BFC,
                  DETOPHT.COD_TPD_BFC AS DIGITO_RUT_BFC,
                  DETOPHT.NUM_CTA_BFC AS CUENTA_BFC,
                  OPEOPHT.NOM_ODN AS NOMBRE_ODN,
                  TRIM (OPEOPHT.NUM_DOC_ODN) AS RUT_ODN,
                  OPEOPHT.COD_TPD_ODN AS DIGITO_RUT_ODN,
                  OPEOPHT.NUM_CTA_ODN CUENTA_ODN,
                  DECODE (
                     PKG_PAB_LIQUIDEZ.FN_PAB_BUS_OPRCN_VSADA (
                        OPEOPHT.NUM_FOL_OPE,
                        OPEOPHT.FEC_ISR_OPE
                     ),
                     0,
                     'NO',
                     'SI'
                  )
                     AS VISADA,
                  v_DET_HAB AS DETALLE
           FROM               PABS_DT_DETLL_OPRCN_HTRCA DETOPHT
                           LEFT JOIN
                              PABS_DT_OPRCN_OPCON_HTRCA OPEOPHT
                           ON DETOPHT.NUM_FOL_OPE = OPEOPHT.NUM_FOL_OPE
                              AND DETOPHT.FEC_ISR_OPE = OPEOPHT.FEC_ISR_OPE
                        LEFT JOIN
                           PABS_DT_ESTDO_ALMOT EST
                        ON EST.COD_EST_AOS = DETOPHT.COD_EST_AOS
                     LEFT JOIN
                        PABS_DT_TIPO_OPRCN_ALMOT TP
                     ON TP.COD_TPO_OPE_AOS = DETOPHT.COD_TPO_OPE_AOS
                  LEFT JOIN
                     PABS_DT_TIPO_SUB_PRDTO SP
                  ON SP.COD_TPO_SPO = TP.COD_TPO_SPO
          WHERE   DETOPHT.FEC_VTA BETWEEN v_FEC_DESDE AND v_FEC_HASTA
                  AND TO_CHAR (DETOPHT.FEC_ACT_OPE, 'HH24MM') BETWEEN v_HOR_DESDE
                                                                  AND  v_HOR_HASTA
                  --AND DETOPHT.FEC_ACT_OPE BETWEEN TO_DATE(p_HOR_DESDE)  AND TO_DATE(p_HOR_HASTA)
                  AND DETOPHT.NUM_FOL_OPE LIKE
                        NVL (v_NUM_FOL_OPE, DETOPHT.NUM_FOL_OPE) || '%'
                  AND DETOPHT.COD_TPO_OPE_AOS =
                        NVL (v_COD_TPO_OPE_AOS, DETOPHT.COD_TPO_OPE_AOS)
                  AND DETOPHT.COD_EST_AOS =
                        NVL (v_COD_EST_AOS, DETOPHT.COD_EST_AOS)
                  AND NVL (DETOPHT.COD_SIS_ENT, '#') LIKE
                        NVL (v_COD_SIS_ENT, NVL (DETOPHT.COD_SIS_ENT, '#'))
                        || '%'
                  AND NVL (DETOPHT.COD_SIS_SAL, '#') LIKE
                        NVL (v_COD_SIS_SAL, NVL (DETOPHT.COD_SIS_SAL, '#'))
                        || '%'
                  AND DETOPHT.COD_BCO_DTN =
                        NVL (v_COD_BCO_DTN, DETOPHT.COD_BCO_DTN)
                  AND DETOPHT.COD_BCO_ORG =
                        NVL (v_COD_BCO_ORG, DETOPHT.COD_BCO_ORG) --Nuevo campo bco Origen
                  AND DETOPHT.COD_MT_SWF = NVL (v_COD_MT, DETOPHT.COD_MT_SWF)
                  AND DETOPHT.FLG_EGR_ING =
                        NVL (v_FLG_EGR_ING, DETOPHT.FLG_EGR_ING) --Nuevo campo Flag ingreso-egreso
                  AND DETOPHT.IMP_OPE BETWEEN NVL (v_IMp_DSD,
                                                   DETOPHT.IMP_OPE)
                                          AND  NVL (v_IMp_HST,
                                                    DETOPHT.IMP_OPE)
                  AND DETOPHT.COD_DVI LIKE
                        NVL (v_COD_DVI, DETOPHT.COD_DVI) || '%'
                  AND PKG_PAB_LIQUIDEZ.FN_PAB_BUS_OPRCN_VSADA (
                        DETOPHT.NUM_FOL_OPE,
                        DETOPHT.FEC_ISR_OPE
                     ) IN
                           (v_VISADA_SI, v_VISADA_NO)                      --;
         UNION
         SELECT   CG.NUM_FOL_OPE AS NUM_OPERACION,
                  CG.FEC_ING_OPE AS FECHA_ISR_OPERACION,
                  CG.NUM_REF_SWF AS NUM_REF_SWIFT,
                  PKG_PAB_UTILITY.FN_PAB_OBT_DSC_SIS_ENT_SAL (CG.COD_SIS_ENT)
                     AS CANAL_ENTRADA,
                  PKG_PAB_UTILITY.FN_PAB_OBT_DSC_SIS_ENT_SAL (CG.COD_SIS_SAL)
                     AS CANAL_SALIDA,
                  CG.COD_MT_SWF AS MT_SWIFT,
                  CG.COD_TPO_OPE_AOS AS TIPO_OPERACION,
                  'OTRO' AS PRODUCTO,                                     ----
                  DECODE (CG.FLG_CGO_ABN, 1, 'Abonada', 'Pagada') AS ESTADO,
                  TRUNC (CG.FEC_ING_OPE) AS FECHA_VALUTA,
                  --CG.FEC_VTA AS FECHA_VALUTA,
                  TO_CHAR (CG.FEC_ING_OPE, 'HH24:MM') AS HORA_PAGO,
                  CG.COD_BCO_DTN AS BANCO_DESTINO,
                  PKG_PAB_UTILITY.FN_PAB_OBT_DSC_BCO (CG.COD_BCO_DTN)
                     AS DSC_BCO_DTN,
                  CG.COD_BCO_ORG AS BANCO_ORIGEN,
                  PKG_PAB_UTILITY.FN_PAB_OBT_DSC_BCO (CG.COD_BCO_ORG)
                     AS DSC_BCO_ORG,
                  CG.COD_DVI AS MONEDA,
                  CG.IMP_OPE AS MONTO,
                  DECODE (CG.FLG_CGO_ABN,
                          0, PKG_PAB_CONSTANTES.V_STR_FLG_EGR,
                          PKG_PAB_CONSTANTES.V_STR_FLG_ING)
                     AS TIPO_MOVIMIENTO,
                  CG.NOM_BFC AS NOMBRE_BFC,
                  TRIM (CG.NUM_DOC_BFC) AS RUT_BFC,
                  '' AS DIGITO_RUT_BFC,
                  CG.NUM_CTA_CTE AS CUENTA_BFC,
                  CG.NOM_ODN AS NOMBRE_ODN,
                  TRIM (CG.NUM_DOC_ODN) AS RUT_ODN,
                  '' AS DIGITO_RUT_ODN,
                  CG.NUM_CTA_ODN AS CUENTA_ODN,
                  'NO' AS VISADA,
                  v_DET_BLO AS DETALLE
           FROM   PABS_DT_DETLL_CARGO_ABONO_CAJA CG
          WHERE   TRUNC (CG.FEC_ING_OPE) BETWEEN v_FEC_DESDE AND v_FEC_HASTA
                  AND CG.FEC_VTA BETWEEN v_FEC_DESDE AND v_FEC_HASTA
                  AND TO_CHAR (CG.FEC_ING_OPE, 'HH24MM') BETWEEN v_HOR_DESDE
                                                             AND  v_HOR_HASTA
                  AND CG.NUM_FOL_OPE LIKE
                        NVL (v_NUM_FOL_OPE, CG.NUM_FOL_OPE) || '%'
                  AND CG.FLG_CGO_ABN IN (v_COD_EST_PAG, v_COD_EST_ABN) -- ESTADO NO HAY SOLO CARGO O ABONO
                  AND NVL (CG.COD_SIS_ENT, '#') LIKE
                        NVL (v_COD_SIS_ENT, NVL (CG.COD_SIS_ENT, '#')) || '%'
                  AND NVL (CG.COD_SIS_SAL, '#') LIKE
                        NVL (v_COD_SIS_SAL, NVL (CG.COD_SIS_SAL, '#')) || '%'
                  AND CG.COD_BCO_DTN = NVL (v_COD_BCO_DTN, CG.COD_BCO_DTN)
                  AND CG.COD_BCO_ORG =
                        NVL (TRIM (v_COD_BCO_ORG), CG.COD_BCO_ORG)
                  AND CG.FLG_CGO_ABN = NVL (v_FLG_EGR_ING, CG.FLG_CGO_ABN)
                  AND CG.COD_MT_SWF = NVL (v_COD_MT, CG.COD_MT_SWF)
                  AND CG.IMP_OPE BETWEEN NVL (v_IMp_DSD, CG.IMP_OPE)
                                     AND  NVL (v_IMp_HST, CG.IMP_OPE)
                  AND CG.COD_DVI LIKE NVL (v_COD_DVI, CG.COD_DVI) || '%'
                  AND CG.FLG_CGO_ABN = v_VIS_CAJ;         -- VISADO NO MOSTRAR

      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   --Procedimiento ejecutado de forma correcta
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_CON_OPE_SWF_ALCO;

   --***********************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_BUS_OPRCN_VSADA
   -- Objetivo: Funcion que retorna si la operacion fue Visada
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN ,
   --                  PABS_DT_NOMNA_INFCN_OPCON,
   --                  PABS_DT_BTCOR_OPRCN
   -- Fecha: 21-11-2018
   -- Autor: CAH
   -- Input:
   --      p_FOLIO_OPE     -> Fecha a cosultar
   --      p_FECHA_OPE      -> Hora a consultar
   -- N/A
   -- Output:
   -- N/A.
   -- Input/Output:
   -- N/A.
   -- Retorno:
   --      v_MONTO_HORA -> monto de la moneda consultada.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   --***********************************************************************************************
   FUNCTION FN_PAB_BUS_OPRCN_VSADA (p_FOLIO_OPE   IN NUMBER,
                                    p_FECHA_OPE   IN DATE)
      RETURN NUMBER
   IS
      V_NOM_SP   VARCHAR2 (30) := 'FN_PAB_BUS_TRAMO_PRMDO';
      v_TRAMO    NUMBER;
      v_VISADA   NUMBER;
   BEGIN
      SELECT   COUNT (OPD.NUM_FOL_OPE)
        INTO   v_VISADA
        FROM   PABS_DT_DETLL_OPRCN OPD,
               PABS_DT_NOMNA_INFCN_OPCON OP,
               PABS_DT_BTCOR_OPRCN BO
       WHERE       OPD.NUM_FOL_OPE = OP.NUM_FOL_OPE
               AND OPD.FEC_ISR_OPE = OP.FEC_ISR_OPE
               AND OP.NUM_FOL_OPE = BO.NUM_FOL_OPE
               AND OP.FEC_ISR_OPE = BO.FEC_ISR_OPE
               AND BO.FEC_ISR_BTC BETWEEN v_DIA_INI AND v_DIA_FIN
               AND OP.GLS_MTV_VSD IS NOT NULL
               AND BO.COD_EST_ICO = PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPVI --8
               AND BO.COD_EST_FIN = PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT --7
               AND OPD.NUM_FOL_OPE = p_FOLIO_OPE
               AND OPD.FEC_ISR_OPE = p_FECHA_OPE;


      RETURN v_VISADA;
   --
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         v_VISADA := 0;
         RETURN v_VISADA;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END FN_PAB_BUS_OPRCN_VSADA;

   /******************************************************************************************************************************
      -- Funcion/Procedimiento: SP_PAB_CON_SALDO_LIQDZ
      -- Objetivo: Procedimiento almacenado que retorna los saldos de moneda nacional
      -- Sistema: DBO_PAB.
      -- Base de Datos: PABS_DT_DETLL_OPRCN_HTRCA
      -- Tablas Usadas:
      --
      -- Fecha: 08-11-2018
      -- Autor: Santander CAH
      -- Input:
      --      p_FECHA_INI      Fecha inicio de consulta
      --      p_FECHA_FIN      Fecha fin de consulta
      --      p_HORA_INI       Hora incioo consulta
      --      p_HORA_FIN       Hora fin consulta
      --      p_BANCO          Banco a consultar
      --      p_MONEDA         Moneda a consultar
      --
      -- Output:
      --      p_SALDO_LBTR       Cursor con acumulado LBTR
      --      p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
      -- Input/Output: N/A
      -- Retorno: N/A.
      -- Observaciones: <Fecha y Detalle de ultimos cambios>
      --********************************************************************************************/
   PROCEDURE SP_PAB_CON_SALDO_LIQDZ (p_FECHA_INI   IN     DATE,
                                     p_FECHA_FIN   IN     DATE,
                                     p_HORA_INI    IN     NUMBER,
                                     p_HORA_FIN    IN     NUMBER,
                                     p_BANCO       IN     CHAR,
                                     p_MONEDA      IN     CHAR,
                                     p_SALDO          OUT SYS_REFCURSOR,
                                     --   p_SALDO          OUT,
                                     p_ERROR          OUT NUMBER)
   IS
      v_NOM_SP             VARCHAR2 (30) := 'SP_PAB_CON_SALDO_LIQDZ';
      v_FECHA_INI          NUMBER;
      v_FECHA_FIN          NUMBER;
      v_HORA_INI           NUMBER;
      v_HORA_FIN           NUMBER;
      v_MONEDA             CHAR (3);
      v_BANCO              PABS_DT_RGTRO_SALDO_HTRCO.COD_BIC_BCO%TYPE;
      v_FECHA              DATE;
      v_FECHA_ACTUAL       DATE := TRUNC (SYSDATE);
      v_CONVESION_MONEDA   NUMBER;
      v_ERRORCODE          NUMBER;
   BEGIN
      PKG_PAB_UTILITY.SP_PAB_DEV_DIA_HABIL_ANT ('SN', v_FECHA_ACTUAL);
      v_FECHA := v_FECHA_ACTUAL;



      IF p_FECHA_INI IS NULL
      THEN
         v_FECHA_INI := TO_NUMBER (TO_CHAR (SYSDATE, 'YYYYMMDD'));
      ELSE
         v_FECHA_INI := TO_NUMBER (TO_CHAR (p_FECHA_INI, 'YYYYMMDD'));
      END IF;


      IF p_FECHA_FIN IS NULL
      THEN
         v_FECHA_FIN := TO_NUMBER (TO_CHAR (SYSDATE, 'YYYYMMDD'));
      ELSE
         v_FECHA_FIN := TO_NUMBER (TO_CHAR (p_FECHA_FIN, 'YYYYMMDD'));
      END IF;


      --HORA
      IF p_HORA_INI = -1
      THEN
         v_HORA_INI := 0000;
      ELSE
         v_HORA_INI := p_HORA_INI;
      END IF;


      IF p_HORA_FIN = -1
      THEN
         v_HORA_FIN := 2359;
      ELSE
         v_HORA_FIN := p_HORA_FIN;
      END IF;


      --MONEDA
      IF p_MONEDA IS NULL
      THEN
         v_MONEDA := NULL;
      ELSE
         v_MONEDA := p_MONEDA;
      END IF;

      --BANCO
      IF p_BANCO IS NULL
      THEN
         v_BANCO := NULL;
      ELSE
         v_BANCO := p_BANCO;
      END IF;

      IF v_MONEDA = PKG_PAB_CONSTANTES.V_STR_DVI_CLP
      THEN
         BEGIN
            OPEN p_SALDO FOR
                 SELECT   PKG_PAB_CONSTANTES.V_BIC_BCCH AS CONTRAPARTE,
                          TRIM(PKG_PAB_UTILITY.FN_PAB_OBT_DSC_BCO (
                                  PKG_PAB_CONSTANTES.V_BIC_BCCH
                               ))
                             AS NOMBRE_BANCO,
                          v_FECHA_FIN AS FECHA,
                          SUM (SH.IMP_SDO_APR) SALDO_APER,
                          SUM (SH.IMP_NET_ING) AS INGRESOS,
                          SUM (SH.IMP_NET_EGR) AS EGRESOS,
                            SUM (IMP_SDO_APR)
                          + SUM (SH.IMP_NET_ING)
                          - SUM (SH.IMP_NET_EGR)
                             AS SALDO_FINAL,
                          MIN (SH.IMP_NET_MIN) + SUM (SH.IMP_SDO_APR)
                             AS NETO_MIN,
                          MAX (SH.IMP_NET_MAX) + SUM (SH.IMP_SDO_APR)
                             AS NETO_MAX,
                          NVL (
                             (SELECT   MIN (IMP_MIN_ING)
                                FROM   PABS_DT_RGTRO_SALDO_HTRCO SH
                               WHERE   SH.IMP_MIN_ING > 0
                                       AND SH.COD_CNL_SAL =
                                             PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_LBTR
                                       AND SH.FEC_REG_SDO BETWEEN v_FECHA_INI
                                                              AND  v_FECHA_FIN
                                       AND SH.HOR_REG_SDO BETWEEN v_HORA_INI
                                                              AND  v_HORA_FIN
                                       AND SH.COD_DVI =
                                             PKG_PAB_CONSTANTES.V_STR_DVI_CLP
                                       AND SH.COD_BIC_BCO =
                                             NVL (
                                                v_BANCO,
                                                PKG_PAB_CONSTANTES.V_BIC_BCCH
                                             )),
                             0
                          )
                             AS MIN_ING,
                          MAX (SH.IMP_MAX_ING) AS MAX_ING,
                          NVL (
                             (SELECT   MIN (IMP_MIN_EGR)
                                FROM   PABS_DT_RGTRO_SALDO_HTRCO SH
                               WHERE   SH.IMP_MIN_EGR > 0
                                       AND SH.COD_CNL_SAL =
                                             PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_LBTR
                                       AND SH.FEC_REG_SDO BETWEEN v_FECHA_INI
                                                              AND  v_FECHA_FIN
                                       AND SH.HOR_REG_SDO BETWEEN v_HORA_INI
                                                              AND  v_HORA_FIN
                                       AND SH.COD_DVI =
                                             PKG_PAB_CONSTANTES.V_STR_DVI_CLP
                                       AND SH.COD_BIC_BCO =
                                             NVL (
                                                v_BANCO,
                                                PKG_PAB_CONSTANTES.V_BIC_BCCH
                                             )),
                             0
                          )
                             AS MIN_EGR,
                          MAX (SH.IMP_MAX_EGR) AS MAX_EGR,
                          SH.COD_CNL_SAL AS CANAL
                   FROM   PABS_DT_RGTRO_SALDO_HTRCO SH
                  WHERE   SH.FEC_REG_SDO BETWEEN v_FECHA_INI AND v_FECHA_FIN
                          AND SH.HOR_REG_SDO BETWEEN v_HORA_INI AND v_HORA_FIN
                          AND SH.COD_DVI = PKG_PAB_CONSTANTES.V_STR_DVI_CLP
                          AND SH.COD_CNL_SAL =
                                PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_LBTR
                          AND SH.COD_BIC_BCO = PKG_PAB_CONSTANTES.V_BIC_BCCH
               GROUP BY   SH.COD_DVI, SH.COD_CNL_SAL
               UNION
                 SELECT   PKG_PAB_CONSTANTES.V_BIC_COMBANC AS CONTRAPARTE,
                          'COMBANC' AS NOMBRE_BANCO,
                          v_FECHA_FIN AS FECHA,
                          SUM (SH.IMP_SDO_APR) AS SALDO_APER,
                          SUM (SH.IMP_NET_ING) AS INGRESOS,
                          SUM (SH.IMP_NET_EGR) AS EGRESOS,
                          SUM (SH.IMP_NET_ING) - SUM (SH.IMP_NET_EGR)
                             AS SALDO_FINAL,
                          NVL (
                             (  SELECT   MIN (IMP_NET_MIN)
                                  FROM   PABS_DT_RGTRO_SALDO_HTRCO
                                 WHERE   IMP_NET_MIN <> 0
                                         AND FEC_REG_SDO BETWEEN v_FECHA_INI
                                                             AND  v_FECHA_FIN
                                         AND HOR_REG_SDO BETWEEN v_HORA_INI
                                                             AND  v_HORA_FIN
                                         AND COD_DVI =
                                               PKG_PAB_CONSTANTES.V_STR_DVI_CLP
                                         AND COD_BIC_BCO <>
                                               PKG_PAB_CONSTANTES.V_BIC_BCCH
                                         AND COD_BIC_BCO =
                                               NVL (v_BANCO, COD_BIC_BCO)
                                         AND COD_CNL_SAL =
                                               PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_CMN
                              GROUP BY   'COMBANC', COD_CNL_SAL),
                             0
                          )
                             AS NETO_MIN,
                          MAX (SH.IMP_NET_MAX) AS NETO_MAX,
                          NVL (
                             (SELECT   MIN (IMP_MIN_ING)
                                FROM   PABS_DT_RGTRO_SALDO_HTRCO SH
                               WHERE   SH.IMP_MIN_ING <> 0
                                       AND SH.FEC_REG_SDO BETWEEN v_FECHA_INI
                                                              AND  v_FECHA_FIN
                                       AND SH.HOR_REG_SDO BETWEEN v_HORA_INI
                                                              AND  v_HORA_FIN
                                       AND SH.COD_DVI =
                                             PKG_PAB_CONSTANTES.V_STR_DVI_CLP
                                       AND SH.COD_BIC_BCO <>
                                             PKG_PAB_CONSTANTES.V_BIC_BCCH
                                       AND SH.COD_BIC_BCO =
                                             NVL (v_BANCO, SH.COD_BIC_BCO)
                                       AND SH.COD_CNL_SAL =
                                             PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_CMN),
                             0
                          )
                             AS MIN_ING,
                          MAX (SH.IMP_MAX_ING) AS MAX_ING,
                          NVL (
                             (SELECT   MIN (IMP_MIN_EGR)
                                FROM   PABS_DT_RGTRO_SALDO_HTRCO SH
                               WHERE   SH.IMP_MIN_EGR <> 0
                                       AND SH.FEC_REG_SDO BETWEEN v_FECHA_INI
                                                              AND  v_FECHA_FIN
                                       AND SH.HOR_REG_SDO BETWEEN v_HORA_INI
                                                              AND  v_HORA_FIN
                                       AND SH.COD_DVI =
                                             PKG_PAB_CONSTANTES.V_STR_DVI_CLP
                                       AND SH.COD_BIC_BCO <>
                                             PKG_PAB_CONSTANTES.V_BIC_BCCH
                                       AND SH.COD_BIC_BCO =
                                             NVL (v_BANCO, SH.COD_BIC_BCO)
                                       AND SH.COD_CNL_SAL =
                                             PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_CMN),
                             0
                          )
                             AS MIN_EGR,
                          MAX (SH.IMP_MAX_EGR) AS MAX_EGR,
                          SH.COD_CNL_SAL AS CANAL
                   FROM   PABS_DT_RGTRO_SALDO_HTRCO SH
                  WHERE   SH.FEC_REG_SDO BETWEEN v_FECHA_INI AND v_FECHA_FIN
                          AND SH.HOR_REG_SDO BETWEEN v_HORA_INI AND v_HORA_FIN
                          AND SH.COD_DVI = PKG_PAB_CONSTANTES.V_STR_DVI_CLP
                          AND SH.COD_BIC_BCO <> PKG_PAB_CONSTANTES.V_BIC_BCCH
                          AND SH.COD_BIC_BCO = NVL (v_BANCO, SH.COD_BIC_BCO)
                          AND SH.COD_CNL_SAL =
                                PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_CMN
               GROUP BY   'COMBANC', SH.COD_CNL_SAL;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               err_code := SQLCODE;
               err_msg :=
                  SUBSTR (SQLERRM, 1, 300) || 'No se puedo encontrar datos';
               RAISE;
            WHEN OTHERS
            THEN
               err_code := SQLCODE;
               err_msg := SUBSTR (SQLERRM, 1, 300);
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
               p_s_mensaje := err_code || '-' || err_msg;
               RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
         END;
      ELSE
         BEGIN                                           --Saldo Real Intradia
            OPEN p_SALDO FOR
                 SELECT   SH.COD_DVI AS MONEDA,
                          TRUNC (SYSDATE) AS FECHA,
                          --MAX(SH.IMP_SDO_APR) AS SALDO_APER,
                          PKG_PAB_LIQUIDEZ.FN_PAB_BUS_SALDO (
                             0,
                             NVL (v_MONEDA, SH.COD_DVI),
                             v_BANCO,
                             v_FECHA_INI,
                             v_FECHA_FIN,
                             v_HORA_INI,
                             v_HORA_FIN
                          )
                             AS SALDO_APER,
                          SUM (SH.IMP_NET_ING) AS INGRESOS,
                          SUM (SH.IMP_NET_EGR) AS EGRESOS,
                          PKG_PAB_LIQUIDEZ.FN_PAB_BUS_SALDO (
                             0,
                             NVL (v_MONEDA, SH.COD_DVI),
                             v_BANCO,
                             v_FECHA_INI,
                             v_FECHA_FIN,
                             v_HORA_INI,
                             v_HORA_FIN
                          )
                          + SUM (SH.IMP_NET_ING)
                          - SUM (SH.IMP_NET_EGR)
                             AS SALDO_FIN,
                          PKG_PAB_LIQUIDEZ.FN_PAB_BUS_SALDO (
                             1,
                             NVL (v_MONEDA, SH.COD_DVI),
                             v_BANCO,
                             v_FECHA_INI,
                             v_FECHA_FIN,
                             v_HORA_INI,
                             v_HORA_FIN
                          )
                          * (PKG_PAB_LIQUIDEZ.FN_PAB_BUS_CONVERSION (
                                v_FECHA,
                                'USD',
                                SH.COD_DVI
                             ))
                             AS EQUIVALENTE_USD, --Variable arescatar de CHI_CON
                          --MIN (SH.IMP_NET_MIN) AS NETO_MIN,
                          NVL (
                             (SELECT   MIN (IMP_NET_MIN)
                                FROM   PABS_DT_RGTRO_SALDO_HTRCO SH
                               WHERE   SH.IMP_NET_MIN > 0
                                       AND SH.COD_CNL_SAL =
                                             PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_SWF
                                       AND SH.COD_DVI =
                                             NVL (v_MONEDA, SH.COD_DVI)
                                       AND SH.COD_BIC_BCO =
                                             NVL (v_BANCO, SH.COD_BIC_BCO)
                                       AND SH.FEC_REG_SDO BETWEEN v_FECHA_INI
                                                              AND  v_FECHA_FIN
                                       AND SH.HOR_REG_SDO BETWEEN v_HORA_INI
                                                              AND  v_HORA_FIN),
                             0
                          )
                             AS NETO_MIN,
                          MAX (SH.IMP_NET_MAX) AS NETO_MAX,
                          NVL (
                             (SELECT   MIN (IMP_MIN_ING)
                                FROM   PABS_DT_RGTRO_SALDO_HTRCO SH
                               WHERE   SH.IMP_MIN_ING > 0
                                       AND SH.COD_CNL_SAL =
                                             PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_SWF
                                       AND SH.COD_DVI =
                                             NVL (v_MONEDA, SH.COD_DVI)
                                       AND SH.COD_BIC_BCO =
                                             NVL (v_BANCO, SH.COD_BIC_BCO)
                                       AND SH.FEC_REG_SDO BETWEEN v_FECHA_INI
                                                              AND  v_FECHA_FIN
                                       AND SH.HOR_REG_SDO BETWEEN v_HORA_INI
                                                              AND  v_HORA_FIN),
                             0
                          )
                             AS MIN_ING,
                          MAX (SH.IMP_MAX_ING) AS MAX_ING,
                          NVL (
                             (SELECT   MIN (IMP_MIN_EGR)
                                FROM   PABS_DT_RGTRO_SALDO_HTRCO SH
                               WHERE   SH.IMP_MIN_EGR > 0
                                       AND SH.COD_CNL_SAL =
                                             PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_SWF
                                       AND SH.COD_DVI =
                                             NVL (v_MONEDA, SH.COD_DVI)
                                       AND SH.COD_BIC_BCO =
                                             NVL (v_BANCO, SH.COD_BIC_BCO)
                                       AND SH.FEC_REG_SDO BETWEEN v_FECHA_INI
                                                              AND  v_FECHA_FIN
                                       AND SH.HOR_REG_SDO BETWEEN v_HORA_INI
                                                              AND  v_HORA_FIN),
                             0
                          )
                             AS MIN_EGR,
                          MAX (SH.IMP_MAX_EGR) AS MAX_EGR,
                          SH.COD_CNL_SAL AS CANAL
                   FROM   PABS_DT_RGTRO_SALDO_HTRCO SH
                  WHERE   SH.COD_CNL_SAL =
                             PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_SWF
                          AND SH.COD_DVI = NVL (v_MONEDA, SH.COD_DVI)
                          AND SH.COD_DVI <> PKG_PAB_CONSTANTES.V_STR_DVI_CLP
                          AND SH.COD_BIC_BCO = NVL (v_BANCO, SH.COD_BIC_BCO)
                          AND SH.FEC_REG_SDO BETWEEN v_FECHA_INI
                                                 AND  v_FECHA_FIN
                          AND SH.HOR_REG_SDO BETWEEN v_HORA_INI AND v_HORA_FIN
               GROUP BY   SH.COD_DVI, SH.COD_CNL_SAL
               ORDER BY   SH.COD_DVI;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               err_code := SQLCODE;
               err_msg :=
                  SUBSTR (SQLERRM, 1, 300) || 'No se puedo encontrar datos';
               RAISE;
            WHEN OTHERS
            THEN
               err_code := SQLCODE;
               err_msg := SUBSTR (SQLERRM, 1, 300);
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
               p_s_mensaje := err_code || '-' || err_msg;
               RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
         END;
      END IF;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_CON_SALDO_LIQDZ;

   /******************************************************************************************************************************
     -- Funcion/Procedimiento: SP_PAB_CON_DETLL_LIQDZ
     -- Objetivo: Procedimiento almacenado que retorna los saldos de moneda nacional
     -- Sistema: DBO_PAB.
     -- Base de Datos: PABS_DT_RGTRO_SALDO_HTRCO
     -- Tablas Usadas:
     --
     -- Fecha: 08-11-2018
     -- Autor: Santander CAH
     -- Input:
     --      p_FECHA_INI      Fecha inicio de consulta
     --      p_FECHA_FIN      Fecha fin de consulta
     --      p_HORA_INI       Hora incioo consulta
     --      p_HORA_FIN       Hora fin consulta
     --      p_BANCO          Banco a consultar
     --      p_MONEDA         Moneda a consultar
     --      p_CANAL          Canal a consultar
     --
     -- Output:
     --      p_DETLL       Cursor con acumulado por banco
     --      p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
     -- Input/Output: N/A
     -- Retorno: N/A.
     -- Observaciones: <Fecha y Detalle de ultimos cambios>
     --********************************************************************************************/
   PROCEDURE SP_PAB_CON_DETLL_LIQDZ (p_FECHA_INI   IN     DATE,
                                     p_FECHA_FIN   IN     DATE,
                                     p_HORA_INI    IN     NUMBER,
                                     p_HORA_FIN    IN     NUMBER,
                                     p_BANCO       IN     CHAR,
                                     p_MONEDA      IN     CHAR,
                                     p_CANAL       IN     CHAR,
                                     p_DETLL          OUT SYS_REFCURSOR,
                                     p_ERROR          OUT NUMBER)
   IS
      v_NOM_SP             VARCHAR2 (30) := 'SP_PAB_CON_DETLL_LIQDZ';
      v_FECHA_INI          NUMBER;
      v_FECHA_FIN          NUMBER;
      v_HORA_INI           NUMBER;
      v_HORA_FIN           NUMBER;
      v_BANCO              CHAR (11);
      v_MONEDA             CHAR (3);
      v_CANAL              PABS_DT_RGTRO_SALDO_HTRCO.COD_CNL_SAL%TYPE;
      v_FECHA              DATE;
      v_FECHA_ACTUAL       DATE := TRUNC (SYSDATE);
      v_CONVESION_MONEDA   NUMBER;
      v_ERRORCODE          NUMBER;
   BEGIN
      PKG_PAB_UTILITY.SP_PAB_DEV_DIA_HABIL_ANT ('SN', v_FECHA_ACTUAL);
      v_FECHA := v_FECHA_ACTUAL;

      --FECHA
      IF p_FECHA_INI IS NULL
      THEN
         v_FECHA_INI := TO_NUMBER (TO_CHAR (SYSDATE, 'YYYYMMDD'));
      ELSE
         v_FECHA_INI := TO_NUMBER (TO_CHAR (p_FECHA_INI, 'YYYYMMDD'));
      END IF;


      IF p_FECHA_FIN IS NULL
      THEN
         v_FECHA_FIN := TO_NUMBER (TO_CHAR (SYSDATE, 'YYYYMMDD'));
      ELSE
         v_FECHA_FIN := TO_NUMBER (TO_CHAR (p_FECHA_FIN, 'YYYYMMDD'));
      END IF;


      --HORA
      IF p_HORA_INI = -1
      THEN
         v_HORA_INI := 0000;
      ELSE
         v_HORA_INI := p_HORA_INI;
      END IF;


      IF p_HORA_FIN = -1
      THEN
         v_HORA_FIN := 2359;
      ELSE
         v_HORA_FIN := p_HORA_FIN;
      END IF;

      --MONEDA
      IF p_MONEDA IS NULL
      THEN
         v_MONEDA := NULL;
      ELSE
         v_MONEDA := p_MONEDA;
      END IF;

      --BANCO
      IF p_BANCO IS NULL
      THEN
         v_BANCO := NULL;
      ELSE
         v_BANCO := p_BANCO;
      END IF;

      --CANAL
      IF p_CANAL IS NULL
      THEN
         v_CANAL := NULL;
      ELSE
         v_CANAL := p_CANAL;
      END IF;


      IF v_MONEDA = PKG_PAB_CONSTANTES.V_STR_DVI_CLP
      THEN
         BEGIN                                                       --DETALLE
            OPEN p_DETLL FOR
                 SELECT   SH.COD_BIC_BCO AS BANCO,
                          PKG_PAB_UTILITY.FN_PAB_OBT_DSC_BCO (SH.COD_BIC_BCO)
                             AS NOMBRE_BANCO,
                          DECODE (
                             SH.COD_CNL_SAL,
                             PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_LBTR,
                             PKG_PAB_UTILITY.FN_PAB_OBT_DSC_BCO (
                                PKG_PAB_CONSTANTES.V_BIC_BCCH
                             ),
                             PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_CMN,
                             PKG_PAB_UTILITY.FN_PAB_OBT_DSC_BCO (
                                PKG_PAB_CONSTANTES.V_BIC_COMBANC
                             )
                          )
                             AS CANAL_SALIDA,
                          SH.COD_DVI AS MONEDA,
                          TO_DATE (TO_CHAR (SH.FEC_REG_SDO), 'YYYY-MM-DD')
                             AS FECHA,
                          '' AS SALDO_APER,
                          SUM (SH.IMP_NET_ING) AS INGRESOS,
                          SUM (SH.IMP_NET_EGR) AS EGRESOS,
                          SUM (SH.IMP_NET_ING) - SUM (SH.IMP_NET_EGR) SALDO_FIN,
                          MIN (SH.IMP_NET_MIN) AS NETO_MIN,
                          MAX (SH.IMP_NET_MAX) AS NETO_MAX,
                          NVL (
                             (  SELECT   MIN (SH1.IMP_NET_ING)
                                  FROM   PABS_DT_RGTRO_SALDO_HTRCO SH1
                                 WHERE   SH1.COD_DVI =
                                            NVL (v_MONEDA, SH1.COD_DVI)
                                         AND SH1.COD_CNL_SAL =
                                               NVL (v_CANAL, SH1.COD_CNL_SAL)
                                         AND SH1.COD_BIC_BCO =
                                               NVL (v_BANCO, SH1.COD_BIC_BCO)
                                         AND SH1.FEC_REG_SDO BETWEEN v_FECHA_INI
                                                                 AND  v_FECHA_FIN
                                         AND SH1.HOR_REG_SDO BETWEEN v_HORA_INI
                                                                 AND  v_HORA_FIN
                                         AND SH1.COD_BIC_BCO <>
                                               PKG_PAB_CONSTANTES.V_BIC_BCCH
                                         AND SH1.COD_BIC_BCO = SH.COD_BIC_BCO
                                         AND SH1.IMP_NET_ING <> 0
                              GROUP BY   SH.COD_BIC_BCO),
                             0
                          )
                             AS MIN_ING,
                          MAX (SH.IMP_MAX_ING) AS MAX_ING,
                          NVL (
                             (  SELECT   MIN (SH2.IMP_NET_EGR)
                                  FROM   PABS_DT_RGTRO_SALDO_HTRCO SH2
                                 WHERE   SH2.COD_DVI =
                                            NVL (v_MONEDA, SH2.COD_DVI)
                                         AND SH2.COD_CNL_SAL =
                                               NVL (v_CANAL, SH2.COD_CNL_SAL)
                                         AND SH2.COD_BIC_BCO =
                                               NVL (v_BANCO, SH2.COD_BIC_BCO)
                                         AND SH2.FEC_REG_SDO BETWEEN v_FECHA_INI
                                                                 AND  v_FECHA_FIN
                                         AND SH2.HOR_REG_SDO BETWEEN v_HORA_INI
                                                                 AND  v_HORA_FIN
                                         AND SH2.COD_BIC_BCO <>
                                               PKG_PAB_CONSTANTES.V_BIC_BCCH
                                         AND SH2.COD_BIC_BCO = SH.COD_BIC_BCO
                                         AND SH2.IMP_NET_EGR <> 0
                              GROUP BY   SH.COD_BIC_BCO),
                             0
                          )
                             MIN_EGR,
                          -- SUM (SH.IMP_MAX_EGR) AS MAX_EGR
                          MAX (SH.IMP_MAX_EGR) AS MAX_EGR
                   FROM   PABS_DT_RGTRO_SALDO_HTRCO SH
                  WHERE       SH.COD_DVI = NVL (v_MONEDA, SH.COD_DVI)
                          AND SH.COD_CNL_SAL = NVL (v_CANAL, SH.COD_CNL_SAL)
                          AND SH.COD_BIC_BCO = NVL (v_BANCO, SH.COD_BIC_BCO)
                          AND SH.FEC_REG_SDO BETWEEN v_FECHA_INI
                                                 AND  v_FECHA_FIN
                          AND SH.HOR_REG_SDO BETWEEN v_HORA_INI AND v_HORA_FIN
                          AND SH.COD_BIC_BCO <> PKG_PAB_CONSTANTES.V_BIC_BCCH
                          AND (SH.IMP_NET_ING <> 0 OR SH.IMP_NET_EGR <> 0)
               GROUP BY   SH.COD_BIC_BCO,
                          SH.COD_DVI,
                          SH.FEC_REG_SDO,
                          SH.COD_CNL_SAL
               ORDER BY   SH.FEC_REG_SDO;        --SH.COD_DVI, SH.COD_BIC_BCO;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               err_code := SQLCODE;
               err_msg :=
                  SUBSTR (SQLERRM, 1, 300) || 'No se puedo encontrar Datos';
               RAISE;
            WHEN OTHERS
            THEN
               err_code := SQLCODE;
               err_msg := SUBSTR (SQLERRM, 1, 300);
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
               p_s_mensaje := err_code || '-' || err_msg;
               RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
         END;
      ELSE
         BEGIN                                                       --DETALLE
            OPEN p_DETLL FOR
                 SELECT   SH.COD_BIC_BCO AS BANCO,
                          PKG_PAB_UTILITY.FN_PAB_OBT_DSC_BCO (SH.COD_BIC_BCO)
                             AS NOMBRE_BANCO,
                          SH.COD_DVI AS MONEDA,
                          TO_DATE (TO_CHAR (SH.FEC_REG_SDO), 'YYYY-MM-DD')
                             AS FECHA,
                          MAX (SH.IMP_SDO_APR) AS SALDO_APER,
                          SUM (SH.IMP_NET_ING) AS INGRESOS,
                          SUM (SH.IMP_NET_EGR) AS EGRESOS,
                            MAX (SH.IMP_SDO_APR)
                          + SUM (SH.IMP_NET_ING)
                          - SUM (SH.IMP_NET_EGR)
                             AS SALDO_FIN,
                          SUM (SH.IMP_SDO_RAL)
                          * (PKG_PAB_LIQUIDEZ.FN_PAB_BUS_CONVERSION (
                                v_FECHA,
                                'USD',
                                SH.COD_DVI
                             ))
                             AS EQUIVALENTE_USD, -- variable a buscar en CHI_CON
                          SUM (SH.IMP_NET_MIN) AS NETO_MIN,
                          SUM (SH.IMP_NET_MAX) AS NETO_MAX,
                          SUM (SH.IMP_MIN_ING) AS MIN_ING,
                          SUM (SH.IMP_MAX_ING) AS MAX_ING,
                          SUM (SH.IMP_MIN_EGR) AS MIN_EGR,
                          SUM (SH.IMP_MAX_EGR) AS MAX_EGR
                   FROM   PABS_DT_RGTRO_SALDO_HTRCO SH
                  WHERE       SH.COD_DVI = NVL (v_MONEDA, SH.COD_DVI)
                          AND SH.COD_CNL_SAL = NVL (v_CANAL, SH.COD_CNL_SAL)
                          AND SH.COD_BIC_BCO = NVL (v_BANCO, SH.COD_BIC_BCO)
                          AND SH.FEC_REG_SDO BETWEEN v_FECHA_INI
                                                 AND  v_FECHA_FIN
                          AND SH.HOR_REG_SDO BETWEEN v_HORA_INI AND v_HORA_FIN
                          AND SH.COD_CNL_SAL =
                                PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_SWF
               GROUP BY   SH.COD_BIC_BCO, SH.COD_DVI, SH.FEC_REG_SDO
               ORDER BY   SH.FEC_REG_SDO;                        --SH.COD_DVI;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               err_code := SQLCODE;
               err_msg :=
                  SUBSTR (SQLERRM, 1, 300) || 'No se puedo encontrar Datos';
               RAISE;
            WHEN OTHERS
            THEN
               err_code := SQLCODE;
               err_msg := SUBSTR (SQLERRM, 1, 300);
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
               p_s_mensaje := err_code || '-' || err_msg;
               RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
         END;
      END IF;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_CON_DETLL_LIQDZ;

   --***********************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_BUS_SALDO
   -- Objetivo: Funcion que consulta el Saldo inicial o final segun moneda y banco
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas: PABS_DT_RGTRO_SALDO_HTRCO
   -- Fecha: 12-02-2019
   -- Autor: CAH
   -- Input:
   --      p_FLG_SLD   -> Flag de saldo
   --      p_MONEDA    -> Moneda
   --      p_BANCO     -> Banco a consultar
   --      p_FEC_INI   -> Fecha a cosultar
   --      p_FEC_FIN   -> Fecha a cosultar
   --      p_HORA_INI  -> Hora a consultar
   --      p_HORA_FIN  I-> Hora a consultar
   -- N/A
   -- Output:
   -- N/A.
   -- Input/Output:
   -- N/A.
   -- Retorno:
   --      v_SALDO -> Saldo de la moneda consultada.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   --***********************************************************************************************
   FUNCTION FN_PAB_BUS_SALDO (p_FLG_SLD    IN NUMBER,
                              p_MONEDA     IN CHAR,
                              p_BANCO      IN CHAR,
                              p_FEC_INI    IN NUMBER,
                              p_FEC_FIN    IN NUMBER,
                              p_HORA_INI   IN NUMBER,
                              p_HORA_FIN   IN NUMBER)
      RETURN NUMBER
   IS
      V_NOM_SP   VARCHAR2 (30) := 'FN_PAB_BUS_SALDO';
      v_SALDO    NUMBER;
   BEGIN
        SELECT   CASE
                    WHEN (P_FLG_SLD = 0)
                    THEN
                       SUM (MAX (SH.IMP_SDO_APR))
                    ELSE
                       SUM(  MAX (SH.IMP_SDO_APR)
                           + SUM (SH.IMP_NET_ING)
                           - SUM (SH.IMP_NET_EGR))
                 END
          INTO   v_SALDO
          FROM   PABS_DT_RGTRO_SALDO_HTRCO SH
         WHERE       SH.COD_DVI = NVL (p_MONEDA, SH.COD_DVI)
                 --AND SH.COD_CNL_SAL = NVL (null, SH.COD_CNL_SAL)
                 AND SH.COD_BIC_BCO = NVL (p_BANCO, SH.COD_BIC_BCO)
                 AND SH.FEC_REG_SDO BETWEEN p_FEC_INI AND p_FEC_FIN
                 AND SH.HOR_REG_SDO BETWEEN p_HORA_INI AND p_HORA_FIN
                 AND SH.COD_CNL_SAL = PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_SWF
      GROUP BY   SH.COD_BIC_BCO, SH.COD_DVI, SH.FEC_REG_SDO;

      --REVISAR ESTO !!!!!!
      --                    SELECT  SUM(IMP_SDO_RAL)
      --                    FROM   PABS_DT_RGTRO_SALDO_HTRCO SH
      --                    WHERE COD_DVI ='USD'
      --                    AND FEC_REG_SDO BETWEEN 20190509 AND 20190509
      --                    AND SH.HOR_REG_SDO IN (SELECT MAX (SH.HOR_REG_SDO)
      --                    FROM PABS_DT_RGTRO_SALDO_HTRCO SH
      --                    WHERE COD_DVI ='USD' AND  FEC_REG_SDO BETWEEN 20190509 AND 20190509
      --                    AND SH.HOR_REG_SDO BETWEEN 0000 AND 2359)

      RETURN v_SALDO;
   --
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         v_SALDO := 0;
         RETURN v_SALDO;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END FN_PAB_BUS_SALDO;


   --***********************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_BUS_ING_MIN
   -- Objetivo: Funcion que consulta el minimo ingreso  de una moneda
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas: PABS_DT_RGTRO_SALDO_HTRCO
   -- Fecha: 12-02-2019
   -- Autor: CAH
   -- Input:
   --      p_FLG_SLD   -> Flag de saldo
   --      p_MONEDA    -> Moneda
   --      p_BANCO     -> Banco a consultar
   --      p_FEC_INI   -> Fecha a cosultar
   --      p_FEC_FIN   -> Fecha a cosultar
   --      p_HORA_INI  -> Hora a consultar
   --      p_HORA_FIN  I-> Hora a consultar
   -- N/A
   -- Output:
   -- N/A.
   -- Input/Output:
   -- N/A.
   -- Retorno:
   --      v_SALDO -> Saldo de la moneda consultada.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   --***********************************************************************************************
   FUNCTION FN_PAB_BUS_ING_MIN (p_MONEDA     IN CHAR,
                                p_CANAL      IN CHAR,
                                p_FEC_INI    IN NUMBER,
                                p_FEC_FIN    IN NUMBER,
                                p_HORA_INI   IN NUMBER,
                                p_HORA_FIN   IN NUMBER)
      RETURN NUMBER
   IS
      V_NOM_SP    VARCHAR2 (30) := 'FN_PAB_BUS_ING_MIN';
      v_ING_MIN   NUMBER;
      v_CANAL     CHAR (10);                       --PABS_DT_RGTRO_SALDO_HTRCO
   BEGIN
        --    IF p_MONEDA = 'CLP' THEN
        --
        --    v_CANAL:='LBTR';
        --    ELSE
        --     v_CANAL:='SWF';
        --    END IF;

        SELECT   MIN (SH.IMP_MIN_ING)
          INTO   v_ING_MIN
          FROM   PABS_DT_RGTRO_SALDO_HTRCO SH
         WHERE       SH.COD_DVI = NVL (p_MONEDA, SH.COD_DVI)
                 --AND SH.COD_BIC_BCO = NVL (NULL, SH.COD_BIC_BCO)
                 AND SH.FEC_REG_SDO BETWEEN p_FEC_INI AND p_FEC_FIN
                 AND SH.HOR_REG_SDO BETWEEN p_HORA_INI AND p_HORA_FIN
                 AND SH.IMP_MIN_ING NOT IN (0)
                 AND SH.COD_CNL_SAL = p_CANAL --PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_SWF
      ORDER BY   SH.IMP_MIN_ING DESC;

      IF v_ING_MIN IS NULL
      THEN
         v_ING_MIN := 0;
      END IF;

      RETURN v_ING_MIN;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         v_ING_MIN := 0;
         RETURN v_ING_MIN;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END FN_PAB_BUS_ING_MIN;

   --***********************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_BUS_EGR_MIN
   -- Objetivo: Funcion que consulta el minimo EGRESO  de una moneda
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas: PABS_DT_RGTRO_SALDO_HTRCO
   -- Fecha: 12-02-2019
   -- Autor: CAH
   -- Input:
   --      p_FLG_SLD   -> Flag de saldo
   --      p_MONEDA    -> Moneda
   --      p_BANCO     -> Banco a consultar
   --      p_FEC_INI   -> Fecha a cosultar
   --      p_FEC_FIN   -> Fecha a cosultar
   --      p_HORA_INI  -> Hora a consultar
   --      p_HORA_FIN  I-> Hora a consultar
   -- N/A
   -- Output:
   -- N/A.
   -- Input/Output:
   -- N/A.
   -- Retorno:
   --      v_SALDO -> Saldo de la moneda consultada.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   --***********************************************************************************************
   FUNCTION FN_PAB_BUS_EGR_MIN (p_MONEDA     IN CHAR,
                                p_CANAL      IN CHAR,
                                p_FEC_INI    IN NUMBER,
                                p_FEC_FIN    IN NUMBER,
                                p_HORA_INI   IN NUMBER,
                                p_HORA_FIN   IN NUMBER)
      RETURN NUMBER
   IS
      V_NOM_SP    VARCHAR2 (30) := 'FN_PAB_BUS_EGR_MIN';
      v_EGR_MIN   NUMBER;
      v_CANAL     CHAR (10);                       --PABS_DT_RGTRO_SALDO_HTRCO
   BEGIN
        --    IF p_MONEDA = 'CLP' THEN
        --
        --    v_CANAL:='LBTR';
        --    ELSE
        --     v_CANAL:='SWF';
        --    END IF;

        SELECT   MIN (SH.IMP_MIN_EGR)
          INTO   v_EGR_MIN
          FROM   PABS_DT_RGTRO_SALDO_HTRCO SH
         WHERE       SH.COD_DVI = NVL (p_MONEDA, SH.COD_DVI)
                 --AND SH.COD_BIC_BCO = NVL (NULL, SH.COD_BIC_BCO)
                 AND SH.FEC_REG_SDO BETWEEN p_FEC_INI AND p_FEC_FIN
                 AND SH.HOR_REG_SDO BETWEEN p_HORA_INI AND p_HORA_FIN
                 AND SH.IMP_MIN_EGR NOT IN (0)
                 AND SH.COD_CNL_SAL = p_CANAL --PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_SWF
      ORDER BY   SH.IMP_MIN_EGR DESC;

      IF v_EGR_MIN IS NULL
      THEN
         v_EGR_MIN := 0;
      END IF;

      RETURN v_EGR_MIN;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         v_EGR_MIN := 0;
         RETURN v_EGR_MIN;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END FN_PAB_BUS_EGR_MIN;

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_CON_LIMTE_MONDA_ITDIA
   -- Objetivo: Procedimiento que consulta limites intradia de cada moneda
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas:  PABS_DT_MONDA_ALMOT
   -- Fecha: 07/08/2018
   -- Autor: Santander CAH
   -- Input:
   -- Output:
   -- p_CURSOR -> Cursor con los registros de los elementos
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_CON_LIMTE_MONDA_ITDIA (p_CURSOR   OUT SYS_REFCURSOR,
                                           p_ERROR    OUT NUMBER)
   IS
      V_NOM_SP   VARCHAR2 (30) := 'SP_PAB_CON_LIMTE_MONDA_ITDIA';
   BEGIN
      OPEN p_CURSOR FOR
         SELECT   COD_DVI AS MONEDA,
                  IMP_LMT_MAX AS LIMITE_MAX,
                  IMP_LMT_MIN AS LIMITE_MIN,
                  POR_ALR_MAX AS PORCENTAJE_MAX,
                  POR_ALR_MIN AS PORCENTAJE_MIN
           FROM   PABS_DT_MONDA_ALMOT;

      P_ERROR := PKG_PAB_CONSTANTES.V_OK;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := 'No se han encontrado registos de moneda:';
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;

         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_CON_LIMTE_MONDA_ITDIA;

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_ACT_LIMTE_MONDA_ITDIA
   -- Objetivo: Procedimiento que actualiza limites de una moneda
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas:  PABS_DT_MONDA_ALMOT
   -- Fecha: 07/08/2018
   -- Autor: Santander CAH
   -- Input:
   --        p_MONEDA      Moneda a actualizar
   --        p_LIMITE_MAX  Monto maximo
   --        p_LIMITE_MIN  Monto minimo
   --        p_PORCEN_MAX  Porcentaje maximo a actualizar
   --        p_PORCEN_MIN  Porcentaje minimo a actualizar
   --        p_USUARIO     Usuario que modifica.
   -- Output:
   -- p_CURSOR -> Cursor con los registros de los elementos
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_ACT_LIMTE_MONDA_ITDIA (p_MONEDA       IN     CHAR,
                                           p_LIMITE_MAX   IN     NUMBER,
                                           p_LIMITE_MIN   IN     NUMBER,
                                           p_PORCEN_MAX   IN     NUMBER,
                                           p_PORCEN_MIN   IN     NUMBER,
                                           p_USUARIO      IN     CHAR,
                                           p_ERROR           OUT NUMBER)
   IS
      V_NOM_SP   VARCHAR2 (30) := 'SP_PAB_CON_LIMTE_MONDA_ITDIA';
   BEGIN
      --Tabla de limites (moneda )
      UPDATE   PABS_DT_MONDA_ALMOT
         SET   IMP_LMT_MAX = p_LIMITE_MAX,
               IMP_LMT_MIN = p_LIMITE_MIN,
               POR_ALR_MAX = p_PORCEN_MAX,
               POR_ALR_MIN = p_PORCEN_MIN,
               FEC_ULT_MOD = SYSDATE,
               COD_USR_ULT_MOD = p_USUARIO
       WHERE   COD_DVI = p_MONEDA;

      P_ERROR := PKG_PAB_CONSTANTES.V_OK;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := 'No se han encontrado registos de moneda:';
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;

         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_ACT_LIMTE_MONDA_ITDIA;

   --***********************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_BUS_PAIS
   -- Objetivo: Consulta el pais de una operacion, segun flag de ingreso
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN
   --                TGEN_0112 , TCDTBAI
   -- Fecha: 20-09-2018
   -- Autor: CAH
   -- Input:
   --      p_FECHA     -> Fecha a cosultar
   --      p_HORA      -> Hora a consultar
   --      p_MONEDA    -> MOneda
   -- N/A
   -- Output:
   -- N/A.
   -- Input/Output:
   -- N/A.
   -- Retorno:
   --      v_MONTO_HORA -> monto de la moneda consultada.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   --***********************************************************************************************
   FUNCTION FN_PAB_BUS_PAIS (p_FOLIO   IN NUMBER,
                             p_FECHA   IN DATE,
                             p_FLAG    IN NUMBER)
      RETURN VARCHAR2
   IS
      V_NOM_SP   VARCHAR2 (30) := 'FN_PAB_BUS_PAIS';
      v_PAIS     VARCHAR2 (40);
   BEGIN
      IF p_FLAG = 0
      THEN
         SELECT   TRIM (PAIS.TCMPAIS)
           INTO   v_PAIS
           FROM   PABS_DT_DETLL_OPRCN OPE, TGEN_0112 PAIS, TCDTBAI TBAI
          WHERE       TBAI.TGCDSWSA = OPE.COD_BCO_DTN
                  AND PAIS.CODEPAIS = TBAI.COD_PAIS
                  AND OPE.NUM_FOL_OPE = p_FOLIO
                  AND OPE.FEC_ISR_OPE = p_FECHA;
      ELSE
         SELECT   TRIM (PAIS.TCMPAIS)
           INTO   v_PAIS
           FROM   PABS_DT_DETLL_OPRCN OPE, TGEN_0112 PAIS, TCDTBAI TBAI
          WHERE       TBAI.TGCDSWSA = OPE.COD_BCO_ORG
                  AND PAIS.CODEPAIS = TBAI.COD_PAIS
                  AND OPE.NUM_FOL_OPE = p_FOLIO
                  AND OPE.FEC_ISR_OPE = p_FECHA;
      END IF;

      RETURN v_PAIS;
   --
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END FN_PAB_BUS_PAIS;

   --************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_INTFZ_RISGO
   -- Objetivo: Procedimiento que al cierre del sistema se crea la interfaz de Riesgo con Transacciones
   --            Swift del dia.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN OPE,
   --        PABS_DT_OPRCN_INFCN_OPCON ODC
   --        ,TCDTBAI TBAI
   --        ,TGEN_0112 PAIS
   --
   -- Fecha: 21-08-2018 CAH
   -- Autor: Santander
   -- Input: p_FEC_PCS -> Fecha Proceso.
   -- Output: p_CURSOR -> Con datos para la creacion de la interfaz
   --         p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: 21-08-2018, Creacion Procedimiento.
   --***********************************************************************************************
   PROCEDURE SP_PAB_INTFZ_RISGO (p_FEC_PCS   IN     VARCHAR2,
                                 p_CURSOR       OUT SYS_REFCURSOR,
                                 p_ERROR        OUT NUMBER)
   IS
      v_NOM_SP        VARCHAR2 (30) := 'SP_PAB_INTFZ_RISGO';
      v_FEC_PCS       DATE := TO_DATE (p_FEC_PCS, 'DD-MM-YYYY');
      p_COD_ERR_BIB   CHAR (6) := '0';
      p_COD_TPO_PSC   CHAR (3) := 'PCT';
      p_GLS_DET_PCS   VARCHAR2 (300);
      p_FEC_ICO_PCS   VARCHAR2 (30);
      p_NUM_TCK_PCS   NUMBER;
      v_TIPO_CARGO    CHAR (3) := 'OUR';                 --Siempre mismo valor
   BEGIN
      -- INICIALIZACION DE VARIABLES Y PARAMETROS
      p_ERROR := PKG_PAB_CONSTANTES.v_OK;


      --Inicio creacion Interfaz Riesgo
      p_GLS_DET_PCS := 'Iniciando Proceso de Interfaz de Riesgo';
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_CAB_PROCESO (p_COD_TPO_PSC,
                                                   p_FEC_ICO_PCS,
                                                   p_NUM_TCK_PCS,
                                                   p_ERROR);
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);



      OPEN p_CURSOR FOR
         SELECT   PKG_PAB_LIQUIDEZ.FN_PAB_BUS_PAIS (OPE.NUM_FOL_OPE,
                                                    OPE.FEC_ISR_OPE,
                                                    OPE.FLG_EGR_ING)
                     AS PAIS,
                  OPE.COD_MT_SWF AS MENSAJE_SWF,
                  OPE.FLG_EGR_ING AS TIPO_MOVIMIENTO,
                  TRIM (OPE.NUM_OPE_SIS_ENT) AS COD_OPERACION_ORIGEN,
                  TO_CHAR (OPE.FEC_VTA, 'DD-MM-YYYY') AS FECHA_PAGO,
                  NVL (TRIM (OPE.NUM_REF_SWF), '-') AS REFERENCIA,
                  TRIM (OPE.COD_TPO_OPE_AOS) AS "CODIGO_OPERACIÓN",
                  OPE.COD_DVI AS MONEDA,
                  OPE.IMP_OPE AS MONTO,
                  NVL (ODC.NUM_CTA_ODN, 0) AS CTA_ORDENANTE,
                  OPE.COD_BCO_ORG AS BIC_BANCO_ORDENANTE,
                  TRIM (PKG_PAB_UTILITY.FN_PAB_OBT_DSC_BCO (OPE.COD_BCO_ORG))
                     AS NOMBRE_BANCO_ORDENANTE,
                  NVL (ODC.COD_TPD_ODN, 0) AS DIGITO_DOC_ORDENANTE,
                  NVL (ODC.NUM_DOC_ODN, 0) AS NUM_IDENTIFICADOR_ORDENANTE,
                  NVL (OPE.NUM_CTA_BFC, 0) AS CTA_BENEFICIARIO,
                  OPE.COD_BCO_DTN AS BIC_BANCO_DESTINO,
                  TRIM (PKG_PAB_UTILITY.FN_PAB_OBT_DSC_BCO (OPE.COD_BCO_DTN))
                     AS NOMBRE_BANCO_DESTINO,
                  OPE.NOM_BFC AS NOMBRE_BENEFICIARIO,
                  NVL (OPE.COD_TPD_BFC, 0) AS DIGITO_DOC_BENEFICIARIO,
                  NVL (OPE.NUM_DOC_BFC, 0) AS NUM_IDENTIFICADOR_BENEFICIARIO,
                  v_TIPO_CARGO AS DETALLE_TIPO_CARGO,
                  DECODE (OPE.FLG_EGR_ING,
                          0, TRIM (OPE.COD_SIS_SAL),
                          TRIM (OPE.COD_SIS_ENT))
                     AS SISTEMA_OPERACION,
                  NVL (TRIM (ODC.OBS_OPC_SWF), 0) AS MENSAJE_ORGINAL,
                  OPE.FEC_ACT_OPE AS FECHA_HORA_PAGO,
                  DECODE (OPE.COD_BCO_ORG,
                          PKG_PAB_CONSTANTES.V_BIC_COMBANC, 1,
                          0)
                     AS CAMARA_COMPENSACION,
                  OPE.FEC_ISR_OPE AS FECHA_RECEPCION
           FROM   PABS_DT_DETLL_OPRCN OPE,
                  PABS_DT_OPRCN_INFCN_OPCON ODC,
                  TCDTBAI TBAI,
                  TGEN_0112 PAIS
          WHERE       TBAI.TGCDSWSA = OPE.COD_BCO_ORG
                  AND OPE.NUM_FOL_OPE = ODC.NUM_FOL_OPE
                  AND OPE.FEC_ISR_OPE = ODC.FEC_ISR_OPE
                  AND OPE.FEC_ISR_OPE BETWEEN v_DIA_INI AND v_DIA_FIN
                  AND PAIS.CODEPAIS = TBAI.COD_PAIS
                  AND OPE.FEC_VTA = v_FEC_PCS        --fecha proceso v_FEC_PCS
                  AND OPE.COD_EST_AOS IN
                           (PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAG,
                            PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEABN,
                            PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAGCFI,
                            PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDEV);


      p_ERROR := PKG_PAB_CONSTANTES.v_OK;


      --Fin Proceso interfaz
      p_GLS_DET_PCS := 'Fin proceso de creacion Interfaz Riesgo';
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);
      PKG_PAB_TRACKING.Sp_PAB_UPD_BIT_CAB_PROCESO (p_FEC_ICO_PCS,
                                                   p_COD_TPO_PSC,
                                                   p_NUM_TCK_PCS,
                                                   p_ERROR,
                                                   p_ERROR);
   EXCEPTION
      WHEN DUP_VAL_ON_INDEX
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_INTFZ_RISGO;

   --************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_INFRM_CIRRE_MONDA_PRDTO
   -- Objetivo: Procedimiento que crea Informe de cierre del dia por moneda y producto
   --
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN OPE,
   --                    PABS_DT_TIPO_OPRCN_ALMOT
   --                    PABS_DT_TIPO_SUB_PRDTO
   --
   -- Fecha: 24-08-2018 CAH
   -- Autor: Santander
   -- Input: p_FEC_PCS -> Fecha Proceso.
   -- Output: p_CURSOR -> Con datos para la creacion de la interfaz
   --         p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: 21-08-2018, Creacion Procedimiento.
   --***********************************************************************************************
   PROCEDURE SP_PAB_INFRM_CIRRE_MONDA_PRDTO (
      p_FEC_PCS   IN     VARCHAR2,
      p_CURSOR       OUT SYS_REFCURSOR,
      p_ERROR        OUT NUMBER
   )
   IS
      v_NOM_SP        VARCHAR2 (30) := 'SP_PAB_INFRM_CIRRE_MONDA_PRDTO';
      v_FEC_PCS       DATE := TO_DATE (p_FEC_PCS, 'DD-MM-YYYY');
      p_COD_ERR_BIB   CHAR (6) := '0';
      p_COD_TPO_PSC   CHAR (3) := 'PCT';
      p_GLS_DET_PCS   VARCHAR2 (300);
      p_FEC_ICO_PCS   VARCHAR2 (30);
      p_NUM_TCK_PCS   NUMBER;
   BEGIN
      -- INICIALIZACION DE VARIABLES Y PARAMETROS
      p_ERROR := PKG_PAB_CONSTANTES.v_OK;

      --Inicio creacion Informe de cierre del dia por moneda y producto
      p_GLS_DET_PCS :=
         'Iniciando Proceso Informe de cierre del dia por moneda y producto';
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_CAB_PROCESO (p_COD_TPO_PSC,
                                                   p_FEC_ICO_PCS,
                                                   p_NUM_TCK_PCS,
                                                   p_ERROR);
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);

      OPEN p_CURSOR FOR
           SELECT   OPE.COD_DVI AS MONEDA,
                    OPE.COD_TPO_OPE_AOS AS PRODUCTO,
                    TOP.DSC_TPO_OPE_AOS AS DESCP_PRODUCTO,
                    SP.DSC_TPO_SPO AS SUB_PRODUCTO,
                    NVL (SUM (DECODE (OPE.FLG_EGR_ING, 1, OPE.IMP_OPE)), 0)
                       AS INGRESOS,
                    NVL (SUM (DECODE (OPE.FLG_EGR_ING, 0, OPE.IMP_OPE)), 0)
                       AS EGRESOS,
                    NVL (SUM (DECODE (OPE.FLG_EGR_ING, 1, OPE.IMP_OPE)), 0)
                    - NVL (SUM (DECODE (OPE.FLG_EGR_ING, 0, OPE.IMP_OPE)), 0)
                       AS NETO
             FROM   PABS_DT_DETLL_OPRCN OPE,
                    PABS_DT_TIPO_OPRCN_ALMOT TOP,
                    PABS_DT_TIPO_SUB_PRDTO SP
            WHERE   OPE.COD_TPO_OPE_AOS = TOP.COD_TPO_OPE_AOS
                    AND TOP.COD_TPO_SPO = SP.COD_TPO_SPO
                    AND OPE.COD_EST_AOS IN
                             (PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAG,
                              PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEABN,
                              PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAGCFI,
                              PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDEV)
                    AND OPE.FEC_VTA = v_FEC_PCS         -- operaciones del dia
         GROUP BY   OPE.COD_DVI,
                    OPE.COD_TPO_OPE_AOS,
                    TOP.DSC_TPO_OPE_AOS,
                    SP.DSC_TPO_SPO
         ORDER BY   OPE.COD_DVI ASC;

      --15 Abonada
      --18 Devuelta
      --12 Pagada
      --21 Pagada Contingencia

      p_ERROR := PKG_PAB_CONSTANTES.v_OK;


      --Fin Proceso Informe de cierre del dia por moneda y producto
      p_GLS_DET_PCS :=
         'Fin proceso creacion Informe de cierre del dia por moneda y producto';
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);
      PKG_PAB_TRACKING.Sp_PAB_UPD_BIT_CAB_PROCESO (p_FEC_ICO_PCS,
                                                   p_COD_TPO_PSC,
                                                   p_NUM_TCK_PCS,
                                                   p_ERROR,
                                                   p_ERROR);
   EXCEPTION
      WHEN DUP_VAL_ON_INDEX
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_INFRM_CIRRE_MONDA_PRDTO;

   --************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_INFRM_CIRRE_BANCO_CRPSL
   -- Objetivo: Procedimiento que crea Informe Cierre de Bancos Corresponsales MX
   --            Swift del dia.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX
   --
   -- Fecha: 24-08-2018 CAH
   -- Autor: Santander
   -- Input: p_FEC_PCS -> Fecha Proceso.
   -- Output: p_CURSOR -> Con datos para la creacion de la interfaz
   --         p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: 21-08-2018, Creacion Procedimiento.
   --***********************************************************************************************
   PROCEDURE SP_PAB_INFRM_CIRRE_BANCO_CRPSL (
      p_FEC_PCS   IN     VARCHAR2,
      p_CURSOR       OUT SYS_REFCURSOR,
      p_ERROR        OUT NUMBER
   )
   IS
      v_NOM_SP        VARCHAR2 (30) := 'SP_PAB_INFRM_CIRRE_BANCO_CRPSL';
      v_FEC_PCS       DATE := TO_DATE (p_FEC_PCS, 'DD-MM-YYYY');
      p_COD_ERR_BIB   CHAR (6) := '0';
      p_COD_TPO_PSC   CHAR (3) := 'PCT';
      p_GLS_DET_PCS   VARCHAR2 (300);
      p_FEC_ICO_PCS   VARCHAR2 (30);
      p_NUM_TCK_PCS   NUMBER;
   BEGIN
      -- INICIALIZACION DE VARIABLES Y PARAMETROS
      p_ERROR := PKG_PAB_CONSTANTES.v_OK;

      --Inicio creacion Informe Cierre de Bancos Corresponsales MX
      p_GLS_DET_PCS :=
         'Iniciando Proceso Informe Cierre de Bancos Corresponsales MX';
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_CAB_PROCESO (p_COD_TPO_PSC,
                                                   p_FEC_ICO_PCS,
                                                   p_NUM_TCK_PCS,
                                                   p_ERROR);
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);

      OPEN p_CURSOR FOR
           SELECT   SL.COD_BCO_ORG AS BANCO,
                    SL.COD_DVI AS MONEDA,
                    SUM (SL.IMP_APR_AOS) AS SALDO_INICIAL,
                    SUM (SL.IMP_ING_DIA) AS INGRESO,
                    SUM (SL.IMP_EGR_DIA) AS EGRESOS,
                    SUM (SL.IMP_RAL_AOS) AS SALDO_CIERRE
             FROM   PABS_DT_DETLL_SALDO_CAJA_MN_MX SL
         --WHERE   SL.FEC_ING_CTL = v_FEC_PCS
         GROUP BY   SL.COD_BCO_ORG, SL.COD_DVI
         ORDER BY   SL.COD_BCO_ORG, SL.COD_DVI;

      p_ERROR := PKG_PAB_CONSTANTES.v_OK;

      --Fin Proceso
      p_GLS_DET_PCS :=
         'Fin proceso de creacion Informe Cierre de Bancos Corresponsales MX';
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);
      PKG_PAB_TRACKING.Sp_PAB_UPD_BIT_CAB_PROCESO (p_FEC_ICO_PCS,
                                                   p_COD_TPO_PSC,
                                                   p_NUM_TCK_PCS,
                                                   p_ERROR,
                                                   p_ERROR);
   EXCEPTION
      WHEN DUP_VAL_ON_INDEX
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_INFRM_CIRRE_BANCO_CRPSL;

   --************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_INFRM_TRNSC_MN
   -- Objetivo: Procedimiento que crea el informe Resumen de Transferencia MN
   --            Swift del dia.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_BANCO_ALMOT
   --
   -- Fecha: 24-08-2018 CAH
   -- Autor: Santander
   -- Input: p_FEC_PCS -> Fecha Proceso.
   -- Output: p_CURSOR -> Con datos para la creacion de la interfaz
   --         p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: 24-08-2018, Creacion Procedimiento.
   --***********************************************************************************************
   PROCEDURE SP_PAB_INFRM_TRNSC_MN (p_FEC_PCS   IN     VARCHAR2,
                                    p_CURSOR       OUT SYS_REFCURSOR,
                                    p_ERROR        OUT NUMBER)
   IS
      v_NOM_SP        VARCHAR2 (30) := 'SP_PAB_INFRM_TRNSC_MN';
      v_FEC_PCS       DATE := TO_DATE (p_FEC_PCS, 'DD-MM-YYYY');
      p_COD_ERR_BIB   CHAR (6) := '0';
      p_COD_TPO_PSC   CHAR (3) := 'PCT';
      p_GLS_DET_PCS   VARCHAR2 (300);
      p_FEC_ICO_PCS   VARCHAR2 (30);
      p_NUM_TCK_PCS   NUMBER;
   BEGIN
      -- INICIALIZACION DE VARIABLES Y PARAMETROS
      p_ERROR := PKG_PAB_CONSTANTES.v_OK;

      --Inicio creacion informe Resumen de Transferencia MN
      p_GLS_DET_PCS := 'Iniciando Proceso informe Resumen de Transferencia MN';
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_CAB_PROCESO (p_COD_TPO_PSC,
                                                   p_FEC_ICO_PCS,
                                                   p_NUM_TCK_PCS,
                                                   p_ERROR);
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);

      OPEN p_CURSOR FOR
           SELECT   BAN.COD_BIC_BCO AS BANCO,
                    --'LBTR'  AS LBTR,
                    PKG_PAB_LIQUIDEZ.FN_PAB_BUS_CANT_CANAL (
                       PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_LBTR,
                       PKG_PAB_CONSTANTES.V_STR_DVI_CLP,
                       BAN.COD_BIC_BCO,
                       1
                    )
                       AS NUM_ING_LBTR,
                    PKG_PAB_LIQUIDEZ.FN_PAB_BUS_SUMA_CANAL (
                       PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_LBTR,
                       PKG_PAB_CONSTANTES.V_STR_DVI_CLP,
                       BAN.COD_BIC_BCO,
                       1
                    )
                       AS TOT_ING_LBTR,
                    PKG_PAB_LIQUIDEZ.FN_PAB_BUS_CANT_CANAL (
                       PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_LBTR,
                       PKG_PAB_CONSTANTES.V_STR_DVI_CLP,
                       BAN.COD_BIC_BCO,
                       0
                    )
                       AS NUM_EGR_LBTR,
                    PKG_PAB_LIQUIDEZ.FN_PAB_BUS_SUMA_CANAL (
                       PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_LBTR,
                       PKG_PAB_CONSTANTES.V_STR_DVI_CLP,
                       BAN.COD_BIC_BCO,
                       0
                    )
                       AS TOT_EGR_LBTR,
                    PKG_PAB_LIQUIDEZ.FN_PAB_BUS_CANT_CANAL (
                       PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_CMN,
                       PKG_PAB_CONSTANTES.V_STR_DVI_CLP,
                       BAN.COD_BIC_BCO,
                       1
                    )
                       AS NUM_ING_CMB,
                    PKG_PAB_LIQUIDEZ.FN_PAB_BUS_SUMA_CANAL (
                       PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_CMN,
                       PKG_PAB_CONSTANTES.V_STR_DVI_CLP,
                       BAN.COD_BIC_BCO,
                       1
                    )
                       AS TOT_ING_CMB,
                    PKG_PAB_LIQUIDEZ.FN_PAB_BUS_CANT_CANAL (
                       PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_CMN,
                       PKG_PAB_CONSTANTES.V_STR_DVI_CLP,
                       BAN.COD_BIC_BCO,
                       0
                    )
                       AS NUM_EGR_CMB,
                    PKG_PAB_LIQUIDEZ.FN_PAB_BUS_SUMA_CANAL (
                       PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_CMN,
                       PKG_PAB_CONSTANTES.V_STR_DVI_CLP,
                       BAN.COD_BIC_BCO,
                       0
                    )
                       AS TOT_EGR_CMB
             FROM   PABS_DT_BANCO_ALMOT BAN
            WHERE   BAN.COD_SIS_SAL IN
                          (PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_LBTR,
                           PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_CMN)
         GROUP BY   BAN.COD_BIC_BCO
         ORDER BY   BAN.COD_BIC_BCO;

      p_ERROR := PKG_PAB_CONSTANTES.v_OK;

      --Fin Proceso informe
      p_GLS_DET_PCS :=
         'Fin proceso de creacion informe Resumen de Transferencia MN';
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);
      PKG_PAB_TRACKING.Sp_PAB_UPD_BIT_CAB_PROCESO (p_FEC_ICO_PCS,
                                                   p_COD_TPO_PSC,
                                                   p_NUM_TCK_PCS,
                                                   p_ERROR,
                                                   p_ERROR);
   EXCEPTION
      WHEN DUP_VAL_ON_INDEX
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_INFRM_TRNSC_MN;


   --***********************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_BUS_CANT_CANAL
   -- Objetivo: Funcion que retorna la cantidad  de operaciones por canal y flujo(egreso o  ingreso)
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN
   -- Fecha: 05-10-2018
   -- Autor: CAH
   -- Input:
   --      p_CANAL     -> Canal a consultar.
   --      p_BANCO     -> Banco a consultar
   --      p_FLAG      -> Flag de ingreso o egreso

   -- N/A
   -- Output:
   -- N/A.
   -- Input/Output:
   -- N/A.
   -- Retorno:
   --      v_MONTO_HORA -> monto de la moneda consultada.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   --***********************************************************************************************
   FUNCTION FN_PAB_BUS_CANT_CANAL (p_CANAL    IN CHAR,
                                   p_MONEDA   IN CHAR,
                                   p_BANCO    IN CHAR,
                                   p_FLAG     IN NUMBER)
      RETURN NUMBER
   IS
      V_NOM_SP   VARCHAR2 (30) := 'FN_PAB_BUS_CANT_CANAL';
      v_CANT     NUMBER;
   BEGIN
      IF p_FLAG = 0
      THEN
         SELECT   COUNT (1)
           INTO   v_CANT
           FROM   PABS_DT_DETLL_OPRCN OPE
          WHERE       OPE.COD_SIS_SAL = p_CANAL
                  AND OPE.COD_BCO_DTN = p_BANCO
                  AND OPE.COD_DVI = p_MONEDA
                  AND OPE.COD_EST_AOS IN
                           (PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAG,
                            PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAGCFI)
                  AND OPE.FEC_VTA = v_FECHA_HOY;
      ELSE
         SELECT   COUNT (1)
           INTO   v_CANT
           FROM   PABS_DT_DETLL_OPRCN OPE
          WHERE       OPE.COD_SIS_ENT = p_CANAL
                  AND OPE.COD_BCO_ORG = p_BANCO
                  AND OPE.COD_DVI = p_MONEDA
                  AND OPE.COD_EST_AOS IN
                           (PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEABN,
                            PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDEV)
                  AND OPE.FEC_VTA = v_FECHA_HOY;
      END IF;

      RETURN v_CANT;
   --
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END FN_PAB_BUS_CANT_CANAL;

   --***********************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_BUS_SUMA_CANAL
   -- Objetivo: Funcion que retorna la suma por canal y flujo(egreso-ingreso)
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN
   -- Fecha: 05-10-2018
   -- Autor: CAH
   -- Input:
   --      p_CANAL     -> Canal a consultar.
   --      p_FLAG      -> Flag de ingreso o egreso
   -- N/A
   -- Output:
   -- N/A.
   -- Input/Output:
   -- N/A.
   -- Retorno:
   --      v_MONTO_HORA -> monto de la moneda consultada.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   --***********************************************************************************************
   FUNCTION FN_PAB_BUS_SUMA_CANAL (p_CANAL    IN CHAR,
                                   p_MONEDA   IN CHAR,
                                   p_BANCO    IN CHAR,
                                   p_FLAG     IN NUMBER)
      RETURN NUMBER
   IS
      V_NOM_SP   VARCHAR2 (30) := 'FN_PAB_BUS_SUMA_CANAL';
      v_SUMA     NUMBER;
   BEGIN
      IF p_FLAG = 0
      THEN
         SELECT   SUM (OPE.IMP_OPE)
           INTO   v_SUMA
           FROM   PABS_DT_DETLL_OPRCN OPE
          WHERE       OPE.COD_SIS_SAL = p_CANAL
                  AND OPE.COD_BCO_DTN = p_BANCO
                  AND OPE.COD_DVI = p_MONEDA
                  AND OPE.COD_EST_AOS IN
                           (PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAG,
                            PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAGCFI)
                  AND OPE.FEC_VTA = v_FECHA_HOY;
      ELSE
         SELECT   SUM (OPE.IMP_OPE)
           INTO   v_SUMA
           FROM   PABS_DT_DETLL_OPRCN OPE
          WHERE       OPE.COD_SIS_ENT = p_CANAL
                  AND OPE.COD_BCO_ORG = p_BANCO
                  AND OPE.COD_DVI = p_MONEDA
                  AND OPE.COD_EST_AOS IN
                           (PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEABN,
                            PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDEV)
                  AND OPE.FEC_VTA = v_FECHA_HOY;
      END IF;

      RETURN v_SUMA;
   --
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END FN_PAB_BUS_SUMA_CANAL;

   --************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_INFRM_TRNSC_MX
   -- Objetivo: Procedimiento que crea informe Resumen de Transferencias MX
   --            Swift del dia.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX
   --                  PABS_DT_CBCRA_CRTLA
   --
   -- Fecha: 24-08-2018 CAH
   -- Autor: Santander
   -- Input: p_FEC_PCS -> Fecha Proceso.
   -- Output: p_CURSOR -> Con datos para la creacion de la interfaz
   --         p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: 24-08-2018, Creacion Procedimiento.
   --***********************************************************************************************
   PROCEDURE SP_PAB_INFRM_TRNSC_MX (p_FEC_PCS   IN     VARCHAR2,
                                    p_CURSOR       OUT SYS_REFCURSOR,
                                    p_ERROR        OUT NUMBER)
   IS
      v_NOM_SP        VARCHAR2 (30) := 'SP_PAB_INFRM_TRNSC_MX';
      v_FEC_PCS       DATE := TO_DATE (p_FEC_PCS, 'DD-MM-YYYY');
      p_COD_ERR_BIB   CHAR (6) := '0';
      p_COD_TPO_PSC   CHAR (3) := 'PCT';
      p_GLS_DET_PCS   VARCHAR2 (300);
      p_FEC_ICO_PCS   VARCHAR2 (30);
      p_NUM_TCK_PCS   NUMBER;
      v_FEC_HOY       CHAR (10) := TO_CHAR (SYSDATE, 'YYYY-MM-DD');
   BEGIN
      p_ERROR := PKG_PAB_CONSTANTES.v_OK;

      --Inicio creacion informe Resumen de Transferencias MX
      p_GLS_DET_PCS :=
         'Iniciando Proceso informe Resumen de Transferencias MX';
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_CAB_PROCESO (p_COD_TPO_PSC,
                                                   p_FEC_ICO_PCS,
                                                   p_NUM_TCK_PCS,
                                                   p_ERROR);

      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);

      OPEN p_CURSOR FOR
         SELECT   BAN.COD_BCO_ORG AS BANCO,
                  --v_FEC_PCS AS FECHA, -- REVISAR SI ES NECESARIA
                  --  BAN.COD_DVI  AS MONEDA,
                  -- v_FEC_HOY AS FECHA,-- SE MODIFICA COLUMNAS POR UN PROBLEMA EN LA JAVA
                  v_FEC_PCS AS MONEDA,
                  BAN.COD_DVI AS FECHA,
                  PKG_PAB_LIQUIDEZ.FN_PAB_BUS_CANT_CANAL (
                     PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_SWF,
                     BAN.COD_DVI,
                     BAN.COD_BCO_ORG,
                     PKG_PAB_CONSTANTES.V_FLG_ING
                  )
                     AS NUM_INGRESOS,
                  PKG_PAB_LIQUIDEZ.FN_PAB_BUS_SUMA_CANAL (
                     PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_SWF,
                     BAN.COD_DVI,
                     BAN.COD_BCO_ORG,
                     PKG_PAB_CONSTANTES.V_FLG_ING
                  )
                     AS MONTO_INGRESOS,
                  PKG_PAB_LIQUIDEZ.FN_PAB_BUS_CANT_CANAL (
                     PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_SWF,
                     BAN.COD_DVI,
                     BAN.COD_BCO_ORG,
                     PKG_PAB_CONSTANTES.V_FLG_EGR
                  )
                     AS NUM_EGRESOS,
                  PKG_PAB_LIQUIDEZ.FN_PAB_BUS_SUMA_CANAL (
                     PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_SWF,
                     BAN.COD_DVI,
                     BAN.COD_BCO_ORG,
                     PKG_PAB_CONSTANTES.V_FLG_EGR
                  )
                     AS MONTO_EGRESOS,
                  PKG_PAB_LIQUIDEZ.FN_PAB_BUS_SUMA_CANAL (
                     PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_SWF,
                     BAN.COD_DVI,
                     BAN.COD_BCO_ORG,
                     PKG_PAB_CONSTANTES.V_FLG_ING
                  )
                  - PKG_PAB_LIQUIDEZ.FN_PAB_BUS_SUMA_CANAL (
                       PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_SWF,
                       BAN.COD_DVI,
                       BAN.COD_BCO_ORG,
                       PKG_PAB_CONSTANTES.V_FLG_EGR
                    )
                     AS SALDO_NETO
           FROM   PABS_DT_DETLL_SALDO_CAJA_MN_MX BAN, PABS_DT_CBCRA_CRTLA SL
          WHERE       BAN.COD_BCO_ORG = SL.COD_BCO_ORG
                  AND BAN.COD_DVI <> PKG_PAB_CONSTANTES.V_STR_DVI_CLP
                  AND BAN.FEC_ING_CTL = SL.FEC_ING_CTL
                  AND SL.NUM_CTA_CTE = BAN.NUM_CTA_CTE
                  AND BAN.COD_DVI = SL.COD_DVI
                  AND BAN.FEC_ING_CTL <= v_FECHA_HOY;            -- v_FEC_PCS;

      p_ERROR := PKG_PAB_CONSTANTES.v_OK;

      --Fin Proceso informe Resumen de Transferencias MX
      p_GLS_DET_PCS :=
         'Fin proceso de creacion informe Resumen de Transferencias MX';
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);
      PKG_PAB_TRACKING.Sp_PAB_UPD_BIT_CAB_PROCESO (p_FEC_ICO_PCS,
                                                   p_COD_TPO_PSC,
                                                   p_NUM_TCK_PCS,
                                                   p_ERROR,
                                                   p_ERROR);
   EXCEPTION
      WHEN DUP_VAL_ON_INDEX
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_INFRM_TRNSC_MX;

   --************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_INFRM_OPRCN_FUERA_HORA
   -- Objetivo: Procedimiento que crea Informe de operaciones cursadas fuera de horario
   --            Swift del dia.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN
   --
   -- Fecha: 24-08-2018 CAH
   -- Autor: Santander
   -- Input: p_FEC_PCS -> Fecha Proceso.
   -- Output: p_CURSOR -> Con datos para la creacion de la interfaz
   --         p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: 24-08-2018, Creacion Procedimiento.
   --***********************************************************************************************
   PROCEDURE SP_PAB_INFRM_OPRCN_FUERA_HORA (p_FEC_PCS   IN     VARCHAR2,
                                            p_CURSOR       OUT SYS_REFCURSOR,
                                            p_ERROR        OUT NUMBER)
   IS
      v_NOM_SP        VARCHAR2 (30) := 'SP_PAB_INFRM_OPRCN_FUERA_HORA';
      v_FEC_PCS       DATE := TO_DATE (p_FEC_PCS, 'DD-MM-YYYY');
      p_COD_ERR_BIB   CHAR (6) := '0';
      p_COD_TPO_PSC   CHAR (3) := 'PCT';
      p_GLS_DET_PCS   VARCHAR2 (300);
      p_FEC_ICO_PCS   VARCHAR2 (30);
      p_NUM_TCK_PCS   NUMBER;
      v_FUERA_HOR_CLP DATE
            := TRUNC (SYSDATE) + INTERVAL '16' HOUR + INTERVAL '30' MINUTE ;
      v_FUERA_HOR_MX DATE
            := TRUNC (SYSDATE) + INTERVAL '17' HOUR + INTERVAL '00' MINUTE ;
   BEGIN
      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
      --Inicio creacion Informe de operaciones cursadas fuera de horario
      p_GLS_DET_PCS :=
         'Iniciando Proceso Informe de operaciones cursadas fuera de horario';
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_CAB_PROCESO (p_COD_TPO_PSC,
                                                   p_FEC_ICO_PCS,
                                                   p_NUM_TCK_PCS,
                                                   p_ERROR);
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);


      OPEN p_CURSOR FOR      --Cantidad ope. despues 16:30 MN Egresos-Ingresos
         SELECT   DECODE (OPE.FLG_EGR_ING,
                          0, OPE.COD_BCO_DTN,
                          OPE.COD_BCO_ORG)
                     AS BANCO,
                  OPE.NUM_FOL_OPE AS OPERACION,
                  OPE.COD_DVI AS MONEDA,
                  DECODE (OPE.FLG_EGR_ING,
                          0, PKG_PAB_CONSTANTES.V_STR_FLG_EGR,
                          PKG_PAB_CONSTANTES.V_STR_FLG_ING)
                     AS FLUJO_EGR_ING,
                  OPE.IMP_OPE AS MONTO,
                  TO_CHAR (OPE.FEC_ACT_OPE, 'HH24:MI:SS') AS HORA
           FROM   PABS_DT_DETLL_OPRCN OPE
          WHERE       OPE.FEC_ACT_OPE > v_FUERA_HOR_CLP
                  AND TRUNC (OPE.FEC_ISR_OPE) = v_FECHA_HOY          --Del dia
                  AND OPE.COD_DVI = PKG_PAB_CONSTANTES.V_STR_DVI_CLP
                  AND OPE.COD_EST_AOS IN
                           (PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAG,
                            PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEABN,
                            PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAGCFI,
                            PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDEV) --(12, 15, 18, 21)
         UNION
         SELECT   DECODE (OP.FLG_EGR_ING, 0, OP.COD_BCO_DTN, OP.COD_BCO_ORG)
                     AS BANCO,
                  OP.NUM_FOL_OPE AS OPERACION,
                  OP.COD_DVI AS MONEDA,
                  DECODE (OP.FLG_EGR_ING,
                          0, PKG_PAB_CONSTANTES.V_STR_FLG_EGR,
                          PKG_PAB_CONSTANTES.V_STR_FLG_ING)
                     AS FLUJO_EGR_ING,
                  OP.IMP_OPE AS MONTO,
                  TO_CHAR (OP.FEC_ACT_OPE, 'HH24:MI:SS') AS HORA
           FROM   PABS_DT_DETLL_OPRCN OP
          WHERE       OP.FEC_ACT_OPE > v_FUERA_HOR_MX
                  AND TRUNC (OP.FEC_ISR_OPE) = v_FECHA_HOY           --Del dia
                  AND OP.COD_DVI <> PKG_PAB_CONSTANTES.V_STR_DVI_CLP
                  AND OP.COD_EST_AOS IN
                           (PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAG,
                            PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEABN,
                            PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAGCFI,
                            PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDEV) --(12, 15, 18, 21)
         ORDER BY   MONEDA, HORA ASC;


      p_ERROR := PKG_PAB_CONSTANTES.v_OK;

      --Fin Proceso Informe de operaciones cursadas fuera de horario
      p_GLS_DET_PCS :=
         'Fin proceso de creacion Informe de operaciones cursadas fuera de horario';
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);
      PKG_PAB_TRACKING.Sp_PAB_UPD_BIT_CAB_PROCESO (p_FEC_ICO_PCS,
                                                   p_COD_TPO_PSC,
                                                   p_NUM_TCK_PCS,
                                                   p_ERROR,
                                                   p_ERROR);
   EXCEPTION
      WHEN DUP_VAL_ON_INDEX
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_INFRM_OPRCN_FUERA_HORA;

   --************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_INFRM_SBRGR_BANCO_CRPSL
   -- Objetivo: Procedimiento que crea el Informe de sobregiros Bancos Corresponsales MX
   --            Swift del dia.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_RGTRO_SALDO_HTRCO
   --
   -- Fecha: 24-08-2018 CAH
   -- Autor: Santander
   -- Input: p_FEC_PCS -> Fecha Proceso.
   -- Output: p_CURSOR -> Con datos para la creacion de la interfaz
   --         p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: 24-08-2018, Creacion Procedimiento.
   --***********************************************************************************************
   PROCEDURE SP_PAB_INFRM_SBRGR_BANCO_CRPSL (
      p_FEC_PCS   IN     VARCHAR2,
      p_CURSOR       OUT SYS_REFCURSOR,
      p_ERROR        OUT NUMBER
   )
   IS
      v_NOM_SP        VARCHAR2 (30) := 'SP_PAB_INFRM_SBRGR_BANCO_CRPSL';
      v_FEC_PCS       DATE := TO_DATE (p_FEC_PCS, 'DD-MM-YYYY');
      p_COD_ERR_BIB   CHAR (6) := '0';
      p_COD_TPO_PSC   CHAR (3) := 'PCT';
      p_GLS_DET_PCS   VARCHAR2 (300);
      p_FEC_ICO_PCS   VARCHAR2 (30);
      p_NUM_TCK_PCS   NUMBER;
      v_FEC_DIA       NUMBER := TO_NUMBER (TO_CHAR (SYSDATE, 'YYYYMMDD'));
   BEGIN
      p_ERROR := PKG_PAB_CONSTANTES.v_OK;

      --Inicio creacion Interfaz Riesgo
      p_GLS_DET_PCS :=
         'Iniciando Proceso de Informe de sobregiros Bancos Corresponsales MX';
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_CAB_PROCESO (p_COD_TPO_PSC,
                                                   p_FEC_ICO_PCS,
                                                   p_NUM_TCK_PCS,
                                                   p_ERROR);
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);

      OPEN p_CURSOR FOR
         --         SELECT   COD_BIC_BCO AS BANCO,
         --                  FEC_REG_SDO AS FECHA,
         --                  HOR_REG_SDO AS HORA,
         --                  COD_DVI AS MONEDA,
         --                  SAL.IMP_NET_MIN AS MONTO_SOBREGIRO
         SELECT   COD_BIC_BCO AS BANCO,
                  COD_DVI AS FECHA,
                  FEC_REG_SDO AS HORA,
                  HOR_REG_SDO AS MONEDA,
                  SAL.IMP_NET_MIN AS MONTO_SOBREGIRO
           FROM   PABS_DT_RGTRO_SALDO_HTRCO SAL
          WHERE       COD_DVI <> PKG_PAB_CONSTANTES.V_STR_DVI_CLP
                  AND FEC_REG_SDO = v_FEC_DIA
                  AND COD_CNL_SAL = PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_SWF
                  AND IMP_SDO_RAL < 0;                      --Valor negativo.;


      p_ERROR := PKG_PAB_CONSTANTES.v_OK;

      --Fin Proceso de Informe de sobregiros Bancos Corresponsales MX
      p_GLS_DET_PCS :=
         'Fin proceso de creacion Informe de sobregiros Bancos Corresponsales MX';
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);
      PKG_PAB_TRACKING.Sp_PAB_UPD_BIT_CAB_PROCESO (p_FEC_ICO_PCS,
                                                   p_COD_TPO_PSC,
                                                   p_NUM_TCK_PCS,
                                                   p_ERROR,
                                                   p_ERROR);
   EXCEPTION
      WHEN DUP_VAL_ON_INDEX
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_INFRM_SBRGR_BANCO_CRPSL;

   --************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_INFRM_USO_SISTM_PAGO
   -- Objetivo: Procedimiento que crea el Informe Uso Sistema de Pago (Lbtr y Combanc)
   --            Swift del dia.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN
   --
   -- Fecha: 30-10-2018
   -- Autor: CAH
   -- Input: p_FEC_PCS -> Fecha Proceso.
   -- Output:
   --          p_TOTAL     -> Cursor con totales segun canal de salida (CLP)
   --          p_DETALLE   -> Cursor con detalle segun canal de salida (CLP)
   --         p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: 24-08-2018, Creacion Procedimiento.
   --***********************************************************************************************
   PROCEDURE SP_PAB_INFRM_USO_SISTM_PAGO (p_FEC_PCS   IN     VARCHAR2,
                                          p_TOTAL        OUT SYS_REFCURSOR,
                                          p_DETALLE      OUT SYS_REFCURSOR,
                                          p_ERROR        OUT NUMBER)
   IS
      v_NOM_SP        VARCHAR2 (30) := 'SP_PAB_INFRM_USO_SISTM_PAGO';
      v_FEC_PCS       DATE := TO_DATE (p_FEC_PCS, 'DD-MM-YYYY');
      p_COD_ERR_BIB   CHAR (6) := '0';
      p_COD_TPO_PSC   CHAR (3) := 'PCT';
      p_GLS_DET_PCS   VARCHAR2 (300);
      p_FEC_ICO_PCS   VARCHAR2 (30);
      p_NUM_TCK_PCS   NUMBER;
      v_DIA_MES_INI   DATE := TRUNC (ADD_MONTHS (SYSDATE, -1));
      v_DIA_MES_FIN   DATE := TRUNC (SYSDATE);
   BEGIN
      -- INICIALIZACION DE VARIABLES Y PARAMETROS
      p_ERROR := PKG_PAB_CONSTANTES.v_OK;

      --Inicio creacion Informe Uso Sistema de Pago
      p_GLS_DET_PCS := 'Iniciando Proceso de Informe Uso Sistema de Pago';
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_CAB_PROCESO (p_COD_TPO_PSC,
                                                   p_FEC_ICO_PCS,
                                                   p_NUM_TCK_PCS,
                                                   p_ERROR);
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);

      BEGIN                   --Cursor con totales segun canal de salida (CLP)
         OPEN p_TOTAL FOR
            SELECT                                -- TRUNC (SYSDATE) AS FECHA,
                  TO_CHAR (SYSDATE, 'YYYY-MM-DD') AS FECHA,
                     SUM (OPE.IMP_OPE) AS MONTO_EGRESO,
                     ROUND (
                        ( (NVL (
                              SUM(DECODE (
                                     OPE.COD_SIS_SAL,
                                     PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_LBTR,
                                     OPE.IMP_OPE
                                  )),
                              0
                           ))
                         * 100)
                        / SUM (OPE.IMP_OPE),
                        2
                     )
                        AS POR_LBTR,
                     ROUND (
                        ( (NVL (
                              SUM(DECODE (
                                     OPE.COD_SIS_SAL,
                                     PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_CMN,
                                     OPE.IMP_OPE
                                  )),
                              0
                           ))
                         * 100)
                        / SUM (OPE.IMP_OPE),
                        2
                     )
                        AS POR_COMBANC
              FROM   PABS_DT_DETLL_OPRCN OPE
             WHERE   OPE.COD_DVI = PKG_PAB_CONSTANTES.V_STR_DVI_CLP
                     AND OPE.FLG_EGR_ING = PKG_PAB_CONSTANTES.V_FLG_EGR
                     AND OPE.COD_SIS_SAL IN
                              (PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_LBTR,
                               PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_CMN)
                     AND OPE.FEC_ISR_OPE BETWEEN v_DIA_MES_INI
                                             AND  v_DIA_MES_FIN
                     AND OPE.COD_EST_AOS IN
                              (PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAG,
                               PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEABN,
                               PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAGCFI,
                               PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDEV); --(12, 15, 18, 21)   ;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            err_code := SQLCODE;
            err_msg :=
               SUBSTR (SQLERRM, 1, 300) || 'No se puedo encontrar datos';
            RAISE;
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            p_s_mensaje := err_code || '-' || err_msg;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;


      BEGIN                   --Cursor con detalle segun canal de salida (CLP)
         OPEN p_DETALLE FOR                   --debe mostrar data de un mes...
              SELECT                       --TRUNC (OPE.FEC_ISR_OPE) AS FECHA,
                    TO_CHAR (OPE.FEC_ISR_OPE, 'YYYY-MM-DD') AS FECHA,
                       SUM (OPE.IMP_OPE) AS MONTO_EGRESO,
                       SUM(DECODE (OPE.COD_SIS_SAL,
                                   PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_LBTR,
                                   OPE.IMP_OPE,
                                   0))
                          AS TOTAL_LBTR,
                       SUM(DECODE (OPE.COD_SIS_SAL,
                                   PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_CMN,
                                   OPE.IMP_OPE,
                                   0))
                          AS TOTAL_COMBANC,
                       ROUND (
                          ( (NVL (
                                SUM(DECODE (
                                       OPE.COD_SIS_SAL,
                                       PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_LBTR,
                                       OPE.IMP_OPE
                                    )),
                                0
                             ))
                           * 100)
                          / SUM (OPE.IMP_OPE),
                          2
                       )
                          AS POR_LBTR,
                       ROUND (
                          ( (NVL (
                                SUM(DECODE (
                                       OPE.COD_SIS_SAL,
                                       PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_CMN,
                                       OPE.IMP_OPE
                                    )),
                                0
                             ))
                           * 100)
                          / SUM (OPE.IMP_OPE),
                          2
                       )
                          AS POR_COMBANC
                FROM   PABS_DT_DETLL_OPRCN OPE
               WHERE   OPE.COD_DVI = PKG_PAB_CONSTANTES.V_STR_DVI_CLP
                       AND OPE.FLG_EGR_ING = PKG_PAB_CONSTANTES.V_FLG_EGR
                       AND OPE.COD_SIS_SAL IN
                                (PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_LBTR,
                                 PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_CMN)
                       AND OPE.FEC_ISR_OPE BETWEEN v_DIA_MES_INI
                                               AND  v_DIA_MES_FIN
                       AND OPE.COD_EST_AOS IN
                                (PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAG,
                                 PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEABN,
                                 PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAGCFI,
                                 PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDEV) --(12, 15, 18, 21)
            GROUP BY   TO_CHAR (OPE.FEC_ISR_OPE, 'YYYY-MM-DD'),
                       TRUNC (OPE.FEC_ISR_OPE)
            ORDER BY   TRUNC (OPE.FEC_ISR_OPE) ASC;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            err_code := SQLCODE;
            err_msg :=
               SUBSTR (SQLERRM, 1, 300) || 'No se puedo encontrar datos';
            RAISE;
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            p_s_mensaje := err_code || '-' || err_msg;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;

      p_ERROR := PKG_PAB_CONSTANTES.v_OK;

      --Fin Proceso Informe Uso Sistema de Pago
      p_GLS_DET_PCS := 'Fin proceso de creacion Informe Uso Sistema de Pago';
      PKG_PAB_TRACKING.SP_PAB_INS_BIT_DET_PROCESO (p_GLS_DET_PCS,
                                                   p_FEC_ICO_PCS,
                                                   p_COD_ERR_BIB,
                                                   p_COD_TPO_PSC,
                                                   p_ERROR);
      PKG_PAB_TRACKING.Sp_PAB_UPD_BIT_CAB_PROCESO (p_FEC_ICO_PCS,
                                                   p_COD_TPO_PSC,
                                                   p_NUM_TCK_PCS,
                                                   p_ERROR,
                                                   p_ERROR);
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_INFRM_USO_SISTM_PAGO;

   /******************************************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_COMBO_SUB_PRDTO
   -- Objetivo: Procedimiento almacenado que retorna los sub productos
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_TIPO_SUB_PRDTO
   --
   -- Fecha: 15-01-2018
   -- Autor: Santander CAH
   -- Input:
   --
   -- Output:
   --       p_TIPO_PROD : datos de productos
   --       p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --******************************************************************************************************************************/
   PROCEDURE SP_PAB_COMBO_SUB_PRDTO (p_TIPO_PROD   OUT SYS_REFCURSOR,
                                     p_ERROR       OUT NUMBER)
   IS
      v_NOM_SP   VARCHAR2 (30) := 'SP_PAB_COMBO_SUB_PRDTO';
   BEGIN
      OPEN p_TIPO_PROD FOR
           SELECT   TS.COD_TPO_SPO AS COD_OPE,
                    TS.DSC_TPO_SPO AS DESCRIPCION_COD_OPE
             FROM   PABS_DT_TIPO_SUB_PRDTO TS
         ORDER BY   TS.COD_TPO_SPO ASC;

      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_COMBO_SUB_PRDTO;



   /************************************************************************************************
   --  Procedimiento        :  SP_PAB_CREA_PART_SALDO_HTRCO
   --  Objetivo/Descripcion :  Procedimiento que utiliza pkg_dba para crear particiones de
   --                          tablas.
   --  Sistema: PAB
   --  Base de Datos: DGBMSEGDB01_NGALTM
   --  Tablas Usadas: PABS_DT_RGTRO_SALDO_HTRCO
   --  Fecha: 11-12-2018
   --  Autor: Santander CAH
   --  Input:
   --          NOM_TABLA   --> Nombre de tabla para crear particion
   --          NOM_PART    --> Nombre de particion
   --          FEC_DATA    --> Fecha de particion
   --  N/A
   --  Output:
   --       p_ERROR:    Indicador de errores, maneja dos estados, 0 sin errores 1 existen errores..
   --  Input/Output: N/A
   --  Retorno: N/A.
   --  Observaciones: <Fecha y Detalle de ultimos cambios>
   *************************************************************************************************/
   PROCEDURE SP_PAB_CREA_PART_SALDO_HTRCO (NOM_TABLA   IN     VARCHAR2,
                                           NOM_PART    IN     VARCHAR2,
                                           FEC_DATA    IN     VARCHAR2,
                                           p_ERROR        OUT NUMBER)
   IS
      v_NOM_SP     VARCHAR2 (30) := 'SP_PAB_CREA_PART_SALDO_HTRCO';
      valuespart   pkg_dba.valueslist;
   BEGIN
      valuespart := pkg_dba.valueslist (FEC_DATA);
      pkg_dba.add_partt_range (NOM_TABLA, NOM_PART, valuespart);

      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   EXCEPTION
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_CREA_PART_SALDO_HTRCO;


   --***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_ELI_PART_SALDO_HTRCO
   -- Objetivo: Procedimiento que elimina particion de la tabla PABS_DT_RGTRO_SALDO_HTRCO
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_RGTRO_SALDO_HTRCO
   -- Fecha: 11-12-2018
   -- Autor: Santander CAH
   -- Input:
   --          NOM_TABLA   --> Nombre de tabla para crear particion
   --          NOM_PART    --> Nombre de particion a eliminar
   -- Output:
   --       p_ERROR:    Indicador de errores, maneja dos estados, 0 sin errores 1 existen errores..
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_PAB_ELI_PART_SALDO_HTRCO (NOM_TABLA   IN     VARCHAR2,
                                          NOM_PART    IN     VARCHAR2,
                                          p_ERROR        OUT NUMBER)
   IS
      v_NOM_SP   VARCHAR2 (30) := 'SP_PAB_ELI_PART_SALDO_HTRCO';
   BEGIN
      pkg_dba.drop_partt (NOM_TABLA, NOM_PART);

      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   EXCEPTION
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_ELI_PART_SALDO_HTRCO;
END PKG_PAB_LIQUIDEZ;