CREATE OR REPLACE PACKAGE BODY         PKG_PAB_NOMINA
IS

-- Package el cual contrendra todas las funciones y procedimientos relacionados con las nominas
-- @Santander

--***************************Sp_PAB_INS_CAB_NOMINA*****************************
-- Funcion/Procedimiento: Sp_PAB_INS_CAB_NOMINA
-- Objetivo: Procedimiento que inserta la informacion de la cabecera de nomina
-- Sistema: PAB
-- Base de Datos: DGBMSEGDB01_NGALTM
-- Tablas Usadas: PABS_DT_CBCRA_NOMNA
-- Fecha: 21/06/16
-- Autor: Santander
-- Input:
-- p_NUM_FOL_NMN -> Foliador del banco
-- p_GLS_MTv_EST -> Informacion adicional del estado
-- p_NOM_NMN     -> Nombre de la nomina
-- p_COD_SIS_ENT -> Codigo del canal de entrada
-- p_COD_USR     -> Rut usuario
-- p_EST_NMN     -> Indica el estado de la nomina (0 Valida :: 1 Invalida)
-- Output:
-- p_ERROR       -> Indicador de errores (0 Sin errores :: 1 existen errores). Se ven en la tabla Log estos errores.
-- Input/Output: N/A
-- Retorno: N/A.
-- Observaciones: <Fecha y Detalle de ?ltimos cambios>
--***********************************************************************************************
   PROCEDURE Sp_PAB_INS_CAB_NOMINA (p_NUM_FOL_NMN   IN     NUMBER,
                                    p_GLS_MTv_EST   IN     VARCHAR2,
                                    p_NOM_NMN       IN     VARCHAR2,
                                    p_COD_SIS_ENT   IN     CHAR,
                                    p_COD_USR       IN     CHAR,
                                    p_EST_NMN       IN     NUMBER,
                                    p_ERROR         OUT    NUMBER)
   IS
      --Seteo de Variables
      v_NOM_SP        VARCHAR2 (30) := 'Sp_PAB_INS_CAB_NOMINA';
      v_COD_SIS_ENT   CHAR (10);
      v_RESULT        NUMBER;
      v_COD_EST_NMN   NUMBER;
      v_COD_SIS_ENT_AUX   CHAR(30);
      v_GLS_MTv_EST VARCHAR2(300):=p_GLS_MTv_EST;

   BEGIN

      v_COD_SIS_ENT_AUX := p_COD_SIS_ENT;

      --Obtenemos el sistema de entrada del usuario
      PKG_PAB_UTILITY.Sp_PAB_OBT_CAN_ENT_USU (p_COD_USR, v_COD_SIS_ENT, v_RESULT);

      IF (v_RESULT = PKG_PAB_CONSTANTES.v_OK)
      THEN

         IF (p_EST_NMN = PKG_PAB_CONSTANTES.v_OK)
         THEN
            v_COD_EST_NMN := PKG_PAB_CONSTANTES.v_COD_EST_AOS_NMNVAL;                                                                                                                                                                                                                                                                                                                                                                                                                                       --Valida
         ELSE
            v_COD_EST_NMN := PKG_PAB_CONSTANTES.v_COD_EST_AOS_NMNINV;                                                                                                                                                                                                                                                                                                                                                                                                                                     --Invalida
         END IF;

         INSERT INTO PABS_DT_CBCRA_NOMNA (NUM_FOL_NMN,
                                          GLS_MTv_EST,
                                          NOM_NMN,
                                          FEC_ISR_NMN,
                                          HOR_ISR_NMN,
                                          COD_SIS_ENT,
                                          COD_USR,
                                          COD_EST_AOS,
                                          FLG_DVI)
              VALUES (p_NUM_FOL_NMN,
                      v_GLS_MTv_EST,
                      p_NOM_NMN,
                      SYSDATE,
                      TO_NUMBER (TO_CHAR (SYSDATE, 'HH24MI')),
                      v_COD_SIS_ENT,
                      p_COD_USR,
                      v_COD_EST_NMN,
                      0);

         p_ERROR := PKG_PAB_CONSTANTES.v_OK;

      ELSE
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      END IF;
   EXCEPTION
      WHEN DUP_VAL_ON_INDEX
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);

   END Sp_PAB_INS_CAB_NOMINA;

--***********************************************************************************
-- Funcion/Procedimiento : Sp_PAB_INS_DET_NOMINA
-- Objetivo : Procedimiento que inserta la informacion que se encuentra en la nomina
-- Sistema : PAB
-- Base de Datos : DGBMSEGDB01_NGALTM
-- Tablas Usadas : PABS_DT_DETLL_NOMNA
-- Fecha : 21/06/16
-- Autor : Santander
-- Input:
-- p_NUM_FOL_OPE        -> Numero Foliador de la operacion
-- p_NOM_BFC            -> Nombre del Beneficiario
-- p_NUM_FOL_NMN        -> Numero Foliador de la nomina
-- p_NUM_OPE_SIS_ENT    -> Numero de operacion de la operacion en el sistema origen
-- p_COD_MT_SWF         -> Numero del tipo de mensaje swift
-- p_COD_TPO_OPE_AOS    -> Tipo de operacion.
-- p_FEC_VTA            -> Fecha de vencimiento
-- p_COD_DVI            -> Codigo de la divisa
-- p_IMp_OPE            -> Importe de la operacion
-- p_COD_BCO_DTN        -> BIC del banco destino
-- p_NUM_DOC_BFC        -> Numero rut del beneficiario
-- p_NUM_CTA_BFC        -> Numero de cuenta del beneficiario
-- p_COD_SUC            -> Codigo de la sucursal
-- p_GLS_ADC_EST        -> Informacion adicional
-- p_NUM_REF_CTB        -> Numero de referencia contable
-- p_OBS_OPC_SWF        -> Observacion Opcional Swift
-- p_COD_BCO_BFC        -> BIB Banco Beneficario
-- p_COD_BCO_ITD        -> BIC Banco Intermediario
-- p_NUM_CTA_DCv_BFC    -> Numero cuenta DCV Beneficiario
-- p_GLS_DIR_BFC        -> Direccion Beneficario
-- p_NOM_CDD_BFC        -> Nombre ciudad beneficiario
-- p_COD_PAS_BFC        -> Codigo pais beneficiario
-- p_NOM_ODN            -> Nombre ordenante
-- p_NUM_DOC_ODN        -> Numero rut ordenante
-- p_NUM_CTA_ODN        -> Numero cuenta ordenante
-- p_NUM_CTA_DCv_ODN    -> Numero cuenta DCV ordenante
-- p_GLS_DIR_ODN        -> Direccion ordenante
-- p_NOM_CDD_ODN        -> Nombre ciudad ordenante
-- p_COD_PAS_ODN        -> Codigo pais ordenante
-- p_NUM_CLv_NGC        -> Numero de clave de negocio
-- p_NUM_AGT            -> Numero de agente
-- p_COD_FND_CCLV       -> Codigo Fondo (Producto CCLV)
-- p_COD_TPO_SDO        -> Tipo de saldo
-- p_COD_TPO_CMA        -> Tipo de camara
-- p_COD_TPO_FND        -> Tipo de fondo
-- p_FEC_TPO_OPE_CCLV   -> Fecha operacion CCLV
-- p_NUM_CLv_IDF        -> Numero clave identificatorio
-- p_COD_EST_OPE        -> Indica si la operacion esta con errores o no 0)Sin error 1)Error
-- p_COD_USR->          -> Rut usuario
-- p_COD_TPO_ING        -> Tipo de ingreso de la operacion (Masiva, unitaria)
-- Output :
-- p_FEC_ISR_OPE        -> Fecha con la que se inserto la operacion al sistema
-- p_ERROR              -> Indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
-- Input/Output : N/A
-- Retorno : N/A.
-- Observaciones : <Fecha y Detalle de ?ltimos cambios>
--***********************************************************************************************
   PROCEDURE Sp_PAB_INS_DET_NOMINA (p_NOM_BFC            IN     VARCHAR2,
                                    p_NUM_FOL_NMN        IN     NUMBER,
                                    p_NUM_OPE_SIS_ENT    IN     CHAR,
                                    p_COD_MT_SWF         IN     NUMBER,
                                    p_COD_TPO_OPE_AOS    IN     CHAR,
                                    p_FEC_VTA            IN     DATE,
                                    p_COD_DVI            IN     CHAR,
                                    p_IMp_OPE            IN     NUMBER,
                                    p_COD_BCO_DTN        IN     CHAR,
                                    p_NUM_DOC_BFC        IN     CHAR,
                                    p_NUM_CTA_BFC        IN     VARCHAR2,
                                    p_COD_SUC            IN     VARCHAR2,
                                    p_GLS_ADC_EST        IN     VARCHAR2,
                                    p_NUM_REF_CTB        IN     CHAR,
                                    -----------------------------
                                    p_OBS_OPC_SWF        IN     VARCHAR2,
                                    p_COD_BCO_BFC        IN     CHAR,
                                    p_COD_BCO_ITD        IN     CHAR,
                                    p_NUM_CTA_DCv_BFC    IN     VARCHAR2,
                                    p_GLS_DIR_BFC        IN     VARCHAR2,
                                    p_NOM_CDD_BFC        IN     VARCHAR2,
                                    p_COD_PAS_BFC        IN     CHAR,
                                    p_NOM_ODN            IN     VARCHAR2,
                                    p_NUM_DOC_ODN        IN     CHAR,
                                    p_NUM_CTA_ODN        IN     VARCHAR2,
                                    p_NUM_CTA_DCv_ODN    IN     VARCHAR2,
                                    p_GLS_DIR_ODN        IN     VARCHAR2,
                                    p_NOM_CDD_ODN        IN     VARCHAR2,
                                    p_COD_PAS_ODN        IN     CHAR,
                                    p_NUM_CLv_NGC        IN     VARCHAR2,
                                    p_NUM_AGT            IN     CHAR,
                                    p_COD_FND_CCLV       IN     CHAR,
                                    p_COD_TPO_SDO        IN     CHAR,
                                    p_COD_TPO_CMA        IN     CHAR,
                                    p_COD_TPO_FND        IN     VARCHAR2,
                                    p_FEC_TPO_OPE_CCLV   IN     DATE,
                                    p_NUM_CLv_IDF        IN     VARCHAR2,
                                    --------------------------------
                                    p_COD_EST_OPE        IN     NUMBER,
                                    p_COD_USR            IN     CHAR,
                                    p_COD_TPO_ING        IN     NUMBER,
                                    p_FEC_ISR_OPE        OUT    DATE,
                                    p_NUM_FOL_OPE        OUT    NUMBER,
                                    p_ERROR              OUT    NUMBER)
   IS
      --Seteo de Variables
      v_NOM_SP            VARCHAR2 (30) := 'Sp_PAB_INS_DET_NOMINA';
      v_COD_EST_OPE       NUMBER;
      v_FLG_MRD_UTZ_SWF   NUMBER;
      v_NUM_DOC_BFC       VARCHAR2 (20);
      v_COD_TPD_BFC       CHAR (1);
      v_COD_SIS_ENT       CHAR (10);
      v_RESULT            NUMBER;
      v_FEC_ISR_OPE       DATE := SYSDATE;
      v_FLG_OPE_DUP       NUMBER := 0;
      v_NUM_CTA_BFC       VARCHAR2 (30);
      v_COD_FND_CCLV      CHAR(1);
      v_COD_PAS_ODN       CHAR(2);
      v_NOM_BFC           VARCHAR2(100);
      P_OPER_DUP          NUMBER(1);
      v_GLS_ADC_EST       varchar2(210):=p_GLS_ADC_EST;
      V_NUM_FOL_OPE       NUMBER(12) := SPK_PAB_PAG_EAM.NEXTVAL;


   BEGIN
--
        --Variables utilizadas para identificar si es unitario o masivo.
        IF(P_GLS_ADC_EST = 1 OR P_GLS_ADC_EST= 0) THEN
            v_GLS_ADC_EST := NULL;
        END IF;

        --seteamos la variable para que tibco sepa con que fecha se inserto la operacion
        p_FEC_ISR_OPE:= v_FEC_ISR_OPE;

        --Verificamos el estado de la operacion
        IF (p_COD_EST_OPE = PKG_PAB_CONSTANTES.v_COD_EST_AOS_OPEVAL) THEN
            v_COD_EST_OPE := PKG_PAB_CONSTANTES.v_COD_EST_AOS_OPEVAL;
        ELSE
            v_COD_EST_OPE := PKG_PAB_CONSTANTES.v_COD_EST_AOS_OPEINV;
            v_GLS_ADC_EST := 'Informacion con error validacion';
        END IF;

        --Obtenemos el mercado de la operacion
        PKG_PAB_UTILITY.Sp_PAB_CAL_MER (p_COD_DVI, v_FLG_MRD_UTZ_SWF);

        v_NUM_CTA_BFC := TRIM (p_NUM_CTA_BFC);

        -- Mantenedor Beneficiario
        IF (NVL (p_NUM_DOC_BFC, '#') <> '#') THEN

            --Separamos el numero de documento y el digito verificador del rut Beneficiario
            PKG_PAB_UTILITY.Sp_PAB_DESCOMp_RUT (p_NUM_DOC_BFC, v_NUM_DOC_BFC, v_COD_TPD_BFC);

            --Llamamos a mantenedor de beneficiario ordenante
            PKG_PAB_UTILITY.Sp_PAB_MAN_BFC_ODN (v_NUM_DOC_BFC, v_COD_TPD_BFC);

            --Cuenta Normal
            IF (NVL (p_COD_BCO_DTN, '#') <> '#' AND NVL (p_NUM_CTA_BFC, '#') <> '#') THEN

                --Verificamos si existe la cuenta del beneficiario
                PKG_PAB_UTILITY.Sp_PAB_MAN_CTA_BFC_ODN (v_NUM_DOC_BFC,
                                                        v_COD_TPD_BFC,
                                                        v_NUM_CTA_BFC,
                                                        PKG_PAB_CONSTANTES.v_FLG_DCv_NO,
                                                        p_COD_BCO_DTN);
            END IF;

        END IF;

        --Obtenemos el sistema de entrada del usuario
        PKG_PAB_UTILITY.Sp_PAB_OBT_CAN_ENT_USU (p_COD_USR, v_COD_SIS_ENT, v_RESULT);

        --Si no tiene errores la operacion
        IF (v_RESULT = PKG_PAB_CONSTANTES.v_OK) THEN

            -- VALIDA POSIBLE DUPLICIDAD
            IF(v_COD_EST_OPE = PKG_PAB_CONSTANTES.v_COD_EST_AOS_OPEVAL) THEN

                --valida duplicidad
                PKG_PAB_NOMINA.SP_PAB_VAL_OPER_DUPLICADA(    P_FEC_VTA
                                            , P_COD_BCO_DTN
                                            , P_NUM_CTA_BFC
                                            , P_IMP_OPE
                                            , P_COD_USR
                                            , P_OPER_DUP        --OUT  --valor 1 si duplicidad
                                            , P_ERROR           --OUT  --Valor 0 OK
                                         );

                IF P_ERROR = PKG_PAB_CONSTANTES.v_OK THEN

                    IF P_OPER_DUP = PKG_PAB_CONSTANTES.V_COD_DUP_OK THEN
                        v_FLG_OPE_DUP := PKG_PAB_CONSTANTES.V_COD_DUP_OK ; --MARCAMOS EL FLAG CON 1 PARA IDENTIFICARLA COMO POSIBLE DUPLICADA
                        v_COD_EST_OPE := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDUP;
                    END IF;

                END IF;


            END IF;
            ------------------------------------------------

            --En caso de venir variables vacias se setean
            v_NOM_BFC := NVL(p_NOM_BFC,' ');

            --Insertamos la informacion principal de la operacion
            INSERT INTO PABS_DT_DETLL_NOMNA (FEC_ISR_OPE,
                                          NUM_FOL_OPE,
                                          NOM_BFC,
                                          NUM_FOL_NMN,
                                          NUM_OPE_SIS_ENT,
                                          FLG_EGR_ING,
                                          COD_EST_AOS,
                                          COD_TPO_ING,
                                          COD_MT_SWF,
                                          FLG_MRD_UTZ_SWF,
                                          COD_TPO_OPE_AOS,
                                          FEC_VTA,
                                          COD_DVI,
                                          IMp_OPE,
                                          COD_BCO_DTN,
                                          NUM_DOC_BFC,
                                          COD_TPD_BFC,
                                          NUM_CTA_BFC,
                                          COD_SUC,
                                          GLS_ADC_EST,
                                          NUM_REF_CTB,
                                          FLG_OPE_DUP,
                                          FEC_ACT_NMN)-- Fecha de actualización para dashboard
              VALUES (v_FEC_ISR_OPE,
                      V_NUM_FOL_OPE,
                      v_NOM_BFC,
                      p_NUM_FOL_NMN,
                      p_NUM_OPE_SIS_ENT,
                      PKG_PAB_CONSTANTES.v_FLG_EGR,
                      v_COD_EST_OPE,
                      p_COD_TPO_ING, --PKG_PAB_CONSTANTES.v_COD_TIp_ING_MAS,
                      p_COD_MT_SWF,
                      v_FLG_MRD_UTZ_SWF,
                      p_COD_TPO_OPE_AOS,
                      p_FEC_VTA,
                      p_COD_DVI,
                      p_IMp_OPE,
                      p_COD_BCO_DTN,
                      v_NUM_DOC_BFC,
                      v_COD_TPD_BFC,
                      v_NUM_CTA_BFC,
                      LPAD(NVL(p_COD_SUC,'0174'),4,'0'),
                      v_GLS_ADC_EST,
                      LPAD(p_NUM_REF_CTB,12,'0'),
                      v_FLG_OPE_DUP,
                      SYSDATE);-- Fecha de actualización para dashboard

            --Llamamos al procedimiento de Bitacora
            v_DES_BIT := 'Carga de Nomina';
            PKG_PAB_TRACKING.Sp_PAB_INS_BIT_OPE (V_NUM_FOL_OPE,
                                              v_FEC_ISR_OPE,
                                              p_COD_USR,
                                              v_COD_EST_OPE,
                                              v_COD_EST_OPE,
                                              v_DES_BIT,
                                              v_RESULT);

            --Verificamos si vienen a los menos un datos opcional
            IF (   (NVL (p_OBS_OPC_SWF, '#') <> '#')
                OR (NVL (p_COD_BCO_BFC, '#') <> '#')
                OR (NVL (p_COD_BCO_ITD, '#') <> '#')
                OR (NVL (p_NUM_CTA_DCv_BFC, '#') <> '#')
                OR (NVL (p_GLS_DIR_BFC, '#') <> '#')
                OR (NVL (p_NOM_CDD_BFC, '#') <> '#')
                OR (NVL (p_COD_PAS_BFC, '#') <> '#')
                OR (NVL (p_NOM_ODN, '#') <> '#')
                OR (NVL (p_NUM_DOC_ODN, '#') <> '#')
                OR (NVL (p_NUM_CTA_ODN, '#') <> '#')
                OR (NVL (p_NUM_CTA_DCv_ODN, '#') <> '#')
                OR (NVL (p_GLS_DIR_ODN, '#') <> '#')
                OR (NVL (p_NOM_CDD_ODN, '#') <> '#')
                OR (NVL (p_COD_PAS_ODN, '#') <> '#')
                OR (NVL (p_NUM_CLv_NGC, '#') <> '#')
                OR (NVL (p_NUM_AGT, '#') <> '#')
                OR (NVL (p_COD_FND_CCLV, '#') <> '#')
                OR (NVL (p_COD_TPO_SDO, '#') <> '#')
                OR (NVL (p_COD_TPO_CMA, '#') <> '#')
                OR (NVL (p_COD_TPO_FND, '#') <> '#')
                OR (NVL (p_FEC_TPO_OPE_CCLV, '#') <> '#')
                OR (NVL (p_NUM_CLv_IDF, '#') <> '#')) THEN

                --En caso de ser un producto CCLV
                IF( p_COD_TPO_OPE_AOS = PKG_PAB_CONSTANTES.V_STR_TPO_OPE_CCLV ) THEN
                    v_COD_FND_CCLV := 'X';
                END IF;

                --Si es un pago al extranjero y es 103
                IF(p_COD_DVI <> PKG_PAB_CONSTANTES.v_STR_DVI_CLP AND p_COD_MT_SWF <> PKG_PAB_CONSTANTES.v_COD_MT202 ) THEN
                    v_COD_PAS_ODN := PKG_PAB_CONSTANTES.v_STR_COD_PAS_CHILE;
                END IF;

                --Insertamos los campos opcionales
                PKG_PAB_NOMINA.Sp_PAB_INS_DET_OPC_NOMINA (v_FEC_ISR_OPE,
                                                          V_NUM_FOL_OPE,
                                                          p_COD_BCO_DTN,
                                                          v_NUM_DOC_BFC,
                                                          v_COD_TPD_BFC,
                                                          p_OBS_OPC_SWF,
                                                          p_COD_BCO_BFC,
                                                          p_COD_BCO_ITD,
                                                          p_NUM_CTA_DCv_BFC,
                                                          p_GLS_DIR_BFC,
                                                          p_NOM_CDD_BFC,
                                                          p_COD_PAS_BFC,
                                                          p_NOM_ODN,
                                                          p_NUM_DOC_ODN,
                                                          p_NUM_CTA_ODN,
                                                          p_NUM_CTA_DCv_ODN,
                                                          p_GLS_DIR_ODN,
                                                          p_NOM_CDD_ODN,
                                                          v_COD_PAS_ODN,
                                                          p_NUM_CLv_NGC,
                                                          p_NUM_AGT,
                                                          v_COD_FND_CCLV,
                                                          p_COD_TPO_SDO,
                                                          p_COD_TPO_CMA,
                                                          p_COD_TPO_FND,
                                                          p_FEC_TPO_OPE_CCLV,
                                                          p_NUM_CLv_IDF,
                                                          v_RESULT);
            END IF;

        ELSE
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
        END IF;

        p_ERROR := PKG_PAB_CONSTANTES.v_ok;
        p_NUM_FOL_OPE := V_NUM_FOL_OPE;

   EXCEPTION
      WHEN DUP_VAL_ON_INDEX
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);

    --
   END Sp_PAB_INS_DET_NOMINA;

--******************************************************************************
-- IDENTIFICACION PROCEDIMIENTO-> Sp_PAB_INS_DET_OPC_NOMINA
-------------------------------------------------------------------------------
-- Version -> 1.0
-- Descripcion-> Procedimiento que inserta la informacion que se encuentra en la nomina
-- Sistema-> PAB
-- Base de Datos-> DGBMSEGDB01_NGALTM
-- Tablas Usadas-> PABS_DT_DETLL_NOMNA
-- Fecha-> 21/06/16
-- Autor-> Santander
-- Input->
-- p_FEC_ISR_OPE      -> Fecha de cuando fue insertada la operacion
-- p_NUM_FOL_OPE     -> Numero Foliador de la operacion
-- p_COD_BCO_DTN     -> Codigo banco destino
-- p_NUM_DOC_BFC     -> Numero rut del beneficiario
-- p_COD_TPD_BFC     -> Digito verificador beneficiario
-- p_OBS_OPC_SWF     -> Observacion Opcional Swift
-- p_COD_BCO_BFC     ->BIB Banco Beneficario
-- p_COD_BCO_ITD     ->BIC Banco Intermediario
-- p_NUM_CTA_DCv_BFC -> Numero cuenta DCV Beneficiario
-- p_GLS_DIR_BFC     ->Direccion Beneficario
-- p_NOM_CDD_BFC     ->Nombre ciudad beneficiario
-- p_COD_PAS_BFC     ->Codigo pais beneficiario
-- p_NOM_ODN         ->Nombre ordenante
-- p_NUM_DOC_ODN     ->Numero rut ordenante
-- p_NUM_CTA_ODN     ->Numero cuenta ordenante
-- p_NUM_CTA_DCv_ODN -> Numero cuenta DCV ordenante
-- p_GLS_DIR_ODN    -> Direccion ordenante
-- p_NOM_CDD_ODN    ->Nombre ciudad ordenante
-- p_COD_PAS_ODN    ->Codigo pais ordenante
-- p_NUM_CLv_NGC    ->Numero de clave de negocio
-- p_NUM_AGT        ->Numero de agente
-- p_COD_FND_CCLV   -> Codigo Fondo (Producto CCLV)
-- p_COD_TPO_SDO    -> Tipo de saldo
-- p_COD_TPO_CMA    -> Tipo de camara
-- p_COD_TPO_FND     -> Tipo de fondo
-- p_FEC_TPO_OPE_CCLV -> Fecha operacion CCLV
-- p_NUM_CLv_IDF     -> Numero clave identificatorio
-- Output :
-- Input/Output : N/A
-- Retorno : N/A.
-- Observaciones : <Fecha y Detalle de ?ltimos cambios>
--******************************************************************************
   PROCEDURE Sp_PAB_INS_DET_OPC_NOMINA (p_FEC_ISR_OPE        IN     DATE,
                                        p_NUM_FOL_OPE        IN     NUMBER,
                                        p_COD_BCO_DTN        IN     CHAR,
                                        ---------------------------------
                                        p_NUM_DOC_BFC        IN     CHAR,
                                        p_COD_TPD_BFC        IN     CHAR,
                                        p_OBS_OPC_SWF        IN     VARCHAR2,
                                        p_COD_BCO_BFC        IN     CHAR,
                                        p_COD_BCO_ITD        IN     CHAR,
                                        p_NUM_CTA_DCv_BFC    IN     VARCHAR2,
                                        p_GLS_DIR_BFC        IN     VARCHAR2,
                                        p_NOM_CDD_BFC        IN     VARCHAR2,
                                        p_COD_PAS_BFC        IN     CHAR,
                                        p_NOM_ODN            IN     VARCHAR2,
                                        p_NUM_DOC_ODN        IN     CHAR,
                                        p_NUM_CTA_ODN        IN     VARCHAR2,
                                        p_NUM_CTA_DCv_ODN    IN     VARCHAR2,
                                        p_GLS_DIR_ODN        IN     VARCHAR2,
                                        p_NOM_CDD_ODN        IN     VARCHAR2,
                                        p_COD_PAS_ODN        IN     CHAR,
                                        p_NUM_CLv_NGC        IN     VARCHAR2,
                                        p_NUM_AGT            IN     CHAR,
                                        p_COD_FND_CCLV       IN     CHAR,
                                        p_COD_TPO_SDO        IN     CHAR,
                                        p_COD_TPO_CMA        IN     CHAR,
                                        p_COD_TPO_FND        IN     VARCHAR2,
                                        p_FEC_TPO_OPE_CCLV   IN     DATE,
                                        p_NUM_CLv_IDF        IN     VARCHAR2,
                                        p_ERROR                 OUT NUMBER)
   IS
      --Seteo de Variables
      v_NOM_SP            VARCHAR2 (30) := 'Sp_PAB_INS_DET_OPC_NOMINA';
      v_NUM_DOC_ODN       VARCHAR2 (20);
      v_COD_TPD_ODN       CHAR (1);
      v_NUM_CTA_DCv_BFC   VARCHAR2 (30);
      v_NUM_CTA_ODN       VARCHAR2 (30);
      v_NUM_CTA_DCv_ODN   VARCHAR2 (30);
      v_COD_TPO_FND       CHAR (3);
   BEGIN

      v_NUM_CTA_DCv_BFC := TRIM (p_NUM_CTA_DCv_BFC);

      --Guardamos la cuenta DCV si viene informacion
      IF (NVL (p_NUM_CTA_DCv_BFC, '#') <> '#' AND NVL(p_NUM_DOC_BFC, '#') <> '#')
      THEN

         --Verificamos si existe la cuenta DCV del beneficiario
         PKG_PAB_UTILITY.Sp_PAB_MAN_CTA_BFC_ODN (p_NUM_DOC_BFC,
                                                 p_COD_TPD_BFC,
                                                 v_NUM_CTA_DCv_BFC,
                                                 PKG_PAB_CONSTANTES.v_FLG_DCv_SI,
                                                 p_COD_BCO_DTN);
      END IF;

      v_NUM_CTA_DCv_ODN := TRIM (p_NUM_CTA_DCv_ODN);
      v_NUM_CTA_ODN := TRIM (p_NUM_CTA_ODN);

      --Descomponemos el rut del ordenante
      IF (NVL (p_NUM_DOC_ODN, '#') <> '#')
      THEN
         --Separamos el numero de documento y el digito verificador del rut Beneficiario
         PKG_PAB_UTILITY.Sp_PAB_DESCOMp_RUT (p_NUM_DOC_ODN, v_NUM_DOC_ODN, v_COD_TPD_ODN);

         --Llamamos a mantenedor de beneficiario ordenante
         PKG_PAB_UTILITY.Sp_PAB_MAN_BFC_ODN (v_NUM_DOC_ODN, v_COD_TPD_ODN);

         --Guardamos la cuenta si viene informacion
         IF (NVL (p_NUM_CTA_ODN, '#') <> '#')
         THEN

            --Verificamos si existe la cuenta DCV del beneficiario
            PKG_PAB_UTILITY.Sp_PAB_MAN_CTA_BFC_ODN (v_NUM_DOC_ODN,
                                                    v_COD_TPD_ODN,
                                                    v_NUM_CTA_ODN,
                                                    PKG_PAB_CONSTANTES.v_FLG_DCv_NO,
                                                    PKG_PAB_CONSTANTES.v_BIC_BANCO_SANTANDER);
         END IF;

         --Guardamos la cuenta DCV si viene informacion
         IF (NVL (p_NUM_CTA_DCv_ODN, '#') <> '#' AND
             NVL (v_NUM_DOC_ODN, '#') <> '#')
         THEN

            --Verificamos si existe la cuenta DCV del beneficiario
            PKG_PAB_UTILITY.Sp_PAB_MAN_CTA_BFC_ODN (v_NUM_DOC_ODN,
                                                    v_COD_TPD_ODN,
                                                    v_NUM_CTA_DCv_ODN,
                                                    PKG_PAB_CONSTANTES.v_FLG_DCv_SI,
                                                    PKG_PAB_CONSTANTES.v_BIC_BANCO_SANTANDER);
         END IF;
      END IF;


      IF (NVL (p_COD_TPO_FND, '#') <> '#')
      THEN
         PKG_PAB_UTILITY.Sp_PAB_DEv_COD_TPO_FND (p_COD_TPO_FND, v_COD_TPO_FND);
      END IF;

      INSERT INTO PABS_DT_NOMNA_INFCN_OPCON (FEC_ISR_OPE,
                                             NUM_FOL_OPE,
                                             OBS_OPC_SWF,
                                             COD_BCO_BFC,
                                             COD_BCO_ITD,
                                             NUM_DOC_BFC,
                                             COD_TPD_BFC,
                                             NUM_CTA_DCv_BFC,
                                             GLS_DIR_BFC,
                                             NOM_CDD_BFC,
                                             COD_PAS_BFC,
                                             NOM_ODN,
                                             NUM_DOC_ODN,
                                             COD_TPD_ODN,
                                             NUM_CTA_ODN,
                                             NUM_CTA_DCv_ODN,
                                             GLS_DIR_ODN,
                                             NOM_CDD_ODN,
                                             COD_PAS_ODN,
                                             NUM_CLv_NGC,
                                             NUM_AGT,
                                             COD_FND_CCLV,
                                             COD_TPO_SDO,
                                             COD_TPO_CMA,
                                             COD_TPO_FND,
                                             FEC_TPO_OPE_CCLV,
                                             NUM_CLv_IDF)
           VALUES (p_FEC_ISR_OPE,
                   p_NUM_FOL_OPE,
                   p_OBS_OPC_SWF,
                   p_COD_BCO_BFC,
                   p_COD_BCO_ITD,
                   p_NUM_DOC_BFC,
                   p_COD_TPD_BFC,
                   v_NUM_CTA_DCv_BFC,
                   p_GLS_DIR_BFC,
                   p_NOM_CDD_BFC,
                   p_COD_PAS_BFC,
                   p_NOM_ODN,
                   v_NUM_DOC_ODN,
                   v_COD_TPD_ODN,
                   v_NUM_CTA_ODN,
                   v_NUM_CTA_DCv_ODN,
                   p_GLS_DIR_ODN,
                   p_NOM_CDD_ODN,
                   p_COD_PAS_ODN,
                   p_NUM_CLv_NGC,
                   p_NUM_AGT,
                   p_COD_FND_CCLV,
                   p_COD_TPO_SDO,
                   p_COD_TPO_CMA,
                   v_COD_TPO_FND,
                   p_FEC_TPO_OPE_CCLV,
                   p_NUM_CLv_IDF);

      p_ERROR := PKG_PAB_CONSTANTES.v_OK;

   EXCEPTION
      WHEN DUP_VAL_ON_INDEX
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);

   END Sp_PAB_INS_DET_OPC_NOMINA;
--
--
-- ***********************************************************************************************
-- Funcion/Procedimiento: Sp_PAB_BUS_CAB_NOMINA
-- Objetivo: Procedimiento que busca las nominas segun el estado de entrada
-- Sistema: PAB
-- Base de Datos: DGBMSEGDB01_NGALTM
-- Tablas Usadas: PABS_DT_CBCRA_NOMNA, PABS_DT_ESTDO_ALMOT
-- Fecha: 21/06/16
-- Autor: Santander
-- Input:
-- p_COD_EST_AOS -> Estado de la nomina
-- p_COD_SIS_ENT -> codigo del canal de entrada
-- Output:
-- p_CURSOR -> Cursor con las nominas encontradas
-- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
-- Input/Output: N/A
-- Retorno: N/A.
-- Observaciones: <Fecha y Detalle de ?ltimos cambios>
-- ***********************************************************************************************
   PROCEDURE Sp_PAB_BUS_CAB_NOMINA (p_COD_EST_AOS IN CHAR, p_COD_SIS_ENT IN CHAR, p_CURSOR OUT SYS_REFCURSOR, p_ERROR OUT NUMBER)
   IS
       --Seteo de Variables
          V_NOM_SP        VARCHAR2 (30) := 'SP_PAB_BUS_CAB_NOMINA';
          V_COD_EST_AOS   NUMBER (5);
          V_COD_EST_AOS_AUX   NUMBER (5);
          ERR_COD_INEX EXCEPTION;

       BEGIN

           --Seteo de variable estado
             CASE
                   WHEN P_COD_EST_AOS = PKG_PAB_CONSTANTES.V_STR_EST_AOS_NMNVAL   --Validas, listas para cursar.
                   THEN
                      V_COD_EST_AOS := PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNVAL; --Valida
                      V_COD_EST_AOS_AUX := PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNDUP; --Duplicado

                   WHEN P_COD_EST_AOS = PKG_PAB_CONSTANTES.V_STR_EST_AOS_NMNCUR   --Cursadas, Listas para autorizar
                   THEN
                      V_COD_EST_AOS := PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNCUR;
                      V_COD_EST_AOS_AUX := PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNCUR;

                   WHEN P_COD_EST_AOS = PKG_PAB_CONSTANTES.V_STR_EST_AOS_NMNAUT   --Autorizadas
                   THEN
                      V_COD_EST_AOS := PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNAUT;
                      V_COD_EST_AOS_AUX := PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNAUT;
                   WHEN P_COD_EST_AOS = PKG_PAB_CONSTANTES.V_STR_EST_AOS_NMNINV  --Invalidas
                   THEN
                      V_COD_EST_AOS := PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNINV;
                      V_COD_EST_AOS_AUX := PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNINV;
                   WHEN P_COD_EST_AOS = PKG_PAB_CONSTANTES.V_STR_EST_AOS_NMNELI  --Eliminadas
                   THEN
                      V_COD_EST_AOS := PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNELI;
                      V_COD_EST_AOS_AUX := PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNELI;
                   WHEN P_COD_EST_AOS = PKG_PAB_CONSTANTES.V_STR_EST_AOS_NMNREC  --Rechazada
                   THEN
                      V_COD_EST_AOS := PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNREC;
                      V_COD_EST_AOS_AUX := PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNREC;
                   ELSE
                      RAISE ERR_COD_INEX;
             END CASE;

                --Buscamos las nominas segun estado que solicita el front.
                OPEN P_CURSOR FOR
                 SELECT CABNOM.FLG_DVI,
                        CABNOM.NUM_FOL_NMN,
                        DECODE (CABNOM.FLG_DVI, PKG_PAB_CONSTANTES.V_FLG_DVI_MULTI, PKG_PAB_CONSTANTES.V_STR_DVI_MULTI, CABNOM.IMP_TOT_NMN) IMP_TOT_NMN,
                        CABNOM.COD_USR COD_USR_CRC,
                        CABNOM.NOM_NMN,
                        PKG_PAB_NOMINA.FN_PAB_BUS_DVI_NOMINA (CABNOM.FLG_DVI, CABNOM.NUM_FOL_NMN) COD_DVI,
                        EST.DSC_EST_AOS,
                        CABNOM.GLS_MTV_EST,
                        PKG_PAB_UTILITY.FN_PAB_TRAN_COD_EST_GLOSA(CABNOM.COD_EST_AOS, PKG_PAB_CONSTANTES.V_IND_NMN) AS COD_EST_AOS  -- CODIGO ESTADO DE LA NOMINA EJ: 'DUP'
                   FROM PABS_DT_CBCRA_NOMNA CABNOM,
                        PABS_DT_ESTDO_ALMOT EST
                  WHERE CABNOM.COD_EST_AOS = EST.COD_EST_AOS
                    AND CABNOM.COD_EST_AOS IN (V_COD_EST_AOS,V_COD_EST_AOS_AUX)   --Filtramos por el estado solicitado (VAL)
                    AND CABNOM.COD_SIS_ENT = P_COD_SIS_ENT
                    AND TO_DATE(CABNOM.FEC_ISR_NMN,'DD-MM-YYYY') = TO_DATE(SYSDATE,'DD-MM-YYYY') --Solo las nominas del dia
                    ORDER BY cABNOM.NUM_FOL_NMN ASC;

            P_ERROR := PKG_PAB_CONSTANTES.V_OK; --Procedimiento ejecutado de forma correcta

   EXCEPTION
      WHEN ERR_COD_INEX THEN
        err_msg := 'Se esta invocando con codigo inexistente:' || p_COD_EST_AOS;
        PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (NULL,
                                            v_NOM_PCK,
                                            err_msg,
                                            v_NOM_SP);
      WHEN DUP_VAL_ON_INDEX THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);

   END Sp_PAB_BUS_CAB_NOMINA;
--
-- ***********************************************************************************************
-- Funcion/Procedimiento: FN_PAB_BUS_DVI_NOMINA
-- Objetivo: Fusion que trae los el tipo de moneda de la nomina
-- Sistema: PAB
-- Base de Datos: DGBMSEGDB01_NGALTM
-- Tablas Usadas: PABS_DT_DETLL_NOMNA
-- Fecha: 21/06/16
-- Autor: Santander
-- Input:
-- p_NUM_FOL_NMN -> foliador de la nomina
-- p_FLG_DVI -> flag de multimoneda
-- Output:
-- N/A
-- Input/Output: N/A
-- Retorno: N/A.
-- Observaciones: <Fecha y Detalle de ?ltimos cambios>
-- ***********************************************************************************************
   FUNCTION FN_PAB_BUS_DVI_NOMINA (p_FLG_DVI IN NUMBER, p_NUM_FOL_NMN IN NUMBER)
      RETURN VARCHAR2
   IS
      p_TIPO_DIV   VARCHAR2 (11);
      v_NOM_SP     VARCHAR2 (30) := 'FN_PAB_BUS_DVI_NOMINA';
      V_EST_NOM     VARCHAR2(10);

     BEGIN

        SELECT COD_EST_AOS
        INTO V_EST_NOM
        FROM PABS_DT_CBCRA_NOMNA
        WHERE NUM_FOL_NMN = p_NUM_FOL_NMN
        AND COD_EST_AOS <> PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEELI;

        IF (p_FLG_DVI = PKG_PAB_CONSTANTES.v_FLG_DVI_MULTI) THEN
            p_TIPO_DIV := PKG_PAB_CONSTANTES.v_STR_DVI_MULTI;
        ELSE

            --Una Moneda
            IF V_EST_NOM =  PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNELI  THEN--ELIMINADA

                SELECT COD_DVI
                INTO p_TIPO_DIV
                FROM PABS_DT_DETLL_NOMNA
                WHERE NUM_FOL_NMN = p_NUM_FOL_NMN
                AND ROWNUM = 1; -- SIRVE PARA NO UTILIZAR EL GROUP BY

            ELSE

                SELECT COD_DVI
                INTO p_TIPO_DIV
                FROM PABS_DT_DETLL_NOMNA
                WHERE NUM_FOL_NMN = p_NUM_FOL_NMN
                AND COD_EST_AOS <> PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEELI
                AND ROWNUM = 1; -- SIRVE PARA NO UTILIZAR EL GROUP BY

            END IF;

        END IF;

        RETURN p_TIPO_DIV;

    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            p_TIPO_DIV:= NULL;
            RETURN p_TIPO_DIV;

        WHEN OTHERS THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_s_mensaje := err_code || '-' || err_msg;
            RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);

    END FN_PAB_BUS_DVI_NOMINA;

-- ***********************************************************************************************
-- Funcion/Procedimiento: FN_PAB_BUS_DVI_NOMINA_CON
-- Objetivo: Fusion que trae los el tipo de moneda de la nomina
-- Sistema: PAB
-- Base de Datos: DGBMSEGDB01_NGALTM
-- Tablas Usadas: PABS_DT_DETLL_NOMNA
-- Fecha: 21/06/16
-- Autor: Santander
-- Input:
-- p_NUM_FOL_NMN -> foliador de la nomina
-- p_FLG_DVI -> flag de multimoneda
-- Output:
-- N/A
-- Input/Output: N/A
-- Retorno: N/A.
-- Observaciones: <Fecha y Detalle de ?ltimos cambios>
-- ***********************************************************************************************
   FUNCTION FN_PAB_BUS_DVI_NOMINA_CON (p_FLG_DVI IN NUMBER, p_NUM_FOL_NMN IN NUMBER)
      RETURN VARCHAR2
   IS
      p_TIPO_DIV   VARCHAR2 (11);
      v_NOM_SP     VARCHAR2 (30) := 'FN_PAB_BUS_DVI_NOMINA_CON';
      V_EST_NOM     VARCHAR2(10);

     BEGIN

        SELECT COD_EST_AOS
        INTO V_EST_NOM
        FROM PABS_DT_CBCRA_NOMNA
        WHERE NUM_FOL_NMN = p_NUM_FOL_NMN
        AND COD_EST_AOS <> PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEELI;

        IF (p_FLG_DVI = PKG_PAB_CONSTANTES.v_FLG_DVI_MULTI) THEN
            p_TIPO_DIV := PKG_PAB_CONSTANTES.v_STR_DVI_MULTI;
        ELSE

            IF V_EST_NOM =  PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNELI  THEN--ELIMINADA

                SELECT COD_DVI
                INTO p_TIPO_DIV
                FROM PABS_DT_DETLL_NOMNA
                WHERE NUM_FOL_NMN = p_NUM_FOL_NMN
                AND ROWNUM = 1; -- SIRVE PARA NO UTILIZAR EL GROUP BY

            ELSE

                SELECT COD_DVI
                INTO p_TIPO_DIV
                FROM PABS_DT_DETLL_NOMNA
                WHERE NUM_FOL_NMN = p_NUM_FOL_NMN
                AND COD_EST_AOS <> PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEELI
                AND ROWNUM = 1; -- SIRVE PARA NO UTILIZAR EL GROUP BY

            END IF;
            --Una Moneda
            /*SELECT COD_DVI
            INTO p_TIPO_DIV
            FROM PABS_DT_DETLL_NOMNA            
            WHERE NUM_FOL_NMN = p_NUM_FOL_NMN
            AND ROWNUM = 1; -- SIRVE PARA NO UTILIZAR EL GROUP BY     */

        END IF;

        RETURN p_TIPO_DIV;

    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            p_TIPO_DIV:= NULL;
            RETURN p_TIPO_DIV;

        WHEN OTHERS THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_s_mensaje := err_code || '-' || err_msg;
            RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);

    END FN_PAB_BUS_DVI_NOMINA_CON;

-- ***********************************************************************************************
-- //@Santander
-- Funcion/Procedimiento: FN_PAB_BUS_DVI_NOMINA_CON_HIS
-- Objetivo: Fusion que trae los el tipo de moneda de la nomina
-- Sistema: PAB
-- Base de Datos: DGBMSEGDB01_NGALTM
-- Tablas Usadas: PABS_DT_DETLL_NOMNA
-- Fecha: 21/06/16
-- Autor: Santander
-- Input:
-- p_FLG_DVI     -> flag de multimoneda
-- p_NUM_FOL_NMN -> foliador de la nomina
-- P_FEC_CGA_HIS -> Fecha carga historica
-- Output:
-- N/A
-- Input/Output: N/A
-- Retorno:
-- p_TIPO_DIV
-- Observaciones: <Fecha y Detalle de ultimos cambios>
-- ***********************************************************************************************
   FUNCTION FN_PAB_BUS_DVI_NOMINA_CON_HIS (p_FLG_DVI IN NUMBER, p_NUM_FOL_NMN IN NUMBER, P_FEC_CGA_HIS DATE)
      RETURN VARCHAR2
   IS
      p_TIPO_DIV   VARCHAR2 (11);
      v_NOM_SP     VARCHAR2 (30) := 'FN_PAB_BUS_DVI_NOMINA_CON_HIS';
      V_EST_NOM     VARCHAR2(10);

     BEGIN

        SELECT COD_EST_AOS
        INTO V_EST_NOM
        FROM PABS_DT_CBCRA_NOMNA_HTRCA
        WHERE NUM_FOL_NMN = p_NUM_FOL_NMN
        AND FEC_CGA_HIS = P_FEC_CGA_HIS;

        IF (p_FLG_DVI = PKG_PAB_CONSTANTES.v_FLG_DVI_MULTI) THEN
            p_TIPO_DIV := PKG_PAB_CONSTANTES.v_STR_DVI_MULTI;
        ELSE

            --Una Moneda
            SELECT COD_DVI
            INTO p_TIPO_DIV
            FROM PABS_DT_DETLL_NOMNA_HTRCA
            WHERE NUM_FOL_NMN = p_NUM_FOL_NMN
            AND FEC_CGA_HIS = P_FEC_CGA_HIS
            AND ROWNUM = 1; -- SIRVE PARA NO UTILIZAR EL GROUP BY

        END IF;

        RETURN p_TIPO_DIV;

    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            p_TIPO_DIV:= NULL;
            RETURN p_TIPO_DIV;

        WHEN OTHERS THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_s_mensaje := err_code || '-' || err_msg;
            RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);

    END FN_PAB_BUS_DVI_NOMINA_CON_HIS;

--
-- ***********************************************************************************************
-- Funcion/Procedimiento: Sp_PAB_BUS_DET_NOMINA
-- Objetivo: Procedimiento almacenado que retorna todos las operaciones de una nomina que no se encuentren
-- eliminadas
-- Sistema: PAB
-- Base de Datos: DGBMSEGDB01_NGALTM
-- Tablas Usadas: PABS_DT_DETLL_NOMNA, PABS_DT_SISTM_ENTRD_SALID, PABS_DT_CBCRA_NOMNA, TCDT040, TCDTBAI
-- Fecha: 21/06/16
-- Autor: Santander
-- Input:
-- p_NUM_FOL_NMN -> Numero de la nomina
-- Output:
-- p_CURSOR -> Cursor con las nominas encontradas
-- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
-- Input/Output: N/A
-- Retorno: N/A.
-- Observaciones: <Fecha y Detalle de ?ltimos cambios>
-- ***********************************************************************************************
   PROCEDURE Sp_PAB_BUS_DET_NOMINA (p_NUM_FOL_NMN IN NUMBER, p_CURSOR OUT SYS_REFCURSOR, p_ERROR OUT NUMBER)
   IS
      v_NOM_SP   VARCHAR2 (30) := 'Sp_PAB_BUS_DET_NOMINA';
   BEGIN
      OPEN p_CURSOR FOR
        SELECT DETNOM.NUM_FOL_OPE AS NUMERO_OPERACION,
                  DETNOM.FEC_ISR_OPE AS F_INSERT_OPERACION,
                  DETNOM.IMp_OPE AS MONTO_TOTAL,
                  DETNOM.COD_BCO_DTN AS BIC_BAN_DEST,
                  DECODE (T40.TCNIFENT, NULL, 'BANCO EXTRANJERO', T40.TCNIFENT) AS RUT_BANCO,
                  TBAI.DES_BCO AS GLS_BAN_DEST,
                  DETNOM.NUM_CTA_BFC NUM_CTA_BFC,                                                                                                                                                                                                                                                                                                                                                                                   --DETNOM.NUM_CTA_BFC,
                  DETNOM.COD_DVI,
                  CABNOM.NUM_FOL_NMN,
                  DETNOM.FEC_VTA AS FECHA_VALUTA,
                  CABNOM.COD_USR,
                  ENT.DSC_SIS_ENT_SAL,
                  PKG_PAB_UTILITY.FN_PAB_TRAN_COD_EST_GLOSA(DETNOM.COD_EST_AOS, PKG_PAB_CONSTANTES.V_IND_OPE) AS EST_OPE, --llamar a la funcion
                  DETNOM.FLG_OPE_DUP,   --flag de duplicidad a nivel de operacion
                  DETNOM.GLS_ADC_EST   -- SE AGREGA LA GLOSA DEL ESTADO --HLL
             FROM PABS_DT_DETLL_NOMNA DETNOM,
                  PABS_DT_SISTM_ENTRD_SALID ENT,
                  PABS_DT_CBCRA_NOMNA CABNOM,
                  TCDT040 T40,
                  TCDTBAI TBAI
            WHERE DETNOM.NUM_FOL_NMN = p_NUM_FOL_NMN
              AND DETNOM.COD_BCO_DTN = TBAI.TGCDSWSA(+)
              AND DETNOM.COD_BCO_DTN = T40.TGCDSWSA(+)
              AND T40.TCNIFENT (+) <> v_BANCO
              AND CABNOM.NUM_FOL_NMN = DETNOM.NUM_FOL_NMN
              AND CABNOM.COD_SIS_ENT = ENT.COD_SIS_ENT_SAL
              AND DETNOM.COD_EST_AOS <> PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEELI
         ORDER BY DETNOM.COD_EST_AOS,DETNOM.NUM_FOL_OPE;
    --
      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
    --                                                                                                                                                                                                                                                                                                                                                                                                                                --Procedimiento ejecutado de forma correcta
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);
     --
   END Sp_PAB_BUS_DET_NOMINA;
--
-- ***********************************************************************************************
-- Funcion/Procedimiento: Sp_PAB_TOT_DVI_NMN_MULTI
-- Objetivo: Procedimiento que retorna el total de las operaciones agrupados por sus monedas
-- Sistema: PAB
-- Base de Datos: DGBMSEGDB01_NGALTM
-- Tablas Usadas: PABS_DT_CBCRA_NOMNA, PABS_DT_DETLL_NOMNA, PABS_DT_ESTDO_ALMOT
-- Fecha: 21/06/16
-- Autor: Santander
-- Input:
-- p_NUM_FOL_NMN -> Numero de la nomina
-- Output:
-- p_CURSOR -> SYS_REFCURSOR, resultados de consulta.
-- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
-- Input/Output: N/A
-- Retorno: N/A.
-- Observaciones: <Fecha y Detalle de ?ltimos cambios>
-- ***********************************************************************************************
   PROCEDURE Sp_PAB_TOT_DVI_NMN_MULTI (p_NUM_FOL_NMN IN NUMBER, p_CURSOR OUT SYS_REFCURSOR, p_ERROR OUT NUMBER)
   IS
      v_NOM_SP   VARCHAR2 (30) := 'Sp_PAB_TOT_DVI_NMN_MULTI';
   BEGIN
      OPEN p_CURSOR FOR
           SELECT NOMNA.FLG_DVI,
                  DETNOM.NUM_FOL_NMN,
                  SUM (DETNOM.IMp_OPE) IMp_TOT_NMN,
                  NOMNA.COD_USR COD_USR_CRC,
                  NOMNA.NOM_NMN,
                  DETNOM.COD_DVI,
                  ESTA.DSC_EST_AOS
             FROM PABS_DT_CBCRA_NOMNA NOMNA,
                  PABS_DT_DETLL_NOMNA DETNOM,
                  PABS_DT_ESTDO_ALMOT ESTA
            WHERE NOMNA.NUM_FOL_NMN = p_NUM_FOL_NMN
              AND NOMNA.NUM_FOL_NMN = DETNOM.NUM_FOL_NMN
              AND NOMNA.COD_EST_AOS = ESTA.COD_EST_AOS
              AND DETNOM.COD_EST_AOS <> PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEELI
         GROUP BY DETNOM.COD_DVI,
                  DETNOM.NUM_FOL_NMN,
                  NOMNA.COD_USR,
                  NOMNA.NOM_NMN,
                  NOMNA.FLG_DVI,
                  ESTA.DSC_EST_AOS;

      p_ERROR := PKG_PAB_CONSTANTES.v_OK;                                                                                                                                                                                                                                                                                                                                                                                                                                --Procedimiento ejecutado de forma correcta
   EXCEPTION
      WHEN DUP_VAL_ON_INDEX
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);

   END Sp_PAB_TOT_DVI_NMN_MULTI;
--
-- ***********************************************************************************************
-- Funcion/Procedimiento: Sp_PAB_GES_CAM_EST_NOMINA
-- Objetivo: Procedimiento que llama procedimiento que actualiza estado de nomina.
-- Sistema: PAB
-- Base de Datos: DGBMSEGDB01_NGALTM
-- Tablas Usadas: N/A
-- Fecha: 21/06/16
-- Autor: Santander
-- Input:
-- p_NUM_FOL_NMN -> arreglo con los numeros de foliador del banco
-- p_COD_EST_AOS -> codigo del estado con el cual se va a actualizar
-- p_COD_USR -> rut usuario
-- p_GLS_MTV_EST -> motivo o glosa de la actualizacion
-- Output:
-- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
-- Input/Output: N/A
-- Retorno: N/A.
-- Observaciones: <Fecha y Detalle de ultimos cambios>
-- ***********************************************************************************************
  PROCEDURE Sp_PAB_GES_CAM_EST_NOMINA( p_NUM_FOL_NMN    IN T_NUM_FOL_NMN,
                               p_COD_EST_AOS    IN CHAR,
                               p_COD_USR        IN VARCHAR2,
                               p_GLS_MTV_EST    IN VARCHAR2,
                               p_ERROR          OUT NUMBER
                             ) IS
    --
    v_NOM_SP VARCHAR2(31) :='Sp_PAB_GES_CAM_EST_NOMINA';

    --Variables de notificación.
    v_FLG_NTF     NUMBER;
    v_CAN_OPE_NOT NUMBER;
    p_ADJUNTO     NUMBER;
    p_ID          VARCHAR2 (10);
    p_MENSAJE_SWF VARCHAR2(300);
    p_ASUNTO      VARCHAR2(300);
    p_CORREO      VARCHAR2(300);
    err_perror exception;

    --
    BEGIN

        p_ERROR := PKG_PAB_CONSTANTES.v_OK;

        FOR i IN p_NUM_FOL_NMN.FIRST..p_NUM_FOL_NMN.LAST
        LOOP

            PKG_PAB_NOMINA.Sp_PAB_ACT_EST_CAB_NOMINA (p_NUM_FOL_NMN(i),p_COD_EST_AOS,p_COD_USR,p_GLS_MTV_EST,p_ERROR );

            --En caso de error no debemos continuar
            IF(p_ERROR = PKG_PAB_CONSTANTES.v_ERROR ) THEN
               RAISE err_perror;
            END IF;

            CASE
                WHEN p_COD_EST_AOS = 'AUT' AND v_cont_est_vis > 0 THEN

                    --Notificación del área de ingreso nómina
                    PKG_PAB_NOTIFICACION.Sp_PAB_GES_NTFCC_ALMOT (NULL, v_cont_est_vis, NULL, p_NUM_FOL_NMN(i), 'NAUT', PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SE_MESA, p_ERROR);

                    --Busca
                    PKG_PAB_NOTIFICACION.Sp_PAB_BUS_OPE_SIN_CBO (PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEPVI, v_FLG_NTF, v_CAN_OPE_NOT, p_ERROR);


                    PKG_PAB_NOTIFICACION.SP_PAB_DET_CRRO_TIBCO(NULL,
                                                                NULL,
                                                                NULL,
                                                                PKG_PAB_CONSTANTES.V_COD_TIP_CRO_PVIS,
                                                                v_CAN_OPE_NOT,-- v_cont_est_vis,
                                                                PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SE_MESA,
                                                                NULL,
                                                                NULL,
                                                                NULL,
                                                                p_ADJUNTO,
                                                                p_ID ,
                                                                p_MENSAJE_SWF,
                                                                p_ASUNTO,
                                                                p_CORREO,
                                                                p_ERROR);


                WHEN p_COD_EST_AOS = 'CUR' THEN
                    PKG_PAB_NOTIFICACION.Sp_PAB_GES_NTFCC_ALMOT (NULL, 0, NULL, p_NUM_FOL_NMN(i), 'NCUR', NULL, p_ERROR);
                WHEN p_COD_EST_AOS = 'REC' THEN
                    PKG_PAB_NOTIFICACION.Sp_PAB_GES_NTFCC_ALMOT (NULL, 0, NULL, p_NUM_FOL_NMN(i), 'NREC', NULL, p_ERROR);
            ELSE
                NULL; --No genera notificación
            END CASE;

            --reiniciamos
            v_cont_est_vis := 0;

            --En caso de error no debemos continuar
            IF(p_ERROR = PKG_PAB_CONSTANTES.v_ERROR ) THEN
                RAISE err_perror;
            END IF;

        END LOOP;

        COMMIT;

    EXCEPTION
        WHEN err_perror THEN
             err_code := SQLCODE;
             err_msg := 'Se detiene el proceso por error en procedimiento interno';
             PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code, v_NOM_PCK, err_msg, v_NOM_SP);
             p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
        WHEN OTHERS THEN

    ROLLBACK;
             err_code := SQLCODE;
             err_msg := SUBSTR (SQLERRM, 1, 300);
             PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code, v_NOM_PCK, err_msg, v_NOM_SP);
             p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
             p_s_mensaje := err_code || '-' || err_msg;
             RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);

    END Sp_PAB_GES_CAM_EST_NOMINA;
--
-- ***********************************************************************************************
-- Funcion/Procedimiento: Sp_PAB_ACT_EST_CAB_NOMINA
-- Objetivo: Procedimiento que actualiza el estado de la cabecera de una nomina y sus operaciones, registra en la bitacora los cambios
-- Sistema: PAB
-- Base de Datos: DGBMSEGDB01_NGALTM
-- Tablas Usadas: PABS_DT_CBCRA_NOMNA
-- Fecha: 21/06/16
-- Autor: Santander
-- Input:
-- p_NUM_FOL_NMN -> foliador del banco
-- p_COD_EST_AOS -> Identificador de nuevo estado.
-- p_COD_USR -> rut usuario
-- p_GLS_MTV_EST -> motivo o glosa de la actualizacion
-- Output:
-- p_ERROR -> p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
-- Input/Output: N/A
-- Retorno: N/A.
-- Observaciones: <Fecha y Detalle de ultimos cambios>
-- ***********************************************************************************************
  PROCEDURE Sp_PAB_ACT_EST_CAB_NOMINA (
                                    p_NUM_FOL_NMN   IN NUMBER,
                                    p_COD_EST_AOS   IN CHAR,
                                    p_COD_USR       IN VARCHAR2,
                                    p_GLS_MTV_EST   IN VARCHAR2,
                                    p_ERROR         OUT NUMBER
                                        ) IS
    --Seteo de Variables
    v_NOM_SP VARCHAR2(30):='Sp_PAB_ACT_EST_CAB_NOMINA';
    v_COD_EST_NOM NUMBER(5);
    v_COD_EST_OPE NUMBER(5);
    ERR_COD_INEX EXCEPTION;

    BEGIN
        p_ERROR := PKG_PAB_CONSTANTES.v_OK;

       --Seteo de variable estado
        CASE

         WHEN P_COD_EST_AOS = PKG_PAB_CONSTANTES.V_STR_EST_AOS_NMNVAL    -- Nomina Valida
         THEN
            V_COD_EST_NOM  := PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNVAL;
            V_COD_EST_OPE  := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEVAL;

         WHEN P_COD_EST_AOS = PKG_PAB_CONSTANTES.V_STR_EST_AOS_NMNCUR    -- Nomina Cursada
         THEN
            V_COD_EST_NOM  := PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNCUR;
            V_COD_EST_OPE  := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPECUR;

         WHEN P_COD_EST_AOS = PKG_PAB_CONSTANTES.V_STR_EST_AOS_NMNAUT    -- Nomina Autorizada
         THEN
            V_COD_EST_NOM  := PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNAUT;
            V_COD_EST_OPE  := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT;

         WHEN P_COD_EST_AOS = PKG_PAB_CONSTANTES.V_STR_EST_AOS_NMNREC    -- Nomina Rechazada
         THEN
            V_COD_EST_NOM  := PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNREC;
            V_COD_EST_OPE  := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEREC;

         WHEN P_COD_EST_AOS = PKG_PAB_CONSTANTES.V_STR_EST_AOS_NMNELI    -- Nomina Eliminada
         THEN
            V_COD_EST_NOM  := PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNELI;
            V_COD_EST_OPE  := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEELI;

         ELSE
             RAISE ERR_COD_INEX;
        END CASE;

    --Actualizamos el estado de la nomina
    BEGIN
        UPDATE
            PABS_DT_CBCRA_NOMNA
        SET
            COD_EST_AOS = v_COD_EST_NOM,
            GLS_MTV_EST = P_GLS_MTV_EST
        WHERE
            NUM_FOL_NMN = p_NUM_FOL_NMN;
    EXCEPTION
        WHEN OTHERS THEN
           err_code := SQLCODE;
           err_msg := SUBSTR (SQLERRM, 1, 300);
           PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code, v_NOM_PCK, err_msg, v_NOM_SP);
           p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
           RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);
    END;

    --Mandamos a actualizar las operaciones de la nomina
    PKG_PAB_NOMINA.Sp_PAB_REC_ACT_OPE_NOMINA(p_NUM_FOL_NMN,v_COD_EST_OPE,p_COD_USR,p_GLS_MTV_EST,p_ERROR);

    EXCEPTION
        WHEN ERR_COD_INEX THEN
            err_msg := 'Codigo inexistente:' || p_COD_EST_AOS;
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
        WHEN DUP_VAL_ON_INDEX THEN
            err_code := SQLCODE;
            err_msg := SUBSTR(SQLERRM, 1, 300);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
        WHEN OTHERS THEN
             err_code := SQLCODE;
             err_msg := SUBSTR (SQLERRM, 1, 300);
             PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code, v_NOM_PCK, err_msg, v_NOM_SP);
             p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
             p_s_mensaje := err_code || '-' || err_msg;
             RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);

    END Sp_PAB_ACT_EST_CAB_NOMINA;
--
-- ***********************************************************************************************
-- Funcion/Procedimiento: Sp_PAB_REC_ACT_OPE_NOMINA
-- Objetivo: Procedimiento que recorre todas las operaciones de una nomin, para luego llamar a otros
-- procedimientos que actualizan estado y graban la bitacora Sistema: PAB
-- Base de Datos: DGBMSEGDB01_NGALTM
-- Tablas Usadas: N/A
-- Fecha: 21/06/16
-- Autor: Santander
-- Input:
-- p_NUM_FOL_NMN -> foliador del banco
-- p_COD_EST_AOS -> Identificador de nuevo estado.
-- p_COD_USR -> identificador usuario
-- p_GLS_MTV_EST -> motivo o glosa de la actualizacion
-- Output:
-- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
-- Input/Output: N/A
-- Retorno: N/A.
-- Observaciones: <Fecha y Detalle de ultimos cambios>
-- ***********************************************************************************************
    PROCEDURE Sp_PAB_REC_ACT_OPE_NOMINA (
                                        p_NUM_FOL_NMN   IN NUMBER,
                                        p_COD_EST_AOS   IN NUMBER,
                                        p_COD_USR       IN VARCHAR2,
                                        p_GLS_MTV_EST   IN VARCHAR2,
                                        p_ERROR         OUT NUMBER
     ) IS
    --Seteo de Variables
    v_NOM_SP VARCHAR2(30) :='Sp_PAB_REC_ACT_OPE_NOMINA';
    v_RESULT NUMBER;
    --
    BEGIN

        p_ERROR := PKG_PAB_CONSTANTES.v_OK;

        BEGIN
            OPEN C_CONSULTA_NOMINA(p_NUM_FOL_NMN);
        EXCEPTION
            WHEN OTHERS THEN
                err_code := SQLCODE;
                err_msg := SUBSTR(SQLERRM, 1, 300);
                p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
                PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                RAISE_APPLICATION_ERROR(-20000,err_msg);
        END;

        BEGIN
            FETCH C_CONSULTA_NOMINA INTO R_CONSULTA_NOMINA;
        EXCEPTION
            WHEN OTHERS THEN
                err_code := SQLCODE;
                err_msg := SUBSTR(SQLERRM, 1, 300);
                p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
                PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);

                 --Verificamos si el cursor se encuentra abierto en caso de error.
                IF (C_CONSULTA_NOMINA %ISOPEN) THEN
                    CLOSE C_CONSULTA_NOMINA;
                END IF;

                RAISE_APPLICATION_ERROR(-20000,err_msg);
        END;

        WHILE C_CONSULTA_NOMINA%FOUND
        LOOP

            --Reiniciamos variable por cada operación
            v_est_ope_vis := 0;

            --Se actualiza el estado de la operacion de la nomina
            PKG_PAB_NOMINA.Sp_PAB_ACT_EST_OPE_NOMINA(R_CONSULTA_NOMINA.NUM_FOL_OPE,R_CONSULTA_NOMINA.FEC_ISR_OPE,p_COD_EST_AOS,p_GLS_MTV_EST,p_ERROR);

            --En caso de ser una autorizacion se debe traspasar a operacion
            IF p_COD_EST_AOS = PKG_PAB_CONSTANTES.v_COD_EST_AOS_OPEAUT THEN

                PKG_PAB_OPERACION.Sp_PAB_TRAS_NOM_OPE(R_CONSULTA_NOMINA.NUM_FOL_OPE,
                                                      R_CONSULTA_NOMINA.FEC_ISR_OPE,
                                                      p_COD_USR,
                                                      R_CONSULTA_NOMINA.COD_BCO_DTN,
                                                      R_CONSULTA_NOMINA.COD_MT_SWF,
                                                      R_CONSULTA_NOMINA.COD_TPO_OPE_AOS,
                                                      R_CONSULTA_NOMINA.COD_DVI,
                                                      p_ERROR);

            END IF;

            --- Condicion que considere texto de la glosa si la tiene y sino texto fijo
            IF  (NVL (p_GLS_MTV_EST, '#') = '#') THEN
                v_DES_BIT:= 'Cambio de estado de nomina';
            ELSE
                v_DES_BIT:= p_GLS_MTV_EST;
            END IF;

            --Si la operación no esta para visar se guarda la traza de autorización
            IF(v_est_ope_vis = 0) THEN
                --Se llama al procedimiento de bitacora para registrar el usuario que modifica el estado de la operacion
                PKG_PAB_TRACKING.Sp_PAB_INS_BIT_OPE(R_CONSULTA_NOMINA.NUM_FOL_OPE,R_CONSULTA_NOMINA.FEC_ISR_OPE,p_COD_USR,R_CONSULTA_NOMINA.COD_EST_AOS,p_COD_EST_AOS,v_DES_BIT,v_RESULT);
            END IF;

            FETCH C_CONSULTA_NOMINA INTO R_CONSULTA_NOMINA;

        END LOOP;

        CLOSE C_CONSULTA_NOMINA;

    EXCEPTION
        WHEN OTHERS THEN

         --Verificamos si el cursor se encuentra abierto en caso de error.
         IF (C_CONSULTA_NOMINA %ISOPEN) THEN
            CLOSE C_CONSULTA_NOMINA;
         END IF;

             err_code := SQLCODE;
             err_msg := SUBSTR (SQLERRM, 1, 300);
             PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code, v_NOM_PCK, err_msg, v_NOM_SP);
             p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
             p_s_mensaje := err_code || '-' || err_msg;
             RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);

    END Sp_PAB_REC_ACT_OPE_NOMINA;
--
-- ***********************************************************************************************
-- Funcion/Procedimiento: Sp_PAB_ACT_EST_OPE_NOMINA
-- Objetivo: Procedimiento que actualiza el estado de una operacion de una nomina.
-- Base de Datos: DGBMSEGDB01_NGALTM
-- Tablas Usadas: PABS_DT_DETLL_NOMNA
-- Fecha: 21/06/16
-- Autor: Santander
-- Input:
-- p_NUM_FOL_OPE -> foliador del banco, PK tabla operaciones nomina.
-- p_FEC_ISR_OPE -> Fecha de insercion de operacion en el sistema, PK tabla operaciones n?mina.
-- p_COD_EST_AOS -> Identificador de nuevo estado.
-- p_GLS_ADC_EST -> motivo o glosa de la actualizacion
-- Output:
-- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
-- Input/Output: N/A
-- Retorno: N/A.
-- Observaciones: <Fecha y Detalle de ultimos cambios>
-- ***********************************************************************************************
   PROCEDURE Sp_PAB_ACT_EST_OPE_NOMINA (
                            p_NUM_FOL_OPE   IN NUMBER,
                            p_FEC_ISR_OPE   IN DATE,
                            p_COD_EST_AOS   IN NUMBER,
                            p_GLS_ADC_EST   IN VARCHAR2,
                            p_ERROR         OUT NUMBER
     ) IS

    v_NOM_SP VARCHAR2(100):='Sp_PAB_ACT_EST_OPE_NOMINA';

    BEGIN
        p_ERROR := PKG_PAB_CONSTANTES.v_OK;

        --Actualizamos la operacion
        UPDATE
            PABS_DT_DETLL_NOMNA DET
        SET COD_EST_AOS = p_COD_EST_AOS
        , GLS_ADC_EST = p_GLS_ADC_EST
        , FEC_ACT_NMN = SYSDATE -- Fecha actualización para dashborad
        WHERE
            NUM_FOL_OPE = p_NUM_FOL_OPE
        AND FEC_ISR_OPE = p_FEC_ISR_OPE;

        COMMIT;

    EXCEPTION
        WHEN DUP_VAL_ON_INDEX THEN

            err_code := SQLCODE;
            err_msg := SUBSTR(SQLERRM, 1, 300);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);

        WHEN OTHERS THEN
             err_code := SQLCODE;
             err_msg := SUBSTR (SQLERRM, 1, 300);
             PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code, v_NOM_PCK, err_msg, v_NOM_SP);
             p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
             p_s_mensaje := err_code || '-' || err_msg;
             RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);

    END Sp_PAB_ACT_EST_OPE_NOMINA;
--
-- ***********************************************************************************************
-- Funcion/Procedimiento: Sp_PAB_BUS_DET_OPE_NOM
-- Objetivo: Procedimiento almacenado que retorna el detalle de una operacion seleccionada
-- Base de Datos: DGBMSEGDB01_NGALTM
-- Tablas Usadas: PABS_DT_DETLL_NOMNA
-- Fecha: 21/06/16
-- Autor: Santander
-- Input:
-- p_NUM_FOL_OPE->   foliador del banco, PK tabla operaciones nomina.
-- p_FEC_ISR_OPE->   Fecha de insercion de operacion en el sistema, PK tabla operaciones nomina.
-- Output:
-- p_CURSOR -> Informacion de la operacion
-- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
-- Input/Output: N/A
-- Retorno: N/A.
-- Observaciones: <Fecha y Detalle de ?ltimos cambios>
-- ***********************************************************************************************
PROCEDURE Sp_PAB_BUS_DET_OPE_NOM (p_NUM_FOL_OPE     IN NUMBER,
                                  p_FEC_ISR_OPE     IN DATE,
                                  p_CURSOR          OUT SYS_REFCURSOR,
                                  p_ERROR           OUT NUMBER
                                 ) IS
     v_NOM_SP        VARCHAR2(30)    :=  'Sp_PAB_BUS_DET_OPE_NOM';

    BEGIN

        OPEN p_CURSOR FOR
                 SELECT
                        DETOPE.NUM_FOL_OPE          AS NUM_OPE,
                        DETOPE.NUM_REF_SWF          AS REF_SWIFT,
                        DETOPE.COD_TPO_OPE_AOS      AS TIPO_OPERACION,
                        DETOPE.NUM_REF_EXT          AS REF_EXT_SWIFT,
                        DETOPE.COD_MT_SWF           AS COD_MT,
                        DETOPE.COD_DVI              AS DIVISA,
                        SAL.DSC_SIS_ENT_SAL         AS CANAL_SALIDA,
                        DETOPE.COD_BCO_DTN          AS BIC_DESTINO,
                        DETOPC.GLS_DIR_ODN          AS DIRECCION_ORDENANTE,
                        NVL2(DETOPC.NUM_DOC_ODN, TRIM(DETOPC.NUM_DOC_ODN) || '-' || DETOPC.COD_TPD_ODN,NULL) AS DOC_ORDENANTE,
                        DETOPC.NOM_ODN              AS NOMBRE_ORDENANTE,
                        DETOPC.NUM_CTA_ODN          AS CTA_ORDENANTE,
                        DETOPC.NOM_CDD_ODN          AS CIUDAD_ORDENANTE,
                        DETOPC.COD_PAS_ODN          AS COD_PAIS_ORDENANTE,
                        PKG_PAB_UTILITY.FN_PAB_COMBO_PAIS(DETOPC.COD_PAS_ODN) AS PAIS_ORDENANTE,
                        DETOPC.NUM_CTA_DCv_ODN      AS CTA_DCv_ORDENANTE,
                        DETOPC.COD_BCO_BFC          AS BIC_BENEFICIARIO,
                        NVL2(DETOPE.NUM_DOC_BFC, TRIM(DETOPE.NUM_DOC_BFC) || '-' || DETOPE.COD_TPD_BFC,NULL) AS DOC_BENEFICIARIO,
                        DETOPE.NOM_BFC              AS NOMBRE_BENEFICIARIO,
                        DETOPE.NUM_CTA_BFC          AS CTA_BENEFICIARIO,
                        DETOPC.GLS_DIR_BFC          AS DIRECCION_BENEFICIARIO,
                        DETOPC.NOM_CDD_BFC          AS CIUDAD_BENEFICIARIO,
                        DETOPC.COD_PAS_BFC          AS COD_PAIS_BENEFICIARIO,
                        PKG_PAB_UTILITY.FN_PAB_COMBO_PAIS(DETOPC.COD_PAS_BFC) AS PAIS_BENEFICIARIO,
                        DETOPC.NUM_CTA_DCv_BFC      AS CTA_DCv_BENEFICIARIO,
                        DETOPC.COD_BCO_ITD          AS BIC_INTERMEDIARIO,
                        DETOPE.IMp_OPE              AS MONTO_OPE,
                        DETOPE.COD_SUC              AS COD_SUCURSAL,
                        --PKG_PAB_UTILITY.Fn_PAB_COMBO_SUCURSAL (DETOPE.COD_SUC) SUCURSAL, --agregado
                        DETOPE.COD_SUC              AS SUCURSAL,
                        DETOPC.FEC_TPO_OPE_CCLV     AS FECHA_CCLV,
                        DETOPC.COD_TPO_CMA          AS COD_TIPO_CAMARA,
                        PKG_PAB_UTILITY.Fn_PAB_COMBO_CAMARA(DETOPC.COD_TPO_CMA) AS TIPO_CAMARA,
                        DETOPC.NUM_CLv_NGC          AS CLAVE_NEGOCIO,
                        DETOPE.FEC_VTA              AS FECHA_VALUTA,
                        DETOPE.NUM_REF_CTB          AS REF_CONTABLE,
                        DETOPC.COD_TPO_SDO          AS COD_TIPO_SALDO,
                        PKG_PAB_UTILITY.Fn_PAB_COMBO_TSALDO(DETOPC.COD_TPO_SDO) AS TIPO_SALDO,
                        DETOPC.NUM_AGT              AS NUM_AGENTE,
                        DETOPC.COD_FND_CCLV         AS FONDO_CCLV,
                        DETOPC.COD_TPO_FND          AS CODIGO_FONDO,
                        PKG_PAB_UTILITY.FN_PAB_TRAN_COD_FONDO(DETOPC.COD_TPO_FND) AS GLOSA_FONDO,
                        DETOPC.OBS_OPC_SWF          AS OBSERVACION_SWIFT,
                        -------------------------------------------------
                        ESTALM.DSC_EST_AOS          AS ESTADO,
                        DETOPE.FEC_ISR_OPE          AS FEC_INS,
                        DETOPE.NUM_FOL_NMN          AS NUM_NOM,
                        DETOPE.NUM_OPE_SIS_ENT      AS NUM_OPE_ENT,
                        DECODE(DETOPE.FLG_EGR_ING, PKG_PAB_CONSTANTES.v_FLG_EGR,
                                                   PKG_PAB_CONSTANTES.v_STR_FLG_EGR,
                                                   PKG_PAB_CONSTANTES.v_STR_FLG_ING)  AS EGRESO_INGRESO,
                        DETOPE.COD_SIS_SAL          AS COD_CANAL_ENT,
                        ENT.DSC_SIS_ENT_SAL         AS CANAL_ENTRADA,
                        DETOPE.COD_SIS_SAL          AS COD_CANAL_SAL,
                        DECODE(DETOPE.COD_TPO_ING,PKG_PAB_CONSTANTES.v_COD_TIp_ING_MAS,
                                                  PKG_PAB_CONSTANTES.v_STR_TIp_ING_MAS,
                                                  PKG_PAB_CONSTANTES.V_COD_TIP_ING_UNI,
                                                  PKG_PAB_CONSTANTES.V_STR_TIP_ING_UNI,
                                                  PKG_PAB_CONSTANTES.V_COD_TIP_ING_DIR,
                                                  PKG_PAB_CONSTANTES.V_STR_TIP_ING_DIR
                                                  ) AS TIPO_INGRESO,
                        DETOPE.FEC_ENv_RCp_SWF      AS FECHA_SWIFT,
                        DETOPE.HOR_ENv_RCp_SWF      AS HORA_SWIFT,
                        DECODE(DETOPE.FLG_MRD_UTZ_SWF,PKG_PAB_CONSTANTES.v_FLG_MRD_MN,
                                                  PKG_PAB_CONSTANTES.v_STR_FLG_MRD_MN,
                                                  PKG_PAB_CONSTANTES.v_STR_FLG_MRD_MX) AS MERCADO,
                        DECODE(DETOPE.COD_EST_AOS,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEREC,DETOPC.GLS_EST_RCH,
                                                  PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT,DETOPC.GLS_MTv_VSD,
                                                  DETOPE.GLS_ADC_EST) AS GLOSA_ESTADO,
                        DETOPC.NUM_DOC_BFC          AS DOC_BENEFICIARIO_OPC,
                        DETOPC.COD_TPD_BFC          AS TIPO_DOC_BENEFICIARIO_OPC,
                        DETOPC.NUM_CLv_IDF          AS NUMERO_IDENTIFICATORIO,
                        DETOPC.GLS_EST_RCH          AS GLOSA_RECHAZO,
                        DETOPC.GLS_MTv_VSD          AS GLOSA_VISADA
                  FROM PABS_DT_DETLL_NOMNA DETOPE,
                       PABS_DT_NOMNA_INFCN_OPCON DETOPC,
                       PABS_DT_ESTDO_ALMOT ESTALM,
                       PABS_DT_SISTM_ENTRD_SALID ENT,
                       PABS_DT_SISTM_ENTRD_SALID SAL,
                       PABS_DT_CBCRA_NOMNA CABNOM
                  WHERE DETOPE.NUM_FOL_OPE = p_NUM_FOL_OPE
                  AND DETOPE.FEC_ISR_OPE = p_FEC_ISR_OPE
                  AND DETOPE.NUM_FOL_OPE = DETOPC.NUM_FOL_OPE(+)
                  AND DETOPE.FEC_ISR_OPE = DETOPC.FEC_ISR_OPE(+)
                  AND DETOPE.COD_EST_AOS = ESTALM.COD_EST_AOS
                  AND DETOPE.COD_SIS_SAL = SAL.COD_SIS_ENT_SAL(+)
                  AND CABNOM.NUM_FOL_NMN = DETOPE.NUM_FOL_NMN
                  AND CABNOM.COD_SIS_ENT = ENT.COD_SIS_ENT_SAL;

                p_ERROR := PKG_PAB_CONSTANTES.v_OK; --Procedimiento ejecutado de forma correcta

    EXCEPTION

            WHEN NO_DATA_FOUND THEN

                    err_code := SQLCODE;
                    err_msg := SUBSTR(SQLERRM, 1, 300);
                    PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                    p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;

            WHEN OTHERS THEN

                    err_code := SQLCODE;
                    err_msg := SUBSTR(SQLERRM, 1, 300);
                    PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                    p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                    p_s_mensaje := err_code || '-' || err_msg;
                    RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);

    END Sp_PAB_BUS_DET_OPE_NOM;
--
-- ***********************************************************************************************
-- Funcion/Procedimiento: Sp_PAB_ACT_DATOS_OPE_NOM
-- Objetivo:  Procedimiento que actualiza informacion de una operacion de una nomina
-- Base de Datos: DGBMSEGDB01_NGALTM
-- Tablas Usadas: PABS_DT_DETLL_NOMNA
-- Fecha: 21/06/16
-- Autor: Santander
-- Input:
-- p_NUM_FOL_NMN -> foliador del banco
-- p_FEC_ISR_OPE -> Fecha de insercion de la operacion
-- p_NUM_REF_SWF -> Numero de referencia del swift
-- p_FEC_ENv_RCp_SWF -> Fecha Swift
-- p_HOR_ENv_RCp_SWF -> Hora Swift
-- p_GLS_ADC_EST  ->Glosa Adicional Estado
-- p_GLS_EST_RCH  -> Glosa Estado Rechazado
-- p_GLS_MTv_VSD  -> Glosa Motivo Visacion
-- Output:
-- p_ERROR -> p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
-- Input/Output: N/A
-- Retorno: N/A.
-- Observaciones: <Fecha y Detalle de ?ltimos cambios>
-- ***********************************************************************************************
  PROCEDURE Sp_PAB_ACT_DATOS_OPE_NOM (
                    p_NUM_FOL_OPE      IN NUMBER,
                    p_FEC_ISR_OPE      IN DATE,
                    p_NUM_REF_SWF      IN VARCHAR2,
                    p_FEC_ENv_RCp_SWF  IN DATE,
                    p_HOR_ENv_RCp_SWF  IN NUMBER,
                    p_GLS_ADC_EST      IN VARCHAR2,
                    p_GLS_EST_RCH      IN VARCHAR2,
                    p_GLS_MTv_VSD      IN VARCHAR2,
                    p_ERROR            OUT NUMBER)
IS
    v_NOM_SP           VARCHAR2(30) := 'Sp_PAB_ACT_DATOS_OPE_NOM';

    BEGIN

        BEGIN

            --Actualizamos  Operacion nomina
            UPDATE PABS_DT_DETLL_NOMNA DETNOM
            SET NUM_REF_SWF = p_NUM_REF_SWF,
                FEC_ENv_RCp_SWF = p_FEC_ENv_RCp_SWF,
                HOR_ENv_RCp_SWF = p_HOR_ENv_RCp_SWF,
                GLS_ADC_EST = p_GLS_ADC_EST
            WHERE DETNOM.NUM_FOL_OPE = p_NUM_FOL_OPE
              AND DETNOM.FEC_ISR_OPE = p_FEC_ISR_OPE;

        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                err_code := SQLCODE;
                err_msg := SUBSTR(SQLERRM, 1, 300);
                PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                RAISE;

            WHEN OTHERS THEN
                err_code := SQLCODE;
                err_msg := SUBSTR(SQLERRM, 1, 300);
                PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                p_s_mensaje := err_code || '-' || err_msg;
                RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);

        END;

        --Actualizamos informacion opcional de la operacion
        IF( NVL (p_GLS_EST_RCH, '#') <> '#' OR NVL (p_GLS_MTv_VSD, '#') <> '#' ) THEN

            BEGIN

                --
                UPDATE PABS_DT_NOMNA_INFCN_OPCON OPEOP
                SET GLS_EST_RCH = p_GLS_EST_RCH,
                    GLS_MTv_VSD = p_GLS_MTv_VSD
                WHERE OPEOP.NUM_FOL_OPE = p_NUM_FOL_OPE
                  AND OPEOP.FEC_ISR_OPE = p_FEC_ISR_OPE;

            EXCEPTION

                WHEN NO_DATA_FOUND THEN
                    err_code := SQLCODE;
                    err_msg := SUBSTR(SQLERRM, 1, 300);
                    PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                    p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                    RAISE;

                WHEN OTHERS THEN
                    err_code := SQLCODE;
                    err_msg := SUBSTR(SQLERRM, 1, 300);
                    PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                    p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                    p_s_mensaje := err_code || '-' || err_msg;
                    RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);
            END;

        END IF;

    COMMIT;

    EXCEPTION

        WHEN OTHERS THEN
            err_code := SQLCODE;
            err_msg := SUBSTR(SQLERRM, 1, 300);
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
            p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
            p_s_mensaje := err_code || '-' || err_msg;
            RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);

    END Sp_PAB_ACT_DATOS_OPE_NOM;

-- ***********************************************************************************************
-- Funcion/Procedimiento: Sp_PAB_ACT_OPE_NOM
-- Objetivo: Procedimiento almacenado que actualiza informacion de una operaciones de una nomina
-- Sistema: PAB
-- Base de Datos: DGBMSEGDB01_NGALTM
-- Tablas Usadas: PABS_DT_DETLL_NOMNA, PABS_DT_NOMNA_INFCN_OPCON
-- Fecha: 21/06/16
-- Autor: Santander
-- Input:
-- p_NUM_FOL_OPE      IN NUMBER,   -> Numero de la Operacion
-- p_FEC_ISR_OPE      IN DATE,     -> Fecha de insercion de la operacion
-- p_COD_USR          IN CHAR,     -> Usuario que realiza la modificacion
-- p_NUM_DOC_ODN      IN CHAR,     -> Rut del ordenante
-- p_COD_TPD_ODN      IN CHAR,     -> Digito Verificador del ordenante
-- p_NOM_ODN          IN VARCHAR2, -> Nombre Ordenate
-- p_NUM_CTA_ODN      IN VARCHAR2, -> Numero cuenta ordenante
-- p_GLS_DIR_ODN      IN VARCHAR2, -> Glosa direccion ordenante
-- p_NOM_CDD_ODN      IN VARCHAR2, -> nombre ciudad ordenante
-- p_COD_PAS_ODN      IN CHAR,     -> codigo pais ordenante
-- p_NUM_CTA_DCV_ODN  IN VARCHAR2, -> numero cuenta dcv ordenante
-- p_COD_BCO_DTN      IN CHAR,     -> codigo bic banco destino
-- p_NUM_DOC_BFC      IN CHAR,     -> rut del beneficiario
-- p_COD_TPD_BFC      IN CHAR,     -> Digito Verificador del beneficiario
-- p_NOM_BFC          IN VARCHAR2, -> Nombre beneficiario
-- p_NUM_CTA_BFC      IN VARCHAR2, -> Numero cuenta beneficiario
-- p_GLS_DIR_BFC      IN VARCHAR2, -> Glosa direccion beneficiario
-- p_NOM_CDD_BFC      IN VARCHAR2, -> nombre ciudad beneficiario
-- p_COD_PAS_BFC      IN CHAR,     -> codigo pais beneficiario
-- p_NUM_CTA_DCV_BFC  IN VARCHAR2, -> numero cuenta dcv beneficiario
-- p_COD_BCO_ITD      IN CHAR,      -> codigo bic banco intermediario
-- p_COD_BCO_BFC      IN CHAR,      -> codigo bic banco corresponsal
-- p_IMP_OPE          IN NUMBER,      -> monto operacion
-- p_FEC_VTA          IN DATE,     -> fecha de valuta
-- p_COD_SUC          IN VARCHAR2, -> codigo sucursal
-- p_NUM_REF_CTB      IN CHAR,      -> numero referencia contable
-- p_FEC_TPO_OPE_CCLV IN DATE,     -> fecha cclv
-- p_COD_TPO_SDO      IN CHAR,      -> tipo saldo
-- p_COD_TPO_CMA      IN CHAR,      -> tipo camara
-- p_NUM_AGT          IN CHAR,      -> numero agente
-- p_NUM_CLV_NGC      IN VARCHAR2, -> numero clave de negocio
-- p_COD_TPO_FND      IN CHAR,     -> tipo fondo
-- p_NUM_CLv_IDF      IN VARCHAR2, -> numero clave identificatorio
-- P_NUM_OPE_SIS_ENT  IN CHAR,     -> numero operacion origen
-- p_OBS_OPC_SWF      IN VARCHAR2, -> glosa swift
-- Output:
-- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
-- Input/Output: N/A
-- Retorno: N/A.
-- Observaciones: <Fecha y Detalle de ultimos cambios>
-- ***********************************************************************************************
PROCEDURE Sp_PAB_ACT_OPE_NOM (  p_NUM_FOL_OPE      IN NUMBER,
                                p_FEC_ISR_OPE      IN DATE,
                                p_COD_USR          IN CHAR,
                                ------Ordenante---------------------
                                p_NUM_DOC_ODN      IN CHAR,
                                p_COD_TPD_ODN      IN CHAR,
                                p_NOM_ODN          IN VARCHAR2,
                                p_NUM_CTA_ODN      IN VARCHAR2,
                                p_GLS_DIR_ODN      IN VARCHAR2,
                                p_NOM_CDD_ODN      IN VARCHAR2,
                                p_COD_PAS_ODN      IN CHAR,
                                p_NUM_CTA_DCV_ODN  IN VARCHAR2,
                                --------Beneficiario-----------------
                                p_COD_BCO_DTN      IN CHAR,
                                p_NUM_DOC_BFC      IN CHAR,
                                p_COD_TPD_BFC      IN CHAR,
                                p_NOM_BFC          IN VARCHAR2,
                                p_NUM_CTA_BFC      IN VARCHAR2,
                                p_GLS_DIR_BFC      IN VARCHAR2,
                                p_NOM_CDD_BFC      IN VARCHAR2,
                                p_COD_PAS_BFC      IN CHAR,
                                p_NUM_CTA_DCV_BFC  IN VARCHAR2,
                                --------Corresponsales-------------------
                                p_COD_BCO_ITD      IN CHAR,
                                p_COD_BCO_BFC      IN CHAR,
                                --------Operacion------------------------
                                p_IMP_OPE          IN NUMBER,
                                p_FEC_VTA          IN DATE,
                                p_COD_SUC          IN VARCHAR2,
                                p_NUM_REF_CTB      IN CHAR,
                                p_FEC_TPO_OPE_CCLV IN DATE,
                                p_COD_TPO_SDO      IN CHAR,
                                p_COD_TPO_CMA      IN CHAR,
                                p_NUM_AGT          IN CHAR,
                                p_NUM_CLV_NGC      IN VARCHAR2 ,
                                p_COD_TPO_FND      IN CHAR,
                                p_NUM_CLv_IDF      IN VARCHAR2,
                                P_NUM_OPE_SIS_ENT  IN CHAR,
                                p_OBS_OPC_SWF      IN VARCHAR2,
                                p_ERROR OUT NUMBER) IS

       v_NOM_SP   VARCHAR2 (30) := 'Sp_PAB_ACT_OPE_NOM';
       v_DSC_GLB_BTC VARCHAR2(100);
       v_COD_EST_OPE_INI NUMBER;
       v_COD_EST_OPE_FIN NUMBER;

   BEGIN

        BEGIN
              UPDATE PABS_DT_DETLL_NOMNA   DETN
              SET
                     COD_BCO_DTN     = p_COD_BCO_DTN,
                     NUM_DOC_BFC     = p_NUM_DOC_BFC,
                     COD_TPD_BFC     = p_COD_TPD_BFC,
                     NOM_BFC         = NVL(p_NOM_BFC,' '),
                     NUM_CTA_BFC     = p_NUM_CTA_BFC,
                     IMP_OPE         = p_IMP_OPE,
                     FEC_VTA         = p_FEC_VTA,
                     COD_SUC         = p_COD_SUC,
                     NUM_REF_CTB     = p_NUM_REF_CTB,
                     NUM_OPE_SIS_ENT = P_NUM_OPE_SIS_ENT
              WHERE  NUM_FOL_OPE     = p_NUM_FOL_OPE AND
                     FEC_ISR_OPE     = p_FEC_ISR_OPE;

        EXCEPTION
            WHEN DUP_VAL_ON_INDEX
            THEN
               err_code := SQLCODE;
               err_msg := SUBSTR (SQLERRM, 1, 300);
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            WHEN OTHERS
            THEN
               err_code := SQLCODE;
               err_msg := SUBSTR (SQLERRM, 1, 300);
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
               p_s_mensaje := err_code || '-' || err_msg;
               RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);
        END;

        BEGIN
            UPDATE PABS_DT_NOMNA_INFCN_OPCON  DETNINF
            SET    NUM_DOC_ODN      = p_NUM_DOC_ODN,
                   COD_TPD_ODN      = p_COD_TPD_ODN,
                   NOM_ODN          = p_NOM_ODN,
                   NUM_CTA_ODN      = p_NUM_CTA_ODN,
                   GLS_DIR_ODN      = p_GLS_DIR_ODN,
                   NOM_CDD_ODN      = p_NOM_CDD_ODN,
                   COD_PAS_ODN      = p_COD_PAS_ODN,
                   NUM_CTA_DCV_ODN  = p_NUM_CTA_DCV_ODN,
                   NUM_DOC_BFC      = p_NUM_DOC_BFC,
                   COD_TPD_BFC      = p_COD_TPD_BFC,
                   GLS_DIR_BFC      = p_GLS_DIR_BFC,
                   NOM_CDD_BFC      = p_NOM_CDD_BFC,
                   COD_PAS_BFC      = NVL(p_COD_PAS_BFC,COD_PAS_BFC),
                   NUM_CTA_DCV_BFC  = p_NUM_CTA_DCV_BFC,
                   COD_BCO_ITD      = p_COD_BCO_ITD,
                   COD_BCO_BFC      = p_COD_BCO_BFC,
                   FEC_TPO_OPE_CCLV = p_FEC_TPO_OPE_CCLV,
                   COD_TPO_SDO      = NVL(p_COD_TPO_SDO,COD_TPO_SDO),
                   COD_TPO_CMA      = NVL(p_COD_TPO_CMA,COD_TPO_CMA),
                   NUM_AGT          = p_NUM_AGT,
                   NUM_CLV_NGC      = p_NUM_CLV_NGC,
                   COD_TPO_FND      = NVL(p_COD_TPO_FND,COD_TPO_FND),
                   NUM_CLv_IDF      = p_NUM_CLv_IDF,
                   OBS_OPC_SWF      = p_OBS_OPC_SWF
            WHERE  NUM_FOL_OPE      = p_NUM_FOL_OPE  AND
                   FEC_ISR_OPE      = p_FEC_ISR_OPE ;

        EXCEPTION
            WHEN DUP_VAL_ON_INDEX
            THEN
               err_code := SQLCODE;
               err_msg := SUBSTR (SQLERRM, 1, 300);
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            WHEN OTHERS
            THEN
               err_code := SQLCODE;
               err_msg := SUBSTR (SQLERRM, 1, 300);
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
               p_s_mensaje := err_code || '-' || err_msg;
               RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);
        END;


        IF (NVL (p_NUM_DOC_BFC, '#') <> '#' AND NVL (p_COD_TPD_BFC, '#') <> '#') THEN
            --Llamamos a mantenedor de beneficiario ordenante
            PKG_PAB_UTILITY.Sp_PAB_MAN_BFC_ODN (p_NUM_DOC_BFC, p_COD_TPD_BFC);

            --Cuenta Beneficiario
            IF (NVL (p_COD_BCO_DTN, '#') <> '#' AND NVL (p_NUM_CTA_BFC, '#') <> '#') THEN

                --Verificamos si existe la cuenta del beneficiario
                PKG_PAB_UTILITY.Sp_PAB_MAN_CTA_BFC_ODN (p_NUM_DOC_BFC,
                                                        p_COD_TPD_BFC,
                                                        p_NUM_CTA_BFC,
                                                        PKG_PAB_CONSTANTES.v_FLG_DCv_NO,
                                                        p_COD_BCO_DTN);
            END IF;

            --Cuenta Beneficiario DCV
            IF (NVL (p_COD_BCO_DTN, '#') <> '#' AND NVL (p_NUM_CTA_DCV_BFC, '#') <> '#') THEN

                --Verificamos si existe la cuenta del beneficiario
                PKG_PAB_UTILITY.Sp_PAB_MAN_CTA_BFC_ODN (p_NUM_DOC_BFC,
                                                        p_COD_TPD_BFC,
                                                        p_NUM_CTA_DCV_BFC,
                                                        PKG_PAB_CONSTANTES.V_FLG_DCV_SI,
                                                        p_COD_BCO_DTN);
            END IF;

        END IF;

        --Ordenante
        IF (NVL (p_NUM_DOC_ODN, '#') <> '#' AND NVL (p_COD_TPD_ODN, '#') <> '#') THEN
            PKG_PAB_UTILITY.Sp_PAB_MAN_BFC_ODN (p_NUM_DOC_ODN, p_COD_TPD_ODN);

            --Cuenta Ordenante
            IF (NVL (PKG_PAB_CONSTANTES.V_BIC_BANCO_SANTANDER, '#') <> '#' AND NVL (p_NUM_CTA_ODN, '#') <> '#') THEN

                --Verificamos si existe la cuenta del beneficiario
                PKG_PAB_UTILITY.Sp_PAB_MAN_CTA_BFC_ODN (p_NUM_DOC_ODN,
                                                        p_COD_TPD_ODN,
                                                        p_NUM_CTA_ODN,
                                                        PKG_PAB_CONSTANTES.v_FLG_DCv_NO,
                                                        PKG_PAB_CONSTANTES.V_BIC_BANCO_SANTANDER);
            END IF;

            --Cuenta Ordenante DCV
            IF (NVL (PKG_PAB_CONSTANTES.V_BIC_BANCO_SANTANDER, '#') <> '#' AND NVL (p_NUM_CTA_DCV_ODN, '#') <> '#') THEN

                --Verificamos si existe la cuenta del beneficiario
                PKG_PAB_UTILITY.Sp_PAB_MAN_CTA_BFC_ODN (p_NUM_DOC_ODN,
                                                        p_COD_TPD_ODN,
                                                        p_NUM_CTA_DCV_ODN,
                                                        PKG_PAB_CONSTANTES.V_FLG_DCV_SI,
                                                        PKG_PAB_CONSTANTES.V_BIC_BANCO_SANTANDER);
            END IF;

        END IF;

        --
        v_DSC_GLB_BTC := 'Se edito la operacion';
        v_COD_EST_OPE_INI := PKG_PAB_NOMINA.Fn_PAB_BUSCA_EST_OPE(p_NUM_FOL_OPE, p_FEC_ISR_OPE);
        v_COD_EST_OPE_FIN := PKG_PAB_NOMINA.Fn_PAB_BUSCA_EST_OPE(p_NUM_FOL_OPE, p_FEC_ISR_OPE);

        --Se llama al procedimiento de bitacora para registrar el usuario que modifica el estado de la operacion
        PKG_PAB_TRACKING.Sp_PAB_INS_BIT_OPE (p_NUM_FOL_OPE, p_FEC_ISR_OPE, p_COD_USR, v_COD_EST_OPE_INI, v_COD_EST_OPE_FIN, v_DSC_GLB_BTC, p_ERROR);

        p_ERROR := PKG_PAB_CONSTANTES.v_OK;  --Procedimiento ejecutado de forma correcta

        COMMIT;

   EXCEPTION
      WHEN DUP_VAL_ON_INDEX
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);
END Sp_PAB_ACT_OPE_NOM;


-- ***********************************************************************************************
-- Funcion/Procedimiento: Sp_PAB_GES_CAM_EST_DUPLI
-- Objetivo: Procedimiento almacenado que recibe las operaciones a actualizar su estado de duplicada por no duplicada
-- Sistema: PAB
-- Base de Datos: DGBMSEGDB01_NGALTM
-- Tablas Usadas: PABS_DT_DETLL_NOMNA
-- Fecha: 21/06/16
-- Autor: Santander
-- Input:
-- p_REG_OPER -> Numero de Operaciones
-- p_NUM_FOL_NMN -> Numero de la nomina
-- p_COD_USR -> Numero del usuario
-- Output:
-- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
-- Input/Output: N/A
-- Retorno: N/A.
-- Observaciones: <Fecha y Detalle de ultimos cambios>
-- ***********************************************************************************************
   PROCEDURE Sp_PAB_GES_CAM_EST_DUPLI (   p_REG_OPER      IN REG_OPER,
                                          p_NUM_FOL_NMN   IN NUMBER,
                                          p_COD_USR       IN CHAR,
                                          p_ERROR        OUT NUMBER)  IS
       v_NOM_SP   VARCHAR2 (30) := 'Sp_PAB_GES_CAM_EST_DUPLI';
   BEGIN
        p_ERROR := PKG_PAB_CONSTANTES.v_OK;

        IF p_REG_OPER.COUNT > 0 THEN
            FOR i IN p_REG_OPER.FIRST .. p_REG_OPER.LAST
                LOOP

                    Sp_PAB_ACT_OPE_DUP(
                                     p_REG_OPER(i).FEC_ISR_OPE,
                                     p_REG_OPER(i).NUM_FOL_OPE,
                                     p_COD_USR,
                                     p_ERROR
                                     );
                END LOOP;

            --Verificamos si existen operaciones duplicados
            Sp_PAB_ACT_EST_NMN_DUP(  p_NUM_FOL_NMN,
                                     p_ERROR
                                  );
            COMMIT;
        END IF;
                                                                                                                                                                                                                                                                                                                                                                                                                          --Procedimiento ejecutado de forma correcta
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);
    END Sp_PAB_GES_CAM_EST_DUPLI;

-- ***********************************************************************************************
-- Funcion/Procedimiento: Sp_PAB_ACT_OPE_DUP
-- Objetivo: Procedimiento almacenado que actualiza las operaciones duplicada por no duplicada
-- Sistema: PAB
-- Base de Datos: DGBMSEGDB01_NGALTM
-- Tablas Usadas: PABS_DT_DETLL_NOMNA
-- Fecha: 21/06/16
-- Autor: Santander
-- Input:
-- p_FEC_ISR_OPE -> Fecha Operacion
-- p_NUM_FOL_OPE -> Numero de Operacion
-- p_COD_USR -> Numero del usuario
-- Output:
-- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
-- Input/Output: N/A
-- Retorno: N/A.
-- Observaciones: <Fecha y Detalle de ultimos cambios>
-- ***********************************************************************************************
   PROCEDURE Sp_PAB_ACT_OPE_DUP (
                                   p_FEC_ISR_OPE  IN DATE,
                                   p_NUM_FOL_OPE  IN NUMBER,
                                   p_COD_USR    IN CHAR,
                                   p_ERROR       OUT NUMBER  )
   IS
      v_FLG_NO_DUP      NUMBER(1)       := PKG_PAB_CONSTANTES.V_COD_NO_DUP;
      V_COD_EST_OPE_DUP NUMBER(5)       := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDUP;
      V_COD_EST_OPE_VAL NUMBER(5)       := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEVAL;
      V_DSC_GLB_BTC     VARCHAR2(32)    := 'Se valida operacion no duplicada';
      v_NOM_SP   VARCHAR2 (30) := 'Sp_PAB_ACT_OPE_DUP';

    BEGIN

         UPDATE PABS_DT_DETLL_NOMNA DETNOM
         SET    DETNOM.FLG_OPE_DUP     = v_FLG_NO_DUP,
                DETNOM.COD_EST_AOS     = V_COD_EST_OPE_VAL,
                DETNOM.FEC_ACT_NMN     = SYSDATE --Fecha actualización para dashborad
         WHERE  DETNOM.FEC_ISR_OPE     = p_FEC_ISR_OPE
         AND    DETNOM.NUM_FOL_OPE     = p_NUM_FOL_OPE;

        --Se llama al procedimiento de bitacora para registrar el usuario que modifica el estado de la operacion
        PKG_PAB_TRACKING.Sp_PAB_INS_BIT_OPE (p_NUM_FOL_OPE, p_FEC_ISR_OPE, p_COD_USR, V_COD_EST_OPE_DUP, V_COD_EST_OPE_VAL, V_DSC_GLB_BTC, p_ERROR);
        p_ERROR := PKG_PAB_CONSTANTES.v_OK;

        COMMIT;
                                                                                                                                                                                                                                                                                                                                                                                                                                     --Procedimiento ejecutado de forma correcta
    EXCEPTION
      WHEN DUP_VAL_ON_INDEX
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);
    END Sp_PAB_ACT_OPE_DUP;

--**********************************************************************************************
-- Funcion/Procedimiento : Sp_PAB_ACT_EST_NMN_DUP
-- Objetivo : Procedimiento que inserta la informacion que se encuentra en la nomina
-- Sistema : PAB
-- Base de Datos : DGBMSEGDB01_NGALTM
-- Tablas Usadas : PABS_DT_DETLL_NOMNA, PABS_DT_CBCRA_NOMNA
-- Fecha : 21/06/16
-- Autor : Santander
-- Input:
-- p_NUM_FOL_NMN  ->  Numero de folio nomina
-- Output :
-- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
-- Input/Output : N/A
-- Retorno : N/A.
-- Observaciones : <Fecha y Detalle de ultimos cambios>
--***********************************************************************************************
  PROCEDURE Sp_PAB_ACT_EST_NMN_DUP (     p_NUM_FOL_NMN   IN NUMBER,
                                         p_ERROR        OUT NUMBER
                                   )
   IS
       v_NOM_SP   VARCHAR2 (30) := 'Sp_PAB_ACT_EST_NMN_DUP';
       v_FLG_OPE_DUP NUMBER;
       v_cont_ope_dup number;
       V_GLS_MTV_EST VARCHAR2(300) := NULL;

   BEGIN

         p_ERROR := PKG_PAB_CONSTANTES.v_OK;

        --Verificamos si existen operaciones duplicadas en la nomina.
        /*
        SELECT COUNT(1)
              INTO v_FLG_OPE_DUP
        FROM  PABS_DT_DETLL_NOMNA DETN,
              PABS_DT_CBCRA_NOMNA CABN
        WHERE   DETN.NUM_FOL_NMN  = CABN.NUM_FOL_NMN AND
                DETN.FLG_OPE_DUP  = PKG_PAB_CONSTANTES.V_COD_DUP_OK AND
                DETN.NUM_FOL_NMN  = p_NUM_FOL_NMN;*/

        SELECT  COUNT(1)
          INTO  v_FLG_OPE_DUP
          FROM  PABS_DT_DETLL_NOMNA DETN
                ,PABS_DT_CBCRA_NOMNA CABN
                ,PABS_DT_ESTDO_ALMOT EST
          WHERE CABN.NUM_FOL_NMN = DETN.NUM_FOL_NMN
                AND DETN.COD_EST_AOS = EST.COD_EST_AOS
                AND EST.COD_EST_AOS <> PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEELI
                AND DETN.FLG_OPE_DUP = PKG_PAB_CONSTANTES.V_COD_DUP_OK
                AND CABN.NUM_FOL_NMN = P_NUM_FOL_NMN;
        --
        --Si no existen operaciones duplicadas se debe actualir la nomina.
        IF v_FLG_OPE_DUP = 0 THEN

            --Actualizamos la nomina a valida
            SP_PAB_ACT_SOL_EST_CAB_NOM(p_NUM_FOL_NMN,PKG_PAB_CONSTANTES.V_STR_EST_AOS_NMNVAL,V_GLS_MTV_EST,p_ERROR);

        END IF;

        COMMIT;                                                                                                                                                                                                                                                                                                                                                                                                  --Procedimiento ejecutado de forma correcta
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);
    END Sp_PAB_ACT_EST_NMN_DUP;

-- ***********************************************************************************************
-- Funcion/Procedimiento: SP_PAB_ACT_SOL_EST_CAB_NOM
-- Objetivo: Procedimiento que actualiza solo estado de la cabecera de una nomina
-- Sistema: PAB
-- Base de Datos: DGBMSEGDB01_NGALTM
-- Tablas Usadas: PABS_DT_CBCRA_NOMNA
-- Fecha: 21/06/16
-- Autor: Santander
-- Input:
-- p_NUM_FOL_NMN -> foliador del banco
-- p_COD_EST_AOS -> Identificador de nuevo estado.
-- p_GLS_MTV_EST -> motivo o glosa de la actualizacion
-- Output:
-- p_ERROR -> p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
-- Input/Output: N/A
-- Retorno: N/A.
-- Observaciones: <Fecha y Detalle de ultimos cambios>
-- ***********************************************************************************************
  PROCEDURE SP_PAB_ACT_SOL_EST_CAB_NOM (
                                    p_NUM_FOL_NMN   IN NUMBER,
                                    p_COD_EST_AOS   IN CHAR,
                                    p_GLS_MTV_EST   IN VARCHAR2,
                                    p_ERROR         OUT NUMBER
                                        ) IS
    --Seteo de Variables
    v_NOM_SP VARCHAR2(30):='SP_PAB_ACT_SOL_EST_CAB_NOM';
    v_COD_EST_NOM NUMBER(5);
    ERR_COD_INEX EXCEPTION;

    BEGIN
        p_ERROR := PKG_PAB_CONSTANTES.v_OK;


       --Seteo de variable estado
        CASE

         WHEN P_COD_EST_AOS = PKG_PAB_CONSTANTES.V_STR_EST_AOS_NMNVAL  -- Nomina Valida
         THEN
            V_COD_EST_NOM  := PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNVAL;

         WHEN P_COD_EST_AOS = PKG_PAB_CONSTANTES.V_STR_EST_AOS_NMNCUR  -- Nomina Cursada
         THEN
            V_COD_EST_NOM  := PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNCUR;

         WHEN P_COD_EST_AOS = PKG_PAB_CONSTANTES.V_STR_EST_AOS_NMNAUT  -- Nomina Autorizada
         THEN
            V_COD_EST_NOM  := PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNAUT;

         WHEN P_COD_EST_AOS = PKG_PAB_CONSTANTES.V_STR_EST_AOS_NMNREC  -- Nomina Rechazada
         THEN
            V_COD_EST_NOM  := PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNREC;

         WHEN P_COD_EST_AOS = PKG_PAB_CONSTANTES.V_STR_EST_AOS_NMNELI  -- Nomina Eliminada
         THEN
            V_COD_EST_NOM  := PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNELI;

         WHEN P_COD_EST_AOS = PKG_PAB_CONSTANTES.V_STR_EST_AOS_NMNDUP  -- Nomina Duplicada
         THEN
            V_COD_EST_NOM  := PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNDUP;
         ELSE
             RAISE ERR_COD_INEX;
        END CASE;

    --Actualizamos el estado de la nomina
    BEGIN
        UPDATE
            PABS_DT_CBCRA_NOMNA
        SET
            COD_EST_AOS = v_COD_EST_NOM
          , GLS_MTV_EST = P_GLS_MTV_EST
        WHERE
            NUM_FOL_NMN = p_NUM_FOL_NMN;
    EXCEPTION
        WHEN OTHERS THEN
             err_code := SQLCODE;
             err_msg := SUBSTR (SQLERRM, 1, 300);
             PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code, v_NOM_PCK, err_msg, v_NOM_SP);
             p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
             p_s_mensaje := err_code || '-' || err_msg;
             RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);
    END;

    COMMIT;

    EXCEPTION
        WHEN ERR_COD_INEX THEN
            err_msg := 'Codigo inexistente:' || p_COD_EST_AOS;
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
        WHEN DUP_VAL_ON_INDEX THEN
            err_code := SQLCODE;
            err_msg := SUBSTR(SQLERRM, 1, 300);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
        WHEN OTHERS THEN
             err_code := SQLCODE;
             err_msg := SUBSTR (SQLERRM, 1, 300);
             PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code, v_NOM_PCK, err_msg, v_NOM_SP);
             p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
             p_s_mensaje := err_code || '-' || err_msg;
             RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);

    END SP_PAB_ACT_SOL_EST_CAB_NOM;

--***********************************************************************************************
-- Funcion/Procedimiento: SP_PAB_VAL_OPER_DUPLICADA
-- Objetivo: Consulta las posibles operaciones duplicadas
-- Sistema: PAB
-- Base de Datos: DGBMSEGDB01
-- Tablas Usadas: N/A
-- Fecha: 21/06/16
-- Autor: Santander
-- Input:
-- P_FEC_VTA       : Fecha de valuta
-- P_COD_BCO_DTN   : codigo del banco destinatario
-- P_NUM_CTA_BFC   : cuenta beneficiario
-- P_IMP_OPE       : importe total
-- P_COD_USR       : codigo del usuario
-- Output:
-- P_OPER_DUP -> Retorna 1 o 0 si esta o no duplicada.
-- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
-- Input/Output:
-- N/A.
-- Retorno: N/A.
-- Observaciones: <Fecha y Detalle de ?ltimos cambios>
--***********************************************************************************************
   PROCEDURE SP_PAB_VAL_OPER_DUPLICADA (    P_FEC_VTA       IN DATE
                                          , P_COD_BCO_DTN   IN VARCHAR2
                                          , P_NUM_CTA_BFC   IN VARCHAR2
                                          , P_IMP_OPE       IN NUMBER
                                          , P_COD_USR       IN CHAR
                                          , P_OPER_DUP      OUT NUMBER
                                          , P_ERROR         OUT NUMBER ) IS
      --Seteo de Variables
      V_NOM_SP        VARCHAR2 (30) := 'SP_PAB_VAL_OPER_DUPLICADA';
      V_COD_ENT_SAL   char(10);
      V_PERROR_CANAL  NUMBER;

    BEGIN

        -- Devuelve el canal por usuario, el resultado V_COD_ENT_SAL
        PKG_PAB_UTILITY.SP_PAB_OBT_CAN_ENT_USU(P_COD_USR, V_COD_ENT_SAL, V_PERROR_CANAL); -- CONTROLAR ERROR EN CASO DE QUE VARIABLE VENGA VACIA

        -- Funcion que devuelve la duplicidad de un registro ****
        P_OPER_DUP   := FN_PAB_BUS_DUPLICADO(P_FEC_VTA, P_COD_BCO_DTN, P_NUM_CTA_BFC, P_IMP_OPE, V_COD_ENT_SAL );  --1
        P_ERROR := PKG_PAB_CONSTANTES.V_OK;

    EXCEPTION
        WHEN DUP_VAL_ON_INDEX THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,v_NOM_PCK,err_msg,v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
        WHEN OTHERS THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,v_NOM_PCK,err_msg,v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            p_s_mensaje := err_code || '-' || err_msg;
            RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);
    END SP_PAB_VAL_OPER_DUPLICADA;

--***********************************************************************************************
-- Funcion/Procedimiento: FN_PAB_BUS_DUPLICADO
-- Objetivo: Consulta las posibles operaciones duplicadas
-- Sistema: PAB
-- Base de Datos: DGBMSEGDB01
-- Tablas Usadas: PABS_DT_DETLL_NOMNA, PABS_DT_CBCRA_NOMNA
-- Fecha: 21/06/16
-- Autor: Santander
-- Input:
-- P_FEC_VTA -> fecha valuta
-- P_COD_BCO_DTN  -> banco destino
-- P_NUM_CTA_BFC  -> numero cuenta beneficiaria
-- P_IMP_OPE     ->  importe de una operacion
-- P_COD_SIS_ENT  -> codigo de sistema entrada
-- N/A
-- Output:
-- N/A.
-- Input/Output:
-- N/A.
-- Retorno:
-- PKG_PAB_CONSTANTES_2.V_COD_DUP_OK -> flag duplicado ok PKG_PAB_CONSTANTES_2.V_COD_NO_DUP -> flag no duplicado
-- Observaciones: <Fecha y Detalle de ?ltimos cambios>
--***********************************************************************************************
 FUNCTION FN_PAB_BUS_DUPLICADO (   P_FEC_VTA       IN DATE,
                                   P_COD_BCO_DTN   IN CHAR,
                                   P_NUM_CTA_BFC   IN VARCHAR2,
                                   P_IMP_OPE       IN NUMBER,
                                   P_COD_SIS_ENT   IN CHAR
                        ) RETURN NUMBER IS
      V_NOM_SP        VARCHAR2(20) := 'FN_PAB_BUS_DUPLICADO';
      V_DUPLICADO     NUMBER ;
      V_FEC_VTA       VARCHAR2(20) := P_FEC_VTA;

    BEGIN

       SELECT  COUNT(1)
         INTO  V_DUPLICADO
         FROM  PABS_DT_DETLL_NOMNA DETN
                ,PABS_DT_CBCRA_NOMNA CABN
                ,PABS_DT_ESTDO_ALMOT EST
        WHERE   DETN.NUM_FOL_NMN  = CABN.NUM_FOL_NMN
                AND EST.COD_EST_AOS = DETN.COD_EST_AOS
                AND EST.COD_EST_AOS <> PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEELI
                AND CABN.COD_SIS_ENT = P_COD_SIS_ENT
                AND TO_CHAR( CABN.FEC_ISR_NMN, 'DD/MM/YYYY') = TO_CHAR( SYSDATE, 'DD/MM/YYYY') --Solo las nominas del dia
                AND DETN.COD_BCO_DTN = P_COD_BCO_DTN
                AND NVL(DETN.NUM_CTA_BFC,'#') = NVL(P_NUM_CTA_BFC,NVL(DETN.NUM_CTA_BFC,'#'))
                AND DETN.IMP_OPE     = P_IMP_OPE;

       --Indicamos si el regsitro se encuentra duplicado o no.
        IF (V_DUPLICADO >= 1) THEN

            RETURN PKG_PAB_CONSTANTES.V_COD_DUP_OK; --Operacion duplicada  1

        ELSE
            RETURN PKG_PAB_CONSTANTES.V_COD_NO_DUP; --Operacion no duplicada  0

        END IF;

    EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
        WHEN OTHERS
        THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);

    END FN_PAB_BUS_DUPLICADO;

--***********************************************************************************************
-- Funcion/Procedimiento: SP_PAB_VERIF_NOM_DUPL
-- Objetivo: Consulta la si la nomina esta duplicada
-- Sistema: PAB
-- Base de Datos: DGBMSEGDB01
-- Tablas Usadas: PABS_DT_DETLL_NOMNA, PABS_DT_CBCRA_NOMNA
-- Fecha: 21/06/16
-- Autor: Santander
-- Input:
-- P_NUM_FOL_NMN   : numero de folio nomina
-- Output:
-- P_RESUL_OPE_DUP -> Retorna 1 o 0 si esta o no duplicada.
-- P_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
-- Input/Output:
-- N/A.
-- Retorno: N/A.
-- Observaciones: <Fecha y Detalle de ?ltimos cambios>
--***********************************************************************************************
   PROCEDURE SP_PAB_VERIF_NOM_DUPL(  P_NUM_FOL_NMN        IN  NUMBER,
                                     P_RESUL_OPE_DUP      OUT NUMBER,
                                     P_ERROR              OUT NUMBER) IS
      --Seteo de Variables
      V_NOM_SP            VARCHAR2 (30) := 'SP_PAB_VERIF_NOM_DUPL';
      V_DUP               NUMBER := PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNDUP;
      V_FLG_DUPLICADA     NUMBER := PKG_PAB_CONSTANTES.V_COD_DUP_OK;
      V_RESUL_DUPLI       NUMBER;
      V_GLS_MTV_EST       VARCHAR2(300) := NULL;

   BEGIN

        SELECT  COUNT(1)
          INTO  V_RESUL_DUPLI
          FROM  PABS_DT_DETLL_NOMNA DETN
                ,PABS_DT_CBCRA_NOMNA CABN
                ,PABS_DT_ESTDO_ALMOT EST
          WHERE CABN.NUM_FOL_NMN = DETN.NUM_FOL_NMN
                AND DETN.COD_EST_AOS = EST.COD_EST_AOS
                AND EST.COD_EST_AOS <> PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEELI
                AND DETN.FLG_OPE_DUP = PKG_PAB_CONSTANTES.V_COD_DUP_OK
                AND CABN.NUM_FOL_NMN = P_NUM_FOL_NMN;

        IF( V_RESUL_DUPLI >= 1 ) THEN

            --Actualizamos la nomina a duplicada
            --SP_PAB_ACT_SOL_EST_CAB_NOM(P_NUM_FOL_NMN,PKG_PAB_CONSTANTES.V_STR_EST_AOS_NMNDUP,V_GLS_MTV_EST,p_ERROR);

            P_RESUL_OPE_DUP := 1;

        ELSE
            P_RESUL_OPE_DUP := 0;
        END IF;

        P_ERROR := PKG_PAB_CONSTANTES.V_OK;

   EXCEPTION
          WHEN NO_DATA_FOUND
          THEN
             err_code := SQLCODE;
             err_msg := SUBSTR (SQLERRM, 1, 300);
             PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                 v_NOM_PCK,
                                                 err_msg,
                                                 v_NOM_SP);
             p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
          WHEN OTHERS
          THEN
             err_code := SQLCODE;
             err_msg := SUBSTR (SQLERRM, 1, 300);
             PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                 v_NOM_PCK,
                                                 err_msg,
                                                 v_NOM_SP);
             p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
             p_s_mensaje := err_code || '-' || err_msg;
             RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);
   END SP_PAB_VERIF_NOM_DUPL;

-- ***********************************************************************************************
-- Funcion/Procedimiento: Sp_PAB_GES_ELI_OPE
-- Objetivo: Procedimiento que actualiza el estado de una operacion
-- Sistema: PAB
-- Base de Datos: DGBMSEGDB01_NGALTM
-- Tablas Usadas: N/A
-- Fecha: 21/06/16
-- Autor: Santander
-- Input:
-- p_REG_OPER -> parametro tipo registro que contiene el numero y fecha operacion
-- p_COD_USR -> rut usuario
-- p_GLS_MTV_EST -> motivo o glosa de eliminacion
-- p_SIS_NOM_OPE -> origen al que pertenece la operacion. NOM = Nomina - OPE = Operacion
-- Output:
-- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
-- Input/Output: N/A
-- Retorno: N/A.
-- Observaciones: <Fecha y Detalle de ultimos cambios>
-- ***********************************************************************************************
  PROCEDURE Sp_PAB_GES_ELI_OPE( p_REG_OPER    IN REG_OPER,
                                p_COD_USR     IN VARCHAR2,
                                p_GLS_MTV_EST IN VARCHAR2,
                                p_SIS_NOM_OPE IN CHAR,
                                p_ERROR       OUT NUMBER
                                ) IS
    --
    v_NOM_SP VARCHAR2(31) :='Sp_PAB_GES_ELI_OPE';
    v_FEC_ISR_OPE date;
    v_EST_NMN NUMBER;

    BEGIN

        p_ERROR := PKG_PAB_CONSTANTES.v_OK;

        IF p_REG_OPER.COUNT > 0 THEN

            FOR i IN p_REG_OPER.FIRST .. p_REG_OPER.LAST
            LOOP

                PKG_PAB_NOMINA.Sp_PAB_ACT_ELI_OPE (p_REG_OPER(i).NUM_FOL_NMN  , p_REG_OPER(i).FEC_ISR_OPE, p_REG_OPER(i).NUM_FOL_OPE, p_COD_USR, p_GLS_MTV_EST, p_SIS_NOM_OPE, p_ERROR );
            END LOOP;

            --Si es una nomina verificamos si debemos cambiar de estado en el caso de estar todas eliminadas.
            IF p_SIS_NOM_OPE = 'NOM' THEN

                --GSD
                SELECT COD_EST_AOS
                INTO v_EST_NMN
                FROM PABS_DT_CBCRA_NOMNA
                WHERE NUM_FOL_NMN = p_REG_OPER(1).NUM_FOL_NMN;

                --Volvemos a calcular el monto de la nomina debido a que se elimina una operacion
                --GSD
                PKG_PAB_NOMINA.Sp_PAB_ACT_CAB_NOMINA(p_REG_OPER(1).NUM_FOL_NMN, v_EST_NMN, p_ERROR);

                --Validacion cambio estado nomina.
                PKG_PAB_NOMINA.Sp_PAB_ACT_ELI_NMN( p_REG_OPER(1).NUM_FOL_NMN, p_GLS_MTV_EST, p_ERROR );

            END IF;

        END IF;

        COMMIT;

    EXCEPTION

         WHEN NO_DATA_FOUND THEN

            --commit;

            err_code := SQLCODE;
            err_msg := SUBSTR(SQLERRM, 1, 300);
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
            p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;



        WHEN OTHERS THEN
             err_code := SQLCODE;
             err_msg := SUBSTR (SQLERRM, 1, 300);
             PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code, v_NOM_PCK, err_msg, v_NOM_SP);
             p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
             p_s_mensaje := err_code || '-' || err_msg;
             RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);


    END Sp_PAB_GES_ELI_OPE;

-- ***********************************************************************************************
-- Funcion/Procedimiento: Sp_PAB_ACT_ELI_OPE
-- Objetivo: Procedimiento que actualiza el estado de una operacion
-- Sistema: PAB
-- Base de Datos: DGBMSEGDB01_NGALTM
-- Tablas Usadas: PABS_DT_DETLL_NOMNA, PABS_DT_DETLL_OPRCN, PABS_DT_CBCRA_NOMNA
-- Fecha: 21/06/16
-- Autor: Santander
-- Input:
-- p_NUM_FOL_NMN -> foliador del banco
-- p_FEC_ISR_OPE -> Fecha de Operacion
-- p_NUM_FOL_OPE -> Numero de operacion
-- p_COD_USR -> rut usuario
-- p_GLS_MTV_EST -> motivo o glosa de eliminacion
-- p_SIS_NOM_OPE -> origen al que pertenece la operacion. NOM = Nomina - OPE = Operacion
-- Output:
-- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
-- Input/Output: N/A
-- Retorno: N/A.
-- Observaciones: <Fecha y Detalle de ultimos cambios>
-- ***********************************************************************************************
  PROCEDURE Sp_PAB_ACT_ELI_OPE( p_NUM_FOL_NMN   IN NUMBER,
                                p_FEC_ISR_OPE   IN DATE,
                                p_NUM_FOL_OPE   IN NUMBER,
                                p_COD_USR       IN VARCHAR2,
                                p_GLS_MTV_EST   IN VARCHAR2,
                                p_SIS_NOM_OPE   IN CHAR,
                                p_ERROR         OUT NUMBER
                               )IS

    --Seteo de Variables
    V_NOM_SP            VARCHAR2(30):='Sp_PAB_ACT_ELI_OPE';
    V_COD_EST_OPE_INI   NUMBER(5);
    V_COD_EST_OPE_UDP   NUMBER(5) := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEELI;
    ERR_COD_INEX        EXCEPTION;

    BEGIN
        P_ERROR := PKG_PAB_CONSTANTES.V_OK;

        --Rescata el estado actual de la operacion antes del cambio
        BEGIN

            SELECT COD_EST_AOS
            INTO V_COD_EST_OPE_INI
            FROM PABS_DT_DETLL_NOMNA
            WHERE NUM_FOL_OPE = p_NUM_FOL_OPE
                AND FEC_ISR_OPE = p_FEC_ISR_OPE;


        EXCEPTION
            WHEN NO_DATA_FOUND THEN
               err_code := SQLCODE;
               err_msg := SUBSTR (SQLERRM, 1, 300);
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code, v_NOM_PCK, err_msg, v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;

            WHEN OTHERS THEN
               err_code := SQLCODE;
               err_msg := SUBSTR (SQLERRM, 1, 300);
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code, v_NOM_PCK, err_msg, v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
               p_s_mensaje := err_code || '-' || err_msg;
               RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);

        END;

        --Si viene del mundo nomina solo existe informacion en las tablas de nomina.
        IF p_SIS_NOM_OPE = 'NOM' THEN

            BEGIN
                --Actualiza el estado de la nomina/operacion a eliminada
                UPDATE
                    PABS_DT_DETLL_NOMNA det
                SET
                    COD_EST_AOS = V_COD_EST_OPE_UDP
                  , GLS_ADC_EST = p_GLS_MTV_EST
                  , FEC_ACT_NMN = SYSDATE --Fecha actualización para dashborad
                WHERE NUM_FOL_OPE = p_NUM_FOL_OPE
                    AND FEC_ISR_OPE = p_FEC_ISR_OPE;

            EXCEPTION
                WHEN OTHERS THEN
                   err_code := SQLCODE;
                   err_msg := SUBSTR (SQLERRM, 1, 300);
                   PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code, v_NOM_PCK, err_msg, v_NOM_SP);
                   p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
                   p_s_mensaje := err_code || '-' || err_msg;
                   RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);

            END;

        ELSE

            -- Proviene del mundo operacion se debe modificar las tablas de operacion y de nomina
            IF (NVL(p_NUM_FOL_NMN, 0) <> 0) THEN

                -- Disstingue si la operacion proviene del mundo directo
                BEGIN
                    --Actualiza el estado de la nomina/operacion a eliminada
                    UPDATE
                        PABS_DT_DETLL_NOMNA
                    SET
                        COD_EST_AOS = V_COD_EST_OPE_UDP
                      , GLS_ADC_EST = p_GLS_MTV_EST
                      , FEC_ACT_NMN = SYSDATE     --Fecha actualización para dashborad
                    WHERE NUM_FOL_OPE = p_NUM_FOL_OPE
                        AND FEC_ISR_OPE = p_FEC_ISR_OPE;

                EXCEPTION
                    WHEN OTHERS THEN
                       err_code := SQLCODE;
                       err_msg := SUBSTR (SQLERRM, 1, 300);
                       PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code, v_NOM_PCK, err_msg, v_NOM_SP);
                       p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
                       p_s_mensaje := err_code || '-' || err_msg;
                       RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);

                END;
            END IF;

            --Actualiza el estado de la Operacion
            BEGIN
                UPDATE
                    PABS_DT_DETLL_OPRCN
                SET
                    COD_EST_AOS = V_COD_EST_OPE_UDP
                  , GLS_ADC_EST = p_GLS_MTV_EST
                  , FEC_ACT_OPE = SYSDATE --Fecha actualización para dashborad
                WHERE NUM_FOL_OPE = p_NUM_FOL_OPE
                    AND FEC_ISR_OPE = p_FEC_ISR_OPE;

            EXCEPTION
                WHEN OTHERS THEN
                   err_code := SQLCODE;
                   err_msg := SUBSTR (SQLERRM, 1, 300);
                   PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code, v_NOM_PCK, err_msg, v_NOM_SP);
                   p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
                   p_s_mensaje := err_code || '-' || err_msg;
                   RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);
            END;

        END IF;

        --Se llama al procedimiento de bitacora para registrar el usuario que modifica el estado de la operacion
        PKG_PAB_TRACKING.Sp_PAB_INS_BIT_OPE (p_NUM_FOL_OPE, p_FEC_ISR_OPE, p_COD_USR, V_COD_EST_OPE_INI, V_COD_EST_OPE_UDP, p_GLS_MTV_EST, p_ERROR);

    EXCEPTION
        WHEN ERR_COD_INEX THEN
            err_msg := 'Codigo inexistente:' || V_COD_EST_OPE_UDP;
            P_ERROR := PKG_PAB_CONSTANTES.V_ERROR;
            PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD( err_code, V_NOM_PCK ,err_msg , V_NOM_SP);
        WHEN DUP_VAL_ON_INDEX THEN
            err_code := SQLCODE;
            err_msg := SUBSTR(SQLERRM, 1, 300);
            P_ERROR := PKG_PAB_CONSTANTES.V_ERROR;
            PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD( err_code, V_NOM_PCK ,err_msg , V_NOM_SP);
        WHEN OTHERS THEN
             err_code := SQLCODE;
             err_msg := SUBSTR (SQLERRM, 1, 300);
             PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code, v_NOM_PCK, err_msg, v_NOM_SP);
             p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
             p_s_mensaje := err_code || '-' || err_msg;
             RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);

    END Sp_PAB_ACT_ELI_OPE;
--

-- ***********************************************************************************************
-- Funcion/Procedimiento: Sp_PAB_ACT_ELI_NMN
-- Objetivo: Procedimiento que actualiza el estado de una nomina con todas las operaciones eliminadas
-- Sistema: PAB
-- Base de Datos: DGBMSEGDB01_NGALTM
-- Tablas Usadas: PABS_DT_CBCRA_NOMNA
-- Fecha: 21/06/16
-- Autor: Santander
-- Input:
-- p_NUM_FOL_NMN -> foliador del banco
-- p_GLS_MTV_EST -> motivo o glosa de eliminacion
-- Output:
-- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
-- Input/Output: N/A
-- Retorno: N/A.
-- Observaciones: <Fecha y Detalle de ultimos cambios>
-- ***********************************************************************************************
  PROCEDURE Sp_PAB_ACT_ELI_NMN( p_NUM_FOL_NMN   IN NUMBER,
                                p_GLS_MTV_EST   IN VARCHAR2,
                                p_ERROR         OUT NUMBER
                                ) IS

    v_NOM_SP            VARCHAR2(31) :='Sp_PAB_ACT_ELI_NMN';
    V_CANT_OPE          NUMBER(5);
    ERR_COD_INEX        EXCEPTION;
    V_COD_EST_AOS       NUMBER;

    BEGIN

        --Valida si existen mas operaciones en estado distinto de eliminada
        BEGIN

            SELECT COUNT(NUM_FOL_OPE)
            INTO V_CANT_OPE
            FROM PABS_DT_DETLL_NOMNA
            WHERE NUM_FOL_NMN = p_NUM_FOL_NMN
                AND COD_EST_AOS <> PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEELI;

        EXCEPTION
            WHEN NO_DATA_FOUND THEN
               err_code := SQLCODE;
               err_msg := SUBSTR (SQLERRM, 1, 300);
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code, v_NOM_PCK, err_msg, v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;

            WHEN OTHERS THEN
               err_code := SQLCODE;
               err_msg := SUBSTR (SQLERRM, 1, 300);
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code, v_NOM_PCK, err_msg, v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
               p_s_mensaje := err_code || '-' || err_msg;
               RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);

        END;

        --Si todas las operaciones se encuentran eliminadas se debe cambiar el estado de la nomina a eliminada
        IF V_CANT_OPE = 0 THEN


            --Actualizamos el estado de la nomina cuando no quedan operaciones sin eliminar
            BEGIN

                -- Actualizamos la informacion
                Sp_PAB_ACT_CAB_NOMINA_ELI(p_NUM_FOL_NMN,p_ERROR);

                --
                UPDATE
                    PABS_DT_CBCRA_NOMNA
                SET
                    COD_EST_AOS = PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNELI
                WHERE
                    NUM_FOL_NMN = p_NUM_FOL_NMN;

            EXCEPTION
                WHEN OTHERS THEN
                   err_code := SQLCODE;
                   err_msg := SUBSTR (SQLERRM, 1, 300);
                   PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code, v_NOM_PCK, err_msg, v_NOM_SP);
                   p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
                   p_s_mensaje := err_code || '-' || err_msg;
                   RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);
            END;

        --Si no todas las operaciones se encuentran en estado eliminada se debe verificar si se cambia el estado de la nomina a valida
        ELSE


            --Verificamos si existe una operacion distinta de Validas / Eliminadas / Duplicada
            SELECT COUNT(NUM_FOL_OPE)
            INTO V_CANT_OPE
            FROM PABS_DT_DETLL_NOMNA
            WHERE NUM_FOL_NMN = p_NUM_FOL_NMN
            AND COD_EST_AOS NOT IN (PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEVAL,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEELI,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDUP);


            IF V_CANT_OPE = 0 THEN

                BEGIN

                    --Verificamos si existe una operacion Duplicada
                    SELECT COUNT(NUM_FOL_OPE)
                    INTO V_CANT_OPE
                    FROM PABS_DT_DETLL_NOMNA
                    WHERE NUM_FOL_NMN = p_NUM_FOL_NMN
                    AND COD_EST_AOS IN (PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDUP);

                    IF(V_CANT_OPE > 0) THEN
                        V_COD_EST_AOS:= PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNDUP;
                    ELSE
                        V_COD_EST_AOS := PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNVAL;
                    END IF;


                    --Actualizamos
                    UPDATE
                        PABS_DT_CBCRA_NOMNA
                    SET
                        COD_EST_AOS = V_COD_EST_AOS
                      , GLS_MTV_EST = p_GLS_MTV_EST
                    WHERE
                        NUM_FOL_NMN = p_NUM_FOL_NMN;

                EXCEPTION
                    WHEN OTHERS THEN
                       err_code := SQLCODE;
                       err_msg := SUBSTR (SQLERRM, 1, 300);
                       PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code, v_NOM_PCK, err_msg, v_NOM_SP);
                       p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
                       p_s_mensaje := err_code || '-' || err_msg;
                       RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);
                END;



            END IF;

        END IF;

    EXCEPTION
        WHEN DUP_VAL_ON_INDEX THEN
            err_code := SQLCODE;
            err_msg := SUBSTR(SQLERRM, 1, 300);
            P_ERROR := PKG_PAB_CONSTANTES.V_ERROR;
            PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD( err_code, V_NOM_PCK ,err_msg , V_NOM_SP);
        WHEN OTHERS THEN
             err_code := SQLCODE;
             err_msg := SUBSTR (SQLERRM, 1, 300);
             PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code, v_NOM_PCK, err_msg, v_NOM_SP);
             p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
             p_s_mensaje := err_code || '-' || err_msg;
             RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);

    END Sp_PAB_ACT_ELI_NMN;

-- ***********************************************************************************************
-- Funcion/Procedimiento: Sp_PAB_CON_NOM_HIST
-- Objetivo: Procedimiento almacenado que retorna las consultas operaciones historicas de una nomina
-- Sistema: PAB
-- Base de Datos: DGBMSEGDB01_NGALTM
-- Tablas Usadas: PABS_DT_DETLL_OPRCN, PABS_DT_OPRCN_INFCN_OPCON, PABS_DT_DETLL_OPRCN_HTRCA, PABS_DT_OPRCN_OPCON_HTRCA
-- Fecha: 21/06/16
-- Autor: Santander
-- Input:
-- p_COD_EST_AOS -> codigo estado
-- p_COD_SIS_ENT -> codigo sistema entrada
-- p_COD_USR     -> codigo usuario
-- p_NUM_FOL_NMN -> numero folio nomina
-- p_FEC_DESDE   -> fecha desde
-- p_FEC_HASTA   -> fecha hasta
-- p_IMp_TOT_DESDE -> importe total desde
-- p_IMp_TOT_HASTA -> importe total hasta
-- Output:
-- p_CURSOR -> Cursor con las nominas encontradas
-- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
-- Input/Output: N/A
-- Retorno: N/A.
-- Observaciones: <Fecha y Detalle de ultimos cambios>
-- ***********************************************************************************************
PROCEDURE Sp_PAB_CON_NOM_HIST (     p_COD_EST_AOS IN NUMBER,
                                    p_COD_SIS_ENT IN CHAR,
                                    p_COD_USR     IN CHAR,
                                    p_NUM_FOL_NMN IN NUMBER,
                                    p_FEC_DESDE   IN DATE,
                                    p_FEC_HASTA   IN DATE,
                                    p_IMp_TOT_DESDE IN NUMBER,
                                    p_IMp_TOT_HASTA IN NUMBER,
                                    p_CURSOR      OUT SYS_REFCURSOR,
                                    p_ERROR       OUT NUMBER) IS

        v_NOM_SP   VARCHAR2 (30) := 'Sp_PAB_CON_NOM_HIST';
        v_IMp_DSD NUMBER(15,2);
        v_IMp_HST NUMBER(15,2);
        v_FEC_DESDE DATE;
        v_FEC_HASTA DATE;
        v_COD_EST_AOS NUMBER;
        v_DIV_MULTI NUMBER := 1;
        v_NUM_NMN NUMBER;
        v_COD_SIS_ENT CHAR(10);
        v_COD_USR CHAR(11);
        --
    BEGIN

        --VALIDACION MONTO DESDE
        IF p_IMp_TOT_DESDE = -1 THEN
            v_IMp_DSD := 0;
        ELSE
            v_IMp_DSD := p_IMp_TOT_DESDE;
        END IF;

        --VALIDACION MONTO HASTA
        IF p_IMp_TOT_HASTA = -1 THEN
            v_IMp_HST := 9999999999999;
        ELSE
            v_IMp_HST := p_IMp_TOT_HASTA;
        END IF;

        --ESTADO
        IF p_COD_EST_AOS = -1 THEN
            v_COD_EST_AOS := NULL;
        ELSE
            v_COD_EST_AOS := p_COD_EST_AOS;
        END IF;

        -- NUMERO DE FOLIO NOMINA
        IF  p_NUM_FOL_NMN = -1 THEN
            v_NUM_NMN := NULL;
        ELSE
            v_NUM_NMN := p_NUM_FOL_NMN;
        END IF;

        --VALIDACION NUMERO NOMINA
        IF p_COD_SIS_ENT = PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SE_OOFF OR
           p_COD_SIS_ENT = PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SE_TECNO THEN
            v_COD_SIS_ENT := NULL;
        ELSE
            v_COD_SIS_ENT:= p_COD_SIS_ENT;
        END IF;

        IF p_COD_USR = '-1' THEN
            v_COD_USR := NULL;
        ELSE
            v_COD_USR := p_COD_USR;
        END IF;
        --
        OPEN p_CURSOR FOR
         SELECT FLG_DVI,
                NUM_FOL_NMN,
                FEC_ISR_NMN,
                DSC_EST_AOS,
                COD_USR_CRC,
                IMp_TOT_NMN,
                COD_DVI,
                COD_SIS_ENT,
                ORIGEN
                FROM
                (
         SELECT CABNOM.FLG_DVI AS FLG_DVI,
                CABNOM.NUM_FOL_NMN AS NUM_FOL_NMN,
                CABNOM.FEC_ISR_NMN AS FEC_ISR_NMN,
                EST.DSC_EST_AOS AS DSC_EST_AOS,
                CABNOM.COD_USR  AS COD_USR_CRC,
                DECODE(CABNOM.FLG_DVI, v_DIV_MULTI, PKG_PAB_CONSTANTES.V_STR_DVI_MULTI, CABNOM.IMp_TOT_NMN) IMp_TOT_NMN,
                PKG_PAB_NOMINA.FN_PAB_BUS_DVI_NOMINA_CON (CABNOM.FLG_DVI, CABNOM.NUM_FOL_NMN) COD_DVI,
                CABNOM.COD_SIS_ENT,
                'N' AS ORIGEN
           FROM PABS_DT_CBCRA_NOMNA CABNOM,
                PABS_DT_ESTDO_ALMOT EST,
                PABS_DT_SISTM_ENTRD_SALID CAN
          WHERE CABNOM.COD_EST_AOS         = EST.COD_EST_AOS
                AND CABNOM.COD_SIS_ENT     = CAN.COD_SIS_ENT_SAL
                AND CABNOM.COD_EST_AOS     = NVL( v_COD_EST_AOS , CABNOM.COD_EST_AOS)
                AND CABNOM.NUM_FOL_NMN     = NVL( v_NUM_NMN , CABNOM.NUM_FOL_NMN )
                AND CABNOM.COD_SIS_ENT     = NVL( v_COD_SIS_ENT , CABNOM.COD_SIS_ENT)
                AND CABNOM.IMp_TOT_NMN BETWEEN NVL( v_IMp_DSD , CABNOM.IMp_TOT_NMN )
                                           AND NVL( v_IMp_HST , CABNOM.IMP_TOT_NMN )
                AND TRUNC(CABNOM.FEC_ISR_NMN)    BETWEEN  p_FEC_DESDE AND p_FEC_HASTA
                AND CABNOM.COD_USR        = NVL(v_COD_USR, CABNOM.COD_USR)
        UNION
            SELECT CABNOMH.FLG_DVI,
                CABNOMH.NUM_FOL_NMN,
                CABNOMH.FEC_ISR_NMN AS FEC_ISR_NMN,
                EST.DSC_EST_AOS AS DSC_EST_AOS,
                CABNOMH.COD_USR AS COD_USR_CRC,
                DECODE(CABNOMH.FLG_DVI, v_DIV_MULTI, PKG_PAB_CONSTANTES.V_STR_DVI_MULTI, CABNOMH.IMp_TOT_NMN) IMp_TOT_NMN,
                PKG_PAB_NOMINA.FN_PAB_BUS_DVI_NOMINA_CON_HIS (CABNOMH.FLG_DVI, CABNOMH.NUM_FOL_NMN, CABNOMH.FEC_CGA_HIS ) COD_DVI,
                CABNOMH.COD_SIS_ENT,
                'H' AS ORIGEN
           FROM PABS_DT_CBCRA_NOMNA_HTRCA CABNOMH,
                PABS_DT_ESTDO_ALMOT EST,
                PABS_DT_SISTM_ENTRD_SALID CAN
          WHERE  CABNOMH.COD_EST_AOS        = EST.COD_EST_AOS
                AND CABNOMH.COD_SIS_ENT     = CAN.COD_SIS_ENT_SAL
                AND CABNOMH.COD_EST_AOS     = NVL( v_COD_EST_AOS , CABNOMH.COD_EST_AOS)
                AND CABNOMH.NUM_FOL_NMN     = NVL( v_NUM_NMN , CABNOMH.NUM_FOL_NMN )
                AND CABNOMH.COD_SIS_ENT     = NVL( v_COD_SIS_ENT , CABNOMH.COD_SIS_ENT)
                AND CABNOMH.IMp_TOT_NMN BETWEEN NVL( v_IMp_DSD , CABNOMH.IMp_TOT_NMN )
                                           AND NVL( v_IMp_HST , CABNOMH.IMP_TOT_NMN )
                AND TRUNC(CABNOMH.FEC_ISR_NMN)    BETWEEN  p_FEC_DESDE AND p_FEC_HASTA
                AND  CABNOMH.COD_USR = NVL(v_COD_USR, CABNOMH.COD_USR)
            )
            ORDER BY NUM_FOL_NMN DESC;

            p_ERROR := PKG_PAB_CONSTANTES.v_OK; --Procedimiento ejecutado de forma correcta

    EXCEPTION
            WHEN NO_DATA_FOUND THEN
                    err_code := SQLCODE;
                    err_msg := SUBSTR(SQLERRM, 1, 300);
                    PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                    p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
            WHEN OTHERS THEN
                    err_code := SQLCODE;
                    err_msg := SUBSTR(SQLERRM, 1, 300);
                    PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                    p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                    p_s_mensaje := err_code || '-' || err_msg;
                    RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);
    --
    END Sp_PAB_CON_NOM_HIST;

-- ***********************************************************************************************
-- Funcion/Procedimiento: Sp_PAB_BUS_DET_NOM_CON
-- Objetivo: Procedimiento almacenado que retorna las operaciones de una nomina Historica o Normal
-- Sistema: PAB
-- Base de Datos: DGBMSEGDB01_NGALTM
-- Tablas Usadas: PABS_DT_DETLL_NOMNA, PABS_DT_SISTM_ENTRD_SALID, PABS_DT_CBCRA_NOMNA, TCDT040, TCDTBAI
-- Fecha: 21/06/16
-- Autor: Santander
-- Input:
-- p_NUM_FOL_NMN -> Numero de la nomina
-- p_TIP_BUS_HIST -> Indicador de tipo de busqueda H = Historica -  N = Normal
-- Output:
-- p_CURSOR -> Cursor con las nominas encontradas
-- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
-- Input/Output: N/A
-- Retorno: N/A.
-- Observaciones: <Fecha y Detalle de ultimos cambios>
-- ***********************************************************************************************
   PROCEDURE Sp_PAB_BUS_DET_NOM_CON (p_NUM_FOL_NMN IN NUMBER, p_TIP_BUS_HIST IN CHAR, p_CURSOR OUT SYS_REFCURSOR, p_ERROR OUT NUMBER) IS
      v_TPO_BUS_H CHAR := 'H';
      v_TPO_BUS_N CHAR := 'N';
      ERR_COD_INEX EXCEPTION;
       v_NOM_SP        VARCHAR2 (30) := 'Sp_PAB_BUS_DET_NOM_CON';
   BEGIN

                IF  p_TIP_BUS_HIST = v_TPO_BUS_N THEN
                        OPEN p_CURSOR FOR
                           SELECT DETNOM.NUM_FOL_OPE AS NUMERO_OPERACION,
                              DETNOM.FEC_ISR_OPE AS F_INSERT_OPERACION,
                              DETNOM.IMp_OPE AS MONTO_TOTAL,
                              DETNOM.COD_BCO_DTN AS BIC_BAN_DEST,
                              DECODE (T40.TCNIFENT, NULL, 'BANCO EXTRANJERO', T40.TCNIFENT) AS RUT_BANCO,
                              TBAI.DES_BCO AS GLS_BAN_DEST,
                              DETNOM.NUM_CTA_BFC NUM_CTA_BFC,                                                                                                                                                                                                                                                                                                                                                                                   --DETNOM.NUM_CTA_BFC,
                              DETNOM.COD_DVI,
                              CABNOM.NUM_FOL_NMN,
                              DETNOM.FEC_VTA AS FECHA_VALUTA,
                              CABNOM.COD_USR,
                              ENT.DSC_SIS_ENT_SAL,
                              PKG_PAB_UTILITY.FN_PAB_TRAN_COD_EST_GLOSA(DETNOM.COD_EST_AOS,PKG_PAB_CONSTANTES.V_IND_OPE) AS EST_OPE, --llamar a la funcion
                              DETNOM.FLG_OPE_DUP,   --flag de duplicidad a nivel de operacion
                              EST.DSC_EST_AOS AS DSC_EST_OPE,
                              PKG_PAB_NOMINA.FN_PAB_BUS_ID_GTR(DETNOM.NUM_FOL_OPE,DETNOM.FEC_ISR_OPE,p_TIP_BUS_HIST)       AS NUM_IDF_GTR_DOC--- CAH ID GESTOR DOCUMENTAL
                         FROM PABS_DT_DETLL_NOMNA DETNOM,
                              PABS_DT_SISTM_ENTRD_SALID ENT,
                              PABS_DT_CBCRA_NOMNA CABNOM,
                              PABS_DT_ESTDO_ALMOT EST,
                              TCDT040 T40,
                              TCDTBAI TBAI
                        WHERE DETNOM.NUM_FOL_NMN = p_NUM_FOL_NMN
                          AND DETNOM.COD_BCO_DTN = TBAI.TGCDSWSA(+)
                          AND DETNOM.COD_BCO_DTN = T40.TGCDSWSA(+)
                          AND T40.TCNIFENT (+) <> v_BANCO
                          AND CABNOM.NUM_FOL_NMN = DETNOM.NUM_FOL_NMN
                          AND CABNOM.COD_SIS_ENT = ENT.COD_SIS_ENT_SAL
                          AND DETNOM.COD_EST_AOS = EST.COD_EST_AOS
                     ORDER BY DETNOM.COD_EST_AOS,DETNOM.NUM_FOL_OPE;
                ELSIF  p_TIP_BUS_HIST = v_TPO_BUS_H  THEN
                        OPEN p_CURSOR FOR
                            SELECT DETNOM.NUM_FOL_OPE AS NUMERO_OPERACION,
                              DETNOM.FEC_ISR_OPE AS F_INSERT_OPERACION,
                              DETNOM.IMp_OPE AS MONTO_TOTAL,
                              DETNOM.COD_BCO_DTN AS BIC_BAN_DEST,
                              DECODE (T40.TCNIFENT, NULL, 'BANCO EXTRANJERO', T40.TCNIFENT) AS RUT_BANCO,
                              TBAI.DES_BCO AS GLS_BAN_DEST,
                              DETNOM.NUM_CTA_BFC NUM_CTA_BFC,                                                                                                                                                                                                                                                                                                                                                                                   --DETNOM.NUM_CTA_BFC,
                              DETNOM.COD_DVI,
                              CABNOM.NUM_FOL_NMN,
                              DETNOM.FEC_VTA AS FECHA_VALUTA,
                              CABNOM.COD_USR,
                              ENT.DSC_SIS_ENT_SAL,
                              PKG_PAB_UTILITY.FN_PAB_TRAN_COD_EST_GLOSA(DETNOM.COD_EST_AOS,PKG_PAB_CONSTANTES.V_IND_OPE) AS EST_OPE, --llamar a la funcion
                              DETNOM.FLG_OPE_DUP,   --flag de duplicidad a nivel de operacion
                              EST.DSC_EST_AOS AS DSC_EST_OPE,
                              PKG_PAB_NOMINA.FN_PAB_BUS_ID_GTR(DETNOM.NUM_FOL_OPE,DETNOM.FEC_ISR_OPE,p_TIP_BUS_HIST)       AS NUM_IDF_GTR_DOC-- CAH ID GESTOR DOCUMENTAL
                         FROM PABS_DT_DETLL_NOMNA_HTRCA DETNOM,
                              PABS_DT_SISTM_ENTRD_SALID ENT,
                              PABS_DT_ESTDO_ALMOT EST,
                              PABS_DT_CBCRA_NOMNA_HTRCA CABNOM,
                              TCDT040 T40,
                              TCDTBAI TBAI
                        WHERE DETNOM.NUM_FOL_NMN = p_NUM_FOL_NMN
                          AND DETNOM.COD_BCO_DTN = TBAI.TGCDSWSA(+)
                          AND DETNOM.COD_BCO_DTN = T40.TGCDSWSA(+)
                          AND T40.TCNIFENT (+) <> v_BANCO
                          AND CABNOM.NUM_FOL_NMN = DETNOM.NUM_FOL_NMN
                          AND CABNOM.COD_SIS_ENT = ENT.COD_SIS_ENT_SAL
                          AND DETNOM.COD_EST_AOS = EST.COD_EST_AOS
                        ORDER BY DETNOM.COD_EST_AOS, DETNOM.NUM_FOL_OPE;

                ELSE
                    RAISE ERR_COD_INEX;
                END IF;

          p_ERROR := PKG_PAB_CONSTANTES.v_OK;                                                                                                                                                                                                                                                                                                                                                                                                                               --Procedimiento ejecutado de forma correcta
   EXCEPTION
         WHEN ERR_COD_INEX THEN
        err_msg := 'Se esta invocando con codigo historificacion inexistente:' || p_TIP_BUS_HIST;
        PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (NULL,
                                            v_NOM_PCK,
                                            err_msg,
                                            v_NOM_SP);
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);
   END Sp_PAB_BUS_DET_NOM_CON;

-- ***********************************************************************************************
-- Funcion/Procedimiento: Sp_PAB_BUS_DET_NOMINA_ELI
-- Objetivo: Procedimiento almacenado que retorna las operaciones de una nomina
-- Sistema: PAB
-- Base de Datos: DGBMSEGDB01_NGALTM
-- Tablas Usadas: PABS_DT_DETLL_NOMNA, PABS_DT_SISTM_ENTRD_SALID, PABS_DT_CBCRA_NOMNA, TCDT040, TCDTBAI
-- Fecha: 21/06/16
-- Autor: Santander
-- Input:
-- p_NUM_FOL_NMN -> Numero de la nomina
-- Output:
-- p_CURSOR -> Cursor con las nominas encontradas
-- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
-- Input/Output: N/A
-- Retorno: N/A.
-- Observaciones: <Fecha y Detalle de ?ltimos cambios>
-- ***********************************************************************************************
   PROCEDURE Sp_PAB_BUS_DET_NOMINA_ELI (p_NUM_FOL_NMN IN NUMBER, p_CURSOR OUT SYS_REFCURSOR, p_ERROR OUT NUMBER)
   IS
      v_NOM_SP   VARCHAR2 (30) := 'Sp_PAB_BUS_DET_NOMINA_ELI';
   BEGIN
        OPEN p_CURSOR FOR
              SELECT DETNOM.NUM_FOL_OPE AS NUMERO_OPERACION,
                        DETNOM.FEC_ISR_OPE AS F_INSERT_OPERACION,
                        DETNOM.IMp_OPE AS MONTO_TOTAL,
                        DETNOM.COD_BCO_DTN AS BIC_BAN_DEST,
                        DECODE (T40.TCNIFENT, NULL, 'BANCO EXTRANJERO', T40.TCNIFENT) AS RUT_BANCO,
                        TBAI.DES_BCO AS GLS_BAN_DEST,
                        DETNOM.NUM_CTA_BFC NUM_CTA_BFC,                                                                                                                                                                                                                                                                                                                                                                                   --DETNOM.NUM_CTA_BFC,
                        DETNOM.COD_DVI,
                        CABNOM.NUM_FOL_NMN,
                        DETNOM.FEC_VTA AS FECHA_VALUTA,
                        CABNOM.COD_USR,
                        ENT.DSC_SIS_ENT_SAL,
                        PKG_PAB_UTILITY.FN_PAB_TRAN_COD_EST_GLOSA(DETNOM.COD_EST_AOS, PKG_PAB_CONSTANTES.V_IND_OPE) AS EST_OPE, --llamar a la funcion
                        DETNOM.FLG_OPE_DUP   --flag de duplicidad a nivel de operacion
                   FROM PABS_DT_DETLL_NOMNA DETNOM,
                        PABS_DT_SISTM_ENTRD_SALID ENT,
                        PABS_DT_CBCRA_NOMNA CABNOM,
                        TCDT040 T40,
                        TCDTBAI TBAI
                  WHERE DETNOM.NUM_FOL_NMN = p_NUM_FOL_NMN
                    AND DETNOM.COD_BCO_DTN = TBAI.TGCDSWSA(+)
                    AND DETNOM.COD_BCO_DTN = T40.TGCDSWSA(+)
                    AND T40.TCNIFENT (+) <> v_BANCO
                    AND CABNOM.NUM_FOL_NMN = DETNOM.NUM_FOL_NMN
                    AND CABNOM.COD_SIS_ENT = ENT.COD_SIS_ENT_SAL
                    AND DETNOM.COD_EST_AOS = PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEELI
               ORDER BY DETNOM.COD_EST_AOS,DETNOM.NUM_FOL_OPE;
            p_ERROR := PKG_PAB_CONSTANTES.v_OK;
      EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               err_code := SQLCODE;
               err_msg := SUBSTR (SQLERRM, 1, 300);
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            WHEN OTHERS
            THEN
               err_code := SQLCODE;
               err_msg := SUBSTR (SQLERRM, 1, 300);
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
               p_s_mensaje := err_code || '-' || err_msg;
               RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);
   END Sp_PAB_BUS_DET_NOMINA_ELI;

--***********************************************************************************************
-- Funcion/Procedimiento: Fn_PAB_BUSCA_EST_OPE
-- Objetivo: BUSCA EL ESTADO POR OPERACION Y DEVUELVE EL ESTADO
-- Sistema: PAB
-- Base de Datos: DGBMSEGDB01
-- Tablas Usadas: PABS_DT_CBCRA_NOMNA, PABS_DT_DETLL_NOMNA
-- Fecha: 21/06/16
-- Autor: Santander
-- Input:
-- p_NUM_FOL_OPE -> NUMERO DE FOLIO OPERACION
-- p_FEC_ISR_OPE -> Fecha de Operacion
-- N/A
-- Output:
-- N/A.
-- Input/Output:
-- N/A.
-- Retorno:
-- v_COD_EST_AOS-> CODIGO DEL ESTADO
-- Observaciones: <Fecha y Detalle de ultimos cambios>
--***********************************************************************************************
   FUNCTION Fn_PAB_BUSCA_EST_OPE ( p_NUM_FOL_OPE IN NUMBER, p_FEC_ISR_OPE IN DATE)
   RETURN NUMBER
   IS
      v_NOM_SP   VARCHAR2 (30) := 'Fn_PAB_BUSCA_EST_OPE';
      v_COD_EST_AOS NUMBER;
   BEGIN
               SELECT DETN.COD_EST_AOS INTO v_COD_EST_AOS
                 FROM PABS_DT_DETLL_NOMNA DETN
                WHERE DETN.NUM_FOL_OPE = p_NUM_FOL_OPE
                AND DETN.FEC_ISR_OPE = p_FEC_ISR_OPE;
         --
         RETURN v_COD_EST_AOS;
         --
    EXCEPTION
          WHEN NO_DATA_FOUND
          THEN
             err_code := SQLCODE;
             err_msg := SUBSTR (SQLERRM, 1, 300);
             PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                 v_NOM_PCK,
                                                 err_msg,
                                                 v_NOM_SP);
          WHEN OTHERS
          THEN
             err_code := SQLCODE;
             err_msg := SUBSTR (SQLERRM, 1, 300);
             PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                 v_NOM_PCK,
                                                 err_msg,
                                                 v_NOM_SP);
             p_s_mensaje := err_code || '-' || err_msg;
             RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);
   END Fn_PAB_BUSCA_EST_OPE;

-- ***********************************************************************************************
-- Funcion/Procedimiento: Sp_PAB_BUS_DET_OPE_NOM_CON
-- Objetivo: Procedimiento almacenado que retorna el detalle de una operacion historica seleccionada
-- Base de Datos: DGBMSEGDB01_NGALTM
-- Tablas Usadas: PABS_DT_DETLL_NOMNA
-- Fecha: 21/06/16
-- Autor: Santander
-- Input:
-- p_NUM_FOL_OPE->   foliador del banco, PK tabla operaciones nomina.
-- p_FEC_ISR_OPE->   Fecha de insercion de operacion.
-- p_TIP_BUS_HIST-> tipo busqueda de historificacion
-- Output:
-- p_CURSOR -> Informacion de la operacion
-- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
-- Input/Output: N/A
-- Retorno: N/A.
-- Observaciones: <Fecha y Detalle de ?ltimos cambios>
-- ***********************************************************************************************
PROCEDURE Sp_PAB_BUS_DET_OPE_NOM_CON (  p_NUM_FOL_OPE     IN NUMBER,
                                        p_FEC_ISR_OPE     IN DATE,
                                        p_TIP_BUS_HIST    IN CHAR,
                                        p_CURSOR          OUT SYS_REFCURSOR,
                                        p_ERROR           OUT NUMBER
                                 ) IS
    v_NOM_SP        VARCHAR2(30)    :=  'Sp_PAB_BUS_DET_OPE_NOM_CON';
    v_TPO_BUS_H CHAR := 'H';
    v_TPO_BUS_N CHAR := 'N';
    ERR_COD_INEX EXCEPTION;

    BEGIN

        IF  p_TIP_BUS_HIST = v_TPO_BUS_N THEN
        OPEN p_CURSOR FOR
               SELECT
                      DETOPE.NUM_FOL_OPE          AS NUM_OPE,
                      DETOPE.NUM_REF_SWF          AS REF_SWIFT,
                      DETOPE.COD_TPO_OPE_AOS      AS TIPO_OPERACION,
                      DETOPE.NUM_REF_EXT          AS REF_EXT_SWIFT,
                      DETOPE.COD_MT_SWF           AS COD_MT,
                      DETOPE.COD_DVI              AS DIVISA,
                      SAL.DSC_SIS_ENT_SAL         AS CANAL_SALIDA,
                      DETOPE.COD_BCO_DTN          AS BIC_DESTINO,
                      DETOPC.GLS_DIR_ODN          AS DIRECCION_ORDENANTE,
                      NVL2(DETOPC.NUM_DOC_ODN, TRIM(DETOPC.NUM_DOC_ODN) || '-' || DETOPC.COD_TPD_ODN,NULL) AS DOC_ORDENANTE,
                      DETOPC.NOM_ODN              AS NOMBRE_ORDENANTE,
                      DETOPC.NUM_CTA_ODN          AS CTA_ORDENANTE,
                      DETOPC.NOM_CDD_ODN          AS CIUDAD_ORDENANTE,
                      PKG_PAB_UTILITY.FN_PAB_COMBO_PAIS(DETOPC.COD_PAS_ODN) AS PAIS_ORDENANTE,
                      DETOPC.NUM_CTA_DCv_ODN      AS CTA_DCv_ORDENANTE,
                      DETOPC.COD_BCO_BFC          AS BIC_BENEFICIARIO,
                      NVL2(DETOPE.NUM_DOC_BFC, TRIM(DETOPE.NUM_DOC_BFC) || '-' || DETOPE.COD_TPD_BFC,NULL) AS DOC_BENEFICIARIO,
                      DETOPE.NOM_BFC              AS NOMBRE_BENEFICIARIO,
                      DETOPE.NUM_CTA_BFC          AS CTA_BENEFICIARIO,
                      DETOPC.GLS_DIR_BFC          AS DIRECCION_BENEFICIARIO,
                      DETOPC.NOM_CDD_BFC          AS CIUDAD_BENEFICIARIO,
                      PKG_PAB_UTILITY.FN_PAB_COMBO_PAIS(DETOPC.COD_PAS_BFC) AS PAIS_BENEFICIARIO,
                      DETOPC.NUM_CTA_DCv_BFC      AS CTA_DCv_BENEFICIARIO,
                      DETOPC.COD_BCO_ITD          AS BIC_INTERMEDIARIO,
                      DETOPE.IMp_OPE              AS MONTO_OPE,
                      DETOPE.COD_SUC              AS COD_SUCURSAL,
                      DETOPC.FEC_TPO_OPE_CCLV     AS FECHA_CCLV,
                      PKG_PAB_UTILITY.Fn_PAB_COMBO_CAMARA(DETOPC.COD_TPO_CMA) AS TIPO_CAMARA,
                      DETOPC.NUM_CLv_NGC          AS CLAVE_NEGOCIO,
                      DETOPE.FEC_VTA              AS FECHA_VALUTA,
                      DETOPE.NUM_REF_CTB          AS REF_CONTABLE,
                      PKG_PAB_UTILITY.Fn_PAB_COMBO_TSALDO(DETOPC.COD_TPO_SDO) AS TIPO_SALDO,
                      DETOPC.NUM_AGT              AS NUM_AGENTE,
                      DETOPC.COD_FND_CCLV         AS FONDO_CCLV,
                      DETOPC.OBS_OPC_SWF          AS OBSERVACION_SWIFT,
                      -------------------------------------------------
                      ESTALM.DSC_EST_AOS          AS ESTADO,
                      DETOPE.FEC_ISR_OPE          AS FEC_INS,
                      DETOPE.NUM_FOL_NMN          AS NUM_NOM,
                      DETOPE.NUM_OPE_SIS_ENT      AS NUM_OPE_ENT,
                      DECODE(DETOPE.FLG_EGR_ING, PKG_PAB_CONSTANTES.v_FLG_EGR,
                                                 PKG_PAB_CONSTANTES.v_STR_FLG_EGR,
                                                 PKG_PAB_CONSTANTES.v_STR_FLG_ING)  AS EGRESO_INGRESO,
                      DETOPE.COD_SIS_SAL          AS COD_CANAL_ENT,
                      ENT.DSC_SIS_ENT_SAL         AS CANAL_ENTRADA,
                      DETOPE.COD_SIS_SAL          AS COD_CANAL_SAL,
                      DECODE(DETOPE.COD_TPO_ING,  PKG_PAB_CONSTANTES.v_COD_TIp_ING_MAS,
                                                  PKG_PAB_CONSTANTES.v_STR_TIp_ING_MAS,
                                                  PKG_PAB_CONSTANTES.V_COD_TIP_ING_UNI,
                                                  PKG_PAB_CONSTANTES.V_STR_TIP_ING_UNI,
                                                  PKG_PAB_CONSTANTES.V_COD_TIP_ING_DIR,
                                                  PKG_PAB_CONSTANTES.V_STR_TIP_ING_DIR
                                                  ) AS TIPO_INGRESO,
                      DETOPE.FEC_ENv_RCp_SWF      AS FECHA_SWIFT,
                      DETOPE.HOR_ENv_RCp_SWF      AS HORA_SWIFT,
                      DECODE(DETOPE.FLG_MRD_UTZ_SWF,PKG_PAB_CONSTANTES.v_FLG_MRD_MN,
                                                PKG_PAB_CONSTANTES.v_STR_FLG_MRD_MN,
                                                PKG_PAB_CONSTANTES.v_STR_FLG_MRD_MX) AS MERCADO,
                      DECODE(DETOPE.COD_EST_AOS,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEREC,DETOPC.GLS_EST_RCH,
                                                  PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT,DETOPC.GLS_MTv_VSD,
                                                  DETOPE.GLS_ADC_EST) AS GLOSA_ESTADO,
                      DETOPC.NUM_DOC_BFC          AS DOC_BENEFICIARIO_OPC,
                      DETOPC.COD_TPD_BFC          AS TIPO_DOC_BENEFICIARIO_OPC,
                      DETOPC.COD_TPO_FND          AS CODIGO_FONDO,
                      DETOPC.NUM_CLv_IDF          AS NUMERO_IDENTIFICATORIO,
                      DETOPC.GLS_EST_RCH          AS GLOSA_RECHAZO,
                      DETOPC.GLS_MTv_VSD          AS GLOSA_VISADA,
                      PKG_PAB_CONSTANTES.V_BIC_BANCO_SANTANDER  AS BIC_ORIGEN
                FROM PABS_DT_DETLL_NOMNA DETOPE,
                     PABS_DT_NOMNA_INFCN_OPCON DETOPC,
                     PABS_DT_ESTDO_ALMOT ESTALM,
                     PABS_DT_SISTM_ENTRD_SALID ENT,
                     PABS_DT_SISTM_ENTRD_SALID SAL,
                     PABS_DT_CBCRA_NOMNA CABNOM
                WHERE DETOPE.NUM_FOL_OPE = p_NUM_FOL_OPE
                AND DETOPE.FEC_ISR_OPE  = p_FEC_ISR_OPE
                AND DETOPE.NUM_FOL_OPE = DETOPC.NUM_FOL_OPE(+)
                AND DETOPE.FEC_ISR_OPE = DETOPC.FEC_ISR_OPE(+)
                AND DETOPE.COD_EST_AOS = ESTALM.COD_EST_AOS
                AND DETOPE.COD_SIS_SAL = SAL.COD_SIS_ENT_SAL(+)
                AND CABNOM.NUM_FOL_NMN = DETOPE.NUM_FOL_NMN
                AND CABNOM.COD_SIS_ENT = ENT.COD_SIS_ENT_SAL;
        ELSIF  p_TIP_BUS_HIST = v_TPO_BUS_H THEN

          OPEN p_CURSOR FOR
                  SELECT
                  DETOPEHT.NUM_FOL_OPE          AS NUM_OPE,
                  DETOPEHT.NUM_REF_SWF          AS REF_SWIFT,
                  DETOPEHT.COD_TPO_OPE_AOS      AS TIPO_OPERACION,
                  DETOPEHT.NUM_REF_EXT          AS REF_EXT_SWIFT,
                  DETOPEHT.COD_MT_SWF           AS COD_MT,
                  DETOPEHT.COD_DVI              AS DIVISA,
                  SAL.DSC_SIS_ENT_SAL         AS CANAL_SALIDA,
                  DETOPEHT.COD_BCO_DTN          AS BIC_DESTINO,
                  DETOPCHT.GLS_DIR_ODN          AS DIRECCION_ORDENANTE,
                  NVL2(DETOPCHT.NUM_DOC_ODN, TRIM(DETOPCHT.NUM_DOC_ODN) || '-' || DETOPCHT.COD_TPD_ODN,NULL) AS DOC_ORDENANTE,
                  DETOPCHT.NOM_ODN              AS NOMBRE_ORDENANTE,
                  DETOPCHT.NUM_CTA_ODN          AS CTA_ORDENANTE,
                  DETOPCHT.NOM_CDD_ODN          AS CIUDAD_ORDENANTE,
                  PKG_PAB_UTILITY.FN_PAB_COMBO_PAIS(DETOPCHT.COD_PAS_ODN) AS PAIS_ORDENANTE,
                  DETOPCHT.NUM_CTA_DCv_ODN      AS CTA_DCv_ORDENANTE,
                  DETOPCHT.COD_BCO_BFC          AS BIC_BENEFICIARIO,
                  NVL2(DETOPEHT.NUM_DOC_BFC, TRIM(DETOPEHT.NUM_DOC_BFC) || '-' || DETOPEHT.COD_TPD_BFC,NULL) AS DOC_BENEFICIARIO,
                  DETOPEHT.NOM_BFC              AS NOMBRE_BENEFICIARIO,
                  DETOPEHT.NUM_CTA_BFC          AS CTA_BENEFICIARIO,
                  DETOPCHT.GLS_DIR_BFC          AS DIRECCION_BENEFICIARIO,
                  DETOPCHT.NOM_CDD_BFC          AS CIUDAD_BENEFICIARIO,
                  PKG_PAB_UTILITY.FN_PAB_COMBO_PAIS(DETOPCHT.COD_PAS_BFC) AS PAIS_BENEFICIARIO,
                  DETOPCHT.NUM_CTA_DCv_BFC      AS CTA_DCv_BENEFICIARIO,
                  DETOPCHT.COD_BCO_ITD          AS BIC_INTERMEDIARIO,
                  DETOPEHT.IMp_OPE              AS MONTO_OPE,
                  DETOPEHT.COD_SUC              AS COD_SUCURSAL,
                  DETOPCHT.FEC_TPO_OPE_CCLV     AS FECHA_CCLV,
                  PKG_PAB_UTILITY.Fn_PAB_COMBO_CAMARA(DETOPCHT.COD_TPO_CMA) AS TIPO_CAMARA,
                  DETOPCHT.NUM_CLv_NGC          AS CLAVE_NEGOCIO,
                  DETOPEHT.FEC_VTA              AS FECHA_VALUTA,
                  DETOPEHT.NUM_REF_CTB          AS REF_CONTABLE,
                  PKG_PAB_UTILITY.Fn_PAB_COMBO_TSALDO(DETOPCHT.COD_TPO_SDO) AS TIPO_SALDO,
                  DETOPCHT.NUM_AGT              AS NUM_AGENTE,
                  DETOPCHT.COD_FND_CCLV         AS FONDO_CCLV,
                  DETOPCHT.OBS_OPC_SWF          AS OBSERVACION_SWIFT,
                  -------------------------------------------------
                  ESTALM.DSC_EST_AOS          AS ESTADO,
                  DETOPEHT.FEC_ISR_OPE          AS FEC_INS,
                  DETOPEHT.NUM_FOL_NMN          AS NUM_NOM,
                  DETOPEHT.NUM_OPE_SIS_ENT      AS NUM_OPE_ENT,
                  DECODE(DETOPEHT.FLG_EGR_ING, PKG_PAB_CONSTANTES.v_FLG_EGR,
                                             PKG_PAB_CONSTANTES.v_STR_FLG_EGR,
                                             PKG_PAB_CONSTANTES.v_STR_FLG_ING)  AS EGRESO_INGRESO,
                  DETOPEHT.COD_SIS_SAL          AS COD_CANAL_ENT,
                  ENT.DSC_SIS_ENT_SAL         AS CANAL_ENTRADA,
                  DETOPEHT.COD_SIS_SAL          AS COD_CANAL_SAL,
                  DECODE(DETOPEHT.COD_TPO_ING,PKG_PAB_CONSTANTES.v_COD_TIp_ING_MAS,
                                                  PKG_PAB_CONSTANTES.v_STR_TIp_ING_MAS,
                                                  PKG_PAB_CONSTANTES.V_COD_TIP_ING_UNI,
                                                  PKG_PAB_CONSTANTES.V_STR_TIP_ING_UNI,
                                                  PKG_PAB_CONSTANTES.V_COD_TIP_ING_DIR,
                                                  PKG_PAB_CONSTANTES.V_STR_TIP_ING_DIR
                                                  ) AS TIPO_INGRESO,
                  DETOPEHT.FEC_ENv_RCp_SWF      AS FECHA_SWIFT,
                  DETOPEHT.HOR_ENv_RCp_SWF      AS HORA_SWIFT,
                  DECODE(DETOPEHT.FLG_MRD_UTZ_SWF,PKG_PAB_CONSTANTES.v_FLG_MRD_MN,
                                            PKG_PAB_CONSTANTES.v_STR_FLG_MRD_MN,
                                            PKG_PAB_CONSTANTES.v_STR_FLG_MRD_MX) AS MERCADO,
                  DECODE(DETOPEHT.COD_EST_AOS,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEREC,DETOPCHT.GLS_EST_RCH,
                                                  PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT,DETOPCHT.GLS_MTv_VSD,
                                                  DETOPEHT.GLS_ADC_EST) AS GLOSA_ESTADO,
                  DETOPCHT.NUM_DOC_BFC          AS DOC_BENEFICIARIO_OPC,
                  DETOPCHT.COD_TPD_BFC          AS TIPO_DOC_BENEFICIARIO_OPC,
                  DETOPCHT.COD_TPO_FND          AS CODIGO_FONDO,
                  DETOPCHT.NUM_CLv_IDF          AS NUMERO_IDENTIFICATORIO,
                  DETOPCHT.GLS_EST_RCH          AS GLOSA_RECHAZO,
                  DETOPCHT.GLS_MTv_VSD          AS GLOSA_VISADA,
                  PKG_PAB_CONSTANTES.V_BIC_BANCO_SANTANDER  AS BIC_ORIGEN
            FROM PABS_DT_DETLL_NOMNA_HTRCA DETOPEHT,
                 PABS_DT_NOMNA_OPCON_HTRCA DETOPCHT,
                 PABS_DT_ESTDO_ALMOT ESTALM,
                 PABS_DT_SISTM_ENTRD_SALID ENT,
                 PABS_DT_SISTM_ENTRD_SALID SAL,
                 PABS_DT_CBCRA_NOMNA_HTRCA CABNOMHT
            WHERE DETOPEHT.NUM_FOL_OPE = p_NUM_FOL_OPE
              AND DETOPEHT.FEC_ISR_OPE = p_FEC_ISR_OPE
              AND DETOPEHT.NUM_FOL_OPE = DETOPCHT.NUM_FOL_OPE(+)
              AND DETOPEHT.FEC_ISR_OPE = DETOPCHT.FEC_ISR_OPE(+)
              AND DETOPEHT.COD_EST_AOS = ESTALM.COD_EST_AOS
              AND DETOPEHT.COD_SIS_SAL = SAL.COD_SIS_ENT_SAL(+)
              AND CABNOMHT.NUM_FOL_NMN = DETOPEHT.NUM_FOL_NMN
              AND CABNOMHT.COD_SIS_ENT = ENT.COD_SIS_ENT_SAL;
        ELSE
            RAISE ERR_COD_INEX;
        END IF;

        p_ERROR := PKG_PAB_CONSTANTES.v_OK; --Procedimiento ejecutado de forma correcta

    EXCEPTION
          WHEN ERR_COD_INEX THEN
                err_msg := 'Se esta invocando con codigo historificacion inexistente:' || p_TIP_BUS_HIST;
                PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (NULL, v_NOM_PCK, err_msg, v_NOM_SP);
          WHEN NO_DATA_FOUND THEN
                    err_code := SQLCODE;
                    err_msg := SUBSTR(SQLERRM, 1, 300);
                    PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                    p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
          WHEN OTHERS THEN
                    err_code := SQLCODE;
                    err_msg := SUBSTR(SQLERRM, 1, 300);
                    PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                    p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                    p_s_mensaje := err_code || '-' || err_msg;
                    RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);

    END Sp_PAB_BUS_DET_OPE_NOM_CON;

--**********************************************************************************************
-- Funcion/Procedimiento: SP_PAB_ELI_MOT_NO_HABLT
-- Objetivo: Procedimiento que elimina registro de errores encontrados por las validaciones de integracion
-- en las n?minas
-- Sistema: PAB
-- Base de Datos: DGBMSEGDB01_NGALTM
-- Tablas Usadas: PABS_DT_MOTVO_NO_HABLT
-- Fecha: 19/08/16
-- Autor: Santander
-- Input:
-- p_NUM_FOL_OPE -> Foliador del banco
-- p_FEC_ISR_OPE -> Fecha de una Operacion
-- Output:
-- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
-- Input/Output: N/A
-- Retorno: N/A.
-- Observaciones: <Fecha y Detalle de utimos cambios>
--***********************************************************************************************
  PROCEDURE SP_PAB_ELI_MOT_NO_HABLT(  p_NUM_FOL_OPE  IN NUMBER
                                    , p_FEC_ISR_OPE IN DATE
                                    , p_ERROR       OUT NUMBER
                                    ) IS

    v_NOM_SP    VARCHAR2 (30) := 'SP_PAB_ELI_MOT_NO_HABLT';

    BEGIN

        p_ERROR := PKG_PAB_CONSTANTES.v_OK; --Procedimiento ejecutado de forma correcta

        DELETE FROM PABS_DT_MOTVO_NO_HABLT
        WHERE NUM_FOL_OPE = p_NUM_FOL_OPE
          AND FEC_ISR_OPE = p_FEC_ISR_OPE;

    EXCEPTION
        WHEN OTHERS THEN
            err_code := SQLCODE;
            err_msg := SUBSTR(SQLERRM, 1, 300);
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
            p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
            p_s_mensaje := err_code || '-' || err_msg;
            RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);

    END SP_PAB_ELI_MOT_NO_HABLT;

--**********************************************************************************************
-- Funcion/Procedimiento: Sp_PAB_INS_ERR_VAL
-- Objetivo: Procedimiento que registra los errores encontrados por las validaciones de integracion
-- en las n?minas
-- Sistema: PAB
-- Base de Datos: DGBMSEGDB01_NGALTM
-- Tablas Usadas: PABS_DT_MOTVO_NO_HABLT
-- Fecha: 19/08/16
-- Autor: Santander
-- Input:
-- p_FEC_ISR_OPE -> Fecha de una Operacion
-- p_NUM_FOL_OPE -> Foliador del banco
-- p_COD_ARC_ENT ->Codigo Archivo de Entrada
-- p_COD_CAM_ARC -> Codigo camara
-- p_COD_ERR_BIB -> Codigo error
-- Output:
-- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
-- Input/Output: N/A
-- Retorno: N/A.
-- Observaciones: <Fecha y Detalle de utimos cambios>
--***********************************************************************************************
  PROCEDURE Sp_PAB_INS_ERR_VAL( p_FEC_ISR_OPE  IN DATE
                              , p_NUM_FOL_OPE  IN NUMBER
                              , p_COD_ARC_ENT  IN CHAR
                              , p_COD_CAM_ARC  IN NUMBER
                              , p_COD_ERR_BIB  IN CHAR
                              , p_ERROR        OUT NUMBER
                            ) IS


    v_NOM_SP    VARCHAR2 (30)   := 'Sp_PAB_INS_ERR_VAL';
    v_FEC_ISR_DET_OPE_NM    DATE;

    BEGIN

        p_ERROR := PKG_PAB_CONSTANTES.v_OK; --Procedimiento ejecutado de forma correcta

        -- Si es nulo el parametro p_FEC_ISR_OPE se debe buscar la fecha en la tabla detalle de operaciones nomina.
        IF p_FEC_ISR_OPE IS NULL THEN

            BEGIN

                SELECT FEC_ISR_OPE
                INTO v_FEC_ISR_DET_OPE_NM
                FROM PABS_DT_DETLL_NOMNA
                WHERE NUM_FOL_OPE = p_NUM_FOL_OPE;

            EXCEPTION

                WHEN NO_DATA_FOUND THEN
                    err_code := SQLCODE;
                    err_msg := SUBSTR (SQLERRM, 1, 300);
                    PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code, v_NOM_PCK, err_msg, v_NOM_SP);
                    p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;

                WHEN OTHERS THEN
                    err_code := SQLCODE;
                    err_msg := SUBSTR(SQLERRM, 1, 300);
                    PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                    p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                    p_s_mensaje := err_code || '-' || err_msg;
                    RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);

            END;

        ELSE
            v_FEC_ISR_DET_OPE_NM := p_FEC_ISR_OPE;

        END IF;

        INSERT INTO PABS_DT_MOTVO_NO_HABLT
            ( NUM_COR_MVO
            , FEC_ISR_OPE
            , NUM_FOL_OPE
            , COD_ARC_ENT
            , COD_CAM_ARC
            , COD_ERR_BIB
            )
        VALUES (
            SPK_PAB_MOTVO_NO_HABLT.NEXTVAL
            , v_FEC_ISR_DET_OPE_NM
            , p_NUM_FOL_OPE
            , p_COD_ARC_ENT
            , p_COD_CAM_ARC
            , p_COD_ERR_BIB
            );

    EXCEPTION
        WHEN OTHERS THEN
            err_code := SQLCODE;
            err_msg := SUBSTR(SQLERRM, 1, 300);
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
            p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
            p_s_mensaje := err_code || '-' || err_msg;
            RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);

    END Sp_PAB_INS_ERR_VAL;

--**********************************************************************************************
-- Funcion/Procedimiento: Sp_PAB_OBT_CAM_INV_OPE
-- Objetivo: Procedimiento que devuelve los campos invalidos que tiene una operacion
-- que existen entre los tipos de mensajes, tipo de operaciones y  sistema de salida.
-- Sistema: PAB
-- Base de Datos: DGBMSEGDB01_NGALTM
-- Tablas Usadas: PABS_DT_MOTVO_NO_HABLT, PABS_DT_BIBLI_ERROR_ALMOT, PABS_DT_CAMPO_ARCHV_ENTRD
-- PABS_DT_ESTRT_ARCHV_ENTRD
-- Fecha: 23/09/2016
-- Autor: Santander
-- Input:
-- p_NUM_FOL_OPE -> Numero de Operacion
-- p_FEC_ISR_OPE -> Fecha de Operacion
-- Output:
-- p_CURSOR  -> Cursor con todao los registros decampos invalidos de la opracion
-- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
-- Input/Output: N/A
-- Retorno: N/A.
-- Observaciones: <Fecha y Detalle de utimos cambios>
--***********************************************************************************************
  PROCEDURE SP_PAB_OBT_CAM_INV_OPE( p_NUM_FOL_OPE   IN NUMBER,
                                    p_FEC_ISR_OPE   IN DATE,
                                    p_CURSOR        OUT SYS_REFCURSOR,
                                    p_ERROR         OUT NUMBER
                                    ) IS

    v_NOM_SP    VARCHAR2 (30) := 'SP_PAB_OBT_CAM_INV_OPE';

    BEGIN

        p_ERROR := PKG_PAB_CONSTANTES.v_OK; --Procedimiento ejecutado de forma correcta

        OPEN p_CURSOR FOR
              SELECT
                MNH.FEC_ISR_OPE     AS FECHA_INSERCION
              , MNH.NUM_FOL_OPE     AS NUM_OPER
              , MNH.COD_ARC_ENT     AS COD_ARCHIVO
              , EAE.NOM_TAG_CAM_ARC AS TAG_XML
              , MNH.COD_CAM_ARC     AS POS_CAMPO
              , CAE.NOM_CAM_ARC     AS NOMBRE_CAMPO
              , MNH.COD_ERR_BIB     AS CODIGO_ERROR
              , BEA.GLS_ERR_BIB     AS GLOSA_ERROR
              , BEA.GLS_SOL_BIB     AS GLOSA_SOLU
            FROM PABS_DT_MOTVO_NO_HABLT MNH
              , PABS_DT_BIBLI_ERROR_ALMOT BEA
              , PABS_DT_CAMPO_ARCHV_ENTRD CAE
              , PABS_DT_ESTRT_ARCHV_ENTRD EAE
            WHERE MNH.NUM_FOL_OPE = p_NUM_FOL_OPE
            AND MNH.FEC_ISR_OPE = p_FEC_ISR_OPE
            AND MNH.COD_CAM_ARC = CAE.COD_CAM_ARC
            AND MNH.COD_ERR_BIB = BEA.COD_ERR_BIB
            AND MNH.COD_CAM_ARC = EAE.COD_CAM_ARC
            ;

    EXCEPTION
          WHEN NO_DATA_FOUND THEN
                err_code := SQLCODE;
                err_msg := SUBSTR (SQLERRM, 1, 300);
                PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code, v_NOM_PCK, err_msg, v_NOM_SP);
                p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
          WHEN OTHERS THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code, v_NOM_PCK, err_msg, v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            p_s_mensaje := err_code || '-' || err_msg;
            RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);

    END SP_PAB_OBT_CAM_INV_OPE;


--***********************************************************************************************
-- Funcion/Procedimiento: FN_PAB_CONT_EST
-- Objetivo: Cuenta por numero de operacion y estado de la misma
-- Sistema: PAB
-- Base de Datos: DGBMSEGDB01
-- Tablas Usadas: PABS_DT_DETLL_NOMNA, PABS_DT_CBCRA_NOMNA
-- Fecha: 21/06/16
-- Autor: Santander
-- Input:
-- p_NUM_FOL_NMN -> folio de nomina
-- p_STR_EST_AOS  -> String de estado de la operacion
-- N/A
-- Output:
-- N/A.
-- Input/Output:
-- N/A.
-- Retorno:
-- PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEINV -> string de salida invalida
-- PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEVAL -> string de salida valida
-- Observaciones: <Fecha y Detalle de ?ltimos cambios>
--***********************************************************************************************
 FUNCTION FN_PAB_CONT_EST (   p_NUM_FOL_NMN     IN NUMBER,
                              p_STR_EST_AOS     IN CHAR
                            ) RETURN VARCHAR2 IS
      v_NOM_SP        VARCHAR2(20) := 'FN_PAB_CONT_EST';
      v_CANT_EST_OPE  NUMBER;
      v_COD_EST_AOS   NUMBER;
   BEGIN
       --
       CASE
            WHEN p_STR_EST_AOS = PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEVAL  THEN
                v_COD_EST_AOS := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEVAL;
            WHEN p_STR_EST_AOS = PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEINV  THEN
                v_COD_EST_AOS := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEINV;
       END CASE;
           --
            SELECT  COUNT(1)
             INTO  v_CANT_EST_OPE
             FROM  PABS_DT_DETLL_NOMNA DETN
                    ,PABS_DT_CBCRA_NOMNA CABN
            WHERE   DETN.NUM_FOL_NMN  = CABN.NUM_FOL_NMN AND
                    DETN.COD_EST_AOS  = v_COD_EST_AOS AND
                    DETN.NUM_FOL_NMN  = p_NUM_FOL_NMN ;
           --
        RETURN v_CANT_EST_OPE;

    EXCEPTION
          WHEN NO_DATA_FOUND
          THEN
             err_code := SQLCODE;
             err_msg := SUBSTR (SQLERRM, 1, 300);
             PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                 v_NOM_PCK,
                                                 err_msg,
                                                 v_NOM_SP);
             RETURN 0;
          WHEN OTHERS
          THEN
             err_code := SQLCODE;
             err_msg := SUBSTR (SQLERRM, 1, 300);
             PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                 v_NOM_PCK,
                                                 err_msg,
                                                 v_NOM_SP);
             --p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
             p_s_mensaje := err_code || '-' || err_msg;
             RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);
    END FN_PAB_CONT_EST;

-- ***********************************************************************************************
-- Funcion/Procedimiento: Sp_PAB_TOT_DVI_NMN_MULTI_HIST
-- Objetivo: Procedimiento que retorna el total de las operaciones Historicas agrupados por sus monedas
-- Sistema: PAB
-- Base de Datos: DGBMSEGDB01_NGALTM
-- Tablas Usadas: PABS_DT_CBCRA_NOMNA_HTRCA, PABS_DT_DETLL_NOMNA_HTRCA, PABS_DT_ESTDO_ALMOT
-- Fecha: 21/06/16
-- Autor: Santander
-- Input:
-- p_NUM_FOL_NMN -> Numero de la nomina
-- p_TIP_BUS_HIST -> Tipo de busqueda historica
-- Output:
-- p_CURSOR -> SYS_REFCURSOR, resultados de consulta.
-- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
-- Input/Output: N/A
-- Retorno: N/A.
-- Observaciones: <Fecha y Detalle de ?ltimos cambios>
-- ***********************************************************************************************
PROCEDURE Sp_PAB_TOT_DVI_NMN_MULTI_HIST (p_NUM_FOL_NMN IN NUMBER, p_TIP_BUS_HIST IN CHAR, p_CURSOR OUT SYS_REFCURSOR, p_ERROR OUT NUMBER)
IS
    v_NOM_SP   VARCHAR2 (30) := 'Sp_PAB_TOT_DVI_NMN_MULTI_HIST';
    v_TPO_BUS_H CHAR := 'H';
    v_TPO_BUS_N CHAR := 'N';
    ERR_COD_INEX EXCEPTION;
BEGIN
        CASE
            WHEN  p_TIP_BUS_HIST = v_TPO_BUS_N THEN
                OPEN p_CURSOR FOR
                   SELECT NOMNA.FLG_DVI,
                          DETNOM.NUM_FOL_NMN,
                          SUM (DETNOM.IMp_OPE) IMp_TOT_NMN,
                          NOMNA.COD_USR COD_USR_CRC,
                          NOMNA.NOM_NMN,
                          DETNOM.COD_DVI,
                          ESTA.DSC_EST_AOS
                     FROM PABS_DT_CBCRA_NOMNA NOMNA,
                          PABS_DT_DETLL_NOMNA DETNOM,
                          PABS_DT_ESTDO_ALMOT ESTA
                    WHERE NOMNA.NUM_FOL_NMN = p_NUM_FOL_NMN AND
                          NOMNA.NUM_FOL_NMN = DETNOM.NUM_FOL_NMN AND
                          DETNOM.COD_EST_AOS = ESTA.COD_EST_AOS
                 GROUP BY DETNOM.COD_DVI,
                          DETNOM.NUM_FOL_NMN,
                          NOMNA.COD_USR,
                          NOMNA.NOM_NMN,
                          NOMNA.FLG_DVI,
                          ESTA.DSC_EST_AOS;
            WHEN  p_TIP_BUS_HIST = v_TPO_BUS_H THEN
                OPEN p_CURSOR FOR
                   SELECT NOMNA.FLG_DVI,
                          DETNOM.NUM_FOL_NMN,
                          SUM (DETNOM.IMp_OPE) IMp_TOT_NMN,
                          NOMNA.COD_USR COD_USR_CRC,
                          NOMNA.NOM_NMN,
                          DETNOM.COD_DVI,
                          ESTA.DSC_EST_AOS
                     FROM PABS_DT_CBCRA_NOMNA_HTRCA NOMNA,
                          PABS_DT_DETLL_NOMNA_HTRCA DETNOM,
                          PABS_DT_ESTDO_ALMOT ESTA
                    WHERE NOMNA.NUM_FOL_NMN = p_NUM_FOL_NMN AND
                          NOMNA.NUM_FOL_NMN = DETNOM.NUM_FOL_NMN AND
                          DETNOM.COD_EST_AOS = ESTA.COD_EST_AOS
                 GROUP BY DETNOM.COD_DVI,
                          DETNOM.NUM_FOL_NMN,
                          NOMNA.COD_USR,
                          NOMNA.NOM_NMN,
                          NOMNA.FLG_DVI,
                          ESTA.DSC_EST_AOS;
            ELSE
                RAISE ERR_COD_INEX;
            END CASE;
    --
    p_ERROR := PKG_PAB_CONSTANTES.v_OK;
    --                                                                                                                                                                                                                                                                                                                                                                                                                             --Procedimiento ejecutado de forma correcta
EXCEPTION
    WHEN ERR_COD_INEX THEN
        err_msg := 'Se esta invocando con codigo historificacion inexistente:' || p_TIP_BUS_HIST;
        PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (NULL,
                                            v_NOM_PCK,
                                            err_msg,
                                            v_NOM_SP);
    WHEN NO_DATA_FOUND
        THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                 v_NOM_PCK,
                                                 err_msg,
                                                 v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
    WHEN OTHERS
        THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                 v_NOM_PCK,
                                                 err_msg,
                                                 v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
        p_s_mensaje := err_code || '-' || err_msg;
        RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);

    END Sp_PAB_TOT_DVI_NMN_MULTI_HIST;


--**********************************************************************************************
-- Funcion/Procedimiento: Sp_PAB_GES_VAL_HORA_MONDA
-- Objetivo: Procedimiento que llama a los SP que validan e informan ventana horaria de sistema
-- y monto maximo importe segun moneda
-- Sistema: PAB
-- Base de Datos: DGBMSEGDB01_NGALTM
-- Tablas Usadas: N/A
-- Fecha: 19/08/16
-- Autor: Santander
-- Input:
-- p_COD_USR -> Codigo de usuario
-- p_COD_DVI -> Codigo Divisa
-- p_IMP_INF -> Importe informado para consultar en tabla de moneda
-- Output:
-- p_RESP_DET -> Detalle de respuesta para validaciones de horario y sistema
-- p_RESP_VAL -> Flag cumple con las condiciones de hora y monto importe maximo -> 0 cumple y 1 no cumple
-- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
-- (Se ven en la tabla Log estos errores)
-- Input/Output: N/A
-- Retorno: N/A.
-- Observaciones: <Fecha y Detalle de ultimos cambios>
--***********************************************************************************************
  PROCEDURE Sp_PAB_GES_VAL_HORA_MONDA(  p_COD_USR   IN CHAR,
                                        p_COD_DVI   IN CHAR,
                                        p_IMP_INF   IN NUMBER,
                                        p_RESP_DET  OUT VARCHAR2,
                                        p_RESP_VAL  OUT NUMBER,
                                        p_ERROR     OUT NUMBER
                                        ) IS

    v_NOM_SP    VARCHAR2 (30) := 'Sp_PAB_GES_VAL_HORA_MONDA';
    v_FLG_SIS_HAB   NUMBER;
    v_FLG_IMP_VAL   NUMBER;
    v_COD_SIS_ENT   CHAR(10);
    v_ERROR         NUMBER;

    BEGIN

        p_ERROR := PKG_PAB_CONSTANTES.v_OK; --Procedimiento ejecutado de forma correcta

        --Rescatamos canal de entrada del usuario
        PKG_PAB_UTILITY.Sp_PAB_OBT_CAN_ENT_USU(p_COD_USR,v_COD_SIS_ENT,v_ERROR);


        PKG_PAB_UTILITY.Sp_PAB_VAL_HOR_SIS(v_COD_SIS_ENT,
                                            v_FLG_SIS_HAB,
                                            p_ERROR
                                            );

        PKG_PAB_UTILITY.Sp_PAB_VAL_MONDA_ALMOT(p_COD_DVI,
                                                p_IMP_INF,
                                                v_FLG_IMP_VAL,
                                                p_ERROR);

        IF v_FLG_SIS_HAB = PKG_PAB_CONSTANTES.V_error AND v_FLG_IMP_VAL = PKG_PAB_CONSTANTES.V_OK THEN
            p_RESP_DET := 'Operacion Fuera de Horario';
            p_RESP_VAL := PKG_PAB_CONSTANTES.V_error;

        ELSIF v_FLG_IMP_VAL = PKG_PAB_CONSTANTES.V_error AND v_FLG_SIS_HAB = PKG_PAB_CONSTANTES.V_OK THEN
            p_RESP_DET := 'Operacion Supera Monto Maximo';
            p_RESP_VAL := PKG_PAB_CONSTANTES.V_error;

        ELSIF v_FLG_IMP_VAL = PKG_PAB_CONSTANTES.V_error AND v_FLG_IMP_VAL = PKG_PAB_CONSTANTES.V_error THEN
            p_RESP_DET := 'Operacion Fuera de Horario y Excede Monto';
            p_RESP_VAL := PKG_PAB_CONSTANTES.V_error;

        ELSE
            p_RESP_DET := 'Operacion Cumple con Horario y Monto Maximo';
            p_RESP_VAL := PKG_PAB_CONSTANTES.V_OK;

        END IF;

    EXCEPTION
          WHEN OTHERS THEN
            err_code := SQLCODE;
            err_msg := SUBSTR(SQLERRM, 1, 300);
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
            p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
            p_s_mensaje := err_code || '-' || err_msg;
            RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);

    END Sp_PAB_GES_VAL_HORA_MONDA;

--******************************************************************************
-- //@Santander
-- Funcion/Procedimiento: Sp_PAB_ACT_CAB_NOMINA_ELI
-- Objetivo: Procedimiento que actualiza la informacion de la cabecera de la nomina en estado eliminada
-- Sistema: PAB
-- Base de Datos: DGBMSEGDB01_NGALTM
-- Tablas Usadas: PABS_DT_DETLL_NOMNA
-- Fecha: 21/06/16
-- Autor: Santander
-- Input:
-- p_NUM_FOL_NMN -> foliador de la nomina
-- p_GLS_MTv_EST -> Informacion adicional del estado
-- Output:
-- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
-- Input/Output: N/A
-- Retorno: N/A.
-- Observaciones: <Fecha y Detalle de ?ltimos cambios>
-- ***********************************************************************************************
   PROCEDURE Sp_PAB_ACT_CAB_NOMINA_ELI (p_NUM_FOL_NMN IN NUMBER, p_ERROR OUT NUMBER)
   IS
      --Seteo de Variables
      v_NOM_SP         VARCHAR2 (30) := 'Sp_PAB_ACT_CAB_NOMINA_ELI';
      v_IMp_TOT_NMN    NUMBER;
      v_FLG_DVI        NUMBER;
      v_CANT_MONEDAS   NUMBER;
      V_RESUL_OPE_DUP  NUMBER;
   BEGIN
      BEGIN

        --Obtenemos la cantidad de monedas
        SELECT COUNT (COD_DVI)
        INTO v_CANT_MONEDAS
        FROM PABS_DT_DETLL_NOMNA
        WHERE NUM_FOL_NMN = p_NUM_FOL_NMN
        GROUP BY COD_DVI;

         --Una moneda
         v_FLG_DVI := PKG_PAB_CONSTANTES.v_FLG_DVI_UNA;

         --Obtenemos el monto total de la moneda
         BEGIN
            SELECT SUM (IMp_OPE)
              INTO v_IMp_TOT_NMN
              FROM PABS_DT_DETLL_NOMNA
             WHERE NUM_FOL_NMN = p_NUM_FOL_NMN;

         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               err_code := SQLCODE;
               err_msg := SUBSTR (SQLERRM, 1, 300);
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
               RAISE;
            WHEN OTHERS
            THEN
               err_code := SQLCODE;
               err_msg := SUBSTR (SQLERRM, 1, 300);
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;

               p_s_mensaje := err_code || '-' || err_msg;
               RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);
         END;
      EXCEPTION
         WHEN TOO_MANY_ROWS
         THEN
            --Multimoneda
            v_FLG_DVI := PKG_PAB_CONSTANTES.v_FLG_DVI_MULTI;
            v_IMp_TOT_NMN := 0;
         WHEN NO_DATA_FOUND
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            RAISE;
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            p_s_mensaje := err_code || '-' || err_msg;
            RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);
      END;

      BEGIN

        --Actualizamos la informacion de la cabecera
        UPDATE PABS_DT_CBCRA_NOMNA
           SET IMp_TOT_NMN = v_IMp_TOT_NMN,
               FLG_DVI = v_FLG_DVI
         WHERE NUM_FOL_NMN = p_NUM_FOL_NMN;

      EXCEPTION
         WHEN DUP_VAL_ON_INDEX
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            RAISE;
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            p_s_mensaje := err_code || '-' || err_msg;
            RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);
      END;

      p_ERROR := PKG_PAB_CONSTANTES.v_OK;

   EXCEPTION
      WHEN NO_DATA_FOUND THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ok;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);

   END Sp_PAB_ACT_CAB_NOMINA_ELI;

--**********************************************************************************************
-- Funcion/Procedimiento: SP_PAB_DEV_NOM_COM
-- Objetivo: Procedimiento que devuelve datos de cabecera de una nomina y todos sus detalles
-- Sistema: PAB
-- Base de Datos: DGBMSEGDB01_NGALTM
-- Tablas Usadas:
-- Fecha: 05/12/2016
-- Autor: Santander
-- Input:
-- p_NUM_FOL_NMN
-- Output:
-- p_CURSOR_CBCRA
-- p_CURSOR_DETLL_OPE
-- p_ERROR -> Indicador de errores (Se ven en la tabla Log estos errores)
-- Input/Output: N/A
-- Retorno: N/A.
-- Observaciones: <Fecha y Detalle de utimos cambios>
--***********************************************************************************************
 PROCEDURE SP_PAB_DEV_NOM_COM  (p_NUM_FOL_NMN IN NUMBER, p_CURSOR_CBCRA OUT SYS_REFCURSOR, p_CURSOR_DETLL_OPE OUT SYS_REFCURSOR, p_ERROR OUT NUMBER) IS

   v_NOM_SP   VARCHAR2 (30) := 'SP_PAB_DEV_NOM_COM';
   v_cod_tip_pro char(3) := 'RVL';

   BEGIN
                --Buscamos la nomina
                OPEN p_CURSOR_CBCRA FOR
                       SELECT
                           v_cod_tip_pro   AS TIPO_PROCESO,
                           CABNOM.NUM_FOL_NMN AS NUM_NOM,
                           CABNOM.NOM_NMN AS NOMBRE_NOMINA,
                           CABNOM.COD_SIS_ENT AS CANAL,
                           CABNOM.COD_USR AS USUARIO,
                           CABNOM.IMP_TOT_NMN AS MONTO_TOTAL,
                           CABNOM.FLG_DVI AS TIPO_MONEDA,
                           CABNOM.COD_EST_AOS AS ESTADO
                       FROM PABS_DT_CBCRA_NOMNA CABNOM,
                            PABS_DT_ESTDO_ALMOT EST
                       WHERE CABNOM.COD_EST_AOS = EST.COD_EST_AOS
                       AND CABNOM.NUM_FOL_NMN = p_NUM_FOL_NMN;

                P_ERROR := PKG_PAB_CONSTANTES.V_OK; --Procedimiento ejecutado de forma correcta

                OPEN p_CURSOR_DETLL_OPE FOR
                        SELECT
                        DETNOM.FEC_ISR_OPE            AS FEC_INS,
                        DETNOM.NUM_FOL_OPE            AS NUM_OPE,
                        DETNOM.NUM_FOL_NMN            AS NUM_NOM,
                        DETNOM.NUM_OPE_SIS_ENT        AS NUM_OPE_ENT,
                        DETNOM.COD_SIS_SAL            AS COD_CANAL_SAL,
                        DETNOM.FEC_ENv_RCp_SWF        AS FECHA_SWIFT,
                        DETNOM.HOR_ENv_RCp_SWF        AS HORA_SWIFT,
                        DETNOM.COD_MT_SWF             AS COD_MT,
                        DETNOM.COD_TPO_OPE_AOS        AS TIPO_OPERACION,
                        DETNOM.FEC_VTA                AS FECHA_PAGO,
                        DETNOM.COD_DVI                AS DIVISA,
                        DETNOM.IMp_OPE                AS MONTO_TOTAL,
                        DETNOM.COD_BCO_DTN            AS BIC_DESTINO,
                        DETNOM.NUM_DOC_BFC            AS DOC_BENEFICIARIO,
                        DETNOM.COD_TPD_BFC            AS TIPO_DOC_BENEFICIARIO,
                        DETNOM.NOM_BFC                AS BENEFICIARIO,
                        DETNOM.NUM_CTA_BFC            AS CTA_BENEFICIARIO,
                        DETNOM.COD_SUC                AS COD_SUCURSAL,
                        DETNOM.NUM_REF_SWF            AS REF_SWIFT,
                        DETNOM.NUM_REF_EXT            AS REF_EXT_SWIFT,
                        DETNOM.GLS_ADC_EST            AS GLOSA_ESTADO,
                        DETNOMOP.COD_BCO_BFC          AS BIC_BENEFICIARIO,
                        DETNOMOP.COD_BCO_ITD          AS BIC_INTERMEDIARIO,
                        DETNOMOP.NUM_DOC_BFC          AS DOC_BENEFICIARIO_OPC,
                        DETNOMOP.COD_TPD_BFC          AS TIPO_DOC_BENEFICIARIO_OPC,
                        DETNOMOP.NUM_CTA_DCv_BFC      AS CTA_DCv_BENEFICIARIO,
                        DETNOMOP.GLS_DIR_BFC          AS DIRECCION_BENEFICIARIO,
                        DETNOMOP.NOM_CDD_BFC          AS CIUDAD_BENEFICIARIO,
                        DETNOMOP.COD_PAS_BFC          AS PAIS_BENEFICIARIO,
                        DETNOMOP.NOM_ODN              AS NOMBRE_ORDENANTE,
                        DETNOMOP.NUM_DOC_ODN          AS DOC_ORDENANTE,
                        DETNOMOP.COD_TPD_ODN          AS TIPO_DOC_ORDENANTE,
                        DETNOMOP.NUM_CTA_ODN          AS CTA_ORDENANTE,
                        DETNOMOP.NUM_CTA_DCv_ODN      AS CTA_DCv_ORDENANTE,
                        DETNOMOP.GLS_DIR_ODN          AS DIRECCION_ORDENANTE,
                        DETNOMOP.NOM_CDD_ODN          AS CIUDAD_ORDENANTE,
                        DETNOMOP.COD_PAS_ODN          AS PAIS_ORDENANTE,
                        DETNOMOP.NUM_CLv_NGC          AS CLAVE_NEGOCIO,
                        DETNOMOP.NUM_AGT              AS NUM_AGENTE,
                        DETNOMOP.COD_FND_CCLV         AS FONDO_CCLV,
                        DETNOMOP.COD_TPO_SDO          AS TIPO_SALDO,
                        DETNOMOP.COD_TPO_CMA          AS TIPO_CAMARA,
                        DETNOMOP.COD_TPO_FND          AS CODIGO_FONDO,
                        DETNOMOP.FEC_TPO_OPE_CCLV     AS FECHA_CCLV,
                        DETNOMOP.NUM_CLv_IDF          AS NUMERO_IDENTIFICATORIO,
                        DETNOMOP.GLS_EST_RCH          AS GLOSA_RECHAZO,
                        DETNOMOP.GLS_MTv_VSD          AS GLOSA_VISADA,
                        DETNOMOP.OBS_OPC_SWF          AS OBSERVACION_SWIFT
                  FROM PABS_DT_DETLL_NOMNA DETNOM,
                       PABS_DT_NOMNA_INFCN_OPCON DETNOMOP
                  WHERE
                       DETNOM.NUM_FOL_NMN  = p_NUM_FOL_NMN
                       AND DETNOM.NUM_FOL_OPE = DETNOMOP.NUM_FOL_OPE
                       AND DETNOM.FEC_ISR_OPE = DETNOMOP.FEC_ISR_OPE
                       AND DETNOM.COD_EST_AOS NOT IN (PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEELI);

                    P_ERROR := PKG_PAB_CONSTANTES.V_OK; --Procedimiento ejecutado de forma correcta

                 EXCEPTION

                WHEN NO_DATA_FOUND THEN

                    err_code := SQLCODE;
                    err_msg := SUBSTR(SQLERRM, 1, 300);
                    PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                    p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                    RAISE;

                WHEN OTHERS THEN
                    err_code := SQLCODE;
                    err_msg := SUBSTR(SQLERRM, 1, 300);
                    p_s_mensaje := err_code || '-' || err_msg;
                    PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                    p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                    RAISE_APPLICATION_ERROR(-20000,p_s_mensaje);


   END SP_PAB_DEV_NOM_COM;

-- ***********************************************************************************************
-- Funcion/Procedimiento: Sp_PAB_ACT_CAB_NOMINA
-- Objetivo: Procedimiento que actualiza la informacion de la cabecera de la nomina
-- Sistema: PAB
-- Base de Datos: DGBMSEGDB01_NGALTM
-- Tablas Usadas: PABS_DT_DETLL_NOMNA
-- Fecha: 21/06/16
-- Autor: Santander
-- Input:
-- p_NUM_FOL_NMN -> foliador de la nomina
-- p_EST_NMN -> Indica el estado de la nomina 0)Valida 1)Invalida
-- Output:
-- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
-- Input/Output: N/A
-- Retorno: N/A.
-- Observaciones: <Fecha y Detalle de ?ltimos cambios>
-- ***********************************************************************************************
   PROCEDURE Sp_PAB_ACT_CAB_NOMINA (p_NUM_FOL_NMN IN NUMBER,  p_EST_NMN IN NUMBER,p_ERROR OUT NUMBER)
   IS
      --Seteo de Variables
      v_NOM_SP         VARCHAR2 (30) := 'Sp_PAB_ACT_CAB_NOMINA';
      v_IMp_TOT_NMN    NUMBER;
      v_FLG_DVI        NUMBER;
      v_CANT_MONEDAS   NUMBER;
      v_RESUL_OPE_DUP  NUMBER;
      v_GLS_MTV_EST    VARCHAR2(300):= NULL;
      V_EST_NMN        NUMBER(5);
    BEGIN

        BEGIN

            --Obtenemos la cantidad de monedas
            SELECT COUNT( COD_DVI)
            INTO v_CANT_MONEDAS
            FROM PABS_DT_DETLL_NOMNA
            WHERE NUM_FOL_NMN = p_NUM_FOL_NMN
            AND COD_EST_AOS <> PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEELI --No consideramos las eliminadas
            GROUP BY COD_DVI;

             --Una moneda
             v_FLG_DVI := PKG_PAB_CONSTANTES.v_FLG_DVI_UNA;

             --Obtenemos el monto total de la moneda
             BEGIN
                SELECT SUM (IMp_OPE)
                  INTO v_IMp_TOT_NMN
                  FROM PABS_DT_DETLL_NOMNA
                 WHERE NUM_FOL_NMN = p_NUM_FOL_NMN
                 AND COD_EST_AOS <> PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEELI; --No consideramos las eliminadas

             EXCEPTION
                WHEN NO_DATA_FOUND
                THEN
                   err_code := SQLCODE;
                   err_msg := 'No se pudo obtener el monto total de la Nomina:' || p_NUM_FOL_NMN;
                   PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                       v_NOM_PCK,
                                                       err_msg,
                                                       v_NOM_SP);
                   p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
                   RAISE;
                WHEN OTHERS
                THEN
                   err_code := SQLCODE;
                   err_msg := SUBSTR (SQLERRM, 1, 300);
                   PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                       v_NOM_PCK,
                                                       err_msg,
                                                       v_NOM_SP);
                   p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;

                   p_s_mensaje := err_code || '-' || err_msg;
                   RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);
             END;
        EXCEPTION
            WHEN TOO_MANY_ROWS THEN

                --Multimoneda
                v_FLG_DVI := PKG_PAB_CONSTANTES.v_FLG_DVI_MULTI;
                v_IMp_TOT_NMN := 0;

            WHEN NO_DATA_FOUND THEN

                err_code := SQLCODE;
                err_msg := 'No se encontraron operaciones distinto de eliminada para la nomina:' || p_NUM_FOL_NMN;
                p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;

                RAISE;
        WHEN OTHERS THEN

            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            p_s_mensaje := err_code || '-' || err_msg;
            RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);
        END;

        --Si la nomina se encuentra valida, verificamos si existen duplicados
        IF(p_EST_NMN = PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNVAL ) THEN

             --Verificamos si existen duplicados
            v_GLS_MTV_EST := 'Nomina Valida...';

            PKG_PAB_NOMINA.SP_PAB_VERIF_NOM_DUPL(p_NUM_FOL_NMN,v_RESUL_OPE_DUP, P_ERROR);

            IF(v_RESUL_OPE_DUP = PKG_PAB_CONSTANTES.V_COD_DUP_OK) THEN
                --
                V_EST_NMN := PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNDUP;
            ELSE
                V_EST_NMN := p_EST_NMN;
            END IF;

        END IF;

        --Si la nomina va a quedar invalida
        IF(p_EST_NMN = PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNINV) THEN

            v_GLS_MTV_EST := 'Nomina con problemas en validacion';
            V_EST_NMN := p_EST_NMN;
        END IF;

        --Si la nomina esta en Proceso
        IF(p_EST_NMN = PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNPRC) THEN

            v_GLS_MTV_EST := 'Nomina en Proceso de Validacion...';
            V_EST_NMN := p_EST_NMN;

        END IF;

        BEGIN

            --Actualizamos la informacion de la cabecera
            UPDATE PABS_DT_CBCRA_NOMNA
            SET IMp_TOT_NMN = v_IMp_TOT_NMN,
                COD_EST_AOS = NVL(V_EST_NMN,COD_EST_AOS), --Si viene nulo el cambio no
                FLG_DVI = v_FLG_DVI,
                GLS_MTV_EST = NVL(v_GLS_MTV_EST,GLS_MTV_EST)
            WHERE NUM_FOL_NMN = p_NUM_FOL_NMN;

        EXCEPTION
            WHEN DUP_VAL_ON_INDEX THEN

                err_code := SQLCODE;
                err_msg := SUBSTR (SQLERRM, 1, 300);
                PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                    v_NOM_PCK,
                                                    err_msg,
                                                    v_NOM_SP);
                p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
                RAISE;

            WHEN OTHERS THEN
                err_code := SQLCODE;
                err_msg := SUBSTR (SQLERRM, 1, 300);
                PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                    v_NOM_PCK,
                                                    err_msg,
                                                    v_NOM_SP);
                p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
                p_s_mensaje := err_code || '-' || err_msg;
                RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);
        END;

      p_ERROR := PKG_PAB_CONSTANTES.v_OK;

    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;

        WHEN OTHERS THEN
        err_code := SQLCODE;
        err_msg := SUBSTR (SQLERRM, 1, 300);
        PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                         v_NOM_PCK,
                                         err_msg,
                                         v_NOM_SP);
        p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
        p_s_mensaje := err_code || '-' || err_msg;
        RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);

    END Sp_PAB_ACT_CAB_NOMINA;

/************************************************************************************************
-- Funcion/Procedimiento: FN_PAB_BUS_ID_GTR
-- Objetivo: Funcion que retorna el codigo indentificatorio del gestor documental
-- Sistema: DBO_PAB.
-- Base de Datos: DBO_PAB.
-- Tablas Usadas: N/A 
-- Fecha: 31/05/2017
-- Autor: Santander CAH
-- Input:     
-- p_NUM_FOL_OPE->   foliador del banco, PK tabla operaciones nomina.
-- p_FEC_ISR_OPE->   Fecha de insercion de la operacion.
-- p_TIP_BUS_HIST-> Tipo de busqueda H o N
-- Output:
--          Retorna el codigo de gestor documental
-- Input/Output: N/A
-- Retorno: N/A.
-- Observaciones: <Fecha y Detalle de ultimos cambios>
--***********************************************************************************************/

   FUNCTION FN_PAB_BUS_ID_GTR (p_NUM_FOL_OPE  IN NUMBER, p_FEC_ISR_OPE IN DATE,p_TIP_BUS_HIST IN CHAR)
      RETURN VARCHAR2
         IS

      v_NUM_IDF_GTR_DOC   VARCHAR2 (40);
      v_NOM_SP    VARCHAR2 (30) := 'FN_PAB_BUS_ID_GTR';
      v_TPO_BUS_HIS CHAR := 'H';
      v_TPO_BUS_NOR CHAR := 'N';

    BEGIN


        IF p_TIP_BUS_HIST = v_TPO_BUS_NOR THEN

            --Busca Numero identificatorio del Gestor Documental
            SELECT OP.NUM_IDF_GTR_DOC INTO v_NUM_IDF_GTR_DOC
            FROM PABS_DT_DETLL_OPRCN OP
            WHERE OP.NUM_FOL_OPE = p_NUM_FOL_OPE
            AND OP.FEC_ISR_OPE = p_FEC_ISR_OPE;

            RETURN v_NUM_IDF_GTR_DOC;

        ELSE --p_TIP_BUS_HIST = v_TPO_BUS_HIS THEN

            SELECT OPH.NUM_IDF_GTR_DOC INTO v_NUM_IDF_GTR_DOC
            FROM PABS_DT_DETLL_OPRCN_HTRCA OPH
            WHERE OPH.NUM_FOL_OPE = p_NUM_FOL_OPE
            AND OPH.FEC_ISR_OPE = p_FEC_ISR_OPE;

            RETURN v_NUM_IDF_GTR_DOC;

        END IF;

    EXCEPTION
        WHEN NO_DATA_FOUND
        THEN

          v_NUM_IDF_GTR_DOC:= NULL;
          RETURN v_NUM_IDF_GTR_DOC;


        WHEN OTHERS
        THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);

    END FN_PAB_BUS_ID_GTR;

--***********************************************************************************************
-- Funcion/Procedimiento: FN_PAB_BUS_EST_NOM
-- Objetivo: Comprara el estado de una nomina con la que figura en la tabla de nominas.
-- Sistema: PAB
-- Base de Datos: DGBMSEGDB01
-- Tablas Usadas: PABS_DT_DETLL_NOMNA
-- Fecha: 09/11/2016
-- Autor: Santander
-- Input:
-- P_NUM_FOL_NMN -> numero de nomina
-- P_COD_EST_AOS -> codigo estado nomina
-- N/A
-- Output:
-- N/A.
-- Input/Output:
-- N/A.
-- Retorno:
-- P_FLAG_EST -> valor de retorno flag de comparacion
-- Observaciones: <Fecha y Detalle de ?ltimos cambios>
--***********************************************************************************************
   FUNCTION FN_PAB_BUS_EST_NOM (P_NUM_FOL_NMN   IN NUMBER,
                                P_COD_EST_AOS   IN CHAR)
      RETURN NUMBER
   IS
      --
      v_NOM_SP           VARCHAR2 (22) := 'FN_PAB_BUS_EST_NOM';
      P_FLAG_EST         NUMBER;
      V_COD_EST_AOS      NUMBER;
      V_COD_EST_RESULT   NUMBER;
   BEGIN
      --
      V_COD_EST_RESULT :=
         DBO_PAB.PKG_PAB_UTILITY.FN_PAB_OBT_COD_DSC_EST (
            P_COD_EST_AOS,
            PKG_PAB_CONSTANTES.V_IND_NMN
         );
      --
      BEGIN
         SELECT   NOM.COD_EST_AOS
           INTO   V_COD_EST_AOS
           FROM   PABS_DT_CBCRA_NOMNA NOM
          WHERE   NOM.NUM_FOL_NMN = P_NUM_FOL_NMN
                  AND NOM.COD_EST_AOS = V_COD_EST_RESULT;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            V_COD_EST_AOS := 0;
         WHEN OTHERS
         THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;

      --
      IF V_COD_EST_AOS = V_COD_EST_RESULT
      THEN
         P_FLAG_EST := 0;
      ELSE
         P_FLAG_EST := 1;
      END IF;

      --
      RETURN P_FLAG_EST;
   --
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   --
   END FN_PAB_BUS_EST_NOM;

END PKG_PAB_NOMINA;