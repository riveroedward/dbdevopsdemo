CREATE OR REPLACE PACKAGE BODY         PKG_PAB_PERFIL
IS
   /**  Package el cual contrendra todas las funciones y procedimientos relacionados con los perfiles
   -- @Santander */

   /************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUS_MENU_USU
   -- Objetivo: Procedimiento que obitne el menu del usaurio segun su perfil y area
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_USRIO
   --        , PABS_DT_TIPO_ROL_SISTM_ENTRD
   --        , PABS_DT_RELCN_ROL_MENU
   --        , PABS_DT_MENU_ALMOT
   -- Fecha: 02/06/16
   -- Autor: Santander
   -- Input: p_COD_USU->    CODIGO DEL USUARIO
   -- Output:
   --      p_CURSOR:   Cursor de salida con datos del menu por usuario.
   --          p_ERROR:  indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_BUS_MENU_USU (p_COD_USU   IN     CHAR,
                                  p_CURSOR       OUT SYS_REFCURSOR,
                                  p_ERROR        OUT NUMBER)
   IS
   BEGIN
      v_NOM_SP := 'Sp_PAB_BUS_MENU_USU';
      v_COD_USU := UPPER (p_COD_USU);

      OPEN p_CURSOR FOR
           SELECT   MEN.COD_MNS_AOS,
                    MEN.NOM_MNS_AOS,
                    MEN.DSC_MNS_AOS,
                    NVL (MEN.COD_MNS_PAD_AOS, 0) AS COD_MNS_PAD_AOS,
                    MEN.NUM_NVL_PFD,
                    MEN.NUM_ORD_VSN,
                    MEN.GLS_PTH_MNS_AOS
             FROM   PABS_DT_USRIO USU,
                    PABS_DT_TIPO_ROL_SISTM_ENTRD TROL,
                    PABS_DT_RELCN_ROL_MENU ROLM,
                    PABS_DT_MENU_ALMOT MEN
            WHERE       USU.COD_TPO_ROL_AOS = TROL.COD_TPO_ROL_AOS
                    AND USU.COD_SIS_ENT = TROL.COD_SIS_ENT
                    ------------------------------------------------
                    AND TROL.COD_SIS_ENT = ROLM.COD_SIS_ENT
                    AND TROL.COD_TPO_ROL_AOS = ROLM.COD_TPO_ROL_AOS
                    ------------------------------------------------
                    AND MEN.COD_MNS_AOS = ROLM.COD_MNS_AOS
                    ------------------------------------------------
                    AND MEN.FLG_VGN = PKG_PAB_CONSTANTES.v_REG_VIG
                    AND ROLM.FLG_VGN = PKG_PAB_CONSTANTES.v_REG_VIG
                    AND TROL.FLG_VGN = PKG_PAB_CONSTANTES.v_REG_VIG
                    AND USU.FLG_VGN = PKG_PAB_CONSTANTES.v_REG_VIG
                    AND USU.COD_USR = v_COD_USU        -- Parametro de entrada
         ORDER BY   MEN.NUM_NVL_PFD,
                    MEN.NUM_ORD_VSN,
                    MEN.COD_MNS_AOS,
                    ROLM.FLG_VGN;

      --
      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   -- Este commit solo afecta a lo que sucede en este procedimiento.
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg :=
            'No se han encontrado registos asociados a la busqueda por usuario:'
            || p_COD_USU;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_BUS_MENU_USU;

   /************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_CON_EXIS_USU
   -- Objetivo: Procedimiento que muestra si el usuario esta vigente y habilitado para funcionar en el aplicativo.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_USRIO
   --        , PABS_DT_TIPO_ROL_SISTM_ENTRD
   --        , PABS_DT_TIPO_ROL_ALMOT
   --        , PABS_DT_SISTM_ENTRD_SALID
   -- Fecha: 02/06/16
   -- Autor: Santander
   -- Input: p_COD_USU ->    CODIGO DEL USUARIO
   -- Output:
   --      p_CURSOR: Cursor de salida con datos usuario vigente.
   --          p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_CON_EXIS_USU (p_COD_USU   IN     CHAR,
                                  p_CURSOR       OUT SYS_REFCURSOR,
                                  p_ERROR        OUT NUMBER)
   IS
   BEGIN
      v_NOM_SP := 'Sp_PAB_CON_EXIS_USU';
      v_COD_USU := UPPER (p_COD_USU);

      OPEN p_CURSOR FOR
         SELECT   USU.COD_USR,
                  USU.COD_SIS_ENT,
                  CANENT.DSC_SIS_ENT_SAL,
                  TROL.NOM_TPO_ROL_AOS,
                  TROL.COD_TPO_ROL_AOS ROL
           FROM   PABS_DT_USRIO USU,
                  PABS_DT_TIPO_ROL_SISTM_ENTRD TROLSI,
                  PABS_DT_TIPO_ROL_ALMOT TROL,
                  PABS_DT_SISTM_ENTRD_SALID CANENT
          WHERE       USU.COD_TPO_ROL_AOS = TROLSI.COD_TPO_ROL_AOS
                  AND USU.COD_SIS_ENT = TROLSI.COD_SIS_ENT
                  AND TROLSI.COD_TPO_ROL_AOS = TROL.COD_TPO_ROL_AOS
                  AND CANENT.COD_SIS_ENT_SAL = USU.COD_SIS_ENT
                  AND USU.COD_USR = v_COD_USU
                  AND USU.FLG_VGN = PKG_PAB_CONSTANTES.v_REG_VIG
                  AND TROLSI.FLG_VGN = PKG_PAB_CONSTANTES.v_REG_VIG
                  AND TROL.FLG_VGN = PKG_PAB_CONSTANTES.v_REG_VIG
                  AND CANENT.FLG_VGN = PKG_PAB_CONSTANTES.v_REG_VIG;

      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg :=
            'No se han encontrado registos asociados a la busqueda por usuario:'
            || p_COD_USU;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_CON_EXIS_USU;

   /******************************************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUS_PERFIL
   -- Objetivo: Procedimiento almacenado que retorna lista de usuarios de altos montos.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_USRIO
   --
   -- Fecha: 03-01-2018
   -- Autor: Santander CAH
   -- Input:
   --  p_COD_USR           Rut usuario
   --   p_COD_SIS_ENT       Cod. sistema entrada eje:OOFF
   -- Output:
   --       p_CURSOR        Notificaciones del día
   --       p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --******************************************************************************************************************************/
   PROCEDURE Sp_PAB_BUS_PERFIL (p_COD_USR       IN     CHAR,
                                p_COD_SIS_ENT   IN     CHAR,
                                p_CURSOR           OUT SYS_REFCURSOR,
                                p_ERROR            OUT NUMBER)
   IS
      v_NOM_SP        VARCHAR2 (30) := 'Sp_PAB_BUS_PERFIL';
      v_COD_USR       CHAR (11);
      v_COD_SIS_ENT   CHAR (10);
   --v_COD_TPO_ROL_AOS   NUMBER;

   BEGIN
      --Rut usuario
      IF p_COD_USR = '-1'
      THEN
         v_COD_USR := NULL;
      ELSE
         v_COD_USR := p_COD_USR;
      END IF;

      --Area usuario
      IF p_COD_SIS_ENT = '-1'
      THEN
         v_COD_SIS_ENT := NULL;
      ELSE
         v_COD_SIS_ENT := p_COD_SIS_ENT;
      END IF;

      --Rol usuario
      --        IF p_COD_TPO_ROL_AOS = -1 THEN
      --            v_COD_TPO_ROL_AOS := NULL;
      --        ELSE
      --            v_COD_TPO_ROL_AOS := p_COD_TPO_ROL_AOS;
      --        END IF;

      OPEN p_CURSOR FOR
           SELECT   US.COD_USR AS RUT_USU,
                    SIS.DSC_SIS_ENT_SAL AS AREA_USU,
                    TROL.NOM_TPO_ROL_AOS AS PERFIL_USU,
                    US.COD_SIS_ENT AS SISTEMA
             FROM   PABS_DT_USRIO US,
                    PABS_DT_SISTM_ENTRD_SALID SIS,
                    PABS_DT_TIPO_ROL_ALMOT TROL
            WHERE       US.COD_SIS_ENT = SIS.COD_SIS_ENT_SAL
                    AND US.COD_TPO_ROL_AOS = TROL.COD_TPO_ROL_AOS
                    AND US.COD_USR = NVL (v_COD_USR, COD_USR)
                    AND US.COD_SIS_ENT = NVL (v_COD_SIS_ENT, COD_SIS_ENT)
                    AND US.FLG_VGN = PKG_PAB_CONSTANTES.V_FLG_VGN_SI
         ORDER BY   COD_SIS_ENT;

      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   --Procedimiento ejecutado de forma correcta
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg :=
            'No se han encontrado registos asociados a la busqueda por usuario:'
            || p_COD_USR;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);

         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_BUS_PERFIL;

   /******************************************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ACT_PERFIL
   -- Objetivo: Procedimiento almacenado que actuliza el perfil de un usuario
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_USRIO
   --
   -- Fecha: 03-01-2018
   -- Autor: Santander CAH
   -- Input:
   --  p_COD_USR_PAD       Rut usuario creación
   --   p_COD_USR           Rut usuario
   --   p_COD_SIS_ENT       Cod. sistema entrada
   --   p_COD_TPO_ROL_AOS  Rol de usuario
   -- Output:
   --       p_CURSOR        Notificaciones del día
   --       p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --******************************************************************************************************************************/
   PROCEDURE Sp_PAB_ACT_PERFIL (p_COD_USR_PAD       IN     CHAR,
                                p_COD_USR           IN     CHAR,
                                p_COD_SIS_ENT       IN     CHAR,
                                p_COD_TPO_ROL_AOS   IN     NUMBER,
                                p_ERROR                OUT NUMBER)
   IS
      v_NOM_SP            VARCHAR2 (30) := 'Sp_PAB_ACT_PERFIL';
      v_COD_USR           CHAR (11);
      v_COD_SIS_ENT       CHAR (10);
      v_COD_TPO_ROL_AOS   NUMBER (2);
      v_COD_USR_ULT_MOD   CHAR (8);
      v_FEC_ULT_MOD       DATE := SYSDATE;
   BEGIN
      v_COD_USR_ULT_MOD := SUBSTR (p_COD_USR_PAD, 1, 8);

      p_ERROR := PKG_PAB_CONSTANTES.v_OK;

      BEGIN
         UPDATE   PABS_DT_USRIO US
            SET   US.COD_TPO_ROL_AOS = p_COD_TPO_ROL_AOS,
                  US.COD_SIS_ENT = p_COD_SIS_ENT,
                  US.COD_USR_ULT_MOD = v_COD_USR_ULT_MOD,
                  US.FEC_ULT_MOD = v_FEC_ULT_MOD
          WHERE   US.COD_USR = p_COD_USR;

         COMMIT;
      EXCEPTION
         WHEN DUP_VAL_ON_INDEX
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            P_ERROR := PKG_PAB_CONSTANTES.V_ERROR;
            PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (err_code,
                                                V_NOM_PCK,
                                                err_msg,
                                                V_NOM_SP);
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            p_s_mensaje := err_code || '-' || err_msg;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;
   END Sp_PAB_ACT_PERFIL;

   /******************************************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ELI_PERFIL
   -- Objetivo: Procedimiento almacenado que elimina un usuario de altos montos.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_USRIO
   --
   -- Fecha: 03-01-2018
   -- Autor: Santander CAH
   -- Input:
   --  p_COD_USR           Rut usuario
   --   p_COD_SIS_ENT       Cod. sistema entrada
   --   p_COD_TPO_ROL_AOS   Rol usuario ingresado
   -- Output:
   --       p_CURSOR        Notificaciones del día
   --       p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --******************************************************************************************************************************/
   PROCEDURE Sp_PAB_ELI_PERFIL (p_COD_USR           IN     CHAR,
                                p_COD_SIS_ENT       IN     CHAR,
                                p_COD_TPO_ROL_AOS   IN     NUMBER,
                                p_ERROR                OUT NUMBER)
   IS
      v_NOM_SP            VARCHAR2 (30) := 'Sp_PAB_ELI_PERFIL';
      v_COD_USR           CHAR (11);
      v_COD_SIS_ENT       CHAR (10);
      v_COD_TPO_ROL_AOS   NUMBER (2);
      v_DES_ELI           VARCHAR2 (300);
   BEGIN
      p_ERROR := PKG_PAB_CONSTANTES.v_OK;

      DELETE   PABS_DT_USRIO
       WHERE   COD_USR = p_COD_USR AND COD_SIS_ENT = p_COD_SIS_ENT;

      COMMIT;
      v_DES_ELI :=
         'Se ha eliminado al usuario ' || p_COD_USR || ', el día ' || SYSDATE;
      err_code := 0;

      -- Dejamos una traza del usuario eliminado
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                          v_NOM_PCK,
                                          v_DES_ELI,
                                          v_NOM_SP);
   EXCEPTION
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_ELI_PERFIL;

   /******************************************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_INS_PERFIL
   -- Objetivo: Procedimiento almacenado que inserta un nuevo usuario en altos montos.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_USRIO
   --
   -- Fecha: 03-01-2018
   -- Autor: Santander CAH
   -- Input:
   --  p_COD_USR_PAD       Rut usuario creación
   --  p_COD_USR           Rut usuario
   --   p_COD_SIS_ENT       Cod. sistema entrada
   --   p_COD_TPO_ROL_AOS   Rol usuario ingresado
   -- Output:
   --       p_CURSOR        Notificaciones del día
   --       p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --******************************************************************************************************************************/
   PROCEDURE Sp_PAB_INS_PERFIL (p_COD_USR_PAD       IN     CHAR,
                                p_COD_USR           IN     CHAR,
                                p_COD_SIS_ENT       IN     CHAR,
                                p_COD_TPO_ROL_AOS   IN     NUMBER,
                                p_ERROR                OUT NUMBER)
   IS
      v_NOM_SP            VARCHAR2 (30) := 'Sp_PAB_INS_PERFIL';
      v_COD_USR           CHAR (11);
      v_COD_SIS_ENT       CHAR (10);
      v_COD_TPO_ROL_AOS   NUMBER (2);
      v_COD_USR_ULT_MOD   CHAR (8);
   BEGIN
      v_COD_USR_ULT_MOD := SUBSTR (p_COD_USR_PAD, 1, 8);

      BEGIN
         INSERT INTO PABS_DT_USRIO (COD_USR,
                                    COD_SIS_ENT,
                                    COD_TPO_ROL_AOS,
                                    COD_USR_PAD,
                                    FLG_VGN,
                                    COD_USR_CRC,
                                    FEC_CRC,
                                    COD_USR_ULT_MOD,
                                    FEC_ULT_MOD)
           VALUES   (p_COD_USR,
                     p_COD_SIS_ENT,
                     p_COD_TPO_ROL_AOS,
                     p_COD_USR_PAD,
                     PKG_PAB_CONSTANTES.V_FLG_VGN_SI,             --p_FLG_VGN,
                     v_COD_USR_ULT_MOD,                       --p_COD_USR_CRC,
                     SYSDATE,                                    -- p_FEC_CRC,
                     v_COD_USR_ULT_MOD,                   --p_COD_USR_ULT_MOD,
                     NULL                                      --p_FEC_ULT_MOD
                         );

         p_ERROR := PKG_PAB_CONSTANTES.v_OK;
      EXCEPTION
         WHEN DUP_VAL_ON_INDEX
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            P_ERROR := PKG_PAB_CONSTANTES.V_ERROR_KEY_DUPLICATE;
            PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (err_code,
                                                V_NOM_PCK,
                                                err_msg,
                                                V_NOM_SP);
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            p_s_mensaje := err_code || '-' || err_msg;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;
   END Sp_PAB_INS_PERFIL;

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_COMBO_AREA
   -- Objetivo: Procedimiento que busca Area
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_SISTM_ENTRD_SALID
   -- Fecha: 03/07/2019
   -- Autor: NEORIS
   -- Input:
   -- N/A
   -- Output:
   -- p_CURSOR -> Cursor con los registros de los 2 estados
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************                                    );
   PROCEDURE SP_PAB_COMBO_AREA (p_CURSOR   OUT SYS_REFCURSOR,
                                p_ERROR    OUT NUMBER)
   IS
      V_NOM_SP   VARCHAR2 (30) := 'SP_PAB_COMBO_AREA';
   BEGIN
      P_ERROR := PKG_PAB_CONSTANTES.V_OK;

      OPEN p_CURSOR FOR
           SELECT   COD_SIS_ENT_SAL AS CODIGO, DSC_SIS_ENT_SAL AS AREA
             FROM   PABS_DT_SISTM_ENTRD_SALID
            WHERE   FLG_SIS_ENT_SAL IN
                          (PKG_PAB_CONSTANTES.V_FLG_SIS_ENT,
                           PKG_PAB_CONSTANTES.V_FLG_SIS_ENT_SAL_OTRO)
                    AND COD_SIS_ENT_SAL NOT IN
                             (PKG_PAB_CONSTANTES.V_COD_TIP_CTACTELBTR,
                              PKG_PAB_CONSTANTES.V_COD_TIP_LBTRCOMB,
                              PKG_PAB_CONSTANTES.V_COD_TIP_OB,
                              PKG_PAB_CONSTANTES.V_COD_TIP_CAJA)
                    AND FLG_VGN = PKG_PAB_CONSTANTES.V_FLG_VGN_SI
         ORDER BY   DSC_SIS_ENT_SAL;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);

         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_COMBO_AREA;

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_COMBO_CANAL_CAM_SAL
   -- Objetivo: Procedimiento que busca Area
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_SISTM_ENTRD_SALID
   -- Fecha: 03/07/2019
   -- Autor: NEORIS
   -- Input:
   -- N/A
   -- Output:
   -- p_CURSOR -> Cursor con los registros de los 2 estados
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************                                    );
   PROCEDURE Sp_PAB_COMBO_CANAL_CAM_SAL (p_CURSOR   OUT SYS_REFCURSOR,
                                         p_ERROR    OUT NUMBER)
   IS
      V_NOM_SP   VARCHAR2 (30) := 'Sp_PAB_COMBO_CANAL_CAM_SAL';
   BEGIN
      P_ERROR := PKG_PAB_CONSTANTES.V_OK;

      OPEN p_CURSOR FOR
           SELECT   COD_SIS_ENT_SAL AS CODIGO, DSC_SIS_ENT_SAL AS AREA
             FROM   PABS_DT_SISTM_ENTRD_SALID
            WHERE   COD_SIS_ENT_SAL IN
                          (PKG_PAB_CONSTANTES.V_COD_TIP_ABOMAS,
                           PKG_PAB_CONSTANTES.V_COD_TIP_CTACTELBTR,
                           PKG_PAB_CONSTANTES.V_COD_TIP_CANJE,
                           PKG_PAB_CONSTANTES.V_COD_TIP_CUSTODIA,
                           PKG_PAB_CONSTANTES.V_COD_TIP_FONMUT,
                           PKG_PAB_CONSTANTES.V_COD_TIP_HIPOTE,
                           PKG_PAB_CONSTANTES.V_COD_TIP_CONTA,
                           PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SE_OOFF,
                           PKG_PAB_CONSTANTES.V_COD_TIP_CREDIT,
                           PKG_PAB_CONSTANTES.V_COD_TIP_TESCEN)
                    AND FLG_VGN = PKG_PAB_CONSTANTES.V_FLG_VGN_SI
         ORDER BY   DSC_SIS_ENT_SAL;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);

         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_COMBO_CANAL_CAM_SAL;

   /********************************************************************************************************
   FUNCION/PROCEDIMIENTO: SP_PAB_COMBO_PERFIL.
   OBJETIVO             : MUESTRA FILTRO EN: ADM SISTEMAS -> MANTENEDORES -> GESTION DE PERFILES.
                              COMBO: ÁREA.
   SISTEMA              : DBO_PAB.
   BASE DE DATOS        : DBO_PAB.
   TABLAS USADAS        : PABS_DT_TIPO_ROL_ALMOT.
                          PABS_DT_TIPO_ROL_SISTM_ENTRD
   FECHA                : 03/07/2018
   AUTOR                : NEORIS.
   INPUT                : P_COD_SIS_ENT := CÓDIGO SISTEMA ENTRADA/SALIDA.
   OUTPUT               : P_CURSOR      := RESULTADOS DE CONSULTA.
                          P_ERR_MSG := GLOSA QUE INDICA EL ERROR.
   OBSERVACIONES
   FECHA       USUARIO  DETALLE
   03/07/2018  NEORIS   CREACIÓN SP.
   ********************************************************************************************************/
   PROCEDURE Sp_PAB_COMBO_PERFIL (p_COD_SIS_ENT   IN     CHAR,
                                  p_CURSOR           OUT SYS_REFCURSOR)
   IS
      --
      v_NOM_SP            VARCHAR2 (30) := 'Sp_PAB_COMBO_PERFIL';
      V_FLG_SIS_ENT_SAL   NUMBER := NULL;
   --
   BEGIN
      OPEN p_CURSOR FOR
         SELECT   ROL.NOM_TPO_ROL_AOS AS PERFIL,
                  ROL.COD_TPO_ROL_AOS AS COD_PERFIL,
                  1 AS CNT_FLU
           FROM   PABS_DT_TIPO_ROL_ALMOT ROL,
                  PABS_DT_TIPO_ROL_SISTM_ENTRD ROL_SIS
          WHERE   ROL_SIS.cod_sis_ent = p_COD_SIS_ENT
                  AND ROL_SIS.cod_tpo_rol_aos = ROL.cod_tpo_rol_aos;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_COMBO_PERFIL;
END PKG_PAB_PERFIL;