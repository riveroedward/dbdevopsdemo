CREATE OR REPLACE PACKAGE BODY         PKG_PAB_CAJA_MX
IS

/************************************************************************************************
-- Funcion/Procedimiento: SP_BUS_SALDO_CIERRE_MX
-- Objetivo: Procedimiento almacenado que Muestra los datos de saldos del cierre MX
-- Sistema: DBO_PAB.
-- Base de Datos: DBO_PAB.
-- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX
--
-- Fecha: 31-10-2017
-- Autor: Santander
-- Input:
--          p_MONEDA ->     Moneda a consultar.
--
-- Output:
--          p_CURSOR ->     SYS_REFCURSOR, resultados de consulta.
--          p_ERROR: ->     Indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
-- Input/Output: N/A
-- Retorno: N/A.
-- Observaciones: <Fecha y Detalle de ultimos cambios>
--***********************************************************************************************/
PROCEDURE SP_BUS_SALDO_CIERRE_MX  (p_MONEDA         IN CHAR,
                                   p_CURSOR          OUT SYS_REFCURSOR,
                                   p_ERROR           OUT NUMBER)IS

     v_NOM_SP  VARCHAR2(30) := 'SP_BUS_SALDO_CIERRE_MX';
     v_FECHA_ACTUAL  DATE := TRUNC(SYSDATE);


   --v_MONEDA CHAR :=  p_MONEDA;


    BEGIN

     IF (p_MONEDA IS NOT NULL) THEN
     BEGIN

     OPEN p_CURSOR FOR


                --SALDOS CIERRE



           /* SELECT  TI.DES_BCO  AS DSC_BCO_ORG,
                CC.NUM_CTA_CTE  AS CUENTA_CORRIENTE,
                CC.FEC_ING_CTL  AS FEC_ULT_CART,
                CC.IMP_INI_CTL  AS SALDO_INICIAL,
                CC.IMP_ING_CTL  AS INGRESOS,
                CC.IMP_EGR_CTL  AS EGRESOS,
                CC.IMP_FNL_CTL  AS SALDO_CIERRE,
                CC.IMP_ING_OVR  AS INGRESOS_OVER,
                CC.IMP_EGR_OVR  AS EGRESOS_OVER,
                CC.IMP_FNL_CTL  AS SALDO_OVER,
                CC.NUM_FOL_CTL  AS FOLIO_CARTOLA,
                CC.COD_BCO_ORG  AS BIC_BANCO,
                CC.NUM_FOL_CTL  AS NUM_CTL
                FROM TCDTBAI TI,
            PABS_DT_DETLL_SALDO_CAJA_MN_MX CC
            WHERE CC.COD_BCO_ORG = TI.TGCDSWSA (+)
            AND CC.COD_DVI = p_MONEDA;    */




            SELECT  TI.DES_BCO  AS DSC_BCO_ORG,
                p_MONEDA  AS MONEDA,
                CC.NUM_CTA_CTE  AS CUENTA_CORRIENTE,
                SL.FEC_ING_CTL  AS FEC_ULT_CART,
                NVL(SL.IMP_INI_CTL,0)  AS SALDO_INICIAL,
                NVL(SL.IMP_ING_CTL,0)  AS INGRESOS,
                NVL(SL.IMP_EGR_CTL,0)  AS EGRESOS,
                NVL(SL.IMP_FNL_CTL,0)  AS SALDO_CIERRE,
                NVL(CC.IMP_ING_OVR,0)  AS INGRESOS_OVER,
                NVL(CC.IMP_EGR_OVR,0)  AS EGRESOS_OVER,
                NVL(CC.IMP_FNL_CTL,0)  AS SALDO_OVER,
                SL.NUM_FOL_CTL  AS FOLIO_CARTOLA,
                CC.COD_BCO_ORG  AS BIC_BANCO,
                CC.NUM_FOL_CTL  AS NUM_CTL
                FROM TCDTBAI TI,
            PABS_DT_CBCRA_CRTLA SL,
            PABS_DT_DETLL_SALDO_CAJA_MN_MX CC
            WHERE CC.COD_BCO_ORG = TI.TGCDSWSA (+)
            AND SUBSTR(CC.COD_BCO_ORG,0,8) || '   ' = SL.COD_BCO_ORG (+)
            AND CC.COD_DVI = p_MONEDA
            AND SL.COD_DVI = CC.COD_DVI
            AND SL.FEC_ING_CTL = (SELECT MAX(CCD.FEC_ING_CTL)
                                  FROM PABS_DT_CBCRA_CRTLA CCD
                                  WHERE
                                   CCD.COD_BCO_ORG = SL.COD_BCO_ORG
                                  AND CCD.COD_DVI = p_MONEDA
                                  AND CCD.FEC_ING_CTL < TRUNC(SYSDATE)) -- no la cartola del dia.

            /*UNION

            SELECT  TI.DES_BCO  AS DSC_BCO_ORG,
                 ''  AS MONEDA,
                CC.NUM_CTA_CTE  AS CUENTA_CORRIENTE,
                SL.FEC_ING_CTL  AS FEC_ULT_CART,
                NVL(SL.IMP_INI_CTL,0)  AS SALDO_INICIAL,
                NVL(SL.IMP_ING_CTL,0)  AS INGRESOS,
                NVL(SL.IMP_EGR_CTL,0)  AS EGRESOS,
                NVL(SL.IMP_FNL_CTL,0)  AS SALDO_CIERRE,
                NVL(CC.IMP_ING_OVR,0)  AS INGRESOS_OVER,
                NVL(CC.IMP_EGR_OVR,0)  AS EGRESOS_OVER,
                NVL(CC.IMP_FNL_CTL,0)  AS SALDO_OVER,
                SL.NUM_FOL_CTL  AS FOLIO_CARTOLA,
                CC.COD_BCO_ORG  AS BIC_BANCO,
                CC.NUM_FOL_CTL  AS NUM_CTL
                FROM TCDTBAI TI,
            PABS_DT_CBCRA_CRTLA SL,
            PABS_DT_DETLL_SALDO_CAJA_MN_MX CC
            WHERE CC.COD_BCO_ORG = TI.TGCDSWSA (+)
            AND SUBSTR(CC.COD_BCO_ORG,0,8) || '   ' = SL.COD_BCO_ORG (+)
            AND CC.COD_DVI = p_MONEDA
            AND CC.FEC_ING_CTL < TRUNC(SYSDATE)
            AND CC.COD_BCO_ORG NOT IN (SELECT DISTINCT(CCD.COD_BCO_ORG)
                                  FROM PABS_DT_CBCRA_CRTLA CCD
                                  WHERE CCD.FEC_ING_CTL < TRUNC(SYSDATE))*/;




           /* SELECT  TI.DES_BCO  AS DSC_BCO_ORG,
                CC.NUM_CTA_CTE  AS CUENTA_CORRIENTE,
                CC.FEC_ING_CTL  AS FEC_ULT_CART,
                CC.IMP_INI_CTL  AS SALDO_INICIAL,
                CC.IMP_ING_CTL  AS INGRESOS,
                CC.IMP_EGR_CTL  AS EGRESOS,
                CC.IMP_FNL_CTL  AS SALDO_CIERRE,
                CC.IMP_ING_OVR  AS INGRESOS_OVER,
                CC.IMP_EGR_OVR  AS EGRESOS_OVER,
                CC.IMP_FNL_CTL  AS SALDO_OVER,
                CC.NUM_FOL_CTL  AS FOLIO_CARTOLA,
                CC.COD_BCO_ORG  AS BIC_BANCO,
                CC.NUM_FOL_CTL  AS NUM_CTL
                FROM TCDTBAI TI,
            PABS_DT_DETLL_SALDO_CAJA_MN_MX CC
            WHERE CC.COD_BCO_ORG = TI.TGCDSWSA (+)
            AND CC.COD_DVI = p_MONEDA;    */



     EXCEPTION

            WHEN NO_DATA_FOUND THEN

                    err_code := SQLCODE;
                    err_msg := 'No se han encontrado registos asociados a la busqueda por moneda :' || p_MONEDA;
                    PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                    p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;

            WHEN OTHERS THEN

                    err_code := SQLCODE;
                    err_msg := SUBSTR(SQLERRM, 1, 300);
                    p_s_mensaje := err_code || '-' || err_msg;
                    PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                    p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                    RAISE_APPLICATION_ERROR(-20000,p_s_mensaje);



     END;

     ELSE
     BEGIN

     OPEN p_CURSOR FOR


                --SALDOS CIERRE

            SELECT  TI.DES_BCO  AS DSC_BCO_ORG,
                CC.COD_DVI  AS MONEDA,
                CC.NUM_CTA_CTE  AS CUENTA_CORRIENTE,
                SL.FEC_ING_CTL  AS FEC_ULT_CART,
                NVL(SL.IMP_INI_CTL,0)  AS SALDO_INICIAL,
                NVL(SL.IMP_ING_CTL,0)  AS INGRESOS,
                NVL(SL.IMP_EGR_CTL,0)  AS EGRESOS,
                NVL(SL.IMP_FNL_CTL,0)  AS SALDO_CIERRE,
                NVL(CC.IMP_ING_OVR,0)  AS INGRESOS_OVER,
                NVL(CC.IMP_EGR_OVR,0)  AS EGRESOS_OVER,
                NVL(CC.IMP_FNL_CTL,0)  AS SALDO_OVER,
                SL.NUM_FOL_CTL  AS FOLIO_CARTOLA,
                CC.COD_BCO_ORG  AS BIC_BANCO,
                CC.NUM_FOL_CTL  AS NUM_CTL
                FROM TCDTBAI TI,
            PABS_DT_CBCRA_CRTLA SL,
            PABS_DT_DETLL_SALDO_CAJA_MN_MX CC
            WHERE CC.COD_BCO_ORG = TI.TGCDSWSA (+)
            AND SUBSTR(CC.COD_BCO_ORG,0,8) || '   ' = SL.COD_BCO_ORG (+)
           -- AND CC.COD_DVI = p_MONEDA
            AND SL.COD_DVI = CC.COD_DVI
            AND SL.FEC_ING_CTL = (SELECT MAX(CCD.FEC_ING_CTL)
                                  FROM PABS_DT_CBCRA_CRTLA CCD
                                  WHERE
                                   CCD.COD_BCO_ORG = SL.COD_BCO_ORG
                                  AND CCD.COD_DVI = CC.COD_DVI
                                  AND CCD.FEC_ING_CTL < TRUNC(SYSDATE)); -- no la cartola del dia.



     EXCEPTION

            WHEN NO_DATA_FOUND THEN

                    err_code := SQLCODE;
                    err_msg := 'No se han encontrado registos asociados a la busqueda por moneda :' || p_MONEDA;
                    PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                    p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;

            WHEN OTHERS THEN

                    err_code := SQLCODE;
                    err_msg := SUBSTR(SQLERRM, 1, 300);
                    p_s_mensaje := err_code || '-' || err_msg;
                    PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                    p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                    RAISE_APPLICATION_ERROR(-20000,p_s_mensaje);



     END;
     END IF;






END SP_BUS_SALDO_CIERRE_MX;

/************************************************************************************************
-- Funcion/Procedimiento: SP_BUS_SALDO_INTRADIA_MX
-- Objetivo: Procedimiento almacenado que Muestra los datos de saldo intradía
-- Sistema: DBO_PAB.
-- Base de Datos: DBO_PAB.
-- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX
--
-- Fecha: 31-10-2017
-- Autor: Santander
-- Input:
--          p_MONEDA  ->    Moneda a consultar.
--
-- Output:
--          p_CURSOR ->     SYS_REFCURSOR, resultados de consulta.
--          p_ERROR  ->     Indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
-- Input/Output: N/A
-- Retorno: N/A.
-- Observaciones: <Fecha y Detalle de ultimos cambios>
--***********************************************************************************************/
PROCEDURE SP_BUS_SALDO_INTRADIA_MX  (p_MONEDA         IN CHAR,
                                   p_CURSOR          OUT SYS_REFCURSOR,
                                   p_ERROR           OUT NUMBER)IS

     v_NOM_SP  VARCHAR2(30) := 'SP_BUS_SALDO_INTRADIA_MX';
     v_FECHA_ACTUAL  DATE := TRUNC(SYSDATE);


    BEGIN
                --SALDOS INTRADIA
         IF p_MONEDA IS NOT NULL THEN
         BEGIN
         OPEN p_CURSOR FOR
            SELECT TI.DES_BCO  AS DSC_BCO_ORG,
                CC.NUM_CTA_CTE  AS CUENTA_CORRIENTE,
                CC.FEC_ING_CTL  AS FEC_ULT_CART,
                CC.IMP_INI_CTL  AS SALDO_INICIAL,
                CC.IMP_ING_CTL  AS INGRESOS,
                CC.IMP_EGR_CTL  AS EGRESOS,
                CC.IMP_FNL_CTL  AS SALDO_CIERRE,

                CC.IMP_APR_AOS  AS SALDO_APERTURA, -- (SALDO INCIAL DEL DIA)
                CC.IMP_ING_CPR  AS INGRESO_INTRA,
                CC.IMP_EGR_CPR  AS EGRESOS_INTRA,
                CC.IMP_TOT_FNL_DIA AS SALDO_FINAL_INTRA,

                CC.NUM_FOL_CTL  AS FOLIO_CARTOLA,
                CC.COD_BCO_ORG  AS BIC_BANCO,
                CC.COD_DVI  AS MONEDA


           FROM TCDTBAI TI,
            PABS_DT_DETLL_SALDO_CAJA_MN_MX CC
            WHERE CC.COD_BCO_ORG = TI.TGCDSWSA (+)
            AND CC.COD_DVI = p_MONEDA;

          EXCEPTION

            WHEN NO_DATA_FOUND THEN

                    err_code := SQLCODE;
                    err_msg := 'No se han encontrado registos asociados a la busqueda por moneda :' || p_MONEDA;
                    PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                    p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;

            WHEN OTHERS THEN

                    err_code := SQLCODE;
                    err_msg := SUBSTR(SQLERRM, 1, 300);
                    p_s_mensaje := err_code || '-' || err_msg;
                    PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                    p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                    RAISE_APPLICATION_ERROR(-20000,p_s_mensaje);
           END;
           ELSE
           BEGIN
           OPEN p_CURSOR FOR
            SELECT TI.DES_BCO  AS DSC_BCO_ORG,
                CC.NUM_CTA_CTE  AS CUENTA_CORRIENTE,
                CC.FEC_ING_CTL  AS FEC_ULT_CART,
                CC.IMP_INI_CTL  AS SALDO_INICIAL,
                CC.IMP_ING_CTL  AS INGRESOS,
                CC.IMP_EGR_CTL  AS EGRESOS,
                CC.IMP_FNL_CTL  AS SALDO_CIERRE,

                CC.IMP_APR_AOS  AS SALDO_APERTURA, -- (SALDO INCIAL DEL DIA)
                CC.IMP_ING_CPR  AS INGRESO_INTRA,
                CC.IMP_EGR_CPR  AS EGRESOS_INTRA,
                CC.IMP_TOT_FNL_DIA AS SALDO_FINAL_INTRA,

                CC.NUM_FOL_CTL  AS FOLIO_CARTOLA,
                CC.COD_BCO_ORG  AS BIC_BANCO,
                CC.COD_DVI  AS MONEDA


           FROM TCDTBAI TI,
            PABS_DT_DETLL_SALDO_CAJA_MN_MX CC
            WHERE CC.COD_BCO_ORG = TI.TGCDSWSA (+)
            AND (CC.IMP_APR_AOS <> 0 OR CC.IMP_TOT_FNL_DIA <> 0 OR CC.IMP_ING_CPR <> 0 OR CC.IMP_EGR_CPR <> 0)

            ;
            --AND CC.COD_DVI = p_MONEDA;

          EXCEPTION

            WHEN NO_DATA_FOUND THEN

                    err_code := SQLCODE;
                    err_msg := 'No se han encontrado registos asociados a la busqueda por moneda :' || p_MONEDA;
                    PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                    p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;

            WHEN OTHERS THEN

                    err_code := SQLCODE;
                    err_msg := SUBSTR(SQLERRM, 1, 300);
                    p_s_mensaje := err_code || '-' || err_msg;
                    PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                    p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                    RAISE_APPLICATION_ERROR(-20000,p_s_mensaje);

           END;
           END IF;

END SP_BUS_SALDO_INTRADIA_MX;

/************************************************************************************************
-- Funcion/Procedimiento: SP_BUS_SALDO_24HORAS_MX
-- Objetivo: Procedimiento almacenado que Muestra los datos de saldo a 24 horas
-- Sistema: DBO_PAB.
-- Base de Datos: DBO_PAB.
-- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX, TCDTBAI
--
-- Fecha: 31-10-2017
-- Autor: Santander
-- Input:
--          p_MONEDA  ->    Moneda a consultar.
--
-- Output:
--          p_CURSOR  ->    SYS_REFCURSOR, resultados de consulta.
--          p_ERROR:  ->    Indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
-- Input/Output: N/A
-- Retorno: N/A.
-- Observaciones: <Fecha y Detalle de ultimos cambios>
--***********************************************************************************************/
PROCEDURE SP_BUS_SALDO_24HORAS_MX  (p_MONEDA         IN CHAR,
                                   p_CURSOR          OUT SYS_REFCURSOR,
                                   p_ERROR           OUT NUMBER)IS

     v_NOM_SP  VARCHAR2(30) := 'SP_BUS_SALDO_24HORAS_MX';
     v_FECHA_ACTUAL  DATE := TRUNC(SYSDATE);

    BEGIN
        IF p_MONEDA IS NOT NULL THEN
        BEGIN
                --SALDOS INTRADIA

         OPEN p_CURSOR FOR
            SELECT TI.DES_BCO  AS DSC_BCO_ORG,
                CC.NUM_CTA_CTE  AS CUENTA_CORRIENTE,
                CC.FEC_ING_CTL  AS FEC_ULT_CART,
                CC.IMP_INI_CTL  AS SALDO_INICIAL,
                CC.IMP_ING_CTL  AS INGRESOS,
                CC.IMP_EGR_CTL  AS EGRESOS,
                CC.IMP_FNL_CTL  AS SALDO_CIERRE,

                CC.IMP_TOT_FNL_DIA AS SALDO_APER_24,
                CC.IMP_ING_24  AS INGRESO_24,
                CC.IMP_EGR_24  AS EGRESOS_24,
                CC.IMP_TOT_24  AS SALDO_FINAL_24,

                CC.NUM_FOL_CTL  AS FOLIO_CARTOLA,
                CC.COD_BCO_ORG  AS BIC_BANCO,
                CC.COD_DVI  AS MONEDA
                --PKG_PAB_UTILITY.FN_PAB_OBT_DSC_BCO(CC.COD_BCO_ORG) AS DSC_BCO_ORG

                FROM TCDTBAI TI,
            PABS_DT_DETLL_SALDO_CAJA_MN_MX CC
            WHERE CC.COD_BCO_ORG = TI.TGCDSWSA (+)
            AND CC.COD_DVI = p_MONEDA;

          EXCEPTION

            WHEN NO_DATA_FOUND THEN

                    err_code := SQLCODE;
                    err_msg := 'No se han encontrado registos asociados a la busqueda por moneda :' || p_MONEDA;
                    PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                    p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;

            WHEN OTHERS THEN

                    err_code := SQLCODE;
                    err_msg := SUBSTR(SQLERRM, 1, 300);
                    p_s_mensaje := err_code || '-' || err_msg;
                    PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                    p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                    RAISE_APPLICATION_ERROR(-20000,p_s_mensaje);
        END;
        ELSE
        BEGIN
            OPEN p_CURSOR FOR
            SELECT TI.DES_BCO  AS DSC_BCO_ORG,
                CC.NUM_CTA_CTE  AS CUENTA_CORRIENTE,
                CC.FEC_ING_CTL  AS FEC_ULT_CART,
                CC.IMP_INI_CTL  AS SALDO_INICIAL,
                CC.IMP_ING_CTL  AS INGRESOS,
                CC.IMP_EGR_CTL  AS EGRESOS,
                CC.IMP_FNL_CTL  AS SALDO_CIERRE,

                CC.IMP_TOT_FNL_DIA AS SALDO_APER_24,
                CC.IMP_ING_24  AS INGRESO_24,
                CC.IMP_EGR_24  AS EGRESOS_24,
                CC.IMP_TOT_24  AS SALDO_FINAL_24,

                CC.NUM_FOL_CTL  AS FOLIO_CARTOLA,
                CC.COD_BCO_ORG  AS BIC_BANCO,
                CC.COD_DVI  AS MONEDA
                --PKG_PAB_UTILITY.FN_PAB_OBT_DSC_BCO(CC.COD_BCO_ORG) AS DSC_BCO_ORG

                FROM TCDTBAI TI,
            PABS_DT_DETLL_SALDO_CAJA_MN_MX CC
            WHERE CC.COD_BCO_ORG = TI.TGCDSWSA (+)
            AND (CC.IMP_APR_AOS <> 0 OR CC.IMP_TOT_FNL_DIA <> 0 OR CC.IMP_ING_CPR <> 0 OR CC.IMP_EGR_CPR <> 0);
           -- AND CC.COD_DVI = p_MONEDA;

          EXCEPTION

            WHEN NO_DATA_FOUND THEN

                    err_code := SQLCODE;
                    err_msg := 'No se han encontrado registos asociados a la busqueda por moneda :' || p_MONEDA;
                    PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                    p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;

            WHEN OTHERS THEN

                    err_code := SQLCODE;
                    err_msg := SUBSTR(SQLERRM, 1, 300);
                    p_s_mensaje := err_code || '-' || err_msg;
                    PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                    p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                    RAISE_APPLICATION_ERROR(-20000,p_s_mensaje);


        END;
        END IF;


END SP_BUS_SALDO_24HORAS_MX;

/************************************************************************************************
-- Funcion/Procedimiento: SP_BUS_SALDO_48HORAS_MX
-- Objetivo: Procedimiento almacenado que Muestra los datos de saldo a 48 horas
-- Sistema: DBO_PAB.
-- Base de Datos: DBO_PAB.
-- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX, TCDTBAI
--
-- Fecha: 31-10-2017
-- Autor: Santander
-- Input:
--          p_MONEDA  ->    Moneda a consultar.
--
-- Output:
--          p_CURSOR  ->    SYS_REFCURSOR, resultados de consulta.
--          p_ERROR:  ->    Indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
-- Input/Output: N/A
-- Retorno: N/A.
-- Observaciones: <Fecha y Detalle de ultimos cambios>
--***********************************************************************************************/
PROCEDURE SP_BUS_SALDO_48HORAS_MX  (p_MONEDA         IN CHAR,
                                   p_CURSOR          OUT SYS_REFCURSOR,
                                   p_ERROR           OUT NUMBER)IS

     v_NOM_SP  VARCHAR2(30) := 'SP_BUS_SALDO_48HORAS_MX';
     v_FECHA_ACTUAL  DATE := TRUNC(SYSDATE);


    BEGIN
    IF p_MONEDA IS NOT NULL THEN
    BEGIN
                --SALDOS 48 horas
         OPEN p_CURSOR FOR
          SELECT TI.DES_BCO  AS DSC_BCO_ORG,
                CC.NUM_CTA_CTE  AS CUENTA_CORRIENTE,
                CC.FEC_ING_CTL  AS FEC_ULT_CART,
                CC.IMP_INI_CTL  AS SALDO_INICIAL,
                CC.IMP_ING_CTL  AS INGRESOS,
                CC.IMP_EGR_CTL  AS EGRESOS,
                CC.IMP_FNL_CTL  AS SALDO_CIERRE,

                CC.IMP_TOT_24  AS SALDO_APER_48,
                CC.IMP_ING_48  AS INGRESO_48,
                CC.IMP_EGR_48  AS EGRESOS_48,
                CC.IMP_TOT_48  AS SALDO_FINAL_48,

                CC.NUM_FOL_CTL  AS FOLIO_CARTOLA,
                CC.COD_BCO_ORG  AS BIC_BANCO,
                CC.COD_DVI  AS MONEDA

               FROM TCDTBAI TI,
            PABS_DT_DETLL_SALDO_CAJA_MN_MX CC
            WHERE CC.COD_BCO_ORG = TI.TGCDSWSA (+)
            AND CC.COD_DVI = p_MONEDA;

          EXCEPTION

            WHEN NO_DATA_FOUND THEN

                    err_code := SQLCODE;
                    err_msg := 'No se han encontrado registos asociados a la busqueda por moneda :' || p_MONEDA;
                    PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                    p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;

            WHEN OTHERS THEN

                    err_code := SQLCODE;
                    err_msg := SUBSTR(SQLERRM, 1, 300);
                    p_s_mensaje := err_code || '-' || err_msg;
                    PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                    p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                    RAISE_APPLICATION_ERROR(-20000,p_s_mensaje);
         END;
         ELSE
         BEGIN
              --SALDOS 48 horas
         OPEN p_CURSOR FOR
          SELECT TI.DES_BCO  AS DSC_BCO_ORG,
                CC.NUM_CTA_CTE  AS CUENTA_CORRIENTE,
                CC.FEC_ING_CTL  AS FEC_ULT_CART,
                CC.IMP_INI_CTL  AS SALDO_INICIAL,
                CC.IMP_ING_CTL  AS INGRESOS,
                CC.IMP_EGR_CTL  AS EGRESOS,
                CC.IMP_FNL_CTL  AS SALDO_CIERRE,

                CC.IMP_TOT_24  AS SALDO_APER_48,
                CC.IMP_ING_48  AS INGRESO_48,
                CC.IMP_EGR_48  AS EGRESOS_48,
                CC.IMP_TOT_48  AS SALDO_FINAL_48,

                CC.NUM_FOL_CTL  AS FOLIO_CARTOLA,
                CC.COD_BCO_ORG  AS BIC_BANCO,
                CC.COD_DVI  AS MONEDA

               FROM TCDTBAI TI,
            PABS_DT_DETLL_SALDO_CAJA_MN_MX CC
            WHERE CC.COD_BCO_ORG = TI.TGCDSWSA (+)
            AND (CC.IMP_APR_AOS <> 0 OR CC.IMP_TOT_FNL_DIA <> 0 OR CC.IMP_ING_CPR <> 0 OR CC.IMP_EGR_CPR <> 0);
           -- AND CC.COD_DVI = p_MONEDA;

          EXCEPTION

            WHEN NO_DATA_FOUND THEN

                    err_code := SQLCODE;
                    err_msg := 'No se han encontrado registos asociados a la busqueda por moneda :' || p_MONEDA;
                    PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                    p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;

            WHEN OTHERS THEN

                    err_code := SQLCODE;
                    err_msg := SUBSTR(SQLERRM, 1, 300);
                    p_s_mensaje := err_code || '-' || err_msg;
                    PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                    p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                    RAISE_APPLICATION_ERROR(-20000,p_s_mensaje);

         END;
         END IF;

END SP_BUS_SALDO_48HORAS_MX;

/************************************************************************************************
-- Funcion/Procedimiento: SP_BUS_CRTLA_CTCTE_MX
-- Objetivo: Procedimiento almacenado que Muestra la cabecera y detalle de la Cartola MX por tipo de movimiento
-- Sistema: DBO_PAB.
-- Base de Datos: DBO_PAB.
-- Tablas Usadas: PABS_DT_CBCRA_CRTLA , PABS_DT_CENTA_BANCO_CRPSL , PABS_DT_DETLL_CRTLA, PABS_DT_DETLL_SALDO_CAJA_MN_MX, PABS_DT_DETLL_MVNTO_CAJA
-- Fecha: 02-11-2017
-- Autor: Santander
-- Input:
--          p_FOLIO   ->     Moneda a consultar.
--          p_BANCO   ->     Bic Banco
--          p_CTA_CTE ->     Cuenta Corriente
--          p_COD_DET ->     Código Detalle a buscar
--
-- Output:
--          p_CABECERA  ->    SYS_REFCURSOR, datos de cabecera.
--          p_DETALLE   ->    SYS_REFCURSOR, datos del detalleresultados de consulta.
--          p_ERROR:      ->    Indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
-- Input/Output: N/A
-- Retorno: N/A.
-- Observaciones: <Fecha y Detalle de ultimos cambios>
--***********************************************************************************************/
PROCEDURE SP_BUS_CRTLA_CTCTE_MX  (p_FOLIO         IN NUMBER,
                                  p_BANCO         IN CHAR,
                                  p_CTA_CTE       IN VARCHAR2,
                                  p_FEC_CTL       IN DATE,
                                  p_COD_DET       IN CHAR,
                                  p_COD_DVI       IN CHAR,
                                  p_CABECERA       OUT SYS_REFCURSOR,
                                  p_DETALLE        OUT SYS_REFCURSOR,
                                  p_ERROR          OUT NUMBER)IS

     v_NOM_SP  VARCHAR2(30) := 'SP_BUS_CRTLA_CTCTE_MX';
     v_FECHA_ACTUAL  DATE := TRUNC(SYSDATE);
     v_BANCO CHAR(11);
     v_FECHA_24H DATE := TRUNC(SYSDATE+1);
     v_FECHA_48H DATE := TRUNC(SYSDATE+2);
     v_VAL_FECHA NUMBER := 0;
     v_VAL_HORA CHAR(5) := '00:00';
     v_COD_DVI CHAR(3); -- obtengo la divisa de la cuenta ingresa, para la busqueda del detalle.


    BEGIN


         -- OBTENDREMOS EL DETALLE  POR MOVIMIENTO
         v_BANCO := TRIM(p_BANCO); --BIC DE BANCO
         v_COD_DVI := p_COD_DVI;

        IF p_COD_DET = 'CTL' THEN -- CARTOLA

            BEGIN

                OPEN p_CABECERA FOR
                SELECT DISTINCT (CRL.COD_BIC_BCO)   AS BIC,
                       TI.DES_BCO                  AS BANCO,
                       CC.NUM_CTA_CTE              AS CUENTA,
                       CRL.COD_DVI      AS MONEDA,
                       CC.FEC_ING_CTL AS FECHA,--HORA TAMBIEN
                       TO_CHAR(FLOOR(CC.HOR_ING_CTL/100)) || ':' || TO_CHAR(MOD(CC.HOR_ING_CTL,100))  AS HORA,--HORA TAMBIEN
                       CC.IMP_INI_CTL  AS SALDO_INCIAL,
                       CC.IMP_ING_CTL  AS INGRESO,
                       CC.IMP_EGR_CTL  AS EGRESO,
                       CC.IMP_FNL_CTL  AS SALDO_CIERRE,
                       CC.NUM_FOL_CTL  AS FOLIO_CARTOLA,
                       CRL.IMP_TOT_LCD AS LINEA_CREDITO,
                       TI.DES_CD       AS PLAZA
                FROM TCDTBAI TI,
                PABS_DT_CBCRA_CRTLA CC,
                PABS_DT_CENTA_BANCO_CRPSL CRL
                WHERE SUBSTR(CC.COD_BCO_ORG,0,8) = SUBSTR(TI.TGCDSWSA,0,8)
                      AND CC.NUM_FOL_CTL = p_FOLIO
                      AND CC.FEC_ING_CTL = p_FEC_CTL
                      AND SUBSTR(CC.COD_BCO_ORG,0,8) = SUBSTR(v_BANCO,0,8)
                      AND CC.NUM_CTA_CTE = CRL.NUM_CTA_COR
                      AND SUBSTR(CRL.COD_BIC_BCO,0,8) =  SUBSTR(CC.COD_BCO_ORG,0,8)
                      AND CRL.COD_DVI = CC.COD_DVI
                      AND CC.COD_DVI <> PKG_PAB_CONSTANTES.V_STR_DVI_CLP;

            EXCEPTION

            WHEN NO_DATA_FOUND THEN

            err_code := SQLCODE;
            err_msg := 'No se han encontrado registos asociados a la busqueda por FOLIO :' || p_FOLIO || ' - BANCO : ' || v_BANCO;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
            p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;

            WHEN OTHERS THEN

            err_code := SQLCODE;
            err_msg := SUBSTR(SQLERRM, 1, 300);
            p_s_mensaje := err_code || '-' || err_msg;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
            p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
            RAISE_APPLICATION_ERROR(-20000,p_s_mensaje);

            END;

           --Detalle cartola grilla...
               BEGIN

                  OPEN p_DETALLE FOR
                        SELECT DC.NUM_REF_ITN_CTA AS REFERENCIA,
                        --DC.NUM_REF_SWF_OPE AS REFERENCIA,
                        DC.FEC_TRN_OPE  AS FECHA,
                        TO_DATE(CC.FEC_ING_CTL || ' ' || TO_CHAR(FLOOR(CC.HOR_ING_CTL/100)) || ':' || TO_CHAR(MOD(CC.HOR_ING_CTL,100)) , 'DD-MM-RRRR HH24:MI') AS HORA,
                        DC.FLG_DEB_CRE  AS FLAG,
                        DECODE (DC.FLG_DEB_CRE, 1,DC.IMP_TRN,0)     AS INGRESO,
                        DECODE (DC.FLG_DEB_CRE, 0,DC.IMP_TRN,0)     AS EGRESO,
                        DC.GLS_TRN      AS GLOSA
                        FROM PABS_DT_CBCRA_CRTLA CC
                        ,PABS_DT_DETLL_CRTLA DC
                        WHERE SUBSTR(CC.COD_BCO_ORG,0,8) = SUBSTR(v_BANCO,0,8)
                         AND CC.NUM_CTA_CTE = p_CTA_CTE
                         AND CC.FEC_ING_CTL = p_FEC_CTL
                         AND DC.COD_BCO_ORG = CC.COD_BCO_ORG
                         AND DC.NUM_CTA_CTE = CC.NUM_CTA_CTE
                         AND DC.FEC_ING_CTL = CC.FEC_ING_CTL
                         AND CC.COD_DVI <> PKG_PAB_CONSTANTES.V_STR_DVI_CLP;


               EXCEPTION

                    WHEN NO_DATA_FOUND THEN

                            err_code := SQLCODE;
                            err_msg := 'No se han encontrado registos asociados a la busqueda por FOLIO :' || p_FOLIO;
                            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                            p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;

                    WHEN OTHERS THEN

                            err_code := SQLCODE;
                            err_msg := SUBSTR(SQLERRM, 1, 300);
                            p_s_mensaje := err_code || '-' || err_msg;
                            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                            p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                            RAISE_APPLICATION_ERROR(-20000,p_s_mensaje);

                 END;

         END IF;

           IF p_COD_DET = 'ITD' THEN --

               BEGIN
                   OPEN p_CABECERA FOR
                        SELECT DISTINCT (CC.COD_BCO_ORG)   AS BIC,
                        TI.DES_BCO  AS BANCO,
                        CC.NUM_CTA_CTE  AS CUENTA,
                        CC.COD_DVI  AS MONEDA,
                        CC.FEC_ING_CTL AS FECHA,
                        v_VAL_HORA AS HORA,
                        CC.IMP_APR_AOS  AS SALDO_INCIAL,
                        CC.IMP_ING_CPR  AS INGRESO,
                        CC.IMP_EGR_CPR  AS EGRESO,
                        CC.IMP_TOT_FNL_DIA  AS SALDO_CIERRE,
                        CC.NUM_FOL_CTL  AS FOLIO_CARTOLA,
                        '0' AS LINEA_CREDITO,
                        TI.DES_CD       AS PLAZA
                        FROM TCDTBAI TI,
                        PABS_DT_DETLL_SALDO_CAJA_MN_MX CC, PABS_DT_CENTA_BANCO_CRPSL CRL
                        WHERE CC.COD_BCO_ORG = TI.TGCDSWSA
                        AND CC.NUM_FOL_CTL = p_FOLIO
                        AND SUBSTR(CC.COD_BCO_ORG,0,8) = SUBSTR(v_BANCO,0,8)
                        AND CRL.NUM_CTA_COR = p_CTA_CTE
                        AND CC.COD_DVI = v_COD_DVI
                        AND CC.NUM_CTA_CTE = CRL.NUM_CTA_COR;

               EXCEPTION

                    WHEN NO_DATA_FOUND THEN

                            err_code := SQLCODE;
                            err_msg := 'No se han encontrado registos asociados a la busqueda por FOLIO :' || p_FOLIO || ' - BANCO : ' || v_BANCO;
                            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                            p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;

                    WHEN OTHERS THEN

                            err_code := SQLCODE;
                            err_msg := SUBSTR(SQLERRM, 1, 300);
                            p_s_mensaje := err_code || '-' || err_msg;
                            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                            p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                            RAISE_APPLICATION_ERROR(-20000,p_s_mensaje);

                 END;
           --Detalle cartola grilla...

                BEGIN
                  OPEN p_DETALLE FOR
                        SELECT DC.NUM_FOL_NMN AS REFERENCIA,
                        DC.FEC_VTA  AS FECHA,
                        DC.FEC_INS_OPE  AS HORA,
                        DC.FLG_ING_EGR  AS FLAG,
                        DECODE (DC.FLG_ING_EGR, 1,DC.IMP_OPE,0)     AS INGRESO,
                        DECODE (DC.FLG_ING_EGR, 0,DC.IMP_OPE,0)     AS EGRESO,
                        DC.GLS_MVT      AS GLOSA
                        FROM PABS_DT_DETLL_MVNTO_CAJA DC
                        WHERE
                        DC.FEC_VTA = v_FECHA_ACTUAL--REVISAR FECHA AND
                        AND (SUBSTR(DC.COD_BCO_ORG,0,8) = SUBSTR(v_BANCO,0,8) OR SUBSTR(DC.COD_BCO_DTN,0,8) = SUBSTR(v_BANCO,0,8))
                        AND DC.COD_DVI = v_COD_DVI
                        AND DC.COD_DVI <> PKG_PAB_CONSTANTES.V_STR_DVI_CLP;

                EXCEPTION

                    WHEN NO_DATA_FOUND THEN

                            err_code := SQLCODE;
                            err_msg := 'No se han encontrado registos asociados a la busqueda por FECHA VALUTA :' || v_FECHA_ACTUAL || ' - BANCO : ' || v_BANCO;
                            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                            p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;

                    WHEN OTHERS THEN

                            err_code := SQLCODE;
                            err_msg := SUBSTR(SQLERRM, 1, 300);
                            p_s_mensaje := err_code || '-' || err_msg;
                            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                            p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                            RAISE_APPLICATION_ERROR(-20000,p_s_mensaje);

                 END;

         END IF;

           IF p_COD_DET = 'ONL' THEN -- CARTOLA

             BEGIN
                   OPEN p_CABECERA FOR
                        SELECT DISTINCT (CC.COD_BCO_ORG)   AS BIC,
                        TI.DES_BCO  AS BANCO,
                        CC.NUM_CTA_CTE  AS CUENTA,
                        CC.COD_DVI  AS MONEDA,
                        CC.FEC_ING_CTL AS FECHA,
                        v_VAL_HORA AS HORA,
                        CC.IMP_APR_AOS  AS SALDO_INCIAL,
                        CC.IMP_ING_DIA  AS INGRESO,
                        CC.IMP_EGR_DIA  AS EGRESO,
                        CC.IMP_RAL_AOS  AS SALDO_CIERRE,
                        CC.NUM_FOL_CTL  AS FOLIO_CARTOLA,
                        '0' AS LINEA_CREDITO,
                        TI.DES_CD       AS PLAZA
                        FROM TCDTBAI TI,
                        PABS_DT_DETLL_SALDO_CAJA_MN_MX CC, PABS_DT_CENTA_BANCO_CRPSL CRL
                        WHERE CC.COD_BCO_ORG = TI.TGCDSWSA   (+)
                        AND CC.NUM_FOL_CTL = p_FOLIO
                        --AND CTL.NUM_FOL_CTL = p_FOLIO
                        --AND CTL.NUM_FOL_CTL = CC.NUM_FOL_CTL
                        AND SUBSTR(CC.COD_BCO_ORG,0,8) = SUBSTR(v_BANCO,0,8)
                        AND CRL.NUM_CTA_COR = p_CTA_CTE
                        AND CC.NUM_CTA_CTE = CRL.NUM_CTA_COR
                        AND CC.COD_DVI = v_COD_DVI
                        AND CC.COD_DVI <> PKG_PAB_CONSTANTES.V_STR_DVI_CLP;  --PRUEBA
               EXCEPTION

                    WHEN NO_DATA_FOUND THEN

                            err_code := SQLCODE;
                            err_msg := 'No se han encontrado registos asociados a la busqueda por FOLIO :' || p_FOLIO || ' - BANCO : ' || v_BANCO;
                            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                            p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;

                    WHEN OTHERS THEN

                            err_code := SQLCODE;
                            err_msg := SUBSTR(SQLERRM, 1, 300);
                            p_s_mensaje := err_code || '-' || err_msg;
                            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                            p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                            RAISE_APPLICATION_ERROR(-20000,p_s_mensaje);

                 END;

           --Detalle cartola grilla...
                BEGIN

                  OPEN p_DETALLE FOR
                        SELECT DC.NUM_REF_SWF AS REFERENCIA,
                        DC.FEC_ACT_OPE  AS FECHA,
                        DC.FEC_ACT_OPE  AS HORA,
                        DC.FLG_EGR_ING  AS FLAG,
                        DECODE (DC.FLG_EGR_ING, 1,DC.IMP_OPE,0)     AS INGRESO,
                        DECODE (DC.FLG_EGR_ING, 0,DC.IMP_OPE,0)     AS EGRESO,
                        OP.OBS_OPC_SWF      AS GLOSA
                        FROM PABS_DT_DETLL_OPRCN DC,
                        PABS_DT_OPRCN_INFCN_OPCON OP
                        WHERE
                        DC.FEC_ISR_OPE = OP.FEC_ISR_OPE
                        AND DC.NUM_FOL_OPE = OP.NUM_FOL_OPE
                        AND SUBSTR(OP.COD_BCO_BFC,0,8) = SUBSTR(v_BANCO,0,8)
                        AND DC.FEC_VTA = v_FECHA_ACTUAL--REVISAR FECHA AND
                        AND DC.COD_EST_AOS = 12  ---FOLIO CARTOLA
                        AND DC.COD_DVI = v_COD_DVI
                        AND DC.COD_DVI <> PKG_PAB_CONSTANTES.V_STR_DVI_CLP

                        UNION

                        SELECT DC.NUM_REF_SWF AS REFERENCIA,
                        DC.FEC_ING_OPE  AS FECHA,
                        DC.FEC_ING_OPE  AS HORA,
                        DC.FLG_CGO_ABN  AS FLAG,
                        DECODE (DC.FLG_CGO_ABN, 1,DC.IMP_OPE,0)     AS INGRESO,
                        DECODE (DC.FLG_CGO_ABN, 0,DC.IMP_OPE,0)     AS EGRESO,
                        DC.GLS_TRN      AS GLOSA
                        FROM PABS_DT_DETLL_CARGO_ABONO_CAJA DC
                        WHERE TRUNC(DC.FEC_ING_OPE) = TRUNC(SYSDATE)
                        AND DC.COD_DVI <> PKG_PAB_CONSTANTES.V_STR_DVI_CLP
                        AND DC.COD_DVI = v_COD_DVI
                        AND (SUBSTR(DC.COD_BCO_ORG,0,8) = SUBSTR(TRIM(v_BANCO),0,8) OR SUBSTR(DC.COD_BCO_DTN,0,8) = SUBSTR(TRIM(v_BANCO),0,8));

                EXCEPTION

                    WHEN NO_DATA_FOUND THEN

                            err_code := SQLCODE;
                            err_msg := 'No se han encontrado registos asociados a la busqueda por FOLIO :' || p_FOLIO ;
                            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                            p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;

                    WHEN OTHERS THEN

                            err_code := SQLCODE;
                            err_msg := SUBSTR(SQLERRM, 1, 300);
                            p_s_mensaje := err_code || '-' || err_msg;
                            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                            p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                            RAISE_APPLICATION_ERROR(-20000,p_s_mensaje);

                 END;



         END IF;

           IF p_COD_DET = '24H' THEN

              BEGIN
               OPEN p_CABECERA FOR
                    SELECT DISTINCT (CC.COD_BCO_ORG)   AS BIC,
                    TI.DES_BCO  AS BANCO,
                    CC.NUM_CTA_CTE  AS CUENTA,
                    CC.COD_DVI  AS MONEDA,
                    CC.FEC_ING_CTL AS FECHA,
                    v_VAL_HORA AS HORA,
                    CC.IMP_TOT_FNL_DIA  AS SALDO_INCIAL,
                    CC.IMP_ING_24  AS INGRESO,
                    CC.IMP_EGR_24  AS EGRESO,
                    CC.IMP_TOT_24  AS SALDO_CIERRE,
                    CC.NUM_FOL_CTL  AS FOLIO_CARTOLA,
                    '0' AS LINEA_CREDITO,
                    TI.DES_CD       AS PLAZA
                    FROM TCDTBAI TI,
                    PABS_DT_DETLL_SALDO_CAJA_MN_MX CC, PABS_DT_CENTA_BANCO_CRPSL CRL
                    WHERE CC.COD_BCO_ORG = TI.TGCDSWSA
                    AND CC.NUM_FOL_CTL = p_FOLIO
                    AND SUBSTR(CC.COD_BCO_ORG,0,8) = SUBSTR(v_BANCO,0,8)
                    AND CC.COD_DVI = v_COD_DVI
                    AND CC.NUM_CTA_CTE = CRL.NUM_CTA_COR;
              EXCEPTION

                    WHEN NO_DATA_FOUND THEN

                            err_code := SQLCODE;
                            err_msg := 'No se han encontrado registos asociados a la busqueda por FOLIO :' || p_FOLIO || ' BANCO : ' || v_BANCO ;
                            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                            p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;

                    WHEN OTHERS THEN

                            err_code := SQLCODE;
                            err_msg := SUBSTR(SQLERRM, 1, 300);
                            p_s_mensaje := err_code || '-' || err_msg;
                            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                            p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                            RAISE_APPLICATION_ERROR(-20000,p_s_mensaje);

                 END;
           --Detalle cartola grilla...
               BEGIN

                      v_VAL_FECHA  := TO_CHAR(v_FECHA_24H,'D','NLS_DATE_LANGUAGE=SPANISH');

                         IF (v_VAL_FECHA = 6) THEN

                         v_FECHA_24H := v_FECHA_24H + 2;

                         END IF;

                   OPEN p_DETALLE FOR
                    SELECT DC.NUM_FOL_NMN AS REFERENCIA,
                    DC.FEC_VTA  AS FECHA,
                    DC.FEC_INS_OPE  AS HORA,
                    DC.FLG_ING_EGR  AS FLAG,
                    DECODE (DC.FLG_ING_EGR, 1,DC.IMP_OPE,0)     AS INGRESO,
                    DECODE (DC.FLG_ING_EGR, 0,DC.IMP_OPE,0)     AS EGRESO,
                    DC.GLS_MVT      AS GLOSA
                    FROM PABS_DT_DETLL_MVNTO_CAJA DC
                    WHERE
                    DC.FEC_VTA = v_FECHA_24H--REVISAR FECHA AND
                    AND (SUBSTR(DC.COD_BCO_ORG,0,8) = SUBSTR(v_BANCO,0,8) OR SUBSTR(DC.COD_BCO_DTN,0,8) = SUBSTR(v_BANCO,0,8))
                    AND DC.COD_DVI = v_COD_DVI;

               EXCEPTION

                        WHEN NO_DATA_FOUND THEN

                                err_code := SQLCODE;
                                err_msg := 'No se han encontrado registos asociados a la busqueda por FECHA :' || v_FECHA_ACTUAL || ' BANCO : ' || v_BANCO ;
                                PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                                p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;

                        WHEN OTHERS THEN

                                err_code := SQLCODE;
                                err_msg := SUBSTR(SQLERRM, 1, 300);
                                p_s_mensaje := err_code || '-' || err_msg;
                                PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                                p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                                RAISE_APPLICATION_ERROR(-20000,p_s_mensaje);

                     END;

         END IF;

           IF p_COD_DET = '48H' THEN -- CARTOLA

               BEGIN

               OPEN p_CABECERA FOR
                    SELECT DISTINCT (CC.COD_BCO_ORG)   AS BIC,
                    TI.DES_BCO  AS BANCO,
                    CC.NUM_CTA_CTE  AS CUENTA,
                    CC.COD_DVI  AS MONEDA,
                    CC.FEC_ING_CTL AS FECHA,
                    v_VAL_HORA AS HORA,
                    CC.IMP_TOT_24  AS SALDO_INCIAL,
                    CC.IMP_ING_48  AS INGRESO,
                    CC.IMP_EGR_48  AS EGRESO,
                    CC.IMP_TOT_48  AS SALDO_CIERRE,
                    CC.NUM_FOL_CTL  AS FOLIO_CARTOLA,
                    '0' AS LINEA_CREDITO,
                    TI.DES_CD       AS PLAZA
                    FROM TCDTBAI TI,
                    PABS_DT_DETLL_SALDO_CAJA_MN_MX CC, PABS_DT_CENTA_BANCO_CRPSL CRL
                    WHERE CC.COD_BCO_ORG = TI.TGCDSWSA
                    AND CC.NUM_FOL_CTL = p_FOLIO
                    AND SUBSTR(CC.COD_BCO_ORG,0,8) = SUBSTR(v_BANCO,0,8)
                    AND CC.COD_DVI = v_COD_DVI
                    AND CC.NUM_CTA_CTE = CRL.NUM_CTA_COR;
               EXCEPTION

                    WHEN NO_DATA_FOUND THEN

                            err_code := SQLCODE;
                            err_msg := 'No se han encontrado registos asociados a la busqueda por FOLIO :' || p_FOLIO || ' BANCO : ' || v_BANCO ;
                            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                            p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;

                    WHEN OTHERS THEN

                            err_code := SQLCODE;
                            err_msg := SUBSTR(SQLERRM, 1, 300);
                            p_s_mensaje := err_code || '-' || err_msg;
                            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                            p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                            RAISE_APPLICATION_ERROR(-20000,p_s_mensaje);

                 END;

           BEGIN

            v_VAL_FECHA  := TO_CHAR(v_FECHA_48H,'D','NLS_DATE_LANGUAGE=SPANISH');

                         IF (v_VAL_FECHA = 6) OR (v_VAL_FECHA = 7) THEN

                         v_FECHA_48H := v_FECHA_48H + 2;

                         END IF;

              OPEN p_DETALLE FOR
                    SELECT DC.NUM_FOL_NMN AS REFERENCIA,
                    DC.FEC_VTA  AS FECHA,
                    DC.FEC_INS_OPE  AS HORA,
                    DC.FLG_ING_EGR  AS FLAG,
                    DECODE (DC.FLG_ING_EGR, 1,DC.IMP_OPE,0)     AS INGRESO,
                    DECODE (DC.FLG_ING_EGR, 0,DC.IMP_OPE,0)     AS EGRESO,
                    DC.GLS_MVT      AS GLOSA
                    FROM PABS_DT_DETLL_MVNTO_CAJA DC
                    WHERE
                    DC.FEC_VTA = v_FECHA_48H--REVISAR FECHA AND
                    AND (SUBSTR(DC.COD_BCO_ORG,0,8) = SUBSTR(v_BANCO,0,8) OR SUBSTR(DC.COD_BCO_DTN,0,8) = SUBSTR(v_BANCO,0,8))
                    AND DC.COD_DVI = v_COD_DVI;
           EXCEPTION

                    WHEN NO_DATA_FOUND THEN

                            err_code := SQLCODE;
                            err_msg := 'No se han encontrado registos asociados a la busqueda por FECHA :' || v_FECHA_ACTUAL || ' BANCO : ' || v_BANCO ;
                            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                            p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;

                    WHEN OTHERS THEN

                            err_code := SQLCODE;
                            err_msg := SUBSTR(SQLERRM, 1, 300);
                            p_s_mensaje := err_code || '-' || err_msg;
                            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                            p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                            RAISE_APPLICATION_ERROR(-20000,p_s_mensaje);

                 END;


         END IF;
         -- FIN


    EXCEPTION
           WHEN OTHERS THEN

                    err_code := SQLCODE;
                    err_msg := SUBSTR(SQLERRM, 1, 300);
                    p_s_mensaje := err_code || '-' || err_msg;
                    PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                    p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                    RAISE_APPLICATION_ERROR(-20000,p_s_mensaje);

END SP_BUS_CRTLA_CTCTE_MX;



/************************************************************************************************
-- Funcion/Procedimiento: SP_BUS_BANCO_MX
-- Objetivo: Procedimiento almacenado que Busca los bancos corresponsales
-- Sistema: DBO_PAB.
-- Base de Datos: DBO_PAB.
-- Tablas Usadas: PABS_DT_CENTA_BANCO_CRPSL
--
-- Fecha: 02-11-2017
-- Autor: Santander
-- Input:  N/A
-- Output:
--        p_CURSOR : SYS_REFCURSOR, resultados de consulta.
--        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
-- Input/Output: N/A
-- Retorno: N/A.
-- Observaciones: <Fecha y Detalle de ultimos cambios>
--***********************************************************************************************/
PROCEDURE SP_BUS_BANCO_MX  (p_CURSOR          OUT SYS_REFCURSOR,
                            p_ERROR           OUT NUMBER)IS

     v_NOM_SP  VARCHAR2(30) := 'SP_BUS_BANCO_MX';
     v_FECHA_ACTUAL  DATE := TRUNC(SYSDATE);


    BEGIN
                --Cabecera cartola
         OPEN p_CURSOR FOR


         SELECT CRP.COD_BIC_BCO AS BANCO,
                CRP.COD_BIC_BCO AS BIC
                FROM PABS_DT_CENTA_BANCO_CRPSL CRP;



          EXCEPTION

            WHEN NO_DATA_FOUND THEN

                    err_code := SQLCODE;
                    err_msg := 'No se han encontrado registos asociados a la busqueda por Bancos.';
                    PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                    p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;

            WHEN OTHERS THEN

                    err_code := SQLCODE;
                    err_msg := SUBSTR(SQLERRM, 1, 300);
                    p_s_mensaje := err_code || '-' || err_msg;
                    PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                    p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                    RAISE_APPLICATION_ERROR(-20000,p_s_mensaje);

END SP_BUS_BANCO_MX;

/************************************************************************************************
-- Funcion/Procedimiento: SP_BUS_SALDO_BANCO_MX
-- Objetivo: Procedimiento almacenado que Busca los movimientos por banco selecccionado.
-- Sistema: DBO_PAB.
-- Base de Datos: DBO_PAB.
-- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX
--
-- Fecha: 02-11-2017
-- Autor: Santander
-- Input:
--   p_BANCO  --> Bic Banco a consultar.
--
-- Output:
--        p_CURSOR : SYS_REFCURSOR, resultados de consulta.
--        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
-- Input/Output: N/A
-- Retorno: N/A.
-- Observaciones: <Fecha y Detalle de ultimos cambios>
--***********************************************************************************************/
PROCEDURE SP_BUS_SALDO_BANCO_MX  (p_BANCO           IN CHAR,
                                  p_CURSOR          OUT SYS_REFCURSOR,
                                  p_ERROR           OUT NUMBER)IS

     v_NOM_SP  VARCHAR2(30) := 'SP_BUS_SALDO_BANCO_MX';
     v_FECHA_ACTUAL  DATE := TRUNC(SYSDATE);
     v_BANCO CHAR(11);


    BEGIN

        --En caso de querer ver todos los bancos
        IF(p_BANCO = '-1') THEN
            v_BANCO := NULL;
        ELSE
            v_BANCO := p_BANCO;
        END IF;

            --Cartolas por banco
        OPEN p_CURSOR FOR


          SELECT TI.DES_BCO  AS DSC_BCO_ORG,
                CC.COD_BCO_ORG  AS BIC,
                CC.NUM_CTA_CTE  AS CUENTA,
                SL.FEC_ING_CTL  AS FECHA,
                NVL(SL.IMP_INI_CTL,0)  AS SALDO_INICIAL,
                NVL(SL.IMP_ING_CTL,0)  AS INGRESOS,
                NVL(SL.IMP_EGR_CTL,0)  AS EGRESOS,
                NVL(SL.IMP_FNL_CTL,0)  AS SALDO_CIERRE,
                NVL(CC.IMP_ING_OVR,0)  AS INGRESOS_OVER,
                NVL(CC.IMP_EGR_OVR,0)  AS EGRESOS_OVER,
                NVL(SL.IMP_FNL_CTL,0)  AS SALDO_OVER,
                SL.NUM_FOL_CTL  AS FOLIO_CARTOLA,
                CC.COD_DVI      AS DIVISA
                FROM TCDTBAI TI,
            PABS_DT_CBCRA_CRTLA SL,
            PABS_DT_DETLL_SALDO_CAJA_MN_MX CC
            WHERE CC.COD_BCO_ORG = TI.TGCDSWSA (+)
            AND CC.COD_BCO_ORG = SL.COD_BCO_ORG (+)
            AND CC.NUM_CTA_CTE = SL.NUM_CTA_CTE -- se agrega detelle por cuenta
            AND CC.COD_BCO_ORG = NVL(v_BANCO,CC.COD_BCO_ORG)
            AND CC.COD_DVI <> PKG_PAB_CONSTANTES.V_STR_DVI_CLP
            AND CC.FEC_ING_CTL < SYSDATE;


    EXCEPTION

        WHEN NO_DATA_FOUND THEN

                err_code := SQLCODE;
                err_msg := 'No se han encontrado registos asociados a la busqueda por BANCO ' || v_BANCO ;
                PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;

        WHEN OTHERS THEN

                err_code := SQLCODE;
                err_msg := SUBSTR(SQLERRM, 1, 300);
                p_s_mensaje := err_code || '-' || err_msg;
                PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                RAISE_APPLICATION_ERROR(-20000,p_s_mensaje);

    END SP_BUS_SALDO_BANCO_MX;

/************************************************************************************************
-- Funcion/Procedimiento: SP_BUS_MONDA_ALMOT
-- Objetivo: Procedimiento almacenado que Muestra todas las monedas de altos montos
-- Sistema: DBO_PAB.
-- Base de Datos: DBO_PAB.
-- Tablas Usadas: PABS_DT_MONDA_ALMOT
--
-- Fecha: 07-11-2017
-- Autor: Santander
-- Input: N/A
--
-- Output:
--        p_CURSOR : SYS_REFCURSOR, resultados de consulta.
--        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
-- Input/Output: N/A
-- Retorno: N/A.
-- Observaciones: <Fecha y Detalle de ultimos cambios>
--***********************************************************************************************/
PROCEDURE SP_BUS_MONDA_ALMOT (p_CURSOR          OUT SYS_REFCURSOR,
                              p_ERROR           OUT NUMBER)IS

     v_NOM_SP  VARCHAR2(30) := 'SP_BUS_MONDA_ALMOT';



    BEGIN
            ---Posición banco central
         OPEN p_CURSOR FOR
                SELECT MO.COD_DVI AS MONEDA
                FROM PABS_DT_MONDA_ALMOT MO
                WHERE MO.COD_DVI <> PKG_PAB_CONSTANTES.V_STR_DVI_CLP
                ORDER BY MO.COD_DVI DESC;


        p_ERROR:= PKG_PAB_CONSTANTES.v_OK;

          EXCEPTION

            WHEN NO_DATA_FOUND THEN

                    err_code := SQLCODE;
                    err_msg := 'No se han encontrado registos asociados a la busqueda por MONEDA';
                    PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                    p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;

            WHEN OTHERS THEN

                    err_code := SQLCODE;
                    err_msg := 'No se han encontrado registos asociados a la busqueda por MONEDA';
                    p_s_mensaje := err_code || '-' || err_msg;
                    PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                    p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                    RAISE_APPLICATION_ERROR(-20000,p_s_mensaje);

END  SP_BUS_MONDA_ALMOT;




/************************************************************************************************
-- Funcion/Procedimiento: SP_BUS_ENC_SALDOS_MX
-- Objetivo: Procedimiento almacenado que Muestra encabezado de saldos MX
-- Sistema: DBO_PAB.
-- Base de Datos: DBO_PAB.
-- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX
--
-- Fecha: 14-11-2017
-- Autor: Santander
-- Input:
--          p_MONEDA  ->    Moneda a consultar.
-- Output:
--          p_CURSOR -> SYS_REFCURSOR, resultados de consulta.
--          p_ERROR  -> Indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
-- Input/Output: N/A
-- Retorno: N/A.
-- Observaciones: <Fecha y Detalle de ultimos cambios>
--***********************************************************************************************/
PROCEDURE SP_BUS_ENC_SALDOS_MX  (p_MONEDA         IN CHAR,
                            p_CURSOR          OUT SYS_REFCURSOR,
                            p_ERROR           OUT NUMBER)IS

     v_NOM_SP     VARCHAR2(30) := 'SP_BUS_ENC_SALDOS_MX';
     v_FECHA_ACTUAL  DATE := TRUNC(SYSDATE);
     v_IMP_INI_CTL NUMBER(18,2);
     v_IMP_ING_CTL NUMBER(18,2);
     v_IMP_EGR_CTL NUMBER(18,2);
     v_IMP_FNL_CTL NUMBER(18,2);
     v_IMP_INI_CTL_TOT NUMBER(18,2) := 0;
     v_IMP_ING_CTL_TOT NUMBER(18,2) := 0;
     v_IMP_EGR_CTL_TOT NUMBER(18,2) := 0;
     v_IMP_FNL_CTL_TOT NUMBER(18,2) := 0;

    BEGIN

       /* SE DEBE TRAER LAS FECHAS Y MAXIMAS CARTOLAS */

        BEGIN
            OPEN C_CONSULTA_CTAS_CTL(p_MONEDA);
        EXCEPTION
            WHEN OTHERS THEN
                err_code := SQLCODE;
                err_msg := SUBSTR(SQLERRM, 1, 300);
                p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
                PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                RAISE_APPLICATION_ERROR(-20000,err_msg);
        END;

          BEGIN
            FETCH C_CONSULTA_CTAS_CTL INTO R_CONSULTA_CTAS_CTL;
        EXCEPTION
            WHEN OTHERS THEN
                err_code := SQLCODE;
                err_msg := SUBSTR(SQLERRM, 1, 300);
                p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
                PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                --Verificamos si el cursor se encuentra abierto en caso de error.
                IF (C_CONSULTA_CTAS_CTL %ISOPEN) THEN
                    CLOSE C_CONSULTA_CTAS_CTL;
                END IF;
                RAISE_APPLICATION_ERROR(-20000,err_msg);
        END;

        WHILE C_CONSULTA_CTAS_CTL%FOUND
        LOOP
           -- SOLO SE DEBEN TOMAR LOS SALDOS DE LAS CUENTAS QUE NO TENGAN CARTOLA EL MISMO DIA.
            BEGIN
               SELECT  IMP_INI_CTL,
                       IMP_ING_CTL,
                       IMP_EGR_CTL,
                       IMP_FNL_CTL
              /*         
                SELECT SUM(IMP_INI_CTL),
                       SUM(IMP_ING_CTL),
                       SUM(IMP_EGR_CTL),
                       SUM(IMP_FNL_CTL)*/
                INTO v_IMP_INI_CTL,
                     v_IMP_ING_CTL,
                     v_IMP_EGR_CTL,
                     v_IMP_FNL_CTL
                FROM PABS_DT_CBCRA_CRTLA
                WHERE COD_BCO_ORG =  R_CONSULTA_CTAS_CTL.BANCO
                AND   NUM_CTA_CTE =  R_CONSULTA_CTAS_CTL.CUENTA
                AND   FEC_ING_CTL =  R_CONSULTA_CTAS_CTL.FEC_ULT_CAR_REC
                AND   COD_DVI = p_MONEDA;
             EXCEPTION
            WHEN NO_DATA_FOUND THEN
                      err_code := SQLCODE;
                      err_msg := 'No se pudo obtener la información de banco :' || R_CONSULTA_CTAS_CTL.BANCO || ' - cuenta : ' ||  R_CONSULTA_CTAS_CTL.CUENTA  || ' - fecha : ' || R_CONSULTA_CTAS_CTL.FEC_ULT_CAR_REC;
                      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                      p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
            WHEN OTHERS THEN
                err_code := SQLCODE;
                err_msg := SUBSTR(SQLERRM, 1, 300);
                p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
                PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                --Verificamos si el cursor se encuentra abierto en caso de error.
                IF (C_CONSULTA_CTAS_CTL %ISOPEN) THEN
                    CLOSE C_CONSULTA_CTAS_CTL;
                END IF;
                RAISE_APPLICATION_ERROR(-20000,err_msg);

            END;

            /* ACUMULAMOS EL MONTO.*/

            v_IMP_INI_CTL_TOT := NVL(v_IMP_INI_CTL_TOT,0) + NVL(v_IMP_INI_CTL,0);
            v_IMP_ING_CTL_TOT := NVL(v_IMP_ING_CTL_TOT,0) + NVL(v_IMP_ING_CTL,0);
            v_IMP_EGR_CTL_TOT := NVL(v_IMP_EGR_CTL_TOT,0) + NVL(v_IMP_EGR_CTL,0);
            v_IMP_FNL_CTL_TOT := NVL(v_IMP_FNL_CTL_TOT,0) + NVL(v_IMP_FNL_CTL,0);


           /*v_IMP_INI_CTL_TOT := v_IMP_INI_CTL;
            v_IMP_ING_CTL_TOT := v_IMP_ING_CTL;
            v_IMP_EGR_CTL_TOT := v_IMP_EGR_CTL;
            v_IMP_FNL_CTL_TOT := v_IMP_FNL_CTL;*/



            FETCH C_CONSULTA_CTAS_CTL INTO R_CONSULTA_CTAS_CTL;

        END LOOP;

        CLOSE C_CONSULTA_CTAS_CTL;

        BEGIN
            OPEN p_CURSOR FOR
             SELECT v_IMP_INI_CTL_TOT AS SALDO_INICIAL,
                    v_IMP_ING_CTL_TOT AS INGRESO_CIERRE,
                    v_IMP_EGR_CTL_TOT AS EGRESOS_CIERRE,
                    v_IMP_FNL_CTL_TOT AS SALDO_CIERRE
                    FROM DUAL;

        EXCEPTION
            WHEN NO_DATA_FOUND THEN

                err_code := SQLCODE;
                err_msg := 'No se han encontrado registos asociados a la busqueda por MONEDA ' || p_MONEDA;
                PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
        END;

    EXCEPTION
        WHEN NO_DATA_FOUND THEN

                err_code := SQLCODE;
                err_msg := 'No se han encontrado registos asociados a la busqueda por MONEDA ' || p_MONEDA;
                PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;

        WHEN OTHERS THEN

                --Verificamos si el cursor se encuentra abierto en caso de error.
                IF (C_CONSULTA_CTAS_CTL %ISOPEN) THEN
                    CLOSE C_CONSULTA_CTAS_CTL;
                END IF;

                err_code := SQLCODE;
                err_msg := SUBSTR(SQLERRM, 1, 300);
                p_s_mensaje := err_code || '-' || err_msg;
                PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                RAISE_APPLICATION_ERROR(-20000,p_s_mensaje);

    END SP_BUS_ENC_SALDOS_MX;

/************************************************************************************************
-- Funcion/Procedimiento: SP_BUS_ENC_SALDOS_INTRA_MX
-- Objetivo: Procedimiento almacenado que Muestra encabezado de saldos intradía MX
-- Sistema: DBO_PAB.
-- Base de Datos: DBO_PAB.
-- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX
--
-- Fecha: 14-11-2017
-- Autor: Santander
-- Input:
--          p_MONEDA  ->    Moneda a consultar.
-- Output:
--          p_CURSOR -> SYS_REFCURSOR, resultados de consulta.
--          p_ERROR  -> Indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
-- Input/Output: N/A
-- Retorno: N/A.
-- Observaciones: <Fecha y Detalle de ultimos cambios>
--***********************************************************************************************/
PROCEDURE SP_BUS_ENC_SALDOS_INTRA_MX  (p_MONEDA         IN CHAR,
                            p_CURSOR          OUT SYS_REFCURSOR,
                            p_ERROR           OUT NUMBER)IS

     v_NOM_SP     VARCHAR2(30) := 'SP_BUS_ENC_SALDOS_MX';
     v_FECHA_ACTUAL  DATE := TRUNC(SYSDATE);


    BEGIN

         OPEN p_CURSOR FOR
         SELECT SUM(SL.IMP_APR_AOS) AS SALDO_INICIAL,
                SUM(SL.IMP_ING_CPR) AS INGRESO_CIERRE,
                SUM(SL.IMP_EGR_CPR) AS EGRESOS_CIERRE,
                SUM(SL.IMP_TOT_FNL_DIA) AS SALDO_CIERRE
                FROM PABS_DT_DETLL_SALDO_CAJA_MN_MX SL
                WHERE SL.COD_DVI = p_MONEDA;

          EXCEPTION

            WHEN NO_DATA_FOUND THEN

                    err_code := SQLCODE;
                    err_msg := 'No se han encontrado registos asociados a la busqueda por MONEDA ' || p_MONEDA;
                    PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                    p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;

            WHEN OTHERS THEN

                    err_code := SQLCODE;
                    err_msg := SUBSTR(SQLERRM, 1, 300);
                    p_s_mensaje := err_code || '-' || err_msg;
                    PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                    p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                    RAISE_APPLICATION_ERROR(-20000,p_s_mensaje);


END SP_BUS_ENC_SALDOS_INTRA_MX;





/************************************************************************************************
-- Funcion/Procedimiento: SP_BUS_ENC_SALDOS_24HRS_MX
-- Objetivo: Procedimiento almacenado que Muestra encabezado de saldos 24 horas MX
-- Sistema: DBO_PAB.
-- Base de Datos: DBO_PAB.
-- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX
--
-- Fecha: 14-11-2017
-- Autor: Santander
-- Input:
--          p_MONEDA  ->    Moneda a consultar.
-- Output:
--          p_CURSOR -> SYS_REFCURSOR, resultados de consulta.
--          p_ERROR  -> Indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
-- Input/Output: N/A
-- Retorno: N/A.
-- Observaciones: <Fecha y Detalle de ultimos cambios>
--***********************************************************************************************/
PROCEDURE SP_BUS_ENC_SALDOS_24HRS_MX  (p_MONEDA         IN CHAR,
                            p_CURSOR          OUT SYS_REFCURSOR,
                            p_ERROR           OUT NUMBER)IS

     v_NOM_SP     VARCHAR2(30) := 'SP_BUS_ENC_SALDOS_MX';
     v_FECHA_ACTUAL  DATE ;
     P_VAL_FEC NUMBER;
     V_IMP_TOT_FNL_DIA NUMBER;



    BEGIN

        v_FECHA_ACTUAL := TO_DATE(SYSDATE + 1 ,'DD-MM-RRRR'); -- SALDO 24
        P_VAL_FEC:= TO_CHAR(v_FECHA_ACTUAL,'D','NLS_DATE_LANGUAGE=SPANISH');

        IF P_VAL_FEC = 6 THEN -- SABADO

            v_FECHA_ACTUAL := TO_DATE(v_FECHA_ACTUAL + 2 ,'DD-MM-RRRR');

             ELSE
            v_FECHA_ACTUAL := TRUNC(SYSDATE);


        END IF;

            BEGIN

             SELECT SUM(SL.IMP_TOT_FNL_DIA)
             INTO V_IMP_TOT_FNL_DIA
              FROM
             PABS_DT_DETLL_SALDO_CAJA_MN_MX SL WHERE COD_DVI = p_MONEDA;
             EXCEPTION

                WHEN NO_DATA_FOUND THEN

                        err_code := SQLCODE;
                        err_msg := 'No se han encontrado monto total asociados a la busqueda por MONEDA ' || p_MONEDA;
                        PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                        p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;

                WHEN OTHERS THEN

                        err_code := SQLCODE;
                        err_msg := SUBSTR(SQLERRM, 1, 300);
                        p_s_mensaje := err_code || '-' || err_msg;
                        PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                        p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                        RAISE_APPLICATION_ERROR(-20000,p_s_mensaje);
            END;

            BEGIN
                OPEN p_CURSOR FOR
                 SELECT SUM(SL.IMP_TOT_FNL_DIA) AS SALDO_INICIAL,
                        SUM(SL.IMP_ING_24) AS INGRESO_CIERRE,
                        SUM(SL.IMP_EGR_24) AS EGRESOS_CIERRE,
                        SUM(SL.IMP_TOT_24) AS SALDO_CIERRE
                        FROM PABS_DT_DETLL_SALDO_CAJA_MN_MX SL
                        WHERE SL.COD_DVI = p_MONEDA;



            EXCEPTION

                WHEN NO_DATA_FOUND THEN

                        err_code := SQLCODE;
                        err_msg := 'No se han encontrado registos asociados a la busqueda por MONEDA ' || p_MONEDA;
                        PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                        p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;

                WHEN OTHERS THEN

                        err_code := SQLCODE;
                        err_msg := SUBSTR(SQLERRM, 1, 300);
                        p_s_mensaje := err_code || '-' || err_msg;
                        PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                        p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                        RAISE_APPLICATION_ERROR(-20000,p_s_mensaje);
                END ;
END SP_BUS_ENC_SALDOS_24HRS_MX;




/************************************************************************************************
-- Funcion/Procedimiento: SP_BUS_ENC_SALDOS_48HRS_MX
-- Objetivo: Procedimiento almacenado que Muestra encabezado de saldos 48 horas MX
-- Sistema: DBO_PAB.
-- Base de Datos: DBO_PAB.
-- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX
--
-- Fecha: 14-11-2017
-- Autor:
-- Input:
--          p_MONEDA  ->    Moneda a consultar.
-- Output:
--          p_CURSOR -> SYS_REFCURSOR, resultados de consulta.
--          p_ERROR  -> Indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
-- Input/Output: N/A
-- Retorno: N/A.
-- Observaciones: <Fecha y Detalle de ultimos cambios>
--***********************************************************************************************/
PROCEDURE SP_BUS_ENC_SALDOS_48HRS_MX  (p_MONEDA         IN CHAR,
                            p_CURSOR          OUT SYS_REFCURSOR,
                            p_ERROR           OUT NUMBER)IS

     v_NOM_SP     VARCHAR2(30) := 'SP_BUS_ENC_SALDOS_MX';
     v_FECHA_ACTUAL  DATE ;
     P_VAL_FEC NUMBER;
     V_IMP_TOT_FNL_DIA NUMBER;



 BEGIN

     v_FECHA_ACTUAL := TO_DATE(SYSDATE + 2 ,'DD-MM-RRRR'); -- SALDO 48
        P_VAL_FEC:= TO_CHAR(v_FECHA_ACTUAL,'D','NLS_DATE_LANGUAGE=SPANISH');

        IF P_VAL_FEC = 7 THEN -- SABADO

            v_FECHA_ACTUAL := TO_DATE(v_FECHA_ACTUAL + 3 ,'DD-MM-RRRR');

           ELSIF P_VAL_FEC = 0 THEN

             v_FECHA_ACTUAL := TO_DATE(v_FECHA_ACTUAL + 3 ,'DD-MM-RRRR');

             ELSE
            v_FECHA_ACTUAL := TRUNC(SYSDATE);

           v_FECHA_ACTUAL := TRUNC(SYSDATE);
        END IF;

        BEGIN

             SELECT SUM(SL.IMP_TOT_24)
             INTO V_IMP_TOT_FNL_DIA
             FROM
             PABS_DT_DETLL_SALDO_CAJA_MN_MX SL WHERE COD_DVI = p_MONEDA;

        EXCEPTION

        WHEN NO_DATA_FOUND THEN

                    err_code := SQLCODE;
                    err_msg := 'No se encuentran registros asociados a MONEDA '|| p_MONEDA;
                    PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                    p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;

            WHEN OTHERS THEN

                    err_code := SQLCODE;
                    err_msg := SUBSTR(SQLERRM, 1, 300);
                    p_s_mensaje := err_code || '-' || err_msg;
                    PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                    p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                    RAISE_APPLICATION_ERROR(-20000,p_s_mensaje);
         END;

        BEGIN

         OPEN p_CURSOR FOR
         SELECT  SUM(SL.IMP_TOT_24) SALDO_INICIAL,
                SUM(SL.IMP_ING_48) AS INGRESO_CIERRE,
                SUM(SL.IMP_EGR_48) AS EGRESOS_CIERRE,
                SUM(SL.IMP_TOT_48) AS SALDO_CIERRE
                FROM PABS_DT_DETLL_SALDO_CAJA_MN_MX SL
                WHERE SL.COD_DVI = p_MONEDA;
        EXCEPTION
        WHEN NO_DATA_FOUND THEN

                    err_code := SQLCODE;
                    err_msg := 'No se encuentran registros asociados a la moneda  '|| p_MONEDA;
                    PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                    p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;

            WHEN OTHERS THEN

                    err_code := SQLCODE;
                    err_msg := SUBSTR(SQLERRM, 1, 300);
                    p_s_mensaje := err_code || '-' || err_msg;
                    PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                    p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                    RAISE_APPLICATION_ERROR(-20000,p_s_mensaje);
          END;


END SP_BUS_ENC_SALDOS_48HRS_MX;



/************************************************************************************************
-- Funcion/Procedimiento: SP_GES_CAJA_EGRESO_MX
-- Objetivo: Procedimiento que actualiza los saldos de operaciones MX recepcionadas tras una confirmación de Swift
-- Sistema: DBO_PAB.
-- Base de Datos: DBO_PAB.
-- Tablas Usadas: PABS_DT_DETLL_OPRCN,PABS_DT_DETLL_SALDO_CAJA_MN_MX
-- Fecha: 14-11-2017
-- Autor: Santander
-- Input:
-- p_NUM_REF_SWF  -> Referencia de operación a actualizar
-- Output:
-- p_ERROR        -> Indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
-- Input/Output: N/A
-- Retorno: N/A.
-- Observaciones: <Fecha y Detalle de ultimos cambios>
--***********************************************************************************************/
PROCEDURE SP_GES_CAJA_EGRESO_MX (p_NUM_REF_SWF      IN VARCHAR2,
                                 p_ERROR           OUT NUMBER)IS

    v_NOM_SP        VARCHAR2(30) := 'SP_GES_CAJA_EGRESO_MX';
    v_NUM_REF_SWF   CHAR(16);
    v_IMP_OPE       NUMBER(15,2);
    v_FLG_EGR_ING   NUMBER(1);
    v_COD_DVI       CHAR(3);
    v_COD_BCO_ORG   CHAR(11);
    v_COD_BCO_DTN   CHAR(11);
    v_NUM_CTA_CTE   VARCHAR2(30);
    v_IMP_ING_DIA   NUMBER(15,2);
    v_IMP_EGR_DIA   NUMBER(15,2);
    v_IMP_RAL_AOS   NUMBER(15,2);
    v_FEC_INS       DATE:= TRUNC(SYSDATE);
    ERROR_REFSWF    EXCEPTION;

    BEGIN

            v_NUM_REF_SWF := p_NUM_REF_SWF;

            BEGIN
                --Obtenemos la informacion de la operacion origen
                SELECT IMP_OPE, COD_DVI, COD_BCO_DTN, NUM_CTA_BFC
                INTO v_IMP_OPE, v_COD_DVI, v_COD_BCO_DTN, v_NUM_CTA_CTE
                FROM PABS_DT_DETLL_OPRCN
                WHERE NUM_REF_SWF = v_NUM_REF_SWF AND
                      COD_DVI <> PKG_PAB_CONSTANTES.V_STR_DVI_CLP AND
                      FLG_EGR_ING = PKG_PAB_CONSTANTES.V_FLG_EGR;

                IF( v_IMP_OPE IS NULL OR v_COD_DVI IS NULL OR v_COD_BCO_DTN IS NULL OR v_NUM_CTA_CTE IS NULL ) THEN
                    RAISE ERROR_REFSWF;
                END IF;

            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    err_code := SQLCODE;
                    err_msg := 'No se pudo obtener la información de la referencia:' || v_NUM_REF_SWF;
                    PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                    p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                    RAISE;

                WHEN OTHERS THEN
                    err_code := SQLCODE;
                    err_msg := SUBSTR(SQLERRM, 1, 300);
                    p_s_mensaje := err_code || '-' || err_msg;
                    PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                    p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                    RAISE_APPLICATION_ERROR(-20000,p_s_mensaje);
            END;

            BEGIN
                            INSERT INTO PABS_DT_DETLL_SALDO_CAJA_MN_MX (
                                    FEC_ING_CTL,
                                    COD_BCO_ORG,
                                    NUM_CTA_CTE,
                                    COD_DVI,
                                    IMP_EGR_DIA,
                                    IMP_RAL_AOS
                                    )
                                    VALUES
                                    (
                                    v_FEC_INS,
                                    v_COD_BCO_DTN,
                                    v_NUM_CTA_CTE,
                                    v_COD_DVI,
                                    v_IMP_OPE,
                                    v_IMP_OPE
                                    );
                EXCEPTION
                WHEN DUP_VAL_ON_INDEX THEN

                    --Actualiza Saldos en Tabla Saldo MN MX
                    UPDATE PABS_DT_DETLL_SALDO_CAJA_MN_MX
                        SET
                            FEC_ING_CTL    = v_FEC_INS,
                            IMP_EGR_DIA    = IMP_EGR_DIA+v_IMP_OPE,
                            IMP_RAL_AOS    = IMP_RAL_AOS+v_IMP_OPE
                        WHERE
                            COD_BCO_ORG    = v_COD_BCO_DTN AND
                            NUM_CTA_CTE    = v_NUM_CTA_CTE AND
                            COD_DVI        = v_COD_DVI;

                WHEN OTHERS THEN
                err_code := SQLCODE;
                err_msg := SUBSTR(SQLERRM, 1, 300);
                p_s_mensaje := err_code || '-' || err_msg;
                PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                RAISE_APPLICATION_ERROR(-20000,p_s_mensaje);

            END;

            p_ERROR:= PKG_PAB_CONSTANTES.v_OK;

            EXCEPTION
                WHEN ERROR_REFSWF THEN
                    err_msg := 'Datos requeridos vienen nulos para referencia:' || p_NUM_REF_SWF;
                    PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( NULL, v_NOM_PCK ,err_msg , v_NOM_SP);
                    p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;

                WHEN NO_DATA_FOUND THEN
                        err_code := SQLCODE;
                        err_msg := SUBSTR(SQLERRM, 1, 300);
                        PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                        p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;

                WHEN OTHERS THEN
                        err_code := SQLCODE;
                        err_msg := SUBSTR(SQLERRM, 1, 300);
                        p_s_mensaje := err_code || '-' || err_msg;
                        PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                        p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                        RAISE_APPLICATION_ERROR(-20000,p_s_mensaje);

    END  SP_GES_CAJA_EGRESO_MX;



/************************************************************************************************
-- Funcion/Procedimiento: SP_OBT_MOV_PROY_MX
-- Objetivo: Procedimiento que obtiene los movimientos cargados proyectados MX
-- Sistema: DBO_PAB.
-- Base de Datos: DBO_PAB.
-- Tablas Usadas: PABS_DT_DETLL_MVNTO_CAJA
-- Fecha: 14-11-2017
-- Autor: Santander
-- Input:
-- N/A
-- Output:
-- p_CURSOR -> SYS_REFCURSOR, resultados de consulta.
-- p_ERROR        -> Indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
-- Input/Output: N/A
-- Retorno: N/A.
-- Observaciones: <Fecha y Detalle de ultimos cambios>
--***********************************************************************************************/
PROCEDURE SP_OBT_MOV_PROY_MX ( p_CURSOR          OUT SYS_REFCURSOR,
                               p_ERROR           OUT NUMBER)IS

    v_NOM_SP        VARCHAR2(30) := 'SP_OBT_MOV_PROY_MX';
    v_FEC_INS       DATE:= TRUNC(SYSDATE);
    ERROR_REFSWF    EXCEPTION;

    BEGIN


          OPEN p_CURSOR FOR
          SELECT NUM_FOL_OPE AS NUM_OPE,
                 NUM_FOL_NMN AS NUM_NMN,
                 DECODE (FLG_ING_EGR,PKG_PAB_CONSTANTES.V_FLG_EGR,'Egreso','Ingreso') AS FLUJO,
                 DECODE (FLG_ING_EGR,PKG_PAB_CONSTANTES.V_FLG_EGR,COD_BCO_DTN,COD_BCO_ORG) AS BIC_BANCO,
                 PKG_PAB_UTILITY.FN_PAB_OBT_DSC_BCO(DECODE (FLG_ING_EGR,PKG_PAB_CONSTANTES.V_FLG_EGR,COD_BCO_DTN,COD_BCO_ORG)) AS DSC_BCO_ORG,
                 NUM_CTA_CTE AS CTA_CTE,
                 COD_TPO_OPE_AOS AS TIPO_OPE,
                 COD_DVI AS MONEDA,
                 IMP_OPE AS MONTO,
                 TO_CHAR(FEC_VTA,'DD-MM-YYYY') AS FEC_VTA, --
                 GLS_SIS_ORG AS OPE_ORI,
                 GLS_MVT AS OBSERVACION
                FROM PABS_DT_DETLL_MVNTO_CAJA
                WHERE FEC_VTA >= v_FEC_INS
                AND COD_DVI <> PKG_PAB_CONSTANTES.V_STR_DVI_CLP;

          EXCEPTION

            WHEN NO_DATA_FOUND THEN

                    err_code := SQLCODE;
                    err_msg := 'No existen registros para fecha :' || v_FEC_INS;
                    PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                    p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;

            WHEN OTHERS THEN

                    err_code := SQLCODE;
                    err_msg := SUBSTR(SQLERRM, 1, 300);
                    p_s_mensaje := err_code || '-' || err_msg;
                    PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                    p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                    RAISE_APPLICATION_ERROR(-20000,p_s_mensaje);

    END  SP_OBT_MOV_PROY_MX;



/************************************************************************************************
-- Funcion/Procedimiento: SP_ELI_MOV_PROY_MX
-- Objetivo: Procedimiento que ELIMINA movimientos cargados proyectados MX
-- Sistema: DBO_PAB.
-- Base de Datos: DBO_PAB.
-- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX
-- Fecha: 14-11-2017
-- Autor: Santander
-- Input:
-- p_NUM_FOL_OPE        -> Número de operación
-- p_COD_BIC_ORG        -> Código Bic Banco
-- p_COD_DIV            -> Código Divisa
-- p_NUM_CTA_CTE        -> Número cuenta corriente
-- Output:
-- p_ERROR                -> Indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
-- Input/Output: N/A
-- Retorno: N/A.
-- Observaciones: <Fecha y Detalle de ultimos cambios>
--***********************************************************************************************/
PROCEDURE SP_ELI_MOV_PROY_MX ( p_NUM_FOL_OPE        IN NUMBER,
                               p_COD_BIC_ORG        IN VARCHAR2,
                               p_COD_DIV            IN CHAR,
                               p_NUM_CTA_CTE        IN VARCHAR2,
                               p_ERROR           OUT NUMBER)
                               IS

    v_NOM_SP        VARCHAR2(30) := 'SP_ELI_MOV_PROY_MX';
    v_FEC_INS       DATE:= TRUNC(SYSDATE);
    v_NUM_FOL_OPE     NUMBER (12)  := SPK_PAB_OPE_CJA_AM.NEXTVAL;
    v_COD_BCO_DTN     CHAR(11) := PKG_PAB_CONSTANTES.V_BIC_BANCO_SANTANDER;
    v_COD_BCO_ORG     CHAR(11) := PKG_PAB_CONSTANTES.V_BIC_BANCO_SANTANDER;
    v_COD_BCO         CHAR(11) := '0';
    v_NUM_DOC_EPS     CHAR(11);
    v_COD_TPO_DOC_EPS CHAR(1);
    v_IMP_ING_DIA     NUMBER(15,2);
    v_IMP_EGR_DIA     NUMBER(15,2);
    v_IMP_RAL_AOS     NUMBER(15,2);
    v_IMP_ING_24      NUMBER(15,2);
    v_IMP_EGR_24      NUMBER(15,2);
    v_IMP_TOT_24      NUMBER(15,2);
    v_IMP_ING_48      NUMBER(15,2);
    v_IMP_EGR_48      NUMBER(15,2);
    v_IMP_TOT_48      NUMBER(15,2);
    v_COD_DVI         CHAR(3);
    v_VAL_FEC         NUMBER := 0;
    v_IND_HABIL       NUMBER := 0;
    v_FLG_FECHA       NUMBER := 0;

    v_IMP_RAL_AOS_SAL     NUMBER(15,2);
    v_IMP_TOT_24_SAL      NUMBER(15,2);
    v_IMP_TOT_48_SAL      NUMBER(15,2);
    v_IMP_APR_AOS_SAL     NUMBER(15,2);
    v_BIC_CENTRAL         CHAR(11) := 'BCECCLRR'; -- CAMBIAR A VARIABLE
    v_CTA_CENTRAL         VARCHAR2(30) := '34220102314';-- CAMBIAR A VARIABLE

    v_FECHA_ACTUAL  DATE ;
    P_VAL_FEC NUMBER;

    ERROR_REFSWF    EXCEPTION;

 BEGIN


            v_COD_DVI := p_COD_DIV;
            v_COD_BCO := p_COD_BIC_ORG ;

          DELETE FROM PABS_DT_DETLL_MVNTO_CAJA
          WHERE NUM_FOL_OPE = p_NUM_FOL_OPE ;

          COMMIT;
          /* REALIZAMOS PROCESO DE RECALCUO */
        BEGIN
          SELECT
                    IMP_APR_AOS
                    INTO
                    v_IMP_APR_AOS_SAL
                    FROM PABS_DT_DETLL_SALDO_CAJA_MN_MX SALDO
                    WHERE
                    SALDO.NUM_CTA_CTE = p_NUM_CTA_CTE AND
                    SALDO.COD_DVI = v_COD_DVI AND
                    SUBSTR(SALDO.COD_BCO_ORG,0,8) = SUBSTR(v_COD_BCO,0,8);
           EXCEPTION

            WHEN NO_DATA_FOUND THEN

                    err_code := SQLCODE;
                    err_msg := 'No se han encontrado Saldo Apertura para cuenta:  ' || p_NUM_CTA_CTE || ' - Divisa : ' || v_COD_DVI || ' - Banco : '|| v_COD_BCO ;
                    PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                    p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;

            WHEN OTHERS THEN

                    err_code := SQLCODE;
                    err_msg := SUBSTR(SQLERRM, 1, 300);
                    p_s_mensaje := err_code || '-' || err_msg;
                    PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                    p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                    RAISE_APPLICATION_ERROR(-20000,p_s_mensaje);

           END;

                   --Se rescata saldos intradía
                    PKG_PAB_CAJA.SP_PAB_CNA_SLD_CJA(v_FEC_INS,p_NUM_CTA_CTE,v_COD_DVI,v_COD_BCO, v_IMP_ING_DIA, v_IMP_EGR_DIA, v_IMP_RAL_AOS, p_ERROR);

                    UPDATE PABS_DT_DETLL_SALDO_CAJA_MN_MX
                           SET
                                IMP_ING_CPR     = (NVL(v_IMP_ING_DIA,0)), --INGRESOS COMPROMETIDOS
                                IMP_EGR_CPR     = (NVL(v_IMP_EGR_DIA,0)), -- EGRESOS COMPROMETIDOS.
                                IMP_TOT_FNL_DIA = (NVL(v_IMP_APR_AOS_SAL,0) + NVL(v_IMP_ING_DIA,0) - NVL(v_IMP_EGR_DIA,0)) --  TOTAL COMPROMETIDO
                           WHERE
                                SUBSTR(COD_BCO_ORG,0,8)    = SUBSTR(v_COD_BCO,0,8) AND
                                NUM_CTA_CTE    = p_NUM_CTA_CTE AND
                                COD_DVI        = v_COD_DVI;

           BEGIN
                    SELECT
                            IMP_TOT_FNL_DIA
                    INTO
                            v_IMP_RAL_AOS_SAL
                    FROM PABS_DT_DETLL_SALDO_CAJA_MN_MX SALDO
                    WHERE
                            SALDO.NUM_CTA_CTE = p_NUM_CTA_CTE AND
                            SALDO.COD_DVI = v_COD_DVI AND
                            SUBSTR(SALDO.COD_BCO_ORG,0,8) = SUBSTR(v_COD_BCO,0,8);

             EXCEPTION

            WHEN NO_DATA_FOUND THEN

                        err_code := SQLCODE;
                        err_msg := 'No se han encontrado Saldo Final para cuenta:  ' || p_NUM_CTA_CTE || ' - Divisa : ' || v_COD_DVI || ' - Banco : '|| v_COD_BCO ;
                        PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                        p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;

            WHEN OTHERS THEN

                    err_code := SQLCODE;
                    err_msg := SUBSTR(SQLERRM, 1, 300);
                    p_s_mensaje := err_code || '-' || err_msg;
                    PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                    p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                    RAISE_APPLICATION_ERROR(-20000,p_s_mensaje);

           END;


           /* MODIFICACION PARA CALCULO DE SALDOS 24 HRS */

           v_FECHA_ACTUAL := TO_DATE(SYSDATE + 1 ,'DD-MM-RRRR'); -- SALDO 24
           P_VAL_FEC:= TO_CHAR(v_FECHA_ACTUAL,'D','NLS_DATE_LANGUAGE=SPANISH');


            IF P_VAL_FEC = 6 THEN -- SABADO

                v_FECHA_ACTUAL := TO_DATE(v_FECHA_ACTUAL + 2 ,'DD-MM-RRRR');

                 ELSE
                v_FECHA_ACTUAL := TRUNC(SYSDATE + 1);


            END IF;


                  --Se rescata saldos 24 horas
                    PKG_PAB_CAJA.SP_PAB_CNA_SLD_CJA(v_FECHA_ACTUAL,p_NUM_CTA_CTE,v_COD_DVI,v_COD_BCO,v_IMP_ING_24, v_IMP_EGR_24, v_IMP_TOT_24, p_ERROR);



                    UPDATE PABS_DT_DETLL_SALDO_CAJA_MN_MX
                           SET
                                IMP_ING_24     = (NVL(v_IMP_ING_24,0)),
                                IMP_EGR_24     = (NVL(v_IMP_EGR_24,0)),
                                IMP_TOT_24     = (NVL(IMP_TOT_FNL_DIA,0) + NVL(v_IMP_ING_24,0) - NVL(v_IMP_EGR_24,0))

                            WHERE
                                SUBSTR(COD_BCO_ORG,0,8)    = SUBSTR(v_COD_BCO,0,8) AND
                                NUM_CTA_CTE    = p_NUM_CTA_CTE AND
                                COD_DVI        = v_COD_DVI;


           /* MODIFICACION PARA CALCULO DE SALDOS 24 HRS */

        v_FECHA_ACTUAL := TO_DATE(SYSDATE + 2 ,'DD-MM-RRRR'); -- SALDO 48
        P_VAL_FEC:= TO_CHAR(v_FECHA_ACTUAL,'D','NLS_DATE_LANGUAGE=SPANISH');



        IF P_VAL_FEC = 6 THEN -- SABADO

            v_FECHA_ACTUAL := TO_DATE(v_FECHA_ACTUAL + 2 ,'DD-MM-RRRR');

           ELSIF P_VAL_FEC = 7 THEN

             v_FECHA_ACTUAL := TO_DATE(v_FECHA_ACTUAL + 2 ,'DD-MM-RRRR');

             ELSE
            v_FECHA_ACTUAL := TRUNC(SYSDATE + 2);

        END IF;

                    --Se rescata saldos 48 horas.
                    PKG_PAB_CAJA.SP_PAB_CNA_SLD_CJA(v_FECHA_ACTUAL,p_NUM_CTA_CTE,v_COD_DVI,v_COD_BCO, v_IMP_ING_48, v_IMP_EGR_48, v_IMP_TOT_48, p_ERROR);

            BEGIN
                  SELECT
                            IMP_TOT_24
                    INTO
                            v_IMP_TOT_24_SAL
                    FROM PABS_DT_DETLL_SALDO_CAJA_MN_MX SALDO
                    WHERE
                            SALDO.NUM_CTA_CTE = p_NUM_CTA_CTE AND
                            SALDO.COD_DVI = v_COD_DVI AND
                            SUBSTR(SALDO.COD_BCO_ORG,0,8) = SUBSTR(v_COD_BCO,0,8);

             EXCEPTION
             WHEN NO_DATA_FOUND THEN

                        err_code := SQLCODE;
                        err_msg := 'No se han encontrado Saldo 24 Hrs para cuenta:  ' || p_NUM_CTA_CTE || ' - Divisa : ' || v_COD_DVI || ' - Banco : '|| v_COD_BCO ;
                        PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                        p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;

             WHEN OTHERS THEN

                    err_code := SQLCODE;
                    err_msg := SUBSTR(SQLERRM, 1, 300);
                    p_s_mensaje := err_code || '-' || err_msg;
                    PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                    p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                    RAISE_APPLICATION_ERROR(-20000,p_s_mensaje);

             END;


                    UPDATE PABS_DT_DETLL_SALDO_CAJA_MN_MX
                           SET
                                IMP_ING_48     = (NVL(v_IMP_ING_48,0)),
                                IMP_EGR_48     = (NVL(v_IMP_EGR_48,0)),
                                IMP_TOT_48     = (NVL(v_IMP_TOT_24_SAL,0) + NVL(v_IMP_ING_48,0) - NVL(v_IMP_EGR_48,0))
                            WHERE
                                SUBSTR(COD_BCO_ORG,0,8)    = SUBSTR(v_COD_BCO,0,8) AND
                                NUM_CTA_CTE    = p_NUM_CTA_CTE AND
                                COD_DVI        = v_COD_DVI;

             COMMIT;
          EXCEPTION

            WHEN OTHERS THEN

                    err_code := SQLCODE;
                    err_msg := SUBSTR(SQLERRM, 1, 300);
                    p_s_mensaje := err_code || '-' || err_msg;
                    PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                    p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                    RAISE_APPLICATION_ERROR(-20000,p_s_mensaje);

    END  SP_ELI_MOV_PROY_MX;

/************************************************************************************************
-- Funcion/Procedimiento: SP_ACT_SLD_EGR_MX
-- Objetivo: Actualizacion de saldos de egreso de moneda extranjera
-- Sistema: DBO_PAB.
-- Base de Datos: DBO_PAB.
-- Tablas Usadas: DUAL
-- Fecha: 14-11-2017
-- Autor: Santander
-- Input:
-- p_NUM_REF_SWF        -> Número referencia
-- Output:
-- p_ERROR                -> Indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
-- Input/Output: N/A
-- Retorno: N/A.
-- Observaciones: <Fecha y Detalle de ultimos cambios>
--***********************************************************************************************/

PROCEDURE SP_ACT_SLD_EGR_MX ( p_NUM_REF_SWF     IN  VARCHAR2,
                              p_ERROR           OUT NUMBER)IS

    v_NOM_SP        VARCHAR2(30) := 'SP_ACT_SLD_EGR_MX';
    v_FEC_INS       DATE:= TRUNC(SYSDATE);
    ERROR_REFSWF    EXCEPTION;

    v_NUM_REF_SWF     CHAR(16);
    v_IMP_OPE         NUMBER(15,2);
    v_COD_BCO_BFC     CHAR(11);
    v_COD_BCO_DTN     CHAR(11);
    v_NUM_CTA_BFC     VARCHAR2(30);
    v_COD_DVI         CHAR(3);
    v_IMP_EGR_DIA     NUMBER(15,2);
    v_IMP_ING_DIA     NUMBER(15,2);
    v_IMP_RAL_AOS     NUMBER(15,2);
    v_IMP_EGR_DIA_PAR NUMBER(15,2);
    v_IMP_ING_DIA_PAR NUMBER(15,2);
    v_IMP_RAL_AOS_PAR NUMBER(15,2);
    v_COD_MT_SWF      NUMBER(3);

    BEGIN

        v_NUM_REF_SWF := p_NUM_REF_SWF;

        BEGIN

            SELECT DET.IMP_OPE,
            OPC.COD_BCO_BFC,
            DET.NUM_CTA_BFC,
            DET.COD_DVI,
            DET.COD_MT_SWF,
            DET.COD_BCO_DTN
            INTO
            v_IMP_OPE,
            v_COD_BCO_BFC,
            v_NUM_CTA_BFC,
            v_COD_DVI,
            v_COD_MT_SWF,
            v_COD_BCO_DTN
            FROM PABS_DT_DETLL_OPRCN DET,
            PABS_DT_OPRCN_INFCN_OPCON OPC
            WHERE  DET.FEC_ISR_OPE = OPC.FEC_ISR_OPE
            AND DET.NUM_FOL_OPE = OPC.NUM_FOL_OPE
            AND DET.NUM_REF_SWF = v_NUM_REF_SWF;

        EXCEPTION
            WHEN NO_DATA_FOUND THEN

                        err_code := SQLCODE;
                        err_msg := 'No se han encontrado INFORMACION para referencia:  ' || v_NUM_REF_SWF ;
                        PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                        p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;

             WHEN OTHERS THEN

                    err_code := SQLCODE;
                    err_msg := SUBSTR(SQLERRM, 1, 300);
                    p_s_mensaje := err_code || '-' || err_msg;
                    PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                    p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                    RAISE_APPLICATION_ERROR(-20000,p_s_mensaje);

            END;

            -- BUSCAMOS EL VALOR QUE SE ENCUENTRA ALMACENADO EN LOS REGISTROS DE LA CUENTA PARA SER ALMACENADOS...


                BEGIN
                    SELECT
                    IMP_EGR_DIA,
                    IMP_ING_DIA,
                    IMP_RAL_AOS
                    INTO
                    v_IMP_EGR_DIA,
                    v_IMP_ING_DIA,
                    v_IMP_RAL_AOS
                    FROM PABS_DT_DETLL_SALDO_CAJA_MN_MX
                    WHERE COD_BCO_ORG = v_COD_BCO_BFC
                    --AND NUM_CTA_CTE = v_NUM_CTA_BFC
                    AND COD_DVI = v_COD_DVI;

                EXCEPTION
                WHEN NO_DATA_FOUND THEN

                            err_code := SQLCODE;
                            err_msg := 'No se han encontrado INFORMACION para banco:  ' || v_COD_BCO_BFC || ' - cuenta - : '|| v_NUM_CTA_BFC || '- divisa : ' || v_COD_DVI  ;
                            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                            p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;

                WHEN OTHERS THEN

                        err_code := SQLCODE;
                        err_msg := SUBSTR(SQLERRM, 1, 300);
                        p_s_mensaje := err_code || '-' || err_msg;
                        PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                        p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                        RAISE_APPLICATION_ERROR(-20000,p_s_mensaje);

                END;


               -- CALCULO NUEVOS SALDOS (EGRESO Y TOTAL)  PARA ACTUALIZAR EL MOVIMIENTO

                v_IMP_EGR_DIA_PAR := NVL(v_IMP_EGR_DIA,0) + NVL(v_IMP_OPE,0); -- NUEVO MONTO DE EGRESO DEL DIA

                v_IMP_RAL_AOS_PAR := NVL(v_IMP_RAL_AOS,0) - NVL(v_IMP_OPE,0); -- + NVL(v_IMP_ING_DIA,0); -- NUEVO MONTO TOTAL DEL DIA

               -- AHORA ACTUALIZAMOS EL MONTO EN LAS TABLAS DE SALDOS

                UPDATE PABS_DT_DETLL_SALDO_CAJA_MN_MX
                SET
                IMP_EGR_DIA = v_IMP_EGR_DIA_PAR,
                IMP_RAL_AOS = v_IMP_RAL_AOS_PAR
                WHERE COD_BCO_ORG = v_COD_BCO_BFC
                --AND NUM_CTA_CTE = v_NUM_CTA_BFC
                AND COD_DVI = v_COD_DVI;

                p_ERROR:= PKG_PAB_CONSTANTES.v_OK;

                 err_msg := 'actualizado:  ' || v_COD_BCO_BFC || 'divisa : ' || v_COD_DVI || ' - montos : '|| v_IMP_EGR_DIA_PAR || ' ' || v_IMP_RAL_AOS_PAR ;
                 PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( '0', v_NOM_PCK ,err_msg , v_NOM_SP);


                IF  v_COD_MT_SWF = PKG_PAB_CONSTANTES.V_COD_MT200 THEN

                     BEGIN
                        SELECT
                        IMP_EGR_DIA,
                        IMP_ING_DIA,
                        IMP_RAL_AOS
                        INTO
                        v_IMP_EGR_DIA,
                        v_IMP_ING_DIA,
                        v_IMP_RAL_AOS
                        FROM PABS_DT_DETLL_SALDO_CAJA_MN_MX
                        WHERE COD_BCO_ORG = v_COD_BCO_DTN
                        --AND NUM_CTA_CTE = v_NUM_CTA_BFC
                        AND COD_DVI = v_COD_DVI;

                    EXCEPTION
                    WHEN NO_DATA_FOUND THEN

                                err_code := SQLCODE;
                                err_msg := 'No se han encontrado INFORMACION para banco:  ' || v_COD_BCO_BFC || ' - cuenta - : '|| v_NUM_CTA_BFC || '- divisa : ' || v_COD_DVI  ;
                                PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                                p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;

                    WHEN OTHERS THEN

                            err_code := SQLCODE;
                            err_msg := SUBSTR(SQLERRM, 1, 300);
                            p_s_mensaje := err_code || '-' || err_msg;
                            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                            p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                            RAISE_APPLICATION_ERROR(-20000,p_s_mensaje);

                    END;


                   -- CALCULO NUEVOS SALDOS (EGRESO Y TOTAL)  PARA ACTUALIZAR EL MOVIMIENTO

                    v_IMP_ING_DIA_PAR := NVL(v_IMP_ING_DIA,0) + NVL(v_IMP_OPE,0); -- NUEVO MONTO DE EGRESO DEL DIA

                    v_IMP_RAL_AOS_PAR := NVL(v_IMP_RAL_AOS,0) + NVL(v_IMP_OPE,0); -- NUEVO MONTO TOTAL DEL DIA

                   -- AHORA ACTUALIZAMOS EL MONTO EN LAS TABLAS DE SALDOS

                    UPDATE PABS_DT_DETLL_SALDO_CAJA_MN_MX
                    SET
                    IMP_ING_DIA = v_IMP_ING_DIA_PAR,
                    IMP_RAL_AOS = v_IMP_RAL_AOS_PAR
                    WHERE COD_BCO_ORG = v_COD_BCO_DTN
                    --AND NUM_CTA_CTE = v_NUM_CTA_BFC
                    AND COD_DVI = v_COD_DVI;

                    p_ERROR:= PKG_PAB_CONSTANTES.v_OK;

                     err_msg := 'actualizado:  ' || v_COD_BCO_BFC || 'divisa : ' || v_COD_DVI || ' - montos : '|| v_IMP_EGR_DIA_PAR || ' ' || v_IMP_RAL_AOS_PAR ;
                     PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( '0', v_NOM_PCK ,err_msg , v_NOM_SP);


                END IF;

    EXCEPTION
     WHEN OTHERS THEN

                        err_code := SQLCODE;
                        err_msg := SUBSTR(SQLERRM, 1, 300);
                        p_s_mensaje := err_code || '-' || err_msg;
                        PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                        p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                        RAISE_APPLICATION_ERROR(-20000,p_s_mensaje);


    END  SP_ACT_SLD_EGR_MX;

/************************************************************************************************
-- Funcion/Procedimiento: SP_BUS_ENC_SALDOS_DIARIOS_MX
-- Objetivo: Procedimiento almacenado que Muestra encabezado de saldos Diarios MX
-- Sistema: DBO_PAB.
-- Base de Datos: DBO_PAB.
-- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX
--
-- Fecha: 14-11-2017
-- Autor: Santander
-- Input:
--          p_MONEDA  ->    Moneda a consultar.
-- Output:
--          p_CURSOR -> SYS_REFCURSOR, resultados de consulta.
--          p_ERROR  -> Indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
-- Input/Output: N/A
-- Retorno: N/A.
-- Observaciones: <Fecha y Detalle de ultimos cambios>
--***********************************************************************************************/
PROCEDURE SP_BUS_ENC_SALDOS_DIARIOS_MX  (p_MONEDA         IN CHAR,
                            p_CURSOR          OUT SYS_REFCURSOR,
                            p_ERROR           OUT NUMBER)IS

     v_NOM_SP     VARCHAR2(30) := 'SP_BUS_ENC_SALDOS_DIARIOS_MX';
     v_FECHA_ACTUAL  DATE := TRUNC(SYSDATE);


    BEGIN

         OPEN p_CURSOR FOR
         SELECT SUM(SL.IMP_APR_AOS) AS SALDO_INICIAL,
                SUM(SL.IMP_ING_DIA) AS INGRESO_CIERRE,
                SUM(SL.IMP_EGR_DIA) AS EGRESOS_CIERRE,
                SUM(SL.IMP_RAL_AOS) AS SALDO_CIERRE
                FROM PABS_DT_DETLL_SALDO_CAJA_MN_MX SL
                WHERE SL.COD_DVI = p_MONEDA;

          EXCEPTION

            WHEN NO_DATA_FOUND THEN

                    err_code := SQLCODE;
                    err_msg := 'No se han encontrado registos asociados a la busqueda por MONEDA ' || p_MONEDA;
                    PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                    p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;

            WHEN OTHERS THEN

                    err_code := SQLCODE;
                    err_msg := SUBSTR(SQLERRM, 1, 300);
                    p_s_mensaje := err_code || '-' || err_msg;
                    PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                    p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                    RAISE_APPLICATION_ERROR(-20000,p_s_mensaje);

END SP_BUS_ENC_SALDOS_DIARIOS_MX;



/************************************************************************************************
-- Funcion/Procedimiento: SP_ACT_SLD_ING_MX
-- Objetivo: ACTUALIZA INGRESOS PARA CAJA MX.
-- Sistema: DBO_PAB.
-- Base de Datos: DBO_PAB.
-- Tablas Usadas: DUAL
-- Fecha: 14-11-2017
-- Autor: Santander
-- Input:
-- p_NUM_REF_SWF        -> Número referencia
-- p_NUM_REF_EXT        -> Numero de referencia externa cuando es un MT900 / 910
-- p_COD_MT_SWF         -> Numero de mensaje asociado
-- Output:
-- p_ERROR                -> Indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
-- Input/Output: N/A
-- Retorno: N/A.
-- Observaciones: <Fecha y Detalle de ultimos cambios>
--***********************************************************************************************/

PROCEDURE SP_ACT_SLD_ING_MX ( p_NUM_REF_SWF     IN  VARCHAR2,
                              p_NUM_REF_EXT     IN  VARCHAR2,    -- SI ES UN MT0900 O MT 910 SE DEBE ENVIAR LA REFERENCIA RELACIONADA.
                              p_COD_MT_SWF      IN  CHAR,
                              p_ERROR           OUT NUMBER)IS

    v_NOM_SP        VARCHAR2(30) := 'SP_ACT_SLD_ING_MX';
    v_FEC_INS       DATE:= TRUNC(SYSDATE);
    ERROR_REFSWF    EXCEPTION;

    v_NUM_REF_SWF     CHAR(16);
    v_NUM_REF_EXT     CHAR(16);
    v_IMP_OPE         NUMBER(15,2);
    v_COD_BCO_ORG     CHAR(11);
    v_COD_BCO_DTN     CHAR(11);
    v_COD_BCO_ITD     CHAR(11);
    v_NUM_CTA_CTE     VARCHAR2(30);
    v_COD_DVI         CHAR(3);
    v_IMP_APR_AOS     NUMBER(15,2);
    v_IMP_EGR_DIA     NUMBER(15,2);
    v_IMP_ING_DIA     NUMBER(15,2);
    v_IMP_RAL_AOS     NUMBER(15,2);
    v_IMP_EGR_DIA_PAR NUMBER(15,2);
    v_IMP_ING_DIA_PAR NUMBER(15,2);
    v_IMP_RAL_AOS_PAR NUMBER(15,2);
    v_COD_MT_SWF      NUMBER(3);
    v_FLG_CGO_ABN     NUMBER(1);
    v_FEC_VTA         DATE;

    BEGIN

        v_NUM_REF_SWF := p_NUM_REF_SWF;
        v_NUM_REF_EXT := p_NUM_REF_EXT;
        v_COD_MT_SWF  := p_COD_MT_SWF;
        v_FEC_VTA := TRUNC(SYSDATE);

        BEGIN

         /* SE DEBE VALIDAR QUE EL MENSAJE RECIBIDO ES UNA MENSAJE YA REGISTRADO EN TABLAS
            SI ES MT 103/202/200, COMPARAMOS CON NUM_REF_SWF
            SI ES MT 900/910 COMPARAMOS CON NUM_REF_EXT (INDICA SI ES UN MT ASOCIADO
          */


                IF v_COD_MT_SWF = PKG_PAB_CONSTANTES.V_COD_MT910 THEN-- SI EL MENSAJE ES 910,

                    BEGIN

                        -- SE AGREGA VALIDACION A QUE SEA POR FECHA Y TIPO
                        BEGIN

                            SELECT MAX(FEC_VTA) INTO v_FEC_VTA
                            FROM PABS_DT_DETLL_CARGO_ABONO_CAJA
                            WHERE   NUM_REF_SWF = v_NUM_REF_SWF
                     --     AND NUM_REF_EXT = v_NUM_REF_EXT
                            AND COD_MT_SWF = v_COD_MT_SWF;

                        EXCEPTION
                        WHEN NO_DATA_FOUND THEN

                            err_msg := 'NO EXISTE REGISTRO PARA ' || v_NUM_REF_EXT  || ' Y TIPO MT ' || v_COD_MT_SWF  ;
                            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( '0', v_NOM_PCK ,err_msg , v_NOM_SP);

                        WHEN OTHERS THEN

                            err_code := SQLCODE;
                            err_msg := 'MENSAJE REFERENCIA'|| v_NUM_REF_SWF ||' y TIPO MT ' || v_COD_DVI || ' PRESENTA ERRORES...,';
                            p_s_mensaje := err_code || '-' || err_msg;
                            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                            p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                            RAISE_APPLICATION_ERROR(-20000,p_s_mensaje);

                        END;

                        -- BUSCO EL REGISTRO ASOCIADO EN LA TABLA

                        SELECT
                        IMP_OPE
                        INTO v_IMP_OPE
                        FROM PABS_DT_DETLL_OPRCN
                        WHERE NUM_REF_SWF = v_NUM_REF_EXT
                        AND FEC_VTA =  v_FEC_VTA
                        AND ROWNUM = 1 AND IMP_OPE IS NOT NULL;

                        err_msg := 'SE ENCUENTRA REGISTRO ASOCIADO A  ' || v_NUM_REF_EXT  || '. NO SE CONSIDERA PARA ACTUALIZAR' ;
                        PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( '0', v_NOM_PCK ,err_msg , v_NOM_SP);


                    EXCEPTION
                    WHEN NO_DATA_FOUND THEN

                            BEGIN

                            SELECT IMP_OPE
                            INTO v_IMP_OPE
                            FROM PABS_DT_DETLL_CARGO_ABONO_CAJA
                            WHERE  NUM_REF_SWF = v_NUM_REF_SWF
                           -- AND NUM_REF_EXT = v_NUM_REF_EXT
                            AND COD_MT_SWF = v_COD_MT_SWF
                            AND ROWNUM = 1 AND IMP_OPE IS NOT NULL
                            AND FEC_VTA =  v_FEC_VTA;

                            EXCEPTION

                                WHEN NO_DATA_FOUND THEN
                                    -- SI NO SE ENCUENTRA, ES UN VALOR QUE NO ESTA REFERENCIADO, POR LO QUE DEBE SUMAR. A LOS SALDOS.

                            BEGIN
                                        SELECT IMP_OPE,
                                        COD_BCO_ORG,
                                        COD_BCO_DTN,
                                        COD_BCO_ITD,
                                       -- NUM_CTA_CTE,
                                        COD_DVI,
                                        FLG_CGO_ABN
                                        INTO
                                        v_IMP_OPE,
                                        v_COD_BCO_ORG,
                                        v_COD_BCO_DTN,
                                        v_COD_BCO_ITD,
                                     --  v_NUM_CTA_CTE,
                                        v_COD_DVI,
                                        v_FLG_CGO_ABN
                                        FROM PABS_DT_DETLL_CARGO_ABONO_CAJA
                                        WHERE  NUM_REF_SWF = v_NUM_REF_SWF
                                        AND NUM_REF_EXT = v_NUM_REF_EXT
                                        AND FEC_VTA =  v_FEC_VTA;

                                IF v_COD_DVI <>  'CLP' THEN
                                            err_msg := 'SE ENCUENTRA REGISTRO ASOCIADO A  ' || p_NUM_REF_EXT  || ' Y ' || v_NUM_REF_SWF ;
                                            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( '0', v_NOM_PCK ,err_msg , v_NOM_SP);




                                            SELECT IMP_APR_AOS,
                                                    IMP_EGR_DIA,
                                                    IMP_ING_DIA,
                                                    IMP_RAL_AOS
                                            INTO
                                                   v_IMP_APR_AOS,
                                                   v_IMP_EGR_DIA,
                                                   v_IMP_ING_DIA,
                                                   v_IMP_RAL_AOS
                                            FROM PABS_DT_DETLL_SALDO_CAJA_MN_MX
                                            WHERE SUBSTR(COD_BCO_ORG,0,8) = SUBSTR(v_COD_BCO_ORG,0,8)
                                            --AND NUM_CTA_CTE = v_NUM_CTA_CTE
                                            AND COD_DVI = v_COD_DVI;


                                            err_msg := 'VALORES OBTENIDOS ' || v_IMP_APR_AOS  || ' - ' || v_IMP_EGR_DIA || ' - ' || v_IMP_ING_DIA ||' - ' || v_IMP_RAL_AOS;
                                            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( '0', v_NOM_PCK ,err_msg , v_NOM_SP);


                                             -- CALCULO NUEVOS SALDOS (EGRESO Y TOTAL)  PARA ACTUALIZAR EL MOVIMIENTO

                                            v_IMP_ING_DIA_PAR := NVL(v_IMP_ING_DIA,0) + NVL(v_IMP_OPE,0); -- NUEVO MONTO DE EGRESO DEL DIA

                                            v_IMP_RAL_AOS_PAR := NVL(v_IMP_APR_AOS,0) + NVL(v_IMP_OPE,0); -- NUEVO MONTO TOTAL DEL DIA

                                           -- AHORA ACTUALIZAMOS EL MONTO EN LAS TABLAS DE SALIDA

                                            err_msg := 'BANCO ' || v_COD_BCO_ORG  || ' - ' || v_COD_DVI;
                                            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( '0', v_NOM_PCK ,err_msg , v_NOM_SP);

                                            UPDATE PABS_DT_DETLL_SALDO_CAJA_MN_MX
                                            SET
                                            IMP_ING_DIA = v_IMP_ING_DIA_PAR,
                                            IMP_RAL_AOS = v_IMP_RAL_AOS_PAR
                                            WHERE SUBSTR(COD_BCO_ORG,0,8) = SUBSTR(v_COD_BCO_ORG,0,8)
                                            --AND NUM_CTA_CTE = v_NUM_CTA_CTE
                                            AND COD_DVI = v_COD_DVI;
                                    ELSE

                                    err_msg := 'REGISTRO ES MN. ' || p_NUM_REF_EXT  || ' Y ' || v_NUM_REF_SWF || '. NO SE ACTUALIZARÁ' ;
                                            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( '0', v_NOM_PCK ,err_msg , v_NOM_SP);

                                END IF;



                            EXCEPTION
                                     WHEN NO_DATA_FOUND THEN
                                                err_code := SQLCODE;
                                                err_msg := 'Registro con referencia '|| v_NUM_REF_SWF ||' y referncia externa '|| v_NUM_REF_EXT ||' no se encuentra,';
                                                p_s_mensaje := err_code || '-' || err_msg;
                                                PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                                                p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                                      WHEN OTHERS THEN
                                                err_code := SQLCODE;
                                                err_msg := SUBSTR (SQLERRM, 1, 300);
                                                p_s_mensaje := err_code || '-' || err_msg;
                                                --PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                                                p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                                                RAISE_APPLICATION_ERROR(-20000,p_s_mensaje);

                                END;

                                WHEN OTHERS THEN
                                        err_code := SQLCODE;
                                        err_msg := SUBSTR(SQLERRM, 1, 300);
                                        p_s_mensaje := err_code || '-' || err_msg;
                                        PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                                        p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                                        RAISE_APPLICATION_ERROR(-20000,p_s_mensaje);
                        END;
                    END;
                END IF;

                 IF v_COD_MT_SWF = PKG_PAB_CONSTANTES.V_COD_MT900 THEN-- SI EL MENSAJE ES 900, SE REALIZO UN CARGO EN LA CUENTA
                                                                 -- VALIDAMOS SI EL MENSAJE LLEGO O TIENE UNA ASOCIACION.

                    BEGIN

                        BEGIN

                            SELECT MAX(FEC_VTA) INTO v_FEC_VTA
                            FROM PABS_DT_DETLL_CARGO_ABONO_CAJA
                            WHERE   NUM_REF_SWF = v_NUM_REF_SWF
                            AND NUM_REF_EXT = v_NUM_REF_EXT
                            AND COD_MT_SWF = v_COD_MT_SWF;

                        EXCEPTION
                        WHEN NO_DATA_FOUND THEN

                            err_msg := 'NO EXISTE REGISTRO PARA ' || v_NUM_REF_EXT  || ' Y TIPO MT ' || v_COD_MT_SWF  ;
                            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( '0', v_NOM_PCK ,err_msg , v_NOM_SP);

                        WHEN OTHERS THEN

                            err_code := SQLCODE;
                            err_msg := 'MENSAJE REFERENCIA'|| v_NUM_REF_SWF ||' y TIPO MT ' || v_COD_DVI || ' PRESENTA ERRORES...,';
                            p_s_mensaje := err_code || '-' || err_msg;
                            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                            p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                            RAISE_APPLICATION_ERROR(-20000,p_s_mensaje);

                        END;

                        SELECT
                        IMP_OPE
                        INTO v_IMP_OPE
                        FROM PABS_DT_DETLL_OPRCN
                        WHERE NUM_REF_SWF = v_NUM_REF_EXT
                        AND FEC_VTA =  v_FEC_VTA
                        AND ROWNUM = 1 AND IMP_OPE IS NOT NULL;

                        err_msg := 'SE ENCUENTRA REGISTRO ASOCIADO A  ' || v_NUM_REF_EXT  || '. NO SE CONSIDERA PARA ACTUALIZAR' ;
                        PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( '0', v_NOM_PCK ,err_msg , v_NOM_SP);

                    EXCEPTION
                    WHEN NO_DATA_FOUND THEN

                        -- SE AGREGA VALIDACION QUE NO EXISTA DENTRO DE LA TABLA DE PAGOS ENVIADOS

                            BEGIN

                            SELECT IMP_OPE
                            INTO v_IMP_OPE
                            FROM PABS_DT_DETLL_CARGO_ABONO_CAJA
                            WHERE  NUM_REF_SWF = v_NUM_REF_SWF
                            AND NUM_REF_EXT = v_NUM_REF_EXT
                            AND COD_MT_SWF = v_COD_MT_SWF
                            AND FEC_VTA =  v_FEC_VTA;

                            EXCEPTION
                                WHEN NO_DATA_FOUND THEN

                                    -- SI NO SE ENCUENTRA EN NINGUNA TABLA, , ES UN VALOR QUE NO ESTA REFERENCIADO, POR LO QUE DEBE SUMAR. A LOS SALDOS.
                                BEGIN
                                            SELECT IMP_OPE,
                                            COD_BCO_ORG,
                                            COD_BCO_DTN,
                                         --   NUM_CTA_CTE,
                                            COD_DVI,
                                            FLG_CGO_ABN
                                            INTO
                                            v_IMP_OPE,
                                            v_COD_BCO_ORG,
                                            v_COD_BCO_DTN,
                                          --  v_NUM_CTA_CTE,
                                            v_COD_DVI,
                                            v_FLG_CGO_ABN
                                            FROM PABS_DT_DETLL_CARGO_ABONO_CAJA
                                            WHERE  NUM_REF_SWF = v_NUM_REF_SWF
                                            AND NUM_REF_EXT = v_NUM_REF_EXT
                                            AND FEC_VTA =  v_FEC_VTA;

                                         IF v_COD_DVI <>  'CLP' THEN
                                                err_msg := 'SE ENCUENTRA REGISTRO ASOCIADO A  ' || p_NUM_REF_EXT  || ' Y ' || v_NUM_REF_SWF ;
                                                PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( '0', v_NOM_PCK ,err_msg , v_NOM_SP);


                                                SELECT IMP_APR_AOS,
                                                        IMP_EGR_DIA,
                                                        IMP_ING_DIA,
                                                        IMP_RAL_AOS
                                                INTO
                                                       v_IMP_APR_AOS,
                                                       v_IMP_EGR_DIA,
                                                       v_IMP_ING_DIA,
                                                       v_IMP_RAL_AOS
                                                FROM PABS_DT_DETLL_SALDO_CAJA_MN_MX
                                                WHERE COD_BCO_ORG LIKE TRIM(v_COD_BCO_ORG) || '%'
                                                --AND NUM_CTA_CTE = trim(v_NUM_CTA_CTE)
                                                AND COD_DVI = v_COD_DVI;

                                                err_msg := 'OBTENGO SALDOS ACTUALES  ' || v_IMP_APR_AOS  || ' - ' || v_IMP_EGR_DIA || ' - ' || v_IMP_ING_DIA || ' - ' || v_IMP_RAL_AOS ;
                                                PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( '0', v_NOM_PCK ,err_msg , v_NOM_SP);

                                                 -- CALCULO NUEVOS SALDOS (EGRESO Y TOTAL)  PARA ACTUALIZAR EL MOVIMIENTO

                                                v_IMP_EGR_DIA_PAR := NVL(v_IMP_EGR_DIA,0) + NVL(v_IMP_OPE,0); -- NUEVO MONTO DE EGRESO DEL DIA

                                                v_IMP_RAL_AOS_PAR := NVL(v_IMP_APR_AOS,0) - NVL(v_IMP_OPE,0); -- NUEVO MONTO TOTAL DEL DIA

                                               -- AHORA ACTUALIZAMOS EL MONTO EN LAS TABLAS DE SALIDA

                                                UPDATE PABS_DT_DETLL_SALDO_CAJA_MN_MX
                                                SET
                                                IMP_EGR_DIA = v_IMP_EGR_DIA_PAR,
                                                IMP_RAL_AOS = v_IMP_RAL_AOS_PAR
                                                WHERE COD_BCO_ORG LIKE TRIM(v_COD_BCO_ORG) || '%'
                                                --AND NUM_CTA_CTE = trim(v_NUM_CTA_CTE)
                                                AND COD_DVI = v_COD_DVI;
                                    ELSE

                                     err_msg := 'REGISTRO ES MN. ' || p_NUM_REF_EXT  || ' Y ' || v_NUM_REF_SWF || '. NO SE ACTUALIZARÁ' ;
                                                PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( '0', v_NOM_PCK ,err_msg , v_NOM_SP);

                                    END IF;

                              EXCEPTION
                                     WHEN NO_DATA_FOUND THEN
                                                err_code := SQLCODE;
                                                err_msg := 'Registro con referencia '|| v_NUM_REF_SWF ||' y referncia externa '|| v_NUM_REF_EXT ||' no se encuentra,';
                                                p_s_mensaje := err_code || '-' || err_msg;
                                                PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                                                p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                                      WHEN OTHERS THEN
                                                err_code := SQLCODE;
                                                err_msg := 'Registro con referencia '|| v_NUM_REF_SWF ||' y referncia externa '|| v_NUM_REF_EXT ||' ya se encuentra registrado';
                                                p_s_mensaje := err_code || '-' || err_msg;
                                                --PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                                                p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                                                RAISE_APPLICATION_ERROR(-20000,p_s_mensaje);


                              END;

                                WHEN OTHERS THEN
                                        err_code := SQLCODE;
                                        err_msg := SUBSTR(SQLERRM, 1, 300);
                                        p_s_mensaje := err_code || '-' || err_msg;
                                        --PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                                        p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                                        RAISE_APPLICATION_ERROR(-20000,p_s_mensaje);
                        END;
                    END;
                END IF;

                IF v_COD_MT_SWF = PKG_PAB_CONSTANTES.V_COD_MT103 THEN -- SI ES 103

                    BEGIN

                        -- VALIDO QUE EL MENSAJE NO EXISTA COMO REFERENCIA DE UN 910 O 900.
                        SELECT IMP_OPE
                        INTO v_IMP_OPE
                        FROM PABS_DT_DETLL_CARGO_ABONO_CAJA
                        WHERE NUM_REF_EXT = v_NUM_REF_SWF
                        AND FEC_VTA =  v_FEC_VTA
                        AND COD_MT_SWF IN (900,910)
                        AND ROWNUM = 1 AND IMP_OPE IS NOT NULL;

                        err_msg := 'ENCUENTRA REGISTRO' || p_NUM_REF_SWF;
                        PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( '0', v_NOM_PCK ,err_msg , v_NOM_SP);


                    EXCEPTION
                    WHEN NO_DATA_FOUND THEN -- O CARGO, 1 ABONO

                        -- SI NO SE ENCUENTRA, ES UN VALOR QUE NO ESTA REFERENCIADO, POR LO QUE DEBE SUMAR. A LOS SALDOS.
                        err_msg := 'MENSAJE NO ES REFERENCIA. SE BUSCARÁ DATOS PARA ACTUALIZAR' || p_NUM_REF_SWF;
                        PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( '0', v_NOM_PCK ,err_msg , v_NOM_SP);

                            BEGIN
                                        SELECT IMP_OPE,
                                        COD_BCO_ORG,
                                        COD_BCO_DTN,
                                       -- NUM_CTA_CTE,
                                        COD_DVI,
                                        FLG_CGO_ABN
                                        INTO
                                        v_IMP_OPE,
                                        v_COD_BCO_ORG,
                                        v_COD_BCO_DTN,
                                      --  v_NUM_CTA_CTE,
                                        v_COD_DVI,
                                        v_FLG_CGO_ABN
                                        FROM PABS_DT_DETLL_CARGO_ABONO_CAJA
                                        WHERE  NUM_REF_SWF = v_NUM_REF_SWF
                                        AND FEC_VTA =  v_FEC_VTA
                                        AND ROWNUM = 1
                                       -- AND COD_MT_SWF = '103'
                                        AND COD_MT_SWF = v_COD_MT_SWF;

                           EXCEPTION
                           WHEN NO_DATA_FOUND THEN
                                err_code := SQLCODE;
                                err_msg := 'Registro con referencia '|| v_NUM_REF_SWF ||' no se encuentra,';
                                p_s_mensaje := err_code || '-' || err_msg;
                                PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                                p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                           WHEN OTHERS THEN
                                err_code := SQLCODE;
                                err_msg := 'Registro con referencia '|| v_NUM_REF_SWF ||' ya se encuentra registrado';
                                p_s_mensaje := err_code || '-' || err_msg;
                                PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                                p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                                RAISE_APPLICATION_ERROR(-20000,p_s_mensaje);
                           END;

                           IF v_COD_DVI <> 'CLP' THEN

                              IF v_COD_BCO_ORG = PKG_PAB_CONSTANTES.V_BIC_BANCO_SANTANDER OR v_COD_BCO_ORG = PKG_PAB_CONSTANTES.V_BIC_BANCO_SANTANDER_HOM THEN-- v_FLG_CGO_ABN = 1 THEN -- SUMO PARA EGRESOS

                                 BEGIN
                                    SELECT IMP_APR_AOS,
                                            IMP_EGR_DIA,
                                            IMP_ING_DIA,
                                            IMP_RAL_AOS
                                    INTO
                                           v_IMP_APR_AOS,
                                           v_IMP_EGR_DIA,
                                           v_IMP_ING_DIA,
                                           v_IMP_RAL_AOS
                                    FROM PABS_DT_DETLL_SALDO_CAJA_MN_MX
                                    WHERE SUBSTR(COD_BCO_ORG,0,8) =SUBSTR(v_COD_BCO_DTN,0,8)
                                    AND COD_DVI = v_COD_DVI;

                                 EXCEPTION
                                   WHEN NO_DATA_FOUND THEN
                                        err_code := SQLCODE;
                                        err_msg := 'Registro BANCO EGRESO '|| v_COD_BCO_DTN ||' y DIVISA ' || v_COD_DVI || ' no se encuentra';
                                        p_s_mensaje := err_code || '-' || err_msg;
                                        PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                                        p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                                   WHEN OTHERS THEN
                                        err_code := SQLCODE;
                                        err_msg := 'Registro BANCO EGRESO '|| v_COD_BCO_DTN ||' y DIVISA ' || v_COD_DVI || ' presenta errores...,';
                                        p_s_mensaje := err_code || '-' || err_msg;
                                        PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                                        p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                                        RAISE_APPLICATION_ERROR(-20000,p_s_mensaje);
                                 END;

                                    -- CALCULO NUEVOS SALDOS (EGRESO Y TOTAL)  PARA ACTUALIZAR EL MOVIMIENTO

                                    v_IMP_EGR_DIA_PAR := NVL(v_IMP_EGR_DIA,0) + NVL(v_IMP_OPE,0); -- NUEVO MONTO DE EGRESO DEL DIA

                                    v_IMP_RAL_AOS_PAR := NVL(v_IMP_APR_AOS,0) - NVL(v_IMP_OPE,0); -- NUEVO MONTO TOTAL DEL DIA

                                    -- AHORA ACTUALIZAMOS EL MONTO EN LAS TABLAS DE SALIDA

                                    UPDATE PABS_DT_DETLL_SALDO_CAJA_MN_MX
                                    SET
                                    IMP_EGR_DIA = v_IMP_EGR_DIA_PAR,
                                    IMP_RAL_AOS = v_IMP_RAL_AOS_PAR
                                    WHERE SUBSTR(COD_BCO_ORG,0,8) = SUBSTR(v_COD_BCO_DTN,0,8)
                                    --AND NUM_CTA_CTE = v_NUM_CTA_CTE
                                    AND COD_DVI = v_COD_DVI;

                                     err_msg := 'ENCUENTRA REGISTRO' || v_IMP_EGR_DIA_PAR || ' ' || v_IMP_RAL_AOS_PAR || ' ' || v_COD_BCO_DTN;
                                    PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( '0', v_NOM_PCK ,err_msg , v_NOM_SP);

                              ELSE -- ES INGRESO

                                 BEGIN
                                        SELECT IMP_APR_AOS,
                                                IMP_EGR_DIA,
                                                IMP_ING_DIA,
                                                IMP_RAL_AOS
                                        INTO
                                                v_IMP_APR_AOS,
                                                v_IMP_EGR_DIA,
                                                v_IMP_ING_DIA,
                                                v_IMP_RAL_AOS
                                        FROM PABS_DT_DETLL_SALDO_CAJA_MN_MX
                                        WHERE SUBSTR(COD_BCO_ORG,0,8) = SUBSTR(v_COD_BCO_ORG,0,8)
                                        AND COD_DVI = v_COD_DVI;


                                 EXCEPTION
                                 WHEN NO_DATA_FOUND THEN
                                        err_code := SQLCODE;
                                        err_msg := 'Registro BANCO INGRESO '|| v_COD_BCO_ORG ||' y DIVISA ' || v_COD_DVI || ' no se encuentra';
                                        p_s_mensaje := err_code || '-' || err_msg;
                                        PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                                        p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                                 WHEN OTHERS THEN
                                        err_code := SQLCODE;
                                        err_msg := 'Registro BANCO INGRESO'|| v_COD_BCO_ORG ||' y DIVISA ' || v_COD_DVI || ' presenta errores...,';
                                        p_s_mensaje := err_code || '-' || err_msg;
                                        PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                                        p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                                        RAISE_APPLICATION_ERROR(-20000,p_s_mensaje);

                                 END;
                                          -- CALCULO NUEVOS SALDOS (EGRESO Y TOTAL)  PARA ACTUALIZAR EL MOVIMIENTO

                                        v_IMP_ING_DIA_PAR := NVL(v_IMP_ING_DIA,0) + NVL(v_IMP_OPE,0); -- NUEVO MONTO DE EGRESO DEL DIA

                                        --v_IMP_RAL_AOS_PAR := NVL(v_IMP_APR_AOS,0) - NVL(v_IMP_EGR_DIA,0) + NVL(v_IMP_OPE,0); -- NUEVO MONTO TOTAL DEL DIA
                                        v_IMP_RAL_AOS_PAR := NVL(v_IMP_RAL_AOS,0)  + NVL(v_IMP_OPE,0);
                                       -- AHORA ACTUALIZAMOS EL MONTO EN LAS TABLAS DE SALIDA

                                        UPDATE PABS_DT_DETLL_SALDO_CAJA_MN_MX
                                        SET
                                        IMP_ING_DIA = v_IMP_ING_DIA_PAR,
                                        IMP_RAL_AOS = v_IMP_RAL_AOS_PAR
                                        WHERE SUBSTR(COD_BCO_ORG,0,8) = SUBSTR(v_COD_BCO_ORG,0,8)
                                        --AND NUM_CTA_CTE = v_NUM_CTA_CTE
                                        AND COD_DVI = v_COD_DVI;

                                       err_msg := 'ENCUENTRA REGISTRO' || v_IMP_ING_DIA_PAR || ' ' || v_IMP_RAL_AOS_PAR || ' ' || v_COD_BCO_ORG;
                                       PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( '0', v_NOM_PCK ,err_msg , v_NOM_SP);


                              END IF;

                         END IF;

                            WHEN OTHERS THEN
                                    err_code := SQLCODE;
                                    err_msg := SUBSTR(SQLERRM, 1, 300);
                                    p_s_mensaje := err_code || '-' || err_msg;
                                    PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                                    p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                                    RAISE_APPLICATION_ERROR(-20000,p_s_mensaje);

                    END;
                END IF;

                 IF v_COD_MT_SWF = PKG_PAB_CONSTANTES.V_COD_MT202 THEN-- SI EL MENSAJE ES 900, SE REALIZO UN CARGO EN LA CUENTA
                                                                 -- VALIDAMOS SI EL MENSAJE LLEGO O TIENE UNA ASOCIACION.

                    BEGIN

                        -- VALIDO QUE EL MENSAJE NO EXISTA COMO REFERENCIA DE UN 910 O 900.
                        SELECT IMP_OPE
                        INTO v_IMP_OPE
                        FROM PABS_DT_DETLL_CARGO_ABONO_CAJA
                        WHERE NUM_REF_EXT = p_NUM_REF_SWF
                        AND ROWNUM = 1 AND IMP_OPE IS NOT NULL;



                    EXCEPTION
                    WHEN NO_DATA_FOUND THEN -- O CARGO, 1 ABONO

                        -- SI NO SE ENCUENTRA, ES UN VALOR QUE NO ESTA REFERENCIADO, POR LO QUE DEBE SUMAR. A LOS SALDOS.

                    BEGIN
                                SELECT IMP_OPE,
                                       COD_BCO_ORG,
                                       COD_BCO_DTN,
                                     --  NUM_CTA_CTE,
                                       COD_DVI,
                                       FLG_CGO_ABN
                                       INTO
                                       v_IMP_OPE,
                                       v_COD_BCO_ORG,
                                    --   v_COD_BCO_DTN,
                                       v_NUM_CTA_CTE,
                                       v_COD_DVI,
                                       v_FLG_CGO_ABN
                                FROM PABS_DT_DETLL_CARGO_ABONO_CAJA
                                WHERE
                                ROWNUM = 1 AND
                                NUM_REF_SWF = v_NUM_REF_SWF;
                                --AND NUM_REF_EXT = v_NUM_REF_EXT;

                    EXCEPTION
                           WHEN NO_DATA_FOUND THEN
                                err_code := SQLCODE;
                                err_msg := 'Registro con referencia '|| v_NUM_REF_SWF ||' no se encuentra,';
                                p_s_mensaje := err_code || '-' || err_msg;
                                PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                                p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                           WHEN OTHERS THEN
                                err_code := SQLCODE;
                                err_msg := 'Registro con referencia '|| v_NUM_REF_SWF ||' ya se encuentra registrado';
                                p_s_mensaje := err_code || '-' || err_msg;
                                PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                                p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                                RAISE_APPLICATION_ERROR(-20000,p_s_mensaje);
                    END;

                          IF  v_COD_DVI <> 'CLP' THEN

                                 IF v_COD_BCO_ORG = PKG_PAB_CONSTANTES.V_BIC_BANCO_SANTANDER OR v_COD_BCO_ORG = PKG_PAB_CONSTANTES.V_BIC_BANCO_SANTANDER_HOM THEN-- v_FLG_CGO_ABN = 1 THEN -- SUMO PARA EGRESOS

                                            BEGIN
                                                SELECT IMP_APR_AOS,
                                                        IMP_EGR_DIA,
                                                        IMP_ING_DIA,
                                                        IMP_RAL_AOS
                                                INTO
                                                       v_IMP_APR_AOS,
                                                       v_IMP_EGR_DIA,
                                                       v_IMP_ING_DIA,
                                                       v_IMP_RAL_AOS
                                                FROM PABS_DT_DETLL_SALDO_CAJA_MN_MX
                                                WHERE SUBSTR(COD_BCO_ORG,0,8) = SUBSTR(v_COD_BCO_DTN,0,8)
                                                --AND NUM_CTA_CTE = v_NUM_CTA_CTE
                                                AND COD_DVI = v_COD_DVI;

                                             EXCEPTION
                                               WHEN NO_DATA_FOUND THEN
                                                    err_code := SQLCODE;
                                                    err_msg := 'Registro BANCO '|| v_COD_BCO_DTN ||' no se encuentra,';
                                                    p_s_mensaje := err_code || '-' || err_msg;
                                                    PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                                                    p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                                               WHEN OTHERS THEN
                                                    err_code := SQLCODE;
                                                    err_msg := 'Registro BANCO '|| v_COD_BCO_DTN ||' ya se encuentra registrado';
                                                    p_s_mensaje := err_code || '-' || err_msg;
                                                    PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                                                    p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                                                    RAISE_APPLICATION_ERROR(-20000,p_s_mensaje);
                                            END;
                                            -- CALCULO NUEVOS SALDOS (EGRESO Y TOTAL)  PARA ACTUALIZAR EL MOVIMIENTO

                                            v_IMP_EGR_DIA_PAR := NVL(v_IMP_EGR_DIA,0) + NVL(v_IMP_OPE,0); -- NUEVO MONTO DE EGRESO DEL DIA

                                            v_IMP_RAL_AOS_PAR := NVL(v_IMP_APR_AOS,0) - NVL(v_IMP_OPE,0); -- NUEVO MONTO TOTAL DEL DIA

                                            -- AHORA ACTUALIZAMOS EL MONTO EN LAS TABLAS DE SALIDA

                                            UPDATE PABS_DT_DETLL_SALDO_CAJA_MN_MX
                                            SET
                                            IMP_EGR_DIA = v_IMP_EGR_DIA_PAR,
                                            IMP_RAL_AOS = v_IMP_RAL_AOS_PAR
                                            WHERE SUBSTR(COD_BCO_ORG,0,8) = SUBSTR(v_COD_BCO_DTN,0,8)
                                            AND COD_DVI = v_COD_DVI;

                                             err_msg := 'ENCUENTRA REGISTRO' || v_IMP_EGR_DIA_PAR || ' ' || v_IMP_RAL_AOS_PAR || ' ' || v_COD_BCO_DTN;
                                            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( '0', v_NOM_PCK ,err_msg , v_NOM_SP);

                                            ELSE -- ES INGRESO


                                            BEGIN
                                                SELECT IMP_APR_AOS,
                                                        IMP_EGR_DIA,
                                                        IMP_ING_DIA,
                                                        IMP_RAL_AOS
                                                INTO
                                                        v_IMP_APR_AOS,
                                                        v_IMP_EGR_DIA,
                                                        v_IMP_ING_DIA,
                                                        v_IMP_RAL_AOS
                                                FROM PABS_DT_DETLL_SALDO_CAJA_MN_MX
                                                WHERE SUBSTR(COD_BCO_ORG,0,8) = SUBSTR(v_COD_BCO_ORG,0,8)
                                                --AND NUM_CTA_CTE = v_NUM_CTA_CTE
                                                AND COD_DVI = v_COD_DVI;
                                             EXCEPTION
                                               WHEN NO_DATA_FOUND THEN
                                                    err_code := SQLCODE;
                                                    err_msg := 'Registro BANCO  '|| v_COD_BCO_DTN ||' no se encuentra,';
                                                    p_s_mensaje := err_code || '-' || err_msg;
                                                    PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                                                    p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                                               WHEN OTHERS THEN
                                                    err_code := SQLCODE;
                                                    err_msg := 'Registro BANCO '|| v_COD_BCO_DTN ||' ya se encuentra registrado';
                                                    p_s_mensaje := err_code || '-' || err_msg;
                                                    PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                                                    p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                                                    RAISE_APPLICATION_ERROR(-20000,p_s_mensaje);
                                            END;
                                                  -- CALCULO NUEVOS SALDOS (EGRESO Y TOTAL)  PARA ACTUALIZAR EL MOVIMIENTO

                                                v_IMP_ING_DIA_PAR := NVL(v_IMP_ING_DIA,0) + NVL(v_IMP_OPE,0); -- NUEVO MONTO DE EGRESO DEL DIA

                                                v_IMP_RAL_AOS_PAR := NVL(v_IMP_APR_AOS,0) + NVL(v_IMP_OPE,0); -- NUEVO MONTO TOTAL DEL DIA

                                               -- AHORA ACTUALIZAMOS EL MONTO EN LAS TABLAS DE SALIDA

                                                UPDATE PABS_DT_DETLL_SALDO_CAJA_MN_MX
                                                SET
                                                IMP_ING_DIA = v_IMP_ING_DIA_PAR,
                                                IMP_RAL_AOS = v_IMP_RAL_AOS_PAR
                                                WHERE SUBSTR(COD_BCO_ORG,0,8) = SUBSTR(v_COD_BCO_ORG,0,8)
                                                --AND NUM_CTA_CTE = v_NUM_CTA_CTE
                                                AND COD_DVI = v_COD_DVI;


                                            END IF;


                    END IF;


                    WHEN OTHERS THEN
                            err_code := SQLCODE;
                            err_msg := SUBSTR(SQLERRM, 1, 300);
                            p_s_mensaje := err_code || '-' || err_msg;
                            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                            p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                            RAISE_APPLICATION_ERROR(-20000,p_s_mensaje);

                    END;
                END IF;

                   IF v_COD_MT_SWF = PKG_PAB_CONSTANTES.V_COD_MT200 THEN-- SI EL MENSAJE ES 900, SE REALIZO UN CARGO EN LA CUENTA
                                                                 -- VALIDAMOS SI EL MENSAJE LLEGO O TIENE UNA ASOCIACION.

                    BEGIN

                        -- VALIDO QUE EL MENSAJE NO EXISTA COMO REFERENCIA DE UN 910 O 900.
                        SELECT IMP_OPE
                        INTO v_IMP_OPE
                        FROM PABS_DT_DETLL_CARGO_ABONO_CAJA
                        WHERE NUM_REF_EXT = p_NUM_REF_SWF;



                    EXCEPTION
                    WHEN NO_DATA_FOUND THEN -- O CARGO, 1 ABONO

                        -- SI NO SE ENCUENTRA, ES UN VALOR QUE NO ESTA REFERENCIADO, POR LO QUE DEBE SUMAR. A LOS SALDOS.
                            SELECT IMP_OPE,
                            COD_BCO_ORG,
                            NUM_CTA_CTE,
                            COD_DVI,
                            FLG_CGO_ABN
                            INTO
                            v_IMP_OPE,
                            v_COD_BCO_ORG,
                            v_NUM_CTA_CTE,
                            v_COD_DVI,
                            v_FLG_CGO_ABN
                            FROM PABS_DT_DETLL_CARGO_ABONO_CAJA
                            WHERE  NUM_REF_SWF = v_NUM_REF_SWF
                            AND ROWNUM = 1
                            AND NUM_REF_EXT = v_NUM_REF_EXT;



                            SELECT IMP_EGR_DIA,
                                   IMP_ING_DIA,
                                   IMP_RAL_AOS
                            INTO
                                   v_IMP_EGR_DIA,
                                   v_IMP_ING_DIA,
                                   v_IMP_RAL_AOS
                            FROM PABS_DT_DETLL_SALDO_CAJA_MN_MX
                            WHERE SUBSTR(COD_BCO_ORG,0,8) = SUBSTR(v_COD_BCO_ORG,0,9)
                            AND NUM_CTA_CTE = v_NUM_CTA_CTE
                            AND COD_DVI = v_COD_DVI;


                             -- CALCULO NUEVOS SALDOS (EGRESO Y TOTAL)  PARA ACTUALIZAR EL MOVIMIENTO

                            v_IMP_EGR_DIA_PAR := NVL(v_IMP_EGR_DIA,0) + NVL(v_IMP_OPE,0); -- NUEVO MONTO DE EGRESO DEL DIA

                            v_IMP_RAL_AOS_PAR := NVL(v_IMP_EGR_DIA_PAR,0) - NVL(v_IMP_ING_DIA,0); -- NUEVO MONTO TOTAL DEL DIA

                           -- AHORA ACTUALIZAMOS EL MONTO EN LAS TABLAS DE SALIDA

                            UPDATE PABS_DT_DETLL_SALDO_CAJA_MN_MX
                            SET
                            IMP_EGR_DIA = v_IMP_EGR_DIA_PAR,
                            IMP_RAL_AOS = v_IMP_RAL_AOS_PAR
                            WHERE SUBSTR(COD_BCO_ORG,0,8) = SUBSTR(v_COD_BCO_ORG,0,8)
                            AND NUM_CTA_CTE = v_NUM_CTA_CTE
                            AND COD_DVI = v_COD_DVI;


                    WHEN OTHERS THEN
                            err_code := SQLCODE;
                            err_msg := SUBSTR(SQLERRM, 1, 300);
                            p_s_mensaje := err_code || '-' || err_msg;
                            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                            p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                            RAISE_APPLICATION_ERROR(-20000,p_s_mensaje);

                    END;
                END IF;



        EXCEPTION


             WHEN OTHERS THEN

                    err_code := SQLCODE;
                    err_msg := SUBSTR(SQLERRM, 1, 300);
                    p_s_mensaje := err_code || '-' || err_msg;
                    PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                    p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                    RAISE_APPLICATION_ERROR(-20000,p_s_mensaje);

            END;



    EXCEPTION
     WHEN OTHERS THEN

                        err_code := SQLCODE;
                        err_msg := SUBSTR(SQLERRM, 1, 300);
                        p_s_mensaje := err_code || '-' || err_msg;
                        PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                        p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                        RAISE_APPLICATION_ERROR(-20000,p_s_mensaje);

    END  SP_ACT_SLD_ING_MX;

/************************************************************************************************
-- Funcion/Procedimiento: SP_BUS_SALDO_ONLINE_MX
-- Objetivo: Procedimiento almacenado que Muestra los datos de saldo ONLINE
-- Sistema: DBO_PAB.
-- Base de Datos: DBO_PAB.
-- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX
--
-- Fecha: 31-10-2017
-- Autor: Santander
-- Input:
--          p_MONEDA  ->    Moneda a consultar.
--
-- Output:
--          p_CURSOR ->     SYS_REFCURSOR, resultados de consulta.
--          p_ERROR  ->     Indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
-- Input/Output: N/A
-- Retorno: N/A.
-- Observaciones: <Fecha y Detalle de ultimos cambios>
--***********************************************************************************************/
PROCEDURE SP_BUS_SALDO_ONLINE_MX  (p_MONEDA         IN CHAR,
                                   p_CURSOR          OUT SYS_REFCURSOR,
                                   p_ERROR           OUT NUMBER)IS

     v_NOM_SP  VARCHAR2(30) := 'SP_BUS_SALDO_ONLINE_MX';
     v_FECHA_ACTUAL  DATE := TRUNC(SYSDATE);


    BEGIN
        IF p_MONEDA IS NOT NULL THEN
        BEGIN
                --SALDOS INTRADIA
         OPEN p_CURSOR FOR



            SELECT

                TI.DES_BCO  AS DSC_BCO_ORG,
                CC.NUM_CTA_CTE  AS CUENTA_CORRIENTE,
                CC.FEC_ING_CTL  AS FEC_ULT_CART,
                CC.IMP_INI_CTL  AS SALDO_INICIAL,
                CC.IMP_ING_CTL  AS INGRESOS,
                CC.IMP_EGR_CTL  AS EGRESOS,
                CC.IMP_FNL_CTL  AS SALDO_CIERRE,

                CC.IMP_APR_AOS  AS SALDO_APERTURA, -- (SALDO INCIAL DEL DIA)
                CC.IMP_ING_DIA  AS INGRESO_INTRA,
                CC.IMP_EGR_DIA  AS EGRESOS_INTRA,
                CC.IMP_RAL_AOS  AS SALDO_FINAL_INTRA,

                CC.NUM_FOL_CTL  AS FOLIO_CARTOLA,
                CC.COD_BCO_ORG  AS BIC_BANCO,
                CC.COD_DVI  AS MONEDA

            FROM TCDTBAI TI,
            PABS_DT_DETLL_SALDO_CAJA_MN_MX CC
            WHERE CC.COD_BCO_ORG = TI.TGCDSWSA (+)
            AND CC.COD_DVI = p_MONEDA;

          EXCEPTION

            WHEN NO_DATA_FOUND THEN

                    err_code := SQLCODE;
                    err_msg := 'No se han encontrado registos asociados a la busqueda por moneda :' || p_MONEDA;
                    PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                    p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;

            WHEN OTHERS THEN

                    err_code := SQLCODE;
                    err_msg := SUBSTR(SQLERRM, 1, 300);
                    p_s_mensaje := err_code || '-' || err_msg;
                    PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                    p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                    RAISE_APPLICATION_ERROR(-20000,p_s_mensaje);
          END;
          ELSE
          BEGIN
                 --SALDOS INTRADIA
         OPEN p_CURSOR FOR



            SELECT

                TI.DES_BCO  AS DSC_BCO_ORG,
                CC.NUM_CTA_CTE  AS CUENTA_CORRIENTE,
                CC.FEC_ING_CTL  AS FEC_ULT_CART,
                CC.IMP_INI_CTL  AS SALDO_INICIAL,
                CC.IMP_ING_CTL  AS INGRESOS,
                CC.IMP_EGR_CTL  AS EGRESOS,
                CC.IMP_FNL_CTL  AS SALDO_CIERRE,

                CC.IMP_APR_AOS  AS SALDO_APERTURA, -- (SALDO INCIAL DEL DIA)
                CC.IMP_ING_DIA  AS INGRESO_INTRA,
                CC.IMP_EGR_DIA  AS EGRESOS_INTRA,
                CC.IMP_RAL_AOS  AS SALDO_FINAL_INTRA,

                CC.NUM_FOL_CTL  AS FOLIO_CARTOLA,
                CC.COD_BCO_ORG  AS BIC_BANCO,
                CC.COD_DVI  AS MONEDA

            FROM TCDTBAI TI,
            PABS_DT_DETLL_SALDO_CAJA_MN_MX CC
            WHERE CC.COD_BCO_ORG = TI.TGCDSWSA (+)
            AND (CC.IMP_APR_AOS <> 0 OR CC.IMP_TOT_FNL_DIA <> 0 OR CC.IMP_ING_CPR <> 0 OR CC.IMP_EGR_CPR <> 0);
           -- AND CC.COD_DVI = p_MONEDA;

          EXCEPTION

            WHEN NO_DATA_FOUND THEN

                    err_code := SQLCODE;
                    err_msg := 'No se han encontrado registos asociados a la busqueda por moneda :' || p_MONEDA;
                    PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                    p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;

            WHEN OTHERS THEN

                    err_code := SQLCODE;
                    err_msg := SUBSTR(SQLERRM, 1, 300);
                    p_s_mensaje := err_code || '-' || err_msg;
                    PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                    p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                    RAISE_APPLICATION_ERROR(-20000,p_s_mensaje);
          END;
          END IF;

END SP_BUS_SALDO_ONLINE_MX;

/************************************************************************************************
-- Funcion/Procedimiento: SP_PAB_VAL_COD_ABA
-- Objetivo: Procedimiento que busca codigo ABA de banco dado un BIC
-- Sistema: DBO_PAB.
-- Base de Datos: DBO_PAB.
-- Tablas Usadas: DBO_PAB.PABS_DT_BANCO_ALMOT
--
-- Fecha: 27-09-2018
-- Autor: Santander
-- Input:
--          p_COD_ABA  ->    CODIGO ABA A CONSULTAR.
--
-- Output:
--          p_COD_BCO ->     CODIGO BIC CORRESPONDIENTE AL ABA INGRESADO.
--          p_ERROR  ->     Indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
-- Input/Output: N/A
-- Retorno: N/A.
-- Observaciones: <Fecha y Detalle de ultimos cambios>
--***********************************************************************************************/
PROCEDURE SP_PAB_VAL_COD_ABA      (p_COD_ABA IN  CHAR,
                                   p_COD_BCO OUT CHAR,
                                   p_ERROR   OUT NUMBER) IS

    v_NOM_SP   VARCHAR2(30) := 'SP_PAB_VAL_COD_ABA';
    v_COD_ABA  NUMBER := p_COD_ABA;
    v_COD_BCO  CHAR(11);


    BEGIN

        p_ERROR:= PKG_PAB_CONSTANTES.V_OK;

            SELECT COD_BIC_BCO
            INTO v_COD_BCO
            FROM PABS_DT_BANCO_ALMOT
            WHERE COD_ABA_BCO = v_COD_ABA ;

            p_COD_BCO := v_COD_BCO ;

          EXCEPTION

            WHEN NO_DATA_FOUND THEN

                    err_code := SQLCODE;
                    err_msg := 'No existe codigo BIC para el codigo ABA ingresado :' || v_COD_ABA;
                    PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                    p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;

            WHEN TOO_MANY_ROWS THEN

                    err_code := SQLCODE;
                    err_msg := 'Existe mas de un BIC para el codigo ABA ingresado. :' || v_COD_ABA;
                    PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                    p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
            WHEN OTHERS THEN

                    err_code := SQLCODE;
                    err_msg := SUBSTR(SQLERRM, 1, 300);
                    p_s_mensaje := err_code || '-' || err_msg;
                    PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_COD_ABA);
                    p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                    RAISE_APPLICATION_ERROR(-20000,p_s_mensaje);

END SP_PAB_VAL_COD_ABA;

END PKG_PAB_CAJA_MX;