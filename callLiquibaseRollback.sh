JAVA_HOME=/software/jdk1.8.0_221
export JAVA_HOME
PATH=$JAVA_HOME/bin:$PATH
export PATH

export JENKINS_HOME=/var/jenkins_home

export lquser=USR_LIQUIDBASE
export lqpassword=USR_LIQUIDBASE
export hostname=drrcspfdb01.cl.bsch
export portnumber=1651
export sid=SCSPF


bash $JENKINS_HOME/liquibase/liquibase --changeLogFile=$1 --url="jdbc:oracle:thin:@$hostname:$portnumber:$sid"  --username=$lquser --password=$lqpassword  --driver=oracle.jdbc.OracleDriver --classpath=$JENKINS_HOME/liquibase/ojdbc8.jar rollbackCount $2